--*****************************************************************************
--
-- SOFTING Industrial Automation GmbH
-- Richard-Reitzner-Allee 6
-- D-85540 Haar
-- Phone: ++49-89-4 56 56-0
-- Fax: ++49-89-4 56 56-3 99
-- http://www.softing.com
--
-- Copyright (C) SOFTING Industrial Automation GmbH 2011-2015. All Rights Reserved.
--
-- Version: 1.00.00
--
--*****************************************************************************

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;
use ieee.numeric_std.all;

library utils;
use utils.all;

library regulator;
use regulator.all;

--=============================================================================

entity altera_ink_pn is
	generic(
		REGULATOR_CONST_BITS	: natural := 21;
		REGULATOR_CALC_BITS		: natural := 32;
		IRC_MAX_STEPS			: natural := 1999; --- 2000 irc steps
		PWM_MAX_DC				: natural := 1000; -- maximal PWM duty cycle
		PWM_DC_BITS				: natural := 10
	);
  port
  (
    -- clk input and reset
    PortInClk50A                 : in    std_logic;
    PortInClk50B                 : in    std_logic;
    PortInClk50C                 : in    std_logic;

    -- sdram interface
    PortOutDRAMAddr              : out   std_logic_vector(12 downto 0);
    PortOutDRAMBa                : out   std_logic_vector(1 downto 0);
    PortOutDRAMCasN              : out   std_logic;
    PortOutDRAMCke               : out   std_logic;
    PortOutDRAMClk               : out   std_logic;
    PortOutDRAMCsN               : out   std_logic;
    PortBidirDRAMDq              : inout std_logic_vector(31 downto 0);
    PortOutDRAMDqm               : out   std_logic_vector(3 downto 0);
    PortOutDRAMRasN              : out   std_logic;
    PortOutDRAMWeN               : out   std_logic;

    -- sram interface
    PortBidirSRAMDq              : inout std_logic_vector(15 downto 0);
    PortOutSRAMAddr              : out   std_logic_vector(19 downto 0);
    PortOutSRAMUbN               : out   std_logic;
    PortOutSRAMLbN               : out   std_logic;
    PortOutSRAMWeN               : out   std_logic;
    PortOutSRAMCeN               : out   std_logic;
    PortOutSRAMOeN               : out   std_logic;

    -- config flash interface
    PortInConfigFlashData        : in    std_logic;
    PortOutConfigFlashSdo        : out   std_logic;
    PortOutConfigFlashDclk       : out   std_logic;
    PortOutConfigFlashSce        : out   std_logic;

    -- Ethernet Interface
    PortOutEth0GtxClk            : out   std_logic := '0';
    PortInEth0Col                : in    std_logic;
    PortInEth0Crs                : in    std_logic;
    PortInEth0RxClk              : in    std_logic;
    PortInEth0RxData             : in    std_logic_vector(3 downto 0);
    PortInEth0RxDv               : in    std_logic;
    PortInEth0RxError            : in    std_logic;
    PortInEth0TxClk              : in    std_logic;
    PortOutEth0TxData            : out   std_logic_vector(3 downto 0);
    PortOutEth0TxEnable          : out   std_logic;
    PortOutEth0TxError           : out   std_logic;
    PortInEth0Link100            : in    std_logic;

    PortOutEth1GtxClk            : out   std_logic := '0';
    PortInEth1Col                : in    std_logic;
    PortInEth1Crs                : in    std_logic;
    PortInEth1RxClk              : in    std_logic;
    PortInEth1RxData             : in    std_logic_vector(3 downto 0);
    PortInEth1RxDv               : in    std_logic;
    PortInEth1RxError            : in    std_logic;
    PortInEth1TxClk              : in    std_logic;
    PortOutEth1TxData            : out   std_logic_vector(3 downto 0) := ( others => '0' );
    PortOutEth1TxEnable          : out   std_logic := '0';
    PortOutEth1TxError           : out   std_logic := '0';
    PortInEth1Link100            : in    std_logic;

    -- PHY Interface
    PortOutPhy0ResetN            : out   std_logic;
    PortOutPhy1ResetN            : out   std_logic;
    PortInPhy0IntN               : in    std_logic;
    PortInPhy1IntN               : in    std_logic;

    -- MDIO Interface
    PortOutMdioClk0              : out   std_logic;
    PortBidirMdio0               : inout std_logic;
    PortOutMdioClk1              : out   std_logic;
    PortBidirMdio1               : inout std_logic;
    
    -- LED Interface
    PortOutLedGreen              : out   std_logic_vector(8 downto 0);
    PortOutLedRed                : out   std_logic_vector(17 downto 0);

    -- Button Interface
    PortInButton                 : in    std_logic_vector(17 downto 0);

    -- Key Interface
    PortInKey                    : in    std_logic_vector(3 downto 0);

    -- 7-Segment Interface
    PortOut7Seg0                 : out   std_logic_vector(6 downto 0);
    PortOut7Seg1                 : out   std_logic_vector(6 downto 0);
    PortOut7Seg2                 : out   std_logic_vector(6 downto 0);
    PortOut7Seg3                 : out   std_logic_vector(6 downto 0);
    PortOut7Seg4                 : out   std_logic_vector(6 downto 0);
    PortOut7Seg5                 : out   std_logic_vector(6 downto 0);
    PortOut7Seg6                 : out   std_logic_vector(6 downto 0);
    PortOut7Seg7                 : out   std_logic_vector(6 downto 0);

    -- LCD Interface
    PortOutLcdBlOn               : out   std_logic;
    PortBidirLcdData             : inout std_logic_vector(7 downto 0);
    PortOutLcdEN                 : out   std_logic;
    PortOutLcdON                 : out   std_logic;
    PortOutLcdRS                 : out   std_logic;
    PortOutLcdRW                 : out   std_logic;

    -- Auth Interface
    PortOutClk                   : out   std_logic;
    PortOutDataOutValid          : out   std_logic;
    PortOutDataOut               : out   std_logic;
    PortInDataInValid            : in    std_logic;
    PortInDataIn                 : in    std_logic;
	 
	 -- Axis0 
	 PortInAxis0Hal0, PortInAxis0Hal1, PortInAxis0Hal2 				:	in std_logic;
 	 PortInAxis0IrcA, PortInAxis0IrcB, PortInAxis0IrcIdx				:	in std_logic;
    PortOutAxis0Pwm0, PortOutAxis0Pwm1, PortOutAxis0Pwm2				:	out std_logic;
    PortOutAxis0Pwm0En, PortOutAxis0Pwm1En, PortOutAxis0Pwm2En		:	out std_logic
  );
end entity altera_ink_pn;

--=============================================================================

architecture toplevel of altera_ink_pn is

  --===========================================================================
  -- component declarations
  --===========================================================================

  component qsys_profinet_system is
    port
    (
      clk_50_a_clk                                               : in    std_logic                     := 'X';             -- clk
      clk_50_areset_reset_n                                      : in    std_logic                     := 'X';             -- reset_n
      clk_50_b_clk                                               : in    std_logic                     := 'X';             -- clk
      clk_50_b_reset_reset_n                                     : in    std_logic                     := 'X';             -- reset_n
      appl_subsystem_lcd_data                                    : inout std_logic_vector(7 downto 0)  := (others => 'X'); -- data
      appl_subsystem_lcd_E                                       : out   std_logic;                                        -- E
      appl_subsystem_lcd_RS                                      : out   std_logic;                                        -- RS
      appl_subsystem_lcd_RW                                      : out   std_logic;                                        -- RW
      appl_subsystem_seven_segment_export                        : out   std_logic_vector(63 downto 0);                    -- export
      appl_subsystem_pio_key_export                              : in    std_logic_vector(3 downto 0)  := (others => 'X'); -- export
      appl_subsystem_pio_button_export                           : in    std_logic_vector(17 downto 0) := (others => 'X'); -- export
      appl_subsystem_pio_led_red_export                          : out   std_logic_vector(17 downto 0);                    -- export
      appl_subsystem_pio_led_green_export                        : out   std_logic_vector(8 downto 0);                     -- export
      epcs_config_flash_dclk                                     : out   std_logic;                                        -- dclk
      epcs_config_flash_sce                                      : out   std_logic;                                        -- sce
      epcs_config_flash_sdo                                      : out   std_logic;                                        -- sdo
      epcs_config_flash_data0                                    : in    std_logic                     := 'X';             -- data0
      pll_sdram_c1_clk                                           : out   std_logic;                                        -- clk
      sdram_addr                                                 : out   std_logic_vector(12 downto 0);                    -- addr
      sdram_ba                                                   : out   std_logic_vector(1 downto 0);                     -- ba
      sdram_cas_n                                                : out   std_logic;                                        -- cas_n
      sdram_cke                                                  : out   std_logic;                                        -- cke
      sdram_cs_n                                                 : out   std_logic;                                        -- cs_n
      sdram_dq                                                   : inout std_logic_vector(31 downto 0) := (others => 'X'); -- dq
      sdram_dqm                                                  : out   std_logic_vector(3 downto 0);                     -- dqm
      sdram_ras_n                                                : out   std_logic;                                        -- ras_n
      sdram_we_n                                                 : out   std_logic;                                        -- we_n
      tristate_bus_sram_tcm_chipselect_n_out                     : out   std_logic;
      tristate_bus_sram_tcm_outputenable_n_out                   : out   std_logic;
      tristate_bus_sram_tcm_byteenable_n_out                     : out   std_logic_vector(1 downto 0);
      tristate_bus_sram_tcm_write_n_out                          : out   std_logic;
      tristate_bus_address                                       : out   std_logic_vector (20 downto 0);
      tristate_bus_data                                          : inout std_logic_vector (15 downto 0) := (others => 'X');
      pio_performance_indicator_export                           : out   std_logic_vector(31 downto 0);
      test_subsystem_pattern_counter_notification_out_export     : out   std_logic;
      test_subsystem_pattern_ramp_notification_out_export        : out   std_logic;
      test_subsystem_pattern_sinus_notification_out_export       : out   std_logic;
      test_subsystem_pattern_constant_notification_out_export    : out   std_logic;
      test_subsystem_pattern_sink_notification_in_export         : in    std_logic;
      test_subsystem_pattern_mirror_notification_out_export      : out   std_logic;
      test_subsystem_pattern_mirror_notification_in_export       : in    std_logic;
      test_subsystem_pattern_isochronous_notification_in_export  : in    std_logic                     := 'X';
      test_subsystem_pattern_isochronous_notification_out_export : out   std_logic;
      test_subsystem_pattern_isochronous_isochronous_red_enable  : in    std_logic                     := 'X';
      test_subsystem_pattern_isochronous_isochronous_data_valid  : out   std_logic;
      pll_sdram_locked_export                                    : out   std_logic;                                        -- export
      internal_pio_reset_cpu_fieldbus_export                     : out   std_logic_vector(1 downto 0);                     -- export
      internal_pio_int_to_fieldbus_export                        : out   std_logic;                                        -- export
      internal_pio_int_from_fieldbus_export                      : in    std_logic_vector(2 downto 0)  := (others => 'X'); -- export
      internal_pio_int_to_appl_export                            : out   std_logic;                                        -- export
      internal_pio_int_from_appl_export                          : in    std_logic                     := 'X';             -- export
      internal_pio_sync_export                                   : in    std_logic_vector(1 downto 0)  := (others => 'X'); -- export
      rte_subsystem_pio_phy_export                               : in    std_logic_vector(1 downto 0)  := (others => 'X'); -- export
      rte_subsystem_reset                                        : in    std_logic                     := 'X';             -- reset
      rte_subsystem_cpu_reset_resetrequest                       : in    std_logic                     := 'X';             -- resetrequest
      rte_subsystem_cpu_reset_resettaken                         : out   std_logic;                                        -- resettaken
      rte_subsystem_switch_mii_p0_collision                      : in    std_logic                     := 'X';             -- p0_collision
      rte_subsystem_switch_mii_p0_carrier                        : in    std_logic                     := 'X';             -- p0_carrier
      rte_subsystem_switch_mii_p0_rxclock                        : in    std_logic                     := 'X';             -- p0_rxclock
      rte_subsystem_switch_mii_p0_rxerror                        : in    std_logic                     := 'X';             -- p0_rxerror
      rte_subsystem_switch_mii_p0_rxenable                       : in    std_logic                     := 'X';             -- p0_rxenable
      rte_subsystem_switch_mii_p0_rx_red_enable                  : out   std_logic;                                        -- p0_rx_red_enable
      rte_subsystem_switch_mii_p0_rx                             : in    std_logic_vector(3 downto 0)  := (others => 'X'); -- p0_rx
      rte_subsystem_switch_mii_p0_txclock                        : in    std_logic                     := 'X';             -- p0_txclock
      rte_subsystem_switch_mii_p0_txerror                        : out   std_logic;                                        -- p0_txerror
      rte_subsystem_switch_mii_p0_txenable                       : out   std_logic;                                        -- p0_txenable
      rte_subsystem_switch_mii_p0_tx_red_enable                  : out   std_logic;                                        -- p0_tx_red_enable
      rte_subsystem_switch_mii_p0_tx                             : out   std_logic_vector(3 downto 0);                     -- p0_tx
      rte_subsystem_switch_mii_p1_collision                      : in    std_logic                     := 'X';             -- p1_collision
      rte_subsystem_switch_mii_p1_carrier                        : in    std_logic                     := 'X';             -- p1_carrier
      rte_subsystem_switch_mii_p1_rxclock                        : in    std_logic                     := 'X';             -- p1_rxclock
      rte_subsystem_switch_mii_p1_rxerror                        : in    std_logic                     := 'X';             -- p1_rxerror
      rte_subsystem_switch_mii_p1_rxenable                       : in    std_logic                     := 'X';             -- p1_rxenable
      rte_subsystem_switch_mii_p1_rx_red_enable                  : out   std_logic;                                        -- p1_rx_red_enable
      rte_subsystem_switch_mii_p1_rx                             : in    std_logic_vector(3 downto 0)  := (others => 'X'); -- p1_rx
      rte_subsystem_switch_mii_p1_txclock                        : in    std_logic                     := 'X';             -- p1_txclock
      rte_subsystem_switch_mii_p1_txerror                        : out   std_logic;                                        -- p1_txerror
      rte_subsystem_switch_mii_p1_txenable                       : out   std_logic;                                        -- p1_txenable
      rte_subsystem_switch_mii_p1_tx_red_enable                  : out   std_logic;                                        -- p1_tx_red_enable
      rte_subsystem_switch_mii_p1_tx                             : out   std_logic_vector(3 downto 0);                     -- p1_tx
      rte_subsystem_switch_mdio_clock                            : out   std_logic;                                        -- clock
      rte_subsystem_switch_mdio_in                               : in    std_logic                     := 'X';             -- in
      rte_subsystem_switch_mdio_out                              : out   std_logic;                                        -- out
      rte_subsystem_switch_mdio_outenable                        : out   std_logic;                                        -- outenable
      rte_subsystem_switch_mdio_portselect                       : out   std_logic;                                        -- portselect
      rte_subsystem_switch_signal_sync                           : out   std_logic;                                        -- sync
      rte_subsystem_switch_signal_sync_timestamp                 : out   std_logic_vector (31 downto 0);                   -- sync
      rte_subsystem_switch_signal_cycle_counter                  : out   std_logic_vector (15 downto 0);                   -- sync
      rte_subsystem_switch_signal_31_25us_base_clk               : out   std_logic;                                        -- sync
      rte_subsystem_switch_signal_cycle_timestamp                : out   std_logic_vector (15 downto 0);                   -- sync
      rte_subsystem_switch_signal_consumer_update                : out   std_logic;                                        -- cputx
      rte_subsystem_switch_signal_provider_update                : out   std_logic;                                        -- cputx
      rte_subsystem_switch_signal_cputx                          : out   std_logic;                                        -- cputx
      rte_subsystem_switch_signal_cpurx                          : out   std_logic;                                        -- cpurx
      rte_subsystem_switch_auth_clk                              : out   std_logic;                                        -- clk
      rte_subsystem_switch_auth_dataoutvalid                     : out   std_logic;                                        -- dataoutvalid
      rte_subsystem_switch_auth_dataout                          : out   std_logic;                                        -- dataout
      rte_subsystem_switch_auth_datainvalid                      : in    std_logic                     := 'X';             -- datainvalid
      rte_subsystem_switch_auth_datain                           : in    std_logic                     := 'X';             -- datain
      rte_subsystem_data_dma_running_input                       : out   std_logic;
      rte_subsystem_data_dma_running_output                      : out   std_logic;
      rte_subsystem_dma_notification_in_export                   : in    std_logic_vector (63 downto 0):= (others => 'X');
      rte_subsystem_dma_notification_out_export                  : out   std_logic_vector (63 downto 0);
      internal_switch_clockshift_90                              : in    std_logic                     := 'X';             -- 90
      internal_switch_clockshift_180                             : in    std_logic                     := 'X';             -- 180
      internal_switch_clockshift_270                             : in    std_logic                     := 'X';             -- 270
      pll_125mhz_locked_export                                   : out   std_logic;                                        -- export
      pll_125mhz_90_clk                                          : out   std_logic;                                        -- clk
      pll_125mhz_180_clk                                         : out   std_logic;                                        -- clk
      pll_125mhz_270_clk                                         : out   std_logic;                                         -- clk
		
		-- Motor control
		appl_subsystem_pio_axis0_reference_export							: out std_logic_vector(31 downto 0) -- axis0 reference
    );
  end component qsys_profinet_system;
  
  --===========================================================================
  -- type definitions
  --===========================================================================

  --===========================================================================
  -- constants
  --===========================================================================

  --===========================================================================
  -- signal declarations
  --===========================================================================

  signal SystemResetN            : std_ulogic;
  signal PllSdramLocked          : std_ulogic;
  signal PllIrtLocked            : std_ulogic;

  signal MdioClock               : std_ulogic;
  signal MdioInput               : std_ulogic;
  signal MdioOutput              : std_ulogic;
  signal MdioOutputEnable        : std_ulogic;
  signal MdioChannelSelect       : std_ulogic;

  signal Eth0TxEnable            : std_ulogic := '0';
  signal Eth1TxEnable            : std_ulogic := '0';

  signal IntFromFieldbusToAppl   : std_ulogic;
  signal IntFromApplToFieldbus   : std_ulogic;

  signal FieldbusInputDmaRunning : std_ulogic;
  signal FieldbusOutputDmaRunning: std_ulogic;

  -- each of the notification represents an SDAI unit
  -- 0    : head module
  -- 1 - 4: reserved for software units
  signal DmaNotificationIn       : std_logic_vector (63 downto 0) := ( others => '0' );
  signal DmaNotificationOut      : std_logic_vector (63 downto 0);

  signal ResetFieldbusSubsystem  : std_ulogic;
  signal ResetCpuFieldbus        : std_ulogic;

  signal CPURxEnable             : std_ulogic;
  signal CPUTxEnable             : std_ulogic;
  signal Eth0RxRedEnable         : std_ulogic := '0';
  signal Eth0TxRedEnable         : std_ulogic := '0';
  signal Eth1RxRedEnable         : std_ulogic := '0';
  signal Eth1TxRedEnable         : std_ulogic := '0';
  signal RteSyncSignal           : std_ulogic;
  signal SyncTimestamp           : std_logic_vector (31 downto 0);
  signal CycleCounter            : std_logic_vector (15 downto 0);
  signal PROFINETBaseClock       : std_ulogic;
  signal CycleTimestamp          : std_logic_vector (15 downto 0);
  signal ConsumerUpdate          : std_ulogic;
  signal ProviderUpdate          : std_ulogic;
  
  signal Output7Segment          : std_logic_vector (63 downto 0);

  signal ShiftedClocks           : std_logic_vector (2 downto 0);
  
  signal SRamAddress             : std_logic_vector (20 downto 0);

  signal PerformanceIndicator    : std_logic_vector (31 downto 0);
  signal IsochronousUnitDataValid: std_ulogic;
  signal RedEnable               : std_ulogic;
  
  -- Motor control
  signal Axis0EnaPwmA, Axis0EnaPwmB, Axis0EnaPwmC	: std_logic;
  signal Axis0PwmAOut, Axis0PwmBOut, Axis0PwmCOut 	: std_logic;
  signal Axis0PwmGenOut										: std_logic;
  signal Axis0PwmDC											: signed(31 downto 0);

  signal Axis0IrcEvent										: std_logic;
  signal Axis0IrcDirection									: std_logic;
  signal Axis0IrcCount										: std_logic_vector(31 downto 0);

  signal Axis0Reference										: signed(31 downto 0);
  signal Axis0ReferenceSlv									: std_logic_vector(31 downto 0);
  signal Axis0RegulatorClock								: std_logic;
  signal Axis0MeasuredSpeed								: signed(REGULATOR_CALC_BITS -1 downto 0);
	
  
  --===========================================================================
  -- function declarations
  --===========================================================================

  --===========================================================================
  -- Timing constraints for intermediate signals
  --===========================================================================

  attribute preserve                      : boolean;
  attribute preserve of Eth0RxRedEnable   : signal is true;
  attribute preserve of Eth0TxRedEnable   : signal is true;
  attribute preserve of Eth1RxRedEnable   : signal is true;
  attribute preserve of Eth1TxRedEnable   : signal is true;
  attribute preserve of SyncTimestamp     : signal is true;
  attribute preserve of CycleCounter      : signal is true;
  attribute preserve of PROFINETBaseClock : signal is true;
  attribute preserve of CycleTimestamp    : signal is true;
  attribute preserve of ConsumerUpdate    : signal is true;
  attribute preserve of ProviderUpdate    : signal is true;

  --###########################################################################

begin

  cpu_system_inst : qsys_profinet_system
  port map
  (
    clk_50_a_clk                                               => PortInClk50A,
    clk_50_areset_reset_n                                      => SystemResetN,
    clk_50_b_clk                                               => PortInClk50B,
    clk_50_b_reset_reset_n                                     => SystemResetN,
    appl_subsystem_lcd_data                                    => PortBidirLcdData,
    appl_subsystem_lcd_E                                       => PortOutLcdEN,
    appl_subsystem_lcd_RS                                      => PortOutLcdRS,
    appl_subsystem_lcd_RW                                      => PortOutLcdRW,
    appl_subsystem_seven_segment_export                        => Output7Segment,
    appl_subsystem_pio_key_export                              => PortInKey,
    appl_subsystem_pio_button_export                           => PortInButton,
    -- appl_subsystem_pio_led_red_export                          => PortOutLedRed,
    -- appl_subsystem_pio_led_green_export                        => PortOutLedGreen
    epcs_config_flash_dclk                                     => PortOutConfigFlashDclk,
    epcs_config_flash_sce                                      => PortOutConfigFlashSce,
    epcs_config_flash_sdo                                      => PortOutConfigFlashSdo,
    epcs_config_flash_data0                                    => PortInConfigFlashData,
    pll_sdram_c1_clk                                           => PortOutDRAMClk,
    sdram_addr                                                 => PortOutDRAMAddr,
    sdram_ba                                                   => PortOutDRAMBa,
    sdram_cas_n                                                => PortOutDRAMCasN,
    sdram_cke                                                  => PortOutDRAMCke,
    sdram_cs_n                                                 => PortOutDRAMCsN,
    sdram_dq                                                   => PortBidirDRAMDq,
    sdram_dqm                                                  => PortOutDRAMDqm,
    sdram_ras_n                                                => PortOutDRAMRasN,
    sdram_we_n                                                 => PortOutDRAMWeN,
    tristate_bus_sram_tcm_chipselect_n_out                     => PortOutSRAMCeN,
    tristate_bus_sram_tcm_outputenable_n_out                   => PortOutSRAMOeN,
    tristate_bus_sram_tcm_byteenable_n_out(0)                  => PortOutSRAMLbN,
    tristate_bus_sram_tcm_byteenable_n_out(1)                  => PortOutSRAMUbN,
    tristate_bus_sram_tcm_write_n_out                          => PortOutSRAMWeN,
    tristate_bus_address                                       => SRamAddress,
    tristate_bus_data                                          => PortBidirSRAMDq,
    pio_performance_indicator_export                           => PerformanceIndicator,
    test_subsystem_pattern_counter_notification_out_export     => DmaNotificationIn (32),
    test_subsystem_pattern_ramp_notification_out_export        => DmaNotificationIn (33),
    test_subsystem_pattern_sinus_notification_out_export       => DmaNotificationIn (34),
    test_subsystem_pattern_constant_notification_out_export    => DmaNotificationIn (35),
    test_subsystem_pattern_sink_notification_in_export         => DmaNotificationOut (36),
    test_subsystem_pattern_mirror_notification_out_export      => DmaNotificationIn (37),
    test_subsystem_pattern_mirror_notification_in_export       => DmaNotificationOut (37),
    test_subsystem_pattern_isochronous_notification_out_export => DmaNotificationIn (60),
    test_subsystem_pattern_isochronous_notification_in_export  => DmaNotificationOut (60),
    test_subsystem_pattern_isochronous_isochronous_red_enable  => RedEnable,
    test_subsystem_pattern_isochronous_isochronous_data_valid  => IsochronousUnitDataValid,
    pll_sdram_locked_export                                    => PllSdramLocked,
    internal_pio_reset_cpu_fieldbus_export(0)                  => ResetCpuFieldbus,
    internal_pio_reset_cpu_fieldbus_export(1)                  => ResetFieldbusSubsystem,
    internal_pio_int_to_fieldbus_export                        => IntFromApplToFieldbus,
    internal_pio_int_from_fieldbus_export                      => FieldbusOutputDmaRunning & FieldbusInputDmaRunning & IntFromFieldbusToAppl,
    internal_pio_int_to_appl_export                            => IntFromFieldbusToAppl,
    internal_pio_int_from_appl_export                          => IntFromApplToFieldbus,
    internal_pio_sync_export                                   => '0' & RteSyncSignal,
    rte_subsystem_pio_phy_export(0)                            => PortInPhy0IntN,
    rte_subsystem_pio_phy_export(1)                            => PortInPhy1IntN,
    rte_subsystem_reset                                        => ResetFieldbusSubsystem,
    rte_subsystem_cpu_reset_resetrequest                       => ResetCpuFieldbus,
    rte_subsystem_cpu_reset_resettaken                         => open,
    rte_subsystem_switch_mii_p0_collision                      => PortInEth0Col,
    rte_subsystem_switch_mii_p0_carrier                        => PortInEth0Crs,
    rte_subsystem_switch_mii_p0_rxclock                        => PortInEth0RxClk,
    rte_subsystem_switch_mii_p0_rxerror                        => PortInEth0RxError,
    rte_subsystem_switch_mii_p0_rxenable                       => PortInEth0RxDv,
    rte_subsystem_switch_mii_p0_rx_red_enable                  => Eth0RxRedEnable,
    rte_subsystem_switch_mii_p0_rx                             => PortInEth0RxData,
    rte_subsystem_switch_mii_p0_txclock                        => PortInEth0TxClk,
    rte_subsystem_switch_mii_p0_txerror                        => PortOutEth0TxError,
    rte_subsystem_switch_mii_p0_txenable                       => Eth0TxEnable,
    rte_subsystem_switch_mii_p0_tx_red_enable                  => Eth0TxRedEnable,
    rte_subsystem_switch_mii_p0_tx                             => PortOutEth0TxData,
    rte_subsystem_switch_mii_p1_collision                      => PortInEth1Col,
    rte_subsystem_switch_mii_p1_carrier                        => PortInEth1Crs,
    rte_subsystem_switch_mii_p1_rxclock                        => PortInEth1RxClk,
    rte_subsystem_switch_mii_p1_rxerror                        => PortInEth1RxError,
    rte_subsystem_switch_mii_p1_rxenable                       => PortInEth1RxDv,
    rte_subsystem_switch_mii_p1_rx_red_enable                  => Eth1RxRedEnable,
    rte_subsystem_switch_mii_p1_rx                             => PortInEth1RxData,
    rte_subsystem_switch_mii_p1_txclock                        => PortInEth1TxClk,
    rte_subsystem_switch_mii_p1_txerror                        => PortOutEth1TxError,
    rte_subsystem_switch_mii_p1_txenable                       => Eth1TxEnable,
    rte_subsystem_switch_mii_p1_tx_red_enable                  => Eth1TxRedEnable,
    rte_subsystem_switch_mii_p1_tx                             => PortOutEth1TxData,
    rte_subsystem_switch_mdio_clock                            => MdioClock,
    rte_subsystem_switch_mdio_in                               => MdioInput,
    rte_subsystem_switch_mdio_out                              => MdioOutput,
    rte_subsystem_switch_mdio_outenable                        => MdioOutputEnable,
    rte_subsystem_switch_mdio_portselect                       => MdioChannelSelect,
    rte_subsystem_switch_signal_sync                           => RteSyncSignal,
    rte_subsystem_switch_signal_sync_timestamp                 => SyncTimestamp,
    rte_subsystem_switch_signal_cycle_counter                  => CycleCounter,
    rte_subsystem_switch_signal_31_25us_base_clk               => PROFINETBaseClock,
    rte_subsystem_switch_signal_cycle_timestamp                => CycleTimestamp,
    rte_subsystem_switch_signal_consumer_update                => ConsumerUpdate,
    rte_subsystem_switch_signal_provider_update                => ProviderUpdate,
    rte_subsystem_switch_signal_cputx                          => CPUTxEnable,
    rte_subsystem_switch_signal_cpurx                          => CPURxEnable,
    rte_subsystem_switch_auth_clk                              => PortOutClk,
    rte_subsystem_switch_auth_dataoutvalid                     => PortOutDataOutValid,
    rte_subsystem_switch_auth_dataout                          => PortOutDataOut,
    rte_subsystem_switch_auth_datainvalid                      => PortInDataInValid,
    rte_subsystem_switch_auth_datain                           => PortInDataIn,
    rte_subsystem_data_dma_running_input                       => FieldbusInputDmaRunning,
    rte_subsystem_data_dma_running_output                      => FieldbusOutputDmaRunning,
    rte_subsystem_dma_notification_in_export                   => DmaNotificationIn,
    rte_subsystem_dma_notification_out_export                  => DmaNotificationOut,
    internal_switch_clockshift_90                              => ShiftedClocks (0),
    internal_switch_clockshift_180                             => ShiftedClocks (1),
    internal_switch_clockshift_270                             => ShiftedClocks (2),
    pll_125mhz_locked_export                                   => PllIrtLocked,
    pll_125mhz_90_clk                                          => ShiftedClocks (0),
    pll_125mhz_180_clk                                         => ShiftedClocks (1),
    pll_125mhz_270_clk                                         => ShiftedClocks (2),
	 
	 -- Motor control
	 appl_subsystem_pio_axis0_reference_export						=> Axis0ReferenceSlv
  );
  
  --###########################################################################
  
  SystemResetN        <= (PllIrtLocked and PllSdramLocked);
   
  PortOutPhy0ResetN   <= SystemResetN;
  PortOutPhy1ResetN   <= SystemResetN;
  PortOutEth0GtxClk   <= '0';
  PortOutEth1GtxClk   <= '0';
  
  PortOutMdioClk0     <= MdioClock;
  PortOutMdioClk1     <= MdioClock;
  PortBidirMdio0      <= MdioOutput when ((MdioOutputEnable = '1') and (MdioChannelSelect = '0')) else 'Z';
  PortBidirMdio1      <= MdioOutput when ((MdioOutputEnable = '1') and (MdioChannelSelect = '1')) else 'Z';
  MdioInput           <= PortBidirMdio0 when (MdioChannelSelect = '0') else PortBidirMdio1;
  
  PortOutEth0TxEnable <= Eth0TxEnable;
  PortOutEth1TxEnable <= Eth1TxEnable;
  
  PortOutSRAMAddr     <= SRamAddress (20 downto 1);

  --###########################################################################

  RedEnable           <= (Eth0RxRedEnable or Eth0TxRedEnable or Eth1RxRedEnable or Eth1TxRedEnable);

  --###########################################################################

  PortOut7Seg7        <= Output7Segment(62 downto 56);
  PortOut7Seg6        <= Output7Segment(54 downto 48);
  PortOut7Seg5        <= Output7Segment(46 downto 40);
  PortOut7Seg4        <= Output7Segment(38 downto 32);
  PortOut7Seg3        <= Output7Segment(30 downto 24);
  PortOut7Seg2        <= Output7Segment(22 downto 16);
  PortOut7Seg1        <= Output7Segment(14 downto 8);
  PortOut7Seg0        <= Output7Segment(6 downto 0);

  --###########################################################################

  PortOutLcdON        <= '1';
  PortOutLcdBlOn      <= '0';

  --###########################################################################
  -- Motor control
  --###########################################################################

  -- signal conversion
  ----
  Axis0Reference <= signed(Axis0ReferenceSlv);
  -- Axis0Reference <= to_signed(40, Axis0Reference'length);
  
  -- BLDC motor commutator
  ----
	commutator: CommutatorHALL
	port map(
		HALL_A	=> PortInAxis0Hal0,
		HALL_B	=> PortInAxis0Hal1,
		HALL_C	=> PortInAxis0Hal2,
		DIR		=> Axis0PwmDC(Axis0PwmDC'high), -- PWM_DC is signed (MSB is sign)
		CHAN_A_ENA => Axis0EnaPwmA,
		CHAN_B_ENA => Axis0EnaPwmB,
		CHAN_C_ENA => Axis0EnaPwmC
	);
	
	----
	-- Quadrature encoder
	----
	
	quadr: QuadCount
	port map(
		clk			=> PortInClk50A,
		chan_A_in	=> PortInAxis0IrcA,
		chan_B_in	=> PortInAxis0IrcB,
		irc_index_in=> PortInAxis0IrcIdx,
		cnt_event	=> Axis0IrcEvent,
		cnt_out		=> Axis0IrcCount,
		cnt_way		=> Axis0IrcDirection
	);
	
	----
	-- PWM generator
	----
	pwm_gen: PWMGenerator
	generic map(
		DUTY_MAX => PWM_MAX_DC
	)
	port map(
		clk => PortInClk50A,
		duty_cycle => Axis0PwmDC,
		pwm_out => Axis0PwmGenOut
	);
	
	----
	-- PID regulator
	----
	
	pid: RegulatorPID
	generic map(
		VAL_BITS	=> REGULATOR_CONST_BITS,
		CALC_BITS	=> REGULATOR_CALC_BITS,
		PWM_DC_MAX_INTEGER => PWM_MAX_DC
	)
	port map(
		REGULATOR_CLOCK		=> Axis0RegulatorClock,
		ACLR						=> '0',
		
		REFERENCE			=> Axis0Reference,
		STATE_MEAS			=> Axis0MeasuredSpeed,
		
		REGULATOR_UPDATE	=> '0',
		REGULATOR_KP		=> (others=>'0'),
		REGULATOR_KI		=> (others=>'0'),
		REGULATOR_KD		=> (others=>'0'),
		
		ACTION				=> Axis0PwmDC
	);
	
	----
	-- Regulator clock generator
	-- generates clock for the regulator
	-- Dividers output frequency can be changed by changing PRESCALER_SIZE. 
	-- For fixed frequency you might want use ClockDivider entity
	----
	clkgen: ClockDividerVariable 
	generic map(
		PRESCALER_SIZE_BITS	=> REGULATOR_CONST_BITS
	)
	port map(
		CLK_SYS	=> PortInClk50A,
		ACLR	=> '0',
		UPDATE	=> '0', 
		PRESCALER_SIZE => to_unsigned(25000, REGULATOR_CONST_BITS),
		CLK_OUT	=> Axis0RegulatorClock
	);
	
	----
	-- Input differentiator
	-- calculates the motor speed from the change of position
	----
	diff: Differentiator
	generic map(
		BUS_SIZE	=> 32,
		IRC_MAX => IRC_MAX_STEPS
	)
	port map(
		SYSCLK => PortInClk50A,
		CLK	=> Axis0RegulatorClock,
		ACLR	=> '0',
		
		IRC_EVENT	=> Axis0IrcEvent,
		IRC_DIR		=> Axis0IrcDirection,
		SPEED		=> Axis0MeasuredSpeed
	);
	
	----
	-- OUTPUT
	----
	PortOutLedRed <= std_logic_vector(Axis0PwmDC(17 downto 0));
	Axis0PwmAOut <= Axis0PwmGenOut and Axis0EnaPwmA;
	Axis0PwmBOut <= Axis0PwmGenOut and Axis0EnaPwmB;
	Axis0PwmCOut <= Axis0PwmGenOut and Axis0EnaPwmC;

	PortOutAxis0Pwm0 <= Axis0PwmAOut;
	PortOutAxis0Pwm1 <= Axis0PwmBOut;
	PortOutAxis0Pwm2 <= Axis0PwmCOut;
	
	PortOutAxis0Pwm0En <= '0';
	PortOutAxis0Pwm1En <= '0';
	PortOutAxis0Pwm2En <= '0';
	
		PortOutLedGreen(2 downto 0) <= Axis0PwmAOut & Axis0PwmBOut & Axis0PwmCOut;
	
	PortOutLedGreen(5 downto 3) <= PortInAxis0Hal2 & PortInAxis0Hal1 & PortInAxis0Hal0;
	
end toplevel;
