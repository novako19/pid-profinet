--------------------------------------------------------------------------------
--                       QUADRATURE ENCODER COUNTER
--
-- author: Tomas Rycl <rycltom1@fel.cvut.cz>
-- edited by: Ondrej Novak <novako19@fel.cvut.cz> (added cnt_event output and renamed entity)
-- 
-- description: Quadrature encoder counter to be used with PXMC library 
-- for motion control.
--
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.all;
LIBRARY std;
USE std.standard.all;
library utils;
use utils.all;


--------------------------------------------------------------------------------
--entity: QuadCount
-----------------------
--INPUTS
--clk: system clock
--chan_A: channel A
--chan_B: channel B
-----------------------
--OUTPUTS
--cnt_out: counter output value
-----------------------
--DESCRIPTION
--Quadratic counter will compute the value and sign of the signals.
--There are generally 3 modes possible:
--1x)Update counter after 1 full pulse from both channels. eg. A1 B1 A0 B0, then counter = 1
--2x)Update counter after 1 full pulse from 1 channel. eg. A1 B1 A0 then counter = 1, A1 B1 A0 B0 then counter = 2
--4x)Update counter after any event (falling, rising edges). eg. A1 B1 then counter = 2, A1 B1 A0 B0 then counter = 4
--
--        _____________________
--        |                    |
-- chan_A |                    |
--________|                    |_____________________
--
--                   ___________________
--                   |                  |
-- chan_B            |                  | 
--___________________|                  |____________________
--
--       A1          B1        A0        B0
--
-- This code uses mode 4x.
-- TODO: Is debounce filter necessary?         
--------------------------------------------------------------------------------
ENTITY QuadCount is
        port(
            clk         : in    std_logic;
            chan_A_in   : in    std_logic;
            chan_B_in   : in    std_logic;
            irc_index_in        : in    std_logic;                          --signals 1 full rotation
            cnt_out     : out   std_logic_vector(31 downto 0);      --Signed integer in program
            irc_index_out       : out   std_logic_vector(31 downto 0);  --Index value relative to counter
            --bud pridat cnt_index_zmena a cnt_index_ack nebo 4bit index counter
            irc_index_cnt_out   : out   std_logic_vector(3 downto 0); --signalization of index changed
            cnt_ovrflw  : out   std_logic;                          --counter overflew, starting again from 0
            cnt_way     : out   std_logic;                           --'1'=UP or '0'=DOWN
            cnt_event	: out	std_logic							-- '1' for one clock period when counter is updated
        );      
END QuadCount;

ARCHITECTURE behavioral_counter of QuadCount is
    -- components declarations
    component debounce_filter is
        port(
            clk         : in    std_logic;
            sig_in      : in    std_logic;
            sig_out     : out   std_logic
        );      
  end component;
  
component dff3 is
  port
	(
    clk_i   : in std_logic;
    d_i     : in std_logic;
    q_o     : out std_logic
  );
end component;
    -- constant declarations
    constant clk_freq   : integer := 50000000;        --50 MHz input clockit in signed integer format?
    constant cnt_min 	: integer := -(2**30);
    constant cnt_max    : integer := 2**30;
    
    -- signal/variable declarations 
    signal counter      : integer := 0;
    signal chan_A       : std_logic;
    signal chan_B       : std_logic;
    signal chan_A_dff   : std_logic;
    signal chan_B_dff   : std_logic;
	signal chan_A_last	: std_logic;
	signal chan_B_last	: std_logic;			--beacuse synthesizer cannot detect edges on mutliple clocks
    signal chan_A_event : std_logic;
    signal chan_B_event : std_logic;
    signal irc_index_last: std_logic_vector(31 downto 0) := x"00000000";
    signal irc_index_count : integer := 0;
    signal irc_index    : std_logic;
    signal irc_index_dff: std_logic;
    

    -- function declarations

    -- procedure declarations

    -- type declarations
    
    ----------------------------
    -- BODY
    ----------------------------
BEGIN
    ---------------------------------------------------------------------------
    -- Signal steady state
    ---------------------------------------------------------------------------
    --First run the input signals through series of D flip flop
    --to make all the signals synchronized
    dff_chan_a: dff3
    port map
	(
		clk_i   => clk,
		d_i     => chan_A_in,
		q_o     => chan_A_dff
	);

    dff_chan_b: dff3
	port map
	(
		clk_i   => clk,
		d_i     => chan_B_in,
		q_o     => chan_B_dff
	);

    dff_index: dff3
	port map
	(
		clk_i   => clk,
		d_i     => irc_index_in,
		q_o     => irc_index_dff
	);
    
    ---------------------------------------------------------------------------
    -- Debounce filter
    ---------------------------------------------------------------------------
    --Then run the signals through debounce filter
    dbnc_chanA : debounce_filter
    port map
    (
        clk     => clk,
        sig_in  => chan_A_dff,
        sig_out => chan_A
    );
    
    dbnc_chanB : debounce_filter
    port map
    (
        clk     => clk,
        sig_in  => chan_B_dff,
        sig_out => chan_B
    );
    
    dbnc_index : debounce_filter
    port map
    (
        clk     => clk,
        sig_in  => irc_index_dff,
        sig_out => irc_index
    );
    
    ---------------------------------------------------------------------------
    -- Counter process
    ---------------------------------------------------------------------------
    quad_proc: PROCESS(clk)
    BEGIN
        if(clk'event and clk='1') then
          --Find edges
          chan_A_event <= chan_A xor chan_A_last;
          chan_B_event <= chan_B xor chan_B_last;
          cnt_ovrflw   <= '0';
          
          if(chan_A_event = '1' or chan_B_event = '1') then
          	
          	cnt_event <= '1';
          	
          else
          	
          	cnt_event <= '0';
          	
          end if; -- chan_A_event = '1' or chan_B_event = '1'
          
		  --Detect overflow
          if(counter >= cnt_max or counter <= cnt_min) then
              counter     <= 0;
              cnt_ovrflw  <= '1' after 20 ns;
          elsif(chan_A_event = '1' and chan_A = '1' and chan_B = '0') then
		      counter <= counter + 1;
			  cnt_way <= '1';
          elsif(chan_A_event = '1' and chan_A = '1' and chan_B = '1') then
                counter <= counter - 1;
                cnt_way <= '0';
          elsif(chan_A_event = '1' and chan_A = '0' and chan_B = '0') then
                counter <= counter - 1;
                cnt_way <= '0';
          elsif(chan_A_event = '1' and chan_A = '0' and chan_B = '1') then  
                counter <= counter + 1;
                cnt_way <= '1';
          elsif(chan_B_event = '1' and chan_B = '1' and chan_A = '0') then
                counter <= counter - 1;
                cnt_way <= '0';
          elsif(chan_B_event = '1' and chan_B = '1' and chan_A = '1') then
                counter <= counter + 1;
                cnt_way <= '1';
          elsif(chan_B_event = '1' and chan_B = '0' and chan_A = '0') then
                counter <= counter + 1;
                cnt_way <= '1';
          elsif(chan_B_event = '1' and chan_B = '0' and chan_A = '1') then  
                counter <= counter - 1;
                cnt_way <= '0';
          end if;		
            
            --Index detection
            if(irc_index = '1') then
                irc_index_out <= std_logic_vector(to_signed(counter, cnt_out'length));
                irc_index_last<= std_logic_vector(to_signed(counter, cnt_out'length));
                irc_index_count <= irc_index_count + 1;
            else
                irc_index_out <= irc_index_last;
            end if;
            
            
            --Update output 
            cnt_out <= std_logic_vector(to_signed(counter, cnt_out'length)); --Do I need to care about endianness?
            irc_index_cnt_out <= std_logic_vector(to_signed(irc_index_count, irc_index_cnt_out'length));
        
            --Store the last values
            chan_A_last <= chan_A after 20 ns;
            chan_B_last <= chan_B after 20 ns;
        end if;
    END PROCESS quad_proc;    
END behavioral_counter;