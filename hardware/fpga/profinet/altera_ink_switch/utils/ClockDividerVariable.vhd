-- -------------------------------------------
-- Title:			Variable clock divider
-- Description:		Divides high frequency input clock
--					to lower frequency output clock.
--					Output clock frequency may be changed by
--					setting required prescaler value on input PRESCALER_SIZE
--					and log. 1 on input UPDATE.
--					Prescaler size can be calculated using equation
--					PRESCALER_SIZE = F_INPUT / (2 * F_OUTPUT),
--					where F_INPUT and F_OUTPUT are frequencies of input 
--					and output clock signals respectively.
--					 
-- Author:			Novak, Ondrej <novako19@fel.cvut.cz>
-- Date:			3/2016
-- -------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clockDividerVariable is
	generic(
    	PRESCALER_SIZE_BITS	: integer := 21
    );
    port(
        CLK_SYS				: in std_logic;
        ACLR 				: in std_logic;
        UPDATE				: in std_logic;
        PRESCALER_SIZE		: in unsigned(PRESCALER_SIZE_BITS - 1 downto 0);
        CLK_OUT		: out std_logic
    );
end entity;

architecture behav of clockDividerVariable is

	signal CLK_in		: std_logic;

begin
	
	divider: process(ACLR, UPDATE, CLK_SYS)
		variable CNT: integer range 0 to (2**PRESCALER_SIZE_BITS -1);
	begin
		
		if ACLR = '1' then
			
			CNT := 0;
			
		elsif UPDATE = '1' then
			
			-- if prescaler size changed and counter has overrun the prescaler size, we need to reset it
			if CNT > (PRESCALER_SIZE - 1) then
				
				CLK_in <= not CLK_in;
				CNT := 0;
				
			end if; -- CNT > PRESCALER_SIZE
			
		elsif rising_edge(CLK_SYS) then
			
			if CNT = (PRESCALER_SIZE - 1) then
				
				CNT := 0;
				CLK_in <= not CLK_in;
				
			else 
				
				CNT := CNT + 1;
				
			end if; -- CNT = (DIVISOR-1) 
				
		end if; -- rising_edge(CLK)
		 
	end process;
	
	CLK_OUT <= CLK_in;

end architecture;