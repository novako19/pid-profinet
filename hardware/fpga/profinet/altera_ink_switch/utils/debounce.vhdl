--------------------------------------------------------------------------------
--                       DEBOUNCE FILTER
--
-- author: Tomas Rycl <rycltom1@fel.cvut.cz>
--
-- description: Debounce filter
--
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.all;
LIBRARY std, work;
USE std.standard.all;
USE work.all;


--------------------------------------------------------------------------------
--entity: quad_count
-----------------------
--INPUTS
--clk
--sig_in: signal to be filtered
-----------------------
--OUTPUTS
--sig_out: debounced signal
-----------------------
--DESCRIPTION
--This component will detect changes on input, but will not change
--the output, until the input remains changed for n clock cycles
--------------------------------------------------------------------------------
ENTITY debounce_filter is
        port(
            clk         : in    std_logic;
            sig_in      : in    std_logic;
            sig_out     : out   std_logic
        );      
END debounce_filter;

ARCHITECTURE behavioral_debounce of debounce_filter is
    -- components declarations

    -- constant declarations
    constant debounce_trshld : integer      := 5;
    
    -- signal/variable declarations 
    signal sig_last         : std_logic     := '0';
    signal sig_out_last     : std_logic     := '0';

    -- function declarations

    -- procedure declarations

    -- type declarations
    
    ----------------------------
    -- BODY
    ----------------------------
BEGIN
    ---------------------------------------------------------------------------
    -- 
    ---------------------------------------------------------------------------
    debounce_proc: PROCESS(clk)
    
    variable steady_count   : integer       := 0; --need to be variable, cause we want to
                                                    --set it 0 the moment the change is detected
                                                    --not the next clock cycle
    
    BEGIN
        if(clk'event and clk='1') then
          --Detect level changes
          if(sig_in /= sig_last) then
            --sig_changed <= '1';
            --reset counter
            steady_count := 0;
          else
            --sig_changed <= '0';
            --increment counter
            steady_count := steady_count + 1;
            --overflow precaution
            if(steady_count >= 2**24) then
                steady_count := debounce_trshld +1;
            end if;
          end if;
          
          --Update output
          if(steady_count >= debounce_trshld) then
            sig_out         <= sig_in;
            sig_out_last    <= sig_in;
          else
            sig_out <= sig_out_last;
          end if;
          sig_last <= sig_in after 10ns;
        end if;
    END PROCESS debounce_proc;    
END behavioral_debounce;