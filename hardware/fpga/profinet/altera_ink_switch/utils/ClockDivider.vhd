-- -------------------------------------------
-- Title:			Clock divider
-- Description:		Divides high frequency input clock
--					to lower frequency output clock.
--					Internally, PRESCALER size if calculated using
--					equation PRESCALER = F_INPUT / (2 * F_OUTPUT),
--					where F_INPUT and F_OUTPUT are frequencies of input 
--					and output clock signals respectively.
--					Input and output frequency should be set up using 
--					appropriate generics. 
-- Author:			Novak, Ondrej <novako19@fel.cvut.cz>
-- Date:			3/2016
-- -------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clockDivider is
	generic(
    	CLK_SYS_FREQ		: integer := 50*1000*1000;
    	CLK_OUT_FREQ	: integer := 1000
    );
    port(
        CLK_SYS			: in std_logic;
        ACLR 		: in std_logic;
        CLK_OUT		: out std_logic
    );
end entity;

architecture behav of clockDivider is

	constant PRESCALER	: integer	:= CLK_SYS_FREQ/(2*CLK_OUT_FREQ);
	
	signal CLK_in		: std_logic;

begin
	
	divider: process(ACLR, CLK_SYS)
		variable CNT: integer range 0 to (PRESCALER-1);
	begin
		
		if ACLR = '1' then
			
			CNT := 0;
			
		elsif rising_edge(CLK_SYS) then
			
			if CNT = (PRESCALER-1) then
				CNT := 0;
				CLK_in <= not CLK_in;
			else 
				
				CNT := CNT + 1;
				
			end if; -- CNT = (DIVISOR-1) 
				
		end if; -- rising_edge(CLK)
		 
	end process;
	
	CLK_OUT <= CLK_in;

end architecture;