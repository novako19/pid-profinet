--------------------------------------------------------------------------------
--                           PWM GENERATOR
--
-- author: Tomas Rycl <rycltom1@fel.cvut.cz>
-- edited by: Ondrej Novak <novako19@fel.cvut.cz> (added support for negative duty cycle, moved DUTY_MAX to generics and renamed the entity)
-- description: PWM generator to be used with PXMC library 
-- on the Altera DE2-115 board.
--
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

--------------------------------------------------------------------------------
--entity: PWMGenerator
-----------------------
--INPUTS
--clk: system clock
--duty_cycle: 0-100% duty cycle of the pwm
--TODO: pwm_frequency as variable input?
-----------------------
--OUTPUTS
--pwm:
-----------------------
--DESCRIPTION
--pwm_generator will output 1 or 0 to drive the output pins of FPGA.
--High level will be at the start of the PWM period.
--Period is calculated as clk/frequency and therefore is in clk ticks.
--              <-----------------PWM PERIOD-------------->
--               ______________                            ____
--               |             |                           |
--               |             |                           | 
--               |             |                           |
-- ______________|             |___________________________| 
--TODO:1)Or position in the middle?
--------------------------------------------------------------------------------
ENTITY PWMGenerator is
	generic (
		DC_BITS		: positive := 32;
		DUTY_MAX	: natural := 1000 -- 100 % duty
	);
    port(
        clk         : in    std_logic;
        duty_cycle  : in    signed(DC_BITS -1  downto 0);      --expected values are 0 - duty_max(1000)
        pwm_out     : out   std_logic
    );      
END PWMGenerator;

ARCHITECTURE behavioral_pwm of PWMGenerator is
    -- components declarations

    -- constant declarations
--    constant clk_freq   : integer := 50000000;          --50 MHz input clock
--    constant pwm_freq   : integer := 50000;            --frequency of pwm
--    constant pwm_period : integer := clk_freq/pwm_freq; --pwm period in ticks
--    constant duty_max   : natural := 1024;              --100% duty
    -------------------
    -- Dirty trick
    -- We set our duty input to actually match H_stop.
    -- It's like having duty_max == pwm_period
    -- It is to avoid floating point division in vhdl code
    
    -- signal/variable declarations
    signal pos_counter  : integer range 0 to DUTY_MAX := 0;                 --position counter       
    signal H_stop       : integer range 0 to DUTY_MAX := 0;                 --jump to Low level
    

    -- function declarations

    -- procedure declarations

    -- type declarations
    
    ----------------------------
    -- BODY
    ----------------------------
BEGIN
	pwm_proc: PROCESS(clk)
		variable H_stop_tmp : integer range(-(2**(DC_BITS-1)) +1) to (2**(DC_BITS-1) -1);
	BEGIN
        if (clk'event and clk = '1') then
            --Update counter
            if (pos_counter = (DUTY_MAX - 1)) then
                pos_counter <= 0;
                --Update position where jump to Low level only after previous session ended
                --H_stop <= conv_integer((duty_cycle/duty_max)*(pwm_period-1));
                H_stop_tmp :=  to_integer(signed(duty_cycle));
                if H_stop_tmp < 0 then
                	
                	H_stop_tmp := - H_stop_tmp;
                	
                end if;
                
                H_stop <= H_stop_tmp;
            else
                pos_counter <= pos_counter + 1;
            end if;
            
            --Update output
            if (pos_counter >= H_stop) then
                pwm_out <= '0';
            else
                pwm_out <= '1';
            end if;
        end if;
    END PROCESS pwm_proc;
END behavioral_pwm;
















