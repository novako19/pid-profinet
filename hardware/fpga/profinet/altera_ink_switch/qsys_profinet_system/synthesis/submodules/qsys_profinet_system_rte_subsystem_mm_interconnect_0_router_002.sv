// (C) 2001-2014 Altera Corporation. All rights reserved.
// Your use of Altera Corporation's design tools, logic functions and other 
// software and tools, and its AMPP partner logic functions, and any output 
// files any of the foregoing (including device programming or simulation 
// files), and any associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License Subscription 
// Agreement, Altera MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your use is for the 
// sole purpose of programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the applicable 
// agreement for further details.



// Your use of Altera Corporation's design tools, logic functions and other 
// software and tools, and its AMPP partner logic functions, and any output 
// files any of the foregoing (including device programming or simulation 
// files), and any associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License Subscription 
// Agreement, Altera MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your use is for the 
// sole purpose of programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the applicable 
// agreement for further details.


// $Id: //acds/rel/14.1/ip/merlin/altera_merlin_router/altera_merlin_router.sv.terp#1 $
// $Revision: #1 $
// $Date: 2014/10/06 $
// $Author: swbranch $

// -------------------------------------------------------
// Merlin Router
//
// Asserts the appropriate one-hot encoded channel based on 
// either (a) the address or (b) the dest id. The DECODER_TYPE
// parameter controls this behaviour. 0 means address decoder,
// 1 means dest id decoder.
//
// In the case of (a), it also sets the destination id.
// -------------------------------------------------------

`timescale 1 ns / 1 ns

module qsys_profinet_system_rte_subsystem_mm_interconnect_0_router_002_default_decode
  #(
     parameter DEFAULT_CHANNEL = 0,
               DEFAULT_WR_CHANNEL = -1,
               DEFAULT_RD_CHANNEL = -1,
               DEFAULT_DESTID = 1 
   )
  (output [98 - 94 : 0] default_destination_id,
   output [24-1 : 0] default_wr_channel,
   output [24-1 : 0] default_rd_channel,
   output [24-1 : 0] default_src_channel
  );

  assign default_destination_id = 
    DEFAULT_DESTID[98 - 94 : 0];

  generate
    if (DEFAULT_CHANNEL == -1) begin : no_default_channel_assignment
      assign default_src_channel = '0;
    end
    else begin : default_channel_assignment
      assign default_src_channel = 24'b1 << DEFAULT_CHANNEL;
    end
  endgenerate

  generate
    if (DEFAULT_RD_CHANNEL == -1) begin : no_default_rw_channel_assignment
      assign default_wr_channel = '0;
      assign default_rd_channel = '0;
    end
    else begin : default_rw_channel_assignment
      assign default_wr_channel = 24'b1 << DEFAULT_WR_CHANNEL;
      assign default_rd_channel = 24'b1 << DEFAULT_RD_CHANNEL;
    end
  endgenerate

endmodule


module qsys_profinet_system_rte_subsystem_mm_interconnect_0_router_002
(
    // -------------------
    // Clock & Reset
    // -------------------
    input clk,
    input reset,

    // -------------------
    // Command Sink (Input)
    // -------------------
    input                       sink_valid,
    input  [112-1 : 0]    sink_data,
    input                       sink_startofpacket,
    input                       sink_endofpacket,
    output                      sink_ready,

    // -------------------
    // Command Source (Output)
    // -------------------
    output                          src_valid,
    output reg [112-1    : 0] src_data,
    output reg [24-1 : 0] src_channel,
    output                          src_startofpacket,
    output                          src_endofpacket,
    input                           src_ready
);

    // -------------------------------------------------------
    // Local parameters and variables
    // -------------------------------------------------------
    localparam PKT_ADDR_H = 67;
    localparam PKT_ADDR_L = 36;
    localparam PKT_DEST_ID_H = 98;
    localparam PKT_DEST_ID_L = 94;
    localparam PKT_PROTECTION_H = 102;
    localparam PKT_PROTECTION_L = 100;
    localparam ST_DATA_W = 112;
    localparam ST_CHANNEL_W = 24;
    localparam DECODER_TYPE = 0;

    localparam PKT_TRANS_WRITE = 70;
    localparam PKT_TRANS_READ  = 71;

    localparam PKT_ADDR_W = PKT_ADDR_H-PKT_ADDR_L + 1;
    localparam PKT_DEST_ID_W = PKT_DEST_ID_H-PKT_DEST_ID_L + 1;



    // -------------------------------------------------------
    // Figure out the number of bits to mask off for each slave span
    // during address decoding
    // -------------------------------------------------------
    localparam PAD0 = log2ceil(64'h1010 - 64'h1000); 
    localparam PAD1 = log2ceil(64'h8000 - 64'h4000); 
    localparam PAD2 = log2ceil(64'h8040 - 64'h8000); 
    localparam PAD3 = log2ceil(64'h10020 - 64'h10000); 
    localparam PAD4 = log2ceil(64'h10108 - 64'h10100); 
    localparam PAD5 = log2ceil(64'h10210 - 64'h10200); 
    localparam PAD6 = log2ceil(64'h10320 - 64'h10300); 
    localparam PAD7 = log2ceil(64'h10340 - 64'h10320); 
    localparam PAD8 = log2ceil(64'h11000 - 64'h10800); 
    localparam PAD9 = log2ceil(64'h11010 - 64'h11000); 
    localparam PAD10 = log2ceil(64'h11020 - 64'h11010); 
    localparam PAD11 = log2ceil(64'h11150 - 64'h11140); 
    localparam PAD12 = log2ceil(64'h28000 - 64'h20000); 
    localparam PAD13 = log2ceil(64'h30800 - 64'h30000); 
    localparam PAD14 = log2ceil(64'h58000 - 64'h50000); 
    localparam PAD15 = log2ceil(64'h880000 - 64'h800000); 
    localparam PAD16 = log2ceil(64'h890000 - 64'h880000); 
    localparam PAD17 = log2ceil(64'h40000000 - 64'h20000000); 
    localparam PAD18 = log2ceil(64'h41000000 - 64'h40000000); 
    localparam PAD19 = log2ceil(64'h50100000 - 64'h50000000); 
    // -------------------------------------------------------
    // Work out which address bits are significant based on the
    // address range of the slaves. If the required width is too
    // large or too small, we use the address field width instead.
    // -------------------------------------------------------
    localparam ADDR_RANGE = 64'h50100000;
    localparam RANGE_ADDR_WIDTH = log2ceil(ADDR_RANGE);
    localparam OPTIMIZED_ADDR_H = (RANGE_ADDR_WIDTH > PKT_ADDR_W) ||
                                  (RANGE_ADDR_WIDTH == 0) ?
                                        PKT_ADDR_H :
                                        PKT_ADDR_L + RANGE_ADDR_WIDTH - 1;
    localparam ADDRESS_ALIASING_W = PKT_ADDR_H > OPTIMIZED_ADDR_H + 1 ? PKT_ADDR_H - OPTIMIZED_ADDR_H - 1 : 0;

    localparam RG = RANGE_ADDR_WIDTH-1;
    localparam REAL_ADDRESS_RANGE = OPTIMIZED_ADDR_H - PKT_ADDR_L;

      reg [PKT_ADDR_W-1 : 0] address;
      always @* begin
        address = {PKT_ADDR_W{1'b0}};
        address [REAL_ADDRESS_RANGE:0] = sink_data[OPTIMIZED_ADDR_H : PKT_ADDR_L];
      end   

    // -------------------------------------------------------
    // Pass almost everything through, untouched
    // -------------------------------------------------------
    assign sink_ready        = src_ready;
    assign src_valid         = sink_valid;
    assign src_startofpacket = sink_startofpacket;
    assign src_endofpacket   = sink_endofpacket;
    wire [PKT_DEST_ID_W-1:0] default_destid;
    wire [24-1 : 0] default_src_channel;




    // -------------------------------------------------------
    // Write and read transaction signals
    // -------------------------------------------------------
    wire read_transaction;
    assign read_transaction  = sink_data[PKT_TRANS_READ];


    qsys_profinet_system_rte_subsystem_mm_interconnect_0_router_002_default_decode the_default_decode(
      .default_destination_id (default_destid),
      .default_wr_channel   (),
      .default_rd_channel   (),
      .default_src_channel  (default_src_channel)
    );

    always @* begin
        src_data    = sink_data;
        src_channel = default_src_channel;
        src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = default_destid;

        // --------------------------------------------------
        // Addresss aliasing will be routed to default slave (if present)
        // --------------------------------------------------
        if ( PKT_ADDR_H == OPTIMIZED_ADDR_H || 
             (|sink_data[PKT_ADDR_H:PKT_ADDR_H - ADDRESS_ALIASING_W] == 0) ) begin

        // --------------------------------------------------
        // Address Decoder
        // Sets the channel and destination ID based on the address
        // --------------------------------------------------

    // ( 0x1000 .. 0x1010 )
    if ( {address[RG:PAD0],{PAD0{1'b0}}} == 31'h1000   ) begin
            src_channel = 24'b000000100000000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 2;
    end

    // ( 0x4000 .. 0x8000 )
    if ( {address[RG:PAD1],{PAD1{1'b0}}} == 31'h4000   ) begin
            src_channel = 24'b000000000001000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 6;
    end

    // ( 0x8000 .. 0x8040 )
    if ( {address[RG:PAD2],{PAD2{1'b0}}} == 31'h8000   ) begin
            src_channel = 24'b000000000010000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 8;
    end

    // ( 0x10000 .. 0x10020 )
    if ( {address[RG:PAD3],{PAD3{1'b0}}} == 31'h10000   ) begin
            src_channel = 24'b000100000000000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 23;
    end

    // ( 0x10100 .. 0x10108 )
    if ( {address[RG:PAD4],{PAD4{1'b0}}} == 31'h10100   ) begin
            src_channel = 24'b000000000000000100000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 12;
    end

    // ( 0x10200 .. 0x10210 )
    if ( {address[RG:PAD5],{PAD5{1'b0}}} == 31'h10200   ) begin
            src_channel = 24'b000010000000000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 17;
    end

    // ( 0x10300 .. 0x10320 )
    if ( {address[RG:PAD6],{PAD6{1'b0}}} == 31'h10300   ) begin
            src_channel = 24'b000000001000000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 10;
    end

    // ( 0x10320 .. 0x10340 )
    if ( {address[RG:PAD7],{PAD7{1'b0}}} == 31'h10320   ) begin
            src_channel = 24'b000000000100000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 9;
    end

    // ( 0x10800 .. 0x11000 )
    if ( {address[RG:PAD8],{PAD8{1'b0}}} == 31'h10800   ) begin
            src_channel = 24'b000000000000000000100;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 0;
    end

    // ( 0x11000 .. 0x11010 )
    if ( {address[RG:PAD9],{PAD9{1'b0}}} == 31'h11000   ) begin
            src_channel = 24'b001000000000000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 18;
    end

    // ( 0x11010 .. 0x11020 )
    if ( {address[RG:PAD10],{PAD10{1'b0}}} == 31'h11010   ) begin
            src_channel = 24'b000000000000010000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 3;
    end

    // ( 0x11140 .. 0x11150 )
    if ( {address[RG:PAD11],{PAD11{1'b0}}} == 31'h11140  && read_transaction  ) begin
            src_channel = 24'b010000000000000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 19;
    end

    // ( 0x20000 .. 0x28000 )
    if ( {address[RG:PAD12],{PAD12{1'b0}}} == 31'h20000   ) begin
            src_channel = 24'b100000000000000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 11;
    end

    // ( 0x30000 .. 0x30800 )
    if ( {address[RG:PAD13],{PAD13{1'b0}}} == 31'h30000   ) begin
            src_channel = 24'b000000000000000010000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 20;
    end

    // ( 0x50000 .. 0x58000 )
    if ( {address[RG:PAD14],{PAD14{1'b0}}} == 31'h50000   ) begin
            src_channel = 24'b000000000000100000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 5;
    end

    // ( 0x800000 .. 0x880000 )
    if ( {address[RG:PAD15],{PAD15{1'b0}}} == 31'h800000   ) begin
            src_channel = 24'b000000000000001000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 21;
    end

    // ( 0x880000 .. 0x890000 )
    if ( {address[RG:PAD16],{PAD16{1'b0}}} == 31'h880000   ) begin
            src_channel = 24'b000000010000000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 22;
    end

    // ( 0x20000000 .. 0x40000000 )
    if ( {address[RG:PAD17],{PAD17{1'b0}}} == 31'h20000000   ) begin
            src_channel = 24'b000000000000000001000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 13;
    end

    // ( 0x40000000 .. 0x41000000 )
    if ( {address[RG:PAD18],{PAD18{1'b0}}} == 31'h40000000   ) begin
            src_channel = 24'b000000000000000000010;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 15;
    end

    // ( 0x50000000 .. 0x50100000 )
    if ( {address[RG:PAD19],{PAD19{1'b0}}} == 31'h50000000   ) begin
            src_channel = 24'b000001000000000000000;
            src_data[PKT_DEST_ID_H:PKT_DEST_ID_L] = 16;
    end
        end // end for if-else for memory aliasing decode

end


    // --------------------------------------------------
    // Ceil(log2()) function
    // --------------------------------------------------
    function integer log2ceil;
        input reg[65:0] val;
        reg [65:0] i;

        begin
            i = 1;
            log2ceil = 0;

            while (i < val) begin
                log2ceil = log2ceil + 1;
                i = i << 1;
            end
        end
    endfunction

endmodule


