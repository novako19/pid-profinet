	component qsys_profinet_system is
		port (
			appl_subsystem_lcd_RS                                      : out   std_logic;                                        -- RS
			appl_subsystem_lcd_RW                                      : out   std_logic;                                        -- RW
			appl_subsystem_lcd_data                                    : inout std_logic_vector(7 downto 0)  := (others => 'X'); -- data
			appl_subsystem_lcd_E                                       : out   std_logic;                                        -- E
			appl_subsystem_pio_button_export                           : in    std_logic_vector(17 downto 0) := (others => 'X'); -- export
			appl_subsystem_pio_key_export                              : in    std_logic_vector(3 downto 0)  := (others => 'X'); -- export
			appl_subsystem_pio_led_green_export                        : out   std_logic_vector(8 downto 0);                     -- export
			appl_subsystem_pio_led_red_export                          : out   std_logic_vector(17 downto 0);                    -- export
			appl_subsystem_seven_segment_export                        : out   std_logic_vector(63 downto 0);                    -- export
			clk_50_a_clk                                               : in    std_logic                     := 'X';             -- clk
			clk_50_areset_reset_n                                      : in    std_logic                     := 'X';             -- reset_n
			clk_50_b_clk                                               : in    std_logic                     := 'X';             -- clk
			clk_50_b_reset_reset_n                                     : in    std_logic                     := 'X';             -- reset_n
			epcs_config_flash_dclk                                     : out   std_logic;                                        -- dclk
			epcs_config_flash_sce                                      : out   std_logic;                                        -- sce
			epcs_config_flash_sdo                                      : out   std_logic;                                        -- sdo
			epcs_config_flash_data0                                    : in    std_logic                     := 'X';             -- data0
			internal_pio_int_from_appl_export                          : in    std_logic                     := 'X';             -- export
			internal_pio_int_from_fieldbus_export                      : in    std_logic_vector(2 downto 0)  := (others => 'X'); -- export
			internal_pio_int_to_appl_export                            : out   std_logic;                                        -- export
			internal_pio_int_to_fieldbus_export                        : out   std_logic;                                        -- export
			internal_pio_reset_cpu_fieldbus_export                     : out   std_logic_vector(1 downto 0);                     -- export
			internal_pio_sync_export                                   : in    std_logic_vector(1 downto 0)  := (others => 'X'); -- export
			internal_switch_clockshift_90                              : in    std_logic                     := 'X';             -- 90
			internal_switch_clockshift_180                             : in    std_logic                     := 'X';             -- 180
			internal_switch_clockshift_270                             : in    std_logic                     := 'X';             -- 270
			pio_performance_indicator_export                           : out   std_logic_vector(31 downto 0);                    -- export
			pll_125mhz_180_clk                                         : out   std_logic;                                        -- clk
			pll_125mhz_270_clk                                         : out   std_logic;                                        -- clk
			pll_125mhz_90_clk                                          : out   std_logic;                                        -- clk
			pll_125mhz_locked_export                                   : out   std_logic;                                        -- export
			pll_sdram_c1_clk                                           : out   std_logic;                                        -- clk
			pll_sdram_locked_export                                    : out   std_logic;                                        -- export
			rte_subsystem_reset                                        : in    std_logic                     := 'X';             -- reset
			rte_subsystem_cpu_reset_resetrequest                       : in    std_logic                     := 'X';             -- resetrequest
			rte_subsystem_cpu_reset_resettaken                         : out   std_logic;                                        -- resettaken
			rte_subsystem_data_dma_running_input                       : out   std_logic;                                        -- input
			rte_subsystem_data_dma_running_output                      : out   std_logic;                                        -- output
			rte_subsystem_dma_notification_in_export                   : in    std_logic_vector(63 downto 0) := (others => 'X'); -- export
			rte_subsystem_dma_notification_out_export                  : out   std_logic_vector(63 downto 0);                    -- export
			rte_subsystem_pio_phy_export                               : in    std_logic_vector(1 downto 0)  := (others => 'X'); -- export
			rte_subsystem_switch_auth_clk                              : out   std_logic;                                        -- clk
			rte_subsystem_switch_auth_dataoutvalid                     : out   std_logic;                                        -- dataoutvalid
			rte_subsystem_switch_auth_dataout                          : out   std_logic;                                        -- dataout
			rte_subsystem_switch_auth_datainvalid                      : in    std_logic                     := 'X';             -- datainvalid
			rte_subsystem_switch_auth_datain                           : in    std_logic                     := 'X';             -- datain
			rte_subsystem_switch_mdio_clock                            : out   std_logic;                                        -- clock
			rte_subsystem_switch_mdio_in                               : in    std_logic                     := 'X';             -- in
			rte_subsystem_switch_mdio_out                              : out   std_logic;                                        -- out
			rte_subsystem_switch_mdio_outenable                        : out   std_logic;                                        -- outenable
			rte_subsystem_switch_mdio_portselect                       : out   std_logic;                                        -- portselect
			rte_subsystem_switch_mii_p0_collision                      : in    std_logic                     := 'X';             -- p0_collision
			rte_subsystem_switch_mii_p0_carrier                        : in    std_logic                     := 'X';             -- p0_carrier
			rte_subsystem_switch_mii_p0_rxclock                        : in    std_logic                     := 'X';             -- p0_rxclock
			rte_subsystem_switch_mii_p0_rxerror                        : in    std_logic                     := 'X';             -- p0_rxerror
			rte_subsystem_switch_mii_p0_rxenable                       : in    std_logic                     := 'X';             -- p0_rxenable
			rte_subsystem_switch_mii_p0_rx_red_enable                  : out   std_logic;                                        -- p0_rx_red_enable
			rte_subsystem_switch_mii_p0_rx                             : in    std_logic_vector(3 downto 0)  := (others => 'X'); -- p0_rx
			rte_subsystem_switch_mii_p0_txclock                        : in    std_logic                     := 'X';             -- p0_txclock
			rte_subsystem_switch_mii_p0_txerror                        : out   std_logic;                                        -- p0_txerror
			rte_subsystem_switch_mii_p0_txenable                       : out   std_logic;                                        -- p0_txenable
			rte_subsystem_switch_mii_p0_tx_red_enable                  : out   std_logic;                                        -- p0_tx_red_enable
			rte_subsystem_switch_mii_p0_tx                             : out   std_logic_vector(3 downto 0);                     -- p0_tx
			rte_subsystem_switch_mii_p1_collision                      : in    std_logic                     := 'X';             -- p1_collision
			rte_subsystem_switch_mii_p1_carrier                        : in    std_logic                     := 'X';             -- p1_carrier
			rte_subsystem_switch_mii_p1_rxclock                        : in    std_logic                     := 'X';             -- p1_rxclock
			rte_subsystem_switch_mii_p1_rxerror                        : in    std_logic                     := 'X';             -- p1_rxerror
			rte_subsystem_switch_mii_p1_rxenable                       : in    std_logic                     := 'X';             -- p1_rxenable
			rte_subsystem_switch_mii_p1_rx_red_enable                  : out   std_logic;                                        -- p1_rx_red_enable
			rte_subsystem_switch_mii_p1_rx                             : in    std_logic_vector(3 downto 0)  := (others => 'X'); -- p1_rx
			rte_subsystem_switch_mii_p1_txclock                        : in    std_logic                     := 'X';             -- p1_txclock
			rte_subsystem_switch_mii_p1_txerror                        : out   std_logic;                                        -- p1_txerror
			rte_subsystem_switch_mii_p1_txenable                       : out   std_logic;                                        -- p1_txenable
			rte_subsystem_switch_mii_p1_tx_red_enable                  : out   std_logic;                                        -- p1_tx_red_enable
			rte_subsystem_switch_mii_p1_tx                             : out   std_logic_vector(3 downto 0);                     -- p1_tx
			rte_subsystem_switch_signal_sync                           : out   std_logic;                                        -- sync
			rte_subsystem_switch_signal_sync_timestamp                 : out   std_logic_vector(31 downto 0);                    -- sync_timestamp
			rte_subsystem_switch_signal_31_25us_base_clk               : out   std_logic;                                        -- 31_25us_base_clk
			rte_subsystem_switch_signal_cycle_counter                  : out   std_logic_vector(15 downto 0);                    -- cycle_counter
			rte_subsystem_switch_signal_cycle_timestamp                : out   std_logic_vector(15 downto 0);                    -- cycle_timestamp
			rte_subsystem_switch_signal_consumer_update                : out   std_logic;                                        -- consumer_update
			rte_subsystem_switch_signal_provider_update                : out   std_logic;                                        -- provider_update
			rte_subsystem_switch_signal_cputx                          : out   std_logic;                                        -- cputx
			rte_subsystem_switch_signal_cpurx                          : out   std_logic;                                        -- cpurx
			sdram_addr                                                 : out   std_logic_vector(12 downto 0);                    -- addr
			sdram_ba                                                   : out   std_logic_vector(1 downto 0);                     -- ba
			sdram_cas_n                                                : out   std_logic;                                        -- cas_n
			sdram_cke                                                  : out   std_logic;                                        -- cke
			sdram_cs_n                                                 : out   std_logic;                                        -- cs_n
			sdram_dq                                                   : inout std_logic_vector(31 downto 0) := (others => 'X'); -- dq
			sdram_dqm                                                  : out   std_logic_vector(3 downto 0);                     -- dqm
			sdram_ras_n                                                : out   std_logic;                                        -- ras_n
			sdram_we_n                                                 : out   std_logic;                                        -- we_n
			test_subsystem_pattern_constant_notification_out_export    : out   std_logic;                                        -- export
			test_subsystem_pattern_counter_notification_out_export     : out   std_logic;                                        -- export
			test_subsystem_pattern_isochronous_isochronous_red_enable  : in    std_logic                     := 'X';             -- red_enable
			test_subsystem_pattern_isochronous_isochronous_data_valid  : out   std_logic;                                        -- data_valid
			test_subsystem_pattern_isochronous_notification_in_export  : in    std_logic                     := 'X';             -- export
			test_subsystem_pattern_isochronous_notification_out_export : out   std_logic;                                        -- export
			test_subsystem_pattern_mirror_notification_in_export       : in    std_logic                     := 'X';             -- export
			test_subsystem_pattern_mirror_notification_out_export      : out   std_logic;                                        -- export
			test_subsystem_pattern_ramp_notification_out_export        : out   std_logic;                                        -- export
			test_subsystem_pattern_sink_notification_in_export         : in    std_logic                     := 'X';             -- export
			test_subsystem_pattern_sinus_notification_out_export       : out   std_logic;                                        -- export
			tristate_bus_address                                       : out   std_logic_vector(20 downto 0);                    -- address
			tristate_bus_sram_tcm_outputenable_n_out                   : out   std_logic_vector(0 downto 0);                     -- sram_tcm_outputenable_n_out
			tristate_bus_data                                          : inout std_logic_vector(15 downto 0) := (others => 'X'); -- data
			tristate_bus_sram_tcm_chipselect_n_out                     : out   std_logic_vector(0 downto 0);                     -- sram_tcm_chipselect_n_out
			tristate_bus_sram_tcm_byteenable_n_out                     : out   std_logic_vector(1 downto 0);                     -- sram_tcm_byteenable_n_out
			tristate_bus_sram_tcm_write_n_out                          : out   std_logic_vector(0 downto 0);                     -- sram_tcm_write_n_out
			appl_subsystem_pio_axis0_reference_export                  : out   std_logic_vector(31 downto 0)                     -- export
		);
	end component qsys_profinet_system;

	u0 : component qsys_profinet_system
		port map (
			appl_subsystem_lcd_RS                                      => CONNECTED_TO_appl_subsystem_lcd_RS,                                      --                                  appl_subsystem_lcd.RS
			appl_subsystem_lcd_RW                                      => CONNECTED_TO_appl_subsystem_lcd_RW,                                      --                                                    .RW
			appl_subsystem_lcd_data                                    => CONNECTED_TO_appl_subsystem_lcd_data,                                    --                                                    .data
			appl_subsystem_lcd_E                                       => CONNECTED_TO_appl_subsystem_lcd_E,                                       --                                                    .E
			appl_subsystem_pio_button_export                           => CONNECTED_TO_appl_subsystem_pio_button_export,                           --                           appl_subsystem_pio_button.export
			appl_subsystem_pio_key_export                              => CONNECTED_TO_appl_subsystem_pio_key_export,                              --                              appl_subsystem_pio_key.export
			appl_subsystem_pio_led_green_export                        => CONNECTED_TO_appl_subsystem_pio_led_green_export,                        --                        appl_subsystem_pio_led_green.export
			appl_subsystem_pio_led_red_export                          => CONNECTED_TO_appl_subsystem_pio_led_red_export,                          --                          appl_subsystem_pio_led_red.export
			appl_subsystem_seven_segment_export                        => CONNECTED_TO_appl_subsystem_seven_segment_export,                        --                        appl_subsystem_seven_segment.export
			clk_50_a_clk                                               => CONNECTED_TO_clk_50_a_clk,                                               --                                            clk_50_a.clk
			clk_50_areset_reset_n                                      => CONNECTED_TO_clk_50_areset_reset_n,                                      --                                       clk_50_areset.reset_n
			clk_50_b_clk                                               => CONNECTED_TO_clk_50_b_clk,                                               --                                            clk_50_b.clk
			clk_50_b_reset_reset_n                                     => CONNECTED_TO_clk_50_b_reset_reset_n,                                     --                                      clk_50_b_reset.reset_n
			epcs_config_flash_dclk                                     => CONNECTED_TO_epcs_config_flash_dclk,                                     --                                   epcs_config_flash.dclk
			epcs_config_flash_sce                                      => CONNECTED_TO_epcs_config_flash_sce,                                      --                                                    .sce
			epcs_config_flash_sdo                                      => CONNECTED_TO_epcs_config_flash_sdo,                                      --                                                    .sdo
			epcs_config_flash_data0                                    => CONNECTED_TO_epcs_config_flash_data0,                                    --                                                    .data0
			internal_pio_int_from_appl_export                          => CONNECTED_TO_internal_pio_int_from_appl_export,                          --                          internal_pio_int_from_appl.export
			internal_pio_int_from_fieldbus_export                      => CONNECTED_TO_internal_pio_int_from_fieldbus_export,                      --                      internal_pio_int_from_fieldbus.export
			internal_pio_int_to_appl_export                            => CONNECTED_TO_internal_pio_int_to_appl_export,                            --                            internal_pio_int_to_appl.export
			internal_pio_int_to_fieldbus_export                        => CONNECTED_TO_internal_pio_int_to_fieldbus_export,                        --                        internal_pio_int_to_fieldbus.export
			internal_pio_reset_cpu_fieldbus_export                     => CONNECTED_TO_internal_pio_reset_cpu_fieldbus_export,                     --                     internal_pio_reset_cpu_fieldbus.export
			internal_pio_sync_export                                   => CONNECTED_TO_internal_pio_sync_export,                                   --                                   internal_pio_sync.export
			internal_switch_clockshift_90                              => CONNECTED_TO_internal_switch_clockshift_90,                              --                          internal_switch_clockshift.90
			internal_switch_clockshift_180                             => CONNECTED_TO_internal_switch_clockshift_180,                             --                                                    .180
			internal_switch_clockshift_270                             => CONNECTED_TO_internal_switch_clockshift_270,                             --                                                    .270
			pio_performance_indicator_export                           => CONNECTED_TO_pio_performance_indicator_export,                           --                           pio_performance_indicator.export
			pll_125mhz_180_clk                                         => CONNECTED_TO_pll_125mhz_180_clk,                                         --                                      pll_125mhz_180.clk
			pll_125mhz_270_clk                                         => CONNECTED_TO_pll_125mhz_270_clk,                                         --                                      pll_125mhz_270.clk
			pll_125mhz_90_clk                                          => CONNECTED_TO_pll_125mhz_90_clk,                                          --                                       pll_125mhz_90.clk
			pll_125mhz_locked_export                                   => CONNECTED_TO_pll_125mhz_locked_export,                                   --                                   pll_125mhz_locked.export
			pll_sdram_c1_clk                                           => CONNECTED_TO_pll_sdram_c1_clk,                                           --                                        pll_sdram_c1.clk
			pll_sdram_locked_export                                    => CONNECTED_TO_pll_sdram_locked_export,                                    --                                    pll_sdram_locked.export
			rte_subsystem_reset                                        => CONNECTED_TO_rte_subsystem_reset,                                        --                                       rte_subsystem.reset
			rte_subsystem_cpu_reset_resetrequest                       => CONNECTED_TO_rte_subsystem_cpu_reset_resetrequest,                       --                             rte_subsystem_cpu_reset.resetrequest
			rte_subsystem_cpu_reset_resettaken                         => CONNECTED_TO_rte_subsystem_cpu_reset_resettaken,                         --                                                    .resettaken
			rte_subsystem_data_dma_running_input                       => CONNECTED_TO_rte_subsystem_data_dma_running_input,                       --                      rte_subsystem_data_dma_running.input
			rte_subsystem_data_dma_running_output                      => CONNECTED_TO_rte_subsystem_data_dma_running_output,                      --                                                    .output
			rte_subsystem_dma_notification_in_export                   => CONNECTED_TO_rte_subsystem_dma_notification_in_export,                   --                   rte_subsystem_dma_notification_in.export
			rte_subsystem_dma_notification_out_export                  => CONNECTED_TO_rte_subsystem_dma_notification_out_export,                  --                  rte_subsystem_dma_notification_out.export
			rte_subsystem_pio_phy_export                               => CONNECTED_TO_rte_subsystem_pio_phy_export,                               --                               rte_subsystem_pio_phy.export
			rte_subsystem_switch_auth_clk                              => CONNECTED_TO_rte_subsystem_switch_auth_clk,                              --                           rte_subsystem_switch_auth.clk
			rte_subsystem_switch_auth_dataoutvalid                     => CONNECTED_TO_rte_subsystem_switch_auth_dataoutvalid,                     --                                                    .dataoutvalid
			rte_subsystem_switch_auth_dataout                          => CONNECTED_TO_rte_subsystem_switch_auth_dataout,                          --                                                    .dataout
			rte_subsystem_switch_auth_datainvalid                      => CONNECTED_TO_rte_subsystem_switch_auth_datainvalid,                      --                                                    .datainvalid
			rte_subsystem_switch_auth_datain                           => CONNECTED_TO_rte_subsystem_switch_auth_datain,                           --                                                    .datain
			rte_subsystem_switch_mdio_clock                            => CONNECTED_TO_rte_subsystem_switch_mdio_clock,                            --                           rte_subsystem_switch_mdio.clock
			rte_subsystem_switch_mdio_in                               => CONNECTED_TO_rte_subsystem_switch_mdio_in,                               --                                                    .in
			rte_subsystem_switch_mdio_out                              => CONNECTED_TO_rte_subsystem_switch_mdio_out,                              --                                                    .out
			rte_subsystem_switch_mdio_outenable                        => CONNECTED_TO_rte_subsystem_switch_mdio_outenable,                        --                                                    .outenable
			rte_subsystem_switch_mdio_portselect                       => CONNECTED_TO_rte_subsystem_switch_mdio_portselect,                       --                                                    .portselect
			rte_subsystem_switch_mii_p0_collision                      => CONNECTED_TO_rte_subsystem_switch_mii_p0_collision,                      --                            rte_subsystem_switch_mii.p0_collision
			rte_subsystem_switch_mii_p0_carrier                        => CONNECTED_TO_rte_subsystem_switch_mii_p0_carrier,                        --                                                    .p0_carrier
			rte_subsystem_switch_mii_p0_rxclock                        => CONNECTED_TO_rte_subsystem_switch_mii_p0_rxclock,                        --                                                    .p0_rxclock
			rte_subsystem_switch_mii_p0_rxerror                        => CONNECTED_TO_rte_subsystem_switch_mii_p0_rxerror,                        --                                                    .p0_rxerror
			rte_subsystem_switch_mii_p0_rxenable                       => CONNECTED_TO_rte_subsystem_switch_mii_p0_rxenable,                       --                                                    .p0_rxenable
			rte_subsystem_switch_mii_p0_rx_red_enable                  => CONNECTED_TO_rte_subsystem_switch_mii_p0_rx_red_enable,                  --                                                    .p0_rx_red_enable
			rte_subsystem_switch_mii_p0_rx                             => CONNECTED_TO_rte_subsystem_switch_mii_p0_rx,                             --                                                    .p0_rx
			rte_subsystem_switch_mii_p0_txclock                        => CONNECTED_TO_rte_subsystem_switch_mii_p0_txclock,                        --                                                    .p0_txclock
			rte_subsystem_switch_mii_p0_txerror                        => CONNECTED_TO_rte_subsystem_switch_mii_p0_txerror,                        --                                                    .p0_txerror
			rte_subsystem_switch_mii_p0_txenable                       => CONNECTED_TO_rte_subsystem_switch_mii_p0_txenable,                       --                                                    .p0_txenable
			rte_subsystem_switch_mii_p0_tx_red_enable                  => CONNECTED_TO_rte_subsystem_switch_mii_p0_tx_red_enable,                  --                                                    .p0_tx_red_enable
			rte_subsystem_switch_mii_p0_tx                             => CONNECTED_TO_rte_subsystem_switch_mii_p0_tx,                             --                                                    .p0_tx
			rte_subsystem_switch_mii_p1_collision                      => CONNECTED_TO_rte_subsystem_switch_mii_p1_collision,                      --                                                    .p1_collision
			rte_subsystem_switch_mii_p1_carrier                        => CONNECTED_TO_rte_subsystem_switch_mii_p1_carrier,                        --                                                    .p1_carrier
			rte_subsystem_switch_mii_p1_rxclock                        => CONNECTED_TO_rte_subsystem_switch_mii_p1_rxclock,                        --                                                    .p1_rxclock
			rte_subsystem_switch_mii_p1_rxerror                        => CONNECTED_TO_rte_subsystem_switch_mii_p1_rxerror,                        --                                                    .p1_rxerror
			rte_subsystem_switch_mii_p1_rxenable                       => CONNECTED_TO_rte_subsystem_switch_mii_p1_rxenable,                       --                                                    .p1_rxenable
			rte_subsystem_switch_mii_p1_rx_red_enable                  => CONNECTED_TO_rte_subsystem_switch_mii_p1_rx_red_enable,                  --                                                    .p1_rx_red_enable
			rte_subsystem_switch_mii_p1_rx                             => CONNECTED_TO_rte_subsystem_switch_mii_p1_rx,                             --                                                    .p1_rx
			rte_subsystem_switch_mii_p1_txclock                        => CONNECTED_TO_rte_subsystem_switch_mii_p1_txclock,                        --                                                    .p1_txclock
			rte_subsystem_switch_mii_p1_txerror                        => CONNECTED_TO_rte_subsystem_switch_mii_p1_txerror,                        --                                                    .p1_txerror
			rte_subsystem_switch_mii_p1_txenable                       => CONNECTED_TO_rte_subsystem_switch_mii_p1_txenable,                       --                                                    .p1_txenable
			rte_subsystem_switch_mii_p1_tx_red_enable                  => CONNECTED_TO_rte_subsystem_switch_mii_p1_tx_red_enable,                  --                                                    .p1_tx_red_enable
			rte_subsystem_switch_mii_p1_tx                             => CONNECTED_TO_rte_subsystem_switch_mii_p1_tx,                             --                                                    .p1_tx
			rte_subsystem_switch_signal_sync                           => CONNECTED_TO_rte_subsystem_switch_signal_sync,                           --                         rte_subsystem_switch_signal.sync
			rte_subsystem_switch_signal_sync_timestamp                 => CONNECTED_TO_rte_subsystem_switch_signal_sync_timestamp,                 --                                                    .sync_timestamp
			rte_subsystem_switch_signal_31_25us_base_clk               => CONNECTED_TO_rte_subsystem_switch_signal_31_25us_base_clk,               --                                                    .31_25us_base_clk
			rte_subsystem_switch_signal_cycle_counter                  => CONNECTED_TO_rte_subsystem_switch_signal_cycle_counter,                  --                                                    .cycle_counter
			rte_subsystem_switch_signal_cycle_timestamp                => CONNECTED_TO_rte_subsystem_switch_signal_cycle_timestamp,                --                                                    .cycle_timestamp
			rte_subsystem_switch_signal_consumer_update                => CONNECTED_TO_rte_subsystem_switch_signal_consumer_update,                --                                                    .consumer_update
			rte_subsystem_switch_signal_provider_update                => CONNECTED_TO_rte_subsystem_switch_signal_provider_update,                --                                                    .provider_update
			rte_subsystem_switch_signal_cputx                          => CONNECTED_TO_rte_subsystem_switch_signal_cputx,                          --                                                    .cputx
			rte_subsystem_switch_signal_cpurx                          => CONNECTED_TO_rte_subsystem_switch_signal_cpurx,                          --                                                    .cpurx
			sdram_addr                                                 => CONNECTED_TO_sdram_addr,                                                 --                                               sdram.addr
			sdram_ba                                                   => CONNECTED_TO_sdram_ba,                                                   --                                                    .ba
			sdram_cas_n                                                => CONNECTED_TO_sdram_cas_n,                                                --                                                    .cas_n
			sdram_cke                                                  => CONNECTED_TO_sdram_cke,                                                  --                                                    .cke
			sdram_cs_n                                                 => CONNECTED_TO_sdram_cs_n,                                                 --                                                    .cs_n
			sdram_dq                                                   => CONNECTED_TO_sdram_dq,                                                   --                                                    .dq
			sdram_dqm                                                  => CONNECTED_TO_sdram_dqm,                                                  --                                                    .dqm
			sdram_ras_n                                                => CONNECTED_TO_sdram_ras_n,                                                --                                                    .ras_n
			sdram_we_n                                                 => CONNECTED_TO_sdram_we_n,                                                 --                                                    .we_n
			test_subsystem_pattern_constant_notification_out_export    => CONNECTED_TO_test_subsystem_pattern_constant_notification_out_export,    --    test_subsystem_pattern_constant_notification_out.export
			test_subsystem_pattern_counter_notification_out_export     => CONNECTED_TO_test_subsystem_pattern_counter_notification_out_export,     --     test_subsystem_pattern_counter_notification_out.export
			test_subsystem_pattern_isochronous_isochronous_red_enable  => CONNECTED_TO_test_subsystem_pattern_isochronous_isochronous_red_enable,  --      test_subsystem_pattern_isochronous_isochronous.red_enable
			test_subsystem_pattern_isochronous_isochronous_data_valid  => CONNECTED_TO_test_subsystem_pattern_isochronous_isochronous_data_valid,  --                                                    .data_valid
			test_subsystem_pattern_isochronous_notification_in_export  => CONNECTED_TO_test_subsystem_pattern_isochronous_notification_in_export,  --  test_subsystem_pattern_isochronous_notification_in.export
			test_subsystem_pattern_isochronous_notification_out_export => CONNECTED_TO_test_subsystem_pattern_isochronous_notification_out_export, -- test_subsystem_pattern_isochronous_notification_out.export
			test_subsystem_pattern_mirror_notification_in_export       => CONNECTED_TO_test_subsystem_pattern_mirror_notification_in_export,       --       test_subsystem_pattern_mirror_notification_in.export
			test_subsystem_pattern_mirror_notification_out_export      => CONNECTED_TO_test_subsystem_pattern_mirror_notification_out_export,      --      test_subsystem_pattern_mirror_notification_out.export
			test_subsystem_pattern_ramp_notification_out_export        => CONNECTED_TO_test_subsystem_pattern_ramp_notification_out_export,        --        test_subsystem_pattern_ramp_notification_out.export
			test_subsystem_pattern_sink_notification_in_export         => CONNECTED_TO_test_subsystem_pattern_sink_notification_in_export,         --         test_subsystem_pattern_sink_notification_in.export
			test_subsystem_pattern_sinus_notification_out_export       => CONNECTED_TO_test_subsystem_pattern_sinus_notification_out_export,       --       test_subsystem_pattern_sinus_notification_out.export
			tristate_bus_address                                       => CONNECTED_TO_tristate_bus_address,                                       --                                        tristate_bus.address
			tristate_bus_sram_tcm_outputenable_n_out                   => CONNECTED_TO_tristate_bus_sram_tcm_outputenable_n_out,                   --                                                    .sram_tcm_outputenable_n_out
			tristate_bus_data                                          => CONNECTED_TO_tristate_bus_data,                                          --                                                    .data
			tristate_bus_sram_tcm_chipselect_n_out                     => CONNECTED_TO_tristate_bus_sram_tcm_chipselect_n_out,                     --                                                    .sram_tcm_chipselect_n_out
			tristate_bus_sram_tcm_byteenable_n_out                     => CONNECTED_TO_tristate_bus_sram_tcm_byteenable_n_out,                     --                                                    .sram_tcm_byteenable_n_out
			tristate_bus_sram_tcm_write_n_out                          => CONNECTED_TO_tristate_bus_sram_tcm_write_n_out,                          --                                                    .sram_tcm_write_n_out
			appl_subsystem_pio_axis0_reference_export                  => CONNECTED_TO_appl_subsystem_pio_axis0_reference_export                   --                  appl_subsystem_pio_axis0_reference.export
		);

