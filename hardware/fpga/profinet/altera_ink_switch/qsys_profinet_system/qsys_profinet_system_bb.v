
module qsys_profinet_system (
	appl_subsystem_lcd_RS,
	appl_subsystem_lcd_RW,
	appl_subsystem_lcd_data,
	appl_subsystem_lcd_E,
	appl_subsystem_pio_button_export,
	appl_subsystem_pio_key_export,
	appl_subsystem_pio_led_green_export,
	appl_subsystem_pio_led_red_export,
	appl_subsystem_seven_segment_export,
	clk_50_a_clk,
	clk_50_areset_reset_n,
	clk_50_b_clk,
	clk_50_b_reset_reset_n,
	epcs_config_flash_dclk,
	epcs_config_flash_sce,
	epcs_config_flash_sdo,
	epcs_config_flash_data0,
	internal_pio_int_from_appl_export,
	internal_pio_int_from_fieldbus_export,
	internal_pio_int_to_appl_export,
	internal_pio_int_to_fieldbus_export,
	internal_pio_reset_cpu_fieldbus_export,
	internal_pio_sync_export,
	internal_switch_clockshift_90,
	internal_switch_clockshift_180,
	internal_switch_clockshift_270,
	pio_performance_indicator_export,
	pll_125mhz_180_clk,
	pll_125mhz_270_clk,
	pll_125mhz_90_clk,
	pll_125mhz_locked_export,
	pll_sdram_c1_clk,
	pll_sdram_locked_export,
	rte_subsystem_reset,
	rte_subsystem_cpu_reset_resetrequest,
	rte_subsystem_cpu_reset_resettaken,
	rte_subsystem_data_dma_running_input,
	rte_subsystem_data_dma_running_output,
	rte_subsystem_dma_notification_in_export,
	rte_subsystem_dma_notification_out_export,
	rte_subsystem_pio_phy_export,
	rte_subsystem_switch_auth_clk,
	rte_subsystem_switch_auth_dataoutvalid,
	rte_subsystem_switch_auth_dataout,
	rte_subsystem_switch_auth_datainvalid,
	rte_subsystem_switch_auth_datain,
	rte_subsystem_switch_mdio_clock,
	rte_subsystem_switch_mdio_in,
	rte_subsystem_switch_mdio_out,
	rte_subsystem_switch_mdio_outenable,
	rte_subsystem_switch_mdio_portselect,
	rte_subsystem_switch_mii_p0_collision,
	rte_subsystem_switch_mii_p0_carrier,
	rte_subsystem_switch_mii_p0_rxclock,
	rte_subsystem_switch_mii_p0_rxerror,
	rte_subsystem_switch_mii_p0_rxenable,
	rte_subsystem_switch_mii_p0_rx_red_enable,
	rte_subsystem_switch_mii_p0_rx,
	rte_subsystem_switch_mii_p0_txclock,
	rte_subsystem_switch_mii_p0_txerror,
	rte_subsystem_switch_mii_p0_txenable,
	rte_subsystem_switch_mii_p0_tx_red_enable,
	rte_subsystem_switch_mii_p0_tx,
	rte_subsystem_switch_mii_p1_collision,
	rte_subsystem_switch_mii_p1_carrier,
	rte_subsystem_switch_mii_p1_rxclock,
	rte_subsystem_switch_mii_p1_rxerror,
	rte_subsystem_switch_mii_p1_rxenable,
	rte_subsystem_switch_mii_p1_rx_red_enable,
	rte_subsystem_switch_mii_p1_rx,
	rte_subsystem_switch_mii_p1_txclock,
	rte_subsystem_switch_mii_p1_txerror,
	rte_subsystem_switch_mii_p1_txenable,
	rte_subsystem_switch_mii_p1_tx_red_enable,
	rte_subsystem_switch_mii_p1_tx,
	rte_subsystem_switch_signal_sync,
	rte_subsystem_switch_signal_sync_timestamp,
	rte_subsystem_switch_signal_31_25us_base_clk,
	rte_subsystem_switch_signal_cycle_counter,
	rte_subsystem_switch_signal_cycle_timestamp,
	rte_subsystem_switch_signal_consumer_update,
	rte_subsystem_switch_signal_provider_update,
	rte_subsystem_switch_signal_cputx,
	rte_subsystem_switch_signal_cpurx,
	sdram_addr,
	sdram_ba,
	sdram_cas_n,
	sdram_cke,
	sdram_cs_n,
	sdram_dq,
	sdram_dqm,
	sdram_ras_n,
	sdram_we_n,
	test_subsystem_pattern_constant_notification_out_export,
	test_subsystem_pattern_counter_notification_out_export,
	test_subsystem_pattern_isochronous_isochronous_red_enable,
	test_subsystem_pattern_isochronous_isochronous_data_valid,
	test_subsystem_pattern_isochronous_notification_in_export,
	test_subsystem_pattern_isochronous_notification_out_export,
	test_subsystem_pattern_mirror_notification_in_export,
	test_subsystem_pattern_mirror_notification_out_export,
	test_subsystem_pattern_ramp_notification_out_export,
	test_subsystem_pattern_sink_notification_in_export,
	test_subsystem_pattern_sinus_notification_out_export,
	tristate_bus_address,
	tristate_bus_sram_tcm_outputenable_n_out,
	tristate_bus_data,
	tristate_bus_sram_tcm_chipselect_n_out,
	tristate_bus_sram_tcm_byteenable_n_out,
	tristate_bus_sram_tcm_write_n_out,
	appl_subsystem_pio_axis0_reference_export);	

	output		appl_subsystem_lcd_RS;
	output		appl_subsystem_lcd_RW;
	inout	[7:0]	appl_subsystem_lcd_data;
	output		appl_subsystem_lcd_E;
	input	[17:0]	appl_subsystem_pio_button_export;
	input	[3:0]	appl_subsystem_pio_key_export;
	output	[8:0]	appl_subsystem_pio_led_green_export;
	output	[17:0]	appl_subsystem_pio_led_red_export;
	output	[63:0]	appl_subsystem_seven_segment_export;
	input		clk_50_a_clk;
	input		clk_50_areset_reset_n;
	input		clk_50_b_clk;
	input		clk_50_b_reset_reset_n;
	output		epcs_config_flash_dclk;
	output		epcs_config_flash_sce;
	output		epcs_config_flash_sdo;
	input		epcs_config_flash_data0;
	input		internal_pio_int_from_appl_export;
	input	[2:0]	internal_pio_int_from_fieldbus_export;
	output		internal_pio_int_to_appl_export;
	output		internal_pio_int_to_fieldbus_export;
	output	[1:0]	internal_pio_reset_cpu_fieldbus_export;
	input	[1:0]	internal_pio_sync_export;
	input		internal_switch_clockshift_90;
	input		internal_switch_clockshift_180;
	input		internal_switch_clockshift_270;
	output	[31:0]	pio_performance_indicator_export;
	output		pll_125mhz_180_clk;
	output		pll_125mhz_270_clk;
	output		pll_125mhz_90_clk;
	output		pll_125mhz_locked_export;
	output		pll_sdram_c1_clk;
	output		pll_sdram_locked_export;
	input		rte_subsystem_reset;
	input		rte_subsystem_cpu_reset_resetrequest;
	output		rte_subsystem_cpu_reset_resettaken;
	output		rte_subsystem_data_dma_running_input;
	output		rte_subsystem_data_dma_running_output;
	input	[63:0]	rte_subsystem_dma_notification_in_export;
	output	[63:0]	rte_subsystem_dma_notification_out_export;
	input	[1:0]	rte_subsystem_pio_phy_export;
	output		rte_subsystem_switch_auth_clk;
	output		rte_subsystem_switch_auth_dataoutvalid;
	output		rte_subsystem_switch_auth_dataout;
	input		rte_subsystem_switch_auth_datainvalid;
	input		rte_subsystem_switch_auth_datain;
	output		rte_subsystem_switch_mdio_clock;
	input		rte_subsystem_switch_mdio_in;
	output		rte_subsystem_switch_mdio_out;
	output		rte_subsystem_switch_mdio_outenable;
	output		rte_subsystem_switch_mdio_portselect;
	input		rte_subsystem_switch_mii_p0_collision;
	input		rte_subsystem_switch_mii_p0_carrier;
	input		rte_subsystem_switch_mii_p0_rxclock;
	input		rte_subsystem_switch_mii_p0_rxerror;
	input		rte_subsystem_switch_mii_p0_rxenable;
	output		rte_subsystem_switch_mii_p0_rx_red_enable;
	input	[3:0]	rte_subsystem_switch_mii_p0_rx;
	input		rte_subsystem_switch_mii_p0_txclock;
	output		rte_subsystem_switch_mii_p0_txerror;
	output		rte_subsystem_switch_mii_p0_txenable;
	output		rte_subsystem_switch_mii_p0_tx_red_enable;
	output	[3:0]	rte_subsystem_switch_mii_p0_tx;
	input		rte_subsystem_switch_mii_p1_collision;
	input		rte_subsystem_switch_mii_p1_carrier;
	input		rte_subsystem_switch_mii_p1_rxclock;
	input		rte_subsystem_switch_mii_p1_rxerror;
	input		rte_subsystem_switch_mii_p1_rxenable;
	output		rte_subsystem_switch_mii_p1_rx_red_enable;
	input	[3:0]	rte_subsystem_switch_mii_p1_rx;
	input		rte_subsystem_switch_mii_p1_txclock;
	output		rte_subsystem_switch_mii_p1_txerror;
	output		rte_subsystem_switch_mii_p1_txenable;
	output		rte_subsystem_switch_mii_p1_tx_red_enable;
	output	[3:0]	rte_subsystem_switch_mii_p1_tx;
	output		rte_subsystem_switch_signal_sync;
	output	[31:0]	rte_subsystem_switch_signal_sync_timestamp;
	output		rte_subsystem_switch_signal_31_25us_base_clk;
	output	[15:0]	rte_subsystem_switch_signal_cycle_counter;
	output	[15:0]	rte_subsystem_switch_signal_cycle_timestamp;
	output		rte_subsystem_switch_signal_consumer_update;
	output		rte_subsystem_switch_signal_provider_update;
	output		rte_subsystem_switch_signal_cputx;
	output		rte_subsystem_switch_signal_cpurx;
	output	[12:0]	sdram_addr;
	output	[1:0]	sdram_ba;
	output		sdram_cas_n;
	output		sdram_cke;
	output		sdram_cs_n;
	inout	[31:0]	sdram_dq;
	output	[3:0]	sdram_dqm;
	output		sdram_ras_n;
	output		sdram_we_n;
	output		test_subsystem_pattern_constant_notification_out_export;
	output		test_subsystem_pattern_counter_notification_out_export;
	input		test_subsystem_pattern_isochronous_isochronous_red_enable;
	output		test_subsystem_pattern_isochronous_isochronous_data_valid;
	input		test_subsystem_pattern_isochronous_notification_in_export;
	output		test_subsystem_pattern_isochronous_notification_out_export;
	input		test_subsystem_pattern_mirror_notification_in_export;
	output		test_subsystem_pattern_mirror_notification_out_export;
	output		test_subsystem_pattern_ramp_notification_out_export;
	input		test_subsystem_pattern_sink_notification_in_export;
	output		test_subsystem_pattern_sinus_notification_out_export;
	output	[20:0]	tristate_bus_address;
	output	[0:0]	tristate_bus_sram_tcm_outputenable_n_out;
	inout	[15:0]	tristate_bus_data;
	output	[0:0]	tristate_bus_sram_tcm_chipselect_n_out;
	output	[1:0]	tristate_bus_sram_tcm_byteenable_n_out;
	output	[0:0]	tristate_bus_sram_tcm_write_n_out;
	output	[31:0]	appl_subsystem_pio_axis0_reference_export;
endmodule
