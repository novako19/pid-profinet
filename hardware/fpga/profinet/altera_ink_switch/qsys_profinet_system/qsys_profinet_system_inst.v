	qsys_profinet_system u0 (
		.appl_subsystem_lcd_RS                                      (<connected-to-appl_subsystem_lcd_RS>),                                      //                                  appl_subsystem_lcd.RS
		.appl_subsystem_lcd_RW                                      (<connected-to-appl_subsystem_lcd_RW>),                                      //                                                    .RW
		.appl_subsystem_lcd_data                                    (<connected-to-appl_subsystem_lcd_data>),                                    //                                                    .data
		.appl_subsystem_lcd_E                                       (<connected-to-appl_subsystem_lcd_E>),                                       //                                                    .E
		.appl_subsystem_pio_button_export                           (<connected-to-appl_subsystem_pio_button_export>),                           //                           appl_subsystem_pio_button.export
		.appl_subsystem_pio_key_export                              (<connected-to-appl_subsystem_pio_key_export>),                              //                              appl_subsystem_pio_key.export
		.appl_subsystem_pio_led_green_export                        (<connected-to-appl_subsystem_pio_led_green_export>),                        //                        appl_subsystem_pio_led_green.export
		.appl_subsystem_pio_led_red_export                          (<connected-to-appl_subsystem_pio_led_red_export>),                          //                          appl_subsystem_pio_led_red.export
		.appl_subsystem_seven_segment_export                        (<connected-to-appl_subsystem_seven_segment_export>),                        //                        appl_subsystem_seven_segment.export
		.clk_50_a_clk                                               (<connected-to-clk_50_a_clk>),                                               //                                            clk_50_a.clk
		.clk_50_areset_reset_n                                      (<connected-to-clk_50_areset_reset_n>),                                      //                                       clk_50_areset.reset_n
		.clk_50_b_clk                                               (<connected-to-clk_50_b_clk>),                                               //                                            clk_50_b.clk
		.clk_50_b_reset_reset_n                                     (<connected-to-clk_50_b_reset_reset_n>),                                     //                                      clk_50_b_reset.reset_n
		.epcs_config_flash_dclk                                     (<connected-to-epcs_config_flash_dclk>),                                     //                                   epcs_config_flash.dclk
		.epcs_config_flash_sce                                      (<connected-to-epcs_config_flash_sce>),                                      //                                                    .sce
		.epcs_config_flash_sdo                                      (<connected-to-epcs_config_flash_sdo>),                                      //                                                    .sdo
		.epcs_config_flash_data0                                    (<connected-to-epcs_config_flash_data0>),                                    //                                                    .data0
		.internal_pio_int_from_appl_export                          (<connected-to-internal_pio_int_from_appl_export>),                          //                          internal_pio_int_from_appl.export
		.internal_pio_int_from_fieldbus_export                      (<connected-to-internal_pio_int_from_fieldbus_export>),                      //                      internal_pio_int_from_fieldbus.export
		.internal_pio_int_to_appl_export                            (<connected-to-internal_pio_int_to_appl_export>),                            //                            internal_pio_int_to_appl.export
		.internal_pio_int_to_fieldbus_export                        (<connected-to-internal_pio_int_to_fieldbus_export>),                        //                        internal_pio_int_to_fieldbus.export
		.internal_pio_reset_cpu_fieldbus_export                     (<connected-to-internal_pio_reset_cpu_fieldbus_export>),                     //                     internal_pio_reset_cpu_fieldbus.export
		.internal_pio_sync_export                                   (<connected-to-internal_pio_sync_export>),                                   //                                   internal_pio_sync.export
		.internal_switch_clockshift_90                              (<connected-to-internal_switch_clockshift_90>),                              //                          internal_switch_clockshift.90
		.internal_switch_clockshift_180                             (<connected-to-internal_switch_clockshift_180>),                             //                                                    .180
		.internal_switch_clockshift_270                             (<connected-to-internal_switch_clockshift_270>),                             //                                                    .270
		.pio_performance_indicator_export                           (<connected-to-pio_performance_indicator_export>),                           //                           pio_performance_indicator.export
		.pll_125mhz_180_clk                                         (<connected-to-pll_125mhz_180_clk>),                                         //                                      pll_125mhz_180.clk
		.pll_125mhz_270_clk                                         (<connected-to-pll_125mhz_270_clk>),                                         //                                      pll_125mhz_270.clk
		.pll_125mhz_90_clk                                          (<connected-to-pll_125mhz_90_clk>),                                          //                                       pll_125mhz_90.clk
		.pll_125mhz_locked_export                                   (<connected-to-pll_125mhz_locked_export>),                                   //                                   pll_125mhz_locked.export
		.pll_sdram_c1_clk                                           (<connected-to-pll_sdram_c1_clk>),                                           //                                        pll_sdram_c1.clk
		.pll_sdram_locked_export                                    (<connected-to-pll_sdram_locked_export>),                                    //                                    pll_sdram_locked.export
		.rte_subsystem_reset                                        (<connected-to-rte_subsystem_reset>),                                        //                                       rte_subsystem.reset
		.rte_subsystem_cpu_reset_resetrequest                       (<connected-to-rte_subsystem_cpu_reset_resetrequest>),                       //                             rte_subsystem_cpu_reset.resetrequest
		.rte_subsystem_cpu_reset_resettaken                         (<connected-to-rte_subsystem_cpu_reset_resettaken>),                         //                                                    .resettaken
		.rte_subsystem_data_dma_running_input                       (<connected-to-rte_subsystem_data_dma_running_input>),                       //                      rte_subsystem_data_dma_running.input
		.rte_subsystem_data_dma_running_output                      (<connected-to-rte_subsystem_data_dma_running_output>),                      //                                                    .output
		.rte_subsystem_dma_notification_in_export                   (<connected-to-rte_subsystem_dma_notification_in_export>),                   //                   rte_subsystem_dma_notification_in.export
		.rte_subsystem_dma_notification_out_export                  (<connected-to-rte_subsystem_dma_notification_out_export>),                  //                  rte_subsystem_dma_notification_out.export
		.rte_subsystem_pio_phy_export                               (<connected-to-rte_subsystem_pio_phy_export>),                               //                               rte_subsystem_pio_phy.export
		.rte_subsystem_switch_auth_clk                              (<connected-to-rte_subsystem_switch_auth_clk>),                              //                           rte_subsystem_switch_auth.clk
		.rte_subsystem_switch_auth_dataoutvalid                     (<connected-to-rte_subsystem_switch_auth_dataoutvalid>),                     //                                                    .dataoutvalid
		.rte_subsystem_switch_auth_dataout                          (<connected-to-rte_subsystem_switch_auth_dataout>),                          //                                                    .dataout
		.rte_subsystem_switch_auth_datainvalid                      (<connected-to-rte_subsystem_switch_auth_datainvalid>),                      //                                                    .datainvalid
		.rte_subsystem_switch_auth_datain                           (<connected-to-rte_subsystem_switch_auth_datain>),                           //                                                    .datain
		.rte_subsystem_switch_mdio_clock                            (<connected-to-rte_subsystem_switch_mdio_clock>),                            //                           rte_subsystem_switch_mdio.clock
		.rte_subsystem_switch_mdio_in                               (<connected-to-rte_subsystem_switch_mdio_in>),                               //                                                    .in
		.rte_subsystem_switch_mdio_out                              (<connected-to-rte_subsystem_switch_mdio_out>),                              //                                                    .out
		.rte_subsystem_switch_mdio_outenable                        (<connected-to-rte_subsystem_switch_mdio_outenable>),                        //                                                    .outenable
		.rte_subsystem_switch_mdio_portselect                       (<connected-to-rte_subsystem_switch_mdio_portselect>),                       //                                                    .portselect
		.rte_subsystem_switch_mii_p0_collision                      (<connected-to-rte_subsystem_switch_mii_p0_collision>),                      //                            rte_subsystem_switch_mii.p0_collision
		.rte_subsystem_switch_mii_p0_carrier                        (<connected-to-rte_subsystem_switch_mii_p0_carrier>),                        //                                                    .p0_carrier
		.rte_subsystem_switch_mii_p0_rxclock                        (<connected-to-rte_subsystem_switch_mii_p0_rxclock>),                        //                                                    .p0_rxclock
		.rte_subsystem_switch_mii_p0_rxerror                        (<connected-to-rte_subsystem_switch_mii_p0_rxerror>),                        //                                                    .p0_rxerror
		.rte_subsystem_switch_mii_p0_rxenable                       (<connected-to-rte_subsystem_switch_mii_p0_rxenable>),                       //                                                    .p0_rxenable
		.rte_subsystem_switch_mii_p0_rx_red_enable                  (<connected-to-rte_subsystem_switch_mii_p0_rx_red_enable>),                  //                                                    .p0_rx_red_enable
		.rte_subsystem_switch_mii_p0_rx                             (<connected-to-rte_subsystem_switch_mii_p0_rx>),                             //                                                    .p0_rx
		.rte_subsystem_switch_mii_p0_txclock                        (<connected-to-rte_subsystem_switch_mii_p0_txclock>),                        //                                                    .p0_txclock
		.rte_subsystem_switch_mii_p0_txerror                        (<connected-to-rte_subsystem_switch_mii_p0_txerror>),                        //                                                    .p0_txerror
		.rte_subsystem_switch_mii_p0_txenable                       (<connected-to-rte_subsystem_switch_mii_p0_txenable>),                       //                                                    .p0_txenable
		.rte_subsystem_switch_mii_p0_tx_red_enable                  (<connected-to-rte_subsystem_switch_mii_p0_tx_red_enable>),                  //                                                    .p0_tx_red_enable
		.rte_subsystem_switch_mii_p0_tx                             (<connected-to-rte_subsystem_switch_mii_p0_tx>),                             //                                                    .p0_tx
		.rte_subsystem_switch_mii_p1_collision                      (<connected-to-rte_subsystem_switch_mii_p1_collision>),                      //                                                    .p1_collision
		.rte_subsystem_switch_mii_p1_carrier                        (<connected-to-rte_subsystem_switch_mii_p1_carrier>),                        //                                                    .p1_carrier
		.rte_subsystem_switch_mii_p1_rxclock                        (<connected-to-rte_subsystem_switch_mii_p1_rxclock>),                        //                                                    .p1_rxclock
		.rte_subsystem_switch_mii_p1_rxerror                        (<connected-to-rte_subsystem_switch_mii_p1_rxerror>),                        //                                                    .p1_rxerror
		.rte_subsystem_switch_mii_p1_rxenable                       (<connected-to-rte_subsystem_switch_mii_p1_rxenable>),                       //                                                    .p1_rxenable
		.rte_subsystem_switch_mii_p1_rx_red_enable                  (<connected-to-rte_subsystem_switch_mii_p1_rx_red_enable>),                  //                                                    .p1_rx_red_enable
		.rte_subsystem_switch_mii_p1_rx                             (<connected-to-rte_subsystem_switch_mii_p1_rx>),                             //                                                    .p1_rx
		.rte_subsystem_switch_mii_p1_txclock                        (<connected-to-rte_subsystem_switch_mii_p1_txclock>),                        //                                                    .p1_txclock
		.rte_subsystem_switch_mii_p1_txerror                        (<connected-to-rte_subsystem_switch_mii_p1_txerror>),                        //                                                    .p1_txerror
		.rte_subsystem_switch_mii_p1_txenable                       (<connected-to-rte_subsystem_switch_mii_p1_txenable>),                       //                                                    .p1_txenable
		.rte_subsystem_switch_mii_p1_tx_red_enable                  (<connected-to-rte_subsystem_switch_mii_p1_tx_red_enable>),                  //                                                    .p1_tx_red_enable
		.rte_subsystem_switch_mii_p1_tx                             (<connected-to-rte_subsystem_switch_mii_p1_tx>),                             //                                                    .p1_tx
		.rte_subsystem_switch_signal_sync                           (<connected-to-rte_subsystem_switch_signal_sync>),                           //                         rte_subsystem_switch_signal.sync
		.rte_subsystem_switch_signal_sync_timestamp                 (<connected-to-rte_subsystem_switch_signal_sync_timestamp>),                 //                                                    .sync_timestamp
		.rte_subsystem_switch_signal_31_25us_base_clk               (<connected-to-rte_subsystem_switch_signal_31_25us_base_clk>),               //                                                    .31_25us_base_clk
		.rte_subsystem_switch_signal_cycle_counter                  (<connected-to-rte_subsystem_switch_signal_cycle_counter>),                  //                                                    .cycle_counter
		.rte_subsystem_switch_signal_cycle_timestamp                (<connected-to-rte_subsystem_switch_signal_cycle_timestamp>),                //                                                    .cycle_timestamp
		.rte_subsystem_switch_signal_consumer_update                (<connected-to-rte_subsystem_switch_signal_consumer_update>),                //                                                    .consumer_update
		.rte_subsystem_switch_signal_provider_update                (<connected-to-rte_subsystem_switch_signal_provider_update>),                //                                                    .provider_update
		.rte_subsystem_switch_signal_cputx                          (<connected-to-rte_subsystem_switch_signal_cputx>),                          //                                                    .cputx
		.rte_subsystem_switch_signal_cpurx                          (<connected-to-rte_subsystem_switch_signal_cpurx>),                          //                                                    .cpurx
		.sdram_addr                                                 (<connected-to-sdram_addr>),                                                 //                                               sdram.addr
		.sdram_ba                                                   (<connected-to-sdram_ba>),                                                   //                                                    .ba
		.sdram_cas_n                                                (<connected-to-sdram_cas_n>),                                                //                                                    .cas_n
		.sdram_cke                                                  (<connected-to-sdram_cke>),                                                  //                                                    .cke
		.sdram_cs_n                                                 (<connected-to-sdram_cs_n>),                                                 //                                                    .cs_n
		.sdram_dq                                                   (<connected-to-sdram_dq>),                                                   //                                                    .dq
		.sdram_dqm                                                  (<connected-to-sdram_dqm>),                                                  //                                                    .dqm
		.sdram_ras_n                                                (<connected-to-sdram_ras_n>),                                                //                                                    .ras_n
		.sdram_we_n                                                 (<connected-to-sdram_we_n>),                                                 //                                                    .we_n
		.test_subsystem_pattern_constant_notification_out_export    (<connected-to-test_subsystem_pattern_constant_notification_out_export>),    //    test_subsystem_pattern_constant_notification_out.export
		.test_subsystem_pattern_counter_notification_out_export     (<connected-to-test_subsystem_pattern_counter_notification_out_export>),     //     test_subsystem_pattern_counter_notification_out.export
		.test_subsystem_pattern_isochronous_isochronous_red_enable  (<connected-to-test_subsystem_pattern_isochronous_isochronous_red_enable>),  //      test_subsystem_pattern_isochronous_isochronous.red_enable
		.test_subsystem_pattern_isochronous_isochronous_data_valid  (<connected-to-test_subsystem_pattern_isochronous_isochronous_data_valid>),  //                                                    .data_valid
		.test_subsystem_pattern_isochronous_notification_in_export  (<connected-to-test_subsystem_pattern_isochronous_notification_in_export>),  //  test_subsystem_pattern_isochronous_notification_in.export
		.test_subsystem_pattern_isochronous_notification_out_export (<connected-to-test_subsystem_pattern_isochronous_notification_out_export>), // test_subsystem_pattern_isochronous_notification_out.export
		.test_subsystem_pattern_mirror_notification_in_export       (<connected-to-test_subsystem_pattern_mirror_notification_in_export>),       //       test_subsystem_pattern_mirror_notification_in.export
		.test_subsystem_pattern_mirror_notification_out_export      (<connected-to-test_subsystem_pattern_mirror_notification_out_export>),      //      test_subsystem_pattern_mirror_notification_out.export
		.test_subsystem_pattern_ramp_notification_out_export        (<connected-to-test_subsystem_pattern_ramp_notification_out_export>),        //        test_subsystem_pattern_ramp_notification_out.export
		.test_subsystem_pattern_sink_notification_in_export         (<connected-to-test_subsystem_pattern_sink_notification_in_export>),         //         test_subsystem_pattern_sink_notification_in.export
		.test_subsystem_pattern_sinus_notification_out_export       (<connected-to-test_subsystem_pattern_sinus_notification_out_export>),       //       test_subsystem_pattern_sinus_notification_out.export
		.tristate_bus_address                                       (<connected-to-tristate_bus_address>),                                       //                                        tristate_bus.address
		.tristate_bus_sram_tcm_outputenable_n_out                   (<connected-to-tristate_bus_sram_tcm_outputenable_n_out>),                   //                                                    .sram_tcm_outputenable_n_out
		.tristate_bus_data                                          (<connected-to-tristate_bus_data>),                                          //                                                    .data
		.tristate_bus_sram_tcm_chipselect_n_out                     (<connected-to-tristate_bus_sram_tcm_chipselect_n_out>),                     //                                                    .sram_tcm_chipselect_n_out
		.tristate_bus_sram_tcm_byteenable_n_out                     (<connected-to-tristate_bus_sram_tcm_byteenable_n_out>),                     //                                                    .sram_tcm_byteenable_n_out
		.tristate_bus_sram_tcm_write_n_out                          (<connected-to-tristate_bus_sram_tcm_write_n_out>),                          //                                                    .sram_tcm_write_n_out
		.appl_subsystem_pio_axis0_reference_export                  (<connected-to-appl_subsystem_pio_axis0_reference_export>)                   //                  appl_subsystem_pio_axis0_reference.export
	);

