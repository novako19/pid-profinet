-- -------------------------------------------
-- Title:			PID regulator
-- Description:		PID regulator with anti-windup and variable  
--					parameters
-- Author:			Novak, Ondrej <novako19@fel.cvut.cz>
-- Date:			3/2016
-- -------------------------------------------

library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

library ieee_proposed;
use ieee_proposed.fixed_pkg.all;

entity RegulatorPID is
	
	generic(
		VAL_BITS			: positive		:= 21; -- size (in bits) of regulator constants 
		CALC_BITS			: positive		:= 32; -- size (in bits) of signals (reference, output,  ...)
		DECIMAL_POINT_BITS	: positive		:= 7; -- decimal point precision (values are shifted by DECIMAL_POINT_BITS to the left to increase precision)
		PWM_DC_MAX_INTEGER	: positive		:= 1000 -- max. duty cycle of PWM
	);
	
	port(
		REGULATOR_CLOCK	:	in std_logic;
		ACLR			:	in std_logic;
		
		REFERENCE	:	in signed(CALC_BITS - 1 downto 0); -- input reference (shifted by DECIMAL_POINT_BITS to the left)
		STATE_MEAS	:	in signed(CALC_BITS - 1 downto 0); -- actual state (speed or position, shifted by DECIMAL_POINT_BITS to the left)
		
		REGULATOR_UPDATE	: in std_logic;
		
		REGULATOR_KP		:	in signed(VAL_BITS -1 downto 0); -- Proportional regulator constant
		REGULATOR_KI		:	in signed(VAL_BITS -1 downto 0); -- Integral regulator constant
		REGULATOR_KD		:	in signed(VAL_BITS -1 downto 0); -- Differential regulator constant
		
		ACTION		:	out signed(CALC_BITS - 1 downto 0) -- output of the regulator (0 - PWM_DC_MAX)
	);
	
end entity;


architecture behav of RegulatorPID is
	
	constant VAL_BOUND_UPPER	: integer := VAL_BITS - DECIMAL_POINT_BITS - 1;
	constant VAL_BOUND_LOWER	: integer := -DECIMAL_POINT_BITS;
	
	constant CALC_BOUND_UPPER	: integer := CALC_BITS - DECIMAL_POINT_BITS - 1;
	constant CALC_BOUND_LOWER	: integer := -DECIMAL_POINT_BITS;
	
	signal REGULATOR_KP_in		:	sfixed(VAL_BOUND_UPPER downto VAL_BOUND_LOWER) := to_sfixed(50, VAL_BOUND_UPPER, VAL_BOUND_LOWER); -- Proportional regulator constant
	signal REGULATOR_KI_in		:	sfixed(VAL_BOUND_UPPER downto VAL_BOUND_LOWER) := to_sfixed(10, VAL_BOUND_UPPER, VAL_BOUND_LOWER);		-- Integral regulator constant
	signal REGULATOR_KD_in		:	sfixed(VAL_BOUND_UPPER downto VAL_BOUND_LOWER) := to_sfixed(0, VAL_BOUND_UPPER, VAL_BOUND_LOWER);		-- Differential regulator constant
	 
	signal REFERENCE_in		:	sfixed(CALC_BOUND_UPPER downto CALC_BOUND_LOWER) := to_sfixed(0, CALC_BOUND_UPPER, CALC_BOUND_LOWER);		-- Set motor reference (inner signal)
	signal ACTUAL_STATE_in	:	sfixed(CALC_BOUND_UPPER downto CALC_BOUND_LOWER) := to_sfixed(0, CALC_BOUND_UPPER, CALC_BOUND_LOWER);		-- Actual state (speed or position) of motor (inner signal)
	
	constant PWM_DC_MAX		: integer := (PWM_DC_MAX_INTEGER - 1)* (2**DECIMAL_POINT_BITS); -- max. duty cycle of PWM
	constant PWM_DC_MIN		: integer := - PWM_DC_MAX; -- negative max. duty cycle of PWM
	
begin
	
	-- input signals to inner signals + synchronization and type conversion 
	synchronize: process(REGULATOR_CLOCK) is
	begin
		
		if rising_edge(REGULATOR_CLOCK) then
		
			REFERENCE_in	<= to_sfixed(REFERENCE, CALC_BOUND_UPPER, CALC_BOUND_LOWER);
			ACTUAL_STATE_in <= to_sfixed(STATE_MEAS, CALC_BOUND_UPPER, CALC_BOUND_LOWER);
			
		end if;
		
	end process;

	-- save changed constants
	constants: process(REGULATOR_UPDATE) is
	-- note: here you can set default values...
	variable REGULATOR_KP_tmp		:	sfixed(VAL_BOUND_UPPER downto VAL_BOUND_LOWER) := to_sfixed(50, VAL_BOUND_UPPER, VAL_BOUND_LOWER);
	variable REGULATOR_KI_tmp		:	sfixed(VAL_BOUND_UPPER downto VAL_BOUND_LOWER) := to_sfixed(10, VAL_BOUND_UPPER, VAL_BOUND_LOWER);
	variable REGULATOR_KD_tmp		:	sfixed(VAL_BOUND_UPPER downto VAL_BOUND_LOWER) := to_sfixed(0, VAL_BOUND_UPPER, VAL_BOUND_LOWER);
	
	begin
		
		if rising_edge(REGULATOR_UPDATE) then
			
			REGULATOR_KP_tmp := to_sfixed(REGULATOR_KP, VAL_BOUND_UPPER, VAL_BOUND_LOWER);
			REGULATOR_KI_tmp := to_sfixed(REGULATOR_KI, VAL_BOUND_UPPER, VAL_BOUND_LOWER);
			REGULATOR_KD_tmp := to_sfixed(REGULATOR_KD, VAL_BOUND_UPPER, VAL_BOUND_LOWER);
			
		end if;
		
		if rising_edge(REGULATOR_CLOCK) then
		
			REGULATOR_KP_in <= REGULATOR_KP_tmp;
			REGULATOR_KI_in <= REGULATOR_KI_tmp;
			REGULATOR_KD_in <= REGULATOR_KD_tmp;
			
		end if;
		
	end process;

	----
	--	REGULATOR
	----
		
	regulator: process(ACLR, REGULATOR_CLOCK) is
		
		constant BOUND_UPPER	: integer := CALC_BOUND_UPPER; -- bound used internally for calculations
		constant BOUND_LOWER	: integer := CALC_BOUND_LOWER; -- bound used internally for calculations
		
		variable integral		: sfixed(BOUND_UPPER downto BOUND_LOWER) := to_sfixed(0, BOUND_UPPER, BOUND_LOWER);
		variable error			: sfixed(BOUND_UPPER downto BOUND_LOWER) := to_sfixed(0, BOUND_UPPER, BOUND_LOWER);
		variable error_last		: sfixed(BOUND_UPPER downto BOUND_LOWER) := to_sfixed(0, BOUND_UPPER, BOUND_LOWER);
		variable action_bound	: sfixed(BOUND_UPPER downto BOUND_LOWER) := to_sfixed(0, BOUND_UPPER, BOUND_LOWER);
		variable action_required: sfixed(BOUND_UPPER downto BOUND_LOWER) := to_sfixed(0, BOUND_UPPER, BOUND_LOWER);
		
		variable ref_last		: sfixed(BOUND_UPPER downto BOUND_LOWER) := to_sfixed(0, BOUND_UPPER, BOUND_LOWER); -- reference value at previous cycle
		
	begin
		
		if ACLR = '1' then
			
			integral := to_sfixed(0, BOUND_UPPER, BOUND_LOWER);
			error := to_sfixed(0, BOUND_UPPER, BOUND_LOWER);
			error_last := to_sfixed(0, BOUND_UPPER, BOUND_LOWER);
			action_bound := to_sfixed(0, BOUND_UPPER, BOUND_LOWER);
			action_required := to_sfixed(0, BOUND_UPPER, BOUND_LOWER);
			
		elsif rising_edge(REGULATOR_CLOCK) then
			
			---
			-- regulation
			---
			
			-- reset integrator on reference sign change
			if REFERENCE_in(REFERENCE_in'high) /= ref_last(ref_last'high) then
				
				integral := to_sfixed(0, BOUND_UPPER, BOUND_LOWER);
				
			end if;
			
			ref_last := resize(REFERENCE_in, BOUND_UPPER, BOUND_LOWER);
			
			error := resize(ACTUAL_STATE_in - REFERENCE_in, BOUND_UPPER, BOUND_LOWER);
			if REGULATOR_KI_in = 0 then
--				
				integral := to_sfixed(0, BOUND_UPPER, BOUND_LOWER);
--				
			else
							
				integral := resize(integral + error * REGULATOR_KI_in, BOUND_UPPER, BOUND_LOWER);
				
			end if;

			action_required := resize(REGULATOR_KP_in * error + integral + (error - error_last) * REGULATOR_KD_in, BOUND_UPPER, BOUND_LOWER);
			
			---
			-- anti-windup
			---
			if action_required > PWM_DC_MAX then
				
				action_bound := to_sfixed(PWM_DC_MAX, BOUND_UPPER, BOUND_LOWER);
				
			elsif action_required < PWM_DC_MIN then
				
				action_bound := to_sfixed(PWM_DC_MIN, BOUND_UPPER, BOUND_LOWER);
				
			else
				
				action_bound := action_required;
				
			end if;
			
			integral := resize(integral - (action_required - action_bound) * REGULATOR_KI_in, BOUND_UPPER, BOUND_LOWER);
			error_last := error;
			
			---
			-- output action
			---	
			ACTION <= to_signed( to_integer(action_bound) / (2 ** DECIMAL_POINT_BITS), ACTION'length);
			
		end if; -- if ACLR = '1' then
		
	end process;
	
end architecture;