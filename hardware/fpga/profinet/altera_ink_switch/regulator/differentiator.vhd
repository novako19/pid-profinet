-- -------------------------------------------
-- Title:			Differentiator
-- Description:		Calculates number of IRC_EVENT pulses per one CLK period. 
-- 					If input signal is a position change event, output
--					value is in steps/period unit, that can be converted
--					to some reasonable unit (e.g. rad/s) 
-- 					Entity also keeps the position of the rotor (in IRC steps) 
--					but only from 0 to IRC_MAX!
-- Author:			Novak, Ondrej <novako19@fel.cvut.cz>
-- Date:			3/2016
-- -------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Differentiator is
	generic(
		BUS_SIZE: natural := 32; -- size of input and output bus
		IRC_MAX : natural := 1999 -- maximum of irc counts
	);
	port(
		SYSCLK	: in std_logic; -- system clock
		CLK		: in std_logic; -- differentiator counts IRC_EVENTS for every period of this clock 
		ACLR	: in std_logic;
		
		IRC_EVENT: in std_logic; -- log. 1 when IRC change occurs
		IRC_DIR	: in std_logic; -- specifies rotation direction read by IRC 
		
		SPEED	: out signed(BUS_SIZE -1 downto 0); -- speed
		POSITION: out unsigned(BUS_SIZE -1 downto 0) -- position
	);
end entity;

architecture behav of Differentiator is
	
	signal CNT: signed(BUS_SIZE -1 downto 0); 
	signal CLK_re, CLK_last: std_logic;
	signal IRC_EVENT_re, IRC_EVENT_last : std_logic;
	
begin

	-- detects rising edge of CLK signal
	edetector: process(SYSCLK) is
	begin
		
		if rising_edge(SYSCLK) then
			
			-- detect CLK rising edge
			CLK_re <= (CLK xor CLK_last) and CLK;
			CLK_last <= CLK;
			
			-- detect IRC event rising edge
			IRC_EVENT_re <= (IRC_EVENT xor IRC_EVENT_last) and IRC_EVENT;
			IRC_EVENT_last <= IRC_EVENT;
			
		end if;
		
	end process;

	diff: process(ACLR, SYSCLK) is
	begin
		
		if ACLR = '1' then
			
			CNT <= (others => '0');
			SPEED <= (others => '0');
			
		elsif rising_edge(SYSCLK) then
		
			if CLK_re = '1' then
				
				SPEED <= CNT;
				CNT <= (others => '0');
				
			end if; -- CLK_re = '1'
		
			if IRC_EVENT_re = '1' then
				
				if IRC_DIR = '1' then
					
					CNT <= CNT + 1;
				
				else
					
					CNT <= CNT - 1;
					
				end if;
				
			end if; -- IRC_EVENT_re = '1'
		
		end if;
		
	end process;
	
	pos: process(ACLR, SYSCLK) is
		
		variable position_var : signed(BUS_SIZE -1 downto 0);
		
	begin
		
		if ACLR = '1' then
			
			position_var := (others => '0');
			
		elsif rising_edge(SYSCLK) then
			
			if IRC_EVENT_re = '1' then
				
				if IRC_DIR = '1' then
					
					position_var := position_var + 1;
					
				else
					
					position_var := position_var - 1; 
					
				end if; -- IRC_DIR = '1'
				
				if position_var < 0 then
					
					position_var := position_var + IRC_MAX;
				
				end if; -- position_var < 0
				
				if position_var > IRC_MAX then
					
					position_var := position_var - IRC_MAX;
					
				end if; -- position_var > IRC_MAX
				
			end if; -- IRC_EVENT_re = '1'
			
			POSITION <= unsigned(position_var);
			
		end if; -- ACLR = '1'
	end process;
	
	
end architecture;