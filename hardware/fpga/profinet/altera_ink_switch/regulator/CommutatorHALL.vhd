-- -------------------------------------------
-- Title:			Hall Commutator
-- Description:		Hall sensor based commutator for BLDC
-- Author:			Novak, Ondrej <novako19@fel.cvut.cz>
-- Date:			3/2016
-- -------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
library regulator;
use regulator.all;

entity CommutatorHALL is

	port(
		DIR											: in std_logic; -- requested direction, '1' for counter-clockwise direction 
		HALL_A, HALL_B, HALL_C						: in std_logic; -- status of Hall sensors
		
		CHAN_A_ENA, CHAN_B_ENA, CHAN_C_ENA			: out std_logic -- enable signals for motor phases
	);

end entity;


architecture behav of CommutatorHALL is
	
	signal HALL			: std_logic_vector(3 downto 0);
	signal CHAN_ENA		: std_logic_vector(2 downto 0);
	
begin

	HALL <= DIR & HALL_A & HALL_B & HALL_C;
	CHAN_A_ENA <= CHAN_ENA(2);
	CHAN_B_ENA <= CHAN_ENA(1);
	CHAN_C_ENA <= CHAN_ENA(0);
	

	with HALL select CHAN_ENA <=
	-- CHAN A B C,  DIR HALLA HALLB HALLC-
	-- BEWARE: reverse Hall ordering than in most
	-- online documents
			"001" when "0001",
			"100" when "0010",
			"100" when "0011",
			"010" when "0100",
			"001" when "0101",
			"010" when "0110",
			
			"010" when "1001",
			"001" when "1010",
			"010" when "1011",
			"100" when "1100",
			"100" when "1101",
			"001" when "1110",
			"000" when others;

end architecture;