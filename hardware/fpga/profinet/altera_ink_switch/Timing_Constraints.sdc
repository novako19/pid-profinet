#*****************************************************************************
#
# SOFTING Industrial Automation GmbH
# Richard-Reitzner-Allee 6
# D-85540 Haar
# Phone: ++49-89-4 56 56-0
# Fax: ++49-89-4 56 56-3 99
# http://www.softing.com
#
# Copyright (C) SOFTING Industrial Automation GmbH 2011-2013. All Rights Reserved.
#
# Version: 1.00.00
#
#*****************************************************************************

#######################################################################################################################
# Define Clocks
#######################################################################################################################

# Input clock pins
create_clock -period 20 -name {PortInClk50A} -waveform {0 10} [get_ports {PortInClk50A}]
create_clock -period 20 -name {PortInClk50B} -waveform {0 10} [get_ports {PortInClk50B}]
create_clock -period 20 -name {PortInClk50C} -waveform {0 10} [get_ports {PortInClk50C}]

create_clock -name {PortInEth0RxClk} -period 40.000 -waveform { 0.000 20.000 } [get_ports {PortInEth0RxClk}]
create_clock -name {PortInEth0TxClk} -period 40.000 -waveform { 0.000 20.000 } [get_ports {PortInEth0TxClk}]
create_clock -name {PortInEth1RxClk} -period 40.000 -waveform { 0.000 20.000 } [get_ports {PortInEth1RxClk}]
create_clock -name {PortInEth1TxClk} -period 40.000 -waveform { 0.000 20.000 } [get_ports {PortInEth1TxClk}]

# Derive PLL generated clocks
derive_pll_clocks
derive_clock_uncertainty

#**************************************************************
# Set Input Delay
#**************************************************************
#
# Ext. PHY clock to out time is 10..30ns
#
set_input_delay -clock PortInEth0RxClk -min 10 [get_ports {PortInEth0RxError PortInEth0RxDv PortInEth0RxData[*]}]
set_input_delay -clock PortInEth0RxClk -max 30 [get_ports {PortInEth0RxError PortInEth0RxDv PortInEth0RxData[*]}] -add_delay
set_input_delay -clock PortInEth1RxClk -min 10 [get_ports {PortInEth1RxError PortInEth1RxDv PortInEth1RxData[*]}]
set_input_delay -clock PortInEth1RxClk -max 30 [get_ports {PortInEth1RxError PortInEth1RxDv PortInEth1RxData[*]}] -add_delay

#**************************************************************
# Set Output Delay
#**************************************************************
#
# Ext. PHY Hold-Time  is 0ns
# Ext. PHY Setup-Time is 10ns
#
set_output_delay -clock PortInEth0TxClk -min 0  [get_ports {PortOutEth0TxError PortOutEth0TxEnable PortOutEth0TxData[*]}]
set_output_delay -clock PortInEth0TxClk -max 10 [get_ports {PortOutEth0TxError PortOutEth0TxEnable PortOutEth0TxData[*]}]
set_output_delay -clock PortInEth1TxClk -min 0  [get_ports {PortOutEth1TxError PortOutEth1TxEnable PortOutEth1TxData[*]}]
set_output_delay -clock PortInEth1TxClk -max 10 [get_ports {PortOutEth1TxError PortOutEth1TxEnable PortOutEth1TxData[*]}]

#set_output_delay -clock [get_clocks {cpu_system_inst|pll|sd1|pll7|clk[0]}] -min 0  [get_ports {PortOutEth0TxError PortOutEth0TxEnable PortOutEth0TxData[*]}]
#set_output_delay -clock [get_clocks {cpu_system_inst|pll|sd1|pll7|clk[0]}] -max 10 [get_ports {PortOutEth0TxError PortOutEth0TxEnable PortOutEth0TxData[*]}]
#set_output_delay -clock [get_clocks {cpu_system_inst|pll|sd1|pll7|clk[0]}] -min 0  [get_ports {PortOutEth1TxError PortOutEth1TxEnable PortOutEth1TxData[*]}]
#set_output_delay -clock [get_clocks {cpu_system_inst|pll|sd1|pll7|clk[0]}] -max 10 [get_ports {PortOutEth1TxError PortOutEth1TxEnable PortOutEth1TxData[*]}]

set_multicycle_path -start -to [get_ports {PortOutEth0TxError PortOutEth0TxEnable PortOutEth0TxData[*]}] -setup 4
set_multicycle_path -start -to [get_ports {PortOutEth0TxError PortOutEth0TxEnable PortOutEth0TxData[*]}] -hold  3

set_multicycle_path -start -to [get_ports {PortOutEth1TxError PortOutEth1TxEnable PortOutEth1TxData[*]}] -setup 4
set_multicycle_path -start -to [get_ports {PortOutEth1TxError PortOutEth1TxEnable PortOutEth1TxData[*]}] -hold  3

#**************************************************************
# Constraints for the SDRAM controller
#**************************************************************

# setup the timing values for the external sdram
set SdramSetupTime 1.5
set SdramHoldTime 0.8
set SdramTcoMax 5.4
set SdramTcoMin 3.0

set SdramClkInternal cpu_system_inst|pll_sdram|sd1|pll7|clk[0]
set SdramClkReference cpu_system_inst|pll_sdram|sd1|pll7|clk[1]
set SdramReferencePin PortOutDRAMClk

set_input_delay -clock $SdramClkReference -reference_pin $SdramReferencePin -min $SdramTcoMin [get_ports PortBidirDRAMDq[*]]
set_input_delay -clock $SdramClkReference -reference_pin $SdramReferencePin -max $SdramTcoMax [get_ports PortBidirDRAMDq[*]]
set_output_delay -clock $SdramClkReference -reference_pin $SdramReferencePin -min -$SdramHoldTime [get_ports PortBidirDRAMDq[*]]
set_output_delay -clock $SdramClkReference -reference_pin $SdramReferencePin -max $SdramSetupTime [get_ports PortBidirDRAMDq[*]]
set_output_delay -clock $SdramClkReference -reference_pin $SdramReferencePin -min -$SdramHoldTime [get_ports PortOutDRAMDqm[*]]
set_output_delay -clock $SdramClkReference -reference_pin $SdramReferencePin -max $SdramSetupTime [get_ports PortOutDRAMDqm[*]]
set_output_delay -clock $SdramClkReference -reference_pin $SdramReferencePin -min -$SdramHoldTime [get_ports PortOutDRAMAddr[*]]
set_output_delay -clock $SdramClkReference -reference_pin $SdramReferencePin -max $SdramSetupTime [get_ports PortOutDRAMAddr[*]]
set_output_delay -clock $SdramClkReference -reference_pin $SdramReferencePin -min -$SdramHoldTime [get_ports {PortOutDRAMCasN PortOutDRAMCke PortOutDRAMCsN PortOutDRAMRasN PortOutDRAMWeN PortOutDRAMBa[*]}]
set_output_delay -clock $SdramClkReference -reference_pin $SdramReferencePin -max $SdramSetupTime [get_ports {PortOutDRAMCasN PortOutDRAMCke PortOutDRAMCsN PortOutDRAMRasN PortOutDRAMWeN PortOutDRAMBa[*]}]

set_multicycle_path -setup -end -from $SdramClkReference -to $SdramClkInternal 2
set_multicycle_path -hold -end -from $SdramClkReference -to $SdramClkInternal 0

#**************************************************************
# ssram/flash interface
#**************************************************************

set_output_delay  -clock [ get_clocks {cpu_system_inst|pll_sdram|sd1|pll7|clk[0]} ] 2   [get_ports {PortOutSRAMAddr[*]} ]
set_input_delay   -clock [ get_clocks {cpu_system_inst|pll_sdram|sd1|pll7|clk[0]} ] 2   [get_ports {PortBidirSRAMDq[*]} ]
set_output_delay  -clock [ get_clocks {cpu_system_inst|pll_sdram|sd1|pll7|clk[0]} ] 2   [get_ports {PortBidirSRAMDq[*]} ]
set_output_delay  -clock [ get_clocks {cpu_system_inst|pll_sdram|sd1|pll7|clk[0]}]  2   [get_ports {PortOutSRAMUbN}]
set_output_delay  -clock [ get_clocks {cpu_system_inst|pll_sdram|sd1|pll7|clk[0]}]  2   [get_ports {PortOutSRAMLbN}]
set_output_delay  -clock [ get_clocks {cpu_system_inst|pll_sdram|sd1|pll7|clk[0]}]  2   [get_ports {PortOutSRAMWeN}]
set_output_delay  -clock [ get_clocks {cpu_system_inst|pll_sdram|sd1|pll7|clk[0]}]  2   [get_ports {PortOutSRAMCeN}]
set_output_delay  -clock [ get_clocks {cpu_system_inst|pll_sdram|sd1|pll7|clk[0]}]  2   [get_ports {PortOutSRAMOeN}]

#**************************************************************
# jtag path
#**************************************************************

# Constrain the TDI port
set_input_delay -clock altera_reserved_tck -clock_fall -max 30.0 [get_ports altera_reserved_tdi]
set_input_delay -clock altera_reserved_tck -clock_fall -min  0.0 [get_ports altera_reserved_tdi]
# Constrain the TMS port
set_input_delay -clock altera_reserved_tck -clock_fall -max 30.0 [get_ports altera_reserved_tms]
set_input_delay -clock altera_reserved_tck -clock_fall -min  0.0 [get_ports altera_reserved_tms]
# Constrain the TDO port
set_output_delay -clock altera_reserved_tck -max  25.0 [get_ports altera_reserved_tdo]
set_output_delay -clock altera_reserved_tck -min   0.0 [get_ports altera_reserved_tdo]

#**************************************************************
# Set async path
#**************************************************************

set_max_delay -from [get_clocks {PortInEth0RxClk}] -to [get_clocks {cpu_system_inst|pll|sd1|pll7|clk[*]}] 6.0
set_max_delay -from [get_clocks {PortInEth1RxClk}] -to [get_clocks {cpu_system_inst|pll|sd1|pll7|clk[*]}] 6.0
set_max_delay -from [get_clocks {PortInEth0TxClk}] -to [get_clocks {cpu_system_inst|pll|sd1|pll7|clk[*]}] 4.0
set_max_delay -from [get_clocks {PortInEth1TxClk}] -to [get_clocks {cpu_system_inst|pll|sd1|pll7|clk[*]}] 4.0
set_min_delay -from [get_clocks {PortInEth0RxClk}] -to [get_clocks {cpu_system_inst|pll|sd1|pll7|clk[*]}] 0.0
set_min_delay -from [get_clocks {PortInEth1RxClk}] -to [get_clocks {cpu_system_inst|pll|sd1|pll7|clk[*]}] 0.0
set_min_delay -from [get_clocks {PortInEth0TxClk}] -to [get_clocks {cpu_system_inst|pll|sd1|pll7|clk[*]}] 0.0
set_min_delay -from [get_clocks {PortInEth1TxClk}] -to [get_clocks {cpu_system_inst|pll|sd1|pll7|clk[*]}] 0.0

#**************************************************************
# Set false path
#**************************************************************

set_false_path -from [get_ports {PortInEth0Col}] -to [get_keepers {*}]
set_false_path -from [get_ports {PortInEth0Crs}] -to [get_keepers {*}]
set_false_path -from [get_ports {PortInEth1Col}] -to [get_keepers {*}]
set_false_path -from [get_ports {PortInEth1Crs}] -to [get_keepers {*}]
set_false_path -from [get_ports {PortBidirLcdData[*]}] -to [get_keepers {*}]
set_false_path -from [get_ports {PortInButton[*]}] -to [get_keepers {*}]
set_false_path -from [get_ports {PortInKey[*]}] -to [get_keepers {*}]
set_false_path -from [get_ports {PortInPhy0IntN}] -to [get_keepers {*}]
set_false_path -from [get_ports {PortInPhy1IntN}] -to [get_keepers {*}]
set_false_path -from [get_ports {PortBidirMdio0}] -to [get_keepers {*}]
set_false_path -from [get_ports {PortBidirMdio1}] -to [get_keepers {*}]
set_false_path -from [get_ports {PortInDataIn}] -to [get_keepers {*}]
set_false_path -from [get_ports {PortInDataInValid}] -to [get_keepers {*}]
set_false_path -from [get_ports {PortInConfigFlashData}] -to [get_keepers {*}]
set_false_path -from [get_ports {PortOutPerformanceIndicator*}] -to [get_keepers {*}]

set_false_path -from * -to [get_ports {PortOutDRAMClk}]
set_false_path -from [get_keepers {*}] -to [get_ports {PortOutPhy0ResetN}]
set_false_path -from [get_keepers {*}] -to [get_ports {PortOutPhy1ResetN}]
set_false_path -from [get_keepers {*}] -to [get_ports {PortBidirMdio0}]
set_false_path -from [get_keepers {*}] -to [get_ports {PortBidirMdio1}]
set_false_path -from [get_keepers {*}] -to [get_ports {PortOutMdioClk0}]
set_false_path -from [get_keepers {*}] -to [get_ports {PortOutMdioClk1}]
set_false_path -from [get_keepers {*}] -to [get_ports {PortOutConfigFlashDclk}]
set_false_path -from [get_keepers {*}] -to [get_ports {PortOutConfigFlashSce}]
set_false_path -from [get_keepers {*}] -to [get_ports {PortOutConfigFlashSdo}]
set_false_path -from [get_keepers {*}] -to [get_ports {PortOut7Seg*[*]}]
set_false_path -from [get_keepers {*}] -to [get_ports {PortBidirLcdData[*]}]
set_false_path -from [get_keepers {*}] -to [get_ports {PortOutLedGreen[*]}]
set_false_path -from [get_keepers {*}] -to [get_ports {PortOutLedRed[*]}]
set_false_path -from [get_keepers {*}] -to [get_ports {PortOutLcd*}]
set_false_path -from [get_keepers {*}] -to [get_ports {PortOutClk}]
set_false_path -from [get_keepers {*}] -to [get_ports {PortOutDataOut}]
set_false_path -from [get_keepers {*}] -to [get_ports {PortOutDataOutValid}]
set_false_path -from [get_keepers {*}] -to [get_ports {PortOutPerformanceIndicator*}]

