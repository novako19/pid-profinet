#*****************************************************************************
#
# SOFTING Industrial Automation GmbH
# Richard-Reitzner-Allee 6
# D-85540 Haar
# Phone: ++49-89-4 56 56-0
# Fax: ++49-89-4 56 56-3 99
# http://www.softing.com
#
# Copyright (C) SOFTING Industrial Automation GmbH 2011-2014. All Rights Reserved.
#
# Version: 1.11.00
#
#*****************************************************************************

# -----------------------------------
# request TCL package from ACDS 13.1
# -----------------------------------
package require -exact qsys 13.1

# -----------------------------------
# Module parameter
# -----------------------------------
set MAJOR 1
set MINOR 12
set REVISION 8411
set CORE_VERSION [expr {($MAJOR << 24) + ($MINOR << 16) + $REVISION}]
set_module_property NAME softing_ie_core
set_module_property VERSION $MAJOR.$MINOR.$REVISION
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property GROUP "Softing/Industrial Automation/Ethernet"
set_module_property AUTHOR "Softing Industrial Automation GmbH"
set_module_property DISPLAY_NAME "IE Core 3/4 Ports"
set_module_property DESCRIPTION "Industrial Ethernet IP Core with 1 internal and 2/3 external ports. Supported protocols: PROFINET RT/IRT"
set_module_property ICON_PATH softing.png
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property ANALYZE_HDL TRUE
set_module_property ELABORATION_CALLBACK elaboration_callback
set_module_property VALIDATION_CALLBACK validation_callback

# -----------------------------------
# Files
# -----------------------------------
add_fileset QUARTUS_SYNTH QUARTUS_SYNTH fileset_synth_callback ""
set_fileset_property QUARTUS_SYNTH TOP_LEVEL switch_toplevel
set_fileset_property QUARTUS_SYNTH ENABLE_RELATIVE_INCLUDE_PATHS false


# -----------------------------------
# Parameters
# -----------------------------------
add_parameter WRITE_MASTER_WIDTH NATURAL 32
set_parameter_property WRITE_MASTER_WIDTH DEFAULT_VALUE 32
set_parameter_property WRITE_MASTER_WIDTH DISPLAY_NAME "Receive data master width"
set_parameter_property WRITE_MASTER_WIDTH TYPE NATURAL
set_parameter_property WRITE_MASTER_WIDTH UNITS None
set_parameter_property WRITE_MASTER_WIDTH ALLOWED_RANGES {32}
set_parameter_property WRITE_MASTER_WIDTH DESCRIPTION "Datawidth of the receive data master"
set_parameter_property WRITE_MASTER_WIDTH HDL_PARAMETER true

add_parameter READ_MASTER_WIDTH NATURAL 32
set_parameter_property READ_MASTER_WIDTH DEFAULT_VALUE 32
set_parameter_property READ_MASTER_WIDTH DISPLAY_NAME "Transmit data master width"
set_parameter_property READ_MASTER_WIDTH TYPE NATURAL
set_parameter_property READ_MASTER_WIDTH UNITS None
set_parameter_property READ_MASTER_WIDTH ALLOWED_RANGES {32}
set_parameter_property READ_MASTER_WIDTH DESCRIPTION "Datawidth of the transmit data master"
set_parameter_property READ_MASTER_WIDTH HDL_PARAMETER true

add_parameter PROTOCOL_PN_ENABLE BOOLEAN TRUE
set_parameter_property PROTOCOL_PN_ENABLE DEFAULT_VALUE TRUE
set_parameter_property PROTOCOL_PN_ENABLE DISPLAY_NAME "PN Protocol extension"
set_parameter_property PROTOCOL_PN_ENABLE TYPE BOOLEAN
set_parameter_property PROTOCOL_PN_ENABLE DESCRIPTION "Activates Hardware support for PROFINET IO."
set_parameter_property PROTOCOL_PN_ENABLE HDL_PARAMETER false

add_parameter SECURITY_DEVICE STRING TRUE
set_parameter_property SECURITY_DEVICE DEFAULT_VALUE "Softing EEPROM"
set_parameter_property SECURITY_DEVICE DISPLAY_NAME "IP Licensing Method"
set_parameter_property SECURITY_DEVICE DISPLAY_HINT radio
set_parameter_property SECURITY_DEVICE TYPE STRING
set_parameter_property SECURITY_DEVICE ALLOWED_RANGES {"Softing EEPROM" "Altera CPLD" "Product ID File"}
set_parameter_property SECURITY_DEVICE DESCRIPTION "Select the type of security device"
set_parameter_property SECURITY_DEVICE HDL_PARAMETER true

add_parameter MAX_NUMBER_EXTERNAL_PORTS INTEGER 2
set_parameter_property MAX_NUMBER_EXTERNAL_PORTS DEFAULT_VALUE 2
set_parameter_property MAX_NUMBER_EXTERNAL_PORTS DISPLAY_NAME "Number of physical ethernet ports"
set_parameter_property MAX_NUMBER_EXTERNAL_PORTS UNITS None
set_parameter_property MAX_NUMBER_EXTERNAL_PORTS ALLOWED_RANGES { 2 3 }
set_parameter_property MAX_NUMBER_EXTERNAL_PORTS DESCRIPTION "Select number of physical ethernet ports the switch should be connected"
set_parameter_property MAX_NUMBER_EXTERNAL_PORTS HDL_PARAMETER true

add_parameter PHY_INTERFACE STRING "MII"
set_parameter_property PHY_INTERFACE DEFAULT_VALUE "MII"
set_parameter_property PHY_INTERFACE DISPLAY_NAME "PHY communication type"
set_parameter_property PHY_INTERFACE TYPE STRING
set_parameter_property PHY_INTERFACE UNITS None
set_parameter_property PHY_INTERFACE ALLOWED_RANGES { "MII" "RMII" }
set_parameter_property PHY_INTERFACE DESCRIPTION "Selects the type of communication between PHY and MAC."
set_parameter_property PHY_INTERFACE HDL_PARAMETER true

add_parameter MDIO_CLAUSE STRING "Clause 22"
set_parameter_property MDIO_CLAUSE DEFAULT_VALUE "Clause 22"
set_parameter_property MDIO_CLAUSE DISPLAY_NAME "MDIO Clause"
set_parameter_property MDIO_CLAUSE TYPE STRING
set_parameter_property MDIO_CLAUSE UNITS None
set_parameter_property MDIO_CLAUSE ALLOWED_RANGES {"Clause 22"}
set_parameter_property MDIO_CLAUSE DESCRIPTION "Select MDIO protocol of the used PHYs"
set_parameter_property MDIO_CLAUSE HDL_PARAMETER true

add_parameter MDIO_CLK_DIVIDER INTEGER 50
set_parameter_property MDIO_CLK_DIVIDER DEFAULT_VALUE 50
set_parameter_property MDIO_CLK_DIVIDER DISPLAY_NAME "MDIO clock divider"
set_parameter_property MDIO_CLK_DIVIDER TYPE INTEGER
set_parameter_property MDIO_CLK_DIVIDER UNITS None
set_parameter_property MDIO_CLK_DIVIDER ALLOWED_RANGES { 5 10 20 50 }
set_parameter_property MDIO_CLK_DIVIDER DESCRIPTION "Select the clock divider that the MDIO clock meets the requirements of the used PHYs"
set_parameter_property MDIO_CLK_DIVIDER HDL_PARAMETER true

add_parameter MDIO_CLK INTEGER 0
set_parameter_property MDIO_CLK DEFAULT_VALUE 0
set_parameter_property MDIO_CLK DISPLAY_NAME "MDIO clock"
set_parameter_property MDIO_CLK TYPE INTEGER
set_parameter_property MDIO_CLK UNITS Hertz
set_parameter_property MDIO_CLK DESCRIPTION "Resulting MDIO clock (input clock/clock divider)"
set_parameter_property MDIO_CLK HDL_PARAMETER false
set_parameter_property MDIO_CLK DERIVED true

add_parameter ENABLED_HARDWARE_ACCELERATION INTEGER 1
set_parameter_property ENABLED_HARDWARE_ACCELERATION DEFAULT_VALUE 1
set_parameter_property ENABLED_HARDWARE_ACCELERATION UNITS None
set_parameter_property ENABLED_HARDWARE_ACCELERATION ALLOWED_RANGES 0:3
set_parameter_property ENABLED_HARDWARE_ACCELERATION HDL_PARAMETER true
set_parameter_property ENABLED_HARDWARE_ACCELERATION VISIBLE false
set_parameter_property ENABLED_HARDWARE_ACCELERATION DERIVED true

add_parameter PROCESS_DATA_SIZE STRING "1 kByte"
set_parameter_property PROCESS_DATA_SIZE DEFAULT_VALUE "1 kByte"
set_parameter_property PROCESS_DATA_SIZE DISPLAY_NAME "Size of the process data ram for provider and consumer"
set_parameter_property PROCESS_DATA_SIZE TYPE STRING
set_parameter_property PROCESS_DATA_SIZE UNITS None
set_parameter_property PROCESS_DATA_SIZE ALLOWED_RANGES {"1 kByte" "2 kByte"}
set_parameter_property PROCESS_DATA_SIZE DESCRIPTION "Select size of the process data ram. The overall usage is two times the selected value (sum of provider and consumer)"

add_parameter PROCESS_DATA_RAM_WIDTH INTEGER 8
set_parameter_property PROCESS_DATA_RAM_WIDTH HDL_PARAMETER true
set_parameter_property PROCESS_DATA_RAM_WIDTH VISIBLE false
set_parameter_property PROCESS_DATA_RAM_WIDTH DERIVED true

add_parameter CORE_VERSION INTEGER $CORE_VERSION
set_parameter_property CORE_VERSION HDL_PARAMETER true
set_parameter_property CORE_VERSION VISIBLE false
set_parameter_property CORE_VERSION DERIVED true

# -----------------------------------
# display items
# -----------------------------------
add_display_item "" "CPU Interface settings" group tab
add_display_item "" "Protocol settings" group tab
add_display_item "" "Ethernet Settings" group tab
add_display_item "" "Security Settings" group tab

add_display_item "CPU Interface settings" WRITE_MASTER_WIDTH parameter
add_display_item "CPU Interface settings" READ_MASTER_WIDTH parameter

add_display_item "Protocol settings" PROTOCOL_PN_ENABLE parameter
add_display_item "Protocol settings" PROCESS_DATA_SIZE parameter

add_display_item "Ethernet Settings" MAX_NUMBER_EXTERNAL_PORTS parameter
add_display_item "Ethernet Settings" PHY_INTERFACE parameter
add_display_item "Ethernet Settings" MDIO_CLAUSE parameter
add_display_item "Ethernet Settings" MDIO_CLK_DIVIDER parameter
add_display_item "Ethernet Settings" MDIO_CLK parameter

add_display_item "Security Settings" SECURITY_DEVICE parameter

# -----------------------------------
# Callbacks
# -----------------------------------
proc validation_callback {} {
  set PnEnabled [get_parameter_value PROTOCOL_PN_ENABLE]

  if {[string compare $PnEnabled "true"] == 0} {
    set_parameter_property PROCESS_DATA_SIZE ENABLED true
  } else {
    set_parameter_property PROCESS_DATA_SIZE ENABLED false
  }
}

proc elaboration_callback {} {
  global CORE_VERSION

  set_parameter_value CORE_VERSION $CORE_VERSION

  #----------------------------------------------------------------------------

  # Get the current value of parameters we care about
  set MdioClkDiv [get_parameter_value MDIO_CLK_DIVIDER]
  
  # check PHY clock
  if {$MdioClkDiv == 5} {
    set_parameter_value MDIO_CLK 25000000
    send_message info "MDIO Clock is set to 25 MHz"
  } elseif {$MdioClkDiv == 10} {
    set_parameter_value MDIO_CLK 12500000
    send_message info "MDIO Clock is set to 12.5 MHz"
  } elseif {$MdioClkDiv == 20} {
    set_parameter_value MDIO_CLK 6250000
    send_message info "MDIO Clock is set to 6.25 MHz"
  } else {
    set_parameter_value MDIO_CLK 2500000
    send_message info "MDIO Clock is set to 2.5 MHz"
  }

  #----------------------------------------------------------------------------

  # add system.h macros 
  set_module_assignment embeddedsw.CMacro.PHY_MAX_RX_DELAY 18
  set_module_assignment embeddedsw.CMacro.PHY_MAX_TX_DELAY 8
  
  set PnEnabled [get_parameter_value PROTOCOL_PN_ENABLE]
  set ProcessDataSize [get_parameter_value PROCESS_DATA_SIZE]

  #set default values
  set_parameter_value PROCESS_DATA_RAM_WIDTH 8

  set PNAcceleration 0
  set EIPAcceleration 0
  
  if {[string compare $PnEnabled "true"] == 0} {
    set_module_assignment embeddedsw.CMacro.PN_ENABLED 1
    set_module_assignment embeddedsw.CMacro.PN_PROVIDER_REGISTER_OFFSET 0x00000000
    set_module_assignment embeddedsw.CMacro.PN_PROVIDER_PROCESSDATA_OFFSET 0x00002000
    set_module_assignment embeddedsw.CMacro.PN_CONSUMER_REGISTER_OFFSET 0x00008000
    set_module_assignment embeddedsw.CMacro.PN_CONSUMER_PROCESSDATA_OFFSET 0x0000A000

    if {[string compare $ProcessDataSize "1 kByte"] == 0} {
      set_module_assignment embeddedsw.CMacro.PN_PROVIDER_PROCESSDATA_SIZE 1024
      set_module_assignment embeddedsw.CMacro.PN_CONSUMER_PROCESSDATA_SIZE 1024  
      set_parameter_value PROCESS_DATA_RAM_WIDTH 8
    } elseif {[string compare $ProcessDataSize "2 kByte"] == 0} {
      set_module_assignment embeddedsw.CMacro.PN_PROVIDER_PROCESSDATA_SIZE 2048
      set_module_assignment embeddedsw.CMacro.PN_CONSUMER_PROCESSDATA_SIZE 2048  
      set_parameter_value PROCESS_DATA_RAM_WIDTH 9
    }

    set_module_assignment embeddedsw.CMacro.INCLUDE_PNIO_DEVICE 1
    set_module_assignment embeddedsw.CMacro.NUMBER_SUPPORTED_DEVICES 1

    set PNAcceleration 1
  } else {
    set_module_assignment embeddedsw.CMacro.PN_ENABLED 0
  }
  
  set NumberExternalPorts [get_parameter_value MAX_NUMBER_EXTERNAL_PORTS]

  set_module_assignment embeddedsw.CMacro.SWITCH_NUMBER_EXT_PORTS $NumberExternalPorts
  set_module_assignment embeddedsw.CMacro.SWITCH_NUMBER_INT_PORTS 1
  set_module_assignment embeddedsw.CMacro.SWITCH_MAX_PACKET_LENGTH 1536

  set_parameter_value ENABLED_HARDWARE_ACCELERATION [expr {$PNAcceleration + $EIPAcceleration}]

  #----------------------------------------------------------------------------

  if {$NumberExternalPorts == 3} {
    add_interface_port rmii coe_rmii_p2_clock export Input 1
    add_interface_port rmii coe_rmii_p2_carrier_rxenable export Input 1    
    add_interface_port rmii coe_rmii_p2_rx export Input 2         
    add_interface_port rmii coe_rmii_p2_txenable export Output 1    
    add_interface_port rmii coe_rmii_p2_tx export Output 2         

    #---------------------------------------------------------------------------

    add_interface_port mii coe_mii_p2_collision export Input 1       
    add_interface_port mii coe_mii_p2_carrier export Input 1         
    add_interface_port mii coe_mii_p2_rxclock export Input 1         
    add_interface_port mii coe_mii_p2_rxerror export Input 1         
    add_interface_port mii coe_mii_p2_rxenable export Input 1        
    add_interface_port mii coe_mii_p2_rx_red_enable export Output 1    
    add_interface_port mii coe_mii_p2_rx export Input 4         
    add_interface_port mii coe_mii_p2_txclock export Input 1          
    add_interface_port mii coe_mii_p2_txerror export Output 1          
    add_interface_port mii coe_mii_p2_txenable export Output 1         
    add_interface_port mii coe_mii_p2_tx_red_enable export Output 1    
    add_interface_port mii coe_mii_p2_tx export Output 4              
  } else {
  }

  if {[string compare [get_parameter_value PHY_INTERFACE] "MII"] == 0} {
    set_interface_property rmii ENABLED false
    set_interface_property mii ENABLED true
  } elseif {[string compare [get_parameter_value PHY_INTERFACE] "RMII"] == 0} {
    send_message info "RMII interface currently not supported"
    #set_interface_property rmii ENABLED true
    #set_interface_property mii ENABLED false
  }

  set UsedSecDevice [get_parameter_value SECURITY_DEVICE]

  if {[string compare $UsedSecDevice "Product ID File"] == 0} {
    set_interface_property auth ENABLED false
  }
}


# -----------------------------------
# connection point clock
# -----------------------------------
add_interface clock clock end
set_interface_property clock clockRate 0

set_interface_property clock ENABLED true

add_interface_port clock csi_clk clk Input 1

# -----------------------------------
# connection point clock_reset
# -----------------------------------
add_interface clock_reset reset end
set_interface_property clock_reset associatedClock clock
set_interface_property clock_reset synchronousEdges DEASSERT

set_interface_property clock_reset ENABLED true

add_interface_port clock_reset csi_reset_n reset_n Input 1

# -----------------------------------
# connection point irq
# -----------------------------------
add_interface irq_control interrupt end
set_interface_property irq_control associatedAddressablePoint control
set_interface_property irq_control associatedClock clock
set_interface_property irq_control associatedReset clock_reset

set_interface_property irq_control ENABLED true

add_interface_port irq_control ins_control_irq irq Output 1

# -----------------------------------
# connection point control
# -----------------------------------
add_interface control avalon end
set_interface_property control addressAlignment DYNAMIC
set_interface_property control addressUnits WORDS
set_interface_property control associatedClock clock
set_interface_property control burstOnBurstBoundariesOnly false
set_interface_property control explicitAddressSpan 0
set_interface_property control holdTime 0
set_interface_property control isMemoryDevice false
set_interface_property control isNonVolatileStorage false
set_interface_property control linewrapBursts false
set_interface_property control maximumPendingReadTransactions 0
set_interface_property control printableDevice false
set_interface_property control readLatency 0
set_interface_property control readWaitTime 1
set_interface_property control setupTime 0
set_interface_property control timingUnits Cycles
set_interface_property control writeWaitTime 0

set_interface_property control ENABLED true

add_interface_port control avs_control_read read Input 1
add_interface_port control avs_control_write write Input 1
add_interface_port control avs_control_address address Input 17
add_interface_port control avs_control_readdata readdata Output 32
add_interface_port control avs_control_writedata writedata Input 32
add_interface_port control avs_control_waitrequest waitrequest Output 1

# -----------------------------------
# connection point process_data
# -----------------------------------
add_interface process_data avalon end
set_interface_property process_data addressAlignment DYNAMIC
set_interface_property process_data addressUnits WORDS
set_interface_property process_data associatedClock clock
set_interface_property process_data burstOnBurstBoundariesOnly false
set_interface_property process_data explicitAddressSpan 0
set_interface_property process_data holdTime 0
set_interface_property process_data isMemoryDevice false
set_interface_property process_data isNonVolatileStorage false
set_interface_property process_data linewrapBursts false
set_interface_property process_data maximumPendingReadTransactions 0
set_interface_property process_data printableDevice false
set_interface_property process_data readLatency 0
set_interface_property process_data readWaitTime 1
set_interface_property process_data setupTime 0
set_interface_property process_data timingUnits Cycles
set_interface_property process_data writeWaitTime 0

set_interface_property process_data ENABLED true

add_interface_port process_data avs_process_data_read read Input 1
add_interface_port process_data avs_process_data_write write Input 1
add_interface_port process_data avs_process_data_address address Input 14
add_interface_port process_data avs_process_data_byteenable byteenable Input 4
add_interface_port process_data avs_process_data_readdata readdata Output 32
add_interface_port process_data avs_process_data_writedata writedata Input 32
add_interface_port process_data avs_process_data_waitrequest waitrequest Output 1

# -----------------------------------
# connection point irq
# -----------------------------------
add_interface irq_acyclicp0 interrupt end
set_interface_property irq_acyclicp0 associatedAddressablePoint acyclicp0
set_interface_property irq_acyclicp0 associatedClock clock
set_interface_property irq_acyclicp0 associatedReset clock_reset

set_interface_property irq_acyclicp0 ENABLED true

add_interface_port irq_acyclicp0 ins_acyclicp0_irq irq Output 1

# -----------------------------------
# connection point acyclicp0
# -----------------------------------
add_interface acyclicp0 avalon end
set_interface_property acyclicp0 addressAlignment DYNAMIC
set_interface_property acyclicp0 addressUnits WORDS
set_interface_property acyclicp0 associatedClock clock
set_interface_property acyclicp0 burstOnBurstBoundariesOnly false
set_interface_property acyclicp0 explicitAddressSpan 0
set_interface_property acyclicp0 holdTime 0
set_interface_property acyclicp0 isMemoryDevice false
set_interface_property acyclicp0 isNonVolatileStorage false
set_interface_property acyclicp0 linewrapBursts false
set_interface_property acyclicp0 maximumPendingReadTransactions 0
set_interface_property acyclicp0 printableDevice false
set_interface_property acyclicp0 readLatency 0
set_interface_property acyclicp0 readWaitTime 1
set_interface_property acyclicp0 setupTime 0
set_interface_property acyclicp0 timingUnits Cycles
set_interface_property acyclicp0 writeWaitTime 0

set_interface_property acyclicp0 ENABLED true

add_interface_port acyclicp0 avs_acyclicp0_read read Input 1
add_interface_port acyclicp0 avs_acyclicp0_write write Input 1
add_interface_port acyclicp0 avs_acyclicp0_address address Input 9
add_interface_port acyclicp0 avs_acyclicp0_readdata readdata Output 32
add_interface_port acyclicp0 avs_acyclicp0_writedata writedata Input 32
add_interface_port acyclicp0 avs_acyclicp0_waitrequest waitrequest Output 1

# -----------------------------------
# connection point acyclicp0tx
# -----------------------------------
add_interface acyclicp0tx avalon start
set_interface_property acyclicp0tx addressUnits SYMBOLS
set_interface_property acyclicp0tx associatedClock clock
set_interface_property acyclicp0tx burstOnBurstBoundariesOnly false
set_interface_property acyclicp0tx doStreamReads false
set_interface_property acyclicp0tx doStreamWrites false
set_interface_property acyclicp0tx linewrapBursts false
set_interface_property acyclicp0tx readLatency 0

set_interface_property acyclicp0tx ENABLED true

add_interface_port acyclicp0tx avm_acyclicp0tx_read read Output 1
add_interface_port acyclicp0tx avm_acyclicp0tx_address address Output 32
add_interface_port acyclicp0tx avm_acyclicp0tx_waitrequest waitrequest Input 1
add_interface_port acyclicp0tx avm_acyclicp0tx_readdata readdata Input 32

# -----------------------------------
# connection point acyclicp0rx
# -----------------------------------
add_interface acyclicp0rx avalon start
set_interface_property acyclicp0rx addressUnits SYMBOLS
set_interface_property acyclicp0rx associatedClock clock
set_interface_property acyclicp0rx burstOnBurstBoundariesOnly false
set_interface_property acyclicp0rx doStreamReads false
set_interface_property acyclicp0rx doStreamWrites false
set_interface_property acyclicp0rx linewrapBursts false
set_interface_property acyclicp0rx readLatency 0

set_interface_property acyclicp0rx ENABLED true

add_interface_port acyclicp0rx avm_acyclicp0rx_write write Output 1
add_interface_port acyclicp0rx avm_acyclicp0rx_address address Output 32
add_interface_port acyclicp0rx avm_acyclicp0rx_waitrequest waitrequest Input 1
add_interface_port acyclicp0rx avm_acyclicp0rx_writedata writedata Output 32

# -----------------------------------
# connection point clockshift
# -----------------------------------
add_interface clockshift conduit end

set_interface_property clockshift ENABLED true

add_interface_port clockshift coe_clk_90 export Input 1
add_interface_port clockshift coe_clk_180 export Input 1
add_interface_port clockshift coe_clk_270 export Input 1

# -----------------------------------
# connection point rmii
# -----------------------------------
add_interface rmii conduit end

set_interface_property rmii ENABLED false

add_interface_port rmii coe_rmii_p0_clock export Input 1

add_interface_port rmii coe_rmii_p0_carrier_rxenable export Input 1    
add_interface_port rmii coe_rmii_p0_rx export Input 2         

add_interface_port rmii coe_rmii_p0_txenable export Output 1    
add_interface_port rmii coe_rmii_p0_tx export Output 2         

add_interface_port rmii coe_rmii_p1_clock export Input 1

add_interface_port rmii coe_rmii_p1_carrier_rxenable export Input 1    
add_interface_port rmii coe_rmii_p1_rx export Input 2         

add_interface_port rmii coe_rmii_p1_txenable export Output 1    
add_interface_port rmii coe_rmii_p1_tx export Output 2         

# -----------------------------------
# connection point mii
# -----------------------------------
add_interface mii conduit end

set_interface_property mii ENABLED true

add_interface_port mii coe_mii_p0_collision export Input 1
add_interface_port mii coe_mii_p0_carrier export Input 1  

add_interface_port mii coe_mii_p0_rxclock export Input 1     
add_interface_port mii coe_mii_p0_rxerror export Input 1     
add_interface_port mii coe_mii_p0_rxenable export Input 1    
add_interface_port mii coe_mii_p0_rx_red_enable export Output 1    
add_interface_port mii coe_mii_p0_rx export Input 4         

add_interface_port mii coe_mii_p0_txclock export Input 1     
add_interface_port mii coe_mii_p0_txerror export Output 1     
add_interface_port mii coe_mii_p0_txenable export Output 1    
add_interface_port mii coe_mii_p0_tx_red_enable export Output 1    
add_interface_port mii coe_mii_p0_tx export Output 4         

add_interface_port mii coe_mii_p1_collision export Input 1       
add_interface_port mii coe_mii_p1_carrier export Input 1         

add_interface_port mii coe_mii_p1_rxclock export Input 1         
add_interface_port mii coe_mii_p1_rxerror export Input 1         
add_interface_port mii coe_mii_p1_rxenable export Input 1        
add_interface_port mii coe_mii_p1_rx_red_enable export Output 1    
add_interface_port mii coe_mii_p1_rx export Input 4         

add_interface_port mii coe_mii_p1_txclock export Input 1          
add_interface_port mii coe_mii_p1_txerror export Output 1          
add_interface_port mii coe_mii_p1_txenable export Output 1         
add_interface_port mii coe_mii_p1_tx_red_enable export Output 1    
add_interface_port mii coe_mii_p1_tx export Output 4              

# -----------------------------------
# connection point mdio
# -----------------------------------
add_interface mdio conduit end

set_interface_property mdio ENABLED true

add_interface_port mdio coe_mdio_clock export Output 1
add_interface_port mdio coe_mdio_in export Input 1
add_interface_port mdio coe_mdio_out export Output 1
add_interface_port mdio coe_mdio_outenable export Output 1
add_interface_port mdio coe_mdio_portselect export Output 1

# -----------------------------------
# connection point auth
# -----------------------------------
add_interface auth conduit end

set_interface_property auth ENABLED true

add_interface_port auth coe_auth_clk export Output 1
add_interface_port auth coe_auth_dataoutvalid export Output 1
add_interface_port auth coe_auth_dataout export Output 1
add_interface_port auth coe_auth_datainvalid export Input 1
add_interface_port auth coe_auth_datain export Input 1

# -----------------------------------
# connection point signal
# -----------------------------------
add_interface signal conduit end

set_interface_property signal ENABLED true

add_interface_port signal coe_sync export Output 1
add_interface_port signal coe_sync_timestamp export Output 32
add_interface_port signal coe_31_25us_base_clk export Output 1
add_interface_port signal coe_cycle_counter export Output 16
add_interface_port signal coe_cycle_timestamp export Output 16
add_interface_port signal coe_consumer_update export Output 1
add_interface_port signal coe_provider_update export Output 1
add_interface_port signal coe_cputx export Output 1
add_interface_port signal coe_cpurx export Output 1

# -----------------------------------
# connection point datain
# -----------------------------------
add_interface dma_input_data_selector conduit end
set_interface_property dma_input_data_selector ENABLED true

add_interface_port dma_input_data_selector coe_datain_selector selector Input 16
add_interface_port dma_input_data_selector coe_datain_allowed allowed Output 1

# -----------------------------------
# connection point dataout
# -----------------------------------
add_interface dma_output_data_selector conduit end
set_interface_property dma_output_data_selector ENABLED true

add_interface_port dma_output_data_selector coe_dataout_selector selector Input 16
add_interface_port dma_output_data_selector coe_dataout_allowed allowed Output 1

# -----------------------------------
# connection point dataout
# -----------------------------------
add_interface dma_output_interrupt_cond conduit end
set_interface_property dma_output_interrupt_cond ENABLED true

add_interface_port dma_output_interrupt_cond coe_extin_intreq intreq Output 1
add_interface_port dma_output_interrupt_cond coe_extin_intclear intclear Input 1
add_fileset_file "./softing/switch_toplevel.vhd" VHDL PATH "./switch_toplevel.vhd"
add_fileset_file "./softing/switch_toplevel.v" VERILOG PATH "./switch_toplevel.v"

add_fileset_file "./softing/gkeeper.sdc" sdc PATH "./gkeeper.sdc"
add_fileset_file "./softing/sha1_ram_init.hex" hex PATH "./sha1_ram_init.hex"

proc fileset_synth_callback {entityname} {
if {[ string compare [get_parameter_value SECURITY_DEVICE] "Product ID File"] == 0} {
  add_fileset_file "./softing/product_id_file.vhd" VHDL PATH "./product_id_file.vhd"
  }
}
