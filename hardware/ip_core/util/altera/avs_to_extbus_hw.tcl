#*****************************************************************************
#
# SOFTING Industrial Automation GmbH
# Richard-Reitzner-Allee 6
# D-85540 Haar
# Phone: ++49-89-4 56 56-0
# Fax: ++49-89-4 56 56-3 99
# http://www.softing.com
#
# Copyright (C) SOFTING Industrial Automation GmbH 2011-2014. All Rights Reserved.
#
# Version: 1.10.00
#
#*****************************************************************************

# -----------------------------------
# request TCL package from ACDS 13.1
# -----------------------------------
package require -exact qsys 13.1

# -----------------------------------
# Module parameter
# -----------------------------------
set_module_property NAME softing_avs_to_extbus
set_module_property VERSION 1.10.00
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property GROUP "Softing/Industrial Automation/Utilities"
set_module_property AUTHOR "Softing Industrial Automation GmbH"
set_module_property DISPLAY_NAME "AVS-to-External Bus Core"
set_module_property DESCRIPTION "Connects an Avalon slave core to an external bus. Use this to connect an external memory to the FPGA."
set_module_property ICON_PATH softing.png
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property ANALYZE_HDL TRUE
set_module_property REPORT_TO_TALKBACK false
set_module_property ALLOW_GREYBOX_GENERATION false
set_module_property ELABORATION_CALLBACK elaboration_callback

# -----------------------------------
# Files
# -----------------------------------
add_fileset QUARTUS_SYNTH QUARTUS_SYNTH "" ""
set_fileset_property QUARTUS_SYNTH TOP_LEVEL softing_avs_to_extbus
set_fileset_property QUARTUS_SYNTH ENABLE_RELATIVE_INCLUDE_PATHS false
add_fileset_file "./softing/avs_to_extbus.vhd" VHDL PATH "./avs_to_extbus.vhd"

# -----------------------------------
# Parameters
# -----------------------------------
add_parameter ADDRESS_WIDTH NATURAL 16
set_parameter_property ADDRESS_WIDTH DEFAULT_VALUE 16
set_parameter_property ADDRESS_WIDTH DISPLAY_NAME "Address width"
set_parameter_property ADDRESS_WIDTH DESCRIPTION "The address width of the bus interface."
set_parameter_property ADDRESS_WIDTH TYPE NATURAL
set_parameter_property ADDRESS_WIDTH UNITS BITS
set_parameter_property ADDRESS_WIDTH ALLOWED_RANGES "1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32"
set_parameter_property ADDRESS_WIDTH HDL_PARAMETER true

add_parameter DATA_WIDTH NATURAL 16
set_parameter_property DATA_WIDTH DEFAULT_VALUE 16
set_parameter_property DATA_WIDTH DISPLAY_NAME "Data width"
set_parameter_property DATA_WIDTH DESCRIPTION "The data width of the Avalon and external bus interface."
set_parameter_property DATA_WIDTH TYPE NATURAL
set_parameter_property DATA_WIDTH UNITS BITS
set_parameter_property DATA_WIDTH ALLOWED_RANGES "16 32"
set_parameter_property DATA_WIDTH HDL_PARAMETER true

# -----------------------------------
# display items
# -----------------------------------

# -----------------------------------
# Callbacks
# -----------------------------------
proc elaboration_callback {} {
  set DataWidth [get_parameter_value DATA_WIDTH]

  set_port_property AvalonSlaveByteEnable WIDTH_EXPR [expr {($DataWidth / 8)}]
  set_port_property ExtByteEnableN WIDTH_EXPR [expr {($DataWidth / 8)}]
}

# -----------------------------------
# connection point clock
# -----------------------------------
add_interface clock clock end
set_interface_property clock clockRate 0
set_interface_property clock ENABLED true

add_interface_port clock Clock clk Input 1

# -----------------------------------
# connection point reset
# -----------------------------------
add_interface reset reset end
set_interface_property reset associatedClock clock
set_interface_property reset synchronousEdges DEASSERT
set_interface_property reset ENABLED true

add_interface_port reset ResetN reset_n Input 1

# -----------------------------------
# connection point avalon_slave
# -----------------------------------
add_interface avalon_slave avalon end
set_interface_property avalon_slave addressAlignment DYNAMIC
set_interface_property avalon_slave addressUnits WORDS
set_interface_property avalon_slave associatedClock clock
set_interface_property avalon_slave associatedReset reset
set_interface_property avalon_slave burstOnBurstBoundariesOnly false
set_interface_property avalon_slave explicitAddressSpan 0
set_interface_property avalon_slave holdTime 0
set_interface_property avalon_slave isMemoryDevice true
set_interface_property avalon_slave isNonVolatileStorage false
set_interface_property avalon_slave linewrapBursts false
set_interface_property avalon_slave maximumPendingReadTransactions 0
set_interface_property avalon_slave printableDevice false
set_interface_property avalon_slave readLatency 0
set_interface_property avalon_slave readWaitTime 1
set_interface_property avalon_slave setupTime 0
set_interface_property avalon_slave timingUnits Cycles
set_interface_property avalon_slave writeWaitTime 0

set_interface_property avalon_slave ENABLED true

add_interface_port avalon_slave AvalonSlaveRead read Input 1
add_interface_port avalon_slave AvalonSlaveWrite write Input 1
add_interface_port avalon_slave AvalonSlaveByteEnable byteenable Input 2
add_interface_port avalon_slave AvalonSlaveReadData readdata Output DATA_WIDTH
add_interface_port avalon_slave AvalonSlaveWriteData writedata Input DATA_WIDTH
add_interface_port avalon_slave AvalonSlaveAddress address Input ADDRESS_WIDTH
add_interface_port avalon_slave AvalonSlaveWaitrequest waitrequest Output 1

# -----------------------------------
# connection point extbus
# -----------------------------------
add_interface extbus conduit end

set_interface_property extbus ENABLED true

add_interface_port extbus ExtChipSelectN export Output 1
add_interface_port extbus ExtReadEnableN export Output 1
add_interface_port extbus ExtWriteEnableN export Output 1
add_interface_port extbus ExtByteEnableN export Output 2
add_interface_port extbus ExtData export Bidir DATA_WIDTH
add_interface_port extbus ExtAddress export Output ADDRESS_WIDTH
add_interface_port extbus ExtReadyN export Input 1


