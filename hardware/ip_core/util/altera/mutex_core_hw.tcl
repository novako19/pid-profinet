#*****************************************************************************
#
# SOFTING Industrial Automation GmbH
# Richard-Reitzner-Allee 6
# D-85540 Haar
# Phone: ++49-89-4 56 56-0
# Fax: ++49-89-4 56 56-3 99
# http://www.softing.com
#
# Copyright (C) SOFTING Industrial Automation GmbH 2011-2014. All Rights Reserved.
#
# Version: 1.03.00
#
#*****************************************************************************

# -----------------------------------
# request TCL package from ACDS 13.1
# -----------------------------------
package require -exact qsys 13.1

# -----------------------------------
# Module parameter
# -----------------------------------
set_module_property NAME softing_mutex
set_module_property VERSION 1.04.00
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property GROUP "Softing/Industrial Automation/Utilities"
set_module_property AUTHOR "Softing Industrial Automation GmbH"
set_module_property DISPLAY_NAME "CPU Mutex"
set_module_property DESCRIPTION "Mutex core to synchronize CPUs"
set_module_property ICON_PATH softing.png
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property ANALYZE_HDL TRUE
set_module_property REPORT_TO_TALKBACK false
set_module_property ALLOW_GREYBOX_GENERATION false
set_module_property ELABORATION_CALLBACK elaboration_callback

# -----------------------------------
# Files
# -----------------------------------
add_fileset QUARTUS_SYNTH QUARTUS_SYNTH "" ""
set_fileset_property QUARTUS_SYNTH TOP_LEVEL softing_mutex
set_fileset_property QUARTUS_SYNTH ENABLE_RELATIVE_INCLUDE_PATHS false
add_fileset_file cpu_mutex.vhd VHDL PATH mutex_core.vhd

# -----------------------------------
# Parameters
# -----------------------------------
add_parameter MUTEX_NUMBER NATURAL 5
set_parameter_property MUTEX_NUMBER DEFAULT_VALUE 5
set_parameter_property MUTEX_NUMBER DESCRIPTION "Number available mutex instances."
set_parameter_property MUTEX_NUMBER DISPLAY_NAME "Number instances"
set_parameter_property MUTEX_NUMBER TYPE NATURAL
set_parameter_property MUTEX_NUMBER UNITS None
set_parameter_property MUTEX_NUMBER ALLOWED_RANGES "1 2 3 4 5 6 7 8"
set_parameter_property MUTEX_NUMBER HDL_PARAMETER true

add_parameter MUTEX_REG_WIDTH NATURAL 8
set_parameter_property MUTEX_REG_WIDTH DEFAULT_VALUE 8
set_parameter_property MUTEX_REG_WIDTH DESCRIPTION "The width of the value and owner register of the mutex instances."
set_parameter_property MUTEX_REG_WIDTH DISPLAY_NAME "Register width"
set_parameter_property MUTEX_REG_WIDTH TYPE NATURAL
set_parameter_property MUTEX_REG_WIDTH UNITS BITS
set_parameter_property MUTEX_REG_WIDTH ALLOWED_RANGES "1 2 3 4 5 6 7 8"
set_parameter_property MUTEX_REG_WIDTH HDL_PARAMETER true

add_parameter EXT_ENABLE BOOLEAN false
set_parameter_property EXT_ENABLE DEFAULT_VALUE false
set_parameter_property EXT_ENABLE DESCRIPTION "Enables an external interface to lock/unlock the mutex cores."
set_parameter_property EXT_ENABLE DISPLAY_NAME "External interface"
set_parameter_property EXT_ENABLE TYPE BOOLEAN
set_parameter_property EXT_ENABLE HDL_PARAMETER false

# -----------------------------------
# display items
# -----------------------------------

# -----------------------------------
# Callbacks
# -----------------------------------
proc elaboration_callback {} {
  set MutexCount [get_parameter_value MUTEX_NUMBER]
  set MutexRegWidth [get_parameter_value MUTEX_REG_WIDTH]
  set ExternalInt [get_parameter_value EXT_ENABLE]
  
  if {[string compare $ExternalInt "true"] == 0} {
    set_interface_property ext ENABLED true
    
    set_port_property coe_extlock_req WIDTH_EXPR $MutexCount
    set_port_property coe_extlock_ack WIDTH_EXPR $MutexCount
    set_port_property coe_extlock_free WIDTH_EXPR $MutexCount
  } else {
    set_interface_property ext ENABLED false
    
    set_port_property coe_extlock_req termination_value 0
    set_port_property coe_extlock_req termination true
    set_port_property coe_extlock_free termination_value 0
    set_port_property coe_extlock_free termination true
    set_port_property coe_extlock_ack termination true
  }

  # add system.h macros 
  set_module_assignment embeddedsw.CMacro.MUTEX_COUNT $MutexCount
  set_module_assignment embeddedsw.CMacro.REGISTER_WIDTH $MutexRegWidth
  set_module_assignment embeddedsw.CMacro.EXTINT $ExternalInt
}

# -----------------------------------
# connection point clock_reset
# -----------------------------------
add_interface clock_reset reset end
set_interface_property clock_reset associatedClock clock
set_interface_property clock_reset synchronousEdges DEASSERT
set_interface_property clock_reset ENABLED true

add_interface_port clock_reset csi_reset_n reset_n Input 1

# -----------------------------------
# connection point clock
# -----------------------------------
add_interface clock clock end
set_interface_property clock clockRate 0
set_interface_property clock ENABLED true

add_interface_port clock csi_clk clk Input 1

# -----------------------------------
# connection point control
# -----------------------------------
add_interface control avalon end
set_interface_property control addressAlignment DYNAMIC
set_interface_property control addressUnits WORDS
set_interface_property control associatedClock clock
set_interface_property control burstOnBurstBoundariesOnly false
set_interface_property control explicitAddressSpan 0
set_interface_property control holdTime 0
set_interface_property control isMemoryDevice false
set_interface_property control isNonVolatileStorage false
set_interface_property control linewrapBursts false
set_interface_property control maximumPendingReadTransactions 0
set_interface_property control printableDevice false
set_interface_property control readLatency 0
set_interface_property control readWaitTime 2
set_interface_property control setupTime 0
set_interface_property control timingUnits Cycles
set_interface_property control writeWaitTime 1
set_interface_property control ENABLED true

add_interface_port control avs_control_read read Input 1
add_interface_port control avs_control_write write Input 1
add_interface_port control avs_control_byteenable byteenable Input 2
add_interface_port control avs_control_address address Input 5
add_interface_port control avs_control_readdata readdata Output 16
add_interface_port control avs_control_writedata writedata Input 16

# -----------------------------------
# connection point ext
# -----------------------------------
add_interface ext conduit end
set_interface_property ext ENABLED true

add_interface_port ext coe_extlock_req export Input 1
add_interface_port ext coe_extlock_ack export Output 1
add_interface_port ext coe_extlock_free export Input 1
