#*****************************************************************************
#
# SOFTING Industrial Automation GmbH
# Richard-Reitzner-Allee 6
# D-85540 Haar
# Phone: ++49-89-4 56 56-0
# Fax: ++49-89-4 56 56-3 99
# http://www.softing.com
#
# Copyright (C) SOFTING Industrial Automation GmbH 2011-2014. All Rights Reserved.
#
# Version: 1.10.00
#
#*****************************************************************************

# -----------------------------------
# request TCL package from ACDS 13.1
# -----------------------------------
package require -exact qsys 13.1

# -----------------------------------
# Module parameter
# -----------------------------------
set_module_property NAME softing_extbus_to_avm
set_module_property VERSION 1.10.00
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property GROUP "Softing/Industrial Automation/Utilities"
set_module_property AUTHOR "Softing Industrial Automation GmbH"
set_module_property DISPLAY_NAME "External Bus-to-AVM Core"
set_module_property DESCRIPTION "Connects an external bus to an Avalon master core. Use this to connect an external CPU to memory managed by the FPGA."
set_module_property ICON_PATH softing.png
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property ANALYZE_HDL TRUE
set_module_property REPORT_TO_TALKBACK false
set_module_property ALLOW_GREYBOX_GENERATION false
set_module_property ELABORATION_CALLBACK elaboration_callback

# -----------------------------------
# Files
# -----------------------------------
add_fileset QUARTUS_SYNTH QUARTUS_SYNTH "" ""
set_fileset_property QUARTUS_SYNTH TOP_LEVEL softing_extbus_to_avm
set_fileset_property QUARTUS_SYNTH ENABLE_RELATIVE_INCLUDE_PATHS false
add_fileset_file "./softing/extbus_to_avm.vhd" VHDL PATH "./extbus_to_avm.vhd"

# -----------------------------------
# Parameters
# -----------------------------------
add_parameter CORE_ENABLE INTEGER 1
set_parameter_property CORE_ENABLE DISPLAY_NAME "Enabled"
set_parameter_property CORE_ENABLE DESCRIPTION "Enables or disables the IP Core."
set_parameter_property CORE_ENABLE DEFAULT_VALUE 1
set_parameter_property CORE_ENABLE ALLOWED_RANGES "0 1"
set_parameter_property CORE_ENABLE TYPE INTEGER
set_parameter_property CORE_ENABLE HDL_PARAMETER true

add_parameter RESET_OUT_ENABLE INTEGER 1
set_parameter_property RESET_OUT_ENABLE DISPLAY_NAME "Reset enable"
set_parameter_property RESET_OUT_ENABLE DESCRIPTION "Enables or disables the reset out signal."
set_parameter_property RESET_OUT_ENABLE DEFAULT_VALUE 1
set_parameter_property RESET_OUT_ENABLE ALLOWED_RANGES "0 1"
set_parameter_property RESET_OUT_ENABLE TYPE INTEGER
set_parameter_property RESET_OUT_ENABLE HDL_PARAMETER true

add_parameter EXT_ADDRESS_WIDTH NATURAL 16
set_parameter_property EXT_ADDRESS_WIDTH DEFAULT_VALUE 16
set_parameter_property EXT_ADDRESS_WIDTH DISPLAY_NAME "Ext bus address width"
set_parameter_property EXT_ADDRESS_WIDTH DESCRIPTION "The address width of the external bus interface."
set_parameter_property EXT_ADDRESS_WIDTH TYPE NATURAL
set_parameter_property EXT_ADDRESS_WIDTH UNITS BITS
set_parameter_property EXT_ADDRESS_WIDTH ALLOWED_RANGES "1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32"
set_parameter_property EXT_ADDRESS_WIDTH HDL_PARAMETER true

add_parameter AVL_ADDRESS_WIDTH NATURAL 16
set_parameter_property AVL_ADDRESS_WIDTH DEFAULT_VALUE 16
set_parameter_property AVL_ADDRESS_WIDTH DISPLAY_NAME "Avl bus address width"
set_parameter_property AVL_ADDRESS_WIDTH DESCRIPTION "The address width of the Avalon bus interface."
set_parameter_property AVL_ADDRESS_WIDTH TYPE NATURAL
set_parameter_property AVL_ADDRESS_WIDTH UNITS BITS
set_parameter_property AVL_ADDRESS_WIDTH ALLOWED_RANGES "1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32"
set_parameter_property AVL_ADDRESS_WIDTH HDL_PARAMETER true

add_parameter DATA_WIDTH NATURAL 16
set_parameter_property DATA_WIDTH DEFAULT_VALUE 16
set_parameter_property DATA_WIDTH DISPLAY_NAME "Data width"
set_parameter_property DATA_WIDTH DESCRIPTION "The data width of the Avalon and external bus interface."
set_parameter_property DATA_WIDTH TYPE NATURAL
set_parameter_property DATA_WIDTH UNITS BITS
set_parameter_property DATA_WIDTH ALLOWED_RANGES "16 32"
set_parameter_property DATA_WIDTH HDL_PARAMETER true

# -----------------------------------
# display items
# -----------------------------------

# -----------------------------------
# Callbacks
# -----------------------------------
proc elaboration_callback {} {
  set DataWidth [get_parameter_value DATA_WIDTH]

  set_port_property AvalonMasterByteEnable WIDTH_EXPR [expr {($DataWidth / 8)}]
  set_port_property ExtByteEnableN WIDTH_EXPR [expr {($DataWidth / 8)}]
}

# -----------------------------------
# connection point clock
# -----------------------------------
add_interface clock clock end
set_interface_property clock clockRate 0
set_interface_property clock ENABLED true

add_interface_port clock Clock clk Input 1

# -----------------------------------
# connection point reset
# -----------------------------------
add_interface reset reset end
set_interface_property reset associatedClock clock
set_interface_property reset synchronousEdges DEASSERT
set_interface_property reset ENABLED true

add_interface_port reset ResetN reset_n Input 1

# -----------------------------------
# connection point control
# -----------------------------------
add_interface control avalon end
set_interface_property control addressAlignment DYNAMIC
set_interface_property control associatedClock clock
set_interface_property control burstOnBurstBoundariesOnly false
set_interface_property control explicitAddressSpan 0
set_interface_property control holdTime 0
set_interface_property control isMemoryDevice false
set_interface_property control isNonVolatileStorage false
set_interface_property control linewrapBursts false
set_interface_property control maximumPendingReadTransactions 0
set_interface_property control printableDevice false
set_interface_property control readLatency 0
set_interface_property control readWaitTime 1
set_interface_property control setupTime 0
set_interface_property control timingUnits Cycles
set_interface_property control writeWaitTime 0

set_interface_property control ENABLED true

add_interface_port control RegisterAddress address Input 2
add_interface_port control RegisterWrite write Input 1
add_interface_port control RegisterRead read Input 1
add_interface_port control RegisterWriteData writedata Input 32
add_interface_port control RegisterReadData readdata Output 32

# -----------------------------------
# connection point avalon_master
# -----------------------------------
add_interface avalon_master avalon master
set_interface_property avalon_master associatedClock clock
set_interface_property avalon_master burstOnBurstBoundariesOnly false
set_interface_property avalon_master doStreamReads false
set_interface_property avalon_master doStreamWrites false
set_interface_property avalon_master linewrapBursts false

set_interface_property avalon_master ENABLED true

add_interface_port avalon_master AvalonMasterWaitrequest waitrequest Input 1
add_interface_port avalon_master AvalonMasterAddress address Output AVL_ADDRESS_WIDTH
add_interface_port avalon_master AvalonMasterWriteData writedata Output DATA_WIDTH
add_interface_port avalon_master AvalonMasterReadData readdata Input DATA_WIDTH
add_interface_port avalon_master AvalonMasterByteEnable byteenable Output 2
add_interface_port avalon_master AvalonMasterWrite write Output 1
add_interface_port avalon_master AvalonMasterRead read Output 1

# -----------------------------------
# connection point ext_bus_conduit
# -----------------------------------
add_interface extbus conduit end

set_interface_property extbus ENABLED true

add_interface_port extbus ExtChipSelectN export Input 1
add_interface_port extbus ExtReadEnableN export Input 1
add_interface_port extbus ExtWriteEnableN export Input 1
add_interface_port extbus ExtByteEnableN export Input 2
add_interface_port extbus ExtData export Bidir DATA_WIDTH
add_interface_port extbus ExtAddress export Input EXT_ADDRESS_WIDTH
add_interface_port extbus ExtReadyN export Output 1

# -----------------------------------
# connection point ext_reset_conduit
# -----------------------------------
add_interface extreset conduit end

set_interface_property extreset ENABLED true

add_interface_port extreset ResetOut export Output 1
