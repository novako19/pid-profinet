#*****************************************************************************
#
# SOFTING Industrial Automation GmbH
# Richard-Reitzner-Allee 6
# D-85540 Haar
# Phone: ++49-89-4 56 56-0
# Fax: ++49-89-4 56 56-3 99
# http://www.softing.com
#
# Copyright (C) SOFTING Industrial Automation GmbH 2011-2014. All Rights Reserved.
#
# Version: 1.00.00
#
#*****************************************************************************

# -----------------------------------
# request TCL package from ACDS 13.1
# -----------------------------------
package require -exact qsys 13.1

# -----------------------------------
# Module parameter
# -----------------------------------
set MAJOR 1
set MINOR 0
set REVISION 0
set CORE_VERSION [expr {($MAJOR << 24) + ($MINOR << 16) + $REVISION}]
set_module_property NAME softing_fieldbus_utility_core
set_module_property VERSION $MAJOR.$MINOR.$REVISION
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP false
set_module_property GROUP "Softing/Industrial Automation/Ethernet"
set_module_property AUTHOR "Softing Industrial Automation GmbH"
set_module_property DISPLAY_NAME "Fieldbus Utility Core"
set_module_property DESCRIPTION "Utility IP Core for Fieldbus IP Cores"
set_module_property ICON_PATH softing.png
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property ANALYZE_HDL TRUE
set_module_property VALIDATION_CALLBACK validation_callback
set_module_property COMPOSITION_CALLBACK composition_callback

# -----------------------------------
# Files
# -----------------------------------

# -----------------------------------
# Parameters
# -----------------------------------
# DMA parameter
add_parameter DMA_ENABLE BOOLEAN true
set_parameter_property DMA_ENABLE DISPLAY_NAME "Enabled"
set_parameter_property DMA_ENABLE DEFAULT_VALUE true
set_parameter_property DMA_ENABLE TYPE BOOLEAN

add_parameter DMA_SELECTOR_ENABLE BOOLEAN false
set_parameter_property DMA_SELECTOR_ENABLE DISPLAY_NAME "Selector interface"
set_parameter_property DMA_SELECTOR_ENABLE DEFAULT_VALUE false
set_parameter_property DMA_SELECTOR_ENABLE TYPE BOOLEAN

add_parameter DMA_INPUT_MASTER_READ_ADDRESS_OFFSET STD_LOGIC_VECTOR 0
set_parameter_property DMA_INPUT_MASTER_READ_ADDRESS_OFFSET WIDTH "32"
set_parameter_property DMA_INPUT_MASTER_READ_ADDRESS_OFFSET DISPLAY_NAME "Read master offset"
set_parameter_property DMA_INPUT_MASTER_READ_ADDRESS_OFFSET DESCRIPTION "Address Offset of input read master"
set_parameter_property DMA_INPUT_MASTER_READ_ADDRESS_OFFSET DISPLAY_HINT hexadecimal
set_parameter_property DMA_INPUT_MASTER_READ_ADDRESS_OFFSET DISPLAY_UNITS "address"

add_parameter DMA_INPUT_MASTER_WRITE_ADDRESS_OFFSET STD_LOGIC_VECTOR 0
set_parameter_property DMA_INPUT_MASTER_WRITE_ADDRESS_OFFSET WIDTH "32"
set_parameter_property DMA_INPUT_MASTER_WRITE_ADDRESS_OFFSET DISPLAY_NAME "Write master offset"
set_parameter_property DMA_INPUT_MASTER_WRITE_ADDRESS_OFFSET DESCRIPTION "Address Offset of input write master"
set_parameter_property DMA_INPUT_MASTER_WRITE_ADDRESS_OFFSET DISPLAY_HINT hexadecimal
set_parameter_property DMA_INPUT_MASTER_WRITE_ADDRESS_OFFSET DISPLAY_UNITS "address"

add_parameter DMA_OUTPUT_MASTER_READ_ADDRESS_OFFSET STD_LOGIC_VECTOR 0
set_parameter_property DMA_OUTPUT_MASTER_READ_ADDRESS_OFFSET WIDTH "32"
set_parameter_property DMA_OUTPUT_MASTER_READ_ADDRESS_OFFSET DISPLAY_NAME "Read master offset"
set_parameter_property DMA_OUTPUT_MASTER_READ_ADDRESS_OFFSET DESCRIPTION "Address Offset of output read master"
set_parameter_property DMA_OUTPUT_MASTER_READ_ADDRESS_OFFSET DISPLAY_HINT hexadecimal
set_parameter_property DMA_OUTPUT_MASTER_READ_ADDRESS_OFFSET DISPLAY_UNITS "address"

add_parameter DMA_OUTPUT_MASTER_WRITE_ADDRESS_OFFSET STD_LOGIC_VECTOR 0
set_parameter_property DMA_OUTPUT_MASTER_WRITE_ADDRESS_OFFSET WIDTH "32"
set_parameter_property DMA_OUTPUT_MASTER_WRITE_ADDRESS_OFFSET DISPLAY_NAME "Write master offset"
set_parameter_property DMA_OUTPUT_MASTER_WRITE_ADDRESS_OFFSET DESCRIPTION "Address Offset of output write master"
set_parameter_property DMA_OUTPUT_MASTER_WRITE_ADDRESS_OFFSET DISPLAY_HINT hexadecimal
set_parameter_property DMA_OUTPUT_MASTER_WRITE_ADDRESS_OFFSET DISPLAY_UNITS "address"

# Mutex parameter
add_parameter MUTEX_NUMBER NATURAL 8
set_parameter_property MUTEX_NUMBER DEFAULT_VALUE 8
set_parameter_property MUTEX_NUMBER DESCRIPTION "Number available mutex instances."
set_parameter_property MUTEX_NUMBER DISPLAY_NAME "Number instances"
set_parameter_property MUTEX_NUMBER TYPE NATURAL
set_parameter_property MUTEX_NUMBER UNITS None
set_parameter_property MUTEX_NUMBER ALLOWED_RANGES "1 2 3 4 5 6 7 8"

add_parameter MUTEX_REG_WIDTH NATURAL 8
set_parameter_property MUTEX_REG_WIDTH DEFAULT_VALUE 8
set_parameter_property MUTEX_REG_WIDTH DESCRIPTION "The width of the value and owner register of the mutex instances."
set_parameter_property MUTEX_REG_WIDTH DISPLAY_NAME "Register width"
set_parameter_property MUTEX_REG_WIDTH TYPE NATURAL
set_parameter_property MUTEX_REG_WIDTH UNITS BITS
set_parameter_property MUTEX_REG_WIDTH ALLOWED_RANGES "1 2 3 4 5 6 7 8"
set_parameter_property MUTEX_REG_WIDTH HDL_PARAMETER true

# DPRAM parameter
add_parameter DPRAM_MEMORY_SIZE NATURAL 4096
set_parameter_property DPRAM_MEMORY_SIZE DEFAULT_VALUE 4096
set_parameter_property DPRAM_MEMORY_SIZE DESCRIPTION "The size of the dpram used for communication between fieldbus system and application."
set_parameter_property DPRAM_MEMORY_SIZE DISPLAY_NAME "DPRAM Size"
set_parameter_property DPRAM_MEMORY_SIZE TYPE NATURAL
set_parameter_property DPRAM_MEMORY_SIZE UNITS BYTES

# system-console parameter
add_parameter SYSTEM_CONSOLE_SUPPORT BOOLEAN false
set_parameter_property SYSTEM_CONSOLE_SUPPORT DEFAULT_VALUE false
set_parameter_property SYSTEM_CONSOLE_SUPPORT DISPLAY_NAME "System-Console support"
set_parameter_property SYSTEM_CONSOLE_SUPPORT TYPE BOOLEAN
set_parameter_property SYSTEM_CONSOLE_SUPPORT DESCRIPTION "Enables access to subsystem memory over system console (dangerous)."
set_parameter_property SYSTEM_CONSOLE_SUPPORT HDL_PARAMETER false

# -----------------------------------
# display items
# -----------------------------------
add_display_item "" "DMA" group tab
add_display_item "" "Mutex" group tab
add_display_item "" "PIO" group tab
add_display_item "" "DPRAM" group tab
add_display_item "" "Other settings" group tab

add_display_item "DMA" "General Properties" group
add_display_item "DMA" "Input Path Properties" group
add_display_item "DMA" "Output Path Properties" group
add_display_item "General Properties" DMA_ENABLE parameter
add_display_item "General Properties" DMA_SELECTOR_ENABLE parameter
add_display_item "Input Path Properties" DMA_INPUT_MASTER_WIDTH parameter
add_display_item "Input Path Properties" DMA_INPUT_MASTER_WRITE_ADDRESS_OFFSET parameter
add_display_item "Input Path Properties" DMA_INPUT_MASTER_READ_ADDRESS_OFFSET parameter
add_display_item "Output Path Properties" DMA_OUTPUT_MASTER_WIDTH parameter
add_display_item "Output Path Properties" DMA_OUTPUT_MASTER_WRITE_ADDRESS_OFFSET parameter
add_display_item "Output Path Properties" DMA_OUTPUT_MASTER_READ_ADDRESS_OFFSET parameter

add_display_item "Mutex" MUTEX_NUMBER parameter
add_display_item "Mutex" MUTEX_REG_WIDTH parameter

add_display_item "DPRAM" DPRAM_MEMORY_SIZE parameter

add_display_item "Other settings" SYSTEM_CONSOLE_SUPPORT parameter

# -----------------------------------
# Callbacks
# -----------------------------------
proc validation_callback {} {
  set DmaEnable [get_parameter_value DMA_ENABLE]
  
  set_module_assignment embeddedsw.CMacro.FIELDBUS_DMA_ENABLED $DmaEnable
}

proc composition_callback {} {
  set DmaEnable            [get_parameter_value DMA_ENABLE]
  set DmaSelectorEnable    [get_parameter_value DMA_SELECTOR_ENABLE]
  set DmaInputReadOffset   [get_parameter_value DMA_INPUT_MASTER_READ_ADDRESS_OFFSET]
  set DmaInputWriteOffset  [get_parameter_value DMA_INPUT_MASTER_WRITE_ADDRESS_OFFSET]
  set DmaOutputReadOffset  [get_parameter_value DMA_OUTPUT_MASTER_READ_ADDRESS_OFFSET]
  set DmaOutputWriteOffset [get_parameter_value DMA_OUTPUT_MASTER_WRITE_ADDRESS_OFFSET]
  set MutexNumber          [get_parameter_value MUTEX_NUMBER]
  set MutexRegWidth        [get_parameter_value MUTEX_REG_WIDTH]
  set DpramMemorySize      [get_parameter_value DPRAM_MEMORY_SIZE]
  set SystemConsoleEnable  [get_parameter_value SYSTEM_CONSOLE_SUPPORT]
  
  # add internal components
  add_instance                 internal_system_clk_inst altera_clock_bridge
  add_instance                 internal_system_rst_inst altera_reset_bridge
  set_instance_parameter_value internal_system_rst_inst ACTIVE_LOW_RESET 1
  
  add_instance                 internal_fieldbus_clk_inst altera_clock_bridge
  add_instance                 internal_fieldbus_rst_inst altera_reset_bridge
  set_instance_parameter_value internal_fieldbus_rst_inst ACTIVE_LOW_RESET 1
  
  # add components
  add_instance           mutex softing_mutex
  set_instance_parameter mutex MUTEX_NUMBER    $MutexNumber
  set_instance_parameter mutex MUTEX_REG_WIDTH $MutexRegWidth
  set_instance_parameter mutex EXT_ENABLE      false
  
  add_instance           pio_from_appl softing_pio
  set_instance_parameter pio_from_appl PIO_DATA_WIDTH 1
  set_instance_parameter pio_from_appl PIO_DIRECTION  "Input"
  set_instance_parameter pio_from_appl EXT_ENABLE     false

  add_instance           pio_to_appl softing_pio
  set_instance_parameter pio_to_appl PIO_DATA_WIDTH 1
  set_instance_parameter pio_to_appl PIO_DIRECTION  "Output"
  set_instance_parameter pio_to_appl EXT_ENABLE     false
 
  add_instance           dma_controller softing_dma_controller
  set_instance_parameter dma_controller EXT_SELECTOR_ENABLE                $DmaSelectorEnable
  set_instance_parameter dma_controller DESCRIPTOR_ADDRESS_WIDTH           9
  set_instance_parameter dma_controller INPUT_MASTER_WIDTH                 32
  set_instance_parameter dma_controller INPUT_MASTER_READ_ADDRESS_OFFSET   $DmaInputReadOffset
  set_instance_parameter dma_controller INPUT_MASTER_WRITE_ADDRESS_OFFSET  $DmaInputWriteOffset
  set_instance_parameter dma_controller OUTPUT_MASTER_WIDTH                32
  set_instance_parameter dma_controller OUTPUT_MASTER_READ_ADDRESS_OFFSET  $DmaOutputReadOffset
  set_instance_parameter dma_controller OUTPUT_MASTER_WRITE_ADDRESS_OFFSET $DmaOutputWriteOffset
  set_instance_parameter dma_controller EXT_ENABLE                         true
  set_instance_parameter dma_controller EXT_INT_LOCK_WIDTH                 8
  set_instance_parameter dma_controller EXT_INT_LOCK_BIT                   3
  set_instance_parameter dma_controller EXT_INT_INTERRUPT_WIDTH            1
  set_instance_parameter dma_controller EXT_INT_INTERRUPT_BIT              0
  
  # add external interfaces
  add_interface          clock_system clock sink
  set_interface_property clock_system EXPORT_OF internal_system_clk_inst.in_clk

  add_interface          reset_system reset sink
  set_interface_property reset_system EXPORT_OF internal_system_rst_inst.in_reset

  add_interface          clock_fieldbus clock sink
  set_interface_property clock_fieldbus EXPORT_OF internal_fieldbus_clk_inst.in_clk
 
  add_interface          reset_fieldbus reset sink
  set_interface_property reset_fieldbus EXPORT_OF internal_fieldbus_rst_inst.in_reset

  add_interface          dma_register avalon slave
  add_interface          dma_interrupt interrupt sender
  add_interface          dma_control_master avalon master
  add_interface          dma_input_write_master avalon master
  add_interface          dma_input_read_master avalon master
  add_interface          dma_output_write_master avalon master
  add_interface          dma_output_read_master avalon master
  
  add_interface          dma_output_interrupt interrupt receiver
  add_interface          dma_output_interrupt_cond conduit end
  set_interface_property dma_output_interrupt_cond EXPORT_OF dma_controller.extout

  if {[string compare $DmaSelectorEnable "true"] == 0} {  
    add_interface          dma_input_data_selector conduit end
    set_interface_property dma_input_data_selector EXPORT_OF dma_controller.datain
    add_interface          dma_output_data_selector conduit end
    set_interface_property dma_output_data_selector EXPORT_OF dma_controller.dataout
  }

  add_interface          dma_running conduit end
  set_interface_property dma_running EXPORT_OF dma_controller.running
  
  add_interface          notification_in conduit end
  set_interface_property notification_in EXPORT_OF dma_controller.notification_in
  
  add_interface          notification_out conduit end
  set_interface_property notification_out EXPORT_OF dma_controller.notification_out
  
  # suppress all warnings of the DMA core (intentionally not all conduit interfaces are connected)
  set_instance_property dma_controller SUPPRESS_ALL_WARNINGS true
 
  # keep the external interfaces of the dma core
  if {[string compare $DmaEnable "true"] == 0} {
    #using the dma core
    add_connection internal_fieldbus_clk_inst.out_clk   dma_controller.clock clock ""
    add_connection internal_fieldbus_rst_inst.out_reset dma_controller.clock_reset reset
    
    set_interface_property dma_register            EXPORT_OF dma_controller.register
    set_interface_property dma_interrupt           EXPORT_OF dma_controller.irq_output
    set_interface_property dma_output_interrupt    EXPORT_OF dma_controller.output_irq_input
    set_interface_property dma_control_master      EXPORT_OF dma_controller.control_master
    set_interface_property dma_input_write_master  EXPORT_OF dma_controller.input_write_master
    set_interface_property dma_input_read_master   EXPORT_OF dma_controller.input_read_master
    set_interface_property dma_output_write_master EXPORT_OF dma_controller.output_write_master
    set_interface_property dma_output_read_master  EXPORT_OF dma_controller.output_read_master
    
    set_instance_parameter mutex         EXT_ENABLE true
    set_instance_parameter pio_from_appl EXT_ENABLE true

    add_connection dma_controller.extlock mutex.ext conduit
    add_connection pio_from_appl.ext      dma_controller.extin conduit
  } else {
    #using a dummy core, which will be optimized out and consumes no logic
    add_instance internal_interface_dummy_inst softing_interface_dummy
    set_instance_parameter internal_interface_dummy_inst NUMBER_DUMMY_MASTER  6
    set_instance_parameter internal_interface_dummy_inst NUMBER_DUMMY_SLAVE   6
    set_instance_parameter internal_interface_dummy_inst INTERRUPT_LOOP_DUMMY true
    
    add_connection internal_fieldbus_clk_inst.out_clk   internal_interface_dummy_inst.clock_in       clock
    add_connection internal_fieldbus_rst_inst.out_reset internal_interface_dummy_inst.clock_reset_in

    set_interface_property dma_register            EXPORT_OF internal_interface_dummy_inst.dummyslave0
    set_interface_property dma_interrupt           EXPORT_OF internal_interface_dummy_inst.irqloopout
    set_interface_property dma_output_interrupt    EXPORT_OF internal_interface_dummy_inst.irqloopin
    set_interface_property dma_control_master      EXPORT_OF internal_interface_dummy_inst.dummymaster0
    set_interface_property dma_input_write_master  EXPORT_OF internal_interface_dummy_inst.dummymaster1
    set_interface_property dma_input_read_master   EXPORT_OF internal_interface_dummy_inst.dummymaster2
    set_interface_property dma_output_write_master EXPORT_OF internal_interface_dummy_inst.dummymaster3
    set_interface_property dma_output_read_master  EXPORT_OF internal_interface_dummy_inst.dummymaster4
    
    add_connection internal_interface_dummy_inst.dummymaster5 dma_controller.register
    add_connection dma_controller.control_master              internal_interface_dummy_inst.dummyslave1
    add_connection dma_controller.input_write_master          internal_interface_dummy_inst.dummyslave2
    add_connection dma_controller.input_read_master           internal_interface_dummy_inst.dummyslave3
    add_connection dma_controller.output_write_master         internal_interface_dummy_inst.dummyslave4
    add_connection dma_controller.output_read_master          internal_interface_dummy_inst.dummyslave5
    
    add_connection internal_interface_dummy_inst.clock_out       dma_controller.clock       clock
    add_connection internal_interface_dummy_inst.clock_reset_out dma_controller.clock_reset reset
  }
 
  add_interface          mutex_register avalon slave
  set_interface_property mutex_register EXPORT_OF mutex.control

  add_interface          pio_from_appl_register avalon slave
  set_interface_property pio_from_appl_register EXPORT_OF pio_from_appl.register

  add_interface          pio_from_appl_data conduit end
  set_interface_property pio_from_appl_data EXPORT_OF pio_from_appl.data

  add_interface          pio_from_appl_interrupt interrupt sender
  set_interface_property pio_from_appl_interrupt EXPORT_OF pio_from_appl.irq_output

  add_interface          pio_to_appl_register avalon slave
  set_interface_property pio_to_appl_register EXPORT_OF pio_to_appl.register

  add_interface          pio_to_appl_data conduit end
  set_interface_property pio_to_appl_data EXPORT_OF pio_to_appl.data

  #----------------------------------------------------------------------------

  # add internal components used for dpram
  add_interface          dpram_s1     avalon slave
  add_interface          dpram_s2     avalon slave

  if {$DpramMemorySize > 0} {
    add_instance dpram altera_avalon_onchip_memory2

    # suppress all warnings of the dpram
    set_instance_property dpram SUPPRESS_ALL_WARNINGS true

    set_instance_parameter_value dpram {allowInSystemMemoryContentEditor} {0}
    set_instance_parameter_value dpram {blockType} {AUTO}
    set_instance_parameter_value dpram {dataWidth} {32}
    set_instance_parameter_value dpram {dualPort} {1}
    set_instance_parameter_value dpram {initMemContent} {0}
    set_instance_parameter_value dpram {initializationFileName} {dpram}
    set_instance_parameter_value dpram {instanceID} {NONE}
    set_instance_parameter_value dpram {memorySize} {12288.0}
    set_instance_parameter_value dpram {readDuringWriteMode} {DONT_CARE}
    set_instance_parameter_value dpram {simAllowMRAMContentsFile} {0}
    set_instance_parameter_value dpram {simMemInitOnlyFilename} {0}
    set_instance_parameter_value dpram {singleClockOperation} {0}
    set_instance_parameter_value dpram {slave1Latency} {1}
    set_instance_parameter_value dpram {slave2Latency} {1}
    set_instance_parameter_value dpram {useNonDefaultInitFile} {0}
    set_instance_parameter_value dpram {useShallowMemBlocks} {0}
    set_instance_parameter_value dpram {writable} {1}
    set_instance_parameter_value dpram {ecc_enabled} {0}

    set_instance_parameter_value dpram memorySize $DpramMemorySize

    add_connection internal_fieldbus_clk_inst.out_clk dpram.clk1 clock
    add_connection internal_fieldbus_rst_inst.out_reset dpram.reset1 reset

    add_connection internal_system_clk_inst.out_clk dpram.clk2 clock
    add_connection internal_system_rst_inst.out_reset dpram.reset2 reset

    set_interface_property dpram_s1 EXPORT_OF dpram.s1
    set_interface_property dpram_s2 EXPORT_OF dpram.s2
  } else {
    #using a dummy core, which will be optimized out and consumes no logic
    add_instance internal_interface_dpram_dummy_inst softing_interface_dummy
    set_instance_parameter internal_interface_dpram_dummy_inst NUMBER_DUMMY_SLAVE             2
    set_instance_parameter internal_interface_dpram_dummy_inst NUMBER_DUMMY_SLAVE_ADDR_WIDTH  12
    
    add_connection internal_fieldbus_clk_inst.out_clk   internal_interface_dpram_dummy_inst.clock_in clock
    add_connection internal_fieldbus_rst_inst.out_reset internal_interface_dpram_dummy_inst.clock_reset_in

    set_interface_property dpram_s1 EXPORT_OF internal_interface_dpram_dummy_inst.dummyslave0
    set_interface_property dpram_s2 EXPORT_OF internal_interface_dpram_dummy_inst.dummyslave1
  }

  #----------------------------------------------------------------------------

  add_interface system_console_master_reset reset  start
  add_interface system_console_master       avalon master
  add_interface system_console_slave_reset  reset  sink
  add_interface system_console_slave        avalon slave

  # keep the external interfaces of the system console
  if {[string compare $SystemConsoleEnable "true"] == 0} {
    #using the memory and jtag bridge
    add_instance debug_jtag_bridge altera_jtag_avalon_master
    set_instance_parameter_value debug_jtag_bridge {USE_PLI} {0}
    set_instance_parameter_value debug_jtag_bridge {PLI_PORT} {50000}
    set_instance_parameter_value debug_jtag_bridge {FAST_VER} {0}
    set_instance_parameter_value debug_jtag_bridge {FIFO_DEPTHS} {2}

    add_instance debug_memory altera_avalon_onchip_memory2

    # suppress all warnings of the dpram
    set_instance_property debug_memory SUPPRESS_ALL_WARNINGS true

    set_instance_parameter_value debug_memory {allowInSystemMemoryContentEditor} {0}
    set_instance_parameter_value debug_memory {blockType} {AUTO}
    set_instance_parameter_value debug_memory {dataWidth} {32}
    set_instance_parameter_value debug_memory {dualPort} {0}
    set_instance_parameter_value debug_memory {initMemContent} {0}
    set_instance_parameter_value debug_memory {initializationFileName} {onchip_mem.hex}
    set_instance_parameter_value debug_memory {instanceID} {NONE}
    set_instance_parameter_value debug_memory {memorySize} {2048.0}
    set_instance_parameter_value debug_memory {readDuringWriteMode} {DONT_CARE}
    set_instance_parameter_value debug_memory {simAllowMRAMContentsFile} {0}
    set_instance_parameter_value debug_memory {simMemInitOnlyFilename} {0}
    set_instance_parameter_value debug_memory {singleClockOperation} {0}
    set_instance_parameter_value debug_memory {slave1Latency} {1}
    set_instance_parameter_value debug_memory {slave2Latency} {1}
    set_instance_parameter_value debug_memory {useNonDefaultInitFile} {0}
    set_instance_parameter_value debug_memory {useShallowMemBlocks} {0}
    set_instance_parameter_value debug_memory {writable} {1}
    set_instance_parameter_value debug_memory {ecc_enabled} {0}

    add_connection internal_system_rst_inst.out_reset debug_jtag_bridge.clk_reset reset
    
    add_connection internal_system_clk_inst.out_clk debug_jtag_bridge.clk clock ""
    add_connection internal_system_clk_inst.out_clk debug_memory.clk1 clock ""

    set_interface_property system_console_master_reset EXPORT_OF debug_jtag_bridge.master_reset
    set_interface_property system_console_master       EXPORT_OF debug_jtag_bridge.master
    set_interface_property system_console_slave_reset  EXPORT_OF debug_memory.reset1
    set_interface_property system_console_slave        EXPORT_OF debug_memory.s1
  } else {
    #using a dummy core, which will be optimized out and consumes no logic
    add_instance internal_system_console_dummy_inst softing_interface_dummy
    set_instance_parameter internal_system_console_dummy_inst NUMBER_DUMMY_MASTER          1
    set_instance_parameter internal_system_console_dummy_inst NUMBER_DUMMY_SLAVE           1
    set_instance_parameter internal_system_console_dummy_inst INTERRUPT_LOOP_DUMMY         false
    
    add_connection internal_system_clk_inst.out_clk internal_system_console_dummy_inst.clock_in clock

    set_interface_property system_console_master_reset EXPORT_OF internal_system_console_dummy_inst.clock_reset_out
    set_interface_property system_console_master       EXPORT_OF internal_system_console_dummy_inst.dummymaster0
    set_interface_property system_console_slave_reset  EXPORT_OF internal_system_console_dummy_inst.clock_reset_in
    set_interface_property system_console_slave        EXPORT_OF internal_system_console_dummy_inst.dummyslave0
  }

  #----------------------------------------------------------------------------

  # connections
  add_connection internal_system_clk_inst.out_clk internal_system_rst_inst.clk     clock
  add_connection internal_fieldbus_clk_inst.out_clk internal_fieldbus_rst_inst.clk clock

  add_connection internal_fieldbus_clk_inst.out_clk mutex.clock       clock
  add_connection internal_fieldbus_clk_inst.out_clk pio_from_appl.clock clock
  add_connection internal_system_clk_inst.out_clk pio_to_appl.clock   clock
  
  add_connection internal_fieldbus_rst_inst.out_reset mutex.clock_reset
  add_connection internal_fieldbus_rst_inst.out_reset pio_from_appl.clock_reset    
  add_connection internal_system_rst_inst.out_reset pio_to_appl.clock_reset
}
