#*****************************************************************************
#
# SOFTING Industrial Automation GmbH
# Richard-Reitzner-Allee 6
# D-85540 Haar
# Phone: ++49-89-4 56 56-0
# Fax: ++49-89-4 56 56-3 99
# http://www.softing.com
#
# Copyright (C) SOFTING Industrial Automation GmbH 2011-2014. All Rights Reserved.
#
# Version: 1.00.00
#
#*****************************************************************************

# -----------------------------------
# request TCL package from ACDS 13.1
# -----------------------------------
package require -exact qsys 13.1

# -----------------------------------
# Module parameter
# -----------------------------------
set MAJOR 1
set MINOR 0
set REVISION 0
set CORE_VERSION [expr {($MAJOR << 24) + ($MINOR << 16) + $REVISION}]
set_module_property NAME softing_debug_slave
set_module_property VERSION $MAJOR.$MINOR.$REVISION
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property GROUP "Softing/Industrial Automation/Utilities"
set_module_property AUTHOR "Softing Industrial Automation GmbH"
set_module_property DISPLAY_NAME "Debug Slave"
set_module_property DESCRIPTION "Can be used as AXI default slave"
set_module_property ICON_PATH softing.png
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property ANALYZE_HDL TRUE
set_module_property REPORT_TO_TALKBACK false
set_module_property ALLOW_GREYBOX_GENERATION false
set_module_property ELABORATION_CALLBACK elaboration_callback

# -----------------------------------
# Files
# -----------------------------------
add_fileset QUARTUS_SYNTH QUARTUS_SYNTH "" ""
set_fileset_property QUARTUS_SYNTH TOP_LEVEL softing_debug_slave
set_fileset_property QUARTUS_SYNTH ENABLE_RELATIVE_INCLUDE_PATHS false
add_fileset_file ./softing/debug_slave.vhd VHDL PATH debug_slave.vhd

# -----------------------------------
# Parameters
# -----------------------------------
add_parameter CORE_VERSION INTEGER $CORE_VERSION
set_parameter_property CORE_VERSION HDL_PARAMETER false
set_parameter_property CORE_VERSION VISIBLE false
set_parameter_property CORE_VERSION DERIVED true

# -----------------------------------
# display items
# -----------------------------------

# -----------------------------------
# Callbacks
# -----------------------------------
proc elaboration_callback {} {
  global CORE_VERSION

  set_parameter_value CORE_VERSION $CORE_VERSION
}

# -----------------------------------
# connection point clock
# -----------------------------------
add_interface clock clock end
set_interface_property clock clockRate 0
set_interface_property clock ENABLED true

add_interface_port clock csi_clk clk Input 1

# -----------------------------------
# connection point reset
# -----------------------------------
add_interface reset reset end
set_interface_property reset associatedClock clock
set_interface_property reset synchronousEdges DEASSERT
set_interface_property reset ENABLED true

add_interface_port reset csi_reset_n reset_n Input 1

# -----------------------------------
# connection point irq_output
# -----------------------------------
add_interface irq_output interrupt sender
set_interface_property irq_output associatedAddressablePoint register
set_interface_property irq_output associatedClock clock
set_interface_property irq_output associatedReset reset
set_interface_property irq_output ENABLED true

add_interface_port irq_output ins_out_irq irq Output 1

# -----------------------------------
# connection point default
# -----------------------------------
add_interface default avalon slave
set_interface_property default addressAlignment DYNAMIC
set_interface_property default addressUnits WORDS
set_interface_property default associatedClock clock
set_interface_property default burstOnBurstBoundariesOnly false
set_interface_property default explicitAddressSpan 0
set_interface_property default holdTime 0
set_interface_property default isMemoryDevice false
set_interface_property default isNonVolatileStorage false
set_interface_property default linewrapBursts false
set_interface_property default maximumPendingReadTransactions 0
set_interface_property default printableDevice false
set_interface_property default readLatency 0
set_interface_property default readWaitTime 2
set_interface_property default setupTime 0
set_interface_property default timingUnits Cycles
set_interface_property default writeWaitTime 2
set_interface_property default ENABLED true

add_interface_port default avs_default_read read Input 1
add_interface_port default avs_default_write write Input 1
add_interface_port default avs_default_address address Input 4
add_interface_port default avs_default_readdata readdata Output 32
add_interface_port default avs_default_writedata writedata Input 32
add_interface_port default avs_default_byteenable byteenable Input 4

# -----------------------------------
# connection point register
# -----------------------------------
add_interface register avalon slave
set_interface_property register addressAlignment DYNAMIC
set_interface_property register addressUnits WORDS
set_interface_property register associatedClock clock
set_interface_property register burstOnBurstBoundariesOnly false
set_interface_property register explicitAddressSpan 0
set_interface_property register holdTime 0
set_interface_property register isMemoryDevice false
set_interface_property register isNonVolatileStorage false
set_interface_property register linewrapBursts false
set_interface_property register maximumPendingReadTransactions 0
set_interface_property register printableDevice false
set_interface_property register readLatency 0
set_interface_property register readWaitTime 2
set_interface_property register setupTime 0
set_interface_property register timingUnits Cycles
set_interface_property register writeWaitTime 2
set_interface_property register ENABLED true

add_interface_port register avs_register_read read Input 1
add_interface_port register avs_register_write write Input 1
add_interface_port register avs_register_address address Input 2
add_interface_port register avs_register_readdata readdata Output 32
add_interface_port register avs_register_writedata writedata Input 32
