#*****************************************************************************
#
# SOFTING Industrial Automation GmbH
# Richard-Reitzner-Allee 6
# D-85540 Haar
# Phone: ++49-89-4 56 56-0
# Fax: ++49-89-4 56 56-3 99
# http://www.softing.com
#
# Copyright (C) SOFTING Industrial Automation GmbH 2011-2014. All Rights Reserved.
#
# Version: 1.00.00
#
#*****************************************************************************

# -----------------------------------
# request TCL package from ACDS 13.1
# -----------------------------------
package require -exact qsys 13.1

# -----------------------------------
# Module parameter
# -----------------------------------
set_module_property NAME softing_pio
set_module_property VERSION 1.01.00
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property GROUP "Softing/Industrial Automation/Utilities"
set_module_property AUTHOR "Softing Industrial Automation GmbH"
set_module_property DISPLAY_NAME "CPU PIO"
set_module_property DESCRIPTION "General purpose Input/Output core"
set_module_property ICON_PATH softing.png
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property ANALYZE_HDL TRUE
set_module_property REPORT_TO_TALKBACK false
set_module_property ALLOW_GREYBOX_GENERATION false
set_module_property ELABORATION_CALLBACK elaboration_callback

# -----------------------------------
# Files
# -----------------------------------
add_fileset QUARTUS_SYNTH QUARTUS_SYNTH "" ""
set_fileset_property QUARTUS_SYNTH TOP_LEVEL softing_pio
set_fileset_property QUARTUS_SYNTH ENABLE_RELATIVE_INCLUDE_PATHS false
add_fileset_file ./softing/pio_core.vhd VHDL PATH pio_core.vhd

# -----------------------------------
# Parameters
# -----------------------------------
add_parameter PIO_DATA_WIDTH NATURAL 1
set_parameter_property PIO_DATA_WIDTH DEFAULT_VALUE 1
set_parameter_property PIO_DATA_WIDTH DISPLAY_NAME "Data width"
set_parameter_property PIO_DATA_WIDTH TYPE NATURAL
set_parameter_property PIO_DATA_WIDTH UNITS BITS
set_parameter_property PIO_DATA_WIDTH ALLOWED_RANGES 1:32
set_parameter_property PIO_DATA_WIDTH HDL_PARAMETER true

add_parameter PIO_DIRECTION STRING Input
set_parameter_property PIO_DIRECTION DEFAULT_VALUE Input
set_parameter_property PIO_DIRECTION DISPLAY_NAME "Direction"
set_parameter_property PIO_DIRECTION TYPE STRING
set_parameter_property PIO_DIRECTION ALLOWED_RANGES {Input Output}
set_parameter_property PIO_DIRECTION HDL_PARAMETER true

add_parameter EXT_ENABLE BOOLEAN false
set_parameter_property EXT_ENABLE DEFAULT_VALUE false
set_parameter_property EXT_ENABLE DESCRIPTION "Enables an external interface to clear the pending interrupts."
set_parameter_property EXT_ENABLE DISPLAY_NAME "External interface"
set_parameter_property EXT_ENABLE TYPE BOOLEAN
set_parameter_property EXT_ENABLE HDL_PARAMETER false

# -----------------------------------
# display items
# -----------------------------------

# -----------------------------------
# Callbacks
# -----------------------------------
proc elaboration_callback {} {
  set DataWidth [get_parameter_value PIO_DATA_WIDTH]
  set Direction [get_parameter_value PIO_DIRECTION]
  set ExternalInt [get_parameter_value EXT_ENABLE]

  if {[string compare $Direction "Input"] == 0} {
    add_interface_port data coe_data_input export Input $DataWidth
    set_interface_property irq_output ENABLED true
  } else {
    add_interface_port data coe_data_output export Output $DataWidth
    
    set_interface_property irq_output ENABLED false
    set_port_property ins_out_irq termination true
  }

  if {[string compare $ExternalInt "true"] == 0} {
    set_interface_property ext ENABLED true
    
    set_port_property coe_ext_intreq WIDTH_EXPR $DataWidth
    set_port_property coe_ext_intclear WIDTH_EXPR $DataWidth
  } else {
    set_interface_property ext ENABLED false
    
    set_port_property coe_ext_intreq termination true
    set_port_property coe_ext_intclear termination_value 0
    set_port_property coe_ext_intclear termination true
  }

  # add system.h macros 
  set_module_assignment embeddedsw.CMacro.DATA_WIDTH $DataWidth
  set_module_assignment embeddedsw.CMacro.DIRECTION $Direction
  set_module_assignment embeddedsw.CMacro.EXTINT $ExternalInt
}

# -----------------------------------
# connection point clock
# -----------------------------------
add_interface clock clock end
set_interface_property clock clockRate 0
set_interface_property clock ENABLED true

add_interface_port clock csi_clk clk Input 1

# -----------------------------------
# connection point clock_reset
# -----------------------------------
add_interface clock_reset reset end
set_interface_property clock_reset associatedClock clock
set_interface_property clock_reset synchronousEdges DEASSERT
set_interface_property clock_reset ENABLED true

add_interface_port clock_reset csi_reset_n reset_n Input 1

# -----------------------------------
# connection point irq_output
# -----------------------------------
add_interface irq_output interrupt sender
set_interface_property irq_output associatedAddressablePoint register
set_interface_property irq_output associatedClock clock
set_interface_property irq_output associatedReset clock_reset
# done by the evaluation callback
#set_interface_property irq_output ENABLED true

add_interface_port irq_output ins_out_irq irq Output 1

# -----------------------------------
# connection point register
# -----------------------------------
add_interface register avalon end
set_interface_property register addressAlignment DYNAMIC
set_interface_property register addressUnits WORDS
set_interface_property register associatedClock clock
set_interface_property register burstOnBurstBoundariesOnly false
set_interface_property register explicitAddressSpan 0
set_interface_property register holdTime 0
set_interface_property register isMemoryDevice false
set_interface_property register isNonVolatileStorage false
set_interface_property register linewrapBursts false
set_interface_property register maximumPendingReadTransactions 0
set_interface_property register printableDevice false
set_interface_property register readLatency 0
set_interface_property register readWaitTime 1
set_interface_property register setupTime 0
set_interface_property register timingUnits Cycles
set_interface_property register writeWaitTime 0
set_interface_property register ENABLED true

add_interface_port register avs_register_read read Input 1
add_interface_port register avs_register_write write Input 1
add_interface_port register avs_register_address address Input 3
add_interface_port register avs_register_readdata readdata Output 32
add_interface_port register avs_register_writedata writedata Input 32

# -----------------------------------
# connection point data
# -----------------------------------
add_interface data conduit end
set_interface_property data ENABLED true

# done by the evaluation callback
# add_interface_port data coe_data_input export Input 1
# add_interface_port data coe_data_output export Output 1

# -----------------------------------
# connection point ext
# -----------------------------------
add_interface ext conduit end
set_interface_property ext ENABLED true

add_interface_port ext coe_ext_intreq export Output 1
add_interface_port ext coe_ext_intclear export Input 1
