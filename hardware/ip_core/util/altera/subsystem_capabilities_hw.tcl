#*****************************************************************************
#
# SOFTING Industrial Automation GmbH
# Richard-Reitzner-Allee 6
# D-85540 Haar
# Phone: ++49-89-4 56 56-0
# Fax: ++49-89-4 56 56-3 99
# http://www.softing.com
#
# Copyright (C) SOFTING Industrial Automation GmbH 2011-2014. All Rights Reserved.
#
# Version: 1.00.00
#
#*****************************************************************************

# -----------------------------------
# request TCL package from ACDS 13.1
# -----------------------------------
package require -exact qsys 13.1

# -----------------------------------
# Module parameter
# -----------------------------------
set MAJOR 1
set MINOR 0
set REVISION 0
set CORE_VERSION [expr {($MAJOR << 24) + ($MINOR << 16) + $REVISION}]
set_module_property NAME system_capabilities
set_module_property VERSION $MAJOR.$MINOR.$REVISION
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property GROUP "Softing/Industrial Automation/Utilities"
set_module_property AUTHOR "Softing Industrial Automation GmbH"
set_module_property DISPLAY_NAME "Fieldbus subsystem capabilities"
set_module_property DESCRIPTION "Provides a register describing the hardware capabilities of the subsystem."
set_module_property ICON_PATH softing.png
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property ANALYZE_HDL TRUE
set_module_property REPORT_TO_TALKBACK false
set_module_property ALLOW_GREYBOX_GENERATION false
set_module_property ELABORATION_CALLBACK elaboration_callback

# -----------------------------------
# Files
# -----------------------------------
add_fileset QUARTUS_SYNTH QUARTUS_SYNTH "" ""
set_fileset_property QUARTUS_SYNTH TOP_LEVEL system_capabilities
set_fileset_property QUARTUS_SYNTH ENABLE_RELATIVE_INCLUDE_PATHS false
add_fileset_file fieldbus_subsystem_capabilities.vhd VHDL PATH subsystem_capabilities.vhd

# -----------------------------------
# Parameters
# -----------------------------------
add_parameter CFI_INTEL BOOLEAN false
set_parameter_property CFI_INTEL DEFAULT_VALUE false
set_parameter_property CFI_INTEL DISPLAY_NAME "CFI Intel-Strata flash"
set_parameter_property CFI_INTEL TYPE BOOLEAN
set_parameter_property CFI_INTEL DESCRIPTION "Hardware uses an Intel-Strata flash."
set_parameter_property CFI_INTEL HDL_PARAMETER false

add_parameter CFI_AMD BOOLEAN false
set_parameter_property CFI_AMD DEFAULT_VALUE false
set_parameter_property CFI_AMD DISPLAY_NAME "CFI AMD AM29XXXXX flash"
set_parameter_property CFI_AMD TYPE BOOLEAN
set_parameter_property CFI_AMD DESCRIPTION "Hardware uses an AMD AM29XXXXX flash."
set_parameter_property CFI_AMD HDL_PARAMETER false

add_parameter EPCS_FLASH BOOLEAN false
set_parameter_property EPCS_FLASH DEFAULT_VALUE false
set_parameter_property EPCS_FLASH DISPLAY_NAME "EPCS flash"
set_parameter_property EPCS_FLASH TYPE BOOLEAN
set_parameter_property EPCS_FLASH DESCRIPTION "Hardware uses an EPCS flash."
set_parameter_property EPCS_FLASH HDL_PARAMETER false

add_parameter VIRTUAL_FLASH BOOLEAN false
set_parameter_property VIRTUAL_FLASH DEFAULT_VALUE false
set_parameter_property VIRTUAL_FLASH DISPLAY_NAME "Virtual flash"
set_parameter_property VIRTUAL_FLASH TYPE BOOLEAN
set_parameter_property VIRTUAL_FLASH DESCRIPTION "Hardware does not has a flash."
set_parameter_property VIRTUAL_FLASH HDL_PARAMETER false

add_parameter RTE_SWITCH BOOLEAN false
set_parameter_property RTE_SWITCH DEFAULT_VALUE false
set_parameter_property RTE_SWITCH DISPLAY_NAME "Softing IE switch core"
set_parameter_property RTE_SWITCH TYPE BOOLEAN
set_parameter_property RTE_SWITCH DESCRIPTION "Hardware uses the Softing Industrial Switch core."
set_parameter_property RTE_SWITCH HDL_PARAMETER false

add_parameter RTE_HUB BOOLEAN false
set_parameter_property RTE_HUB DEFAULT_VALUE false
set_parameter_property RTE_HUB DISPLAY_NAME "Softing IE powerlink core"
set_parameter_property RTE_HUB TYPE BOOLEAN
set_parameter_property RTE_HUB DESCRIPTION "Hardware uses the Softing Industrial Powerlink core."
set_parameter_property RTE_HUB HDL_PARAMETER false

add_parameter RTE_ESC BOOLEAN false
set_parameter_property RTE_ESC DEFAULT_VALUE false
set_parameter_property RTE_ESC DISPLAY_NAME "EtherCAT slave controller"
set_parameter_property RTE_ESC TYPE BOOLEAN
set_parameter_property RTE_ESC DESCRIPTION "Hardware uses the EtherCAT slave controller core."
set_parameter_property RTE_ESC HDL_PARAMETER false

add_parameter PB_SLAVE BOOLEAN false
set_parameter_property PB_SLAVE DEFAULT_VALUE false
set_parameter_property PB_SLAVE DISPLAY_NAME "PROFIBUS slave"
set_parameter_property PB_SLAVE TYPE BOOLEAN
set_parameter_property PB_SLAVE DESCRIPTION "Hardware uses the PROFIBUS slave core."
set_parameter_property PB_SLAVE HDL_PARAMETER false

add_parameter PB_MASTER BOOLEAN false
set_parameter_property PB_MASTER DEFAULT_VALUE false
set_parameter_property PB_MASTER DISPLAY_NAME "PROFIBUS master"
set_parameter_property PB_MASTER TYPE BOOLEAN
set_parameter_property PB_MASTER DESCRIPTION "Hardware uses the PROFIBUS master core."
set_parameter_property PB_MASTER HDL_PARAMETER false

add_parameter PHY_INT BOOLEAN false
set_parameter_property PHY_INT DEFAULT_VALUE false
set_parameter_property PHY_INT DISPLAY_NAME "Interrupt support"
set_parameter_property PHY_INT TYPE BOOLEAN
set_parameter_property PHY_INT DESCRIPTION "PHY support interrupts."
set_parameter_property PHY_INT HDL_PARAMETER false

add_parameter PHY_FIBRE_OPTIC_SUPPORT BOOLEAN false
set_parameter_property PHY_FIBRE_OPTIC_SUPPORT DEFAULT_VALUE false
set_parameter_property PHY_FIBRE_OPTIC_SUPPORT DISPLAY_NAME "Fibre optic support"
set_parameter_property PHY_FIBRE_OPTIC_SUPPORT TYPE BOOLEAN
set_parameter_property PHY_FIBRE_OPTIC_SUPPORT DESCRIPTION "Enables the fibre optic support of the PHY driver. Autonegotiation not supported."
set_parameter_property PHY_FIBRE_OPTIC_SUPPORT HDL_PARAMETER false

add_parameter PERF_OPT BOOLEAN false
set_parameter_property PERF_OPT DEFAULT_VALUE false
set_parameter_property PERF_OPT DISPLAY_NAME "Performance optimization"
set_parameter_property PERF_OPT TYPE BOOLEAN
set_parameter_property PERF_OPT DESCRIPTION "Hardware uses the Performance optimization core."
set_parameter_property PERF_OPT HDL_PARAMETER false

add_parameter EXT_TO_AVM BOOLEAN false
set_parameter_property EXT_TO_AVM DEFAULT_VALUE false
set_parameter_property EXT_TO_AVM TYPE BOOLEAN
set_parameter_property EXT_TO_AVM VISIBLE false
set_parameter_property EXT_TO_AVM HDL_PARAMETER false
set_parameter_property EXT_TO_AVM DERIVED true

add_parameter APPLICATION_CPU STRING "internal"
set_parameter_property APPLICATION_CPU DISPLAY_NAME "Application CPU"
set_parameter_property APPLICATION_CPU UNITS None
set_parameter_property APPLICATION_CPU ALLOWED_RANGES {"internal" "external" "single"}
set_parameter_property APPLICATION_CPU HDL_PARAMETER false

add_parameter SYSTEM_CONSOLE_SUPPORT BOOLEAN false
set_parameter_property SYSTEM_CONSOLE_SUPPORT DEFAULT_VALUE false
set_parameter_property SYSTEM_CONSOLE_SUPPORT DISPLAY_NAME "System-Console support"
set_parameter_property SYSTEM_CONSOLE_SUPPORT TYPE BOOLEAN
set_parameter_property SYSTEM_CONSOLE_SUPPORT DESCRIPTION "Enables access to subsystem memory over system console (dangerous)."
set_parameter_property SYSTEM_CONSOLE_SUPPORT HDL_PARAMETER false

add_parameter PROFINET_IRT_SUPPORT BOOLEAN true
set_parameter_property PROFINET_IRT_SUPPORT DEFAULT_VALUE true
set_parameter_property PROFINET_IRT_SUPPORT DISPLAY_NAME "PROFINET IRT support"
set_parameter_property PROFINET_IRT_SUPPORT TYPE BOOLEAN
set_parameter_property PROFINET_IRT_SUPPORT DESCRIPTION "Enables PROFINET IRT support and consumer/provider implementation in logic."
set_parameter_property PROFINET_IRT_SUPPORT HDL_PARAMETER false

add_parameter RTE_SWITCH_EXTERNAL_PORTS integer 2
set_parameter_property RTE_SWITCH_EXTERNAL_PORTS DEFAULT_VALUE 2
set_parameter_property RTE_SWITCH_EXTERNAL_PORTS DISPLAY_NAME "Number of external switch ports"
set_parameter_property RTE_SWITCH_EXTERNAL_PORTS TYPE INTEGER
set_parameter_property RTE_SWITCH_EXTERNAL_PORTS ALLOWED_RANGES { 2 3 4 5 }
set_parameter_property RTE_SWITCH_EXTERNAL_PORTS DESCRIPTION "Number of MAC interfaces of the RTE-Switch. The MAC can be connected to a PHY or directly to another MAC (Chip or PCB internal network)."
set_parameter_property RTE_SWITCH_EXTERNAL_PORTS HDL_PARAMETER false

add_parameter CORE_VERSION INTEGER $CORE_VERSION
set_parameter_property CORE_VERSION HDL_PARAMETER true
set_parameter_property CORE_VERSION VISIBLE false
set_parameter_property CORE_VERSION DERIVED true

add_parameter CAPABILITIES_0 INTEGER 0
set_parameter_property CAPABILITIES_0 UNITS None
set_parameter_property CAPABILITIES_0 HDL_PARAMETER true
set_parameter_property CAPABILITIES_0 VISIBLE false
set_parameter_property CAPABILITIES_0 DERIVED true

add_parameter CAPABILITIES_1 INTEGER 0
set_parameter_property CAPABILITIES_1 UNITS None
set_parameter_property CAPABILITIES_1 HDL_PARAMETER true
set_parameter_property CAPABILITIES_1 VISIBLE false
set_parameter_property CAPABILITIES_1 DERIVED true

add_parameter CAPABILITIES_2 INTEGER 0
set_parameter_property CAPABILITIES_2 UNITS None
set_parameter_property CAPABILITIES_2 HDL_PARAMETER true
set_parameter_property CAPABILITIES_2 VISIBLE false
set_parameter_property CAPABILITIES_2 DERIVED true

# -----------------------------------
# display items
# -----------------------------------
add_display_item "" "Flash settings"               group tab
add_display_item "" "Fieldbus controller settings" group tab
add_display_item "" "MAC / PHY settings"           group tab
add_display_item "" "Other settings"               group tab

add_display_item "Flash settings" CFI_INTEL     parameter
add_display_item "Flash settings" CFI_AMD       parameter
add_display_item "Flash settings" EPCS_FLASH    parameter
add_display_item "Flash settings" VIRTUAL_FLASH parameter

add_display_item "Fieldbus controller settings" RTE_SWITCH parameter
add_display_item "Fieldbus controller settings" PROFINET_IRT_SUPPORT parameter
set_display_item_property PROFINET_IRT_SUPPORT VISIBLE false

add_display_item "Fieldbus controller settings" RTE_HUB    parameter
add_display_item "Fieldbus controller settings" RTE_ESC    parameter
add_display_item "Fieldbus controller settings" PB_SLAVE   parameter
add_display_item "Fieldbus controller settings" PB_MASTER  parameter

add_display_item "MAC / PHY settings" RTE_SWITCH_EXTERNAL_PORTS parameter
add_display_item "MAC / PHY settings" PHY_INT parameter
add_display_item "MAC / PHY settings" PHY_FIBRE_OPTIC_SUPPORT parameter

add_display_item "Other settings" PERF_OPT parameter
add_display_item "Other settings" APPLICATION_CPU parameter
add_display_item "Other settings" SYSTEM_CONSOLE_SUPPORT parameter

# -----------------------------------
# Callbacks
# -----------------------------------
proc elaboration_callback {} {
  global CORE_VERSION

  set_parameter_value CORE_VERSION $CORE_VERSION

  #----------------------------------------------------------------------------

  set Capabilities_0 0

  if {[string compare [get_parameter_value CFI_INTEL] "true"] == 0} {
    set Capabilities_0 [expr {$Capabilities_0 + 0x00000001}]
  }

  if {[string compare [get_parameter_value CFI_AMD] "true"] == 0} {
    set Capabilities_0 [expr {$Capabilities_0 + 0x00000002}]
  }

  if {[string compare [get_parameter_value EPCS_FLASH] "true"] == 0} {
    set Capabilities_0 [expr {$Capabilities_0 + 0x00000004}]
  }

  if {[string compare [get_parameter_value VIRTUAL_FLASH] "true"] == 0} {
    set Capabilities_0 [expr {$Capabilities_0 + 0x00000008}]
  }

  if {[string compare [get_parameter_value RTE_SWITCH] "true"] == 0} {
    set Capabilities_0 [expr {$Capabilities_0 + 0x00000010}]

    set_display_item_property PROFINET_IRT_SUPPORT VISIBLE true
  } else {
    set_display_item_property PROFINET_IRT_SUPPORT VISIBLE false
  }

  if {[string compare [get_parameter_value RTE_HUB] "true"] == 0} {
    set Capabilities_0 [expr {$Capabilities_0 + 0x00000020}]
  }

  if {[string compare [get_parameter_value RTE_ESC] "true"] == 0} {
    set Capabilities_0 [expr {$Capabilities_0 + 0x00000040}]
  }

  if {[string compare [get_parameter_value PB_SLAVE] "true"] == 0} {
    set Capabilities_0 [expr {$Capabilities_0 + 0x00000080}]
  }

  if {[string compare [get_parameter_value PB_MASTER] "true"] == 0} {
    set Capabilities_0 [expr {$Capabilities_0 + 0x00000100}]
  }

  if {[string compare [get_parameter_value PERF_OPT] "true"] == 0} {
    set Capabilities_0 [expr {$Capabilities_0 + 0x00000200}]
  }

  if {[string compare [get_parameter_value PHY_INT] "true"] == 0} {
    set Capabilities_0 [expr {$Capabilities_0 + 0x00000400}]
  }

  if {[string compare [get_parameter_value APPLICATION_CPU] "external"] == 0} {
    set_parameter_value EXT_TO_AVM true

    set Capabilities_0 [expr {$Capabilities_0 + 0x00000800}]
  } elseif {[string compare [get_parameter_value APPLICATION_CPU] "single"] == 0} {
    set Capabilities_0 [expr {$Capabilities_0 + 0x00001000}]
  }

  if {[string compare [get_parameter_value SYSTEM_CONSOLE_SUPPORT] "true"] == 0} {
    set Capabilities_0 [expr {$Capabilities_0 + 0x00002000}]
  }

  if {[string compare [get_parameter_value PROFINET_IRT_SUPPORT] "false"] == 0} {
    set Capabilities_0 [expr {$Capabilities_0 + 0x00004000}]
  }

  if {[string compare [get_parameter_value PHY_FIBRE_OPTIC_SUPPORT ] "true"] == 0} {
    set Capabilities_0 [expr {$Capabilities_0 + 0x00008000}]
  }

  if {[get_parameter_value RTE_SWITCH_EXTERNAL_PORTS] == 2} {
    set Capabilities_0 [expr {$Capabilities_0 + 0x00020000}]
  } elseif {[get_parameter_value RTE_SWITCH_EXTERNAL_PORTS] == 3} {
    set Capabilities_0 [expr {$Capabilities_0 + 0x00030000}]
  } elseif {[get_parameter_value RTE_SWITCH_EXTERNAL_PORTS] == 4} {
    set Capabilities_0 [expr {$Capabilities_0 + 0x00040000}]
  } elseif {[get_parameter_value RTE_SWITCH_EXTERNAL_PORTS] == 5} {
    set Capabilities_0 [expr {$Capabilities_0 + 0x00050000}]
  }

  set_parameter_value CAPABILITIES_0 $Capabilities_0

  set_module_assignment embeddedsw.CMacro.CFI_INTEL_ENABLED             0x00000001
  set_module_assignment embeddedsw.CMacro.CFI_AMD_ENABLED               0x00000002
  set_module_assignment embeddedsw.CMacro.EPCS_FLASH_ENABLED            0x00000004
  set_module_assignment embeddedsw.CMacro.VIRTUAL_FLASH_ENABLED         0x00000008
  set_module_assignment embeddedsw.CMacro.RTE_SWITCH_ENABLED            0x00000010
  set_module_assignment embeddedsw.CMacro.RTE_HUB_ENABLED               0x00000020
  set_module_assignment embeddedsw.CMacro.RTE_ESC_ENABLED               0x00000040
  set_module_assignment embeddedsw.CMacro.PB_SLAVE_ENABLED              0x00000080
  set_module_assignment embeddedsw.CMacro.PB_MASTER_ENABLED             0x00000100
  set_module_assignment embeddedsw.CMacro.PERF_OPT_ENABLED              0x00000200
  set_module_assignment embeddedsw.CMacro.PHY_INT_ENABLED               0x00000400
  set_module_assignment embeddedsw.CMacro.EXT_TO_AVM_ENABLED            0x00000800
  set_module_assignment embeddedsw.CMacro.SINGLE_APPL_CPU_ENABLED       0x00001000
  set_module_assignment embeddedsw.CMacro.SYSTEM_CONSOLE_SUPPORTED      0x00002000
  set_module_assignment embeddedsw.CMacro.PROFINET_IRT_DISABLE          0x00004000
  set_module_assignment embeddedsw.CMacro.PHY_FIBRE_OPTIC_ENABLED       0x00008000
  set_module_assignment embeddedsw.CMacro.RTE_SWITCH_PORT_NUMBER_MASK   0x000F0000
  set_module_assignment embeddedsw.CMacro.RTE_SWITCH_PORT_MASK_INDEX    16

  #----------------------------------------------------------------------------

  set Capabilities_1 0

  set_parameter_value CAPABILITIES_1 $Capabilities_1

  #----------------------------------------------------------------------------

  set Capabilities_2 0

  set_parameter_value CAPABILITIES_2 $Capabilities_2
}

# -----------------------------------
# connection point reset
# -----------------------------------
add_interface reset reset end
set_interface_property reset associatedClock clock
set_interface_property reset synchronousEdges DEASSERT
set_interface_property reset ENABLED true

add_interface_port reset csi_reset_n reset_n Input 1

# -----------------------------------
# connection point clock
# -----------------------------------
add_interface clock clock end
set_interface_property clock clockRate 0
set_interface_property clock ENABLED true

add_interface_port clock csi_clk clk Input 1

# -----------------------------------
# connection point control
# -----------------------------------
add_interface status avalon end
set_interface_property status addressAlignment DYNAMIC
set_interface_property status addressUnits WORDS
set_interface_property status associatedClock clock
set_interface_property status burstOnBurstBoundariesOnly false
set_interface_property status explicitAddressSpan 0
set_interface_property status holdTime 0
set_interface_property status isMemoryDevice false
set_interface_property status isNonVolatileStorage false
set_interface_property status linewrapBursts false
set_interface_property status maximumPendingReadTransactions 0
set_interface_property status printableDevice false
set_interface_property status readLatency 0
set_interface_property status readWaitTime 1
set_interface_property status setupTime 0
set_interface_property status timingUnits Cycles
set_interface_property status writeWaitTime 0
set_interface_property status ENABLED true

add_interface_port status avs_status_read read Input 1
add_interface_port status avs_status_address address Input 2
add_interface_port status avs_status_readdata readdata Output 32

