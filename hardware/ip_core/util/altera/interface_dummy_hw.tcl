#*****************************************************************************
#
# SOFTING Industrial Automation GmbH
# Richard-Reitzner-Allee 6
# D-85540 Haar
# Phone: ++49-89-4 56 56-0
# Fax: ++49-89-4 56 56-3 99
# http://www.softing.com
#
# Copyright (C) SOFTING Industrial Automation GmbH 2011-2014. All Rights Reserved.
#
# Version: 1.00.00
#
#*****************************************************************************

# -----------------------------------
# request TCL package from ACDS 13.1
# -----------------------------------
package require -exact qsys 13.1

# -----------------------------------
# Module parameter
# -----------------------------------
set MAJOR 1
set MINOR 0
set REVISION 0
set CORE_VERSION [expr {($MAJOR << 24) + ($MINOR << 16) + $REVISION}]
set_module_property NAME softing_interface_dummy
set_module_property VERSION $MAJOR.$MINOR.$REVISION
set_module_property INTERNAL true
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property GROUP "Softing/Industrial Automation/Utilities"
set_module_property AUTHOR "Softing Industrial Automation GmbH"
set_module_property DISPLAY_NAME "Interface Dummy"
set_module_property DESCRIPTION "---"
set_module_property ICON_PATH softing.png
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property ANALYZE_HDL false
set_module_property REPORT_TO_TALKBACK false
set_module_property ALLOW_GREYBOX_GENERATION false
set_module_property ELABORATION_CALLBACK elaborate

# -----------------------------------
# Files
# -----------------------------------

# -----------------------------------
# Parameters
# -----------------------------------

add_parameter NUMBER_DUMMY_MASTER NATURAL 0
set_parameter_property NUMBER_DUMMY_MASTER DEFAULT_VALUE 0
set_parameter_property NUMBER_DUMMY_MASTER TYPE NATURAL
set_parameter_property NUMBER_DUMMY_MASTER UNITS None
set_parameter_property NUMBER_DUMMY_MASTER ALLOWED_RANGES {0:32}
set_parameter_property NUMBER_DUMMY_MASTER VISIBLE false

add_parameter NUMBER_DUMMY_SLAVE NATURAL 0
set_parameter_property NUMBER_DUMMY_SLAVE DEFAULT_VALUE 0
set_parameter_property NUMBER_DUMMY_SLAVE TYPE NATURAL
set_parameter_property NUMBER_DUMMY_SLAVE UNITS None
set_parameter_property NUMBER_DUMMY_SLAVE ALLOWED_RANGES {0:32}
set_parameter_property NUMBER_DUMMY_SLAVE VISIBLE false

add_parameter NUMBER_DUMMY_SLAVE_ADDR_WIDTH NATURAL 13
set_parameter_property NUMBER_DUMMY_SLAVE_ADDR_WIDTH DEFAULT_VALUE 13
set_parameter_property NUMBER_DUMMY_SLAVE_ADDR_WIDTH TYPE NATURAL
set_parameter_property NUMBER_DUMMY_SLAVE_ADDR_WIDTH UNITS None
set_parameter_property NUMBER_DUMMY_SLAVE_ADDR_WIDTH ALLOWED_RANGES {2:28}
set_parameter_property NUMBER_DUMMY_SLAVE_ADDR_WIDTH VISIBLE false

add_parameter NUMBER_DUMMY_IRQRX NATURAL 0
set_parameter_property NUMBER_DUMMY_IRQRX DEFAULT_VALUE 0
set_parameter_property NUMBER_DUMMY_IRQRX TYPE NATURAL
set_parameter_property NUMBER_DUMMY_IRQRX UNITS None
set_parameter_property NUMBER_DUMMY_IRQRX ALLOWED_RANGES {0:32}
set_parameter_property NUMBER_DUMMY_IRQRX VISIBLE false

add_parameter NUMBER_DUMMY_IRQTX NATURAL 0
set_parameter_property NUMBER_DUMMY_IRQTX DEFAULT_VALUE 0
set_parameter_property NUMBER_DUMMY_IRQTX TYPE NATURAL
set_parameter_property NUMBER_DUMMY_IRQTX UNITS None
set_parameter_property NUMBER_DUMMY_IRQTX ALLOWED_RANGES {0:32}
set_parameter_property NUMBER_DUMMY_IRQTX VISIBLE false

add_parameter NUMBER_DUMMY_CONDUIT NATURAL 0
set_parameter_property NUMBER_DUMMY_CONDUIT DEFAULT_VALUE 0
set_parameter_property NUMBER_DUMMY_CONDUIT TYPE NATURAL
set_parameter_property NUMBER_DUMMY_CONDUIT UNITS None
set_parameter_property NUMBER_DUMMY_CONDUIT ALLOWED_RANGES {0:32}
set_parameter_property NUMBER_DUMMY_CONDUIT VISIBLE false

add_parameter INTERRUPT_LOOP_DUMMY BOOLEAN false
set_parameter_property INTERRUPT_LOOP_DUMMY DEFAULT_VALUE false
set_parameter_property INTERRUPT_LOOP_DUMMY TYPE BOOLEAN
set_parameter_property INTERRUPT_LOOP_DUMMY UNITS None
set_parameter_property INTERRUPT_LOOP_DUMMY VISIBLE false

# -----------------------------------
# display items
# -----------------------------------

# -----------------------------------
# Callbacks
# -----------------------------------
proc elaborate {} {
  set NumberMasters [get_parameter_value NUMBER_DUMMY_MASTER]
  set NumberSlaves [get_parameter_value NUMBER_DUMMY_SLAVE]
  set NumberIrqRx [get_parameter_value NUMBER_DUMMY_IRQRX]
  set NumberIrqTx [get_parameter_value NUMBER_DUMMY_IRQTX]
  set NumberConduit [get_parameter_value NUMBER_DUMMY_CONDUIT]
  set InterruptLoop [get_parameter_value INTERRUPT_LOOP_DUMMY]

  set_port_property csi_out_clk DRIVEN_BY 0
  set_port_property csi_out_reset_n DRIVEN_BY 0

  for { set s 0 } { $s < $NumberMasters } { incr s } {
    add_interface "dummymaster${s}" avalon master
    set_interface_property "dummymaster${s}" addressUnits SYMBOLS
    set_interface_property "dummymaster${s}" associatedClock clock_in
    set_interface_property "dummymaster${s}" burstOnBurstBoundariesOnly false
    set_interface_property "dummymaster${s}" doStreamReads false
    set_interface_property "dummymaster${s}" doStreamWrites false
    set_interface_property "dummymaster${s}" linewrapBursts false
    set_interface_property "dummymaster${s}" readLatency 0
    set_interface_property "dummymaster${s}" ENABLED true

    add_interface_port "dummymaster${s}" "avm_dummymaster_read${s}" read Output 1
    add_interface_port "dummymaster${s}" "avm_dummymaster_write${s}" write Output 1
    add_interface_port "dummymaster${s}" "avm_dummymaster_address${s}" address Output 32
    add_interface_port "dummymaster${s}" "avm_dummymaster_waitrequest${s}" waitrequest Input 1
    add_interface_port "dummymaster${s}" "avm_dummymaster_readdata${s}" readdata Input 32
    add_interface_port "dummymaster${s}" "avm_dummymaster_writedata${s}" writedata Output 32

    set_port_property "avm_dummymaster_read${s}" DRIVEN_BY 0
    set_port_property "avm_dummymaster_write${s}" DRIVEN_BY 0
    set_port_property "avm_dummymaster_address${s}" DRIVEN_BY 0
    set_port_property "avm_dummymaster_waitrequest${s}" DRIVEN_BY 0
    set_port_property "avm_dummymaster_readdata${s}" DRIVEN_BY 0
    set_port_property "avm_dummymaster_writedata${s}" DRIVEN_BY 0 
  }

  for { set s 0 } { $s < $NumberSlaves } { incr s } {
    add_interface "dummyslave${s}" avalon slave
    set_interface_property "dummyslave${s}" addressAlignment DYNAMIC
    set_interface_property "dummyslave${s}" addressUnits WORDS
    set_interface_property "dummyslave${s}" associatedClock clock_in
    set_interface_property "dummyslave${s}" burstOnBurstBoundariesOnly false
    set_interface_property "dummyslave${s}" explicitAddressSpan 0
    set_interface_property "dummyslave${s}" holdTime 0
    set_interface_property "dummyslave${s}" isMemoryDevice false
    set_interface_property "dummyslave${s}" isNonVolatileStorage false
    set_interface_property "dummyslave${s}" linewrapBursts false
    set_interface_property "dummyslave${s}" maximumPendingReadTransactions 0
    set_interface_property "dummyslave${s}" printableDevice false
    set_interface_property "dummyslave${s}" readLatency 0
    set_interface_property "dummyslave${s}" readWaitTime 1
    set_interface_property "dummyslave${s}" setupTime 0
    set_interface_property "dummyslave${s}" timingUnits Cycles
    set_interface_property "dummyslave${s}" writeWaitTime 0
    set_interface_property "dummyslave${s}" ENABLED true

    add_interface_port "dummyslave${s}" "avs_dummyslave_read${s}" read Input 1
    add_interface_port "dummyslave${s}" "avs_dummyslave_write${s}" write Input 1
    add_interface_port "dummyslave${s}" "avs_dummyslave_address${s}" address Input [get_parameter_value NUMBER_DUMMY_SLAVE_ADDR_WIDTH]
    add_interface_port "dummyslave${s}" "avs_dummyslave_readdata${s}" readdata Output 32
    add_interface_port "dummyslave${s}" "avs_dummyslave_writedata${s}" writedata Input 32
    add_interface_port "dummyslave${s}" "avs_dummyslave_waitrequest${s}" waitrequest Output 1
    add_interface_port "dummyslave${s}" "avs_dummyslave_byteenable${s}" byteenable Input 4

    set_port_property "avs_dummyslave_read${s}" DRIVEN_BY 0
    set_port_property "avs_dummyslave_write${s}" DRIVEN_BY 0
    set_port_property "avs_dummyslave_address${s}" DRIVEN_BY 0
    set_port_property "avs_dummyslave_readdata${s}" DRIVEN_BY 0
    set_port_property "avs_dummyslave_writedata${s}" DRIVEN_BY 0
    set_port_property "avs_dummyslave_waitrequest${s}" DRIVEN_BY 0  
  }

  for { set s 0 } { $s < $NumberIrqRx } { incr s } {
    add_interface "dummyirqrx${s}" interrupt receiver
    set_interface_property "dummyirqrx${s}" associatedAddressablePoint dummymaster0
    set_interface_property "dummyirqrx${s}" associatedClock clock_in
    set_interface_property "dummyirqrx${s}" associatedReset clock_reset_in
    set_interface_property "dummyirqrx${s}" ENABLED true

    add_interface_port "dummyirqrx${s}" "ins_input_irq${s}" irq Input 1

    set_port_property "ins_input_irq${s}" DRIVEN_BY 0
  }

  for { set s 0 } { $s < $NumberIrqTx } { incr s } {
    add_interface "dummyirqtx${s}" interrupt sender
    set_interface_property "dummyirqtx${s}" associatedAddressablePoint dummyslave0
    set_interface_property "dummyirqtx${s}" associatedClock clock_in
    set_interface_property "dummyirqtx${s}" associatedReset clock_reset_in
    set_interface_property "dummyirqtx${s}" ENABLED true

    add_interface_port "dummyirqtx${s}" "ins_output_irq${s}" irq Output 1

    set_port_property "ins_output_irq${s}" DRIVEN_BY 0
  } 

  for { set s 0 } { $s < $NumberConduit } { incr s } {
    add_interface "dummyconduit${s}" conduit end
    set_interface_property "dummyconduit${s}" ENABLED true

    add_interface_port "dummyconduit${s}" "coe_dummy${s}" export Output 1
    set_port_property "coe_dummy${s}" DRIVEN_BY 0
  }
  
  if {[string compare $InterruptLoop "true"] == 0} {
    add_interface irqloopin interrupt receiver
    set_interface_property irqloopin associatedAddressablePoint dummymaster0
    set_interface_property irqloopin associatedClock clock_in
    set_interface_property irqloopin associatedReset clock_reset_in
    set_interface_property irqloopin ENABLED true

    add_interface_port irqloopin ins_irqloopin_irq irq Input 1

    add_interface irqloopout interrupt sender
    set_interface_property irqloopout associatedAddressablePoint dummyslave0
    set_interface_property irqloopout associatedClock clock_in
    set_interface_property irqloopout associatedReset clock_reset_in
    set_interface_property irqloopout ENABLED true

    add_interface_port irqloopout ins_irqloopout_irq irq Output 1

    set_port_property ins_irqloopout_irq DRIVEN_BY ins_irqloopin_irq
  }
}

# -----------------------------------
# connection point clock_in
# -----------------------------------
add_interface clock_in clock sink
set_interface_property clock_in clockRate 0
set_interface_property clock_in ENABLED true

add_interface_port clock_in csi_in_clk clk Input 1

# -----------------------------------
# connection point clock_reset_in
# -----------------------------------
add_interface clock_reset_in reset sink
set_interface_property clock_reset_in associatedClock clock_in
set_interface_property clock_reset_in synchronousEdges DEASSERT
set_interface_property clock_reset_in ENABLED true

add_interface_port clock_reset_in csi_in_reset_n reset_n Input 1

# -----------------------------------
# connection point clock_out
# -----------------------------------
add_interface clock_out clock source
set_interface_property clock_out clockRate 0
set_interface_property clock_out ENABLED true
set_interface_property clock_out associatedDirectClock "clock_in"

add_interface_port clock_out csi_out_clk clk Output 1

# -----------------------------------
# connection point clock_reset_out
# -----------------------------------
add_interface clock_reset_out reset source
set_interface_property clock_reset_out associatedClock ""
set_interface_property clock_reset_out synchronousEdges "none"
set_interface_property clock_reset_out associatedResetSinks "clock_reset_in"
set_interface_property clock_reset_out associatedDirectReset "clock_reset_in"
set_interface_property clock_reset_out ENABLED true

add_interface_port clock_reset_out csi_out_reset_n reset_n Output 1
