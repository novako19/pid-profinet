#*****************************************************************************
#
# SOFTING Industrial Automation GmbH
# Richard-Reitzner-Allee 6
# D-85540 Haar
# Phone: ++49-89-4 56 56-0
# Fax: ++49-89-4 56 56-3 99
# http://www.softing.com
#
# Copyright (C) SOFTING Industrial Automation GmbH 2011-2014. All Rights Reserved.
#
# Version: 1.00.00
#
#*****************************************************************************

# -----------------------------------
# request TCL package from ACDS 13.1
# -----------------------------------
package require -exact qsys 13.1

# -----------------------------------
# Module parameter
# -----------------------------------
set MAJOR 1
set MINOR 0
set REVISION 8411
set CORE_VERSION [expr {($MAJOR << 24) + ($MINOR << 16) + $REVISION}]
set_module_property NAME softing_test_pattern
set_module_property VERSION $MAJOR.$MINOR.$REVISION
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property GROUP "Softing/Industrial Automation/Utilities"
set_module_property AUTHOR "Softing Industrial Automation GmbH"
set_module_property DISPLAY_NAME "Test pattern generator"
set_module_property DESCRIPTION "Can be used as generator of data test patterns."
set_module_property ICON_PATH softing.png
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property ANALYZE_HDL TRUE
set_module_property REPORT_TO_TALKBACK false
set_module_property ALLOW_GREYBOX_GENERATION false
set_module_property ELABORATION_CALLBACK elaboration_callback

# -----------------------------------
# Files
# -----------------------------------
add_fileset QUARTUS_SYNTH QUARTUS_SYNTH "" ""
set_fileset_property QUARTUS_SYNTH TOP_LEVEL softing_test_pattern
set_fileset_property QUARTUS_SYNTH ENABLE_RELATIVE_INCLUDE_PATHS false


# -----------------------------------
# Parameters
# -----------------------------------
add_parameter CORE_VERSION INTEGER $CORE_VERSION
set_parameter_property CORE_VERSION HDL_PARAMETER true
set_parameter_property CORE_VERSION VISIBLE false
set_parameter_property CORE_VERSION DERIVED true

add_parameter ADDRESS_WIDTH INTEGER 2
set_parameter_property ADDRESS_WIDTH HDL_PARAMETER true
set_parameter_property ADDRESS_WIDTH VISIBLE false
set_parameter_property ADDRESS_WIDTH DERIVED true

add_parameter DATA_SIZE NATURAL 4
set_parameter_property DATA_SIZE DEFAULT_VALUE 4
set_parameter_property DATA_SIZE DISPLAY_NAME "Test pattern data size"
set_parameter_property DATA_SIZE DESCRIPTION "The data size of the test pattern in bytes."
set_parameter_property DATA_SIZE TYPE NATURAL
set_parameter_property DATA_SIZE ALLOWED_RANGES "4 8 16 32 64 128"
set_parameter_property DATA_SIZE HDL_PARAMETER true

add_parameter TEST_GENERATOR STRING "Counter"
set_parameter_property TEST_GENERATOR DEFAULT_VALUE "Counter"
set_parameter_property TEST_GENERATOR DISPLAY_NAME "Test pattern generator"
set_parameter_property TEST_GENERATOR TYPE STRING
set_parameter_property TEST_GENERATOR ALLOWED_RANGES {"Counter" "Ramp" "Sinus" "Constant" "Sink" "Mirror"}
set_parameter_property TEST_GENERATOR DESCRIPTION "Select the type of test pattern generator."
set_parameter_property TEST_GENERATOR HDL_PARAMETER true

add_parameter ISOCHRONOUS_ENABLE INTEGER 0
set_parameter_property ISOCHRONOUS_ENABLE ENABLED false
set_parameter_property ISOCHRONOUS_ENABLE DEFAULT_VALUE false
set_parameter_property ISOCHRONOUS_ENABLE DISPLAY_HINT BOOLEAN
set_parameter_property ISOCHRONOUS_ENABLE DESCRIPTION "Enable handling for isochronous input/output of test generator."
set_parameter_property ISOCHRONOUS_ENABLE DISPLAY_NAME "Isochronous"
set_parameter_property ISOCHRONOUS_ENABLE TYPE INTEGER
set_parameter_property ISOCHRONOUS_ENABLE HDL_PARAMETER true

add_parameter CLOCK_FREQUENCY INTEGER
set_parameter_property CLOCK_FREQUENCY SYSTEM_INFO {CLOCK_RATE "clock"}
set_parameter_property CLOCK_FREQUENCY UNITS Hertz
set_parameter_property CLOCK_FREQUENCY VISIBLE false

# -----------------------------------
# display items
# -----------------------------------

# -----------------------------------
# Callbacks
# -----------------------------------
proc elaboration_callback {} {
  set TestGen [get_parameter_value TEST_GENERATOR]
  set DataSize [get_parameter_value DATA_SIZE]
  set IsoEnable [get_parameter_value ISOCHRONOUS_ENABLE]
  set Frequency [get_parameter_value CLOCK_FREQUENCY]
  global CORE_VERSION
  
  set_parameter_value CORE_VERSION $CORE_VERSION
  
  if {[string compare $TestGen "Counter"] == 0 ||
      [string compare $TestGen "Ramp"] == 0 ||
      [string compare $TestGen "Constant"] == 0 ||
      [string compare $TestGen "Sinus"] == 0} {
    set_interface_property notification_in ENABLED false
    set_port_property coe_update_notification termination_value 0
    set_port_property coe_update_notification termination true
  }

  if {[string compare $TestGen "Sink"] == 0} {
    set_interface_property notification_out ENABLED false
    set_port_property coe_changed_notification termination true
  }

  if {[string compare $TestGen "Mirror"] == 0} {
    set_parameter_property ISOCHRONOUS_ENABLE ENABLED true
  }  
  
  if {$IsoEnable == 0} {
    set_interface_property isochronous ENABLED false
    set_port_property coe_red_enable termination_value 0
    set_port_property coe_red_enable termination true
    set_port_property coe_data_valid termination true
    
    set_module_assignment embeddedsw.CMacro.ISOCHRONOUS_ENABLE 0
  } else {
    set_module_assignment embeddedsw.CMacro.ISOCHRONOUS_ENABLE 1
  }

  # we have process data and additionally 1 byte status
  set_parameter_value ADDRESS_WIDTH 2

  if {$DataSize == 16} {
    # we have process data and additionally 1 byte status
    set_parameter_value ADDRESS_WIDTH 3
  } elseif {$DataSize == 32} {
    # we have process data and additionally 1 byte status
    set_parameter_value ADDRESS_WIDTH 4
  } elseif {$DataSize == 64} {
    # we have process data and additionally 1 byte status
    set_parameter_value ADDRESS_WIDTH 5
  } elseif {$DataSize == 128} {
    # we have process data and additionally 1 byte status
    set_parameter_value ADDRESS_WIDTH 6
  }
    
  set_module_assignment embeddedsw.CMacro.FREQ                    $Frequency
  set_module_assignment embeddedsw.CMacro.DATA_WIDTH              $DataSize
  set_module_assignment embeddedsw.CMacro.TEST_GENERATOR_COUNTER  1
  set_module_assignment embeddedsw.CMacro.TEST_GENERATOR_RAMP     2
  set_module_assignment embeddedsw.CMacro.TEST_GENERATOR_SINUS    3
  set_module_assignment embeddedsw.CMacro.TEST_GENERATOR_CONSTANT 4
  set_module_assignment embeddedsw.CMacro.TEST_GENERATOR_SINK     5
  set_module_assignment embeddedsw.CMacro.TEST_GENERATOR_MIRROR   6
}

# -----------------------------------
# connection point clock
# -----------------------------------
add_interface clock clock end
set_interface_property clock clockRate 0
set_interface_property clock ENABLED true

add_interface_port clock csi_clk clk Input 1

# -----------------------------------
# connection point reset
# -----------------------------------
add_interface reset reset end
set_interface_property reset associatedClock clock
set_interface_property reset synchronousEdges DEASSERT
set_interface_property reset ENABLED true

add_interface_port reset csi_reset_n reset_n Input 1

# -----------------------------------
# connection point notification_in
# -----------------------------------
add_interface notification_in conduit end
set_interface_property notification_in ENABLED true

add_interface_port notification_in coe_update_notification export Input 1

# -----------------------------------
# connection point notification_out
# -----------------------------------
add_interface notification_out conduit end
set_interface_property notification_out ENABLED true

add_interface_port notification_out coe_changed_notification export Output 1

# -----------------------------------
# connection point isochronous
# -----------------------------------
add_interface isochronous conduit end
set_interface_property isochronous ENABLED true

add_interface_port isochronous coe_red_enable export Input 1
add_interface_port isochronous coe_data_valid export Output 1

# -----------------------------------
# connection point test_pattern
# -----------------------------------
add_interface test_pattern avalon slave
set_interface_property test_pattern addressAlignment DYNAMIC
set_interface_property test_pattern addressUnits WORDS
set_interface_property test_pattern associatedClock clock
set_interface_property test_pattern burstOnBurstBoundariesOnly false
set_interface_property test_pattern explicitAddressSpan 0
set_interface_property test_pattern holdTime 0
set_interface_property test_pattern isMemoryDevice false
set_interface_property test_pattern isNonVolatileStorage false
set_interface_property test_pattern linewrapBursts false
set_interface_property test_pattern maximumPendingReadTransactions 0
set_interface_property test_pattern printableDevice false
set_interface_property test_pattern readLatency 0
set_interface_property test_pattern readWaitTime 2
set_interface_property test_pattern setupTime 0
set_interface_property test_pattern timingUnits Cycles
set_interface_property test_pattern writeWaitTime 2
set_interface_property test_pattern ENABLED true

add_interface_port test_pattern avs_test_pattern_read read Input 1
add_interface_port test_pattern avs_test_pattern_write write Input 1
add_interface_port test_pattern avs_test_pattern_address address Input ADDRESS_WIDTH
add_interface_port test_pattern avs_test_pattern_readdata readdata Output 32
add_interface_port test_pattern avs_test_pattern_writedata writedata Input 32
add_interface_port test_pattern avs_test_pattern_byteenable byteenable Input 4

# -----------------------------------
# connection point register
# -----------------------------------
add_interface register avalon slave
set_interface_property register addressAlignment DYNAMIC
set_interface_property register addressUnits WORDS
set_interface_property register associatedClock clock
set_interface_property register burstOnBurstBoundariesOnly false
set_interface_property register explicitAddressSpan 0
set_interface_property register holdTime 0
set_interface_property register isMemoryDevice false
set_interface_property register isNonVolatileStorage false
set_interface_property register linewrapBursts false
set_interface_property register maximumPendingReadTransactions 0
set_interface_property register printableDevice false
set_interface_property register readLatency 0
set_interface_property register readWaitTime 2
set_interface_property register setupTime 0
set_interface_property register timingUnits Cycles
set_interface_property register writeWaitTime 2
set_interface_property register ENABLED true

add_interface_port register avs_register_read read Input 1
add_interface_port register avs_register_write write Input 1
add_interface_port register avs_register_address address Input 2
add_interface_port register avs_register_readdata readdata Output 32
add_interface_port register avs_register_writedata writedata Input 32


add_fileset_file "./softing/softing_test_pattern.vhd" VHDL PATH "./softing_test_pattern.vhd"
