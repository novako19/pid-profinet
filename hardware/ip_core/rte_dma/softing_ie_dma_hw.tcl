#*****************************************************************************
#
# SOFTING Industrial Automation GmbH
# Richard-Reitzner-Allee 6
# D-85540 Haar
# Phone: ++49-89-4 56 56-0
# Fax: ++49-89-4 56 56-3 99
# http://www.softing.com
#
# Copyright (C) SOFTING Industrial Automation GmbH 2011-2014. All Rights Reserved.
#
# Version: 1.00.00
#
#*****************************************************************************

# -----------------------------------
# request TCL package from ACDS 13.1
# -----------------------------------
package require -exact qsys 13.1

# -----------------------------------
# Module parameter
# -----------------------------------
set MAJOR 1
set MINOR 10
set REVISION 8411
set CORE_VERSION [expr {($MAJOR << 24) + ($MINOR << 16) + $REVISION}]
set_module_property NAME softing_dma_controller
set_module_property VERSION $MAJOR.$MINOR.$REVISION
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property GROUP "Softing/Industrial Automation/Utilities"
set_module_property AUTHOR "Softing Industrial Automation GmbH"
set_module_property DISPLAY_NAME "DMA controller"
set_module_property DESCRIPTION "DMA controller IP Cores"
set_module_property ICON_PATH softing.png
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property ANALYZE_HDL TRUE
set_module_property ELABORATION_CALLBACK elaboration_callback
set_module_property VALIDATION_CALLBACK validation_callback

# -----------------------------------
# Files
# -----------------------------------
add_fileset QUARTUS_SYNTH QUARTUS_SYNTH fileset_synth_callback ""
set_fileset_property QUARTUS_SYNTH TOP_LEVEL softing_dma_controller_interface
set_fileset_property QUARTUS_SYNTH ENABLE_RELATIVE_INCLUDE_PATHS false


# -----------------------------------
# Parameters
# -----------------------------------
add_parameter CORE_VERSION INTEGER $CORE_VERSION
set_parameter_property CORE_VERSION HDL_PARAMETER true
set_parameter_property CORE_VERSION VISIBLE false
set_parameter_property CORE_VERSION DERIVED true

add_parameter DESCRIPTOR_ADDRESS_WIDTH NATURAL 9
set_parameter_property DESCRIPTOR_ADDRESS_WIDTH DEFAULT_VALUE 9
set_parameter_property DESCRIPTOR_ADDRESS_WIDTH DISPLAY_NAME "Descriptor width"
set_parameter_property DESCRIPTOR_ADDRESS_WIDTH TYPE NATURAL
set_parameter_property DESCRIPTOR_ADDRESS_WIDTH UNITS None
set_parameter_property DESCRIPTOR_ADDRESS_WIDTH ALLOWED_RANGES {9 10 11}
set_parameter_property DESCRIPTOR_ADDRESS_WIDTH HDL_PARAMETER true
set_parameter_property DESCRIPTOR_ADDRESS_WIDTH GROUP "General Properties"

add_parameter INPUT_MASTER_WIDTH NATURAL 32
set_parameter_property INPUT_MASTER_WIDTH DEFAULT_VALUE 32
set_parameter_property INPUT_MASTER_WIDTH DISPLAY_NAME "Master width"
set_parameter_property INPUT_MASTER_WIDTH TYPE NATURAL
set_parameter_property INPUT_MASTER_WIDTH UNITS None
set_parameter_property INPUT_MASTER_WIDTH ALLOWED_RANGES {32}
set_parameter_property INPUT_MASTER_WIDTH DESCRIPTION "Datawidth of the input read and write data master"
set_parameter_property INPUT_MASTER_WIDTH HDL_PARAMETER true
set_parameter_property INPUT_MASTER_WIDTH GROUP "Input Path Properties"

add_parameter INPUT_MASTER_READ_ADDRESS_OFFSET STD_LOGIC_VECTOR 0
set_parameter_property INPUT_MASTER_READ_ADDRESS_OFFSET WIDTH "32"
set_parameter_property INPUT_MASTER_READ_ADDRESS_OFFSET DISPLAY_NAME "Read master offset"
set_parameter_property INPUT_MASTER_READ_ADDRESS_OFFSET DESCRIPTION "Address Offset of input read master"
set_parameter_property INPUT_MASTER_READ_ADDRESS_OFFSET HDL_PARAMETER true
set_parameter_property INPUT_MASTER_READ_ADDRESS_OFFSET DISPLAY_HINT hexadecimal
set_parameter_property INPUT_MASTER_READ_ADDRESS_OFFSET DISPLAY_UNITS "address"
set_parameter_property INPUT_MASTER_READ_ADDRESS_OFFSET GROUP "Input Path Properties"

add_parameter INPUT_MASTER_WRITE_ADDRESS_OFFSET STD_LOGIC_VECTOR 0
set_parameter_property INPUT_MASTER_WRITE_ADDRESS_OFFSET WIDTH "32"
set_parameter_property INPUT_MASTER_WRITE_ADDRESS_OFFSET DISPLAY_NAME "Write master offset"
set_parameter_property INPUT_MASTER_WRITE_ADDRESS_OFFSET DESCRIPTION "Address Offset of input write master"
set_parameter_property INPUT_MASTER_WRITE_ADDRESS_OFFSET HDL_PARAMETER true
set_parameter_property INPUT_MASTER_WRITE_ADDRESS_OFFSET DISPLAY_HINT hexadecimal
set_parameter_property INPUT_MASTER_WRITE_ADDRESS_OFFSET DISPLAY_UNITS "address"
set_parameter_property INPUT_MASTER_WRITE_ADDRESS_OFFSET GROUP "Input Path Properties"

add_parameter OUTPUT_MASTER_WIDTH NATURAL 32
set_parameter_property OUTPUT_MASTER_WIDTH DEFAULT_VALUE 32
set_parameter_property OUTPUT_MASTER_WIDTH DISPLAY_NAME "Master width"
set_parameter_property OUTPUT_MASTER_WIDTH TYPE NATURAL
set_parameter_property OUTPUT_MASTER_WIDTH UNITS None
set_parameter_property OUTPUT_MASTER_WIDTH ALLOWED_RANGES {32}
set_parameter_property OUTPUT_MASTER_WIDTH DESCRIPTION "Datawidth of the output read and write data master"
set_parameter_property OUTPUT_MASTER_WIDTH HDL_PARAMETER true
set_parameter_property OUTPUT_MASTER_WIDTH GROUP "Output Path Properties"

add_parameter OUTPUT_MASTER_READ_ADDRESS_OFFSET STD_LOGIC_VECTOR 0
set_parameter_property OUTPUT_MASTER_READ_ADDRESS_OFFSET WIDTH "32"
set_parameter_property OUTPUT_MASTER_READ_ADDRESS_OFFSET DISPLAY_NAME "Read master offset"
set_parameter_property OUTPUT_MASTER_READ_ADDRESS_OFFSET DESCRIPTION "Address Offset of output read master"
set_parameter_property OUTPUT_MASTER_READ_ADDRESS_OFFSET HDL_PARAMETER true
set_parameter_property OUTPUT_MASTER_READ_ADDRESS_OFFSET DISPLAY_HINT hexadecimal
set_parameter_property OUTPUT_MASTER_READ_ADDRESS_OFFSET DISPLAY_UNITS "address"
set_parameter_property OUTPUT_MASTER_READ_ADDRESS_OFFSET GROUP "Output Path Properties"

add_parameter OUTPUT_MASTER_WRITE_ADDRESS_OFFSET STD_LOGIC_VECTOR 0
set_parameter_property OUTPUT_MASTER_WRITE_ADDRESS_OFFSET WIDTH "32"
set_parameter_property OUTPUT_MASTER_WRITE_ADDRESS_OFFSET DISPLAY_NAME "Write master offset"
set_parameter_property OUTPUT_MASTER_WRITE_ADDRESS_OFFSET DESCRIPTION "Address Offset of output write master"
set_parameter_property OUTPUT_MASTER_WRITE_ADDRESS_OFFSET HDL_PARAMETER true
set_parameter_property OUTPUT_MASTER_WRITE_ADDRESS_OFFSET DISPLAY_HINT hexadecimal
set_parameter_property OUTPUT_MASTER_WRITE_ADDRESS_OFFSET DISPLAY_UNITS "address"
set_parameter_property OUTPUT_MASTER_WRITE_ADDRESS_OFFSET GROUP "Output Path Properties"

add_parameter EXT_SELECTOR_ENABLE BOOLEAN false
set_parameter_property EXT_SELECTOR_ENABLE DEFAULT_VALUE false
set_parameter_property EXT_SELECTOR_ENABLE DESCRIPTION "Enables an external interface providing the selector signals."
set_parameter_property EXT_SELECTOR_ENABLE DISPLAY_NAME "External selector interface"
set_parameter_property EXT_SELECTOR_ENABLE TYPE BOOLEAN
set_parameter_property EXT_SELECTOR_ENABLE GROUP "External Interface"

add_parameter EXT_ENABLE BOOLEAN false
set_parameter_property EXT_ENABLE DEFAULT_VALUE false
set_parameter_property EXT_ENABLE DESCRIPTION "Enables an external interface to clear the pending interrupts and lock the mutex."
set_parameter_property EXT_ENABLE DISPLAY_NAME "External interface"
set_parameter_property EXT_ENABLE TYPE BOOLEAN
set_parameter_property EXT_ENABLE GROUP "External Interface"

add_parameter EXT_INT_LOCK_WIDTH NATURAL 8
set_parameter_property EXT_INT_LOCK_WIDTH DEFAULT_VALUE 8
set_parameter_property EXT_INT_LOCK_WIDTH DISPLAY_NAME "Lock width"
set_parameter_property EXT_INT_LOCK_WIDTH TYPE NATURAL
set_parameter_property EXT_INT_LOCK_WIDTH UNITS None
set_parameter_property EXT_INT_LOCK_WIDTH ALLOWED_RANGES {1:32}
set_parameter_property EXT_INT_LOCK_WIDTH DESCRIPTION "Width of the lock vectors"
set_parameter_property EXT_INT_LOCK_WIDTH HDL_PARAMETER true
set_parameter_property EXT_INT_LOCK_WIDTH GROUP "External Interface"

add_parameter EXT_INT_LOCK_BIT NATURAL 3
set_parameter_property EXT_INT_LOCK_BIT DEFAULT_VALUE 3
set_parameter_property EXT_INT_LOCK_BIT DISPLAY_NAME "Lock bit"
set_parameter_property EXT_INT_LOCK_BIT TYPE NATURAL
set_parameter_property EXT_INT_LOCK_BIT UNITS None
set_parameter_property EXT_INT_LOCK_BIT ALLOWED_RANGES {0:31}
set_parameter_property EXT_INT_LOCK_BIT DESCRIPTION "The bit of the locking vectors used for locking"
set_parameter_property EXT_INT_LOCK_BIT HDL_PARAMETER true
set_parameter_property EXT_INT_LOCK_BIT GROUP "External Interface"

add_parameter EXT_INT_INTERRUPT_WIDTH NATURAL 1
set_parameter_property EXT_INT_INTERRUPT_WIDTH DEFAULT_VALUE 1
set_parameter_property EXT_INT_INTERRUPT_WIDTH DISPLAY_NAME "Interrupt clear width"
set_parameter_property EXT_INT_INTERRUPT_WIDTH TYPE NATURAL
set_parameter_property EXT_INT_INTERRUPT_WIDTH UNITS None
set_parameter_property EXT_INT_INTERRUPT_WIDTH ALLOWED_RANGES {1:32}
set_parameter_property EXT_INT_INTERRUPT_WIDTH DESCRIPTION "Width of the interrupt vectors"
set_parameter_property EXT_INT_INTERRUPT_WIDTH HDL_PARAMETER true
set_parameter_property EXT_INT_INTERRUPT_WIDTH GROUP "External Interface"

add_parameter EXT_INT_INTERRUPT_BIT NATURAL 0
set_parameter_property EXT_INT_INTERRUPT_BIT DEFAULT_VALUE 0
set_parameter_property EXT_INT_INTERRUPT_BIT DISPLAY_NAME "Interrupt clear bit"
set_parameter_property EXT_INT_INTERRUPT_BIT TYPE NATURAL
set_parameter_property EXT_INT_INTERRUPT_BIT UNITS None
set_parameter_property EXT_INT_INTERRUPT_BIT ALLOWED_RANGES {0:31}
set_parameter_property EXT_INT_INTERRUPT_BIT DESCRIPTION "The bit of the interrupt vectors used for locking"
set_parameter_property EXT_INT_INTERRUPT_BIT HDL_PARAMETER true
set_parameter_property EXT_INT_INTERRUPT_BIT GROUP "External Interface"

# -----------------------------------
# display items
# -----------------------------------

# -----------------------------------
# Callbacks
# -----------------------------------
proc validation_callback {} {
}

proc elaboration_callback {} {
  set ExternalInt         [get_parameter_value EXT_ENABLE]
  set ExternalSelectorInt [get_parameter_value EXT_SELECTOR_ENABLE]
  set LockWidth           [get_parameter_value EXT_INT_LOCK_WIDTH]
  set LockBit             [get_parameter_value EXT_INT_LOCK_BIT]
  set IntWidth            [get_parameter_value EXT_INT_INTERRUPT_WIDTH]
  set IntBit              [get_parameter_value EXT_INT_INTERRUPT_BIT]
  global CORE_VERSION

  set_parameter_value CORE_VERSION $CORE_VERSION
  
  if {[string compare $ExternalInt "true"] == 0} {
    set_interface_property extlock ENABLED true
    set_interface_property extin ENABLED true
    set_interface_property extout ENABLED true
    set_interface_property datain ENABLED true
    set_interface_property dataout ENABLED true

    set_port_property coe_extlock_req WIDTH_EXPR $LockWidth
    set_port_property coe_extlock_ack WIDTH_EXPR $LockWidth
    set_port_property coe_extlock_free WIDTH_EXPR $LockWidth
    
    set_port_property coe_extin_intreq WIDTH_EXPR $IntWidth
    set_port_property coe_extin_intclear WIDTH_EXPR $IntWidth
    
    set_port_property coe_extout_intreq WIDTH_EXPR $IntWidth
    set_port_property coe_extout_intclear WIDTH_EXPR $IntWidth
  } else {
    set_interface_property extlock ENABLED false
    set_interface_property extin ENABLED false
    set_interface_property extout ENABLED false
    set_interface_property datain ENABLED false
    set_interface_property dataout ENABLED false

    set_port_property coe_extlock_req termination true
    set_port_property coe_extlock_free termination true
    set_port_property coe_extlock_ack termination_value 0
    set_port_property coe_extlock_ack termination true
  }
  
  if {[string compare $ExternalSelectorInt "false"] == 0} {
    set_port_property coe_datain_selector termination true
    set_port_property coe_datain_allowed termination_value 1
    set_port_property coe_datain_allowed termination true
    
    set_port_property coe_dataout_selector termination true
    set_port_property coe_dataout_allowed termination_value 1
    set_port_property coe_dataout_allowed termination true
  }  
  
  set_module_assignment embeddedsw.CMacro.INPUT_READ_OFFSET [get_parameter_value INPUT_MASTER_READ_ADDRESS_OFFSET]
  set_module_assignment embeddedsw.CMacro.INPUT_WRITE_OFFSET [get_parameter_value INPUT_MASTER_WRITE_ADDRESS_OFFSET]
  set_module_assignment embeddedsw.CMacro.OUTPUT_READ_OFFSET [get_parameter_value OUTPUT_MASTER_READ_ADDRESS_OFFSET]
  set_module_assignment embeddedsw.CMacro.OUTPUT_WRITE_OFFSET [get_parameter_value OUTPUT_MASTER_WRITE_ADDRESS_OFFSET]
}

proc fileset_synth_callback {entityname} {
}

# -----------------------------------
# connection point clock
# -----------------------------------
add_interface clock clock sink
set_interface_property clock clockRate 0
set_interface_property clock ENABLED true

add_interface_port clock csi_clk clk Input 1

# -----------------------------------
# connection point clock_reset
# -----------------------------------
add_interface clock_reset reset sink
set_interface_property clock_reset associatedClock clock
set_interface_property clock_reset synchronousEdges DEASSERT
set_interface_property clock_reset ENABLED true

add_interface_port clock_reset csi_reset_n reset_n Input 1

# -----------------------------------
# connection point irq_output
# -----------------------------------
add_interface irq_output interrupt sender
set_interface_property irq_output associatedAddressablePoint register
set_interface_property irq_output associatedClock clock
set_interface_property irq_output associatedReset clock_reset
set_interface_property irq_output ENABLED true

add_interface_port irq_output ins_out_irq irq Output 1

# -----------------------------------
# connection point input_irq_input
# -----------------------------------
add_interface input_irq_input interrupt receiver
set_interface_property input_irq_input associatedAddressablePoint input_write_master
set_interface_property input_irq_input associatedClock clock
set_interface_property input_irq_input associatedReset clock_reset
set_interface_property input_irq_input ENABLED true

add_interface_port input_irq_input ins_input_in_irq irq Input 1

# -----------------------------------
# connection point output_irq_input
# -----------------------------------
add_interface output_irq_input interrupt receiver
set_interface_property output_irq_input associatedAddressablePoint output_write_master
set_interface_property output_irq_input associatedClock clock
set_interface_property output_irq_input associatedReset clock_reset
set_interface_property output_irq_input ENABLED true

add_interface_port output_irq_input ins_output_in_irq irq Input 1

# -----------------------------------
# connection point register
# -----------------------------------
add_interface register avalon slave
set_interface_property register addressAlignment DYNAMIC
set_interface_property register addressUnits WORDS
set_interface_property register associatedClock clock
set_interface_property register burstOnBurstBoundariesOnly false
set_interface_property register explicitAddressSpan 0
set_interface_property register holdTime 0
set_interface_property register isMemoryDevice false
set_interface_property register isNonVolatileStorage false
set_interface_property register linewrapBursts false
set_interface_property register maximumPendingReadTransactions 0
set_interface_property register printableDevice false
set_interface_property register readLatency 0
set_interface_property register readWaitTime 1
set_interface_property register setupTime 0
set_interface_property register timingUnits Cycles
set_interface_property register writeWaitTime 0
set_interface_property register ENABLED true

add_interface_port register avs_register_read read Input 1
add_interface_port register avs_register_write write Input 1
add_interface_port register avs_register_address address Input 13
add_interface_port register avs_register_readdata readdata Output 32
add_interface_port register avs_register_writedata writedata Input 32
add_interface_port register avs_register_waitrequest waitrequest Output 1

# -----------------------------------
# connection point control_master
# -----------------------------------
add_interface control_master avalon master
set_interface_property control_master addressUnits SYMBOLS
set_interface_property control_master associatedClock clock
set_interface_property control_master burstOnBurstBoundariesOnly false
set_interface_property control_master doStreamReads false
set_interface_property control_master doStreamWrites false
set_interface_property control_master linewrapBursts false
set_interface_property control_master readLatency 0
set_interface_property control_master ENABLED true

add_interface_port control_master avm_control_master_read read Output 1
add_interface_port control_master avm_control_master_address address Output 32
add_interface_port control_master avm_control_master_waitrequest waitrequest Input 1
add_interface_port control_master avm_control_master_readdata readdata Input 32

# -----------------------------------
# connection point input_write_master
# -----------------------------------
add_interface input_write_master avalon master
set_interface_property input_write_master addressUnits SYMBOLS
set_interface_property input_write_master associatedClock clock
set_interface_property input_write_master burstOnBurstBoundariesOnly false
set_interface_property input_write_master doStreamReads false
set_interface_property input_write_master doStreamWrites false
set_interface_property input_write_master linewrapBursts false
set_interface_property input_write_master readLatency 0
set_interface_property input_write_master ENABLED true

add_interface_port input_write_master avm_input_write_master_write write Output 1
add_interface_port input_write_master avm_input_write_master_address address Output 32
add_interface_port input_write_master avm_input_write_master_waitrequest waitrequest Input 1
add_interface_port input_write_master avm_input_write_master_writedata writedata Output 32
add_interface_port input_write_master avm_input_write_master_byteenable byteenable Output 4

# -----------------------------------
# connection point input_read_master
# -----------------------------------
add_interface input_read_master avalon master
set_interface_property input_read_master addressUnits SYMBOLS
set_interface_property input_read_master associatedClock clock
set_interface_property input_read_master burstOnBurstBoundariesOnly false
set_interface_property input_read_master doStreamReads false
set_interface_property input_read_master doStreamWrites false
set_interface_property input_read_master linewrapBursts false
set_interface_property input_read_master readLatency 0
set_interface_property input_read_master ENABLED true

add_interface_port input_read_master avm_input_read_master_read read Output 1
add_interface_port input_read_master avm_input_read_master_address address Output 32
add_interface_port input_read_master avm_input_read_master_waitrequest waitrequest Input 1
add_interface_port input_read_master avm_input_read_master_readdata readdata Input 32
add_interface_port input_read_master avm_input_read_master_byteenable byteenable Output 4

# -----------------------------------
# connection point Output_write_master
# -----------------------------------
add_interface output_write_master avalon master
set_interface_property output_write_master addressUnits SYMBOLS
set_interface_property output_write_master associatedClock clock
set_interface_property output_write_master burstOnBurstBoundariesOnly false
set_interface_property output_write_master doStreamReads false
set_interface_property output_write_master doStreamWrites false
set_interface_property output_write_master linewrapBursts false
set_interface_property output_write_master readLatency 0
set_interface_property output_write_master ENABLED true

add_interface_port output_write_master avm_output_write_master_write write Output 1
add_interface_port output_write_master avm_output_write_master_address address Output 32
add_interface_port output_write_master avm_output_write_master_waitrequest waitrequest Input 1
add_interface_port output_write_master avm_output_write_master_writedata writedata Output 32
add_interface_port output_write_master avm_output_write_master_byteenable byteenable Output 4

# -----------------------------------
# connection point output_read_master
# -----------------------------------
add_interface output_read_master avalon master
set_interface_property output_read_master addressUnits SYMBOLS
set_interface_property output_read_master associatedClock clock
set_interface_property output_read_master burstOnBurstBoundariesOnly false
set_interface_property output_read_master doStreamReads false
set_interface_property output_read_master doStreamWrites false
set_interface_property output_read_master linewrapBursts false
set_interface_property output_read_master readLatency 0
set_interface_property output_read_master ENABLED true

add_interface_port output_read_master avm_output_read_master_read read Output 1
add_interface_port output_read_master avm_output_read_master_address address Output 32
add_interface_port output_read_master avm_output_read_master_waitrequest waitrequest Input 1
add_interface_port output_read_master avm_output_read_master_readdata readdata Input 32
add_interface_port output_read_master avm_output_read_master_byteenable byteenable Output 4

# -----------------------------------
# connection point extlock
# -----------------------------------
add_interface extlock conduit end
set_interface_property extlock ENABLED true

add_interface_port extlock coe_extlock_req export Output 1
add_interface_port extlock coe_extlock_ack export Input 1
add_interface_port extlock coe_extlock_free export Output 1

# -----------------------------------
# connection point extin
# -----------------------------------
add_interface extin conduit end
set_interface_property extin ENABLED true

add_interface_port extin coe_extin_intreq export Input 1
add_interface_port extin coe_extin_intclear export Output 1

# -----------------------------------
# connection point extout
# -----------------------------------
add_interface extout conduit end
set_interface_property extout ENABLED true

add_interface_port extout coe_extout_intreq export Input 1
add_interface_port extout coe_extout_intclear export Output 1

# -----------------------------------
# connection point datain
# -----------------------------------
add_interface datain conduit end
set_interface_property datain ENABLED true

add_interface_port datain coe_datain_selector export Output 16
add_interface_port datain coe_datain_allowed export Input 1

# -----------------------------------
# connection point dataout
# -----------------------------------
add_interface dataout conduit end
set_interface_property dataout ENABLED true

add_interface_port dataout coe_dataout_selector export Output 16
add_interface_port dataout coe_dataout_allowed export Input 1

# -----------------------------------
# connection point running
# -----------------------------------
add_interface running conduit end
set_interface_property running ENABLED true

add_interface_port running coe_running_input export Output 1
add_interface_port running coe_running_output export Output 1

# -----------------------------------
# connection point notification_in
# -----------------------------------
add_interface notification_in conduit end
set_interface_property notification_in ENABLED true

add_interface_port notification_in coe_update_notification export Input 64

# -----------------------------------
# connection point notification_out
# -----------------------------------
add_interface notification_out conduit end
set_interface_property notification_out ENABLED true

add_interface_port notification_out coe_changed_notification export Output 64


add_fileset_file "./softing/softing_ie_dma.vhd" VHDL PATH "./softing_ie_dma.vhd"
