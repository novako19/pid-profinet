################################################################################
.PHONY: check_environment update_version version
################################################################################
# default values for nios and gcc

export BSP_BASE_PATH = ../../../../Samples/sdai
export EDD_BASE_PATH = ../../../../DeviceDescription

ifeq ($(TARGET),)
# set the default target (others are altera_ink_switch and xilinx_iek_switch)
export TARGET=softing_rtem_switch
endif

CROSS_COMPILE = nios2-elf-
export BSP    = nios2-bsp-generate-files

ifeq ($(BSP_PATH),)
ifeq (altera,$(findstring altera,$(TARGET)))
export BSP_PATH = $(BSP_BASE_PATH)/appl_altera_hal/$(subst altera,bsp,$(TARGET))
else
ifeq (ebv,$(findstring ebv,$(TARGET)))
export BSP_PATH = $(BSP_BASE_PATH)/appl_altera_hal/$(subst ebv,bsp,$(TARGET))
else
export BSP_PATH = $(BSP_BASE_PATH)/appl_altera_hal/$(subst softing,bsp,$(TARGET))
endif
endif
endif

export CC      = $(CROSS_COMPILE)gcc
export AR      = $(CROSS_COMPILE)ar
export NM      = $(CROSS_COMPILE)nm
export OBJCOPY = $(CROSS_COMPILE)objcopy
export LINKER  = $(CROSS_COMPILE)ld
export PERL    = perl

################################################################################
# target default make settings

export SDAI_INCLUDE_1 = ../../../include
export SDAI_INCLUDE_2 = ../../../sdai/include

export BSP_FLAGS      = --silent --settings=settings.bsp --bsp-dir=.

export CCFLAGSAPPL    = -c -g                              \
                        -I../..                            \
                        -I$(SDAI_INCLUDE_1)                \
                        -I$(SDAI_INCLUDE_2)                \
                        -DPROCESSOR_APPL                   \
                        -DSYSTEM_BUS_WIDTH=32              \
                        -W -Wall -Wstrict-prototypes -Wno-trigraphs

export ARFLAGS        = -r -c
export LINKERFLAGS    = -r
export NMFLAGS        = -P
export OBJCOPYFLAGS   =

export MAKEOPTIONS    = --no-print-directory -r
                          

CCFLAGSAPPL   += -I$(patsubst \\,/,$(SOPC_KIT_NIOS2))/../ip/altera/nios2_ip/altera_nios2/HAL/inc


################################################################################
# check environment variables and retrieve version information

first_target: all

check_environment:

################################################################################
