.PHONY: $(COMPONENT) clean_$(COMPONENT)

################################################################################
# target files of this component
                
OBJECTS  = porting_pbdp_appl.o         \
           sdai_pbdp_appl.o            \
           sdai_interface_pbdp_appl.o  \
           sdai_drv_pbdp_pbdp_appl.o

LIB = libSdaiPbsAppl.a

################################################################################
# defaults

COMPONENT = pbs_appl

include ../Makerules.mak

vpath %.c ../../../sdai/api
vpath %.c ../../../sdai/driver
vpath %.c ../../../sdai/interface
vpath %.c ../../

CCFLAGSAPPL +=  -DSDAI_INCLUDE_PB_DP -I$(BSP_PATH)

################################################################################
# dependencies

$(COMPONENT): $(LIB)
$(LIB)      : $(OBJECTS)

clean_$(COMPONENT):
	@echo "[CLEAN] $(COMPONENT)"
	-@rm -f $(LIB) $(OBJECTS)

################################################################################
