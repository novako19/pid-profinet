.PHONY: $(COMPONENT) clean_$(COMPONENT)

################################################################################
# target files of this component
                
OBJECTS  = porting_mb_appl.o         \
           sdai_mb_appl.o            \
           sdai_interface_mb_appl.o  \
           sdai_drv_mb_mb_appl.o     \
           sdai_drv_socket_mb_appl.o

LIB = libSdaiMbAppl.a

################################################################################
# defaults

COMPONENT = mb_appl

include ../Makerules.mak

vpath %.c ../../../sdai/api
vpath %.c ../../../sdai/driver
vpath %.c ../../../sdai/interface
vpath %.c ../../

CCFLAGSAPPL +=  -DSDAI_INCLUDE_MBAK -DSDAI_INCLUDE_SOCKET -I$(BSP_PATH)

################################################################################
# dependencies

$(COMPONENT): $(LIB)
$(LIB)      : $(OBJECTS)

clean_$(COMPONENT):
	@echo "[CLEAN] $(COMPONENT)"
	-@rm -f $(LIB) $(OBJECTS)

################################################################################
