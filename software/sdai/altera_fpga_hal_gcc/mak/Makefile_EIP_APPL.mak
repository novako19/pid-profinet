.PHONY: $(COMPONENT) clean_$(COMPONENT)

################################################################################
# target files of this component
                
OBJECTS  = porting_eip_appl.o         \
           sdai_eip_appl.o            \
           sdai_interface_eip_appl.o  \
           sdai_drv_eip_eip_appl.o    \
           sdai_drv_socket_eip_appl.o

LIB = libSdaiEipAppl.a

################################################################################
# defaults

COMPONENT = eip_appl

include ../Makerules.mak

vpath %.c ../../../sdai/api
vpath %.c ../../../sdai/driver
vpath %.c ../../../sdai/interface
vpath %.c ../../

CCFLAGSAPPL +=  -DSDAI_INCLUDE_EIP -DSDAI_INCLUDE_SOCKET -I$(BSP_PATH)

################################################################################
# dependencies

$(COMPONENT): $(LIB)
$(LIB)      : $(OBJECTS)

clean_$(COMPONENT):
	@echo "[CLEAN] $(COMPONENT)"
	-@rm -f $(LIB) $(OBJECTS)

################################################################################
