#! /bin/sh
#

clean () {
	make -f Makefile clean
}

###############################################################################

cve () {
  make -f Makefile esc_appl TARGET=altera_cve_esc CROSS_COMPILE=nios2-elf-
  make -f Makefile eip_appl TARGET=altera_cve_eip CROSS_COMPILE=nios2-elf-
  make -f Makefile mb_appl TARGET=altera_cve_mb CROSS_COMPILE=nios2-elf-
  make -f Makefile pn_appl TARGET=altera_cve_pn CROSS_COMPILE=nios2-elf-
  make -f Makefile epl_appl TARGET=altera_cve_epl CROSS_COMPILE=nios2-elf-
}

###############################################################################

dbc5 () {
  make -f Makefile esc_appl TARGET=ebv_dbc5_esc CROSS_COMPILE=nios2-elf-
  make -f Makefile switch_appl TARGET=ebv_dbc5_switch CROSS_COMPILE=nios2-elf-
}

###############################################################################

ink () {
  make -f Makefile esc_appl TARGET=altera_ink_esc CROSS_COMPILE=nios2-elf-
  make -f Makefile eip_appl TARGET=altera_ink_eip CROSS_COMPILE=nios2-elf-
  make -f Makefile mb_appl TARGET=altera_ink_mb CROSS_COMPILE=nios2-elf-
  make -f Makefile pn_appl TARGET=altera_ink_pn CROSS_COMPILE=nios2-elf-
  make -f Makefile epl_appl TARGET=altera_ink_epl CROSS_COMPILE=nios2-elf-
}

###############################################################################

rtem () {
  #make -f Makefile esc_appl TARGET=softing_rtem_esc CROSS_COMPILE=nios2-elf-
  make -f Makefile switch_appl TARGET=softing_rtem_switch CROSS_COMPILE=nios2-elf-
  #make -f Makefile epl_appl TARGET=softing_rtem_epl CROSS_COMPILE=nios2-elf-
}
###############################################################################

for command in $@; do
  case "$command" in
    ink)
      clean; ink
    ;;
    
    rtem)
      clean; rtem
    ;;
    
    cve)
      clean; cve
    ;;

    dbc5)
      clean; dbc5
    ;;

    clean)
      clean
    ;;

    *)
      echo "Usage: $0 {ink|rtem|cve|cvsoc|dbc5|clean}"
    ;;
  esac
done

if [ $# -eq 0 ]; then
  echo "Usage: $0 {ink|rtem|cve|cvsoc|dbc5|clean}"
fi

