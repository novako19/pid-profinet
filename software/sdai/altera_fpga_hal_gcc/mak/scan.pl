#!/usr/bin/perl

open FILE, ">dump";

while (<STDIN>) 
{
  if ($_ !~ stack_task_main_loop)
  {
    if ($_ =~ /([\w_]+)\sC/) 
    {
      print $1;
      print "\n";
    print FILE $1;
    print FILE "\n";
    }
    if ($_ =~ /([\w_]+)\sT/) 
    {
      print $1;
      print "\n";
    print FILE $1;
    print FILE "\n";
    }
    if ($_ =~ /([\w_]+)\sS/) 
    {
      print $1;
      print "\n";
    print FILE $1;
    print FILE "\n";
    }    
  }
}