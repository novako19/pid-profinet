/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

#include <system.h>
#include <string.h>
#include <io.h>

#include <sys/alt_irq.h>

#include "sdai.h"

#include "cfg_cc.h"

#ifdef SDAI_INCLUDE_SOCKET
  #include "sdai_socket.h"
#endif

#include "sdai_interface.h"

#ifdef SDAI_INCLUDE_SOCKET
  #include "socket_interface.h"
#endif

#include "sdai_drv.h"

#include "porting.h"

/******************************************************************************
DEFINES
******************************************************************************/

#undef PROCESSOR_BYTE_ORDERING_BIG_ENDIAN
#define PROCESSOR_BYTE_ORDERING_LITTLE_ENDIAN   /* byte ordering of this platform */

#if (SDAI_DPRAM_SIZE > RTE_SUBSYSTEM_FIELDBUS_UTILITY_DPRAM_SIZE_VALUE)
  #error "DPRAM to small!"
#endif

#if (APPL_SUBSYSTEM_PIO_INT_FROM_FIELDBUS_BIT_CLEARING_EDGE_REGISTER == 0)
  #error "BIT_CLEARING_EDGE must be enabled for SDAI PIO when performance optimization is used!"
#endif

STATIC_ASSERT((sizeof(T_SHARED_RAM_INTERFACE)<=SDAI_DPRAM_SIZE), "Shared RAM Structure to big!");

#ifdef SDAI_INCLUDE_SOCKET
  STATIC_ASSERT((sizeof(T_SOCKET_INTERFACE)<=(RTE_SUBSYSTEM_FIELDBUS_UTILITY_DPRAM_SIZE_VALUE - SDAI_DPRAM_SIZE)), "Shared RAM Structure to big!");
#endif

#define CYGARC_UNCACHED_ADDRESS(x) (((alt_u32)(x)) | 0x80000000)

#ifdef RTE_SUBSYSTEM_FIELDBUS_UTILITY_MUTEX_BASE
  #define HW_MUTEX_BASE   RTE_SUBSYSTEM_FIELDBUS_UTILITY_MUTEX_BASE
#else
  #define HW_MUTEX_BASE   RTE_SUBSYSTEM_MUTEX_BASE
#endif

/*---------------------------------------------------------------------------*/

/* Mapping of defines from "system.h" to internal used defines */
#define DPRAM_START           CYGARC_UNCACHED_ADDRESS(RTE_SUBSYSTEM_FIELDBUS_UTILITY_DPRAM_BASE) /**< The startaddress of the DPRAM on the Stack CPU */

#define IRQ_RES_NUMBER        APPL_SUBSYSTEM_PIO_INT_FROM_FIELDBUS_IRQ                           /**<  */
#define IRQ_RES_CID           APPL_SUBSYSTEM_PIO_INT_FROM_FIELDBUS_IRQ_INTERRUPT_CONTROLLER_ID   /**<  */
#define IRQ_RES_BASE          CYGARC_UNCACHED_ADDRESS(APPL_SUBSYSTEM_PIO_INT_FROM_FIELDBUS_BASE) /**<  */
#define IRQ_REQ_BASE          CYGARC_UNCACHED_ADDRESS(APPL_SUBSYSTEM_PIO_INT_TO_FIELDBUS_BASE)   /**<  */
#define IRQ_SYNCH_NUMBER      APPL_SUBSYSTEM_PIO_SYNC_IRQ                                        /**<  */
#define IRQ_SYNCH_CID         APPL_SUBSYSTEM_PIO_SYNC_IRQ_INTERRUPT_CONTROLLER_ID                /**<  */
#define IRQ_SYNCH_BASE        CYGARC_UNCACHED_ADDRESS(APPL_SUBSYSTEM_PIO_SYNC_BASE)              /**<  */

#define MANAGEMENT_MUTEX      CYGARC_UNCACHED_ADDRESS(HW_MUTEX_BASE + 0)                         /**<  */
#define INDICATION_MUTEX      CYGARC_UNCACHED_ADDRESS(HW_MUTEX_BASE + 4)                         /**<  */
#define REQ_RES_MUTEX         CYGARC_UNCACHED_ADDRESS(HW_MUTEX_BASE + 8)                         /**<  */
#define UNIT_MUTEX            CYGARC_UNCACHED_ADDRESS(HW_MUTEX_BASE + 12)                        /**<  */
#define FLASH_MUTEX           CYGARC_UNCACHED_ADDRESS(HW_MUTEX_BASE + 16)                        /**<  */

#ifdef SDAI_INCLUDE_SOCKET
  #define SOCKET_MUTEX        CYGARC_UNCACHED_ADDRESS(HW_MUTEX_BASE + 28)                        /**<  */
#endif

#define MUTEX_PATTERN         0x02u                                                              /**<  */

/*---------------------------------------------------------------------------*/

/* offset of the PIO Core registers */
#define PIO_DATA_REGISTER_OFFSET              0x00
#define PIO_INTERRUPT_MASK_REGISTER_OFFSET    0x02
#define PIO_EDGE_CAPTURE_REGISTER_OFFSET      0x03

/*---------------------------------------------------------------------------*/

/* Macros for writing on the registers of the MUTEX Cores.
   A description of the mutex cores can be found at the Altera webside
   (http://www.altera.com/literature/hb/nios2/n2cpu_nii51020.pdf). */

/* Macros to read and write the status register of the mutex cores */
#define MUTEX_WRITE(base, data)        IOWR_16DIRECT(base, 0, data)
#define MUTEX_READ(base)               IORD_16DIRECT(base, 0)

/* Macros to read and write the reset register of the mutex cores */
#define MUTEX_RESET_WRITE(base, data)  IOWR_16DIRECT(base, 2, data)
#define MUTEX_RESET_READ(base)         IORD_16DIRECT(base, 2)

/* Macros to set and mask the entries of the status register */
#define MUTEX_VALUE_MSK                (0x00FF)
#define MUTEX_VALUE_OFST               (0)
#define MUTEX_OWNER_MSK                0xFF00
#define MUTEX_OWNER_OFST               (8)

/******************************************************************************
TYPEDEFS
******************************************************************************/

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

static U32  get_hardware_mutex  (T_INTERFACE_AREA InterfaceArea);
static void mutex_clear         (T_INTERFACE_AREA InterfaceArea);

/******************************************************************************
GLOBAL DATA
******************************************************************************/

_DEFINE_COPYRIGHT();
_DEFINE_FIRMWARE_VERSION();

/******************************************************************************
PRIVATE DATA
******************************************************************************/

static unsigned        InterruptLocked;
static BOOL            SyncSignalEnabled;
static alt_irq_context InterruptContext;  /**< Current interrupt context */

/******************************************************************************
PUBLIC FUNCTIONS
******************************************************************************/

/**
 * @desc The function send_signal_stack() is a dummy function and is not used on
 *       application side.
 * @return
 * - type  : Void
 */
void send_signal_stack (U8 Signal)
{
  _TRACE (("send_signal_stack"));
  _AVOID_UNUSED_WARNING (Signal);

  return;
}

/*===========================================================================*/

/**
 * @desc The function send_signal_application() sends a signals from the application
 *       to the stack CPU. Therefore the application CPU must generate an edge on the interrupt
 *       pin of the stack CPU.
 * @return
 * - type  : Void
 */
void send_signal_application (U8 Signal)
{
  U8    Value;

  _TRACE (("send_signal_application"));

  /* generate an edge on the interrupt pin of the stack cpu */
  _AVOID_UNUSED_WARNING (Signal);

  Value = IORD (IRQ_REQ_BASE, PIO_DATA_REGISTER_OFFSET);
  IOWR (IRQ_REQ_BASE, PIO_DATA_REGISTER_OFFSET, (Value ^ 0x01));

  return;
}


/*===========================================================================*/

/**
 * @desc The function sdai_protocol_stack_task_main_loop() is a dummy function on the
 *       application CPU
 * @return
 * - type  : Void
 */
void sdai_protocol_stack_task_main_loop (void)
{
  _TRACE (("sdai_protocol_stack_task_main_loop"));

  return;
}

/*===========================================================================*/

 /**
  * @desc The function isr() is the interrupt handler on the application CPU if the
  *       Altera HAL is used. The function clears the edge capture register of the
  *       PIO and informs the interface about the signal.
  * @return
  * - type  : Void
  * @param[in] context
  * - type : void*
  * - range: whole address space
  * @param[in] id
  * - type : unsigned long
  * - range: 0 - 31
  */
#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
  void interface_isr (void* context)
#else
  void interface_isr (void* context, unsigned long id)
#endif
{
  U32    Signal;

  _TRACE (("interface_isr"));

  /* read and clear edge capture register */
  _AVOID_UNUSED_WARNING (context);

  Signal = IORD (IRQ_RES_BASE, PIO_EDGE_CAPTURE_REGISTER_OFFSET);

  do
  {
    IOWR (IRQ_RES_BASE, PIO_EDGE_CAPTURE_REGISTER_OFFSET, Signal);

    if (Signal & SDAI_DMA_OUT_RUNNING_SIGNAL)
    {
      /* wait until DMA is finished */
      do
      {
        ;
      } while (IORD (IRQ_RES_BASE, PIO_DATA_REGISTER_OFFSET) & SDAI_DMA_OUT_RUNNING_SIGNAL);

      /* clear it again, because the falling edge generates also an IRQ */
      IOWR (IRQ_RES_BASE, PIO_EDGE_CAPTURE_REGISTER_OFFSET, SDAI_DMA_OUT_RUNNING_SIGNAL);
      interface_receive_data_signal (INTERFACE_STACK);
    }

    if (Signal & SDAI_INTERRUPT_SIGNAL)
    {
      interface_receive_signal (INTERFACE_STACK);
    }

    Signal = IORD (IRQ_RES_BASE, PIO_EDGE_CAPTURE_REGISTER_OFFSET);

  } while (Signal & SDAI_INTERRUPT_SIGNAL_ALL);

  return;
}

/*===========================================================================*/

/**
  * @desc The function synch_isr() is the synch interrupt handler on the application CPU if the
  *       Altera HAL is used. The function clears the edge capture register of the
  *       PIO and informs the interface about the signal.
  * @return
  * - type  : Void
  * @param[in] context
  * - type : void*
  * - range: whole address space
  * @param[in] id
  * - type : unsigned long
  * - range: 0 - 31
  */
#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
  void synch_isr (void* context)
#else
  void synch_isr (void* context, unsigned long id)
#endif
{
  U8    Signal;

  _TRACE (("synch_isr"));

  /* read and clear edge capture register */

  _AVOID_UNUSED_WARNING (context);

  Signal = IORD (IRQ_SYNCH_BASE, PIO_EDGE_CAPTURE_REGISTER_OFFSET);
  IOWR (IRQ_SYNCH_BASE, PIO_EDGE_CAPTURE_REGISTER_OFFSET, Signal);

  /* Synch interrupt signal received - inform interface */
  interface_receive_synch_signal (Signal);

  return;
}

/*===========================================================================*/

/**
 * @desc The function interrupt_lock()
 * @return
 * - type  : VOID
 * @test
 */
void interrupt_lock (void)
{
  _TRACE (("interrupt_lock"));

  InterruptContext = alt_irq_disable_all ();

  return;
}

/*===========================================================================*/

/**
 * @desc The function interrupt_unlock()
 * @return
 * - type  : VOID
 * @test
 */
void interrupt_unlock (void)
{
  _TRACE (("interrupt_unlock"));

  alt_irq_enable_all (InterruptContext);

  return;
}

/*===========================================================================*/

/**
 * @desc The function init_resources() initializes the necessary resources
 *       needed by the respective interface, e.g. interrupts, mutex cores, event flags,
 *       timers. The function is called from the stack and application side and
 *       returns the address of the memory interface between both sides.
 *
 * @return Returns the base address of the SDAI shared RAM interface
 * - type  : T_SHARED_RAM_INTERFACE*
 * - values: #DPRAM_START
 * @param[in] InterfaceSide
 * - type : int
 * - range: #INTERFACE_APPLICATION | #INTERFACE_STACK
 * @param[in] EnableSynchSignal
 * - type : BOOL
 * - range: TRUE, FALSE
 * @pre [enter precondition here]
 * @post [enter postcondition here]
 * @remarks [enter remarks, i.e. compatibility issues, here]
 */
T_SHARED_RAM_INTERFACE* init_resources (int InterfaceSide, BOOL EnableSynchSignal)
{
  _TRACE (("init_resources"));

  _AVOID_UNUSED_WARNING (InterfaceSide);

  InterruptLocked = 0;

  /* initialize the interrupt PIOs/GPIO */
  IOWR(IRQ_REQ_BASE, PIO_DATA_REGISTER_OFFSET,           0x00);
  IOWR(IRQ_RES_BASE, PIO_INTERRUPT_MASK_REGISTER_OFFSET, 0x00);

  /* register the interrupts */
#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
  alt_ic_isr_register(IRQ_RES_CID, IRQ_RES_NUMBER, interface_isr, NULL, 0);
#else
  alt_irq_register (IRQ_RES_NUMBER, NULL, interface_isr);
#endif

  SyncSignalEnabled = EnableSynchSignal;

  if (EnableSynchSignal)
  {
    /* register synch interrupt */
    IOWR(IRQ_SYNCH_BASE, PIO_INTERRUPT_MASK_REGISTER_OFFSET, 0x00);

#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
    alt_ic_isr_register(IRQ_SYNCH_CID, IRQ_SYNCH_NUMBER, synch_isr, NULL, 0);
#else
    alt_irq_register (IRQ_SYNCH_NUMBER, NULL, synch_isr);
#endif
  }

  /* clear the hardware mutex cores */
  mutex_clear(MANAGEMENT_INTERFACE);
  mutex_clear(STATUS_INTERFACE);
  mutex_clear(SERVICE_REQ_RES_INTERFACE);
  mutex_clear(SERVICE_IND_INTERFACE);
  mutex_clear(UNIT_INTERFACE);
  mutex_clear(RESOURCE_INTERFACE);
  mutex_clear(SOCKET_INTERFACE);

  /* return address of the dual port ram */
  return ((T_SHARED_RAM_INTERFACE*) DPRAM_START);
}

/*===========================================================================*/

/**
 * @desc The function term_resources() frees used resources.
 * @return
 * - type  : Void
 * @param[in] InterfaceSide
 * - type : int
 * - range: #INTERFACE_APPLICATION | #INTERFACE_STACK
 */
void term_resources (int InterfaceSide)
{
  _TRACE (("term_resources"));

  _AVOID_UNUSED_WARNING (InterfaceSide);

  #ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
  alt_ic_irq_disable (IRQ_RES_CID, IRQ_RES_NUMBER);
  #else
  alt_irq_disable (IRQ_RES_NUMBER);
  #endif

  /* disable the interrupts */
  IOWR (IRQ_RES_BASE, PIO_INTERRUPT_MASK_REGISTER_OFFSET, 0);
  IOWR (IRQ_RES_BASE, PIO_EDGE_CAPTURE_REGISTER_OFFSET,     0xFFFFFFFF);

  if (SyncSignalEnabled)
  {
    SyncSignalEnabled = FALSE;

    #ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
    alt_ic_irq_disable (IRQ_SYNCH_CID, IRQ_SYNCH_NUMBER);
    #else
    alt_irq_disable (IRQ_SYNCH_NUMBER);
    #endif

    /* disable the interrupts */
    IOWR (IRQ_SYNCH_BASE, PIO_INTERRUPT_MASK_REGISTER_OFFSET, 0);
    IOWR (IRQ_SYNCH_BASE, PIO_EDGE_CAPTURE_REGISTER_OFFSET,   0xFFFFFFFF);
  }

  return;
}

/*===========================================================================*/

void enable_sdai_interrupt (void)
{
  InterruptLocked--;

  if (InterruptLocked == 0)
  {
    #ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
    alt_ic_irq_enable (IRQ_RES_CID, IRQ_RES_NUMBER);
    #else
    alt_irq_enable (IRQ_RES_NUMBER);
    #endif

    if (SyncSignalEnabled)
    {
      #ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
      alt_ic_irq_enable (IRQ_SYNCH_CID, IRQ_SYNCH_NUMBER);
      #else
      alt_irq_enable (IRQ_SYNCH_NUMBER);
      #endif
    }
  }

  return;
}

/*===========================================================================*/

void disable_sdai_interrupt (void)
{
  if (InterruptLocked == 0)
  {
    #ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
    alt_ic_irq_disable (IRQ_RES_CID, IRQ_RES_NUMBER);
    #else
    alt_irq_disable (IRQ_RES_NUMBER);
    #endif

    if (SyncSignalEnabled)
    {
      #ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
      alt_ic_irq_disable (IRQ_SYNCH_CID, IRQ_SYNCH_NUMBER);
      #else
      alt_irq_disable (IRQ_SYNCH_NUMBER);
      #endif
    }
  }

  InterruptLocked++;
}

/*===========================================================================*/

/**
 * @desc The function mutex_lock() claims the requested hardware mutex for the cpu.
 *       If the mutex is locked by the other CPU the function blocks until it becomes free.
 *       This function is called from the application and stack side.
 * @return
 * - type  : Void
 * @param[in] InterfaceArea
 * - type : T_INTERFACE_AREA
 * - range: #MANAGEMENT_INTERFACE |
 *          #STATUS_INTERFACE |
 *          #SERVICE_REQ_RES_INTERFACE |
 *          #SERVICE_IND_INTERFACE |
 *          #UNIT_INTERFACE |
 *          #RESOURCE_INTERFACE
 */
void mutex_lock (T_INTERFACE_AREA InterfaceArea)
{
  U32 MutexBase;
  volatile U16 data, check;
  U16 value = 1;

  _TRACE (("mutex_lock"));

  /* get address of the hardware mutex */
  MutexBase = get_hardware_mutex (InterfaceArea);

  if (MutexBase != 0xFFFFFFFF)
  {
    /* to achieve short cycle times (< 250us) the mutex must not be locked
       for long times. To prevent interruption the interrupts are locked */
    if (InterfaceArea == UNIT_INTERFACE)
    {
      interrupt_lock ();
    }

    /* prepare new mutex state */
    data = (MUTEX_PATTERN << MUTEX_OWNER_OFST) | value;

    do
    {
      /* write new mutex state */
      MUTEX_WRITE(MutexBase, data);
      /* read mutex state */
      check = MUTEX_READ(MutexBase);
      /* check if read state is equal to written state */
    }while(check != data);
  }

  return;
}

/*===========================================================================*/

/**
 * @desc The function mutex_unlock() frees a hardware mutex owned by the CPU.
 *       This function is called from the application and the stack side.
 * @return
 * - type  : Void
 * @param[in] InterfaceArea
 * - type : T_INTERFACE_AREA
 * - range: #MANAGEMENT_INTERFACE |
 *          #STATUS_INTERFACE |
 *          #SERVICE_REQ_RES_INTERFACE |
 *          #SERVICE_IND_INTERFACE |
 *          #UNIT_INTERFACE
 */
void mutex_unlock (T_INTERFACE_AREA InterfaceArea)
{
  U32 MutexBase;
  U16 data;

  _TRACE (("mutex_unlock"));

  /* get address of the hardware mutex */
  MutexBase = get_hardware_mutex (InterfaceArea);

  if (MutexBase != 0xFFFFFFFF)
  {
    /* prepare new mutex state */
    data = (MUTEX_PATTERN << MUTEX_OWNER_OFST) | 0;

    /* write new state with value 0 */
    MUTEX_WRITE(MutexBase, data);

    if (InterfaceArea == UNIT_INTERFACE)
    {
      interrupt_unlock ();
    }
  }

  return;
}

/*===========================================================================*/

/**
 * @desc The function mutex_clear() frees a hardware mutex regardless of which
 *       CPU currently ownes the mutex. This function is called by the application
 *       side on initialization to set the mutex cores to a defined state.
 * @return
 * - type  : Void
 * @param[in] InterfaceArea
 * - type : T_INTERFACE_AREA
 * - range: #MANAGEMENT_INTERFACE |
 *          #STATUS_INTERFACE |
 *          #SERVICE_REQ_RES_INTERFACE |
 *          #SERVICE_IND_INTERFACE |
 *          #UNIT_INTERFACE
 */
static void mutex_clear (T_INTERFACE_AREA InterfaceArea)
{
  U32 MutexBase;
  U16 Owner;

  _TRACE (("mutex_clear"));

  /* get address of the hardware mutex */
  MutexBase = get_hardware_mutex (InterfaceArea);

  if (MutexBase != 0xFFFFFFFF)
  {
    /* read the current state of the mutex and clear the value */
    Owner = (MUTEX_READ(MutexBase) & MUTEX_OWNER_MSK);

    /* clear the reset bit in the reset register */
    MUTEX_RESET_WRITE(MutexBase, 0x1);

    /* write back the state with value 0 so the mutex is free */
    MUTEX_WRITE(MutexBase, Owner);
    MUTEX_WRITE(MutexBase, 0x0000);
  }

  return;
}

/*===========================================================================*/


/**
 * @desc The function check_watchdog_supported() returns if the watchdog is
 *       supported by the hardware or not.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS | SDAI_ERR_INTERNAL
 * @test
 */
U8 check_watchdog_supported (void)
{
  _TRACE (("check_watchdog_supported"));

  return (SDAI_ERR_INTERNAL);
}

/*===========================================================================*/

/**
 * @desc The function handle_stack_watchdog_expired() handle platform specific actions
 *       on the expiration of the stack watchdog. It clears the stack ready flag to allow save call
 *       of sdai_deinit() and on the FPGA resets the stack CPU.
 * @return
 * - type  : Void
 */
void handle_stack_watchdog_expired ()
{
  T_SHARED_RAM_INTERFACE* SharedRam = (T_SHARED_RAM_INTERFACE*)DPRAM_START;
  //U32 Wait;

  SharedRam->ManagementData.StackReady = 0x00;

  /* request reset */
  //IOWR(RESET_PIO_BASE, 0, 0x1);
  /* wait some time */
  //for(Wait = 0; Wait < 100; Wait++) {__asm__("nop");}
  /* release reset */
  //IOWR(RESET_PIO_BASE, 0, 0x0);

  return;
}


/*===========================================================================*/

/**
 * @desc The function create_stack_task() blocks until the stack signals ready.
 *       The function is called from the application side.
 * @return
 * - type  : Void
 * @param[in] task_main_loop
 * - type : function pointer
 * - range: whole address space
 */
void create_stack_task (void (*task_main_loop) (void))
{
  T_SHARED_RAM_INTERFACE* SharedRam = (T_SHARED_RAM_INTERFACE*)DPRAM_START;

  _TRACE (("create_stack_task"));

  _AVOID_UNUSED_WARNING(task_main_loop);

  /* clear application and stack ready flag */
  SharedRam->ManagementData.ApplReady  = 0x00;
  SharedRam->ManagementData.StackReady = 0x00;

  do
  {
    /* set application ready flag */
    SharedRam->ManagementData.ApplReady = SDAI_INTERFACE_APPLICATION_READY;
    /* check if stack signals ready */
  }while( (SharedRam->ManagementData.StackReady != SDAI_INTERFACE_STACK_READY)           &&
          (SharedRam->ManagementData.StackReady != SDAI_INTERFACE_PROTOCOL_NOT_SUPPORTED) );

  /* enable the interrupts */
  IOWR(IRQ_RES_BASE, PIO_EDGE_CAPTURE_REGISTER_OFFSET,     0xFFFFFFFF);
  IOWR(IRQ_RES_BASE, PIO_INTERRUPT_MASK_REGISTER_OFFSET,   SDAI_INTERRUPT_SIGNAL_ALL);
  IOWR(IRQ_SYNCH_BASE, PIO_EDGE_CAPTURE_REGISTER_OFFSET,   0xFFFFFFFF);
  IOWR(IRQ_SYNCH_BASE, PIO_INTERRUPT_MASK_REGISTER_OFFSET, SDAI_SYNC_SIGNAL_ALL);

  return;
}

/*===========================================================================*/

/**
 * @desc The function delete_stack_task() blocks until the stack clears the ready signal.
 *       This function is called from the application side.
 * @return
 * - type  : Void
 * @pre The signal #INTERFACE_SIGNAL_STOP_STACK musst be send to the stack CPU
 *      before the call of the funtion.
 */
void delete_stack_task (void)
{
  T_SHARED_RAM_INTERFACE* SharedRam = (T_SHARED_RAM_INTERFACE*)DPRAM_START;

  _TRACE (("delete_stack_task"));

  /* check if stack has cleared the ready flag */
  while(SharedRam->ManagementData.StackReady == SDAI_INTERFACE_STACK_READY) {__asm__("nop");}

  /* clear application ready flag */
  SharedRam->ManagementData.ApplReady = 0x00;

  return;
}

/*===========================================================================*/

/**
 * @desc The function create_socket_task() creates the socket task.
 *       The function is called from the stack side.
 * @return
 * - type  : Void
 * @param[in] task_main_loop
 * - type : function pointer
 * - range: whole address space
 */
void create_socket_task (void (*task_main_loop) (void))
{
  _TRACE (("create_socket_task"));


  _AVOID_UNUSED_WARNING(task_main_loop);

  return;
}

/*===========================================================================*/

/**
 * @desc The function delete_socket_task() terminates the socket task.
 *       This function is called from the stack side.
 * @return
 * - type  : Void
 * @pre The signal #INTERFACE_SIGNAL_STOP_STACK musst be send to the stack CPU
 *      before the call of the funtion.
 */
void delete_socket_task (void)
{
  _TRACE (("delete_socket_task"));

  return;
}

/*===========================================================================*/

/**
 * @desc The function delete_socket_task_finish() clears the stack ready signal. This function
 *       must be called by the stack side after the protocol stack is shutdown completely.
 * @return
 * - type  : Void
 */
void delete_socket_task_finish (void)
{
  return;
}

/*===========================================================================*/

/**
 * @desc The function wait_application_startup_finished() waits for the application ready signal.
 *       This function is called from the stack side. The stack must call this function
 *       before any other access to the shared RAM.
 * @return
 * - type  : U8
 */
U8 wait_application_startup_finished (void)
{
  T_SHARED_RAM_INTERFACE* SharedRam = (T_SHARED_RAM_INTERFACE*)DPRAM_START;

  _TRACE (("wait_application_startup_finished"));

  _AVOID_UNUSED_WARNING(SharedRam);

  return (SharedRam->ManagementData.BackEnd);
}

/*===========================================================================*/

/**
 * @desc The function create_stack_task_finish() is empty on this platform
 * @return
 * - type  : Void
 */
void create_stack_task_finished (void)
{
  return;
}

/*===========================================================================*/

/**
 * @desc The function delete_stack_task() clears the stack ready signal. This function
 *       must be called by the stack side after the protocol stack is shutdown completely.
 * @return
 * - type  : Void
 */
void delete_stack_task_finish (void)
{
  T_SHARED_RAM_INTERFACE* SharedRam = (T_SHARED_RAM_INTERFACE*)DPRAM_START;

  _TRACE (("delete_stack_task_finish"));

  _AVOID_UNUSED_WARNING(SharedRam);

  return;
}

/*===========================================================================*/

/**
 * @desc The function convert_network_u32_to_processor_u32() converts data of type U32
 *       stored in network format to the format of this processor.
 *
 * @return
 * - type : U32
 * - range: whole range
 * @param[in] SrcValue
 * - type : U32
 * - range: whole range
 */
U32 convert_network_u32_to_processor_u32 (U32 SrcValue)
{
  return (_CONVERT_NETWORK_U32_TO_PROCESSOR_U32 (SrcValue));
}

/*===========================================================================*/

/**
 * @desc The function convert_network_u16_to_processor_u16() converts data of type U16
 *       stored in network format to the format of this processor.
 *
 * @return
 * - type : U16
 * - range: whole range
 * @param[in] SrcValue
 * - type : U16
 * - range: whole range
 */
U16 convert_network_u16_to_processor_u16 (U16 SrcValue)
{
  return (_CONVERT_NETWORK_U16_TO_PROCESSOR_U16 (SrcValue));
}

/*===========================================================================*/

/**
 * @desc The function convert_processor_u32_to_network_u32() converts data of type U32
 *       stored in processor format to the format of this network.
 *
 * @return
 * - type : U32
 * - range: whole range
 * @param[in] SrcValue
 * - type : U32
 * - range: whole range
 */
U32 convert_processor_u32_to_network_u32 (U32 SrcValue)
{
  return (_CONVERT_PROCESSOR_U32_TO_NETWORK_U32 (SrcValue));
}

/*===========================================================================*/

/**
 * @desc The function convert_network_u16_to_processor_u16() converts data of type U16
 *       stored in processor format to the format of this network.
 *
 * @return
 * - type : U16
 * - range: whole range
 * @param[in] SrcValue
 * - type : U16
 * - range: whole range
 */
U16 convert_processor_u16_to_network_u16 (U16 SrcValue)
{
  return (_CONVERT_PROCESSOR_U16_TO_NETWORK_U16 (SrcValue));
}

/******************************************************************************
PROTECTED FUNCTIONS
******************************************************************************/

/******************************************************************************
PRIVATE FUNCTIONS
******************************************************************************/

/**
 * @desc The function get_hardware_mutex() returns the base address of the requested
 *       hardware mutex core. The defines MANAGEMENT_MUTEX, REQ_RES_MUTEX, INDICATION_MUTEX,
 *       UNIT_MUTEX and FLASH_MUTEX must be defined for each prozessor according to the used hardware.
 * @return
 * - type  : unsigned int
 * - values: whole range is valid
 * @param[in] InterfaceArea
 * - type : T_INTERFACE_AREA
 * - range: #MANAGEMENT_INTERFACE |
 *          #STATUS_INTERFACE |
 *          #SERVICE_REQ_RES_INTERFACE |
 *          #SERVICE_IND_INTERFACE |
 *          #UNIT_INTERFACE |
 *          #RESOURCE_INTERFACE
 */
static U32 get_hardware_mutex (T_INTERFACE_AREA InterfaceArea)
{
  _TRACE (("get_hardware_mutex"));

  switch(InterfaceArea)
  {
    case MANAGEMENT_INTERFACE:      {return((U32)MANAGEMENT_MUTEX);}
    case STATUS_INTERFACE:          {return((U32)MANAGEMENT_MUTEX);}
    case SERVICE_REQ_RES_INTERFACE: {return((U32)REQ_RES_MUTEX);}
    case SERVICE_IND_INTERFACE:     {return((U32)INDICATION_MUTEX);}
    case UNIT_INTERFACE:            {return((U32)UNIT_MUTEX);}
    case RESOURCE_INTERFACE:        {return((U32)FLASH_MUTEX);}

  #ifdef SDAI_INCLUDE_SOCKET
    case SOCKET_INTERFACE:          {return((U32)SOCKET_MUTEX);}
  #endif

    default:                        {return(0xFFFFFFFF);}
  }
}

/*===========================================================================*/



/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
