/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

#include <stdio.h>
#include <string.h>

#include "sdai.h"

#include "cfg_cc.h"

#include "sdai_ext.h"
#include "sdai_socket.h"

#ifndef DOXYEXTERN
#include "sdai_drv.h"

#include "sdai_interface.h"

#include "porting.h"

/******************************************************************************
DEFINES
******************************************************************************/

#define LOCAL_IP_ADDRESS_START              0x7F000000uL
#define LOCAL_IP_ADDRESS_END                0x7FFFFFFFuL
#define MULTICAST_IP_ADDRESS_START          0xE0000000uL
#define MULTICAST_IP_ADDRESS_END            0xEFFFFFFFuL

#if (SDAI_DEVNAME_MAX_LEN != 240)
  #error "Devname length wrong"
#endif

#define PN_MAX_LABEL_LENGTH 64

/******************************************************************************
TYPEDEFS
******************************************************************************/

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

static U16    calculate_label_length                    (U8* pLabel, U16 RemainingSize);
static U8     string_compare_nocase                     (const U8* pMem1, const U8* pMem2, U32 NumberBytes);

static U8     sdai_default_deinit                       (T_SDAI_BACKEND_DRIVER*);
static U8     sdai_default_get_version                  (T_SDAI_BACKEND_DRIVER*, struct SDAI_VERSION_DATA*);
static U8     sdai_default_get_state                    (T_SDAI_BACKEND_DRIVER*, U16*, struct SDAI_ADDSTATUS_DATA*);
static U8     sdai_default_plug_unit                    (T_SDAI_BACKEND_DRIVER*, U8, U8, U8, U32*, const struct SDAI_CFG_DATA_EXT*);
static U8     sdai_default_pull_unit                    (T_SDAI_BACKEND_DRIVER*, U32);

static U8     sdai_default_get_data                     (T_SDAI_BACKEND_DRIVER*, U32, U8*, U8*);
static U8     sdai_default_set_data                     (T_SDAI_BACKEND_DRIVER*, U32, U8, const U8*);
static U8     sdai_default_process_data_update_finished (T_SDAI_BACKEND_DRIVER*);

static U8     sdai_default_alarm_request                (T_SDAI_BACKEND_DRIVER*, const struct SDAI_ALARM_REQ*);
static U8     sdai_default_diagnosis_request            (T_SDAI_BACKEND_DRIVER*, const struct SDAI_DIAGNOSIS_REQ*);

static U8     sdai_default_write_response               (T_SDAI_BACKEND_DRIVER*, const struct SDAI_WRITE_RES*);
static U8     sdai_default_read_response                (T_SDAI_BACKEND_DRIVER*, const struct SDAI_READ_RES*);
static U8     sdai_default_control_response             (T_SDAI_BACKEND_DRIVER*, const struct SDAI_CONTROL_RES*);

static U8     sdai_default_proto_start                  (T_SDAI_BACKEND_DRIVER*);
static int    sdai_default_plugging_complete            (T_SDAI_BACKEND_DRIVER*);

static U8     sdai_default_init_watchdog                (T_SDAI_BACKEND_DRIVER*, const U16);
static U8     sdai_default_check_and_trigger_watchdog   (T_SDAI_BACKEND_DRIVER*);

static U8     sdai_default_lock_resources               (T_SDAI_BACKEND_DRIVER*, U16);
static U8     sdai_default_unlock_resources             (T_SDAI_BACKEND_DRIVER*, U16);

/******************************************************************************
PUBLIC DATA
******************************************************************************/

/******************************************************************************
PRIVATE DATA
******************************************************************************/

static U8 SdaiInitialized = 0;
static U8 WatchdogEnabled = 0;

#ifdef SDAI_INCLUDE_SOCKET
static U8 SockInitialized = 0;
#endif

static T_SDAI_BACKEND_DRIVER   SdaiBackendDriver =
{
  .Interface =
  {
    .deinit                       = sdai_default_deinit,
    .get_version                  = sdai_default_get_version,
    .get_state                    = sdai_default_get_state,
    .plug_unit                    = sdai_default_plug_unit,
    .pull_unit                    = sdai_default_pull_unit,

    .get_data                     = sdai_default_get_data,
    .set_data                     = sdai_default_set_data,
    .process_data_update_finished = sdai_default_process_data_update_finished,

    .alarm_request                = sdai_default_alarm_request,
    .diagnosis_request            = sdai_default_diagnosis_request,
    .write_response               = sdai_default_write_response,
    .read_response                = sdai_default_read_response,
    .control_response             = sdai_default_control_response,

    .proto_start                  = sdai_default_proto_start,
    .plugging_complete            = sdai_default_plugging_complete,

    .init_watchdog                = sdai_default_init_watchdog,
    .check_trigger_watchdog       = sdai_default_check_and_trigger_watchdog,

    .lock_resources               = sdai_default_lock_resources,
    .unlock_resources             = sdai_default_unlock_resources
  },

  .pPrivateData = NULL
};

#endif /* DOXYINTERN */

/******************************************************************************
SDAI INTERFACE FUNCTIONS
******************************************************************************/

/** @name SDAI Functions
 * All SDAI functions which can be used by the application */
//@{

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ---  [ label = "SDAI ready" ];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit()" ];
 * @endmsc
 *
 * @desc The function sdai_init() initializes the SDAI, the backend protocol stack and
 *       the memory interface between SDAI and stack.
 * @pnio The mapping of the structure SDAI_INIT to PROFINET IO is described by \ref pn_init_data.
 * @eip  The mapping of the structure SDAI_INIT to EtherNet/IP is described by \ref eip_init_data.
 * @ecat The mapping of the structure SDAI_INIT to EtherCAT is described by \ref ecat_init_data.
 * @mb   The mapping of the structure SDAI_INIT to Modbus TCP is described by \ref mb_init_data.
 * @epl  The mapping of the structure SDAI_INIT to Ethernet Powerlink is described by \ref epl_init_data.
 * @pbdp The mapping of the structure SDAI_INIT to PROFIBUS DP is described by \ref pbdp_init_data.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_INVALID_ARGUMENT |
 *           #SDAI_ERR_INTERNAL
 * @param[in] pApplicationInitData Pointer to an instance of the initialization data
 * - type : struct SDAI_INIT *
 * - range: whole address space
 * @pre The pApplicationInitData structure must be completely set before calling this
 * function.
 * @post If this function returns with #SDAI_SUCCESS all other functions can be called.
 * @remarks This function is not thread save.
 */
U8 sdai_init (struct SDAI_INIT* pApplicationInitData)
{
  U8 Result = SDAI_ERR_INVALID_ARGUMENT;


  _TRACE (("sdai_init"));

  if (SdaiInitialized)
  {
    /* SDAI is already initialized */
    return (SDAI_ERR_INTERNAL);
  }

  if (pApplicationInitData != NULL)
  {
    memcpy (&SdaiBackendDriver.ApplicationInitData, pApplicationInitData, sizeof (SdaiBackendDriver.ApplicationInitData));

    /* whitch backend to initialize */
    switch (pApplicationInitData->BackEnd)
    {
      case SDAI_BACKEND_EIPS  : Result = sdai_drv_eip_init (&SdaiBackendDriver, pApplicationInitData);   break;
      case SDAI_BACKEND_PN    : Result = sdai_drv_pn_init  (&SdaiBackendDriver, pApplicationInitData);   break;
      case SDAI_BACKEND_PB_DP : Result = sdai_drv_pbdp_init(&SdaiBackendDriver, pApplicationInitData);   break;
      case SDAI_BACKEND_ECAT  : Result = sdai_drv_ecat_init(&SdaiBackendDriver, pApplicationInitData);   break;
      case SDAI_BACKEND_MODBUS: Result = sdai_drv_mb_init  (&SdaiBackendDriver, pApplicationInitData);   break;
      case SDAI_BACKEND_EPL   : Result = sdai_drv_epl_init (&SdaiBackendDriver, pApplicationInitData);   break;

      default: break;
    }
  }

  if (Result == SDAI_SUCCESS)
  {
    Result = sdai_extension_init(pApplicationInitData);

    SdaiInitialized = 1;
  }

  return (Result);
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init()" ];
 *
 * ---  [ label = "SDAI ready" ];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ---  [ label = "SDAI not ready" ];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init()" ];
 *
 * ---  [ label = "SDAI ready" ];
 * @endmsc
 *
 * @desc The function sdai_deinit terminates the SDAI, the backend protocol stack and
 *       the memory interface between SDAI and stack.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_STACK_NOT_RDY
 * @pre Before the first call of this function the sdai_init() function must be called.
 * @post After a call of this function, the function sdai_init() must be called
 *       before calling any other function of the SDAI.
 * @remarks This function is not thread save.
 */
U8 sdai_deinit (void)
{
  U8 Result = SDAI_ERR_STACK_NOT_RDY;


  _TRACE (("sdai_deinit"));

  if (SdaiInitialized)
  {
    Result = SdaiBackendDriver.Interface.deinit (&SdaiBackendDriver);

    /*-----------------------------------------------------------------------*/

    SdaiBackendDriver.Interface.plug_unit                    = sdai_default_plug_unit;
    SdaiBackendDriver.Interface.pull_unit                    = sdai_default_pull_unit;
    SdaiBackendDriver.Interface.deinit                       = sdai_default_deinit;
    SdaiBackendDriver.Interface.get_version                  = sdai_default_get_version;
    SdaiBackendDriver.Interface.get_state                    = sdai_default_get_state;

    SdaiBackendDriver.Interface.get_data                     = sdai_default_get_data;
    SdaiBackendDriver.Interface.set_data                     = sdai_default_set_data;
    SdaiBackendDriver.Interface.process_data_update_finished = sdai_default_process_data_update_finished;

    SdaiBackendDriver.Interface.alarm_request                = sdai_default_alarm_request;
    SdaiBackendDriver.Interface.diagnosis_request            = sdai_default_diagnosis_request;
    SdaiBackendDriver.Interface.write_response               = sdai_default_write_response;
    SdaiBackendDriver.Interface.read_response                = sdai_default_read_response;
    SdaiBackendDriver.Interface.control_response             = sdai_default_control_response;

    SdaiBackendDriver.Interface.proto_start                  = sdai_default_proto_start;

    SdaiBackendDriver.Interface.init_watchdog                = sdai_default_init_watchdog;
    SdaiBackendDriver.Interface.check_trigger_watchdog       = sdai_default_check_and_trigger_watchdog;

    /*-----------------------------------------------------------------------*/

    SdaiBackendDriver.pPrivateData = NULL;

    if (Result == SDAI_SUCCESS)
    {
      Result = sdai_extension_deinit ();

      SdaiInitialized = 0;
      WatchdogEnabled = 0;
    }
  }

  return (Result);
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 *
 * ...;
 * "Application"=>"SDAI::API" [ label = "sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)", URL="\ref sdai_plug_unit()"];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)" ];
 *
 * ---  [ label = "Stack online" ];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_get_version()", URL="\ref sdai_get_version()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_get_version()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS, pVersion"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS, pVersion"];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit()" ];
 * @endmsc
 *
 * @desc The function sdai_get_version() returns the version of the SDAI, the backend stack
 *       and of the mapping layer. The format depends on the used backend.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_INVALID_ARGUMENT |
 *           #SDAI_ERR_STACK_NOT_RDY
 * @param[out] pVersionData Pointer to a structure where the version strings are stored.
 * - type : struct SDAI_VERSION_DATA*
 * - range: whole address space
 * @pre Before the first call of this function the sdai_init() function must be called.
 * @remarks This function is not thread save.
 */
U8 sdai_get_version (struct SDAI_VERSION_DATA* pVersionData)
{
  _TRACE (("sdai_get_version"));

  if (pVersionData != NULL)
  {
    return (SdaiBackendDriver.Interface.get_version (&SdaiBackendDriver, pVersionData));
  }

  return (SDAI_ERR_INVALID_ARGUMENT);
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)", URL="\ref sdai_plug_unit()"];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)" ];
 *
 * ---  [ label = "Stack online" ];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_get_state()", URL="\ref sdai_get_state()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_get_state()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS, pState"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS, pState"];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit()" ];
 * @endmsc
 *
 * @desc The function sdai_get_state() returns the current status of the backend stack and a structure
 *       with additional protocol specific informations. The parameter "pState" is a bitfield indicating
 *       common status informations of the backend stacks. If "pAddStatus" is set to NULL, no additional
 *       protocol specific status informations are provided by the function. The data are only valid if
 *       the function returns with SDAI_SUCESS.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_INVALID_ARGUMENT |
 *           #SDAI_ERR_STACK_NOT_RDY
 * @param[out] pState pointer where the common stack status is stored
 * - type : U16*
 * - range: Valid values for the StackStatus: \ref StackStatus.
 * @param[out] pAddStatus pointer where the additional protocol specific data are stored.
 * - type : struct SDAI_ADDSTATUS_DATA*
 * - range: whole address space
 * @pre Before the first call of this function the sdai_init() function must be called.
 * @remarks This function is not thread save.
 */
U8 sdai_get_state (U16 *pState, struct SDAI_ADDSTATUS_DATA* pAddStatus)
{
  _TRACE (("sdai_get_state"));

  if (pState != NULL)
  {
    return (SdaiBackendDriver.Interface.get_state (&SdaiBackendDriver, pState, pAddStatus));
  }

  return (SDAI_ERR_INVALID_ARGUMENT);
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_plug_unit()", URL="\ref sdai_plug_unit()"];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_plug_unit()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS, ID"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS, ID"];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)", URL="\ref sdai_plug_unit()"];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)" ];
 *
 * ---  [ label = "Stack online" ];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit()" ];
 * @endmsc
 *
 * @desc The function sdai_plug_unit() adds a unit to the backends process model.
 *       If plugging is successfull the function returns #SDAI_SUCCESS otherwise
 *       it returns an error code. The function also returns the unique ID for the
 *       unit. Calling this function with the Unit Type #SDAI_UNIT_TYPE_PLUGGING_COMPLETE
 *       signals the SDAI that no further units will follow and the SDAI starts the backend
 *       stack. The application must store the returned ID of all units. The application needs
 *       the unit IDs for other function calls, e.g. sdai_get_data(). The parameter pCfgData is used
 *       to pass protocol specific configuration data to the backend stack. If this parameter
 *       is not used by a stack it shall be set to NULL. See also \ref sdai_io_configuration
 *       for more informations. \n \n
 *       If dynamic configuration is enabled when calling sdai_init() it is allowed to dynamically add new units or change the
 *       configuration of an already plugged unit during runtime. This can be done either by the device application or via the network.
 *       If the configuration is changed via the network, the SDAI signals the new IO configuration to the application by calling
 *       the callback function ControlReqCbk(). In that case the application has to call sdai_plug_unit() again with the received
 *       configuration parameters to take over the new IO configuration for the respective module. Also the
 *       application can add new modules to the process model after changing to ONLINE.
 *
 * @pnio The first plugged unit must always be the head unit of the device. Therefore
 *       the unit type #SDAI_UNIT_TYPE_HEAD shall be used. The protocol specific configuration data
 *       is the Ident Number of the module and submodule. If called during runtime the SDAI automatically generates
 *       a plug alarm to inform the controller. See also \ref pn_plug_unit for more informations of
 *       plugging units for PROFINET IO, especially for dynamic adding and removing of units.
 * @eip  The protocol specific configuration data is the instance ID of the assembly object. Each unit
 *       must have an unique assembly instance ID. If no instance ID is passed by the application,
 *       the EtherNet/IP stack uses a instance ID from the a manufacturer specific range. The SDAI
 *       common unit types #SDAI_UNIT_TYPE_HEAD and #SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT are not supported
 *       by EtherNet/IP. A configuration assembly object can be instantiated by selecting the unit type
 *       #SDAI_EIP_UNIT_TYPE_CONFIGURATION. The length of the configuration data is specified
 *       by parameter OutputSize. The configuration data may be received when an I/O connection
 *       is established and can be read via the function sdai_get_data(). See also \ref eip_plug_unit
 *       for more informations of plugging units for EtherNet/IP.
 * @ecat The protocol specific configuration data are the index number of the PDO mapping object and a description
 *       of the mapped application object(s). Each unit must have a unique index number. The SDAI common unit types
 *       #SDAI_UNIT_TYPE_HEAD and #SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT are not supported by EtherCAT. See also
 *       \ref ecat_plug_unit for more information of plugging units for EtherCAT.
 * @mb   The protocol specific configuration data are the start address of the first data element within a
 *       Modbus table and the number of elements assigned to this unit. The parameters InputSize and OutputSize
 *       are not used. The data length of the unit and thus the required memory space is implicitly specified by
 *       the number of elements and the used unit type. \n\n
 *       The calculation for the unit types #SDAI_MB_UNIT_TYPE_DISCRETE_INPUT and #SDAI_MB_UNIT_TYPE_COILS is: \n
 *       Data Length (Size in bytes) = (NumberElements / 8) + (NumberElements mod 8) \n \n
 *       \b Examples: \n
 *       NumberElements = 1 => Data length = 1 \n
 *       NumberElements = 8 => Data length = 1 \n
 *       NumberElements = 9 => Data length = 2 \n \n
 *       The calculation for the unit types #SDAI_MB_UNIT_TYPE_INPUT_REGISTER and #SDAI_MB_UNIT_TYPE_HOLDING_REGISTER
 *       is: \n
 *       Data Length (Size in bytes) = NumberElements * 2 \n \n
 *       \b Examples: \n
 *       NumberElements = 1 => Data length = 2 \n \n
 *       The common unit types #SDAI_UNIT_TYPE_HEAD, #SDAI_UNIT_TYPE_GENERIC_INPUT, #SDAI_UNIT_TYPE_GENERIC_OUTPUT and
 *       #SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT are not supported for Modbus TCP. See also \ref mb_plug_unit for more
 *       informations of plugging units for Modbus TCP.
 * @epl  The protocol specific configuration data are the Node ID of the communication partner and a description
 *       of the mapped application object(s). The SDAI common unit types #SDAI_UNIT_TYPE_HEAD and #SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT
 *       are not supported by Powerlink. See also \ref epl_plug_unit for more information of plugging units for Powerlink.
 * @pbdp If called during runtime the SDAI automatically generates a plug alarm to inform the DP-master (only if DPV1 mode is activated). The SDAI common unit type
 *       #SDAI_UNIT_TYPE_HEAD is not supported. See also \ref pbdp_plug_unit for more informations of plugging units for PROFIBUS DP.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_RUNTIME_PLUG |
 *           #SDAI_ERR_INVALID_ARGUMENT |
 *           #SDAI_ERR_STACK_NOT_RDY |
 *           #SDAI_ERR_MAX_UNIT_REACHED |
 *           #SDAI_ERR_UNKNOWN_UNIT_TYPE |
 *           #SDAI_ERR_SERVICE_PENDING
 * @param[in] UnitType defines the unit type or signals plugging complete.
 * - type : U8
 * - range: #SDAI_UNIT_TYPE_HEAD |
 *          #SDAI_UNIT_TYPE_GENERIC_INPUT |
 *          #SDAI_UNIT_TYPE_GENERIC_OUTPUT |
 *          #SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT |
 *          #SDAI_MB_UNIT_TYPE_DISCRETE_INPUT |
 *          #SDAI_MB_UNIT_TYPE_COILS |
 *          #SDAI_MB_UNIT_TYPE_INPUT_REGISTER |
 *          #SDAI_MB_UNIT_TYPE_HOLDING_REGISTER |
 *          #SDAI_EIP_UNIT_TYPE_CONFIGURATION |
 *          #SDAI_UNIT_TYPE_PLUGGING_COMPLETE
 * @param[in] InputSize defines the input data length of the unit in bytes.
 *            The input data of the unit will be send to a remote master.
 * - type : U8
 * - range: 0 to #SDAI_MAX_UNIT_IO_DATA_SIZE
 * @param[in] OutputSize defines the output data length of the unit in bytes.
 *            The output data of the unit will be received from a remote master.
 * - type : U8
 * - range: 0 to #SDAI_MAX_UNIT_IO_DATA_SIZE
 * @param[in,out] pId (IN):  \n If a new unit shall be plugged the value #SDAI_INVALID_ID must be set by the application.
 *                              If a previously plugged unit shall be reconfigured the Unit ID created by the first call
 *                              to sdai_plug_unit() must be set by the user. \n \n
 *                    (OUT): \n A newly created ID is returned which uniquely identifies the unit. If the unit has only been
 *                              reconfigured the returned ID is equal to the ID specified by the calling application.
 * - type : U32*
 * - range: whole address space
 * @param[in] pCfgData protocol specific configuration data
 * - type : SDAI_CFG_DATA*
 * - range: whole address space
 * @pre Before the first call of this function the sdai_init() function must be called.
 * @remarks If static configuration is enabled it is not possible to plug additional units after calling sdai_plug_unit() with
 *          the unit type #SDAI_UNIT_TYPE_PLUGGING_COMPLETE. This function is not thread save.
 */
U8 sdai_plug_unit (U8 UnitType, U8 InputSize, U8 OutputSize, U32* pId, const struct SDAI_CFG_DATA* pCfgData)
{
  static struct SDAI_CFG_DATA_EXT CfgDataExt;

  _TRACE (("sdai_plug_unit"));

  if (pId != NULL && ((pCfgData != NULL) || (UnitType == SDAI_UNIT_TYPE_PLUGGING_COMPLETE)))
  {
    if (!(SdaiBackendDriver.ApplicationInitData.Info.Flags & SDAI_DYNAMIC_IO_CONFIGURATION) &&
        SdaiBackendDriver.Interface.plugging_complete (&SdaiBackendDriver)                  )
    {
      return (SDAI_ERR_RUNTIME_PLUG);
    }

    /* map old structure to new one */
    if (pCfgData != NULL)
    {
      CfgDataExt.InterfaceType          = pCfgData->InterfaceType;
      CfgDataExt.PeripheralOffsetInput  = SDAI_PERIPHERAL_OFFSET_DPRAM;
      CfgDataExt.PeripheralOffsetOutput = SDAI_PERIPHERAL_OFFSET_DPRAM;
      memcpy (&CfgDataExt.UseAs, &pCfgData->UseAs, sizeof (CfgDataExt.UseAs));
    }

    return (SdaiBackendDriver.Interface.plug_unit (&SdaiBackendDriver, UnitType, InputSize, OutputSize, pId, &CfgDataExt));
  }

  return (SDAI_ERR_INVALID_ARGUMENT);
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_plug_unit_ext()", URL="\ref sdai_plug_unit_ext()"];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_plug_unit()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS, ID"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS, ID"];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)", URL="\ref sdai_plug_unit()"];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)" ];
 *
 * ---  [ label = "Stack online" ];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit()" ];
 * @endmsc
 *
 * @desc The function sdai_plug_unit_ext() adds a peripheral-unit to the backends process model. It shall be used
 *       if the performance optimization of the fieldbus subsystem is enabled and the peripheral unit provides an
 *       address- and data-bus-interface which can be connected directly to the "peripheral bridge" of the fieldbus
 *       subsystem. The provided data of the peripheral-unit have to fullfil the requirement of the respective protocols.
 *       If plugging is successfull the function returns #SDAI_SUCCESS otherwise
 *       it returns an error code. The function also returns the unique ID for the
 *       unit. Calling this function with the Unit Type #SDAI_UNIT_TYPE_PLUGGING_COMPLETE
 *       shall be avoided. Use function \ref sdai_plug_unit() instead.
 *       The application must store the returned ID of all units. The application needs
 *       the unit IDs for other function calls, e.g. sdai_diagnosis_request(). The parameter pCfgData is used
 *       to pass protocol specific configuration data to the backend stack. If this parameter
 *       is not used by a stack it shall be set to NULL. See also \ref sdai_io_configuration
 *       for more informations. \n \n
 *       If dynamic configuration is enabled when calling sdai_init() it is allowed to dynamically add new units or change the
 *       configuration of an already plugged unit during runtime. This can be done either by the device application or via the network.
 *       If the configuration is changed via the network, the SDAI signals the new IO configuration to the application by calling
 *       the callback function ControlReqCbk(). In that case the application has to call sdai_plug_unit_ext() again with the received
 *       configuration parameters to take over the new IO configuration for the respective module. Also the
 *       application can add new modules to the process model after changing to ONLINE.
 *       \attention A peripheral-input unit shall provide the data status appended at the end of the process data. A peripheral-output
 *                  unit has to accept the data status appended at the end of the output data.
 *
 * @pnio The first plugged unit must always be the head unit of the device (use \ref sdai_plug_unit() for this). The protocol
 *       specific configuration data is the Ident Number of the module and submodule. If called during runtime the SDAI
 *       automatically generates a plug alarm to inform the controller. See also \ref pn_plug_unit for more informations of
 *       plugging units for PROFINET IO, especially for dynamic adding and removing of units.
 * @eip  For future use.
 * @ecat The protocol specific configuration data are the index number of the PDO mapping object and a description
 *       of the mapped application object(s). Each unit must have a unique index number. The SDAI common unit types
 *       #SDAI_UNIT_TYPE_HEAD and #SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT are not supported by EtherCAT. See also
 *       \ref ecat_plug_unit for more information of plugging units for EtherCAT.
 * @mb   For future use.
 * @epl  For future use.
 * @pbdp For future use.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_RUNTIME_PLUG |
 *           #SDAI_ERR_INVALID_ARGUMENT |
 *           #SDAI_ERR_STACK_NOT_RDY |
 *           #SDAI_ERR_MAX_UNIT_REACHED |
 *           #SDAI_ERR_UNKNOWN_UNIT_TYPE |
 *           #SDAI_ERR_SERVICE_PENDING
 * @param[in] UnitType defines the unit type or signals plugging complete.
 * - type : U8
 * - range: #SDAI_UNIT_TYPE_GENERIC_INPUT |
 *          #SDAI_UNIT_TYPE_GENERIC_OUTPUT |
 *          #SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT |
 * @param[in] InputSize defines the input data length of the unit in bytes.
 *            The input data of the unit will be send to a remote master.
 * - type : U8
 * - range: 0 to #SDAI_MAX_UNIT_IO_DATA_SIZE
 * @param[in] OutputSize defines the output data length of the unit in bytes.
 *            The output data of the unit will be received from a remote master.
 * - type : U8
 * - range: 0 to #SDAI_MAX_UNIT_IO_DATA_SIZE
 * @param[in,out] pId (IN):  \n If a new unit shall be plugged the value #SDAI_INVALID_ID must be set by the application.
 *                              If a previously plugged unit shall be reconfigured the Unit ID created by the first call
 *                              to sdai_plug_unit() must be set by the user. \n \n
 *                    (OUT): \n A newly created ID is returned which uniquely identifies the unit. If the unit has only been
 *                              reconfigured the returned ID is equal to the ID specified by the calling application.
 * - type : U32*
 * - range: whole address space
 * @param[in] pCfgData protocol specific configuration data
 * - type : SDAI_CFG_DATA*
 * - range: whole address space
 * @pre Before the first call of this function the sdai_init() function must be called.
 * @remarks If static configuration is enabled it is not possible to plug additional units after calling sdai_plug_unit() with
 *          the unit type #SDAI_UNIT_TYPE_PLUGGING_COMPLETE. This function is not thread save.
 */
U8 sdai_plug_unit_ext (U8 UnitType, U8 InputSize, U8 OutputSize, U32* pId, const struct SDAI_CFG_DATA_EXT* pCfgData)
{
  _TRACE (("sdai_plug_unit_ext"));

  if (pId != NULL && ((pCfgData != NULL) || (UnitType == SDAI_UNIT_TYPE_PLUGGING_COMPLETE)))
  {
    if (!(SdaiBackendDriver.ApplicationInitData.Info.Flags & SDAI_DYNAMIC_IO_CONFIGURATION) &&
        SdaiBackendDriver.Interface.plugging_complete (&SdaiBackendDriver)                  )
    {
      return (SDAI_ERR_RUNTIME_PLUG);
    }

    return (SdaiBackendDriver.Interface.plug_unit (&SdaiBackendDriver, UnitType, InputSize, OutputSize, pId, pCfgData));
  }

  return (SDAI_ERR_INVALID_ARGUMENT);
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_plug_unit()", URL="\ref sdai_plug_unit()"];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_plug_unit()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS, ID"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS, ID"];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)", URL="\ref sdai_plug_unit()"];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)" ];
 *
 * ---  [ label = "Stack online" ];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_pull_unit()", URL="\ref sdai_pull_unit()"];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_pull_unit()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS, ID"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS, ID"];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit()" ];
 * @endmsc
 *
 * @desc The function sdai_pull_unit() removes a unit from the backends process model.
 *       If the removal is successfull the function returns #SDAI_SUCCESS otherwise it returns an error code.
 *       Removing a unit is only supported if dynamic configuration has been enabled when calling sdai_init(),
 *       see also \ref io_cfg_dynamic.
 *
 * @pnio In that case the SDAI automatically generates a pull alarm to inform the controller. The head unit can not be removed.
 * @eip  not supported by protocol
 * @ecat not supported by protocol
 * @mb   not supported by protocol
 * @epl  not supported by protocol
 * @pbdp In that case the SDAI automatically generates a pull alarm to inform the DP-master (only if DPV1 mode is activated).
 *       In addition the SDAI automatically signals the missing module via a "module status" diagnosis. <b>This function is
 *       not yet supported for PROFIBUS</b>, see \ref pbdp_features.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_RUNTIME_PLUG |
 *           #SDAI_ERR_INVALID_ARGUMENT |
 *           #SDAI_ERR_STACK_NOT_RDY |
 *           #SDAI_ERR_SERVICE_PENDING |
 *           #SDAI_ERR_COM_NOT_SUPPORTED
 * @param[in] Id Specifies the unit which shall be removed.
 * - type : U32
 * - range: 0 to 0xFFFFFFFF
 * @pre Before the first call of this function the sdai_init() function must be called. Additionally the unit has been plugged
 *      before by calling sdai_plug_unit().
 * @remarks This function is not thread save.
 */
U8 sdai_pull_unit (U32 Id)
{
  _TRACE (("sdai_pull_unit"));

  if (SdaiBackendDriver.ApplicationInitData.Info.Flags & SDAI_DYNAMIC_IO_CONFIGURATION)
  {
    return (SdaiBackendDriver.Interface.pull_unit (&SdaiBackendDriver, Id));
  }

  return (SDAI_ERR_COM_NOT_SUPPORTED);
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 *
 * ...;
 * "Application"=>"SDAI::API" [ label = "sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)", URL="\ref sdai_plug_unit()"];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)" ];
 *
 * ---  [ label = "Stack online" ];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_get_data()", URL="\ref sdai_get_data()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_get_data()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS, pStatus, pData"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS, pStatus, pData"];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit()" ];
 * @endmsc
 *
 * @desc The function sdai_get_data() reads the data and the status of the Unit specified by the parameter Id.
 *       The length of the returned data is the length specified by the previous call of sdai_plug_unit() for
 *       this unit. This function can be called to read the output data received from the master. The callback
 *       function DataCbk (see \ref SDAI_CALLBACKS) informs the application when new data are received or the
 *       data status has been changed. This enables the application to read new data event-driven. Also this function
 *       can be used to read back the input data previously set by calling sdai_set_data(). For units with the type
 *       #SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT only the output data are copied to the passed data buffer. While
 *       there is no connection to the controller the status of the output units are set to #SDAI_DATA_STATUS_INVALID.
 *       If valid output data are received from the master the status is set to #SDAI_DATA_STATUS_VALID.
 * @pnio When there is a cyclic connection to the controller, the status of a output unit is the received status
 *       from the controller.
 * @eip  When calling this function for a input unit the parameter pStatus is not used and shall be
 *       ignored by the application. The status of a output unit is only set to #SDAI_DATA_STATUS_VALID when
 *       the connection is in RUN mode. If the connection is in IDLE mode or not established the status is
 *       set to #SDAI_DATA_STATUS_INVALID. This function can also be used to read the configuration data received
 *       from the scanner during the connection establishment. To allow the receiption of configuration data a
 *       configuration unit needs to be plugged. The status of the configuration unit is set to #SDAI_DATA_STATUS_VALID
 *       as long as a cyclic connection to the configuration unit exists. *
 * @ecat The status for output units will only be Valid if the EtherCAT slave is in state Operational (see also function \ref sdai_get_state()).
 *       For example if the Sync Manager watchdog timer expires the EtherCAT state changes to Safe-Operational and therefore
 *       the output data status is set to Invalid.
 * @mb   This function can be called for all Modbus specific unit types (#SDAI_MB_UNIT_TYPE_DISCRETE_INPUT,
 *       #SDAI_MB_UNIT_TYPE_COILS, #SDAI_MB_UNIT_TYPE_INPUT_REGISTER and #SDAI_MB_UNIT_TYPE_HOLDING_REGISTER).
 * @epl  The status for output units (RxPDOs) is mapped to the RD flag of the received Poll request or Poll response frame.
 *       If the received RD flag is set the status is set to Valid and otherwise to Invalid.
 * @pbdp The status for output and input/output units will only be set to Valid if the slave is in state Data-Exchange \b and
 *       the master is in state Operate (see also function \ref sdai_get_state()). The output data status is set to Invalid
 *       in the following cases:
 *       - After a failure of the master the process data watchdog of the slave has been expired.
 *       - The master has terminated the cyclic connection.
 *       - Cyclic data exchange is executed with the master but the master is in state Clear.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_INVALID_ARGUMENT |
 *           #SDAI_ERR_NOSUCHUNIT |
 *           #SDAI_ERR_STACK_NOT_RDY
 * @param[in] Id Id for which unit the data shall be read
 * - type : U32
 * - range: 0 to 0xFFFFFFFF
 * @param[out] pStatus Pointer to the read data status
 * - type : U8*
 * - range: whole address range is valid
 * @param[out] pData Pointer to the read data block
 * - type : U8*
 * - range: whole address range
 * @pre Before the first call of this function the sdai_init() function must be called.
 * @remarks This function is not thread save.
 */
U8 sdai_get_data (U32 Id, U8* pStatus, U8* pData)
{
  _TRACE (("sdai_get_data"));

  if ( (pStatus != NULL) && (pData != NULL) )
  {
    return (SdaiBackendDriver.Interface.get_data (&SdaiBackendDriver, Id, pStatus, pData));
  }

  return (SDAI_ERR_INVALID_ARGUMENT);
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 *
 * ...;
 * "Application"=>"SDAI::API" [ label = "sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)", URL="\ref sdai_plug_unit()"];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)" ];
 *
 * ---  [ label = "Stack online" ];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_set_data()", URL="\ref sdai_set_data()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_set_data()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit()" ];
 * @endmsc
 *
 * @desc The function sdai_set_data() sets the data and the status of the Unit specified by the parameter Id.
 *       The length of the returned data is the length specified by the previous call of sdai_plug_unit() for
 *       this unit. Calling this function is only possible for input and input/output units.
 * @pnio The parameter Status is transmitted to the controller over the network.
 * @eip The parameter Status is not used.
 * @ecat During startup the SDAI application must set the status for all plugged input units to Valid. If this is not
 *       the case the cyclic data exchange is not started and the error SDAI_ECAT_STATUS_CODE_NO_VALID_INPUTS_AVAILABLE
 *       is signalled. When the status for all plugged input units is set to Valid for the first time the cyclic data
 *       exchange is started. During cyclic data exchange the Status parameter is ignored. This means the EtherCAT protocol
 *       stack continues the cyclic data exchange also if the user sets the status for one or more input units to Invalid.
 *       If needed the status of a input unit (TxPDO) can be signalled to the EtherCAT master via a PDO Parameter object.
 *       The corresponding PDO Parameter object must be implemented by the SDAI application.
 * @mb   The parameter Status is not used. This function can be called to set input data for all Modbus specific
 *       unit types (#SDAI_MB_UNIT_TYPE_DISCRETE_INPUT, #SDAI_MB_UNIT_TYPE_COILS, #SDAI_MB_UNIT_TYPE_INPUT_REGISTER and
 *       #SDAI_MB_UNIT_TYPE_HOLDING_REGISTER).
 * @epl  The Status set by the application is mapped to the RD flag of the Poll Response.
 * @pbdp During startup the SDAI application must set the status for all plugged input and input/output units to Valid. If this is not
 *       the case the cyclic data exchange is not started and the slave signals "Not ready for data exchange" via the diagnosis response
 *       frame to the DP-master. Only after the status for all plugged input and input/output units is set to Valid for the first time the cyclic data
 *       exchange is started. The Status set by the application is mapped to the identifier-related diagnosis block and to the
 *       "Module Status" block (see also \ref pbdp_diag_alarm). Thus it can be used during cyclic data exchange to signal that the
 *       respective module can not provide valid input data. Although the status of one or more modules has been set to Invalid
 *       the cyclic data exchange continues. If the status is Invalid the input data set by the application for the respective unit are
 *       not transferred to the DP-master. In that case the last Valid data are transferred until the status is set to Valid again.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_INVALID_ARGUMENT |
 *           #SDAI_ERR_NOSUCHUNIT |
 *           #SDAI_ERR_STACK_NOT_RDY
 * @param[in] Id Id for which unit the data shall be set
 * - type : U32
 * - range: 0 to 0xFFFFFFFF
 * @param[in] Status data status to be written
 * - type : U8
 * - range: #SDAI_DATA_STATUS_VALID |
 *          #SDAI_DATA_STATUS_INVALID |
 *          protocol dependent
 * @param[in] pData Pointer to the data block to be written
 * - type : U8*
 * - range: whole address space
 * @pre Before the first call of this function the sdai_init() function must be called.
 * @post If #SDAI_DATA_CONSISTENCY_IO_IMAGE is set, sdai_process_data_update_finished() has to be called to takeover the changed input data.
 * @remarks This function is not thread save.
 */
U8 sdai_set_data (U32 Id, U8 Status, const U8* pData)
{
  _TRACE (("sdai_set_data"));

  return (SdaiBackendDriver.Interface.set_data (&SdaiBackendDriver, Id, Status, pData));
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_set_data()", URL="\ref sdai_set_data()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_set_data()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 * "Application"=>"SDAI::API"    [ label = "sdai_set_data()", URL="\ref sdai_set_data()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_set_data()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_process_data_update_finished()", URL="\ref sdai_process_data_update_finished()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_process_data_update_finished()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ---  [ label = "process data forwarded to the fieldbus stack" ];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit()" ];
 * @endmsc
 *
 * @desc The function sdai_process_data_update_finished() notifies the fieldbus stack about updated IO data.
 *       This is necessary in cases where the application wants to ensure consistency of the whole process
 *       data image for all IO units. This function is not needed for protocols or applications which
 *       uses only one input unit. Currently it is only supported by the protocols listed below.
 *
 * @pnio This function is supported by the PROFINET protocol.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_COM_NOT_SUPPORTED
 * @note The process data image consistancy is not supported by all platforms
 * @pre Before the first call of this function the sdai_init() function must be called.
 * @remarks This function is not thread save.
 */
U8 sdai_process_data_update_finished (void)
{
  _TRACE (("sdai_process_data_update_finished"));

  return (SdaiBackendDriver.Interface.process_data_update_finished(&SdaiBackendDriver));
}

/*===========================================================================*/

/**
 * Alarm handling with AlarmAckCbk:
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 *
 * ...;
 * "Application"=>"SDAI::API" [ label = "sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)", URL="\ref sdai_plug_unit()"];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)" ];
 *
 * ---  [ label = "Stack online" ];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_alarm_request()", URL="\ref sdai_alarm_request()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_alarm_request()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_alarm_request()", URL="\ref sdai_alarm_request()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_alarm_request()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_ERR_SERVICE_PENDING"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_ERR_SERVICE_PENDING"];
 *
 * ...;
 * "Application"<<="SDAI::DRIVER" [ label = "AlarmAckCbk()"];
 * "Application"=>"SDAI::API"    [ label = "sdai_alarm_request()", URL="\ref sdai_alarm_request()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_alarm_request()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit()" ];
 * @endmsc
 *
 * Alarm handling without AlarmAckCbk:
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)", URL="\ref sdai_plug_unit()"];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)" ];
 *
 * ---  [ label = "Stack online" ];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_alarm_request()", URL="\ref sdai_alarm_request()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_alarm_request()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_alarm_request()", URL="\ref sdai_alarm_request()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_alarm_request()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_ERR_SERVICE_PENDING"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_ERR_SERVICE_PENDING"];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_alarm_request()", URL="\ref sdai_alarm_request()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_alarm_request()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_ERR_SERVICE_PENDING"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_ERR_SERVICE_PENDING"];
 *
 * ...;
 *
 * ---  [ label = "Alarm acknowledged by remote controller application" ];
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_alarm_request()", URL="\ref sdai_alarm_request()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_alarm_request()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit()" ];
 * @endmsc
 *
 * @desc The function sdai_alarm_request() sends an alarm request for the module corresponding to the given Id.
 *       If the application has not registered the AlarmAckCbk and wants to send a second alarm of the same type and
 *       priority it can call this function periodically until the return value signals SDAI_SUCCESS.
 *       See also \ref sdai_alarm for more informations.
 * @pnio There can only be one outstanding alarm for each priority regardless of the alarm type. If there are
 *       more then one outstanding alarm for a priority the application must handle the queuing of the alarms.
 *       For sending a Diagnostic-Alarm the function sdai_diagnosis_request() should be used. The parameter AlarmType
 *       may be set to a manufacturer specific value to transfer a manufacturer specific alarm messages.
 * @eip  not supported by protocol
 * @ecat not supported by protocol
 * @mb   not supported by protocol
 * @epl  not supported by protocol
 * @pbdp This function can only be used if DPV1 mode is selected by the master (see also #SDAI_PBDP_CONTROL_PRM_DATA).
 *       It is used to signal alarm messages to the DP-master. For sending a Diagnostic-Alarm the function sdai_diagnosis_request()
 *       should be used. To send manufacturer specific alarm messages the parameter AlarmType of structure #SDAI_ALARM_REQ must
 *       be set to a manufacturer specific value (value range: 32 - 126). See also \ref pbdp_diag_alarm for more informations.
 *       <b>This function is not yet supported for PROFIBUS</b>, see \ref pbdp_features.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_INVALID_ARGUMENT |
 *           #SDAI_ERR_SERVICE_PENDING |
 *           #SDAI_ERR_NOSUCHUNIT |
 *           #SDAI_ERR_INVALID_SIZE |
 *           #SDAI_ERR_STACK_NOT_RDY
 * @param[in] pAlarm Pointer to a buffer holding the alarm request data
 * - type : const struct SDAI_ALARM_REQ*
 * - range: whole address space
 * @pre Before the first call of this function the sdai_init() function must be called.
 * @post If the functions returns with #SDAI_ERR_SERVICE_PENDING the alarm was not send because the service
 *       interface is busy. The application has to resend the alarm request later
 * @remarks This function is not thread save.
 */
U8 sdai_alarm_request (const struct SDAI_ALARM_REQ* pAlarm)
{
  _TRACE (("sdai_alarm_request"));

  if (pAlarm != NULL)
  {
    if (pAlarm->Length <= (SERVICE_SIZE - sizeof (struct SDAI_ALARM_REQ)))
    {
      return (SdaiBackendDriver.Interface.alarm_request (&SdaiBackendDriver, pAlarm));
    }
  }

  return (SDAI_ERR_INVALID_ARGUMENT);
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 *
 * ...;
 * "Application"=>"SDAI::API" [ label = "sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)", URL="\ref sdai_plug_unit()"];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)" ];
 *
 * ---  [ label = "Stack online" ];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_diagnosis_request()", URL="\ref sdai_diagnosis_request()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_diagnosis_request()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit()" ];
 * @endmsc
 *
 * @desc The function sdai_diagnosis_request() sends diagnostic informations for the module corresponding to the given Id.
 *       See also \ref sdai_diagnosis for more informations.
 * @pnio Will be used to transfer Diagnostic-Alarms to the controller. The acknowledgement of this alarm is
 *       automatically handled by the SDAI.
 * @eip  not supported by protocol
 * @ecat See also \ref ecat_diag_alarm for more informations. The structure #SDAI_ECAT_DIAGNOSIS_DATA holds additional EtherCAT specific
 *       diagnostic data. This structure must be attached directly at the end of the structure #SDAI_DIAGNOSIS_REQ.
 * @mb   not supported by protocol
 * @epl  not supported by protocol
 * @pbdp See also \ref pbdp_diag_alarm for more informations. The structure #SDAI_PBDP_DIAGNOSIS_DATA holds additional PROFIBUS specific
 *       diagnostic data. This structure must be attached directly at the end of the structure #SDAI_DIAGNOSIS_REQ. The following diagnostic
 *       data can be send with this function:
 * - Transfer Diagnostic-Alarms to the DP-Master. In that case the acknowledgement of this alarm is automatically handled by the SDAI.
 *   Diagnostic-Alarms are only supported if DPV1 mode is selected by the master (see also #SDAI_PBDP_CONTROL_PRM_DATA). <b>Diagnostic-Alarms are
 *   not yet supported for PROFIBUS</b>, see \ref pbdp_features.
 * - Signal a channel-related diagnosis to the DP-Master. The SDAI creates a channel-related diagnosis block if the StatusSpecifier is set to
 *   #SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS for a new error source. The error source is specified by the parameters Id (Module/Slot), Type (Error) and Channel.
 *   The SDAI removes the channel-related diagnosis block if the StatusSpecifier is set to #SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_DISAPPEARS for the
 *   specified error source. The maximum number of parallel channel-related diagnosis blocks is limited by the size of the diagnosis response frame
 *   which is 244 bytes. If a channel-related diagnosis is created by the application and the maximum size is reached the error #SDAI_ERR_MAX_DIAG_REACHED
 *   is returned. In addition a diagnosis overflow error is signaled to the DP-master.
 * - Signal a device-related diagnosis to the DP-Master. This is only allowed if DPV1 mode is \b not selected. The parameters Id and Type are ignored
 *   for the device-related diagnosis. Thus only one error source is allowed for this diagnosis block.
 * - Transfer status messages to the DP-Master. The parameter StatusSpecifier can be additionally set to #SDAI_PBDP_STATUS_SPECIFIER_NONE to signal "No further differentiation".
 *   Status messages are only supported if DPV1 mode is selected by the master (see also #SDAI_PBDP_CONTROL_PRM_DATA). Status messages of type "module status" are automatically
 *   created and managed by the SDAI. <b>All other Status Messages are not yet supported for PROFIBUS</b>, see \ref pbdp_features.\n\n
 * \b Note: Channel-related diagnosis, diagnostic alarms and Status messages can only be created after a valid Chk-Cfg frame is received from the DP-master. Only at
 * that point of time the used module configuration is defined which is necessary for creating these diagnosis blocks. If the application creates a
 * channel-related diagnosis or a diagnostic alarm before receiption of the Chk-Cfg frame the error #SDAI_ERR_WRONG_STATE is returned.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS              |
 *           #SDAI_ERR_SERVICE_PENDING  |
 *           #SDAI_ERR_NOSUCHUNIT       |
 *           #SDAI_ERR_INVALID_ARGUMENT |
 *           #SDAI_ERR_INVALID_SIZE     |
 *           #SDAI_ERR_STACK_NOT_RDY    |
 *           #SDAI_ERR_WRONG_STATE      |
 *           #SDAI_ERR_MAX_DIAG_REACHED
 * @param[in] pDiagnosis Pointer to a buffer holding the diagnostic data.
 * - type : struct SDAI_DIAGNOSIS_REQ*
 * - range: whole address space
 * @pre Before the first call of this function the sdai_init() function must be called.
 * @post If the functions returns with #SDAI_ERR_SERVICE_PENDING the diagnosis was not send because the service
 *       interface is busy. The application has to resend the diagnosis request later
 * @remarks This function is not thread save.
 */
U8 sdai_diagnosis_request (const struct SDAI_DIAGNOSIS_REQ* pDiagnosis)
{
  _TRACE (("sdai_diagnosis_request"));

  if (pDiagnosis != NULL)
  {
    if (pDiagnosis->Length <= (SERVICE_SIZE - sizeof (struct SDAI_DIAGNOSIS_REQ)))
    {
      return (SdaiBackendDriver.Interface.diagnosis_request (&SdaiBackendDriver, pDiagnosis));
    }
  }

  return (SDAI_ERR_INVALID_ARGUMENT);
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 *
 * ...;
 * "Application"=>"SDAI::API" [ label = "sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)", URL="\ref sdai_plug_unit()"];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)" ];
 *
 * ---  [ label = "Stack online" ];
 * ...;
 * "Application"<<="SDAI::DRIVER" [ label = "WriteReqCbk()"];
 * "Application"=>"SDAI::API"    [ label = "sdai_write_response()", URL="\ref sdai_write_response()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_write_response()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit()" ];
 * @endmsc
 *
 * @desc The function sdai_write_response() must be called by the application
 *       after processing of an write request to signal the processing result.
 * @pnio The stack waits 2 seconds after the call of the WriteReqCbk() on the response
 *       from the application. After this time the stack cancels the write request and a later
 *       response from the application is discarded.
 * @eip  The stack waits 5 seconds after the call of the WriteReqCbk() on the response
 *       from the application. After this time the stack cancels the write request and automatically
 *       sends a error response to the client. A later response from the application is discarded.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_INVALID_ARGUMENT |
 *           #SDAI_ERR_STACK_NOT_RDY |
 *           #SDAI_ERR_SERVICE_PENDING
 * @param[in] pWrite The data for the write response
 * - type : struct SDAI_WRITE_RES*
 * - range: whole address space
 * @pre Before the first call of this function the sdai_init() function must be called.
 * @post If the functions returns with #SDAI_ERR_SERVICE_PENDING the service was not send because the service
 *       interface is busy. The application has to resend the write response later
 * @remarks This function is not thread save.
 */
U8 sdai_write_response (const struct SDAI_WRITE_RES* pWrite)
{
  _TRACE (("sdai_write_response"));

  if (pWrite != NULL)
  {
    return (SdaiBackendDriver.Interface.write_response (&SdaiBackendDriver, pWrite));
  }

  return (SDAI_ERR_INVALID_ARGUMENT);
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 *
 * ...;
 * "Application"=>"SDAI::API" [ label = "sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)", URL="\ref sdai_plug_unit()"];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)" ];
 *
 * ---  [ label = "Stack online" ];
 * ...;
 * "Application"<<="SDAI::DRIVER" [ label = "ReadReqCbk()"];
 * "Application"=>"SDAI::API"    [ label = "sdai_read_response()", URL="\ref sdai_read_response()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_read_response()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit()" ];
 * @endmsc
 *
 * @desc The function sdai_read_response() must be called by the application
 *       after processing of an read request to send the read data.
 * @pnio The stack waits 2 seconds after the call of the ReadReqCbk() on the response
 *       from the application. After this time the stack cancels the read request and a later
 *       response from the application is discarded.
 * @eip  The stack waits 5 seconds after the call of the ReadReqCbk() on the response
 *       from the application. After this time the stack cancels the read request and automatically
 *       sends a error response to the client. A later response from the application is discarded.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_INVALID_ARGUMENT
 * @param[in] pRead The data for the read response
 * - type : struct SDAI_READ_RES*
 * - range: whole address space
 * @pre Before the first call of this function the sdai_init() function must be called.
 * @post If the functions returns with #SDAI_ERR_SERVICE_PENDING the service was not send because the service
 *       interface is busy. The application has to resend the read response later
 * @remarks This function is not thread save.
 */
U8 sdai_read_response (const struct SDAI_READ_RES* pRead)
{
  _TRACE (("sdai_read_response"));

  if (pRead != NULL)
  {
    if (pRead->Length <= (SERVICE_SIZE - sizeof (*pRead)))
    {
      return (SdaiBackendDriver.Interface.read_response(&SdaiBackendDriver, pRead));
    }
  }

  return (SDAI_ERR_INVALID_ARGUMENT);
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 *
 * ...;
 * "Application"=>"SDAI::API" [ label = "sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)", URL="\ref sdai_plug_unit()"];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)" ];
 *
 * ---  [ label = "Stack online" ];
 * ...;
 * "Application"<<="SDAI::DRIVER" [ label = "ControlReqCbk()"];
 * "Application"=>"SDAI::API"    [ label = "sdai_control_response()", URL="\ref sdai_control_response()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_control_response()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit()" ];
 * @endmsc
 *
 * @desc The function sdai_control_response() must be called by the application
 *       after processing of an control request to signal the processing result.
 * @pnio This function is not used by the Profinet IO stack. The stack expect the
 *       application can perform any incoming indication without further effort.
 * @eip  The stack waits 5 seconds after the call of the ControlReqCbk() on the response
 *       from the application. After this time the stack cancels the control request and automatically
 *       sends a error response to the client. A later response from the application is discarded.
 * @ecat not supported by protocol
 * @mb   not supported by protocol
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_INVALID_ARGUMENT
 * @param[in] pControl The data for the control response
 * - type : struct SDAI_CONTROL_RES*
 * - range: whole address space
 * @pre Before the first call of this function the sdai_init() function must be called.
 * @post If the functions returns with #SDAI_ERR_SERVICE_PENDING the service was not send because the service
 *       interface is busy. The application has to resend the control response later
 * @remarks This function is not thread save.
 */
U8 sdai_control_response (const struct SDAI_CONTROL_RES* pControl)
{
  _TRACE (("sdai_read_response"));

  if (pControl != NULL)
  {
    if (pControl->Length <= (SERVICE_SIZE - sizeof (*pControl)))
    {
      return (SdaiBackendDriver.Interface.control_response(&SdaiBackendDriver, pControl));
    }
  }

  return (SDAI_ERR_INVALID_ARGUMENT);
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_init_watchdog()", URL="\ref sdai_init_watchdog()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init_watchdog()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ---  [ label = "watchdog initialized" ];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_init_watchdog()", URL="\ref sdai_init_watchdog()" ];
 * "Application"<<"SDAI::API"    [ label = "SDAI_ERR_INTERNAL"];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit()" ];
 * @endmsc
 *
 * @desc The function sdai_init_watchdog() initializes the watchdog mechanism. It supervises the stack and application CPU.
 *       The WatchdogFactor is the time in multiples of 100ms after which the watchdog, if it is not triggered again, expires.
 *
 * @ecat This function is not supported by the EtherCAT protocol. However, the Softing EtherCAT
 *       stack supports the PDI watchdog and the Process data watchdog. But these watchdogs are configured
 *       and enabled via the EtherCAT network by a EtherCAT configuration tool.
 * @epl  This function is not supported by the Powerlink protocol.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_INTERNAL |
 *           #SDAI_ERR_INVALID_ARGUMENT
 * @param[in] WatchdogFactor Defines the watchdog timeout between 1=100ms and 10=1000ms
 * - type : const U16
 * - range: 1 - 10
 * @note The watchdog functionality is not supported by all platforms
 * @pre Before the first call of this function the sdai_init() function must be called.
 * @post To activate the watchdog the function sdai_check_and_trigger_watchdog() must be called once.
 * @remarks This function is not thread save.
 */
U8 sdai_init_watchdog (const U16 WatchdogFactor)
{
  U8 Result = SDAI_ERR_INVALID_ARGUMENT;


  _TRACE (("sdai_init_watchdog"));

  if (WatchdogEnabled == 1)
  {
    return (SDAI_ERR_INTERNAL);
  }

  if ((WatchdogFactor >= 1) && (WatchdogFactor <= 10))
  {
    Result = SdaiBackendDriver.Interface.init_watchdog(&SdaiBackendDriver, WatchdogFactor);

    if (Result == SDAI_SUCCESS)
    {
      WatchdogEnabled = 1;
      return (Result);
    }
  }

  return (Result);
}

/*===========================================================================*/

/**
 * Watchdog handling: Application watchdog expired
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_init_watchdog()", URL="\ref sdai_init_watchdog()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init_watchdog()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_check_and_trigger_watchdog()", URL="\ref sdai_check_and_trigger_watchdog()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_check_and_trigger_watchdog()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ---  [ label = "watchdog running" ];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_check_and_trigger_watchdog()", URL="\ref sdai_check_and_trigger_watchdog()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_check_and_trigger_watchdog()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_get_state()", URL="\ref sdai_get_state()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_get_state()" ];
 *
 * ---  [ label = "SDAI_STATE_MASK_APPL_WATCHDOG_EXPIRED bit set" ];
 * ---  [ label = "trigger reset of application CPU" ];
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 * @endmsc
 *
 * Watchdog handling: Stack watchdog expired
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_init_watchdog()", URL="\ref sdai_init_watchdog()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init_watchdog()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_check_and_trigger_watchdog()", URL="\ref sdai_check_and_trigger_watchdog()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_check_and_trigger_watchdog()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ---  [ label = "watchdog running" ];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_check_and_trigger_watchdog()", URL="\ref sdai_check_and_trigger_watchdog()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_check_and_trigger_watchdog()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_check_and_trigger_watchdog()", URL="\ref sdai_check_and_trigger_watchdog()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_check_and_trigger_watchdog()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_ERR_WATCHDOG_EXPIRED"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_ERR_WATCHDOG_EXPIRED"];
 *
 * ---  [ label = "stack watchdog expired" ];
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit()" ];
 *
 * ---  [ label = "trigger hardware reset of stack CPU" ];
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 * @endmsc
 *
 * @desc The function sdai_check_and_trigger_watchdog() returns the state of the stack watchdog and triggers the application
 *       watchdog. The first call of the function after initialization activates the watchdog. After starting the watchdog this
 *       function must be called in the half time interval passed to sdai_watchdog_init(). For example if sdai_init_watchdog()
 *       is called with a timeout of 1 (= 100ms) the function sdai_check_and_trigger_watchdog must be called every 50 ms. Otherwise the
 *       application watchdog expires and the stack sets the #SDAI_STATE_MASK_APPL_WATCHDOG_EXPIRED bit in the status and triggers a
 *       reset of the stack CPU. If the application is still running and detects that the stack signals application watchdog expired
 *       it has to call sdai_deinit() and then start the stack again with sdai_init().
 *       After starting the watchdog there is no way to stop it.
 *
 * @ecat This function is not supported by the EtherCAT protocol. However, the Softing EtherCAT
 *       stack supports the PDI watchdog and the Process data watchdog. But these watchdogs are configured
 *       and enabled via the EtherCAT network by a EtherCAT configuration tool.
 * @epl  This function is not supported by the Powerlink protocol.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_COM_NOT_SUPPORTED |
 *           #SDAI_ERR_WATCHDOG_EXPIRED
 * @note The watchdog functionality is not supported by all platforms
 * @pre Before the first call of this function the sdai_init() and the sdai_init_watchdog() function must be called.
 * @remarks This function is not thread save.
 */
U8 sdai_check_and_trigger_watchdog (void)
{
  _TRACE (("sdai_check_and_trigger_watchdog"));

  return (SdaiBackendDriver.Interface.check_trigger_watchdog(&SdaiBackendDriver));
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_lock_resources()", URL="\ref sdai_lock_resources()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_lock_ressources()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ---  [ label = "access shared ressources" ];
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_unlock_resources()", URL="\ref sdai_unlock_resources()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_unlock_ressources()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit ()" ];
 * @endmsc
 *
 * @desc The function sdai_lock_resources() locks the ressources shared between the application
 *       and the stack CPU. It must be called before accessing resources shared between the two CPUs.
 * @pnio Shared resources: serial FLASH, PHY MDIO register interface
 * @eip  Shared resources: serial FLASH, PHY MDIO register interface
 * @ecat No resources are shared between the CPUs.
 * @mb   Shared resources: PHY MDIO register interface
 * @epl  Shared resources: serial FLASH
 * @pbdp No resources are shared between the CPUs.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_INTERNAL
 * @param[in] Resources Specifies which resources should be locked
 * - type : U16
 * - range: #SDAI_SHARED_RESOURCE_FLASH |
 *          #SDAI_SHARED_RESOURCE_MDIO
 * @pre Before the first call of this function the sdai_init() function must be called.
 * @post The function sdai_unlock_resources() must be called after the access of the shared resources
 * @remarks This function is not thread save.
 */
U8 sdai_lock_resources (U16 Resources)
{
  _TRACE (("sdai_lock_resources"));

  return (SdaiBackendDriver.Interface.lock_resources(&SdaiBackendDriver, Resources));
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI::API", "SDAI::DRIVER";
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_init()", URL="\ref sdai_init()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_init ()" ];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_lock_resources()", URL="\ref sdai_lock_resources()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_lock_ressources()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ---  [ label = "access shared ressources" ];
 *
 * "Application"=>"SDAI::API"    [ label = "sdai_unlock_resources()", URL="\ref sdai_unlock_resources()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_unlock_ressources()" ];
 * "SDAI::API"  <<"SDAI::DRIVER" [ label = "SDAI_SUCCESS"];
 * "Application"<<"SDAI::API"    [ label = "SDAI_SUCCESS"];
 *
 * ...;
 * "Application"=>"SDAI::API"    [ label = "sdai_deinit()", URL="\ref sdai_deinit()" ];
 * "SDAI::API"  =>"SDAI::DRIVER" [ label = "sdai_driver_deinit ()" ];
 * @endmsc
 *
 * @desc The function sdai_unlock_resources() unlocks the ressources shared between the application
 *       and the stack CPU. It must be called after accessing resources shared between the two CPUs.
 * @pnio Shared resources: serial FLASH, PHY MDIO register interface
 * @eip  Shared resources: serial FLASH, PHY MDIO register interface
 * @ecat No resources are shared between the CPUs.
 * @mb   Shared resources: PHY MDIO register interface
 * @epl  Shared resources: serial FLASH
 * @pbdp No resources are shared between the CPUs.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS
 * @param[in] Resources Specifies which resources should be unlocked
 * - type : U16
 * - range: #SDAI_SHARED_RESOURCE_FLASH |
 *          #SDAI_SHARED_RESOURCE_MDIO
 * @pre Before the first call of this function the sdai_init() function must be called.
 * @remarks This function is not thread save.
 */
U8 sdai_unlock_resources (U16 Resources)
{
  _TRACE (("sdai_unlock_resources"));

  return (SdaiBackendDriver.Interface.unlock_resources(&SdaiBackendDriver, Resources));
}

/*===========================================================================*/
#ifndef DOXYEXTERN
/**
 * The function sdai_check_ip_settings() checks the passed IP network configuration settings.
 *
 * @return T_IP_CHECK_RESULT
 * @param[in] IpAddress ip address in host format
 * - type : U32
 * - range: whole range is valid
 * @param[in] NetMask netmask in host format
 * - type : U32
 * - range: whole range is valid
 * @param[in] DefaultRouter gateway address in host format
 * - type : U32
 * - range: whole range is valid
 */
T_IP_CHECK_RESULT sdai_check_ip_settings (U32 IpAddress, U32 NetMask, U32 DefaultRouter)
{
  T_IP_CHECK_RESULT   Result = IP_SETTINGS_OK;
  U32                 SubnetMask;
  U32                 BroadcastAddress;
  U16                 BitNumber;


  SubnetMask       = NetMask;
  BroadcastAddress = ((~SubnetMask) | (IpAddress & SubnetMask));
  BitNumber        = 0u;

  /* check if the netmask contains continouse areas of 0's and 1's */
  while ( (! (SubnetMask & 0x00000001)) && (BitNumber < (sizeof (SubnetMask) * 8u)) )
  {
    SubnetMask >>= 1;
    BitNumber++;
  }

  while ( (SubnetMask & 0x00000001) && (BitNumber < (sizeof (SubnetMask) * 8u)) )
  {
    SubnetMask >>= 1;
    BitNumber++;
  }

  if (BitNumber != (sizeof (SubnetMask) * 8u))
  {
    Result = INVALID_SUBNETMASK;
  }

  /*-------------------------------------------------------------------------*/

  if ( (Result == IP_SETTINGS_OK                   ) &&
       (((IpAddress >= LOCAL_IP_ADDRESS_START    ) &&
         (IpAddress <= LOCAL_IP_ADDRESS_END      )) ||
        ((IpAddress >= MULTICAST_IP_ADDRESS_START) &&
         (IpAddress <= MULTICAST_IP_ADDRESS_END  )) ||
        (IpAddress == BroadcastAddress            )) )
  {
    Result = INVALID_IP_ADDRESS;
  }

  /*-------------------------------------------------------------------------*/

  if (DefaultRouter != 0uL)
  {
    SubnetMask = NetMask;

    if ( (Result == IP_SETTINGS_OK                                ) &&
         ((IpAddress & SubnetMask) != (DefaultRouter & SubnetMask)) )
    {
      Result = IP_ADDRESS_AND_ROUTER_MISMATCH;
    }

    if ( (Result == IP_SETTINGS_OK                       ) &&
         (((DefaultRouter >= LOCAL_IP_ADDRESS_START    ) &&
           (DefaultRouter <= LOCAL_IP_ADDRESS_END      )) ||
          ((DefaultRouter >= MULTICAST_IP_ADDRESS_START) &&
           (DefaultRouter <= MULTICAST_IP_ADDRESS_END  )) ||
          (DefaultRouter == BroadcastAddress            )) )
    {
      Result = INVALID_ROUTER_ADDRESS;
    }
  }

  return (Result);
}

/*===========================================================================*/

/**
 * @desc The function sdai_pn_verify_device_name() verifies the device name.
 *
 * @return
 * - type  : U8
 * - values: 0 | 1
 * @param[in] pDeviceName
 * - type : U8*
 * - range: whole address space
 */
U8 sdai_pn_verify_device_name (U8* pDeviceName)
{
  U8* pLabel;

  U16 DeviceNameLength;
  U16 LabelLength;
  U16 RemainingSize;
  U16 Index;

  U8  IsFirstLabel = 1;
  U8  Result = 1;


  pLabel           = pDeviceName;
  RemainingSize    = SDAI_DEVNAME_MAX_LEN;
  LabelLength      = calculate_label_length (pLabel, RemainingSize);
  DeviceNameLength = 0u;

  while ( (LabelLength > 0u) && (LabelLength < PN_MAX_LABEL_LENGTH) )
  {
    if (IsFirstLabel)
    {
      if (LabelLength >= 8u)
      {
        if (string_compare_nocase (pLabel, (const U8*) "port-", 5))
        {
          char* pCharacter1 = (char*) (pLabel + 5);
          char* pCharacter2 = (char*) (pCharacter1 + 1);
          char* pCharacter3 = (char*) (pCharacter2 + 1);


          if ( (*pCharacter1 >= '0') && (*pCharacter1 <= '9') &&
               (*pCharacter2 >= '0') && (*pCharacter2 <= '9') &&
               (*pCharacter3 >= '0') && (*pCharacter3 <= '9') )
          {
            Result = 0;
          }
        }
      }
      else if (LabelLength > 3u)
      {
        if (string_compare_nocase (pLabel, (const U8*) "xn-", 3))
        {
          Result = 0;
        }
      }
      else if (LabelLength == 3u)
      {
        char*   pCharacter1 = (char*) pLabel;
        char*   pCharacter2 = (char*) (pCharacter1 + 1);
        char*   pCharacter3 = (char*) (pCharacter2 + 1);


        if ( (*pCharacter1 >= '0') && (*pCharacter1 <= '9') &&
             (*pCharacter2 >= '0') && (*pCharacter2 <= '9') &&
             (*pCharacter3 >= '0') && (*pCharacter3 <= '9') )
        {
          Result = 0;
        }
      }
      else if (LabelLength == 2u)
      {
        char*   pCharacter1 = (char*) pLabel;
        char*   pCharacter2 = (char*) (pCharacter1 + 1);


        if ( (*pCharacter1 >= '0') && (*pCharacter1 <= '9') &&
             (*pCharacter2 >= '0') && (*pCharacter2 <= '9') )
        {
          Result = 0;
        }
      }
      else if (LabelLength == 1u)
      {
        char* pCharacter1 = (char*) pLabel;


        if ( (*pCharacter1 >= '0') && (*pCharacter1 <= '9') )
        {
          Result = 0;
        }
      }

      IsFirstLabel = 0;
    }

    /*-----------------------------------------------------------------------*/

    for (Index = 0; Index < LabelLength; Index++)
    {
      if ( (Index > 0u) && (Index < (LabelLength - 1u)) )
      {
        Result &= ( ((*pLabel >= '0') && (*pLabel <= '9')) ||
                    ((*pLabel >= 'A') && (*pLabel <= 'Z')) ||
                    ((*pLabel >= 'a') && (*pLabel <= 'z')) ||
                    (*pLabel == '-') );
      }
      else
      {
        Result &= ( ((*pLabel >= '0') && (*pLabel <= '9')) ||
                    ((*pLabel >= 'A') && (*pLabel <= 'Z')) ||
                    ((*pLabel >= 'a') && (*pLabel <= 'z')) );
      }


      pLabel++;
    }

    LabelLength       = (*pLabel == '.') ? LabelLength + 1 : LabelLength; /* add the trailing '.' */
    DeviceNameLength += LabelLength;
    RemainingSize    -= LabelLength;
    pLabel            = pDeviceName + DeviceNameLength;
    LabelLength       = calculate_label_length (pLabel, RemainingSize);
  }

  /*-------------------------------------------------------------------------*/

  if (LabelLength == 0u)
  {
    if (*pDeviceName == '.') { Result = 0; }
  }

  Result = (LabelLength < PN_MAX_LABEL_LENGTH) ? Result : 0;
  Result = (DeviceNameLength <= SDAI_DEVNAME_MAX_LEN) ? Result : 0;

  return (Result);
}
#endif
//@}

/******************************************************************************
SOCKET INTERFACE FUNCTIONS
******************************************************************************/

/** @name Socket Interface Functions
* All SDAI Socket Interface functions which can be used by the application */
//@{

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SOCKET::API", "SOCKET::PROTOCOL";
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_init()", URL="\ref sdai_sock_init()" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS"];
 *
 * ---  [ label = "Socket interface ready" ];
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_term()", URL="\ref sdai_sock_term()" ];
 * @endmsc
 *
 * @desc The function sdai_sock_init() initializes the Socket API and the memory interface between
 *       Socket API and stack processor.
 *
 * @return
 * - type  : unsigned char
 * - values: #SOCK_SUCCESS |
 *           #SOCK_ERR_INVALID_ARGUMENT |
 *           #SOCK_ERR_API_ALREADY_INITIALIZED |
 *           #SOCK_ERR_NOT_SUPPORTED
 * @param[in] pSocketInit Pointer to initialization data for the socket interface
 * - type : struct T_SDAI_SOCK_INIT*
 * - range: whole address space
 * @pre The passed structure #T_SDAI_SOCK_INIT must be completely set before calling this function.
 * @pre The RTE stack must be already started and set to ONLINE. Thus the functions sdai_init() as well as sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE) must be executed before.
 * @post If this function returns with #SOCK_SUCCESS all other functions of the socket interface can be called.
 * @remarks This function is not reentrant.
 */
T_SOCKET_RESULT sdai_sock_init (T_SDAI_SOCK_INIT* pSocketInit)
{
  T_SOCKET_RESULT   Result = SOCK_ERR_NOT_SUPPORTED;


  _TRACE (("sdai_sock_init"));

#ifdef SDAI_INCLUDE_SOCKET
  if (SockInitialized)
  {
    return (SOCK_ERR_API_ALREADY_INITIALIZED);
  }

  if (! SdaiBackendDriver.Interface.plugging_complete (&SdaiBackendDriver) ||
      ! SdaiInitialized                                                    )
  {
    return (SOCK_ERR_NOT_SUPPORTED);
  }

  if (SdaiBackendDriver.ApplicationInitData.BackEnd == SDAI_BACKEND_PB_DP)
  {
    return (SOCK_ERR_NOT_SUPPORTED);
  }

  if (pSocketInit == NULL)
  {
    return (SOCK_ERR_INVALID_ARGUMENT);
  }

  Result = sdai_drv_sock_init (pSocketInit);

  if (Result == SOCK_SUCCESS)
  {
    SockInitialized = 1;
  }
#else
  _AVOID_UNUSED_WARNING(pSocketInit);
#endif

  return (Result);
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SOCKET::API", "SOCKET::PROTOCOL";
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_init()", URL="\ref sdai_sock_init()" ];
 *
 * ---  [ label = "Socket interface ready" ];
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_term()", URL="\ref sdai_sock_term()" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS"];
 *
 * ---  [ label = "Socket interface not ready" ];
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_init()", URL="\ref sdai_sock_init()" ];
 *
 * ---  [ label = "Socket interface ready" ];
 * @endmsc
 *
 * @desc The function sdai_sock_term() terminates the Socket API. This functions also closes all
 * opened sockets.
 *
 * @return
 * - type  : unsigned char
 * - values: #SOCK_SUCCESS |
 *           #SOCK_ERR_NOT_SUPPORTED |
 *           #SOCK_ERR_API_NOT_INITIALIZED |
 *           #SOCK_ERR_FATAL_ERROR
 * @pre Before calling this function the sdai_sock_init() function must be called.
 * @post After calling this function, the function sdai_sock_init() must be called
 *       again before calling any other function of the socket interface.
 * @remarks This function is not reentrant.
 */
T_SOCKET_RESULT sdai_sock_term (void)
{
  T_SOCKET_RESULT   Result = SOCK_ERR_NOT_SUPPORTED;


  _TRACE (("sdai_sock_term"));

#ifdef SDAI_INCLUDE_SOCKET
  if (! SockInitialized)
  {
    return (SOCK_ERR_API_NOT_INITIALIZED);
  }

  Result = sdai_drv_sock_term ();
  SockInitialized = 0;
#endif

  return (Result);
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SOCKET::API", "SOCKET::PROTOCOL";
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_init()", URL="\ref sdai_sock_init()" ];
 * "SOCKET::API"=>"SOCKET::PROTOCOL" [ label = "sdai_sock_send_signal(SOCK_IF_SIGNAL_START_STACK)" ];
 *
 * ---  [ label = "Socket interface ready" ];
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_create()", URL="\ref sdai_sock_create()" ];
 * "SOCKET::API"=>"SOCKET::PROTOCOL" [ label = "sdai_sock_send_signal(SOCK_IF_SIGNAL_CREATE_SOCKET)" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS, Socket ID"];
 *
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_ioctl()", URL="\ref sdai_sock_ioctl()" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS, Socket Status = SOCK_LOCAL_STATUS_OFFLINE"];
 *
 * ...;
 * "SOCKET::PROTOCOL"=>"SOCKET::API" [ label = "Socket Status = SOCK_IF_LOCAL_STATUS_ONLINE" ];
 *
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_ioctl()", URL="\ref sdai_sock_ioctl()" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS, Socket Status = SOCK_LOCAL_STATUS_ONLINE"];
 *
 * ---  [ label = "Socket is online" ];
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_close()", URL="\ref sdai_sock_close()" ];
 * "SOCKET::API"=>"SOCKET::PROTOCOL" [ label = "sdai_sock_send_signal(SOCK_IF_SIGNAL_CLOSE_SOCKET)" ];
 * @endmsc
 *
 * @desc The function sdai_sock_create() creates a new socket and binds the socket to a local address and
 *       a specific transport service provider. By using the parameters the socket operating
 *       mode (client/server communication) as well as the used communication protocol (UDP/TCP)
 *       can be specified. If a TCP client socket shall be created the socket protocol immediately
 *       tries to connect the socket to the specified remote TCP server. In case of a TCP server the socket
 *       immediately listens to connect requests. As opening the socket may require a couple of actions,
 *       this function returns immediately without waiting to open the appropriate socket at the communication
 *       processor side. Thus a return value of #SOCK_SUCCESS only shows that the passed parameters
 *       are accepted and that the creation of the socket has been initiated. The completion of the
 *       socket creation can be determined at a later stage by requesting the local socket status via the
 *       function sdai_sock_ioctl().
 *
 * @return
 * - type  : unsigned char
 * - values: #SOCK_SUCCESS                 |
 *           #SOCK_ERR_API_NOT_INITIALIZED |
 *           #SOCK_ERR_INVALID_ARGUMENT    |
 *           #SOCK_ERR_NOT_SUPPORTED       |
 *           #SOCK_ERR_INVALID_REMOTE_ADDR |
 *           #SOCK_ERR_INVALID_LOCAL_ADDR  |
 *           #SOCK_ERR_ADDR_IN_USE         |
 *           #SOCK_ERR_OUT_OF_SOCKETS
 * @param[in] Flags A bitfield used to set socket specific options. Every set bit activates the respective functionality.
 * - type : U32
 * - range: #SOCK_FLAG_ENABLE_KEEPALIVE |
 *          #SOCK_FLAG_ENABLE_BROADCAST
 * @param[in] pRemoteAddress If this pointer is set to NULL the socket is working in server mode. Otherwise the socket
 *                           is opened in client mode. In case of a client socket pRemoteAddr specifies the IP address
 *                           and port number of the remote server. For TCP client sockets the remote address is checked and if
 *                           not valid the error #SOCK_ERR_INVALID_REMOTE_ADDR is returned, e.g. because the remote IP address
 *                           is not reachable. For UDP client sockets the passed remote address will be ignored (checked when
 *                           calling the function sdai_sock_send()).
 * - type : struct T_SDAI_SOCK_ADDR*
 * - range: whole address space
 * @param[in] pLocalAddress Specifies the IP address and port number which shall be bind to the local socket. Setting
 *                          the local IP address to #SOCK_IP_ADDR_ANY allows to bind the socket to all IP addresses
 *                          of the TCP/IP stack. If the local port is set to #SOCK_PORT_ANY the TCP/IP stack automatically
 *                          selects a unique local port number from the dynamic client port range. The local address is
 *                          checked and if not valid the error #SOCK_ERR_INVALID_LOCAL_ADDR is returned.
 * - type : struct T_SDAI_SOCK_ADDR*
 * - range: whole address space
 * @param[out] pSocketId Pointer to the new socket ID which identifies the local socket.
 * - type : U16*
 * - range: whole address space
 *
 * @pre Before calling this function the sdai_sock_init() function must be called.
 * @remarks This function is not reentrant.
 */
T_SOCKET_RESULT sdai_sock_create
  (
    U32                 Flags,
    T_SDAI_SOCK_ADDR*   pRemoteAddress,
    T_SDAI_SOCK_ADDR*   pLocalAddress,
    U16*                pSocketId
  )
  /*-------------------------------------------------------------------------*/
{
  T_SOCKET_RESULT   Result = SOCK_ERR_NOT_SUPPORTED;


#ifdef SDAI_INCLUDE_SOCKET
  if (! SockInitialized)
  {
    return (SOCK_ERR_API_NOT_INITIALIZED);
  }

  if ( (pLocalAddress == NULL) || (pSocketId == NULL) )
  {
    return (SOCK_ERR_INVALID_ARGUMENT);
  }

  Result = sdai_drv_sock_create (Flags, pRemoteAddress, pLocalAddress, pSocketId);
#else
  _AVOID_UNUSED_WARNING(Flags);
  _AVOID_UNUSED_WARNING(pRemoteAddress);
  _AVOID_UNUSED_WARNING(pLocalAddress);
  _AVOID_UNUSED_WARNING(pSocketId);
#endif

  _TRACE (("sdai_sock_create"));

  return (Result);
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SOCKET::API", "SOCKET::PROTOCOL";
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_init()", URL="\ref sdai_sock_init()" ];
 * "SOCKET::API"=>"SOCKET::PROTOCOL" [ label = "sdai_sock_send_signal(SOCK_IF_SIGNAL_START_STACK)" ];
 *
 * ---  [ label = "Socket interface ready" ];
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_create()", URL="\ref sdai_sock_create()" ];
 * "SOCKET::API"=>"SOCKET::PROTOCOL" [ label = "sdai_sock_send_signal(SOCK_IF_SIGNAL_CREATE_SOCKET)" ];
 *
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_ioctl()", URL="\ref sdai_sock_ioctl()" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS, Socket Status = SOCK_LOCAL_STATUS_OFFLINE"];
 *
 * ...;
 * "SOCKET::PROTOCOL"=>"SOCKET::API" [ label = "Socket Status = SOCK_IF_LOCAL_STATUS_ONLINE" ];
 *
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_ioctl()", URL="\ref sdai_sock_ioctl()" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS, Socket Status = SOCK_LOCAL_STATUS_ONLINE"];
 *
 * ---  [ label = "Socket is online" ];
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_close()", URL="\ref sdai_sock_close()" ];
 * "SOCKET::API"=>"SOCKET::PROTOCOL" [ label = "sdai_sock_send_signal(SOCK_IF_SIGNAL_CLOSE_SOCKET)" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS"];
 *
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_ioctl()", URL="\ref sdai_sock_ioctl()" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS, Socket Status = SOCK_LOCAL_STATUS_ONLINE or SOCK_LOCAL_STATUS_OFFLINE"];
 *
 * ...;
 * "SOCKET::PROTOCOL"=>"SOCKET::API" [ label = "Socket Status = SOCK_IF_LOCAL_STATUS_FREE" ];
 *
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_ioctl()", URL="\ref sdai_sock_ioctl()" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS, Socket Status = SOCK_LOCAL_STATUS_CLOSED"];
 *
 * ---  [ label = "Socket is closed" ];
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_create()", URL="\ref sdai_sock_create()" ];
 * "SOCKET::API"=>"SOCKET::PROTOCOL" [ label = "sdai_sock_send_signal(SOCK_IF_SIGNAL_CREATE_SOCKET)" ];
 * @endmsc
 *
 * @desc The function sdai_sock_close() closes a socket. The function did not wait for completion of
 *       the close request. Thus the return value of the function does not signal that the socket
 *       is already closed. To determine when the socket has been closed the socket status has to
 *       be polled by calling the function sdai_sock_ioctl().
 *
 * @return
 * - type  : unsigned char
 * - values: #SOCK_SUCCESS                 |
 *           #SOCK_ERR_API_NOT_INITIALIZED |
 *           #SOCK_ERR_INVALID_SOCKET_ID
 * @param[in] SocketId The socket ID specifies the local socket
 * - type : U16
 * - range: 0 to (#MAX_NUMBER_SUPPORTED_SOCKETS - 1)
 *
 * @pre Before calling this function the sdai_sock_init() function must be called. If sdai_sock_create() was not called
 *      before no actions are performed and #SOCK_SUCCESS is returned.
 * @remarks This function is not reentrant.
 */
T_SOCKET_RESULT sdai_sock_close (U16 SocketId)
{
  T_SOCKET_RESULT   Result = SOCK_ERR_NOT_SUPPORTED;


  _TRACE (("sdai_sock_close"));

#ifdef SDAI_INCLUDE_SOCKET
  if (! SockInitialized)
  {
    return (SOCK_ERR_API_NOT_INITIALIZED);
  }

  if (SocketId >= MAX_NUMBER_SUPPORTED_SOCKETS)
  {
    return (SOCK_ERR_INVALID_SOCKET_ID);
  }

  Result = sdai_drv_sock_close (SocketId);
#else
  _AVOID_UNUSED_WARNING(SocketId);
#endif

  return (Result);
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SOCKET::API", "SOCKET::PROTOCOL";
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_init()", URL="\ref sdai_sock_init()" ];
 * "SOCKET::API"=>"SOCKET::PROTOCOL" [ label = "sdai_sock_send_signal(SOCK_IF_SIGNAL_START_STACK)" ];
 *
 * ---  [ label = "Socket interface ready" ];
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_create()", URL="\ref sdai_sock_create()" ];
 * "SOCKET::API"=>"SOCKET::PROTOCOL" [ label = "sdai_sock_send_signal(SOCK_IF_SIGNAL_CREATE_SOCKET)" ];
 *
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_ioctl()", URL="\ref sdai_sock_ioctl()" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS, Socket Status = SOCK_LOCAL_STATUS_OFFLINE"];
 *
 * ...;
 * "SOCKET::PROTOCOL"=>"SOCKET::API" [ label = "Socket Status = SOCK_IF_LOCAL_STATUS_ONLINE" ];
 *
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_ioctl()", URL="\ref sdai_sock_ioctl()" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS, Socket Status = SOCK_LOCAL_STATUS_ONLINE"];
 *
 * ---  [ label = "Socket is online" ];
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_send()", URL="\ref sdai_sock_send()" ];
 * "SOCKET::API"=>"SOCKET::PROTOCOL" [ label = "sdai_sock_send_signal(SOCK_IF_SIGNAL_SEND_DATA)" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS"];
 *
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_ioctl()", URL="\ref sdai_sock_ioctl()" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS, Send Status = SOCK_SEND_STATUS_BUSY"];
 *
 * ...;
 * "SOCKET::PROTOCOL"=>"SOCKET::API" [ label = "Socket Status = SOCK_IF_STATUS_SEND_COMPLETED" ];
 *
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_ioctl()", URL="\ref sdai_sock_ioctl()" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS, Send Status = SOCK_SEND_STATUS_COMPLETED"];
 *
 * ---  [ label = "Send Request completed" ];
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_close()", URL="\ref sdai_sock_close()" ];
 * "SOCKET::API"=>"SOCKET::PROTOCOL" [ label = "sdai_sock_send_signal(SOCK_IF_SIGNAL_CLOSE_SOCKET)" ];
 * @endmsc
 *
 * @desc The function sdai_sock_send() sends data on a socket. The function did not wait for completion of
 *       the send request. Thus the return value of the function does not signal that the send request
 *       has been completed. The send status has to be polled by calling the function sdai_sock_ioctl()
 *       to determine when the send request has been completed. The next send request on the same socket
 *       is only possible when the previous send request has been completed.
 *
 * @return
 * - type  : unsigned char
 * - values: #SOCK_SUCCESS                  |
 *           #SOCK_ERR_API_NOT_INITIALIZED  |
 *           #SOCK_ERR_INVALID_SOCKET_ID    |
 *           #SOCK_ERR_INVALID_ARGUMENT     |
 *           #SOCK_ERR_INVALID_REMOTE_ADDR  |
 *           #SOCK_ERR_SOCKET_NOT_CREATED   |
 *           #SOCK_ERR_SOCKET_OFFLINE       |
 *           #SOCK_ERR_SOCKET_NOT_CONNECTED |
 *           #SOCK_ERR_SOCKET_BUSY
 * @param[in] SocketId The socket ID specifies the local socket
 * - type : U16
 * - range: 0 to (#MAX_NUMBER_SUPPORTED_SOCKETS - 1)
 * @param[in] pRemoteAddress Specifies the IP address and port number of the destination host. The remote address is checked and
 *                           if not valid the error #SOCK_ERR_INVALID_REMOTE_ADDR is returned, e.g. because the remote IP address
 *                           is not reachable. Sending a UDP datagram to the global or network specific broadcast address
 *                           is only possible if #SOCK_FLAG_ENABLE_BROADCAST was set when creating the specified socket.
 * - type : struct T_SDAI_SOCK_ADDR*
 * - range: whole address space
 * @param[in] DataLength Data length in bytes of the data pointed to by the pData parameter.
 * - type : U16
 * - range: whole address space
 * @param[in] pData Pointer to a buffer containing the data to be transmitted.
 * - type : const unsigned char*
 * - range: whole address space
 *
 * @pre Before calling this function the sdai_sock_init() function must be called.
 * @remarks This function is not reentrant.
 */
T_SOCKET_RESULT sdai_sock_send
  (
    U16                 SocketId,
    T_SDAI_SOCK_ADDR*   pRemoteAddress,
    U16                 DataLength,
    const U8*           pData
  )
  /*-------------------------------------------------------------------------*/
{
  T_SOCKET_RESULT   Result = SOCK_ERR_NOT_SUPPORTED;


  _TRACE (("sdai_sock_send"));

#ifdef SDAI_INCLUDE_SOCKET
  if (! SockInitialized)
  {
    return (SOCK_ERR_API_NOT_INITIALIZED);
  }

  if ( (pRemoteAddress == NULL) || (pData == NULL) )
  {
    return (SOCK_ERR_INVALID_ARGUMENT);
  }

  if (SocketId >= MAX_NUMBER_SUPPORTED_SOCKETS)
  {
    return (SOCK_ERR_INVALID_SOCKET_ID);
  }

  Result = sdai_drv_sock_send (SocketId, pRemoteAddress, DataLength, pData);
#else
  _AVOID_UNUSED_WARNING(SocketId);
  _AVOID_UNUSED_WARNING(pRemoteAddress);
  _AVOID_UNUSED_WARNING(DataLength);
  _AVOID_UNUSED_WARNING(pData);
#endif

  return (Result);
}

/*===========================================================================*/

/**
 * @msc
 * hscale = "1.7";
 * "Application", "SOCKET::API", "SOCKET::PROTOCOL";
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_init()", URL="\ref sdai_sock_init()" ];
 * "SOCKET::API"=>"SOCKET::PROTOCOL" [ label = "sdai_sock_send_signal(SOCK_IF_SIGNAL_START_STACK)" ];
 *
 * ---  [ label = "Socket interface ready" ];
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_create()", URL="\ref sdai_sock_create()" ];
 * "SOCKET::API"=>"SOCKET::PROTOCOL" [ label = "sdai_sock_send_signal(SOCK_IF_SIGNAL_CREATE_SOCKET)" ];
 *
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_ioctl()", URL="\ref sdai_sock_ioctl()" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS, Socket Status = SOCK_LOCAL_STATUS_OFFLINE"];
 *
 * ...;
 * "SOCKET::PROTOCOL"=>"SOCKET::API" [ label = "Socket Status = SOCK_IF_LOCAL_STATUS_ONLINE" ];
 *
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_ioctl()", URL="\ref sdai_sock_ioctl()" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS, Socket Status = SOCK_LOCAL_STATUS_ONLINE"];
 *
 * ---  [ label = "Socket is online" ];
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_ioctl()", URL="\ref sdai_sock_ioctl()" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS, Receive Status = SOCK_RECEIVE_STATUS_NO_DATA_RECEIVED"];
 *
 * ...;
 * "SOCKET::PROTOCOL"=>"SOCKET::API" [ label = "Socket Status = SOCK_IF_STATUS_DATA_RECEIVED" ];
 *
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_ioctl()", URL="\ref sdai_sock_ioctl()" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS, Receive Status = SOCK_RECEIVE_STATUS_DATA_RECEIVED"];
 *
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_receive()", URL="\ref sdai_sock_receive()" ];
 * "Application"<<"SOCKET::API"      [ label = "SOCK_SUCCESS"];
 *
 * ---  [ label = "Data Received" ];
 * ...;
 * "Application"=>"SOCKET::API"      [ label = "sdai_sock_close()", URL="\ref sdai_sock_close()" ];
 * "SOCKET::API"=>"SOCKET::PROTOCOL" [ label = "sdai_sock_send_signal(SOCK_IF_SIGNAL_CLOSE_SOCKET)" ];
 * @endmsc
 *
 * @desc The function sdai_sock_receive() reads data received on a socket and stores the address information of the sender.
 *       The function did not wait until data are received on the socket. If no new data are available the function
 *       returns immediately with the error code #SOCK_ERR_NO_DATA_RECEIVED. The application may check if new data are
 *       available by calling the function sdai_sock_ioctl().
 *
 * @return
 * - type  : unsigned char
 * - values: #SOCK_SUCCESS                  |
 *           #SOCK_ERR_API_NOT_INITIALIZED  |
 *           #SOCK_ERR_INVALID_SOCKET_ID    |
 *           #SOCK_ERR_INVALID_ARGUMENT     |
 *           #SOCK_ERR_SOCKET_NOT_CREATED   |
 *           #SOCK_ERR_SOCKET_OFFLINE       |
 *           #SOCK_ERR_SOCKET_NOT_CONNECTED |
 *           #SOCK_ERR_NO_DATA_RECEIVED
 * @param[in] SocketId The socket ID specifies the local socket
 * - type : U16
 * - range: 0 to (#MAX_NUMBER_SUPPORTED_SOCKETS - 1)
 * @param[out] pRemoteAddress Specifies the source IP address and port number of the sending host.
 * - type : struct T_SDAI_SOCK_ADDR*
 * - range:
 * @param[in,out] pDataLength (IN):  Data length in bytes of the data pointed to by the pData parameter. \n
 *                            (OUT): Total number of bytes read from the internal receive buffer. This can be less than the
 *                                   number of requested bytes.
 * - type : U16*
 * - range: whole address space
 * @param[out] pData Pointer to a buffer containing the received data.
 * - type : const unsigned char*
 * - range: whole address space
 *
 * @pre Before calling this function the sdai_sock_init() function must be called.
 * @remarks This function is not reentrant.
 */
T_SOCKET_RESULT sdai_sock_receive
  (
    U16                 SocketId,
    T_SDAI_SOCK_ADDR*   pRemoteAddress,
    U16*                pDataLength,
    U8*                 pData
  )
  /*-------------------------------------------------------------------------*/
{
  T_SOCKET_RESULT   Result = SOCK_ERR_NOT_SUPPORTED;


  _TRACE (("sdai_sock_receive"));

#ifdef SDAI_INCLUDE_SOCKET
  if (! SockInitialized)
  {
    return (SOCK_ERR_API_NOT_INITIALIZED);
  }

  if ( (pDataLength == NULL) || (pData == NULL) )
  {
    return (SOCK_ERR_INVALID_ARGUMENT);
  }

  if (SocketId >= MAX_NUMBER_SUPPORTED_SOCKETS)
  {
    return (SOCK_ERR_INVALID_SOCKET_ID);
  }

  Result = sdai_drv_sock_receive (SocketId, pRemoteAddress, pDataLength, pData);
#else
  _AVOID_UNUSED_WARNING(SocketId);
  _AVOID_UNUSED_WARNING(pRemoteAddress);
  _AVOID_UNUSED_WARNING(pDataLength);
  _AVOID_UNUSED_WARNING(pData);
#endif

  return (Result);
}

/*===========================================================================*/

/**
 * @desc The function sdai_sock_ioctl() is returning status information of a socket. This allows the application to determine
 *       the local socket status, the result of the latest sdai_sock_send() function call or to check if new data are
 *       received. Also this function allows to close a specific TCP connection. If the application closes a TCP connection
 *       this function did not wait until the connection has been closed at the communication processor side. It returns
 *       immediately and the application has to poll the socket status via sdai_sock_ioctl() to determine when the connection
 *       is closed.
 *
 * @return
 * - type  : unsigned char
 * - values: #SOCK_SUCCESS                  |
 *           #SOCK_ERR_API_NOT_INITIALIZED  |
 *           #SOCK_ERR_INVALID_SOCKET_ID    |
 *           #SOCK_ERR_INVALID_ARGUMENT     |
 *           #SOCK_ERR_SOCKET_NOT_CREATED   |
 *           #SOCK_ERR_SOCKET_OFFLINE       |
 *           #SOCK_ERR_SOCKET_NOT_CONNECTED |
 *           #SOCK_ERR_NOT_SUPPORTED
 * @param[in] SocketId The socket ID specifies the local socket
 * - type : U16
 * - range: 0 to (#MAX_NUMBER_SUPPORTED_SOCKETS - 1)
 * @param[in,out] pIOControl (IN):  \n The structure parameter Command specifies the IO Control command. If the
 *                                  commands #SOCK_IOC_GET_SEND_STATUS or #SOCK_IOC_GET_RECEIVE_STATUS are set
 *                                  to request the Send/Receive status for a specific TCP connection also the
 *                                  address information of the remote connection partner must be specified by
 *                                  the application. Also for the command #SOCK_IOC_CLOSE_TCP_CONNECTION the
 *                                  address information of the remote connection partner must be specified by
 *                                  the application. \n \n
 *                           (OUT): \n Status informations as requested by the structure parameter Command. No
 *                                  data are returned if the Command #SOCK_IOC_CLOSE_TCP_CONNECTION is set.
 * - type : T_SOCK_IO_CONTROL*
 * - range: whole address space
 *
 * @pre Before calling this function the sdai_sock_init() function must be called.
 * @remarks This function is not reentrant.
 */
T_SOCKET_RESULT sdai_sock_ioctl
  (
    U16                       SocketId,
    T_SDAI_SOCK_IO_CONTROL*   pIOControl
  )
  /*-------------------------------------------------------------------------*/
{
  T_SOCKET_RESULT   Result = SOCK_ERR_NOT_SUPPORTED;


  _TRACE (("sdai_sock_ioctl"));

#ifdef SDAI_INCLUDE_SOCKET
  if (! SockInitialized)
  {
    return (SOCK_ERR_API_NOT_INITIALIZED);
  }

  if (pIOControl == NULL)
  {
    return (SOCK_ERR_INVALID_ARGUMENT);
  }

  if (SocketId >= MAX_NUMBER_SUPPORTED_SOCKETS)
  {
    return (SOCK_ERR_INVALID_SOCKET_ID);
  }

  Result = sdai_drv_sock_ioctl (SocketId, pIOControl);
#else
  _AVOID_UNUSED_WARNING(SocketId);
  _AVOID_UNUSED_WARNING(pIOControl);
#endif

  return (Result);
}

/*===========================================================================*/

/**
 * @desc The function sdai_sock_htonl() converts data of type U32
 *       stored in CPU format to network byte ordering
 *
 * @return
 * - type  : U32
 * @param[in] ControllerValue Value in CPU byte ordering
 * - type : U32
 * - range: whole range is valid
 */
U32 sdai_sock_htonl (U32 ControllerValue)
{
  _TRACE (("sdai_sock_htonl"));

  return (convert_processor_u32_to_network_u32 (ControllerValue));
}

/*===========================================================================*/

/**
 * @desc The function sdai_sock_ntohl() converts data of type U32
 *       stored in network format to CPU byte ordering
 *
 * @return
 * - type  : U32
 * @param[in] NetworkValue Value in network byte ordering
 * - type : U32
 * - range: whole range is valid
 */
U32 sdai_sock_ntohl (U32 NetworkValue)
{
  _TRACE (("sdai_sock_ntohl"));

  return (convert_network_u32_to_processor_u32 (NetworkValue));
}

/*===========================================================================*/

/**
 * @desc The function sdai_sock_htonl() converts data of type U16
 *       stored in CPU format to network byte ordering
 *
 * @return
 * - type  : U16
 * @param[in] ControllerValue Value in CPU byte ordering
 * - type : U16
 * - range: whole range is valid
 */
U16 sdai_sock_htons (U16 ControllerValue)
{
  _TRACE (("sdai_sock_htons"));

  return (convert_processor_u16_to_network_u16 (ControllerValue));
}

/*===========================================================================*/

/**
 * @desc The function sdai_sock_ntohs() converts data of type U16
 *       stored in network format to CPU byte ordering
 *
 * @return
 * - type  : U16
 * @param[in] NetworkValue Value in network byte ordering
 * - type : U16
 * - range: whole range is valid
 */
U16 sdai_sock_ntohs (U16 NetworkValue)
{
  _TRACE (("sdai_sock_ntohl"));

  return (convert_processor_u16_to_network_u16 (NetworkValue));
}

//@}

/******************************************************************************
PRIVATE FUNCTIONS
******************************************************************************/

#ifndef DOXYEXTERN

/**
 * @desc The function calculate_label_length() returns the default
 *       identnumber of Softings demo GSDML file based on module type and module
 *       data length.
 * @return
 * - type  : U8
 * - values: 0 to 65536
 * @param[in] pLabel
 * - type : U8*
 * - range: whole address range
 * @param[in] RemainingSize
 * - type : U16
 * - range: 0 to 65536
 */
static U16 calculate_label_length (U8* pLabel, U16 RemainingSize)
{
  U16 LabelLength;


  for (LabelLength = 0u; LabelLength < RemainingSize; LabelLength++)
  {
    if ( (*pLabel == '\0') || (*pLabel == '.') )
    {
      break;
    }

    pLabel++;
  }

  return (LabelLength);
}

/*===========================================================================*/

/**
 * @desc  The function string_compare_nocase() compares, while ignoring differences
 *        in case, not more than NumberBytes bytes from the string pointed to by pMem1
 *        to the string pointed to by pMem2. The function does an upper to lower
 *        conversions, then a byte comparison.
 * @return
 * - type  : U8
 * - values: 0 | 1
 * @param[in] pMem1
 * - type : const U8*
 * - range: whole address range
 * @param[in] pMem2
 * - type : const U8*
 * - range: whole address range
 * @param[in] NumberBytes
 * - type : U32
 * - range: whole range
 */
static U8 string_compare_nocase (const U8* pMem1, const U8* pMem2, U32 NumberBytes)
{
  U8 DataEqual = 1;


  while ( (NumberBytes) && (DataEqual) )
  {
    U8 Mem1 = *pMem1++;
    U8 Mem2 = *pMem2++;

    if ( (Mem1 >= 'A') && (Mem1 <= 'Z') ) { Mem1 += (char) ('a' - 'A'); }
    if ( (Mem2 >= 'A') && (Mem2 <= 'Z') ) { Mem2 += (char) ('a' - 'A'); }

    DataEqual &= (Mem1 == Mem2);
    NumberBytes--;
  }

  return (DataEqual);
}

/*===========================================================================*/

/**
 * The function sdai_default_deinit() is a dummy function to make calls
 * of unsupported and uninitialized functions save.
 * @return
 * - type  : U8
 * - values: #SDAI_ERR_COM_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver Pointer to the backend driver structure.
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_default_deinit (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  _TRACE (("sdai_default_deinit"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);

  return (SDAI_ERR_COM_NOT_SUPPORTED);
}

/*===========================================================================*/

/**
 * The function sdai_default_get_version() is a dummy function to make calls
 * of unsupported and uninitialized functions save.
 * @return
 * - type  : U8
 * - values: #SDAI_ERR_COM_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver Pointer to the backend driver structure.
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[out] pVersionData Pointer to a string where the version is stored.
 * - type : struct SDAI_VERSION_DATA*
 * - range: whole address space
 */
static U8 sdai_default_get_version (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, struct SDAI_VERSION_DATA* pVersionData)
{
  _TRACE (("sdai_default_get_version"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);
  _AVOID_UNUSED_WARNING (pVersionData);

  return (SDAI_ERR_COM_NOT_SUPPORTED);
}

/*===========================================================================*/

/**
 * The function sdai_default_get_state() is a dummy function to make calls
 * of unsupported and uninitialized functions save.
 * @return
 * - type  : U8
 * - values: #SDAI_ERR_COM_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver Pointer to the backend driver structure.
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[out] pState pointer where the state is stored
 * - type : U16*
 * - range: Valid values for the StackStatus: \ref StackStatus.
 * @param[out] pAddStatus pointer where the additional data are stored.
 * - type : struct SDAI_ADDSTATUS_DATA*
 * - range: whole address space
 */
static U8 sdai_default_get_state (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U16 *pState, struct SDAI_ADDSTATUS_DATA* pAddStatus)
{
  _TRACE (("sdai_default_get_state"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);
  _AVOID_UNUSED_WARNING (pState);
  _AVOID_UNUSED_WARNING (pAddStatus);

  return (SDAI_ERR_COM_NOT_SUPPORTED);
}

/*===========================================================================*/

/**
 * The function sdai_default_plug_unit() is a dummy function to make calls
 * of unsupported and uninitialized functions save.
 * @return
 * - type  : U8
 * - values: #SDAI_ERR_COM_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver Pointer to the backend driver structure.
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] UnitType defines unit type or signal plugging complete.
 * - type : U8
 * - range:
 * @param[in] InputSize defines the input data length of the unit in bytes.
 * - type : U8
 * - range:
 * @param[in] OutputSize defines the output data length of the unit in bytes.
 * - type : U8
 * - range:
 * @param[out] pId Pointer to U32 to store new generated unique ID.
 * - type : U32*
 * - range: whole address space
 * @param[in] pUserData backend specific data
 * - type : U8*
 * - range: whole address space
 */
static U8 sdai_default_plug_unit (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U8 UnitType, U8 InputSize, U8 OutputSize, U32* pId, const struct SDAI_CFG_DATA_EXT* pUserData)
{
  _TRACE (("sdai_default_plug_unit"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);
  _AVOID_UNUSED_WARNING (UnitType);
  _AVOID_UNUSED_WARNING (InputSize);
  _AVOID_UNUSED_WARNING (OutputSize);
  _AVOID_UNUSED_WARNING (pId);
  _AVOID_UNUSED_WARNING (pUserData);

  return (SDAI_ERR_COM_NOT_SUPPORTED);
}

/*===========================================================================*/

/**
 * The function sdai_default_pull_unit() is a dummy function to make calls
 * of unsupported and uninitialized functions save.
 * @return
 * - type  : U8
 * - values: #SDAI_ERR_COM_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver Pointer to the backend driver structure.
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[out] pId Pointer to U32 to store new generated unique ID.
 * - type : U32*
 * - range: whole address space
 */
static U8 sdai_default_pull_unit (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U32 Id)
{
  _TRACE (("sdai_default_plug_unit"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);
  _AVOID_UNUSED_WARNING (Id);

  return (SDAI_ERR_COM_NOT_SUPPORTED);
}

/*===========================================================================*/

/**
 * The function sdai_default_get_data() is a dummy function to make calls
 * of unsupported and uninitialized functions save.
 * @return
 * - type  : U8
 * - values: #SDAI_ERR_COM_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver Pointer to the backend driver structure.
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] Id Id for which unit the data shall be read
 * - type : U32
 * - range: 0 to 0xFFFFFFFF
 * @param[out] pStatus Pointer to the read data status
 * - type : U8*
 * - range: #SDAI_DATA_STATUS_VALID |
 *          #SDAI_DATA_STATUS_INVALID
 * @param[out] pData Pointer to the read data block
 * - type : U8*
 * - range: whole address range
 */
static U8 sdai_default_get_data (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U32 Id, U8* pStatus, U8* pData)
{
  _TRACE (("sdai_default_get_data"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);
  _AVOID_UNUSED_WARNING (Id);
  _AVOID_UNUSED_WARNING (pStatus);
  _AVOID_UNUSED_WARNING (pData);

  return (SDAI_ERR_COM_NOT_SUPPORTED);
}

/*===========================================================================*/

/**
 * The function sdai_default_set_data() is a dummy function to make calls
 * of unsupported and uninitialized functions save.
 * @return
 * - type  : U8
 * - values: #SDAI_ERR_COM_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver Pointer to the backend driver structure.
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] Id Id for which unit the data shall be set
 * - type : U32
 * - range: 0 to 0xFFFFFFFF
 * @param[in] Status data status to be written
 * - type : U8
 * - range: #SDAI_DATA_STATUS_VALID |
 *          #SDAI_DATA_STATUS_INVALID
 * @param[in] pData Pointer to the data block to be written
 * - type : U8*
 * - range: whole address space

 */
static U8 sdai_default_set_data (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U32 Id, U8 Status, const U8* pData)
{
  _TRACE (("sdai_default_set_data"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);
  _AVOID_UNUSED_WARNING (Id);
  _AVOID_UNUSED_WARNING (Status);
  _AVOID_UNUSED_WARNING (pData);

  return (SDAI_ERR_COM_NOT_SUPPORTED);
}

/*===========================================================================*/

/**
 * The function sdai_default_process_data_update_finished() is a dummy function to make calls
 * of unsupported and uninitialized functions save.
 * @return
 * - type  : U8
 * - values: #SDAI_ERR_COM_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver Pointer to the backend driver structure.
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_default_process_data_update_finished (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  _TRACE (("sdai_default_process_data_update_finished"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);

  return (SDAI_ERR_COM_NOT_SUPPORTED);
}

/*===========================================================================*/

/**
 * The function sdai_default_alarm_request() is a dummy function to make calls
 * of unsupported and uninitialized functions save.
 * @return
 * - type  : U8
 * - values: #SDAI_ERR_COM_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver Pointer to the backend driver structure.
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pAlarm The alarm data for the request
 * - type : const struct SDAI_ALARM_REQ*
 * - range: whole address space
 */
static U8 sdai_default_alarm_request (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_ALARM_REQ* pAlarm)
{
  _TRACE (("sdai_default_alarm_request"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);
  _AVOID_UNUSED_WARNING (pAlarm);

  return (SDAI_ERR_COM_NOT_SUPPORTED);
}

/*===========================================================================*/

/**
 * The function sdai_default_diagnosis_request() is a dummy function to make calls
 * of unsupported and uninitialized functions save.
 * @return
 * - type  : U8
 * - values: #SDAI_ERR_COM_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver Pointer to the backend driver structure.
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pDiagnosis The diagnosis data for the request
 * - type : struct SDAI_DIAGNOSIS_REQ*
 * - range: whole address space
 */
static U8 sdai_default_diagnosis_request (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_DIAGNOSIS_REQ* pDiagnosis)
{
  _TRACE (("sdai_default_diagnosis_request"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);
  _AVOID_UNUSED_WARNING (pDiagnosis);

  return (SDAI_ERR_COM_NOT_SUPPORTED);
}

/*===========================================================================*/

/**
 * The function sdai_default_write_response() is a dummy function to make calls
 * of unsupported and uninitialized functions save.
 * @return
 * - type  : U8
 * - values: #SDAI_ERR_COM_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver Pointer to the backend driver structure.
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pWrite The data for the write response
 * - type : struct SDAI_WRITE_RES*
 * - range: whole address space
 */
static U8 sdai_default_write_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_WRITE_RES* pWrite)
{
  _TRACE (("sdai_default_write_response"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);
  _AVOID_UNUSED_WARNING (pWrite);

  return (SDAI_ERR_COM_NOT_SUPPORTED);
}

/*===========================================================================*/

/**
 * The function sdai_default_read_response() is a dummy function to make calls
 * of unsupported and uninitialized functions save.
 * @return
 * - type  : U8
 * - values: #SDAI_ERR_COM_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver Pointer to the backend driver structure.
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pRead The data for the read response
 * - type : struct SDAI_READ_RES*
 * - range: whole address space
 */
static U8 sdai_default_read_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_READ_RES* pRead)
{
  _TRACE (("sdai_default_read_response"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);
  _AVOID_UNUSED_WARNING (pRead);

  return (SDAI_ERR_COM_NOT_SUPPORTED);
}

/*===========================================================================*/

/**
 * The function sdai_default_control_response() is a dummy function to make calls
 * of unsupported and uninitialized functions save.
 * @return
 * - type  : U8
 * - values: #SDAI_ERR_COM_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver Pointer to the backend driver structure.
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pControl The data for the control response
 * - type : struct SDAI_CONTROL_RES*
 * - range: whole address space
 */
static U8 sdai_default_control_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_CONTROL_RES* pControl)
{
  _TRACE (("sdai_default_read_response"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);
  _AVOID_UNUSED_WARNING (pControl);

  return (SDAI_ERR_COM_NOT_SUPPORTED);
}

/*===========================================================================*/

/**
 * The function sdai_default_proto_start() is a dummy function to make calls
 * of unsupported and uninitialized functions save.
 * @return
 * - type  : U8
 * - values: #SDAI_ERR_COM_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver Pointer to the backend driver structure.
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_default_proto_start (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  _TRACE (("sdai_default_proto_start"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);

  return (SDAI_ERR_COM_NOT_SUPPORTED);
}

/*===========================================================================*/

/**
 * The function sdai_default_plugging_complete() is a dummy function to make calls
 * of unsupported and uninitialized functions save.
 * @return
 * - type  : int
 * - values: 0
 * @param[in] pSdaiBackendDriver Pointer to the backend driver structure.
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static int sdai_default_plugging_complete (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  _TRACE (("sdai_default_plugging_complete"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);

  return (0);
}

/*===========================================================================*/

/**
 * The function sdai_default_init_watchdog() is a dummy function to make calls
 * of unsupported and uninitialized functions save.
 * @return
 * - type  : U8
 * - values: #SDAI_ERR_COM_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver Pointer to the backend driver structure.
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] WatchdogFactor
 * - type : const U16
 * - range: 1 - 10
 */
static U8 sdai_default_init_watchdog (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const U16 WatchdogFactor)
{
  _TRACE (("sdai_default_plugging_complete"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);
  _AVOID_UNUSED_WARNING (WatchdogFactor);

  return (SDAI_ERR_COM_NOT_SUPPORTED);
}

/*===========================================================================*/

/**
 * The function sdai_default_check_and_trigger_watchdog() is a dummy function to make calls
 * of unsupported and uninitialized functions save.
 * @return
 * - type  : U8
 * - values: #SDAI_ERR_COM_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver Pointer to the backend driver structure.
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_default_check_and_trigger_watchdog (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  _TRACE (("sdai_default_check_and_trigger_watchdog"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);

  return (SDAI_ERR_COM_NOT_SUPPORTED);
}

/*===========================================================================*/

/**
 * The function sdai_default_lock_resources() is a dummy function to make calls
 * of unsupported and uninitialized functions save.
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS
 * @param[in] pSdaiBackendDriver Pointer to the backend driver structure.
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] Resources Specifies which resources should be unlocked
 * - type : U16
 * - range: #SDAI_SHARED_RESOURCE_FLASH |
 *          #SDAI_SHARED_RESOURCE_MDIO
 */
static U8 sdai_default_lock_resources (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U16 Resource)
{
  _TRACE (("sdai_default_lock_resources"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);
  _AVOID_UNUSED_WARNING (Resource);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_default_unlock_resources() is a dummy function to make calls
 * of unsupported and uninitialized functions save.
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS
 * @param[in] pSdaiBackendDriver Pointer to the backend driver structure.
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] Resources Specifies which resources should be unlocked
 * - type : U16
 * - range: #SDAI_SHARED_RESOURCE_FLASH |
 *          #SDAI_SHARED_RESOURCE_MDIO
 */
static U8 sdai_default_unlock_resources (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U16 Resource)
{
  _TRACE (("sdai_default_unlock_resources"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);
  _AVOID_UNUSED_WARNING (Resource);

  return (SDAI_SUCCESS);
}

#endif /* DOXYINTERN */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
