/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

#include <string.h>

#include "sdai.h"

#include "cfg_cc.h"

#include "sdai_ext.h"
#include "sdai_interface.h"

#ifdef SDAI_INCLUDE_SOCKET
  #include "sdai_socket.h"
  #include "socket_interface.h"
#endif

#include "sdai_drv.h"

#include "porting.h"

#ifdef SDAI_INCLUDE_PNAK

/******************************************************************************
DEFINES
******************************************************************************/

#define SDAI_DRV_PN_INDEX_SUBSLOT_SUBSTITUTE      (U16) 0x801Eu

#define PROFINET_SUBSTITUTION_MODE_ZERO           (U8) 0u
#define PROFINET_SUBSTITUTION_MODE_LAST_VALUE     (U8) 1u
#define PROFINET_SUBSTITUTION_MODE_REPLACEMENT    (U8) 2u

/******************************************************************************
TYPEDEFS
******************************************************************************/

/** \brief Holds the PROFINET related informations in the PN driver Module */
typedef struct _T_SDAI_DRV_PN
{
  int                     ShutdownPending;                         /**< Indicates a pending shutdown command */
  int                     ExceptionPending;                        /**< Indicates a pending exception */
  U8                      AlarmPending;                            /**< Indicates a pending alarm */
  U16                     NumberUnits;                             /**< Number of plugged untits */
  U16                     HighestUnitIndex;                        /**< The highest used unit index */
  U8*                     pIODataBase;                             /**< BaseAddress of the IO data area in the interface */

  int                     PlugComplete;                            /**< User signaled plugging complete */

  BOOL                    AutomaticSlotAssignment;                 /**< */

  struct SDAI_PN_CFG_DATA CfgData [SDAI_MAX_UNITS];                /**< Local copy of the unit configuration data */

  U8                      SubstituteData [INTERFACE_IO_DATA_SIZE]; /**< Current substitute values for the  */

} T_SDAI_DRV_PN;

/*===========================================================================*/

typedef struct _T_PROFINET_BLOCK_HEADER
{
  U8   TypeHighByte;
  U8   TypeLowByte;

  U8   LengthHighByte;
  U8   LengthLowByte;

  U8   VersionHighByte;
  U8   VersionLowByte;

} T_PROFINET_BLOCK_HEADER;

/*===========================================================================*/

typedef struct _T_SDAI_DRV_PN_SUBSTITUTE_VALUE
{
  T_PROFINET_BLOCK_HEADER   Header;

  U8                        SubstitutionModeHighByte;
  U8                        SubstitutionModeLowByte;

/*U8                        SubstituteData [];*/

} T_SDAI_DRV_PN_SUBSTITUTE_VALUE;

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

static U8     sdai_drv_pn_deinit                          (T_SDAI_BACKEND_DRIVER*);
static U8     sdai_drv_pn_get_version                     (T_SDAI_BACKEND_DRIVER*, struct SDAI_VERSION_DATA*);
static U8     sdai_drv_pn_get_state                       (T_SDAI_BACKEND_DRIVER*, U16*, struct SDAI_ADDSTATUS_DATA* pAddStatus);
static U8     sdai_drv_pn_plug_unit                       (T_SDAI_BACKEND_DRIVER*, U8, U8, U8, U32*, const struct SDAI_CFG_DATA_EXT*);
static U8     sdai_drv_pn_pull_unit                       (T_SDAI_BACKEND_DRIVER*, U32);

static U8     sdai_drv_pn_get_data                        (T_SDAI_BACKEND_DRIVER*, U32, U8*, U8*);
static U8     sdai_drv_pn_set_data                        (T_SDAI_BACKEND_DRIVER*, U32, U8, const U8*);
static U8     sdai_drv_pn_process_data_update_finished    (T_SDAI_BACKEND_DRIVER*);

static U8     sdai_drv_pn_alarm_request                   (T_SDAI_BACKEND_DRIVER*, const struct SDAI_ALARM_REQ*);
static U8     sdai_drv_pn_diagnosis_request               (T_SDAI_BACKEND_DRIVER*, const struct SDAI_DIAGNOSIS_REQ*);
static U8     sdai_drv_pn_write_response                  (T_SDAI_BACKEND_DRIVER*, const struct SDAI_WRITE_RES*);
static U8     sdai_drv_pn_read_response                   (T_SDAI_BACKEND_DRIVER*, const struct SDAI_READ_RES*);
static U8     sdai_drv_pn_control_response                (T_SDAI_BACKEND_DRIVER*, const struct SDAI_CONTROL_RES*);

static U8     sdai_drv_pn_proto_start                     (T_SDAI_BACKEND_DRIVER*);
static int    sdai_drv_pn_plugging_complete               (T_SDAI_BACKEND_DRIVER*);

static U8     sdai_drv_pn_init_watchdog                   (T_SDAI_BACKEND_DRIVER*, const U16);
static U8     sdai_drv_pn_check_and_trigger_watchdog      (T_SDAI_BACKEND_DRIVER*);

static U8     sdai_drv_pn_lock_resources                  (T_SDAI_BACKEND_DRIVER*, U16);
static U8     sdai_drv_pn_unlock_resources                (T_SDAI_BACKEND_DRIVER*, U16);

static int    sdai_drv_pn_handle_stack_signal             (U16, void*);
static void   sdai_drv_pn_handle_data_ind                 (T_SDAI_BACKEND_DRIVER*, U16);
static void   sdai_drv_pn_handle_service_ind              (T_SDAI_BACKEND_DRIVER*);

/******************************************************************************
PUBLIC DATA
******************************************************************************/

/******************************************************************************
PRIVATE DATA
******************************************************************************/

static T_SDAI_DRV_PN                   SdaiBackendDriverPN;        /**< Local instance of PN driver structure */
static struct SDAI_OUTPUT_CHANGED_IND  OutputChangedInd;           /**< Holds informations of changed units for data cbk */

/******************************************************************************
PUBLIC FUNCTIONS
******************************************************************************/

/******************************************************************************
PROTECTED FUNCTIONS
******************************************************************************/

/**
 * @desc The function sdai_pn_init() initializes the interface and the PROFINET
 *       related part of the SDAI and and creates the PROFINET task.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_INTERNAL |
 *           #SDAI_ERR_INVALID_ARGUMENT |
 *           #SDAI_ERR_PROTOCOL_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pApplicationInitData
 * - type : struct SDAI_INIT*
 * - range: whole address space
 */
U8 sdai_drv_pn_init (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, struct SDAI_INIT* pApplicationInitData)
{
  T_INTERFACE_MANAGEMENT_DATA*    pManagementData;
  U8                              Result = SDAI_ERR_INTERNAL;
  U32                             IpAddr;
  U32                             Netmask;
  U32                             Gateway;

  _TRACE (("sdai_drv_pn_init"));

  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pApplicationInitData);

  memcpy ((U8*) &IpAddr,  pApplicationInitData->Ident.UseAs.Pn.Address, sizeof (IpAddr));
  memcpy ((U8*) &Netmask, pApplicationInitData->Ident.UseAs.Pn.Netmask, sizeof (Netmask));
  memcpy ((U8*) &Gateway, pApplicationInitData->Ident.UseAs.Pn.Gateway, sizeof (Gateway));

  IpAddr  = convert_network_u32_to_processor_u32 (IpAddr);
  Netmask = convert_network_u32_to_processor_u32 (Netmask);
  Gateway = convert_network_u32_to_processor_u32 (Gateway);

  if ( (sdai_check_ip_settings (IpAddr, Netmask, Gateway) != IP_SETTINGS_OK     ) ||
       (! sdai_pn_verify_device_name ((U8*) pApplicationInitData->Ident.DevName)) )
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  /* @brief The interface of the driver is initialized with default function. So
   * we have to overwrite only functions we support in the profinet implementation.
   */
  pSdaiBackendDriver->Interface.deinit                 = sdai_drv_pn_deinit;
  pSdaiBackendDriver->Interface.get_version            = sdai_drv_pn_get_version;
  pSdaiBackendDriver->Interface.get_state              = sdai_drv_pn_get_state;
  pSdaiBackendDriver->Interface.plug_unit              = sdai_drv_pn_plug_unit;
  pSdaiBackendDriver->Interface.pull_unit              = sdai_drv_pn_pull_unit;

  pSdaiBackendDriver->Interface.get_data               = sdai_drv_pn_get_data;
  pSdaiBackendDriver->Interface.set_data               = sdai_drv_pn_set_data;

  if (pSdaiBackendDriver->ApplicationInitData.Info.Flags & SDAI_DATA_CONSISTENCY_IO_IMAGE)
  {
    pSdaiBackendDriver->Interface.process_data_update_finished = sdai_drv_pn_process_data_update_finished;
  }

  pSdaiBackendDriver->Interface.diagnosis_request      = sdai_drv_pn_diagnosis_request;
  pSdaiBackendDriver->Interface.alarm_request          = sdai_drv_pn_alarm_request;
  pSdaiBackendDriver->Interface.write_response         = sdai_drv_pn_write_response;
  pSdaiBackendDriver->Interface.read_response          = sdai_drv_pn_read_response;
  pSdaiBackendDriver->Interface.control_response       = sdai_drv_pn_control_response;

  pSdaiBackendDriver->Interface.proto_start            = sdai_drv_pn_proto_start;
  pSdaiBackendDriver->Interface.plugging_complete      = sdai_drv_pn_plugging_complete;

  pSdaiBackendDriver->Interface.init_watchdog          = sdai_drv_pn_init_watchdog;

  pSdaiBackendDriver->Interface.lock_resources         = sdai_drv_pn_lock_resources;
  pSdaiBackendDriver->Interface.unlock_resources       = sdai_drv_pn_unlock_resources;

  pSdaiBackendDriver->pPrivateData                     = &SdaiBackendDriverPN;

  /*-------------------------------------------------------------------------*/

  SdaiBackendDriverPN.AlarmPending            = 0u;
  SdaiBackendDriverPN.NumberUnits             = 0u;
  SdaiBackendDriverPN.HighestUnitIndex        = 0u;
  SdaiBackendDriverPN.PlugComplete            = 0;
  SdaiBackendDriverPN.ShutdownPending         = 0u;
  SdaiBackendDriverPN.AutomaticSlotAssignment = TRUE;

  Result = interface_init
           (
             sdai_drv_pn_handle_stack_signal,
             NULL,
             INTERFACE_APPLICATION,
             pSdaiBackendDriver,
             SDAI_BACKEND_PN
           );

  if (Result == SDAI_SUCCESS)
  {
    SdaiBackendDriverPN.pIODataBase = interface_get_iodata_base ();

    pManagementData = interface_lock_data (MANAGEMENT_INTERFACE);

    if (pManagementData != NULL)
    {
      pManagementData->SupportedCallbacks = 0u;

      if (pApplicationInitData->Callback.DataCbk != NULL)      { pManagementData->SupportedCallbacks |= DATA_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.IdentDataCbk != NULL) { pManagementData->SupportedCallbacks |= IDENT_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.AlarmAckCbk != NULL)  { pManagementData->SupportedCallbacks |= ALARM_ACK_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.WriteReqCbk != NULL)  { pManagementData->SupportedCallbacks |= WRITE_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.ReadReqCbk != NULL)   { pManagementData->SupportedCallbacks |= READ_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.ControlReqCbk != NULL){ pManagementData->SupportedCallbacks |= CONTROL_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.ExceptionCbk != NULL) { pManagementData->SupportedCallbacks |= EXCEPTION_CALLBACK_SUPPORTED; }
    }

    interface_unlock_data (MANAGEMENT_INTERFACE);
  }

  return (Result);
}

/******************************************************************************
PRIVATE FUNCTIONS
******************************************************************************/

/**
 * @desc The function sdai_pn_deinit() deinitializes the interface and shuts down
 *       the PROFINET stack.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_drv_pn_deinit (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_SDAI_DRV_PN*    pSdaiBackendDriverPN;


  _TRACE (("sdai_drv_pn_deinit"));

  pSdaiBackendDriverPN = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPN->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pSdaiBackendDriverPN->ShutdownPending = 1;

  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_STOP_STACK);

  /* wait for backend shutting down */
  interface_deinit (INTERFACE_APPLICATION);

  pSdaiBackendDriver->pPrivateData = NULL;

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * @desc The function sdai_pn_get_version() returns the version string of the
 *       PROFINET stack.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pVersionData
 * - type : struct SDAI_VERSION_DATA*
 * - range: whole address space
 */
static U8 sdai_drv_pn_get_version (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, struct SDAI_VERSION_DATA* pVersionData)
{
  T_SDAI_DRV_PN*              pSdaiBackendDriverPN;

  T_INTERFACE_STATUS_DATA*    pStatusData;


  _TRACE (("sdai_drv_pn_get_version"));

  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pVersionData);

  pSdaiBackendDriverPN = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPN->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  pStatusData = interface_lock_data (STATUS_INTERFACE);

  memcpy(pVersionData, &pStatusData->VersionData, sizeof(struct SDAI_VERSION_DATA));

  interface_unlock_data (STATUS_INTERFACE);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * @desc The function sdai_pn_get_state() returns the current state of the PROFINET
 *       protocoll stack.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY |
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[out] pState
 * - type : U16*
 * - range: #SDAI_STATE_MASK_ONLINE                |
 *          #SDAI_STATE_MASK_CONNECTED             |
 *          #SDAI_STATE_MASK_DOUBLE_ADDRESS_ERROR  |
 *          #SDAI_STATE_MASK_HARDWARE_ERROR        |
 *          #SDAI_STATE_MASK_SECPROM_ERROR         |
 *          #SDAI_STATE_MASK_APPL_WATCHDOG_EXPIRED |
 *          #SDAI_STATE_MASK_PROTOCOL_ERROR
 * @param[out] pAddStatus
 * - type : struct SDAI_ADDSTATUS_DATA*
 * - range: whole address space
 */
static U8 sdai_drv_pn_get_state (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U16 *pState, struct SDAI_ADDSTATUS_DATA* pAddStatus)
{
  T_SDAI_DRV_PN*              pSdaiBackendDriverPN;

  T_INTERFACE_STATUS_DATA*    pStatusData;


  _TRACE (("sdai_drv_pn_get_state"));

  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pState);

  pSdaiBackendDriverPN = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPN->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pStatusData = interface_lock_data (STATUS_INTERFACE);
  *pState = pStatusData->StackStatus;

  if (pAddStatus != NULL)
  {
    memcpy(pAddStatus, &pStatusData->StackAddStatus, sizeof(struct SDAI_ADDSTATUS_DATA));
  }

  interface_unlock_data (STATUS_INTERFACE);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_pn_plug_unit() makes the PROFINET IO specific part of
 *       plugging. It calculates the ID for the unit and returns it to the application.
 * @return
 * - type  : U32
 * - values: 0x00010000 to 0x0001FFFF
 * @param[in] UnitType defines unit type or signal plugging complete.
 * - type : U8
 * - range: #SDAI_UNIT_TYPE_HEAD |
 *          #SDAI_UNIT_TYPE_GENERIC_INPUT |
 *          #SDAI_UNIT_TYPE_GENERIC_OUTPUT |
 *          #SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT |
 *          #SDAI_UNIT_TYPE_PLUGGING_COMPLETE
 * @param[in] InputSize defines the input data length of the unit in bytes.
 * - type : U8
 * - range: 0 to 255
 * @param[in] OutputSize defines the output data length of the unit in bytes.
 * - type : U8
 * - range: 0 to 255
 * @param[out] pUnit
 * - type : SDAI_UNIT *
 * - range: whole address space
 */
static U8 sdai_drv_pn_plug_unit (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U8 UnitType, U8 InputSize, U8 OutputSize, U32* pId, const struct SDAI_CFG_DATA_EXT* pCfgData)
{
  T_INTERFACE_STATUS_DATA*        pStatusData;
  T_SDAI_DRV_PN*                  pSdaiBackendDriverPN;
  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  T_INTERFACE_UNIT*               pUnit;
  U16                             UnitIndex = 0xFFFF;

  volatile U16                    ServiceType;

  BOOL                            ReplaceUnit = FALSE;


  _TRACE (("sdai_drv_pn_plug_unit"));

  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverPN = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPN->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  /* if users signals plugging complete we don't want to check the args */
  if (UnitType == SDAI_UNIT_TYPE_PLUGGING_COMPLETE)
  {
    pSdaiBackendDriverPN->PlugComplete = 1;

    return (pSdaiBackendDriver->Interface.proto_start (pSdaiBackendDriver));
  }

  /*-------------------------------------------------------------------------*/

  if (pSdaiBackendDriverPN->NumberUnits == 0)
  {
    if (UnitType != SDAI_UNIT_TYPE_HEAD)
    {
      return (SDAI_ERR_INVALID_ARGUMENT);
    }
  }
  else
  {
    if (UnitType == SDAI_UNIT_TYPE_HEAD)
    {
      return (SDAI_ERR_INVALID_ARGUMENT);
    }
  }

  if ( (InputSize > SDAI_PN_MAX_UNIT_IO_DATA_SIZE ) ||
       (OutputSize > SDAI_PN_MAX_UNIT_IO_DATA_SIZE) )
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  /*-------------------------------------------------------------------------*/

  if (pSdaiBackendDriver->ApplicationInitData.Info.Flags & SDAI_DYNAMIC_IO_CONFIGURATION)
  {
    if (pSdaiBackendDriverPN->NumberUnits == 0)
    {
      if (*pId != SDAI_INVALID_ID)
      {
        if (_SDAI_PN_ID_TO_SLOT(U16, *pId) != 0)
        {
          return (SDAI_ERR_INVALID_ARGUMENT);
        }

        SdaiBackendDriverPN.AutomaticSlotAssignment = FALSE;
      }
    }
    else
    {
      if (SdaiBackendDriverPN.AutomaticSlotAssignment == FALSE)
      {
        if (*pId == SDAI_INVALID_ID)
        {
          return (SDAI_ERR_INVALID_ARGUMENT);
        }
        else
        {
          pUnit = interface_lock_unit (0);

          for (UnitIndex = 0; UnitIndex <= SdaiBackendDriverPN.HighestUnitIndex; UnitIndex++)
          {
            if (pUnit->InternalFlags & UNIT_INTERNAL_FLAG_IN_USE)
            {
              if (_SDAI_PN_ID_TO_SLOT(U16, *pId) == _SDAI_PN_ID_TO_SLOT(U16, pUnit->Id))
              {
                if (pSdaiBackendDriver->ApplicationInitData.Info.Flags & SDAI_APPL_HANDLES_INTERFACE_AND_PORTS)
                {
                  if ( (_SDAI_PN_ID_TO_SYSTEM_DEFINED_SUBSLOT(U16, *pId) != _SDAI_PN_ID_TO_SYSTEM_DEFINED_SUBSLOT(U16, pUnit->Id)) &&
                       (pCfgData->UseAs.Pn.IdentNumber != pSdaiBackendDriverPN->CfgData [UnitIndex].IdentNumber                  ) )
                  {
                    interface_unlock_unit (0);
                    return (SDAI_ERR_INVALID_ARGUMENT);
                  }
                  else if ( (_SDAI_PN_ID_TO_SYSTEM_DEFINED_SUBSLOT(U16, *pId) != _SDAI_PN_ID_TO_SYSTEM_DEFINED_SUBSLOT(U16, pUnit->Id)) &&
                            (pCfgData->UseAs.Pn.SubmoduleIdentNumber == pSdaiBackendDriverPN->CfgData [UnitIndex].SubmoduleIdentNumber) &&
                            ((InputSize != pUnit->DataLengthInput) || (OutputSize != pUnit->DataLengthOutput)                         ) )
                  {
                    interface_unlock_unit (0);
                    return (SDAI_ERR_INVALID_ARGUMENT);
                  }
                  else if (_SDAI_PN_ID_TO_SYSTEM_DEFINED_SUBSLOT(U16, *pId) == _SDAI_PN_ID_TO_SYSTEM_DEFINED_SUBSLOT(U16, pUnit->Id))
                  {
                    ReplaceUnit = TRUE;
                  }
                }
                else
                {
                  if ( (_SDAI_PN_ID_TO_SUBSLOT(U16, *pId) != _SDAI_PN_ID_TO_SUBSLOT(U16, pUnit->Id)            ) &&
                       (pCfgData->UseAs.Pn.IdentNumber != pSdaiBackendDriverPN->CfgData [UnitIndex].IdentNumber) )
                  {
                    interface_unlock_unit (0);
                    return (SDAI_ERR_INVALID_ARGUMENT);
                  }
                  else if ( (_SDAI_PN_ID_TO_SUBSLOT(U16, *pId) != _SDAI_PN_ID_TO_SUBSLOT(U16, pUnit->Id)                              ) &&
                            (pCfgData->UseAs.Pn.SubmoduleIdentNumber == pSdaiBackendDriverPN->CfgData [UnitIndex].SubmoduleIdentNumber) &&
                            ((InputSize != pUnit->DataLengthInput) || (OutputSize != pUnit->DataLengthOutput)                         ) )
                  {
                    interface_unlock_unit (0);
                    return (SDAI_ERR_INVALID_ARGUMENT);
                  }
                  else if (_SDAI_PN_ID_TO_SUBSLOT(U16, *pId) == _SDAI_PN_ID_TO_SUBSLOT(U16, pUnit->Id))
                  {
                    ReplaceUnit = TRUE;
                  }
                }
              }
            }

            pUnit++;
          }

          interface_unlock_unit (0);

          UnitIndex = interface_find_unit (*pId, pSdaiBackendDriverPN->HighestUnitIndex);
        }
      }
      else
      {
        if (*pId != SDAI_INVALID_ID) {return (SDAI_ERR_INVALID_ARGUMENT);}
      }
    }
  }
  else
  {
    /* submodule always 1, module index is the sum of in and out units */
    *pId = _SDAI_PN_SLOT_AND_SUBSLOT_TO_ID (pSdaiBackendDriverPN->NumberUnits, 1);
  }

  /*-------------------------------------------------------------------------*/

  if (pCfgData == NULL)
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  /* add the aditional status byte to the length that is requested from the interface */
  switch (UnitType)
  {
    case SDAI_UNIT_TYPE_GENERIC_INPUT      : OutputSize = 0; InputSize++; break;
    case SDAI_UNIT_TYPE_GENERIC_OUTPUT     : OutputSize++; InputSize = 0;  break;
    case SDAI_UNIT_TYPE_HEAD               : OutputSize = 0; InputSize = 1; break;
    case SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT: OutputSize++; InputSize++;  break;

    default: return (SDAI_ERR_UNKNOWN_UNIT_TYPE);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  pUnit = interface_get_free_unit (InputSize, OutputSize, &UnitIndex);

  if (pUnit != NULL)
  {
    _ASSERT ((pUnit->DataLengthInput == InputSize) && (pUnit->DataLengthOutput == OutputSize));
    _ASSERT (((pUnit->DataInputOffset != 0xFFFF) && (InputSize > 0)) ||
             ((pUnit->DataOutputOffset != 0xFFFF) && (OutputSize > 0)) ||
             ((pUnit->DataInputOffset == 0xFFFF) && (InputSize == 0)) ||
             ((pUnit->DataOutputOffset == 0xFFFF) && (OutputSize == 0)));

    pServiceData->ServiceType = SERVICE_REQ_RES_PLUG_UNIT;

    pServiceData->UseAs.PlugUnit.UnitIndex                      = UnitIndex;
    pServiceData->UseAs.PlugUnit.CfgData.PeripheralOffsetInput  = pCfgData->PeripheralOffsetInput;
    pServiceData->UseAs.PlugUnit.CfgData.PeripheralOffsetOutput = pCfgData->PeripheralOffsetOutput;

    memcpy
    (
      &pServiceData->UseAs.PlugUnit.CfgData.UseAs.Pn,
      &pCfgData->UseAs.Pn,
      sizeof (pServiceData->UseAs.PlugUnit.CfgData.UseAs.Pn)
    );

    memcpy
    (
      &pSdaiBackendDriverPN->CfgData [UnitIndex],
      &pCfgData->UseAs.Pn,
      sizeof (pSdaiBackendDriverPN->CfgData [UnitIndex])
    );

    if (pUnit->DataOutputOffset != 0xFFFF)
    {
      /* initialize the substitute data (use extended length) */
      memset (&SdaiBackendDriverPN.SubstituteData [pUnit->DataOutputOffset], 0u, pUnit->DataLengthOutput);
    }

    /* remove the added additional status byte from the real unit length */
    if (pUnit->DataLengthInput) {pUnit->DataLengthInput--;}
    if (pUnit->DataLengthOutput) {pUnit->DataLengthOutput--;}

    pUnit->Id                = *pId;
    pUnit->ProtocolExtention = (U16) _SDAI_PN_OUTPUT_AND_INPUT_LENGTH_TO_EXTENTION (pUnit->DataLengthOutput, pUnit->DataLengthInput);
    pUnit->Type              = UnitType;
    pUnit->InternalFlags     = (pCfgData->InterfaceType & SDAI_INTERFACE_MASK) | UNIT_INTERNAL_FLAG_IN_USE;
    pUnit->StatusOutput      = SDAI_DATA_STATUS_VALID;
    pUnit->StatusInput       = SDAI_DATA_STATUS_VALID;

    if (pCfgData->PeripheralOffsetInput != SDAI_PERIPHERAL_OFFSET_DPRAM)
    {
      pUnit->InternalFlags |= UNIT_INTERNAL_FLAG_PERIPHERAL;
      pUnit->DataInputOffset = pCfgData->PeripheralOffsetInput;
    }
    if (pCfgData->PeripheralOffsetOutput != SDAI_PERIPHERAL_OFFSET_DPRAM)
    {
      pUnit->InternalFlags |= UNIT_INTERNAL_FLAG_PERIPHERAL;
      pUnit->DataOutputOffset = pCfgData->PeripheralOffsetOutput;
    }

    pStatusData = interface_lock_data (STATUS_INTERFACE);

    if(pStatusData != NULL)
    {
      struct SDAI_PN_ADDSTATUS_DATA*  pPnAddStatusData;

      pPnAddStatusData = &pStatusData->StackAddStatus.UseAs.Pn;

      pPnAddStatusData->UnitData[UnitIndex].Id         = pUnit->Id;
      pPnAddStatusData->UnitData[UnitIndex].CylceTime  = 0;
    }

    if (SdaiBackendDriverPN.AutomaticSlotAssignment == TRUE)
    {
      pSdaiBackendDriverPN->HighestUnitIndex += 1u;
    }
    else
    {
      pSdaiBackendDriverPN->HighestUnitIndex = _MAX(pSdaiBackendDriverPN->HighestUnitIndex, UnitIndex);
    }

    if (! ReplaceUnit)
    {
      pSdaiBackendDriverPN->NumberUnits++;
    }

    interface_unlock_data (STATUS_INTERFACE);
    interface_unlock_unit (UnitIndex);

    /* Send the Profinet specific configuration to the stack */
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

    /* Wait until service data are taken over by the stack */
    do
    {
      pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

      if (pServiceData == NULL)
      {
        interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
        return (SDAI_ERR_INTERNAL);
      }

      ServiceType = pServiceData->ServiceType;
      interface_unlock_data (SERVICE_REQ_RES_INTERFACE);

    } while (ServiceType != SERVICE_REQ_RES_FREE);

    return (SDAI_SUCCESS);
  }
  else
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  }

  return (SDAI_ERR_MAX_UNIT_REACHED);
}

/*===========================================================================*/

/**
 * The function sdai_drv_pn_pull_unit()
 * @return
 * - type  : U8
 * - values: ???
 * @param[in] U32
 * - type : U32
 * - range: whole range
 */
static U8 sdai_drv_pn_pull_unit (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U32 Id)
{
  T_SDAI_DRV_PN*                  pSdaiBackendDriverPN;
  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  T_INTERFACE_UNIT*               pUnit;

  volatile U16                    ServiceType;
  U16                             UnitIndex;


  _TRACE (("sdai_drv_pn_pull_unit"));

  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverPN = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPN->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  UnitIndex = interface_find_unit (Id, pSdaiBackendDriverPN->HighestUnitIndex);

  if (UnitIndex == 0xFFFF)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_NOSUCHUNIT);
  }

  pUnit = interface_lock_unit (UnitIndex);

  if (pUnit->Type == SDAI_UNIT_TYPE_HEAD)
  {
    interface_unlock_unit (UnitIndex);
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  pUnit->InternalFlags = 0;

  if (UnitIndex == pSdaiBackendDriverPN->HighestUnitIndex)
  {
    do
    {
      pSdaiBackendDriverPN->HighestUnitIndex--;
      pUnit--;

    } while ( (pSdaiBackendDriverPN->HighestUnitIndex > 0)      &&
              !(pUnit->InternalFlags & UNIT_INTERNAL_FLAG_IN_USE) );
  }

  interface_unlock_unit (UnitIndex);

  pSdaiBackendDriverPN->NumberUnits--;

  pServiceData->ServiceType = SERVICE_REQ_RES_PULL_UNIT;

  pServiceData->UseAs.PullUnit.UnitIndex = UnitIndex;

  /* Send the Profinet specific configuration to the stack */
  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  /* Wait until service data are taken over by the stack */
  do
  {
    pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

    if (pServiceData == NULL)
    {
      interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
      return (SDAI_ERR_INTERNAL);
    }

    ServiceType = (U16) pServiceData->ServiceType;
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);

  } while (ServiceType != SERVICE_REQ_RES_FREE);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * @desc The function sdai_pn_get_data() gets the current input data for the unit
 *       defined by Id. This function can only be used on output units.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *       SDAI_ERR_NOSUCHUNIT |
 *       SDAI_ERR_STACK_NOT_RDY
 * @param[in] Id
 * - type : U32
 * - range: ???
 * @param[out] pStatus
 * - type : U8*
 * - range: whole address space
 * @param[out] pData
 * - type : U8*
 * - range: whole address space
 */
static U8 sdai_drv_pn_get_data (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U32 Id, U8* pStatus, U8* pData)
{
  T_SDAI_DRV_PN*      pSdaiBackendDriverPN;

  T_INTERFACE_UNIT*   pUnit;

  U16                 UnitIndex;
  U8                  RealOutputLength;

  U8                  Result = SDAI_ERR_NOSUCHUNIT;


  _TRACE (("sdai_drv_pn_get_data"));

  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverPN = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPN->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  UnitIndex = interface_find_unit (Id, pSdaiBackendDriverPN->HighestUnitIndex);

  if (UnitIndex == 0xFFFF)
  {
    return (SDAI_ERR_NOSUCHUNIT);
  }

  pUnit = interface_lock_unit (UnitIndex);

  if (pUnit != NULL)
  {
    U8*   pNewData = &pSdaiBackendDriverPN->pIODataBase [pUnit->DataOutputOffset];

    _ASSERT (((pUnit->DataOutputOffset != 0xFFFF) && (pUnit->DataLengthOutput > 0)) ||
             ((pUnit->DataOutputOffset == 0xFFFF) && (pUnit->DataLengthOutput == 0)));

    RealOutputLength = _SDAI_PN_EXTENTION_TO_OUTPUT_LENGTH (U8, pUnit->ProtocolExtention);

    *pStatus = pSdaiBackendDriverPN->pIODataBase [pUnit->DataOutputOffset + RealOutputLength];

    if (pSdaiBackendDriverPN->CfgData [UnitIndex].Flags & SDAI_PN_UNIT_FLAG_SUPPORT_DATA_SUBSTITUTION)
    {
      if (*pStatus != SDAI_DATA_STATUS_VALID)
      {
        U8   SubstituteMode = pSdaiBackendDriverPN->SubstituteData [pUnit->DataOutputOffset + pUnit->DataLengthOutput];

        *pStatus = SDAI_DATA_STATUS_VALID;

        if (SubstituteMode == PROFINET_SUBSTITUTION_MODE_ZERO)
        {
          pNewData = NULL;
          memset (pData, 0u, RealOutputLength);
        }
        else if (SubstituteMode == PROFINET_SUBSTITUTION_MODE_REPLACEMENT)
        {
          pNewData = &pSdaiBackendDriverPN->SubstituteData [pUnit->DataOutputOffset];
        }

        pUnit->InternalFlags |= UNIT_INTERNAL_FLAG_SUBSTITUTION_ACTIVE;
      }
      else
      {
        pUnit->InternalFlags &= ~UNIT_INTERNAL_FLAG_SUBSTITUTION_ACTIVE;
      }
    }

    if (pNewData != NULL)
    {
      memcpy (pData, pNewData, RealOutputLength);
    }

    Result = SDAI_SUCCESS;
  }

  interface_unlock_unit (UnitIndex);

  return (Result);
}

/*===========================================================================*/

/**
 * @desc The function sdai_pn_set_data() sets the output data for the unit defined by
 *       Id. This function can only be used on input units.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *       SDAI_ERR_NOSUCHUNIT |
 *       SDAI_ERR_INVALID_ARGUMENT |
 *       SDAI_ERR_STACK_NOT_RDY
 * @param[in] Id
 * - type : U32
 * - range: ???
 * @param[in] Status
 * - type : U8
 * - range: ???
 * @param[in] pData
 * - type : U8*
 * - range: whole address space
 */
static U8 sdai_drv_pn_set_data (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U32 Id, U8 Status, const U8* pData)
{
  T_SDAI_DRV_PN*      pSdaiBackendDriverPN;

  T_INTERFACE_UNIT*   pUnit;

  U16                 UnitIndex;
  U8                  RealInputLength;


  _TRACE (("sdai_drv_pn_set_data"));

  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverPN = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPN->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  UnitIndex = interface_find_unit (Id, pSdaiBackendDriverPN->HighestUnitIndex);

  if (UnitIndex == 0xFFFF)
  {
    return (SDAI_ERR_NOSUCHUNIT);
  }

  pUnit = interface_lock_unit (UnitIndex);

  if (pUnit == NULL)
  {
    interface_unlock_unit (UnitIndex);
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (pUnit->InternalFlags & SDAI_INTERFACE_MASK)
  {
    interface_unlock_unit (UnitIndex);
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if ( (pUnit->DataLengthInput > 0u) && (pData == NULL) )
  {
    interface_unlock_unit (UnitIndex);
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  _ASSERT ( (pUnit->Type == SDAI_UNIT_TYPE_HEAD)                                  ||
            ((pUnit->DataInputOffset != 0xFFFF) && (pUnit->DataLengthInput > 0) ) ||
            ((pUnit->DataInputOffset == 0xFFFF) && (pUnit->DataLengthInput == 0)) );

  RealInputLength = _SDAI_PN_EXTENTION_TO_INPUT_LENGTH (U8, pUnit->ProtocolExtention);

  pSdaiBackendDriverPN->pIODataBase [pUnit->DataInputOffset + RealInputLength] = Status;

  if (RealInputLength > 0u)
  {
    memcpy (&pSdaiBackendDriverPN->pIODataBase [pUnit->DataInputOffset], pData, RealInputLength);
  }

  interface_unlock_unit (UnitIndex);

  if (! (pSdaiBackendDriver->ApplicationInitData.Info.Flags & SDAI_DATA_CONSISTENCY_IO_IMAGE))
  {
    interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_UPDATE_INPUT_DATA);
  }

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * @desc The function sdai_pn_process_data_update_finished() notifies the PROFINET
 *       stack about changed input data.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS
 */
static U8 sdai_drv_pn_process_data_update_finished (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);

  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_UPDATE_INPUT_DATA);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/
/**
 * @desc The function sdai_drv_pn_alarm_request() requests an alarm for the module corresponding
 *       to the given Id. If there is still an alarm pending, the functions returns with
 *       SDAI_ERR_ALARM_PENDING.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_INTERNAL |
 *           SDAI_ERR_SERVICE_PENDING |
 *           SDAI_ERR_STACK_NOT_RDY
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pAlarm
 * - type : struct SDAI_ALARM_REQ*
 * - range: whole address space
 * @remarks There can only be one alarm pending at the same time.
 */
static U8 sdai_drv_pn_diagnosis_request (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_DIAGNOSIS_REQ* pDiagnosis)
{
  T_SDAI_DRV_PN*                  pSdaiBackendDriverPN;

  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  T_INTERFACE_STATUS_DATA*        pStatusData;
  struct SDAI_DIAGNOSIS_REQ*      pServiceDiagnosis;

  U16                             UnitIndex;


  _TRACE (("sdai_drv_pn_diagnosis_request"));

  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pDiagnosis);

  pSdaiBackendDriverPN = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPN->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  UnitIndex = interface_find_unit(pDiagnosis->Id, pSdaiBackendDriverPN->HighestUnitIndex);

  if (UnitIndex == 0xFFFF)
  {
    return (SDAI_ERR_NOSUCHUNIT);
  }

  pStatusData = interface_lock_data (STATUS_INTERFACE);

  if (pStatusData == NULL)
  {
    interface_unlock_data (STATUS_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (!(pStatusData->StackStatus & (SDAI_STATE_MASK_INIT | SDAI_STATE_MASK_ONLINE)))
  {
    interface_unlock_data (STATUS_INTERFACE);
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  interface_unlock_data (STATUS_INTERFACE);

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  pSdaiBackendDriverPN = pSdaiBackendDriver->pPrivateData;

  pServiceDiagnosis = &pServiceData->UseAs.Diagnosis;

  pServiceData->ServiceType          = SERVICE_REQ_RES_DIAGNOSIS;
  pServiceDiagnosis->Id              = pDiagnosis->Id;
  pServiceDiagnosis->Type            = pDiagnosis->Type;
  pServiceDiagnosis->Severity        = pDiagnosis->Severity;
  pServiceDiagnosis->StatusSpecifier = pDiagnosis->StatusSpecifier;
  pServiceDiagnosis->Length          = pDiagnosis->Length;

  memcpy ((pServiceDiagnosis + 1), (pDiagnosis + 1), pDiagnosis->Length);

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/
/**
 * @desc The function sdai_drv_pn_alarm_request() requests an alarm for the module corresponding
 *       to the given Id. If there is still an alarm pending, the functions returns with
 *       SDAI_ERR_ALARM_PENDING.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_INTERNAL |
 *           SDAI_ERR_SERVICE_PENDING |
 *           SDAI_ERR_STACK_NOT_RDY
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pAlarm
 * - type : struct SDAI_ALARM_REQ*
 * - range: whole address space
 * @remarks There can only be one alarm pending at the same time.
 */
static U8 sdai_drv_pn_alarm_request (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_ALARM_REQ* pAlarm)
{
  T_SDAI_DRV_PN*                  pSdaiBackendDriverPN;

  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  T_INTERFACE_STATUS_DATA*        pStatusData;
  struct SDAI_ALARM_REQ*          pServiceAlarm;

  U16                             UnitIndex;


  _TRACE (("sdai_drv_pn_alarm_request"));

  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pAlarm);

  pSdaiBackendDriverPN = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPN->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  UnitIndex = interface_find_unit(pAlarm->Id, pSdaiBackendDriverPN->HighestUnitIndex);

  if (UnitIndex == 0xFFFF)
  {
    return (SDAI_ERR_NOSUCHUNIT);
  }

  pStatusData = interface_lock_data (STATUS_INTERFACE);

  if (pStatusData == NULL)
  {
    interface_unlock_data (STATUS_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (!(pStatusData->StackStatus & (SDAI_STATE_MASK_INIT | SDAI_STATE_MASK_ONLINE | SDAI_STATE_MASK_CONNECTED)))
  {
    interface_unlock_data (STATUS_INTERFACE);
    return (SDAI_ERR_STACK_NOT_RDY);
  }


  if ( (pStatusData->StackAddStatus.UseAs.Pn.UnitData[UnitIndex].CylceTime == 0u) ||
       ((pAlarm->AlarmPriority != SDAI_ALARM_PRIO_LOW) && (pAlarm->AlarmPriority != SDAI_ALARM_PRIO_HIGH)) )
  {
    interface_unlock_data (STATUS_INTERFACE);
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  interface_unlock_data (STATUS_INTERFACE);

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  pSdaiBackendDriverPN = pSdaiBackendDriver->pPrivateData;

  if (! (pSdaiBackendDriverPN->AlarmPending & pAlarm->AlarmPriority))
  {
    pSdaiBackendDriverPN->AlarmPending |= pAlarm->AlarmPriority;

    pServiceAlarm = &pServiceData->UseAs.Alarm;

    pServiceData->ServiceType     = SERVICE_REQ_RES_ALARM;
    pServiceAlarm->Id             = pAlarm->Id;
    pServiceAlarm->AlarmType      = pAlarm->AlarmType;
    pServiceAlarm->AlarmPriority  = pAlarm->AlarmPriority;
    pServiceAlarm->Length         = pAlarm->Length;

    memcpy ((pServiceAlarm + 1), (pAlarm + 1), pAlarm->Length);

    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

    return (SDAI_SUCCESS);
  }

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  return (SDAI_ERR_SERVICE_PENDING);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_pn_write_response() handles the PROFINET IO specific
 *       part of the write response. It checks the parameters, copies the data to the
 *       shared ram and sends a signal to the stack.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY |
 *           SDAI_ERR_SERVICE_PENDING |
 *           SDAI_ERR_INTERNAL
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pWrite
 * - type : const struct SDAI_WRITE_RES*
 * - range: whole address space
 * @remarks
 */
static U8 sdai_drv_pn_write_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_WRITE_RES* pWrite)
{
  T_SDAI_DRV_PN*                  pSdaiBackendDriverPN;

  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  struct SDAI_WRITE_RES*          pServiceWrite;


  _TRACE (("sdai_drv_pn_write_response"));

  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pWrite);

  pSdaiBackendDriverPN = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPN->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  pServiceWrite = &pServiceData->UseAs.Write;

  pServiceData->ServiceType     = SERVICE_REQ_RES_WRITE;
  pServiceWrite->UseAs.Pn.Id    = pWrite->UseAs.Pn.Id;
  pServiceWrite->UseAs.Pn.Index = pWrite->UseAs.Pn.Index;
  pServiceWrite->ErrorCode      = pWrite->ErrorCode;

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_pn_read_response() handles the PROFINET IO specific
 *       part of the read response. It checks the parameters, copies the data to the
 *       shared ram and sends a signal to the stack.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY |
 *           SDAI_ERR_SERVICE_PENDING |
 *           SDAI_ERR_INTERNAL
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pRead
 * - type : const struct SDAI_READ_RES*
 * - range: whole address space
 */
static U8 sdai_drv_pn_read_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_READ_RES* pRead)
{
  T_SDAI_DRV_PN*                  pSdaiBackendDriverPN;

  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  struct SDAI_READ_RES*           pServiceRead;


  _TRACE (("sdai_drv_pn_read_response"));

  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pRead);

  pSdaiBackendDriverPN = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPN->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  pServiceRead = &pServiceData->UseAs.Read;

  pServiceData->ServiceType = SERVICE_REQ_RES_READ;

  pServiceRead->UseAs.Pn.Id    = pRead->UseAs.Pn.Id;
  pServiceRead->UseAs.Pn.Index = pRead->UseAs.Pn.Index;
  pServiceRead->ErrorCode      = pRead->ErrorCode;
  pServiceRead->Length         = pRead->Length;

  memcpy ((pServiceRead + 1), (pRead + 1), pRead->Length);

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_pn_control_response() handles the Profinet specific
 * part of the control response. It checks the parameters, copies the data to the
 * shared ram and sends a signal to the stack.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY |
 *           SDAI_ERR_SERVICE_PENDING |
 *           SDAI_ERROR
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pControl
 * - type : const struct SDAI_CONTROL_RES*
 * - range: whole address space
 */
static U8 sdai_drv_pn_control_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_CONTROL_RES* pControl)
{
  T_SDAI_DRV_PN*                  pSdaiBackendDriverPN;

  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  struct SDAI_CONTROL_RES*        pServiceControl;


  _ASSERT_PTR (pControl);
  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverPN = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPN->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  if ( (pControl->ControlCode != SDAI_PN_CONTROL_PARAM_END      ) &&
       (pControl->ControlCode != SDAI_CONTROL_CODE_CFG_DATA_INFO) )
  {
    if ( (pControl->ControlCode == SDAI_CONTROL_RESET_TO_DEFAULTS          ) ||
         (pControl->ControlCode == SDAI_PN_CONTROL_BLINK                   ) ||
         (pControl->ControlCode == SDAI_PN_CONTROL_RESET_APPLICATION_DATA  ) ||
         (pControl->ControlCode == SDAI_PN_CONTROL_RESET_COMMUNICATION_DATA) ||
         (pControl->ControlCode == SDAI_PN_CONTROL_RESET_ENGINEERING_DATA  ) ||
         (pControl->ControlCode == SDAI_PN_CONTROL_RESET_RESET_AND_RESTORE ) )
    {
      return (SDAI_SUCCESS);
    }
    else
    {
      return (SDAI_ERR_INVALID_ARGUMENT);
    }
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  pServiceControl = &pServiceData->UseAs.Control;

  pServiceData->ServiceType    = SERVICE_REQ_RES_CONTROL;
  pServiceControl->ControlCode = pControl->ControlCode;
  pServiceControl->ErrorCode   = pControl->ErrorCode;
  pServiceControl->Length      = 0;

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * @desc The function sdai_pn_start() starts the Profinet stack. This function is called
 *       when the applications signals "plugging complete".
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_INTERNAL
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_drv_pn_proto_start (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;


  _TRACE (("sdai_drv_pn_proto_start"));

  _ASSERT_PTR (pSdaiBackendDriver);

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData != NULL)
  {
    if (pServiceData->ServiceType == SERVICE_REQ_RES_FREE)
    {
      pServiceData->ServiceType = SERVICE_REQ_RES_INIT;

      memcpy
      (
        &pServiceData->UseAs.Init.Device,
        &pSdaiBackendDriver->ApplicationInitData.Info,
        sizeof (pServiceData->UseAs.Init.Device)
      );

      memcpy
      (
        &pServiceData->UseAs.Init.Ident,
        &pSdaiBackendDriver->ApplicationInitData.Ident,
        sizeof (pServiceData->UseAs.Init.Ident)
      );

      interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
      interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_START_STACK);

      return (SDAI_SUCCESS);
    }
  }

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);

  return (SDAI_ERR_INTERNAL);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_pn_plugging_complete() returns the plugging state of
 *       modules.
 * @return
 * - type  : int
 * - values: 0, 1
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static int sdai_drv_pn_plugging_complete (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_SDAI_DRV_PN*      pSdaiBackendDriverPN;


  _TRACE (("sdai_drv_pn_plugging_complete"));

  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverPN = pSdaiBackendDriver->pPrivateData;

  return (pSdaiBackendDriverPN->PlugComplete);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_pn_init_watchdog()
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_drv_pn_init_watchdog (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const U16 WatchdogFactor)
{
  T_INTERFACE_MANAGEMENT_DATA*    pManagementData;
  U8                              Result = SDAI_ERR_COM_NOT_SUPPORTED;

  _TRACE (("sdai_drv_pn_init_watchdog"));

  _ASSERT_PTR (pSdaiBackendDriver);

  if (check_watchdog_supported () == SDAI_SUCCESS)
  {
    pManagementData = interface_lock_data (MANAGEMENT_INTERFACE);

    pManagementData->WatchdogFactor          = (WatchdogFactor * 100);
    pManagementData->WatchdogRunning         = 0;
    pManagementData->WatchdogFlagStack       = 0;
    pManagementData->WatchdogFlagApplication = 0;

    /* install the real trigger function */
    pSdaiBackendDriver->Interface.check_trigger_watchdog = sdai_drv_pn_check_and_trigger_watchdog;

    interface_unlock_data (MANAGEMENT_INTERFACE);
    interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_INIT_WATCHDOG);

    Result = SDAI_SUCCESS;
  }

  return (Result);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_pn_check_and_trigger_watchdog()
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS | #SDAI_ERR_WATCHDOG_EXPIRED
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_drv_pn_check_and_trigger_watchdog (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_INTERFACE_MANAGEMENT_DATA*    pManagementData;
  U8                              Result = SDAI_SUCCESS;


  _TRACE (("sdai_drv_pn_check_and_trigger_watchdog"));

  _ASSERT_PTR (pSdaiBackendDriver);
  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);

  pManagementData = interface_lock_data (MANAGEMENT_INTERFACE);

  /* Trigger WD */
  pManagementData->WatchdogFlagApplication = 0;

  if (pManagementData->WatchdogRunning == 1)
  {
    /* Check WD */
    pManagementData->WatchdogFlagStack += 1;

    if (pManagementData->WatchdogFlagStack > 2)
    {
      handle_stack_watchdog_expired ();
      Result = SDAI_ERR_WATCHDOG_EXPIRED;
    }
  }
  else
  {
    pManagementData->WatchdogRunning = 1;
  }

  interface_unlock_data (MANAGEMENT_INTERFACE);

  return (Result);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_pn_lock_resources()
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS | #SDAI_ERR_INTERNAL
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] Resources Specifies which resources should be unlocked
 * - type : U16
 * - range: #SDAI_SHARED_RESOURCE_FLASH |
 *          #SDAI_SHARED_RESOURCE_MDIO
 */
static U8 sdai_drv_pn_lock_resources (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U16 Resource)
{
  _TRACE (("sdai_drv_pn_lock_resources"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);

  if (Resource == SDAI_SHARED_RESOURCE_FLASH)
  {
    if (interface_lock_data (RESOURCE_INTERFACE) != NULL)
    {
      return (SDAI_SUCCESS);
    }
  }
  else if (Resource == SDAI_SHARED_RESOURCE_MDIO)
  {
    if (interface_lock_data (SHARED_MDIO_INTERFACE) != NULL)
    {
      return (SDAI_SUCCESS);
    }
  }

  return (SDAI_ERR_INTERNAL);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_pn_unlock_resources()
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] Resources Specifies which resources should be unlocked
 * - type : U16
 * - range: #SDAI_SHARED_RESOURCE_FLASH |
 *          #SDAI_SHARED_RESOURCE_MDIO
 */
static U8 sdai_drv_pn_unlock_resources (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U16 Resource)
{
  _TRACE (("sdai_drv_pn_unlock_resources"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);

  if (Resource == SDAI_SHARED_RESOURCE_FLASH)
  {
    interface_unlock_data (RESOURCE_INTERFACE);
  }
  else if (Resource == SDAI_SHARED_RESOURCE_MDIO)
  {
    interface_unlock_data (SHARED_MDIO_INTERFACE);
  }

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * @desc The function sdai_pn_int_callback() handels the signals between the stack and
 *       the application part of the SDAI.
 * @return
 * - type  : int
 * - values: 0
 * @param[in] Signal
 * - type : U16 int
 * - range: INTERFACE_SIGNAL_SERVICE_IND
 * @param[in] pContext
 * - type : VOID*
 * - range: whole address space
 */
static int sdai_drv_pn_handle_stack_signal (U16 Signal, void* pContext)
{
  T_SDAI_BACKEND_DRIVER*   pSdaiBackendDriver = pContext;


  _TRACE (("sdai_drv_pn_handle_stack_signal"));

  if (Signal & INTERFACE_SIGNAL_SERVICE_IND)
  {
    sdai_drv_pn_handle_service_ind (pSdaiBackendDriver);
  }

  if ( (Signal & INTERFACE_SIGNAL_DATA_IND   ) ||
       (Signal & INTERFACE_SIGNAL_DATA_UPDATE) )
  {
    sdai_drv_pn_handle_data_ind (pSdaiBackendDriver, Signal);
  }

#ifdef SDAI_INCLUDE_SOCKET
  if (Signal & INTERFACE_SIGNAL_SOCKET_IND)
  {
    sdai_drv_sock_handle_socket_ind (Signal);
  }
#endif

  return 0;
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_pn_handle_data_ind() handels the incoming
 *       meassages in the data indication channel (Unit interface).
 * @return
 * - type  : VOID
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static void sdai_drv_pn_handle_data_ind (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U16 Signal)
{
  T_INTERFACE_UNIT*   pUnit;

  U16                 UnitIndex;
  U16                 NumberChangedUnits = 0;


  _TRACE (("sdai_drv_pn_handle_data_ind"));

  pUnit = interface_lock_unit (0);

  for (UnitIndex = 0; UnitIndex <= SdaiBackendDriverPN.HighestUnitIndex; UnitIndex++)
  {
    if (pUnit->InternalFlags & UNIT_INTERNAL_FLAG_OUTPUT_CHANGED)
    {
      pUnit->InternalFlags &= ~UNIT_INTERNAL_FLAG_OUTPUT_CHANGED;

      OutputChangedInd.Id[NumberChangedUnits] = pUnit->Id;
      NumberChangedUnits++;
    }
    else if ( (Signal == INTERFACE_SIGNAL_DATA_UPDATE             ) &&
              ((pUnit->Type == SDAI_UNIT_TYPE_GENERIC_OUTPUT     ) ||
               (pUnit->Type == SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT)) )
    {
      OutputChangedInd.Id [NumberChangedUnits] = pUnit->Id;
      NumberChangedUnits++;
    }

    pUnit++;
  }

  interface_unlock_unit (0);

  if (NumberChangedUnits > 0)
  {
    OutputChangedInd.NumberUnits = NumberChangedUnits;

    if (pSdaiBackendDriver->ApplicationInitData.Callback.DataCbk != NULL)
    {
      pSdaiBackendDriver->ApplicationInitData.Callback.DataCbk (&OutputChangedInd);
    }
  }
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_pn_handle_service_ind() handels the incoming
 *       meassages in the service indication channel.
 * @return
 * - type  : VOID
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static void sdai_drv_pn_handle_service_ind (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_INTERFACE_SERVICE_IND*    pServiceData;
  T_SDAI_DRV_PN*              pSdaiBackendDriverPN;


  _TRACE (("sdai_drv_pn_handle_service_ind"));

  pSdaiBackendDriverPN = pSdaiBackendDriver->pPrivateData;

  pServiceData = interface_lock_data (SERVICE_IND_INTERFACE);

  if (pServiceData != NULL)
  {
    switch (pServiceData->ServiceType)
    {
      case SERVICE_IND_EXCEPTION:
      {
        if (pSdaiBackendDriver->ApplicationInitData.Callback.ExceptionCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.ExceptionCbk (&pServiceData->UseAs.Exception);
        }

        pSdaiBackendDriverPN->ExceptionPending = 1;
        break;
      }

      case SERVICE_IND_WRITE:
      {
        if (pServiceData->UseAs.Write.UseAs.Pn.Index == SDAI_DRV_PN_INDEX_SUBSLOT_SUBSTITUTE)
        {
          struct SDAI_WRITE_IND*          pWrite = &pServiceData->UseAs.Write;
          struct SDAI_WRITE_RES*          pWriteRes = (struct SDAI_WRITE_RES*)pWrite;
          T_SDAI_DRV_PN_SUBSTITUTE_VALUE* pSubstituteValue = (T_SDAI_DRV_PN_SUBSTITUTE_VALUE*)(pWrite + 1);
          T_INTERFACE_UNIT*               pUnit;
          U16                             UnitIndex;

          UnitIndex = interface_find_unit (pWrite->UseAs.Pn.Id, pSdaiBackendDriverPN->HighestUnitIndex);

          pUnit = interface_lock_unit (UnitIndex);

          if (pUnit != NULL)
          {
            pSdaiBackendDriverPN->SubstituteData [pUnit->DataOutputOffset + pUnit->DataLengthOutput] = pSubstituteValue->SubstitutionModeLowByte;
            memcpy (&pSdaiBackendDriverPN->SubstituteData [pUnit->DataOutputOffset], (pSubstituteValue + 1), pUnit->DataLengthOutput);
          }

          interface_unlock_unit (UnitIndex);

          pWriteRes->ErrorCode = SDAI_SERVICE_SUCCESS;

          sdai_drv_pn_write_response (pSdaiBackendDriver, (const struct SDAI_WRITE_RES*) pWrite);
        }
        else
        {
          if (pSdaiBackendDriver->ApplicationInitData.Callback.WriteReqCbk != NULL)
          {
            pSdaiBackendDriver->ApplicationInitData.Callback.WriteReqCbk (&pServiceData->UseAs.Write);
          }
        }

        break;
      }

      case SERVICE_IND_READ:
      {
        if (pSdaiBackendDriver->ApplicationInitData.Callback.ReadReqCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.ReadReqCbk (&pServiceData->UseAs.Read);
        }

        break;
      }

      case SERVICE_IND_IDENT:
      {
        if (pSdaiBackendDriver->ApplicationInitData.Callback.IdentDataCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.IdentDataCbk (&pServiceData->UseAs.Ident);
        }

        break;
      }

      case SERVICE_IND_ALARM_ACK:
      {
        if (pSdaiBackendDriver->ApplicationInitData.Callback.AlarmAckCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.AlarmAckCbk (&pServiceData->UseAs.AlarmAck);
        }

        /* now the related alarm is acknowledged and we can transmit a new alarm of this priority */
        pSdaiBackendDriverPN->AlarmPending &= (~pServiceData->UseAs.AlarmAck.AlarmPriority);
        break;
      }

      case SERVICE_IND_CONTROL:
      {
        if(pSdaiBackendDriver->ApplicationInitData.Callback.ControlReqCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.ControlReqCbk (&pServiceData->UseAs.Control);
        }

        break;
      }

      default:
      {
        break;
      }
    }

    pServiceData->ServiceType = SERVICE_IND_FREE;

  #if 0
    /* If pending services on stack side, signal that channel is free */
    if (pServiceData->ServicesPending != 0x00)
  #endif
    {
      interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_CHANNEL_FREE);
    }
  }

  interface_unlock_data (SERVICE_IND_INTERFACE);

  return;
}
#endif  /* SDAI_INCLUDE_PNAK */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
