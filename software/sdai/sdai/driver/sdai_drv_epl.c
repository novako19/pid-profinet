/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

#include <stdio.h>
#include <string.h>

#ifdef _DEBUG
#include <assert.h>
#endif

#include "sdai.h"

#include "cfg_cc.h"

#include "sdai_ext.h"
#include "sdai_interface.h"

#ifdef SDAI_INCLUDE_SOCKET
  #include "sdai_socket.h"
  #include "socket_interface.h"
#endif

#include "sdai_drv.h"

#include "porting.h"

#ifdef SDAI_INCLUDE_EPL

/******************************************************************************
DEFINES
******************************************************************************/

#define SDAI_EPL_MIN_RXPDO_INDEX    0x1600
#define SDAI_EPL_MIN_TXPDO_INDEX    0x1A00

/******************************************************************************
TYPEDEFS
******************************************************************************/

typedef struct _T_SDAI_DRV_EPL
{
  int                         ShutdownPending;              /**< indicates a pending shutdown command */
  int                         ExceptionPending;             /**< indicates a pending exception */

  U8                          ThisNodeId;                   /**< Node ID of this CN station */

  U16                         CurrentSlot;                  /**< the next free slot in the device */
  U16                         TxPdoIndex;                   /**< the next free TxPDO index number */
  U8                          TxPdoNumber;                  /**< number of plugged TxPDOs */
  U16                         RxPdoIndex;                   /**< the next free RxPDO index number */
  U8                          RxPdoNumber;                  /**< number of plugged RxPDOs */
  struct SDAI_EPL_CFG_DATA    EplCfgData [SDAI_MAX_UNITS];  /**< EPL specific configuration data for each unit */

  U8*                         pIODataBase;                  /**< */

  int                         PlugComplete;                 /**< user signaled plugging complete */

} T_SDAI_DRV_EPL;

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

static U8     sdai_drv_epl_deinit                    (T_SDAI_BACKEND_DRIVER*);
static U8     sdai_drv_epl_get_version               (T_SDAI_BACKEND_DRIVER*, struct SDAI_VERSION_DATA*);
static U8     sdai_drv_epl_get_state                 (T_SDAI_BACKEND_DRIVER*, U16*, struct SDAI_ADDSTATUS_DATA*);
static U8     sdai_drv_epl_plug_unit                 (T_SDAI_BACKEND_DRIVER*, U8, U8, U8, U32*, const struct SDAI_CFG_DATA_EXT*);

static U8     sdai_drv_epl_get_data                  (T_SDAI_BACKEND_DRIVER*, U32, U8*, U8*);
static U8     sdai_drv_epl_set_data                  (T_SDAI_BACKEND_DRIVER*, U32, U8, const U8*);

static U8     sdai_drv_epl_write_response            (T_SDAI_BACKEND_DRIVER*, const struct SDAI_WRITE_RES*);
static U8     sdai_drv_epl_read_response             (T_SDAI_BACKEND_DRIVER*, const struct SDAI_READ_RES*);
static U8     sdai_drv_epl_control_response          (T_SDAI_BACKEND_DRIVER*, const struct SDAI_CONTROL_RES*);

static U8     sdai_drv_epl_proto_start               (T_SDAI_BACKEND_DRIVER*);
static int    sdai_drv_epl_plugging_complete         (T_SDAI_BACKEND_DRIVER*);

static U8     sdai_drv_epl_lock_resources            (T_SDAI_BACKEND_DRIVER*, U16);
static U8     sdai_drv_epl_unlock_resources          (T_SDAI_BACKEND_DRIVER*, U16);

static int    sdai_drv_epl_handle_stack_signal       (U16, void*);
static int    sdai_drv_epl_handle_synch_signal       (U8, void*);
static void   sdai_drv_epl_handle_service_ind        (T_SDAI_BACKEND_DRIVER*);
static void   sdai_drv_epl_handle_data_ind           (T_SDAI_BACKEND_DRIVER*);

/******************************************************************************
PUBLIC DATA
******************************************************************************/

/******************************************************************************
PRIVATE DATA
******************************************************************************/

static T_SDAI_DRV_EPL                   SdaiBackendDriverEPL;       /**< local instance of EPL driver struct */
static struct SDAI_OUTPUT_CHANGED_IND   OutputChangedInd;           /**< holds informations of changed units for data cbk */
T_INTERFACE_SERVICE_IND                 ServiceIndBuffer;           /**< temporary buffer for service indications */
static struct SDAI_SYNC_SIGNAL          SyncSignalBuffer;           /**< temporary buffer for synch signals */

/******************************************************************************
PUBLIC FUNCTIONS
******************************************************************************/

/******************************************************************************
PROTECTED FUNCTIONS
******************************************************************************/

/**
 * The function sdai_epl_init() initializes the Powerlink backend.
 * @return
 * - type  : struct SDAI_BACKEND_DRV*
 * - values: whole address space
 * @param[in] InitData
 * - type : SDAI_INIT*
 * - range: whole address space
 */
U8 sdai_drv_epl_init (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, struct SDAI_INIT* pApplicationInitData)
{
  T_INTERFACE_MANAGEMENT_DATA*    pManagementData;
  U8                              Result = SDAI_ERR_INTERNAL;


  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pApplicationInitData);

  /*-------------------------------------------------------------------------*/

  if ( (pApplicationInitData->Ident.UseAs.Epl.NodeId < SDAI_EPL_CN_NODE_ID_MIN_VALUE) ||
       (pApplicationInitData->Ident.UseAs.Epl.NodeId > SDAI_EPL_CN_NODE_ID_MAX_VALUE) )
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  /*-------------------------------------------------------------------------*/

  /* @brief The interface of the driver is initialized with default function. So
   * we have to overwrite only functions we support in the Powerlink implementation.
   */
  pSdaiBackendDriver->Interface.deinit            = sdai_drv_epl_deinit;
  pSdaiBackendDriver->Interface.get_version       = sdai_drv_epl_get_version;
  pSdaiBackendDriver->Interface.get_state         = sdai_drv_epl_get_state;
  pSdaiBackendDriver->Interface.plug_unit         = sdai_drv_epl_plug_unit;

  pSdaiBackendDriver->Interface.get_data          = sdai_drv_epl_get_data;
  pSdaiBackendDriver->Interface.set_data          = sdai_drv_epl_set_data;

  pSdaiBackendDriver->Interface.write_response    = sdai_drv_epl_write_response;
  pSdaiBackendDriver->Interface.read_response     = sdai_drv_epl_read_response;
  pSdaiBackendDriver->Interface.control_response  = sdai_drv_epl_control_response;

  pSdaiBackendDriver->Interface.proto_start       = sdai_drv_epl_proto_start;
  pSdaiBackendDriver->Interface.plugging_complete = sdai_drv_epl_plugging_complete;

  pSdaiBackendDriver->Interface.lock_resources    = sdai_drv_epl_lock_resources;
  pSdaiBackendDriver->Interface.unlock_resources  = sdai_drv_epl_unlock_resources;

  pSdaiBackendDriver->pPrivateData                = &SdaiBackendDriverEPL;

  /*-------------------------------------------------------------------------*/

  memset (SdaiBackendDriverEPL.EplCfgData, 0, sizeof (SdaiBackendDriverEPL.EplCfgData));
  memset (&ServiceIndBuffer, 0, sizeof (ServiceIndBuffer));

  SdaiBackendDriverEPL.PlugComplete      = 0;
  SdaiBackendDriverEPL.ShutdownPending   = 0u;
  SdaiBackendDriverEPL.ThisNodeId        = pApplicationInitData->Ident.UseAs.Epl.NodeId;
  SdaiBackendDriverEPL.CurrentSlot       = 0;
  SdaiBackendDriverEPL.TxPdoIndex        = SDAI_EPL_MIN_TXPDO_INDEX;
  SdaiBackendDriverEPL.TxPdoNumber       = 0;
  SdaiBackendDriverEPL.RxPdoIndex        = SDAI_EPL_MIN_RXPDO_INDEX;
  SdaiBackendDriverEPL.RxPdoNumber       = 0;


  Result = interface_init
  (
    sdai_drv_epl_handle_stack_signal,
    sdai_drv_epl_handle_synch_signal,
    INTERFACE_APPLICATION,
    pSdaiBackendDriver,
    SDAI_BACKEND_EPL
  );

  if (Result == SDAI_SUCCESS)
  {
    SdaiBackendDriverEPL.pIODataBase = interface_get_iodata_base ();

    pManagementData = interface_lock_data (MANAGEMENT_INTERFACE);

    if (pManagementData != NULL)
    {
      pManagementData->SupportedCallbacks = 0u;

      if (pApplicationInitData->Callback.DataCbk != NULL)       { pManagementData->SupportedCallbacks |= DATA_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.IdentDataCbk != NULL)  { pManagementData->SupportedCallbacks |= IDENT_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.WriteReqCbk != NULL)   { pManagementData->SupportedCallbacks |= WRITE_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.ReadReqCbk != NULL)    { pManagementData->SupportedCallbacks |= READ_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.ControlReqCbk != NULL) { pManagementData->SupportedCallbacks |= CONTROL_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.ExceptionCbk != NULL)  { pManagementData->SupportedCallbacks |= EXCEPTION_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.SyncSignalCbk != NULL) { pManagementData->SupportedCallbacks |= SYNC_SIGNAL_CALLBACK_SUPPORTED; }

      pManagementData->BackEnd   = SDAI_BACKEND_EPL;
      pManagementData->ApplReady = SDAI_INTERFACE_APPLICATION_READY;
    }

    interface_unlock_data (MANAGEMENT_INTERFACE);
  }

  return (Result);
}

/******************************************************************************
PRIVATE FUNCTIONS
******************************************************************************/

/**
 * The function sdai_drv_epl_deinit() stops the Powerlink CN Stack and
 * frees the data structure.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS
 */
static U8 sdai_drv_epl_deinit (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_SDAI_DRV_EPL*    pSdaiBackendDriverEPL;


  pSdaiBackendDriverEPL = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverEPL->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pSdaiBackendDriverEPL->ShutdownPending = 1;

  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_STOP_STACK);

  /* wait for backend shutting down */
  interface_deinit (INTERFACE_APPLICATION);

  pSdaiBackendDriver->pPrivateData = NULL;

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_epl_get_version() returns the version string of the
 * Powerlink CN stack, SDAI and Powerlink mapping layer.
 * @return
 * - type  : char*
 * - values: ???
 */
static U8 sdai_drv_epl_get_version (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, struct SDAI_VERSION_DATA* pVersionData)
{
  T_SDAI_DRV_EPL*              pSdaiBackendDriverEPL;
  T_INTERFACE_STATUS_DATA*     pStatusData;


  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pVersionData);

  pSdaiBackendDriverEPL = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverEPL->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  pStatusData = interface_lock_data (STATUS_INTERFACE);
  memcpy (pVersionData, &pStatusData->VersionData, sizeof(struct SDAI_VERSION_DATA));
  interface_unlock_data (STATUS_INTERFACE);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_epl_get_state() returns the current state of the Powerlink CN stack.
 *
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY
 * @pre [enter precondition here]
 * @post [enter postcondition here]
 * @remarks [enter remarks, i.e. compatibility issues, here]
 */
static U8 sdai_drv_epl_get_state (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U16* pState, struct SDAI_ADDSTATUS_DATA* pAddStatus)
{
  T_SDAI_DRV_EPL*              pSdaiBackendDriverEPL;
  T_INTERFACE_STATUS_DATA*     pStatusData;


  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pState);

  pSdaiBackendDriverEPL = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverEPL->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pStatusData = interface_lock_data (STATUS_INTERFACE);

  *pState = pStatusData->StackStatus;

  if (pAddStatus != NULL)
  {
    memcpy (pAddStatus, &pStatusData->StackAddStatus, sizeof (struct SDAI_ADDSTATUS_DATA));
  }

  interface_unlock_data (STATUS_INTERFACE);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_epl_plug_unit() makes the Powerlink specific part of
 * plugging.
 * It calculates the ID for the unit and returns it to the application.
 * @return
 * - type  : U32
 * - values: ???
 * @param[in] UnitType
 * - type : U8
 * - range: SDAI_UNIT_TYPE_INPUT | SDAI_UNIT_TYPE_OUTPUT
 * @param[in] InputSize defines the input data length of the unit in bytes.
 * - type : U8
 * - range: 0 to 255
 * @param[in] OutputSize defines the output data length of the unit in bytes.
 * - type : U8
 * - range: 0 to 255
 * @param[out] pUnit
 * - type : SDAI_UNIT *
 * - range: whole address space
 */
static U8 sdai_drv_epl_plug_unit (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U8 UnitType, U8 InputSize, U8 OutputSize, U32* pId, const struct SDAI_CFG_DATA_EXT* pCfgData)
{
  T_SDAI_DRV_EPL*                 pSdaiBackendDriverEPL;
  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  T_INTERFACE_UNIT*               pUnit;
  U16                             UnitIndex = 0xFFFF;
  U16                             PdoIndex  = 0;

  volatile U16                    ServiceType;


  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverEPL = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverEPL->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  *pId = SDAI_INVALID_ID;

  /* if users signals plugging complete we don't want to check the args */
  if(UnitType == SDAI_UNIT_TYPE_PLUGGING_COMPLETE)
  {
    pSdaiBackendDriverEPL->PlugComplete = 1;

    return (pSdaiBackendDriver->Interface.proto_start (pSdaiBackendDriver));
  }

  if ( (UnitType != SDAI_UNIT_TYPE_GENERIC_INPUT)  &&
       (UnitType != SDAI_UNIT_TYPE_GENERIC_OUTPUT) )
  {
    return (SDAI_ERR_UNKNOWN_UNIT_TYPE);
  }

  if (pCfgData != NULL)
  {
    if (UnitType == SDAI_UNIT_TYPE_GENERIC_OUTPUT)
    {
      if (pSdaiBackendDriverEPL->RxPdoNumber >= SDAI_EPL_MAX_NUMBER_RXPDO_UNITS)
      {
        return (SDAI_ERR_MAX_UNIT_REACHED);
      }

      if ( (pCfgData->UseAs.Epl.NodeId > SDAI_EPL_NODE_ID_MN_RX_MULTICAST) || (pCfgData->UseAs.Epl.NodeId == pSdaiBackendDriverEPL->ThisNodeId) )
      {
        /* invalid Node ID or the Node ID of this station is set
         */
        return (SDAI_ERR_INVALID_ARGUMENT);
      }
    }
    else
    {
      _ASSERT (UnitType == SDAI_UNIT_TYPE_GENERIC_INPUT);

      if (pSdaiBackendDriverEPL->TxPdoNumber >= SDAI_EPL_MAX_NUMBER_TXPDO_UNITS)
      {
        return (SDAI_ERR_MAX_UNIT_REACHED);
      }
    }

    if (pCfgData->UseAs.Epl.NumberMappedObjects > SDAI_EPL_MAX_NUMBER_MAPPED_OBJECTS)
    {
      return (SDAI_ERR_INVALID_ARGUMENT);
    }
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  pUnit = interface_get_free_unit (InputSize, OutputSize, &UnitIndex);

  if (pUnit != NULL)
  {
    _ASSERT ((pUnit->DataLengthInput == InputSize) && (pUnit->DataLengthOutput == OutputSize));
    _ASSERT (((pUnit->DataInputOffset != 0xFFFF) && (InputSize > 0)) ||
             ((pUnit->DataOutputOffset != 0xFFFF) && (OutputSize > 0)));

    pServiceData->ServiceType = SERVICE_REQ_RES_PLUG_UNIT;

    /* copy the Powerlink specific configuration
     */
    if (pCfgData != NULL)
    {
      memcpy
      (
        &pServiceData->UseAs.PlugUnit.CfgData.UseAs.Epl,
        &pCfgData->UseAs.Epl,
        sizeof (pServiceData->UseAs.PlugUnit.CfgData.UseAs.Epl)
      );
    }
    else
    {
      /* set default values
       */
      pServiceData->UseAs.PlugUnit.CfgData.UseAs.Epl.NodeId              = SDAI_EPL_NODE_ID_MN_RX_UNICAST;
      pServiceData->UseAs.PlugUnit.CfgData.UseAs.Epl.MappingVersion      = SDAI_EPL_MAPPING_VERSION_NOT_AVAILABLE;
      pServiceData->UseAs.PlugUnit.CfgData.UseAs.Epl.NumberMappedObjects = 0;

      memset (pServiceData->UseAs.PlugUnit.CfgData.UseAs.Epl.ObjectDescr, 0, sizeof (pServiceData->UseAs.PlugUnit.CfgData.UseAs.Epl.ObjectDescr));
    }

    pServiceData->UseAs.PlugUnit.UnitIndex = UnitIndex;

    if (UnitType == SDAI_UNIT_TYPE_GENERIC_OUTPUT)
    {
      PdoIndex = pSdaiBackendDriverEPL->RxPdoIndex;
      pSdaiBackendDriverEPL->RxPdoIndex  += 1u;
      pSdaiBackendDriverEPL->RxPdoNumber += 1u;
    }
    else
    {
      PdoIndex = pSdaiBackendDriverEPL->TxPdoIndex;
      pSdaiBackendDriverEPL->TxPdoIndex  += 1u;
      pSdaiBackendDriverEPL->TxPdoNumber += 1u;
    }

    pUnit->ProtocolExtention = (U32) 0; /* not needed */
    pUnit->Id                = _SDAI_EPL_UNIT_INDEX_AND_PDO_INDEX_TO_ID (UnitIndex, PdoIndex);
    pUnit->Type              = UnitType;
    pUnit->InternalFlags     = (pCfgData->InterfaceType & SDAI_INTERFACE_MASK) | UNIT_INTERNAL_FLAG_IN_USE;

    pSdaiBackendDriverEPL->CurrentSlot += 1u;
    *pId                                = pUnit->Id;

    /* interface_lock_unit is already called by interface_get_free_unit */
    interface_unlock_unit (UnitIndex);

    /* Send the Powerlink specific configuration to the stack */
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

    /* Wait until service data are taken over by the stack */
    do
    {
      pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

      if (pServiceData == NULL)
      {
        interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
        return (SDAI_ERR_INTERNAL);
      }

      ServiceType = (volatile U16) pServiceData->ServiceType;
      interface_unlock_data (SERVICE_REQ_RES_INTERFACE);

    } while (ServiceType != SERVICE_REQ_RES_FREE);

    return (SDAI_SUCCESS);
  }
  else
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  }

  return (SDAI_ERR_MAX_UNIT_REACHED);
}

/*===========================================================================*/

/**
 * The function sdai_drv_epl_get_data() gets the current output or input data
 * for the unit defined by Id. This function can be used on input and output units.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS | SDAI_ERR_NOSUCHUNIT
 * @param[in] Id
 * - type : U32
 * - range: ???
 * @param[out] pStatus
 * - type : U8*
 * - range: whole address space
 * @param[out] pData
 * - type : U8*
 * - range: whole address space
 */
static U8 sdai_drv_epl_get_data (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U32 Id, U8* pStatus, U8* pData)
{
  T_SDAI_DRV_EPL*     pSdaiBackendDriverEPL;
  T_INTERFACE_UNIT*   pUnit;
  U16                 UnitIndex;


  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverEPL = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverEPL->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  UnitIndex = _SDAI_EPL_ID_TO_UNIT_INDEX (U16, Id);
  pUnit     = interface_lock_unit (UnitIndex);

  if (pUnit == NULL)
  {
    interface_unlock_unit (UnitIndex);
    *pStatus = SDAI_DATA_STATUS_INVALID;
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (Id != pUnit->Id)
  {
    interface_unlock_unit (UnitIndex);
    *pStatus = SDAI_DATA_STATUS_INVALID;
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (pUnit->DataLengthOutput > 0u)
  {
    *pStatus = pUnit->StatusOutput;

    _ASSERT (pUnit->DataOutputOffset != 0xFFFF);
    memcpy (pData, &pSdaiBackendDriverEPL->pIODataBase [pUnit->DataOutputOffset], pUnit->DataLengthOutput);
  }
  else if (pUnit->DataLengthInput > 0u)
  {
    *pStatus = pUnit->StatusInput;

    _ASSERT (pUnit->DataInputOffset != 0xFFFF);
    memcpy (pData, &pSdaiBackendDriverEPL->pIODataBase [pUnit->DataInputOffset], pUnit->DataLengthInput);
  }
  else
  {
    /* Unit has no outputs or inputs configured.
     */
    *pStatus = SDAI_DATA_STATUS_INVALID;
  }

  interface_unlock_unit (UnitIndex);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_epl_set_data() sets the input data for the unit defined by
 * Id. This function can only be used on input units.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS | SDAI_ERR_NOSUCHUNIT
 * @param[in] Id
 * - type : U32
 * - range: ???
 * @param[in] Status
 * - type : U8
 * - range: ???
 * @param[in] pData
 * - type : U8*
 * - range: whole address space
 */
static U8 sdai_drv_epl_set_data (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U32 Id, U8 Status, const U8* pData)
{
  T_SDAI_DRV_EPL*     pSdaiBackendDriverEPL;
  T_INTERFACE_UNIT*   pUnit;
  U16                 UnitIndex;


  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverEPL = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverEPL->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  if (pData == NULL)
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  /*-------------------------------------------------------------------------*/

  UnitIndex = _SDAI_EPL_ID_TO_UNIT_INDEX (U16, Id);
  pUnit     = interface_lock_unit (UnitIndex);

  if (pUnit == NULL)
  {
    interface_unlock_unit (UnitIndex);
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (Id != pUnit->Id)
  {
    interface_unlock_unit (UnitIndex);
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (pUnit->InternalFlags & SDAI_INTERFACE_MASK)
  {
    interface_unlock_unit (UnitIndex);
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (pUnit->DataLengthInput > 0u)
  {
    pUnit->StatusInput = (Status == SDAI_DATA_STATUS_VALID) ? SDAI_DATA_STATUS_VALID : SDAI_DATA_STATUS_INVALID;

    _ASSERT (pUnit->DataInputOffset != 0xFFFF);
    memcpy (&pSdaiBackendDriverEPL->pIODataBase [pUnit->DataInputOffset], pData, pUnit->DataLengthInput);
  }

  interface_unlock_unit (UnitIndex);

  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_UPDATE_INPUT_DATA);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_epl_write_response() handles the Powerlink specific
 * part of the write response. It checks the parameters, copies the data to the
 * shared ram and sends a signal to the stack.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY |
 *           SDAI_ERR_SERVICE_PENDING |
 *           SDAI_ERROR
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pWrite
 * - type : const struct SDAI_WRITE_RES*
 * - range: whole address space
 * @remarks
 */
static U8 sdai_drv_epl_write_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_WRITE_RES* pWrite)
{
  T_SDAI_DRV_EPL*                 pSdaiBackendDriverEPL;

  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  struct SDAI_WRITE_RES*          pServiceWrite;


  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pWrite);

  pSdaiBackendDriverEPL = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverEPL->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  pServiceWrite = &pServiceData->UseAs.Write;

  pServiceData->ServiceType = SERVICE_REQ_RES_WRITE;
  pServiceWrite->ErrorCode  = pWrite->ErrorCode;

  memcpy (&pServiceWrite->UseAs.Epl, &pWrite->UseAs.Epl, sizeof (pServiceWrite->UseAs.Epl));

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_epl_read_response() handles the Powerlink specific
 * part of the read response. It checks the parameters, copies the data to the
 * shared ram and sends a signal to the stack.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY |
 *           SDAI_ERR_SERVICE_PENDING |
 *           SDAI_ERROR
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pRead
 * - type : const struct SDAI_READ_RES*
 * - range: whole address space
 */
static U8 sdai_drv_epl_read_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_READ_RES* pRead)
{
  T_SDAI_DRV_EPL*                 pSdaiBackendDriverEPL;

  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  struct SDAI_READ_RES*           pServiceRead;


  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pRead);

  pSdaiBackendDriverEPL = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverEPL->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  pServiceRead = &pServiceData->UseAs.Read;

  pServiceData->ServiceType = SERVICE_REQ_RES_READ;
  pServiceRead->ErrorCode   = pRead->ErrorCode;
  pServiceRead->Length      = pRead->Length;

  memcpy (&pServiceRead->UseAs.Epl, &pRead->UseAs.Epl, sizeof (pServiceRead->UseAs.Epl));
  memcpy ((pServiceRead + 1), (pRead + 1), pRead->Length);

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_epl_control_response() handles the EtherNet/IP specific
 * part of the control response. It checks the parameters, copies the data to the
 * shared ram and sends a signal to the stack.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY |
 *           SDAI_ERR_SERVICE_PENDING |
 *           SDAI_ERROR
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pControl
 * - type : const struct SDAI_CONTROL_RES*
 * - range: whole address space
 */
static U8 sdai_drv_epl_control_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_CONTROL_RES* pControl)
{
  T_SDAI_DRV_EPL*                 pSdaiBackendDriverEPL;

  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  struct SDAI_CONTROL_RES*        pServiceControl;


  _ASSERT_PTR (pControl);
  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverEPL = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverEPL->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  pServiceControl = &pServiceData->UseAs.Control;

  pServiceData->ServiceType    = SERVICE_REQ_RES_CONTROL;
  pServiceControl->ControlCode = pControl->ControlCode;
  pServiceControl->ErrorCode   = pControl->ErrorCode;
  pServiceControl->Length      = pControl->Length;

  memcpy ((pServiceControl + 1), (pControl + 1), pControl->Length);

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_epl_start() starts the Powerlink backend. This function is called
 * when the applications signals "plugging complete".
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS | SDAI_ERR_COMMAND_PENDING
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_drv_epl_proto_start (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;


  _ASSERT_PTR (pSdaiBackendDriver);

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  pServiceData->ServiceType = SERVICE_REQ_RES_INIT;

  memcpy
  (
    &pServiceData->UseAs.Init.Device,
    &pSdaiBackendDriver->ApplicationInitData.Info,
    sizeof (pServiceData->UseAs.Init.Device)
  );

  memcpy
  (
    &pServiceData->UseAs.Init.Ident,
    &pSdaiBackendDriver->ApplicationInitData.Ident,
    sizeof (pServiceData->UseAs.Init.Ident)
  );

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_START_STACK);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_epl_plugging_complete() returns the plugging state of
 * modules.
 * @return
 * - type  : int
 * - values: 0, 1
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static int sdai_drv_epl_plugging_complete (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_SDAI_DRV_EPL*      pSdaiBackendDriverEPL;


  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverEPL = pSdaiBackendDriver->pPrivateData;

  return (pSdaiBackendDriverEPL->PlugComplete);
}


/*===========================================================================*/

/**
 * @desc The function sdai_drv_epl_lock_resources()
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS | #SDAI_ERR_INTERNAL
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] Resources Specifies which resources should be unlocked
 * - type : U16
 * - range: #SDAI_SHARED_RESOURCE_FLASH |
 *          #SDAI_SHARED_RESOURCE_MDIO
 */
static U8 sdai_drv_epl_lock_resources (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U16 Resource)
{
  _TRACE (("sdai_drv_epl_lock_resources"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);

  if (Resource == SDAI_SHARED_RESOURCE_FLASH)
  {
    if (interface_lock_data (RESOURCE_INTERFACE) != NULL)
    {
      return (SDAI_SUCCESS);
    }
  }

  return (SDAI_ERR_INTERNAL);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_epl_unlock_resources()
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] Resources Specifies which resources should be unlocked
 * - type : U16
 * - range: #SDAI_SHARED_RESOURCE_FLASH |
 *          #SDAI_SHARED_RESOURCE_MDIO
 */
static U8 sdai_drv_epl_unlock_resources (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U16 Resource)
{
  _TRACE (("sdai_drv_epl_unlock_resources"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);

  if (Resource == SDAI_SHARED_RESOURCE_FLASH)
  {
    interface_unlock_data (RESOURCE_INTERFACE);
  }

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_epl_handle_stack_signal() processes an interrupt event from the stack CPU.
 * The respective callback function of the SDAI application will be invoked.
 * @return
 * - type  : int
 * - values: 0
 * @param[in] Signal
 * - type : U16 int
 * - range: INTERFACE_SIGNAL_SERVICE_IND | INTERFACE_SIGNAL_DATA_IND
 * @param[in] pContext
 * - type : VOID*
 * - range: whole address space
 */
static int sdai_drv_epl_handle_stack_signal (U16 Signal, void* pContext)
{
  T_SDAI_BACKEND_DRIVER*    pSdaiBackendDriver = pContext;


  if (Signal & INTERFACE_SIGNAL_SERVICE_IND)
  {
    sdai_drv_epl_handle_service_ind (pSdaiBackendDriver);
  }

  if (Signal & INTERFACE_SIGNAL_DATA_IND)
  {
    sdai_drv_epl_handle_data_ind (pSdaiBackendDriver);
  }

#ifdef SDAI_INCLUDE_SOCKET
  if (Signal & INTERFACE_SIGNAL_SOCKET_IND)
  {
    sdai_drv_sock_handle_socket_ind (Signal);
  }
#endif

  return 0;
}

/*===========================================================================*/

/**
 * The function sdai_drv_epl_handle_synch_signal() handles the SYNCH interrupt signal.
 * The respective callback of the SDAI application will be invoked.
 * the application part of the SDAI.
 * @return
 * - type  : int
 * - values: 0
 * @param[in] Signal
 * - type : U16 int
 * - range: INTERFACE_SIGNAL_SERVICE_IND
 * @param[in] pContext
 * - type : VOID*
 * - range: whole address space
 */
static int sdai_drv_epl_handle_synch_signal (U8 Signal, void* pContext)
{
  T_SDAI_BACKEND_DRIVER*    pSdaiBackendDriver = pContext;


  _AVOID_UNUSED_WARNING (Signal);

  if (pSdaiBackendDriver->ApplicationInitData.Callback.SyncSignalCbk != NULL)
  {
    pSdaiBackendDriver->ApplicationInitData.Callback.SyncSignalCbk (&SyncSignalBuffer);
  }

  return 0;
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_epl_handle_data_ind() handels the incoming
 *       meassages in the data indication channel (Unit interface).
 * @return
 * - type  : VOID
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static void sdai_drv_epl_handle_data_ind (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_INTERFACE_UNIT*   pUnit;
  U16                 UnitIndex;
  U16                 NumberChangedUnits = 0;
  T_SDAI_DRV_EPL*     pSdaiBackendDriverEPL;


  _TRACE (("sdai_drv_epl_handle_data_ind"));

  pSdaiBackendDriverEPL = pSdaiBackendDriver->pPrivateData;

  pUnit = interface_lock_unit (0);

  for (UnitIndex = 0; UnitIndex < pSdaiBackendDriverEPL->CurrentSlot; UnitIndex++)
  {
    if (pUnit->InternalFlags & UNIT_INTERNAL_FLAG_OUTPUT_CHANGED)
    {
      pUnit->InternalFlags &= ~UNIT_INTERNAL_FLAG_OUTPUT_CHANGED;

      OutputChangedInd.Id [NumberChangedUnits] = pUnit->Id;
      NumberChangedUnits++;
    }

    pUnit++;
  }

  interface_unlock_unit (0);

  if (NumberChangedUnits > 0)
  {
    OutputChangedInd.NumberUnits = NumberChangedUnits;

    if (pSdaiBackendDriver->ApplicationInitData.Callback.DataCbk != NULL)
    {
      pSdaiBackendDriver->ApplicationInitData.Callback.DataCbk (&OutputChangedInd);
    }
  }

  return;
}

/*===========================================================================*/

/**
 * The function sdai_drv_epl_handle_service_ind() handels the messages between the stack and
 * the application part of the SDAI.
 * @return
 * - type  : VOID
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static void sdai_drv_epl_handle_service_ind (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_INTERFACE_SERVICE_IND*    pServiceData;
  T_SDAI_DRV_EPL*             pSdaiBackendDriverEPL;

  pServiceData = interface_lock_data (SERVICE_IND_INTERFACE);

  pSdaiBackendDriverEPL = pSdaiBackendDriver->pPrivateData;

  if (pServiceData != NULL)
  {
    switch (pServiceData->ServiceType)
    {
      case SERVICE_IND_EXCEPTION:
      {
        memcpy (&ServiceIndBuffer.UseAs.Exception, &pServiceData->UseAs.Exception, sizeof (ServiceIndBuffer.UseAs.Exception));

        pServiceData->ServiceType = SERVICE_IND_FREE;
        interface_unlock_data (SERVICE_IND_INTERFACE);

        if (pSdaiBackendDriver->ApplicationInitData.Callback.ExceptionCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.ExceptionCbk (&ServiceIndBuffer.UseAs.Exception);
        }

        pSdaiBackendDriverEPL->ExceptionPending = 1;
        break;
      }

      case SERVICE_IND_WRITE:
      {
        memcpy (&ServiceIndBuffer.UseAs.Write, &pServiceData->UseAs.Write, sizeof (ServiceIndBuffer.UseAs.Write));

        if (ServiceIndBuffer.UseAs.Write.Length > 0)
        {
          _ASSERT (ServiceIndBuffer.UseAs.Write.Length <= (SERVICE_SIZE - sizeof (ServiceIndBuffer.UseAs.Write)));
          memcpy (&ServiceIndBuffer.UseAs.Write + 1, &pServiceData->UseAs.Write + 1, ServiceIndBuffer.UseAs.Write.Length);
        }

        pServiceData->ServiceType = SERVICE_IND_FREE;
        interface_unlock_data (SERVICE_IND_INTERFACE);

        if (pSdaiBackendDriver->ApplicationInitData.Callback.WriteReqCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.WriteReqCbk (&ServiceIndBuffer.UseAs.Write);
        }

        break;
      }

      case SERVICE_IND_READ:
      {
        memcpy (&ServiceIndBuffer.UseAs.Read, &pServiceData->UseAs.Read, sizeof (ServiceIndBuffer.UseAs.Read));

        pServiceData->ServiceType = SERVICE_IND_FREE;
        interface_unlock_data (SERVICE_IND_INTERFACE);

        if (pSdaiBackendDriver->ApplicationInitData.Callback.ReadReqCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.ReadReqCbk (&ServiceIndBuffer.UseAs.Read);
        }

        break;
      }

      case SERVICE_IND_IDENT:
      {
        memcpy (&ServiceIndBuffer.UseAs.Ident, &pServiceData->UseAs.Ident, sizeof (ServiceIndBuffer.UseAs.Ident));

        pServiceData->ServiceType = SERVICE_IND_FREE;
        interface_unlock_data (SERVICE_IND_INTERFACE);

        if (pSdaiBackendDriver->ApplicationInitData.Callback.IdentDataCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.IdentDataCbk (&ServiceIndBuffer.UseAs.Ident);
        }

        break;
      }

      case SERVICE_IND_CONTROL:
      {
        memcpy (&ServiceIndBuffer.UseAs.Control, &pServiceData->UseAs.Control, sizeof (ServiceIndBuffer.UseAs.Control));

        if (ServiceIndBuffer.UseAs.Control.Length > 0)
        {
          _ASSERT (ServiceIndBuffer.UseAs.Control.Length <= (SERVICE_SIZE - sizeof (ServiceIndBuffer.UseAs.Control)));
          memcpy (&ServiceIndBuffer.UseAs.Control + 1, &pServiceData->UseAs.Control + 1, ServiceIndBuffer.UseAs.Control.Length);
        }

        pServiceData->ServiceType = SERVICE_IND_FREE;
        interface_unlock_data (SERVICE_IND_INTERFACE);

        if(pSdaiBackendDriver->ApplicationInitData.Callback.ControlReqCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.ControlReqCbk (&ServiceIndBuffer.UseAs.Control);
        }

        break;
      }

      default:
      {
        pServiceData->ServiceType = SERVICE_IND_FREE;
        interface_unlock_data (SERVICE_IND_INTERFACE);
        break;
      }
    }
  }

  return;
}

#endif  /* SDAI_INCLUDE_EPL */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
