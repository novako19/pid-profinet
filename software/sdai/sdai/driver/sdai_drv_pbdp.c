/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

#include <string.h>

#include "sdai.h"
#include "sdai_drv.h"

#include "cfg_cc.h"
#include "sdai_interface.h"

#include "cfg_cc.h"
#include "porting.h"

#ifdef SDAI_INCLUDE_PB_DP

/******************************************************************************
DEFINES
******************************************************************************/

#define PROFIBUS_MAX_TELEGRAM_LENGTH    244u

/******************************************************************************
TYPEDEFS
******************************************************************************/

typedef struct _T_SDAI_DRV_PBDP
{
  struct SDAI_INIT    ApplicationInitData;

  struct SDAI_UNIT*   pUnit [SDAI_MAX_UNITS];

  int                 ShutdownPending;
  int                 ExceptionPending;
  U8                  AlarmPending;

  U16                 HighestUnitIndex;             /**< The highest used unit index */
  U16                 CurrentSlot;
  U8*                 pIODataBase;
  U8                  RemainingOutputSize;
  U8                  RemainingInputSize;

  int                 PlugComplete;

  void*               pPrivateData;

} T_SDAI_DRV_PBDP;

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

static U8     sdai_drv_pbdp_deinit                      (T_SDAI_BACKEND_DRIVER*);
static U8     sdai_drv_pbdp_get_version                 (T_SDAI_BACKEND_DRIVER*, struct SDAI_VERSION_DATA*);
static U8     sdai_drv_pbdp_get_state                   (T_SDAI_BACKEND_DRIVER*, U16*, struct SDAI_ADDSTATUS_DATA*);
static U8     sdai_drv_pbdp_plug_unit                   (T_SDAI_BACKEND_DRIVER*, U8, U8, U8, U32*, const struct SDAI_CFG_DATA_EXT*);

static U8     sdai_drv_pbdp_get_data                    (T_SDAI_BACKEND_DRIVER*, U32, U8*, U8*);
static U8     sdai_drv_pbdp_set_data                    (T_SDAI_BACKEND_DRIVER*, U32, U8, const U8*);

static U8     sdai_drv_pbdp_write_response              (T_SDAI_BACKEND_DRIVER*, const struct SDAI_WRITE_RES*);
static U8     sdai_drv_pbdp_read_response               (T_SDAI_BACKEND_DRIVER*, const struct SDAI_READ_RES*);
static U8     sdai_drv_pbdp_control_response            (T_SDAI_BACKEND_DRIVER*, const struct SDAI_CONTROL_RES*);

static U8     sdai_drv_pbdp_proto_start                 (T_SDAI_BACKEND_DRIVER*);
static int    sdai_drv_pbdp_plugging_complete           (T_SDAI_BACKEND_DRIVER*);

static U8     sdai_drv_pbdp_diagnosis_request           (T_SDAI_BACKEND_DRIVER*, const struct SDAI_DIAGNOSIS_REQ*);

static U8     sdai_drv_pbdp_init_watchdog               (T_SDAI_BACKEND_DRIVER*, const U16);
static U8     sdai_drv_pbdp_check_and_trigger_watchdog  (T_SDAI_BACKEND_DRIVER*);

static int    sdai_drv_pbdp_handle_stack_signal         (U16, void*);
static void   sdai_drv_pbdp_handle_service_ind          (T_SDAI_BACKEND_DRIVER*);
static void   sdai_drv_pbdp_handle_data_ind             (T_SDAI_BACKEND_DRIVER*);

/******************************************************************************
PUBLIC DATA
******************************************************************************/

/******************************************************************************
PRIVATE DATA
******************************************************************************/

static T_SDAI_DRV_PBDP                  SdaiBackendDriverPBDP;
static struct SDAI_OUTPUT_CHANGED_IND   OutputChangedInd;           /**< Holds informations of changed units for data cbk */
static T_INTERFACE_SERVICE_IND          ServiceIndBuffer;           /**< temporary buffer for service indications */

/******************************************************************************
PUBLIC FUNCTIONS
******************************************************************************/

/******************************************************************************
PROTECTED FUNCTIONS
******************************************************************************/

/**
 * The function sdai_pbdp_init() initializes the PROFIBUS backend.
 * @return
 * - type  : struct SDAI_BACKEND_DRV*
 * - values: whole address space
 * @param[in] InitData
 * - type : SDAI_INIT*
 * - range: whole address space
 */
U8 sdai_drv_pbdp_init (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, struct SDAI_INIT* pApplicationInitData)
{
  T_INTERFACE_MANAGEMENT_DATA*    pManagementData;
  U8                              Result = SDAI_ERR_INTERNAL;


  _TRACE (("sdai_drv_pbdp_init"));

  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pApplicationInitData);

  if (pApplicationInitData->Ident.UseAs.PbDp.SlaveAddress > SDAI_PBDP_DEFAULT_SLAVE_ADDRESS)
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  if ( (pApplicationInitData->Ident.UseAs.PbDp.Flags & SDAI_PBDP_SET_SLAVE_ADDR_SUPPORTED) &&
       (pApplicationInitData->Callback.IdentDataCbk == NULL                              ) )
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  if (pApplicationInitData->Ident.UseAs.PbDp.Flags & SDAI_PBDP_SET_SLAVE_ADDR_SUPPORTED)
  {
    /* Set-Slave-Address is not yet supported */
    return (SDAI_ERR_COM_NOT_SUPPORTED);
  }

  if ( (pApplicationInitData->Ident.UseAs.PbDp.Baudrate >  SDAI_PBDP_MBAUD_12   ) &&
       (pApplicationInitData->Ident.UseAs.PbDp.Baudrate != SDAI_PBDP_KBAUD_45_45) &&
       (pApplicationInitData->Ident.UseAs.PbDp.Baudrate != SDAI_PBDP_AUTOBAUD   ) )
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  if (pApplicationInitData->Info.UseAs.PbDp.DeviceDiag.Length > SDAI_PBDP_MAX_DEVICE_DIAG_LENGTH)
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  /* @brief The interface of the driver is initialized with default function. So
   * we have to overwrite only functions we support in the profinet implementation.
   */
  pSdaiBackendDriver->Interface.deinit                 = sdai_drv_pbdp_deinit;
  pSdaiBackendDriver->Interface.get_version            = sdai_drv_pbdp_get_version;
  pSdaiBackendDriver->Interface.get_state              = sdai_drv_pbdp_get_state;
  pSdaiBackendDriver->Interface.plug_unit              = sdai_drv_pbdp_plug_unit;

  pSdaiBackendDriver->Interface.get_data               = sdai_drv_pbdp_get_data;
  pSdaiBackendDriver->Interface.set_data               = sdai_drv_pbdp_set_data;

  pSdaiBackendDriver->Interface.write_response         = sdai_drv_pbdp_write_response;
  pSdaiBackendDriver->Interface.read_response          = sdai_drv_pbdp_read_response;
  pSdaiBackendDriver->Interface.control_response       = sdai_drv_pbdp_control_response;

  pSdaiBackendDriver->Interface.proto_start            = sdai_drv_pbdp_proto_start;
  pSdaiBackendDriver->Interface.plugging_complete      = sdai_drv_pbdp_plugging_complete;

  pSdaiBackendDriver->Interface.diagnosis_request      = sdai_drv_pbdp_diagnosis_request;

  pSdaiBackendDriver->Interface.init_watchdog          = sdai_drv_pbdp_init_watchdog;

  pSdaiBackendDriver->pPrivateData                     = &SdaiBackendDriverPBDP;

  /*-------------------------------------------------------------------------*/

  memset (&ServiceIndBuffer, 0, sizeof (ServiceIndBuffer));

  memcpy (&SdaiBackendDriverPBDP.ApplicationInitData, pApplicationInitData, sizeof (SdaiBackendDriverPBDP.ApplicationInitData));

  SdaiBackendDriverPBDP.AlarmPending        = 0u;
  SdaiBackendDriverPBDP.CurrentSlot         = 0u;
  SdaiBackendDriverPBDP.PlugComplete        = 0;
  SdaiBackendDriverPBDP.HighestUnitIndex    = 0;
  SdaiBackendDriverPBDP.ShutdownPending     = 0u;
  SdaiBackendDriverPBDP.RemainingOutputSize = PROFIBUS_MAX_TELEGRAM_LENGTH;
  SdaiBackendDriverPBDP.RemainingInputSize  = PROFIBUS_MAX_TELEGRAM_LENGTH;

  Result = interface_init
           (
             sdai_drv_pbdp_handle_stack_signal,
             NULL,
             INTERFACE_APPLICATION,
             pSdaiBackendDriver,
             SDAI_BACKEND_PB_DP
           );

  if (Result == SDAI_SUCCESS)
  {
    SdaiBackendDriverPBDP.pIODataBase = interface_get_iodata_base ();

    pManagementData = interface_lock_data (MANAGEMENT_INTERFACE);

    if (pManagementData != NULL)
    {
      pManagementData->SupportedCallbacks = 0u;

      if (pApplicationInitData->Callback.DataCbk != NULL)      { pManagementData->SupportedCallbacks |= DATA_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.IdentDataCbk != NULL) { pManagementData->SupportedCallbacks |= IDENT_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.AlarmAckCbk != NULL)  { pManagementData->SupportedCallbacks |= ALARM_ACK_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.WriteReqCbk != NULL)  { pManagementData->SupportedCallbacks |= WRITE_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.ReadReqCbk != NULL)   { pManagementData->SupportedCallbacks |= READ_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.ControlReqCbk != NULL){ pManagementData->SupportedCallbacks |= CONTROL_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.ExceptionCbk != NULL) { pManagementData->SupportedCallbacks |= EXCEPTION_CALLBACK_SUPPORTED; }
    }

    interface_unlock_data (MANAGEMENT_INTERFACE);
  }

  return (Result);
}

/******************************************************************************
PRIVATE FUNCTIONS
******************************************************************************/

/**
 * The function sdai_drv_pbdp_deinit() shuts down the Profibus stack and
 * frees the data structure.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS
 */
static U8 sdai_drv_pbdp_deinit (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_SDAI_DRV_PBDP*    pSdaiBackendDriverPbDp;


  pSdaiBackendDriverPbDp = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPbDp->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pSdaiBackendDriverPbDp->ShutdownPending = 1;

  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_STOP_STACK);

  /* wait for backend shutting down */
  interface_deinit (INTERFACE_APPLICATION);

  pSdaiBackendDriver->pPrivateData = NULL;

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_pbdp_get_version() returns the version string of the
 * Profibus stack.
 * @return
 * - type  : char*
 * - values: ???
 */
static U8 sdai_drv_pbdp_get_version (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, struct SDAI_VERSION_DATA* pVersionData)
{
  T_SDAI_DRV_PBDP*             pSdaiBackendDriverPbDp;
  T_INTERFACE_STATUS_DATA*     pStatusData;


  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pVersionData);

  pSdaiBackendDriverPbDp = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPbDp->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  pStatusData = interface_lock_data (STATUS_INTERFACE);
  memcpy (pVersionData, &pStatusData->VersionData, sizeof(struct SDAI_VERSION_DATA));
  interface_unlock_data (STATUS_INTERFACE);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_pbdp_get_state() returns the current state of the PROFIBUS
 * protocoll stack.
 * @return
 * - type  : U16
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY
 * @pre [enter precondition here]
 * @post [enter postcondition here]
 * @remarks [enter remarks, i.e. compatibility issues, here]
 */
static U8 sdai_drv_pbdp_get_state (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U16* pState, struct SDAI_ADDSTATUS_DATA* pAddStatus)
{
  T_SDAI_DRV_PBDP*             pSdaiBackendDriverPbDp;
  T_INTERFACE_STATUS_DATA*     pStatusData;


  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pState);

  pSdaiBackendDriverPbDp = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPbDp->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pStatusData = interface_lock_data (STATUS_INTERFACE);

  *pState = pStatusData->StackStatus;

  if (pAddStatus != NULL)
  {
    memcpy (pAddStatus, &pStatusData->StackAddStatus, sizeof (struct SDAI_ADDSTATUS_DATA));
  }

  interface_unlock_data (STATUS_INTERFACE);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_pbdp_plug_unit() makes the PROFIBUS specific part of plugging.
 * It calculates the ID for the unit and returns it to the application.
 * @return
 * - type  : U32
 * - values: ???
 * @param[in] UnitType
 * - type : U8
 * - range: SDAI_UNIT_TYPE_INPUT | SDAI_UNIT_TYPE_OUTPUT
 * @param[in] Size
 * - type : U8
 * - range: ???
 * @param[out] pUnit
 * - type : SDAI_UNIT *
 * - range: whole address space
 */
static U8 sdai_drv_pbdp_plug_unit (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U8 UnitType, U8 InputSize, U8 OutputSize, U32* pId, const struct SDAI_CFG_DATA_EXT* pCfgData)
{
  T_SDAI_DRV_PBDP*                pSdaiBackendDriverPbDp;
  T_INTERFACE_UNIT*               pUnit;
  U16                             UnitIndex = 0xFFFF;
  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;

  volatile U16                    ServiceType;


  _TRACE (("sdai_drv_pbdp_plug_unit"));

  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverPbDp = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPbDp->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  /* if user signals plugging complete we don't want to check the args */
  if (UnitType == SDAI_UNIT_TYPE_PLUGGING_COMPLETE)
  {
    pSdaiBackendDriverPbDp->PlugComplete = 1;

    return (pSdaiBackendDriver->Interface.proto_start (pSdaiBackendDriver));
  }

  /*-------------------------------------------------------------------------*/

  if (pSdaiBackendDriver->ApplicationInitData.Info.Flags & SDAI_DYNAMIC_IO_CONFIGURATION)
  {
    if (*pId != SDAI_INVALID_ID)
    {
      UnitIndex = interface_find_unit (*pId, pSdaiBackendDriverPbDp->HighestUnitIndex);

      if (UnitIndex == 0xFFFF)
      {
        return (SDAI_ERR_NOSUCHUNIT);
      }
    }
    else
    {
      UnitIndex = interface_find_free_unit (SDAI_MAX_UNITS);

      if (UnitIndex == 0xFFFF)
      {
        return (SDAI_ERR_MAX_UNIT_REACHED);
      }
    }
  }
  else
  {
    *pId = SDAI_INVALID_ID;

    if (pSdaiBackendDriverPbDp->PlugComplete)
    {
      return (SDAI_ERR_RUNTIME_PLUG);
    }
  }

  /*-------------------------------------------------------------------------*/

  switch (UnitType)
  {
    case SDAI_UNIT_TYPE_GENERIC_INPUT      : OutputSize = 0;                break;
    case SDAI_UNIT_TYPE_GENERIC_OUTPUT     : InputSize  = 0;                break;
    case SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT:                                break;
    case SDAI_PBDP_UNIT_TYPE_EMPTY_MODULE  : InputSize = 0; OutputSize = 0; break;

    default: return (SDAI_ERR_UNKNOWN_UNIT_TYPE);
  }

  if ( (OutputSize > SDAI_PBDP_MAX_UNIT_IO_DATA_SIZE) ||
       (InputSize > SDAI_PBDP_MAX_UNIT_IO_DATA_SIZE ) )
  {
    return (SDAI_ERR_INVALID_SIZE);
  }

  if (pCfgData != NULL)
  {
    if (pCfgData->UseAs.PbDp.Length > SDAI_PBDP_MAX_MANUFACTURER_SPEC_CFG_DATA_LENGTH)
    {
      return (SDAI_ERR_INVALID_ARGUMENT);
    }
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  pServiceData->ServiceType = SERVICE_REQ_RES_PLUG_UNIT;

  pUnit = interface_get_free_unit (InputSize, OutputSize, &UnitIndex);

  if (pUnit != NULL)
  {
    _ASSERT ((pUnit->DataLengthInput == InputSize) && (pUnit->DataLengthOutput == OutputSize));
    _ASSERT (((pUnit->DataInputOffset != 0xFFFF) && (InputSize > 0)) ||
             ((pUnit->DataOutputOffset != 0xFFFF) && (OutputSize > 0)) ||
             ((pUnit->DataInputOffset == 0xFFFF) && (InputSize == 0)) ||
             ((pUnit->DataOutputOffset == 0xFFFF) && (OutputSize == 0)));

    /* copy the PROFIBUS DP specific configuration
     */
    if (pCfgData != NULL)
    {
      memcpy
      (
        &pServiceData->UseAs.PlugUnit.CfgData.UseAs.PbDp,
        &pCfgData->UseAs.PbDp,
        sizeof (pServiceData->UseAs.PlugUnit.CfgData.UseAs.PbDp)
      );
    }
    else
    {
      /* set default values */
      pServiceData->UseAs.PlugUnit.CfgData.UseAs.PbDp.InputFlags  = SDAI_PBDP_CFG_FLAG_MODULE_CONSISTENCY;
      pServiceData->UseAs.PlugUnit.CfgData.UseAs.PbDp.OutputFlags = SDAI_PBDP_CFG_FLAG_MODULE_CONSISTENCY;
      pServiceData->UseAs.PlugUnit.CfgData.UseAs.PbDp.Length      = 0x00u;
    }

    pServiceData->UseAs.PlugUnit.UnitIndex = UnitIndex;

    if (*pId == SDAI_INVALID_ID)
    {
      pUnit->Id = pSdaiBackendDriverPbDp->CurrentSlot; /* Id is equivalent to the PB slot */
      pSdaiBackendDriverPbDp->CurrentSlot += 1u;
      *pId = pUnit->Id;
    }

    pUnit->Type              = UnitType;
    pUnit->ProtocolExtention = (U32) 0; /* not needed */
    pUnit->InternalFlags     = (pCfgData->InterfaceType & SDAI_INTERFACE_MASK) | UNIT_INTERNAL_FLAG_IN_USE;

    pSdaiBackendDriverPbDp->RemainingOutputSize -= pUnit->DataLengthOutput;
    pSdaiBackendDriverPbDp->RemainingInputSize  -= pUnit->DataLengthInput;
    pSdaiBackendDriverPbDp->HighestUnitIndex     = _MAX (pSdaiBackendDriverPbDp->HighestUnitIndex, UnitIndex);

    /* interface_lock_unit is already called by interface_get_free_unit */
    interface_unlock_unit (_SDAI_PBDP_ID_TO_INDEX (U16, pUnit->Id));

    /* Send the EtherCAT specific configuration to the stack */
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

    /* Wait until service data are taken over by the stack */
    do
    {
      pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

      if (pServiceData == NULL)
      {
        interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
        return (SDAI_ERR_INTERNAL);
      }

      ServiceType = (volatile U16) pServiceData->ServiceType;
      interface_unlock_data (SERVICE_REQ_RES_INTERFACE);

    } while (ServiceType != SERVICE_REQ_RES_FREE);

    return (SDAI_SUCCESS);
  }
  else
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  }

  return (SDAI_ERR_MAX_UNIT_REACHED);
}

/*===========================================================================*/

/**
 * The function sdai_pbdp_get_data() gets the current output data for the unit
 * defined by Id. This function can also be used to read back data set for a pure input unit.
 * For combined input/output units only the output data can be read.
 *
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS | SDAI_ERR_NOSUCHUNIT
 * @param[in] Id
 * - type : U32
 * - range: ???
 * @param[out] pStatus
 * - type : U8*
 * - range: whole address space
 * @param[out] pData
 * - type : U8*
 * - range: whole address space
 * @remarks when we are a device, the input data are the data which are send to us by
 * the controler.
 */
static U8 sdai_drv_pbdp_get_data (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U32 Id, U8* pStatus, U8* pData)
{
  T_SDAI_DRV_PBDP*    pSdaiBackendDriverPbDp;
  T_INTERFACE_UNIT*   pUnit;
  U16                 UnitIndex;


  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverPbDp = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPbDp->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  UnitIndex = _SDAI_PBDP_ID_TO_INDEX (U16, Id);
  pUnit     = interface_lock_unit (UnitIndex);

  if (pUnit == NULL)
  {
    interface_unlock_unit (UnitIndex);
    *pStatus = SDAI_DATA_STATUS_INVALID;
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (Id != pUnit->Id)
  {
    interface_unlock_unit (UnitIndex);
    *pStatus = SDAI_DATA_STATUS_INVALID;
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (pUnit->DataLengthOutput > 0u)
  {
    *pStatus = pUnit->StatusOutput;

    _ASSERT (pUnit->DataOutputOffset != 0xFFFF);
    memcpy (pData, &pSdaiBackendDriverPbDp->pIODataBase [pUnit->DataOutputOffset], pUnit->DataLengthOutput);
  }
  else if (pUnit->DataLengthInput > 0u)
  {
    *pStatus = pUnit->StatusInput;

    _ASSERT (pUnit->DataInputOffset != 0xFFFF);
    memcpy (pData, &pSdaiBackendDriverPbDp->pIODataBase [pUnit->DataInputOffset], pUnit->DataLengthInput);
  }
  else
  {
    /* Unit has no outputs or inputs configured.
     */
    *pStatus = SDAI_DATA_STATUS_INVALID;
  }

  interface_unlock_unit (UnitIndex);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_pbdp_set_data() sets the output data for the unit defined by
 * Id. This function can only be used on input units.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS | SDAI_ERR_NOSUCHUNIT
 * @param[in] Id
 * - type : U32
 * - range: ???
 * @param[in] Status
 * - type : U8
 * - range: ???
 * @param[in] pData
 * - type : U8*
 * - range: whole address space
 * @remarks when we are a device, the output data are the data which we send to
 * the controler.
 */
static U8 sdai_drv_pbdp_set_data (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U32 Id, U8 Status, const U8* pData)
{
  T_SDAI_DRV_PBDP*    pSdaiBackendDriverPbDp;
  T_INTERFACE_UNIT*   pUnit;
  U16                 UnitIndex;

  BOOL                InputChanged = FALSE;
  BOOL                StatusChanged = FALSE;


  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverPbDp = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPbDp->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  if (pData == NULL)
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  /*-------------------------------------------------------------------------*/

  UnitIndex = _SDAI_PBDP_ID_TO_INDEX (U16, Id);
  pUnit     = interface_lock_unit (UnitIndex);

  if (pUnit == NULL)
  {
    interface_unlock_unit (UnitIndex);
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (Id != pUnit->Id)
  {
    interface_unlock_unit (UnitIndex);
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (pUnit->InternalFlags & SDAI_INTERFACE_MASK)
  {
    interface_unlock_unit (UnitIndex);
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (pUnit->DataLengthInput > 0u)
  {
    _ASSERT (pUnit->DataInputOffset != 0xFFFF);

    InputChanged = (BOOL) memcmp (&pSdaiBackendDriverPbDp->pIODataBase [pUnit->DataInputOffset], pData, pUnit->DataLengthInput);

    if (InputChanged)
    {
      memcpy (&pSdaiBackendDriverPbDp->pIODataBase [pUnit->DataInputOffset], pData, pUnit->DataLengthInput);
    }

    if ( ((pUnit->StatusInput == SDAI_DATA_STATUS_VALID)   && (Status != SDAI_DATA_STATUS_VALID  )) ||
         ((pUnit->StatusInput == SDAI_DATA_STATUS_INVALID) && (Status != SDAI_DATA_STATUS_INVALID)) )
    {
      pUnit->StatusInput = (Status == SDAI_DATA_STATUS_VALID) ? SDAI_DATA_STATUS_VALID : SDAI_DATA_STATUS_INVALID;
      StatusChanged      = TRUE;
    }
  }

  interface_unlock_unit (UnitIndex);

  if ( (InputChanged) || (StatusChanged) )
  {
    interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_UPDATE_INPUT_DATA);
  }

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS | SDAI_ERR_NOSUCHUNIT
 * @param[in] Id
 * - type : U32
 * - range: ???
 * @param[in] Status
 * - type : U8
 * - range: ???
 * @param[in] pData
 * - type : U8*
 * - range: whole address space
 * @remarks
 */
static U8 sdai_drv_pbdp_write_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_WRITE_RES* pWrite)
{
  T_SDAI_DRV_PBDP*                pSdaiBackendDriverPbDp;

  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  struct SDAI_WRITE_RES*          pServiceWrite;


  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pWrite);

  pSdaiBackendDriverPbDp = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPbDp->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  pServiceWrite = &pServiceData->UseAs.Write;

  pServiceData->ServiceType = SERVICE_REQ_RES_WRITE;
  pServiceWrite->ErrorCode  = pWrite->ErrorCode;

  memcpy (&pServiceWrite->UseAs.PbDp, &pWrite->UseAs.PbDp, sizeof (pServiceWrite->UseAs.PbDp));

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS | SDAI_ERR_NOSUCHUNIT
 * @param[in] Id
 * - type : U32
 * - range: ???
 * @param[in] Status
 * - type : U8
 * - range: ???
 * @param[in] pData
 * - type : U8*
 * - range: whole address space
 * @remarks
 */
static U8 sdai_drv_pbdp_read_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_READ_RES* pRead)
{
  T_SDAI_DRV_PBDP*                pSdaiBackendDriverPbDp;

  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  struct SDAI_READ_RES*           pServiceRead;


  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pRead);

  pSdaiBackendDriverPbDp = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPbDp->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  pServiceRead = &pServiceData->UseAs.Read;

  pServiceData->ServiceType = SERVICE_REQ_RES_READ;
  pServiceRead->ErrorCode   = pRead->ErrorCode;
  pServiceRead->Length      = pRead->Length;

  memcpy (&pServiceRead->UseAs.PbDp, &pRead->UseAs.PbDp, sizeof (pServiceRead->UseAs.PbDp));
  memcpy ((pServiceRead + 1), (pRead + 1), pRead->Length);

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_pbdp_control_response() handles the PROFIBUS DP specific
 * part of the control response. It checks the parameters, copies the data to the
 * shared ram and sends a signal to the stack.
 *
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY |
 *           SDAI_ERR_SERVICE_PENDING |
 *           SDAI_ERROR
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pControl
 * - type : const struct SDAI_CONTROL_RES*
 * - range: whole address space
 */
static U8 sdai_drv_pbdp_control_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_CONTROL_RES* pControl)
{
  T_SDAI_DRV_PBDP*                pSdaiBackendDriverPbDp;

  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  struct SDAI_CONTROL_RES*        pServiceControl;


  _ASSERT_PTR (pControl);
  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverPbDp = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPbDp->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  if ( (pControl->ControlCode != SDAI_CONTROL_CODE_CFG_DATA_INFO) &&
       (pControl->ControlCode != SDAI_PBDP_CONTROL_CODE_PRM_DATA) &&
       (pControl->ControlCode != SDAI_PBDP_CONTROL_CODE_INITIATE) &&
       (pControl->ControlCode != SDAI_PBDP_CONTROL_CODE_ABORT   ) )
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  pServiceControl = &pServiceData->UseAs.Control;

  pServiceData->ServiceType           = SERVICE_REQ_RES_CONTROL;
  pServiceControl->UseAs.PbDp.CommRef = pControl->UseAs.PbDp.CommRef;
  pServiceControl->UseAs.PbDp.Slot    = pControl->UseAs.PbDp.Slot;
  pServiceControl->UseAs.PbDp.Index   = pControl->UseAs.PbDp.Index;
  pServiceControl->ControlCode        = pControl->ControlCode;
  pServiceControl->ErrorCode          = pControl->ErrorCode;
  pServiceControl->Length             = 0;

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_pbdp_start() starts the PROFIBUS stack. This function is called
 * when the application signals "plugging complete".
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS              |
 *           #SDAI_ERR_INTERNAL         |
 *           #SDAI_ERR_SERVICE_PENDING  |
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_drv_pbdp_proto_start (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_SDAI_DRV_PBDP*                pSdaiBackendDriverPbDp;
  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;


  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverPbDp = pSdaiBackendDriver->pPrivateData;
  pServiceData           = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  _ASSERT_PTR (pSdaiBackendDriverPbDp);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  pServiceData->ServiceType = SERVICE_REQ_RES_INIT;

  memcpy
  (
    &pServiceData->UseAs.Init.Device,
    &pSdaiBackendDriverPbDp->ApplicationInitData.Info,
    sizeof (pServiceData->UseAs.Init.Device)
  );

  memcpy
  (
    &pServiceData->UseAs.Init.Ident,
    &pSdaiBackendDriverPbDp->ApplicationInitData.Ident,
    sizeof (pServiceData->UseAs.Init.Ident)
  );

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_START_STACK);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_pbdp_plugging_complete() returns the plugging state of
 * modules.
 *
 * @return
 * - type  : int
 * - values: 0, 1
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static int sdai_drv_pbdp_plugging_complete (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_SDAI_DRV_PBDP*      pSdaiBackendDriverPbDp;


  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverPbDp = pSdaiBackendDriver->pPrivateData;

  return (pSdaiBackendDriverPbDp->PlugComplete);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_pbdp_diagnosis_request() sends a diagnosis request service to the PROFIBUS stack.
 *       This function waits for the completion of the service request and returns the result to the application.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS              |
 *           #SDAI_ERR_STACK_NOT_RDY    |
 *           #SDAI_ERR_NOSUCHUNIT       |
 *           #SDAI_ERR_INVALID_ARGUMENT |
 *           #SDAI_ERR_INVALID_SIZE     |
 *           #SDAI_ERR_INTERNAL         |
 *           #SDAI_ERR_SERVICE_PENDING  |
 *           #SDAI_ERR_WRONG_STATE      |
 *           #SDAI_ERR_MAX_DIAG_REACHED
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pDiagnosisReq Pointer to a structure containing the diagnosis message
 * - type : SDAI_DIAGNOSIS_REQ*
 * - range: whole address space
 */
static U8 sdai_drv_pbdp_diagnosis_request (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_DIAGNOSIS_REQ* pDiagnosisReq)
{
  T_SDAI_DRV_PBDP*                    pSdaiBackendDriverPbDp;

  struct SDAI_PBDP_DIAGNOSIS_DATA*    pPbDiagnosisData;
  T_INTERFACE_SERVICE_REQ_RES*        pServiceData;
  struct SDAI_DIAGNOSIS_REQ*          pServiceDiagnosis;

  U16                                 UnitIndex;

  volatile U16                        ServiceType;
  volatile U8                         ServiceResult;
  U8                                  SdaiResult;


  _TRACE (("sdai_drv_pbdp_diagnosis_request"));

  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pDiagnosisReq);

  pSdaiBackendDriverPbDp = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverPbDp->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  pPbDiagnosisData = (struct SDAI_PBDP_DIAGNOSIS_DATA*) (pDiagnosisReq + 1);

  /*--- check service parameters --------------------------------------------*/

  if (pPbDiagnosisData->DiagType != SDAI_PBDP_DIAG_TYPE_DEVICE_RELATED)
  {
    UnitIndex = interface_find_unit (pDiagnosisReq->Id, pSdaiBackendDriverPbDp->HighestUnitIndex);

    if (UnitIndex == 0xFFFF)
    {
      return (SDAI_ERR_NOSUCHUNIT);
    }
  }

  if ( ((pDiagnosisReq->Type < SDAI_DIAGNOSIS_TYPE_SHORT_CIRCUIT) || (pDiagnosisReq->Type > SDAI_DIAGNOSIS_TYPE_MONITORING)) &&
       (! (pDiagnosisReq->Type & SDAI_DIAGNOSIS_TYPE_PROTOCOL_SPECIFIC)                                                    ) )
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  if ((pDiagnosisReq->Severity < SDAI_DIAGNOSIS_SEVERITY_MAINTENANCE_REQUIRED) ||
      (pDiagnosisReq->Severity > SDAI_DIAGNOSIS_SEVERITY_FAILURE             ) )
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  if ((pDiagnosisReq->StatusSpecifier != SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS   ) &&
      (pDiagnosisReq->StatusSpecifier != SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_DISAPPEARS) )
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  if ( (pDiagnosisReq->Length > 0) && (pDiagnosisReq->Length <= (SERVICE_SIZE - sizeof (struct SDAI_DIAGNOSIS_REQ))) )
  {
    if ( (pPbDiagnosisData->DiagType != SDAI_PBDP_DIAG_TYPE_CHANNEL_RELATED) &&
         (pPbDiagnosisData->DiagType != SDAI_PBDP_DIAG_TYPE_DEVICE_RELATED ) &&
         (pPbDiagnosisData->DiagType != SDAI_PBDP_DIAG_TYPE_DIAG_ALARM     ) &&
         (pPbDiagnosisData->DiagType != SDAI_PBDP_DIAG_TYPE_STATUS         ) )
    {
      return (SDAI_ERR_INVALID_ARGUMENT);
    }
  }
  else
  {
    return (SDAI_ERR_INVALID_SIZE);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  pServiceDiagnosis = &pServiceData->UseAs.Diagnosis;

  pServiceData->ServiceType          = SERVICE_REQ_RES_DIAGNOSIS;
  pServiceData->ServiceResult        = SDAI_SUCCESS;

  pServiceDiagnosis->Id              = pDiagnosisReq->Id;
  pServiceDiagnosis->Type            = pDiagnosisReq->Type;
  pServiceDiagnosis->Severity        = pDiagnosisReq->Severity;
  pServiceDiagnosis->StatusSpecifier = pDiagnosisReq->StatusSpecifier;
  pServiceDiagnosis->Length          = pDiagnosisReq->Length;

  if (pServiceDiagnosis->Length > 0)
  {
    U8* pDiagnosisData        = (U8*) (pDiagnosisReq + 1);
    U8* pServiceDiagnosisData = (U8*) (pServiceDiagnosis + 1);

    memcpy (pServiceDiagnosisData, pDiagnosisData, pServiceDiagnosis->Length);
  }

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  /* Wait until service data are taken over by the stack */
  do
  {
    pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

    if (pServiceData == NULL)
    {
      interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
      return (SDAI_ERR_INTERNAL);
    }

    ServiceType   = (volatile U16) pServiceData->ServiceType;
    ServiceResult = (volatile U8)  pServiceData->ServiceResult;

    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);

  } while (ServiceType != SERVICE_REQ_RES_FREE);

  switch (ServiceResult)
  {
    case SERVICE_REQ_RES_RESULT_OK                      : SdaiResult = SDAI_SUCCESS;              break;
    case SERVICE_REQ_RES_RESULT_ERR_WRONG_STATE         : SdaiResult = SDAI_ERR_WRONG_STATE;      break;
    case SERVICE_REQ_RES_RESULT_ERR_INVALID_PARAMETER   : SdaiResult = SDAI_ERR_INVALID_ARGUMENT; break;
    case SERVICE_REQ_RES_RESULT_ERR_NO_PARALLEL_SERVICES: SdaiResult = SDAI_ERR_MAX_DIAG_REACHED; break;
    default                                             : SdaiResult = SDAI_ERR_INTERNAL;         break;
  }

  return (SdaiResult);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_pbdp_init_watchdog()
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_drv_pbdp_init_watchdog (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const U16 WatchdogFactor)
{
  T_INTERFACE_MANAGEMENT_DATA*    pManagementData;
  U8                              Result = SDAI_ERR_COM_NOT_SUPPORTED;

  _TRACE (("sdai_drv_pbdp_init_watchdog"));

  _ASSERT_PTR (pSdaiBackendDriver);

  if (check_watchdog_supported () == SDAI_SUCCESS)
  {
    pManagementData = interface_lock_data (MANAGEMENT_INTERFACE);

    pManagementData->WatchdogFactor          = (WatchdogFactor * 100);
    pManagementData->WatchdogRunning         = 0;
    pManagementData->WatchdogFlagStack       = 0;
    pManagementData->WatchdogFlagApplication = 0;

    /* install the real trigger function */
    pSdaiBackendDriver->Interface.check_trigger_watchdog = sdai_drv_pbdp_check_and_trigger_watchdog;

    interface_unlock_data (MANAGEMENT_INTERFACE);
    interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_INIT_WATCHDOG);

    Result = SDAI_SUCCESS;
  }

  return (Result);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_pbdp_check_and_trigger_watchdog()
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS | #SDAI_ERR_WATCHDOG_EXPIRED
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_drv_pbdp_check_and_trigger_watchdog (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_INTERFACE_MANAGEMENT_DATA*    pManagementData;
  U8                              Result = SDAI_SUCCESS;


  _TRACE (("sdai_drv_pbdp_check_and_trigger_watchdog"));

  _ASSERT_PTR (pSdaiBackendDriver);
  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);

  pManagementData = interface_lock_data (MANAGEMENT_INTERFACE);

  /* Trigger WD */
  pManagementData->WatchdogFlagApplication = 0;

  if (pManagementData->WatchdogRunning == 1)
  {
    /* Check WD */
    pManagementData->WatchdogFlagStack += 1;

    if (pManagementData->WatchdogFlagStack > 2)
    {
      handle_stack_watchdog_expired ();
      Result = SDAI_ERR_WATCHDOG_EXPIRED;
    }
  }
  else
  {
    pManagementData->WatchdogRunning = 1;
  }

  interface_unlock_data (MANAGEMENT_INTERFACE);

  return (Result);
}

/*===========================================================================*/

/**
 * The function sdai_pbdp_int_callback() handles the messages between the stack and
 * the application part of the SDAI.
 * @return
 * - type  : int
 * - values: 0
 * @param[in] Signal
 * - type : U16 int
 * - range: INTERFACE_SIGNAL_SERVICE_IND
 * @param[in] pContext
 * - type : VOID*
 * - range: whole address space
 */
static int sdai_drv_pbdp_handle_stack_signal (U16 Signal, void* pContext)
{
  T_SDAI_BACKEND_DRIVER*    pSdaiBackendDriver = pContext;


  if (Signal & INTERFACE_SIGNAL_SERVICE_IND)
  {
    sdai_drv_pbdp_handle_service_ind (pSdaiBackendDriver);
  }

  if (Signal & INTERFACE_SIGNAL_DATA_IND)
  {
    sdai_drv_pbdp_handle_data_ind (pSdaiBackendDriver);
  }

  return 0;
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_pbdp_handle_data_ind() handels the incoming
 *       messages in the data indication channel (Unit interface).
 * @return
 * - type  : VOID
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static void sdai_drv_pbdp_handle_data_ind (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_SDAI_DRV_PBDP*    pSdaiBackendDriverPbDp;
  T_INTERFACE_UNIT*   pUnit;
  U16                 UnitIndex;
  U16                 NumberChangedUnits = 0;


  _TRACE (("sdai_drv_pbdp_handle_data_ind"));

  pSdaiBackendDriverPbDp = pSdaiBackendDriver->pPrivateData;

  pUnit = interface_lock_unit (0);

  for (UnitIndex = 0; UnitIndex < pSdaiBackendDriverPbDp->CurrentSlot; UnitIndex++)
  {
    if (pUnit->InternalFlags & UNIT_INTERNAL_FLAG_OUTPUT_CHANGED)
    {
      pUnit->InternalFlags &= ~UNIT_INTERNAL_FLAG_OUTPUT_CHANGED;

      OutputChangedInd.Id [NumberChangedUnits] = pUnit->Id;
      NumberChangedUnits++;
    }

    pUnit++;
  }

  interface_unlock_unit (0);

  if (NumberChangedUnits > 0)
  {
    OutputChangedInd.NumberUnits = NumberChangedUnits;

    if (pSdaiBackendDriverPbDp->ApplicationInitData.Callback.DataCbk != NULL)
    {
      pSdaiBackendDriverPbDp->ApplicationInitData.Callback.DataCbk (&OutputChangedInd);
    }
  }

  return;
}

/*===========================================================================*/

/**
 * The function sdai_drv_pbdp_handle_service_ind() handles the messages between the stack and
 * the application part of the SDAI.
 *
 * @return
 * - type : VOID
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static void sdai_drv_pbdp_handle_service_ind (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_INTERFACE_SERVICE_IND*    pServiceData;
  T_SDAI_DRV_PBDP*            pSdaiBackendDriverPbDp;


  pServiceData = interface_lock_data (SERVICE_IND_INTERFACE);

  pSdaiBackendDriverPbDp = pSdaiBackendDriver->pPrivateData;

  if (pServiceData != NULL)
  {
    switch (pServiceData->ServiceType)
    {
      case SERVICE_IND_EXCEPTION:
      {
        memcpy (&ServiceIndBuffer.UseAs.Exception, &pServiceData->UseAs.Exception, sizeof (ServiceIndBuffer.UseAs.Exception));

        pServiceData->ServiceType = SERVICE_IND_FREE;
        interface_unlock_data (SERVICE_IND_INTERFACE);

        if (pSdaiBackendDriver->ApplicationInitData.Callback.ExceptionCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.ExceptionCbk (&ServiceIndBuffer.UseAs.Exception);
        }

        pSdaiBackendDriverPbDp->ExceptionPending = 1;
        break;
      }

      case SERVICE_IND_WRITE:
      {
        memcpy (&ServiceIndBuffer.UseAs.Write, &pServiceData->UseAs.Write, sizeof (ServiceIndBuffer.UseAs.Write));

        if (ServiceIndBuffer.UseAs.Write.Length > 0)
        {
          _ASSERT (ServiceIndBuffer.UseAs.Write.Length <= (SERVICE_SIZE - sizeof (ServiceIndBuffer.UseAs.Write)));
          memcpy (&ServiceIndBuffer.UseAs.Write + 1, &pServiceData->UseAs.Write + 1, ServiceIndBuffer.UseAs.Write.Length);
        }

        pServiceData->ServiceType = SERVICE_IND_FREE;
        interface_unlock_data (SERVICE_IND_INTERFACE);

        if (pSdaiBackendDriver->ApplicationInitData.Callback.WriteReqCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.WriteReqCbk (&ServiceIndBuffer.UseAs.Write);
        }

        break;
      }

      case SERVICE_IND_READ:
      {
        memcpy (&ServiceIndBuffer.UseAs.Read, &pServiceData->UseAs.Read, sizeof (ServiceIndBuffer.UseAs.Read));

        pServiceData->ServiceType = SERVICE_IND_FREE;
        interface_unlock_data (SERVICE_IND_INTERFACE);

        if (pSdaiBackendDriver->ApplicationInitData.Callback.ReadReqCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.ReadReqCbk (&ServiceIndBuffer.UseAs.Read);
        }

        break;
      }

      case SERVICE_IND_IDENT:
      {
        break;
      }

      case SERVICE_IND_ALARM_ACK:
      {
        break;
      }

      case SERVICE_IND_CONTROL:
      {
        memcpy (&ServiceIndBuffer.UseAs.Control, &pServiceData->UseAs.Control, sizeof (ServiceIndBuffer.UseAs.Control));

        if (ServiceIndBuffer.UseAs.Control.Length > 0)
        {
          _ASSERT (ServiceIndBuffer.UseAs.Control.Length <= (SERVICE_SIZE - sizeof (ServiceIndBuffer.UseAs.Control)));
          memcpy (&ServiceIndBuffer.UseAs.Control + 1, &pServiceData->UseAs.Control + 1, ServiceIndBuffer.UseAs.Control.Length);
        }

        pServiceData->ServiceType = SERVICE_IND_FREE;
        interface_unlock_data (SERVICE_IND_INTERFACE);

        if (pSdaiBackendDriver->ApplicationInitData.Callback.ControlReqCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.ControlReqCbk (&ServiceIndBuffer.UseAs.Control);
        }

        break;
      }

      default:
      {
        pServiceData->ServiceType = SERVICE_IND_FREE;
        interface_unlock_data (SERVICE_IND_INTERFACE);
        break;
      }
    }
  }

  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_CHANNEL_FREE);
  return;
}

#endif /* SDAI_INCLUDE_PB_DP */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
