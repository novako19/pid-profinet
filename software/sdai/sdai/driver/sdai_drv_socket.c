/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

#include <stdio.h>
#include <string.h>

#ifdef _DEBUG
#include <assert.h>
#endif


#include "sdai.h"

#include "cfg_cc.h"

#include "sdai_socket.h"

#ifndef DOXYEXTERN
#include "sdai.h"
#include "sdai_ext.h"

#include "sdai_drv.h"

#include "sdai_interface.h"
#include "socket_interface.h"

#include "porting.h"

#ifdef SDAI_INCLUDE_SOCKET

/******************************************************************************
DEFINES
******************************************************************************/

#define LOCAL_IP_ADDRESS_START              0x7F000000uL  /**< Local IP start address in host-byte-order (little-endian) */
#define LOCAL_IP_ADDRESS_END                0x7FFFFFFFuL  /**< Local IP end address in host-byte-order (little-endian) */
#define MULTICAST_IP_ADDRESS_START          0xE0000000uL  /**< Multicast IP address start address in host-byte-order (little-endian) */
#define MULTICAST_IP_ADDRESS_END            0xEFFFFFFFuL  /**< Multicast IP address start address in host-byte-order (little-endian) */
#define BROADCAST_IP_ADDRESS                0xFFFFFFFFuL  /**< Broadcast IP address */

/******************************************************************************
TYPEDEFS
******************************************************************************/

typedef struct _T_SOCKET_BUFFER
{
  U16   DataLength;
  U16   RemotePort;

  U32   RemoteAddress;

  U16   Offset;

  U8    Data [INTERNAL_SOCKET_FIFO_SIZE];

} T_SOCKET_BUFFER;

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

static T_SOCKET_RESULT  socket_ioctl_get_local_status         (unsigned short, T_SDAI_SOCK_IO_CONTROL*);
static T_SOCKET_RESULT  socket_ioctl_get_send_status          (unsigned short, T_SDAI_SOCK_IO_CONTROL*);
static T_SOCKET_RESULT  socket_ioctl_get_receive_status       (unsigned short, T_SDAI_SOCK_IO_CONTROL*);
static T_SOCKET_RESULT  socket_ioctl_close_connection         (unsigned short, T_SDAI_SOCK_IO_CONTROL*);
static T_SOCKET_RESULT  socket_ioctl_tcp_accept_filter        (unsigned short, T_SDAI_SOCK_IO_CONTROL*);
static T_SOCKET_RESULT  socket_ioctl_add_or_del_udp_multicast (unsigned short, T_SDAI_SOCK_IO_CONTROL*, BOOL);

static void             socket_copy_data_to_fifo              (T_SOCKET_INTERFACE*, unsigned short, T_SDAI_SOCK_ADDR*, const unsigned char*, U16, U16);
static void             socket_copy_data_from_fifo            (T_SOCKET_INTERFACE*, U16, U16, U32, U32);


/******************************************************************************
PUBLIC DATA
******************************************************************************/

/******************************************************************************
PRIVATE DATA
******************************************************************************/

static T_SDAI_SOCK_INIT         ApplicationInitData;

static T_SOCKET_BUFFER          SocketRcvBuffer [MAX_NUMBER_SUPPORTED_SOCKETS];
static T_BITSET64               SocketDataReceived;
static T_CONNECTION             Connection [MAX_NUMBER_SUPPORTED_SOCKETS] [MAX_NUMBER_CONNECTIONS_PER_SOCKET];

static T_SDAI_SOCK_IO_CONTROL   IOControl;

/******************************************************************************
SOCKET API FUNCTIONS
******************************************************************************/

T_SOCKET_RESULT sdai_drv_sock_init (T_SDAI_SOCK_INIT* pSocketInit)
{
  T_SOCKET_INTERFACE*   pSocketInterface;


  memcpy (&ApplicationInitData, pSocketInit, sizeof (ApplicationInitData));

  pSocketInterface = interface_lock_data (SOCKET_INTERFACE);

  if (pSocketInterface == NULL)
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_NOT_SUPPORTED);
  }

  memset (pSocketInterface, 0, sizeof (T_SOCKET_INTERFACE));

  pSocketInterface->Management.KeepAliveTimeout   = pSocketInit->ConfigOptions.Timeout;
  pSocketInterface->Management.SupportedCallbacks = 0;

  if (pSocketInit->Callback.StateChangedCbk != NULL)  { pSocketInterface->Management.SupportedCallbacks |= STATE_CHANGED_CALLBACK_SUPPORTED; }
  if (pSocketInit->Callback.DataReceivedCbk != NULL)  { pSocketInterface->Management.SupportedCallbacks |= DATA_RECEIVED_CALLBACK_SUPPORTED; }

  interface_unlock_data (SOCKET_INTERFACE);

  return (SOCK_SUCCESS);
}

/*===========================================================================*/

T_SOCKET_RESULT sdai_drv_sock_term (void)
{
  T_SOCKET_INTERFACE*   pSocketInterface;
  unsigned              Index;
  U32                   Flags;


  pSocketInterface = interface_lock_data (SOCKET_INTERFACE);

  if (pSocketInterface == NULL)
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_NOT_SUPPORTED);
  }

  pSocketInterface->Management.KeepAliveTimeout = 0;
  interface_unlock_data (SOCKET_INTERFACE);

  for (Index = 0u; Index < _NUMBER_ARRAY_ELEMENTS (pSocketInterface->Admin.Socket); Index++)
  {
    pSocketInterface = interface_lock_data (SOCKET_INTERFACE);
    _ASSERT (pSocketInterface != NULL);
    Flags = _CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->Admin.Socket [Index].Flags);
    interface_unlock_data (SOCKET_INTERFACE);

    if (Flags & SOCKET_FLAG_CREATED)
    {
      volatile T_SOCKET_RESULT   Result;


      if (sdai_drv_sock_close (Index) != SOCK_SUCCESS)
      {
        return (SOCK_ERR_FATAL_ERROR);
      }

      /* wait until the socket has been closed on stack side */
      do
      {
        IOControl.Command = SOCK_IOC_GET_LOCAL_STATUS;
        Result = socket_ioctl_get_local_status (Index, &IOControl);

      } while (Result != SOCK_ERR_SOCKET_NOT_CREATED);
    }
  }

  return (SOCK_SUCCESS);
}

/*===========================================================================*/

T_SOCKET_RESULT sdai_drv_sock_create
  (
    U32                 Flags,
    T_SDAI_SOCK_ADDR*   pRemoteAddress,
    T_SDAI_SOCK_ADDR*   pLocalAddress,
    unsigned short*     pSocket
  )
  /*-------------------------------------------------------------------------*/
{
  T_SOCKET_RESULT       Result = SOCK_SUCCESS;
  unsigned              Index;
  T_SOCKET_INTERFACE*   pSocketInterface;


  _TRACE (("sdai_drv_sock_create"));

  pSocketInterface = interface_lock_data (SOCKET_INTERFACE);

  if (pSocketInterface == NULL)
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_NOT_SUPPORTED);
  }

  for (Index = 0u; Index < _NUMBER_ARRAY_ELEMENTS (pSocketInterface->Admin.Socket); Index++)
  {
    if (_CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->Admin.Socket [Index].Flags) & SOCKET_FLAG_CREATED)
    {
      if ( (_CONVERT_U16_TO_APPL_BYTE_ORDERING (pSocketInterface->Admin.Socket [Index].LocalPort) != SOCK_PORT_ANY                                    ) &&
           (_CONVERT_U16_TO_APPL_BYTE_ORDERING (pSocketInterface->Admin.Socket [Index].LocalPort) == pLocalAddress->Port                              ) &&
           ((_CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->Admin.Socket [Index].Flags) & SOCK_FLAG_USER_MASK) == (Flags & SOCK_FLAG_USER_MASK)) )
      {
        interface_unlock_data (SOCKET_INTERFACE);
        return (SOCK_ERR_ADDR_IN_USE);
      }
    }
  }

  /*-----------------------------------------------------------------------*/

  for (Index = 0u; Index < _NUMBER_ARRAY_ELEMENTS (pSocketInterface->Admin.Socket); Index++)
  {
    if (! (_CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->Admin.Socket [Index].Flags) & SOCKET_FLAG_CREATED))
    {
      break;
    }
  }

  /*-----------------------------------------------------------------------*/

  if (Index < _NUMBER_ARRAY_ELEMENTS (pSocketInterface->Admin.Socket))
  {
    if (! (_CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketReq.EventReg) & APPL_EVENT_CREATE_SOCKET_REQ))
    {
      *pSocket = (unsigned short) Index;

      pSocketInterface->Admin.Socket [Index].Flags         = _CONVERT_U32_TO_STACK_BYTE_ORDERING (SOCKET_FLAG_CREATED | (Flags & SOCK_FLAG_USER_MASK));
      pSocketInterface->Admin.Socket [Index].LocalAddress  = _CONVERT_U32_TO_STACK_BYTE_ORDERING (pLocalAddress->IpAddress);
      pSocketInterface->Admin.Socket [Index].RemoteAddress = _CONVERT_U32_TO_STACK_BYTE_ORDERING (pRemoteAddress->IpAddress);

      pSocketInterface->Admin.Socket [Index].LocalPort     = _CONVERT_U16_TO_STACK_BYTE_ORDERING (pLocalAddress->Port);
      pSocketInterface->Admin.Socket [Index].RemotePort    = _CONVERT_U16_TO_STACK_BYTE_ORDERING (pRemoteAddress->Port);

      _BITSET_SET_BIT (pSocketInterface->SocketReq.CreateSocket, Index);
      interrupt_lock ();
      pSocketInterface->SocketReq.EventReg |= _CONVERT_U32_TO_STACK_BYTE_ORDERING (APPL_EVENT_CREATE_SOCKET_REQ);
      interrupt_unlock ();

      interface_unlock_data (SOCKET_INTERFACE);
      interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SOCKET_REQ);
    }
    else
    {
      /* socket create request is already pending */
      interface_unlock_data (SOCKET_INTERFACE);
      Result = SOCK_ERR_TEMPORARY_NOT_EXECUTABLE;
    }
  }
  else
  {
    interface_unlock_data (SOCKET_INTERFACE);
    Result = SOCK_ERR_OUT_OF_SOCKETS;
  }

  return (Result);
}

/*===========================================================================*/

T_SOCKET_RESULT sdai_drv_sock_close (unsigned short Socket)
{
  T_SOCKET_RESULT       Result = SOCK_SUCCESS;
  T_SOCKET_INTERFACE*   pSocketInterface;


  _TRACE (("sdai_drv_sock_close"));

  pSocketInterface = interface_lock_data (SOCKET_INTERFACE);

  if (pSocketInterface == NULL)
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_NOT_SUPPORTED);
  }

  if (Socket < _NUMBER_ARRAY_ELEMENTS (pSocketInterface->Admin.Socket))
  {
    if (_CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->Admin.Socket [Socket].Flags) & SOCKET_FLAG_CREATED)
    {
      if (! (_CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketReq.EventReg) & APPL_EVENT_CLOSE_SOCKET_REQ))
      {
        _BITSET_SET_BIT (pSocketInterface->SocketReq.CloseSocket, Socket);
        interrupt_lock ();
        pSocketInterface->SocketReq.EventReg |= _CONVERT_U32_TO_STACK_BYTE_ORDERING (APPL_EVENT_CLOSE_SOCKET_REQ);
        interrupt_unlock ();

        interface_unlock_data (SOCKET_INTERFACE);
        interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SOCKET_REQ);
      }
      else
      {
        /* socket create request is already pending */
        interface_unlock_data (SOCKET_INTERFACE);
        Result = SOCK_ERR_TEMPORARY_NOT_EXECUTABLE;
      }
    }
    else
    {
      interface_unlock_data (SOCKET_INTERFACE);
    }
  }
  else
  {
    interface_unlock_data (SOCKET_INTERFACE);
    Result = SOCK_ERR_INVALID_SOCKET_ID;
  }

  return (Result);
}

/*===========================================================================*/

T_SOCKET_RESULT sdai_drv_sock_send
  (
    unsigned short          Socket,
    T_SDAI_SOCK_ADDR*       pRemoteAddress,
    U16                     DataLength,
    const unsigned char*    pData
  )
  /*-------------------------------------------------------------------------*/
{
  T_SOCKET_INTERFACE*   pSocketInterface;

  U32                   Flags;
  U32                   PacketsRead;
  U32                   PacketsWritten;

  U16                   FifoReadOffset;
  U16                   FifoWriteOffset;
  U16                   FreeFifoSize;

  T_SOCKET_RESULT       Result;


  _TRACE (("sdai_drv_sock_send"));

  pSocketInterface = interface_lock_data (SOCKET_INTERFACE);

  if (pSocketInterface == NULL)
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_NOT_SUPPORTED);
  }

  if (Socket >= _NUMBER_ARRAY_ELEMENTS (pSocketInterface->Admin.Socket))
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_INVALID_SOCKET_ID);
  }

  Flags = _CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->Admin.Socket [Socket].Flags);

  if (! (Flags & SOCKET_FLAG_CREATED))
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_SOCKET_NOT_CREATED);
  }

  if ((Flags & SOCKET_FLAG_CONNECTION_STATUS_MASK) != SOCKET_FLAG_CONNECTION_STATUS_CONNECTED)
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_SOCKET_NOT_CONNECTED);
  }

  PacketsRead     = _CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketReq.PacketsRead);
  PacketsWritten  = _CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketReq.PacketsWritten);
  FifoReadOffset  = _CONVERT_U16_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketReq.FifoReadOffset);
  FifoWriteOffset = _CONVERT_U16_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketReq.FifoWriteOffset);
  FreeFifoSize    = (U16) ((FifoReadOffset > FifoWriteOffset) ? (FifoReadOffset - FifoWriteOffset) : ((INTERNAL_SOCKET_FIFO_SIZE - FifoWriteOffset) + FifoReadOffset));
  FreeFifoSize   -= (FreeFifoSize > sizeof (T_APPL_SOCKET_DATA_HEADER)) ? sizeof (T_APPL_SOCKET_DATA_HEADER) : FreeFifoSize;

  if (DataLength > (INTERNAL_SOCKET_FIFO_SIZE - sizeof (T_APPL_SOCKET_DATA_HEADER)))
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_INVALID_ARGUMENT);
  }

  if ( (FreeFifoSize < _ALIGN4 (U16, DataLength)) ||
       ((FifoReadOffset == FifoWriteOffset     ) &&
        (PacketsRead != PacketsWritten         )) )
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_SOCKET_BUSY);
  }

  if (! (_CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketReq.EventReg) & APPL_EVENT_SOCKET_SEND_REQ))
  {
    socket_copy_data_to_fifo (pSocketInterface, Socket, pRemoteAddress, pData, DataLength, FifoWriteOffset);
    interrupt_lock ();
    pSocketInterface->SocketReq.EventReg |= _CONVERT_U32_TO_STACK_BYTE_ORDERING (APPL_EVENT_SOCKET_SEND_REQ);
    interrupt_unlock ();

    interface_unlock_data (SOCKET_INTERFACE);
    interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SOCKET_REQ);

    Result = SOCK_SUCCESS;
  }
  else
  {
    /* socket create request is already pending */
    interface_unlock_data (SOCKET_INTERFACE);
    Result = SOCK_ERR_TEMPORARY_NOT_EXECUTABLE;
  }

  return (Result);
}

/*===========================================================================*/

T_SOCKET_RESULT sdai_drv_sock_receive
  (
    unsigned short      Socket,
    T_SDAI_SOCK_ADDR*   pRemoteAddress,
    U16*                pDataLength,
    unsigned char*      pData
  )
  /*-------------------------------------------------------------------------*/
{
  T_SOCKET_RESULT                 Result = SOCK_SUCCESS;
  T_SOCKET_INTERFACE*             pSocketInterface;
  T_SDAI_SOCK_DATA_RECEIVED_IND   DataReceived;
  U32                             Flags;
  U16                             FifoReadOffset;
  U16                             FifoWriteOffset;
  U32                             PacketsRead;
  U32                             PacketsWritten;


  _TRACE (("sdai_drv_sock_receive"));

  pSocketInterface = interface_lock_data (SOCKET_INTERFACE);

  if (pSocketInterface == NULL)
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_NOT_SUPPORTED);
  }

  Flags = _CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->Admin.Socket [Socket].Flags);

  if (! (Flags & SOCKET_FLAG_CREATED))
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_SOCKET_NOT_CREATED);
  }

  if (SocketRcvBuffer [Socket].DataLength == 0u)
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_NO_DATA_RECEIVED);
  }

  *pDataLength = _MIN (SocketRcvBuffer [Socket].DataLength, *pDataLength);

  pRemoteAddress->IpAddress = SocketRcvBuffer [Socket].RemoteAddress;
  pRemoteAddress->Port      = SocketRcvBuffer [Socket].RemotePort;

  disable_sdai_interrupt ();

  memcpy (pData, &SocketRcvBuffer [Socket].Data [SocketRcvBuffer [Socket].Offset], *pDataLength);

  SocketRcvBuffer [Socket].DataLength -= *pDataLength;
  SocketRcvBuffer [Socket].Offset      = (SocketRcvBuffer [Socket].DataLength > 0u) ? *pDataLength : 0u;

  FifoReadOffset  = _CONVERT_U16_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketInd.FifoReadOffset);
  FifoWriteOffset = _CONVERT_U16_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketInd.FifoWriteOffset);
  PacketsRead     = _CONVERT_U16_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketInd.PacketsRead);
  PacketsWritten  = _CONVERT_U16_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketInd.PacketsWritten);

  _BITSET_RESET_BIT (SocketDataReceived, Socket);

  memcpy (DataReceived.Socket, SocketDataReceived, sizeof (DataReceived.Socket));

  socket_copy_data_from_fifo (pSocketInterface, FifoReadOffset, FifoWriteOffset, PacketsRead, PacketsWritten);

  /* check if new data where received for a socket for which the application is not informed yet and call the
     received callback. We do this under interrupt lock to prevent possible recursion if the application calles
     the receive function directly from the callback. */
  if (memcmp (DataReceived.Socket, SocketDataReceived, sizeof (DataReceived.Socket)))
  {
    memcpy (DataReceived.Socket, SocketDataReceived, sizeof (DataReceived.Socket));

    if (ApplicationInitData.Callback.DataReceivedCbk != NULL)
    {
      ApplicationInitData.Callback.DataReceivedCbk (&DataReceived);
    }
  }

  enable_sdai_interrupt ();
  interface_unlock_data (SOCKET_INTERFACE);

  return (Result);
}

/*===========================================================================*/

T_SOCKET_RESULT sdai_drv_sock_ioctl
  (
    unsigned short            Socket,
    T_SDAI_SOCK_IO_CONTROL*   pIOControl
  )
  /*-------------------------------------------------------------------------*/
{
  T_SOCKET_RESULT       Result = SOCK_SUCCESS;


  _TRACE (("sdai_drv_sock_ioctl"));

  switch (pIOControl->Command)
  {
    case SOCK_IOC_GET_LOCAL_STATUS    : Result = socket_ioctl_get_local_status (Socket, pIOControl);                break;
    case SOCK_IOC_GET_SEND_STATUS     : Result = socket_ioctl_get_send_status (Socket, pIOControl);                 break;
    case SOCK_IOC_GET_RECEIVE_STATUS  : Result = socket_ioctl_get_receive_status (Socket, pIOControl);              break;
    case SOCK_IOC_CLOSE_TCP_CONNECTION: Result = socket_ioctl_close_connection (Socket, pIOControl);                break;
    case SOCK_IOC_TCP_ACCEPT_FILTER   : Result = socket_ioctl_tcp_accept_filter (Socket, pIOControl);               break;
    case SOCK_IOC_UDP_ADD_MULTICAST   : Result = socket_ioctl_add_or_del_udp_multicast (Socket, pIOControl, TRUE);  break;
    case SOCK_IOC_UDP_DEL_MULTICAST   : Result = socket_ioctl_add_or_del_udp_multicast (Socket, pIOControl, FALSE); break;

    default: Result = SOCK_ERR_INVALID_ARGUMENT; break;
  }

  return (Result);
}

/*===========================================================================*/

void sdai_drv_sock_handle_socket_ind (U16 Signal)
{
  T_SOCKET_INTERFACE*             pSocketInterface;
  T_SDAI_SOCK_DATA_RECEIVED_IND   DataReceived;
  BOOL                            SendSocketStateIndAck = FALSE;


  _TRACE (("sdai_drv_sock_handle_socket_ind"));

  if (Signal & INTERFACE_SIGNAL_SOCKET_IND)
  {
    U32   EventReg;


    pSocketInterface = interface_lock_data (SOCKET_INTERFACE);
    _ASSERT (pSocketInterface != NULL);
    EventReg = _CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketInd.EventReg);

    if (EventReg & APPL_EVENT_SOCKET_STATE_CHANGED_IND)
    {
      U16         Socket;
      unsigned    Index;


      Socket = _CONVERT_U16_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketInd.SocketState.Socket);
      _ASSERT (Socket < _NUMBER_ARRAY_ELEMENTS (Connection));

      for (Index = 0u; Index < _NUMBER_ARRAY_ELEMENTS (Connection [Socket]); Index++)
      {
        Connection [Socket] [Index].IpAddress = _CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketInd.SocketState.Connection [Index].IpAddress);
        Connection [Socket] [Index].Port      = _CONVERT_U16_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketInd.SocketState.Connection [Index].Port);
        Connection [Socket] [Index].Status    = _CONVERT_U16_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketInd.SocketState.Connection [Index].Status);
      }

      if (ApplicationInitData.Callback.StateChangedCbk != NULL)
      {
        ApplicationInitData.Callback.StateChangedCbk (Socket);
      }

      EventReg &= ~APPL_EVENT_SOCKET_STATE_CHANGED_IND;
      SendSocketStateIndAck = TRUE;
    }

    if (EventReg & APPL_EVENT_SOCKET_DATA_RECEIVED_IND)
    {
      U16   FifoReadOffset;
      U16   FifoWriteOffset;
      U32   PacketsRead;
      U32   PacketsWritten;

      FifoReadOffset  = _CONVERT_U16_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketInd.FifoReadOffset);
      FifoWriteOffset = _CONVERT_U16_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketInd.FifoWriteOffset);
      PacketsRead     = _CONVERT_U16_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketInd.PacketsRead);
      PacketsWritten  = _CONVERT_U16_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketInd.PacketsWritten);

      socket_copy_data_from_fifo (pSocketInterface, FifoReadOffset, FifoWriteOffset, PacketsRead, PacketsWritten);

      memcpy (DataReceived.Socket, SocketDataReceived, sizeof (DataReceived.Socket));

      if (ApplicationInitData.Callback.DataReceivedCbk != NULL)
      {
        ApplicationInitData.Callback.DataReceivedCbk (&DataReceived);
      }

      EventReg &= ~APPL_EVENT_SOCKET_DATA_RECEIVED_IND;
    }

    pSocketInterface->SocketInd.EventReg = 0;
    _ASSERT (! (EventReg & (~APPL_EVENT_SOCKET_IND_MASK)));

    if (SendSocketStateIndAck)
    {
      /* acknowledge socket status indication to allow the receiption of any further pending socket status indications */
      pSocketInterface->SocketReq.EventReg |= _CONVERT_U32_TO_STACK_BYTE_ORDERING (APPL_EVENT_SOCKET_STATE_CHANGED_IND_ACK);
    }

    interface_unlock_data (SOCKET_INTERFACE);

    if (SendSocketStateIndAck)
    {
      interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SOCKET_REQ);
    }
  }

  return;
}

/******************************************************************************
PRIVATE FUNCTIONS
******************************************************************************/

static T_SOCKET_RESULT socket_ioctl_get_local_status
  (
    unsigned short            Socket,
    T_SDAI_SOCK_IO_CONTROL*   pIOControl
  )
  /*-------------------------------------------------------------------------*/
{
  T_SOCKET_RESULT                             Result = SOCK_SUCCESS;
  T_SOCKET_INTERFACE*                         pSocketInterface;
  U32                                         Flags;
  T_SDAI_SOCK_TCP_CONNECTION_LOCAL_STATUS*    pConnection = &pIOControl->UseAs.GetLocalStatus.UseAs.Tcp.Connection [0];
  unsigned                                    Index;



  _TRACE (("socket_ioctl_get_local_status"));

  pSocketInterface = interface_lock_data (SOCKET_INTERFACE);

  if (pSocketInterface == NULL)
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_NOT_SUPPORTED);
  }

  Flags = _CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->Admin.Socket [Socket].Flags);
  interface_unlock_data (SOCKET_INTERFACE);

  if (! (Flags & SOCKET_FLAG_CREATED))
  {
    return (SOCK_ERR_SOCKET_NOT_CREATED);
  }

  switch (Flags & SOCKET_FLAG_CONNECTION_STATUS_MASK)
  {
    case SOCKET_FLAG_CONNECTION_STATUS_UNCONNECTED: pIOControl->UseAs.GetLocalStatus.Status = SOCK_LOCAL_STATUS_CLOSED;  break;
    case SOCKET_FLAG_CONNECTION_STATUS_CONNECTING : pIOControl->UseAs.GetLocalStatus.Status = SOCK_LOCAL_STATUS_OFFLINE; break;
    case SOCKET_FLAG_CONNECTION_STATUS_CONNECTED  :
    default                                       : pIOControl->UseAs.GetLocalStatus.Status = SOCK_LOCAL_STATUS_ONLINE;  break;
  }

  switch (Flags & SOCKET_FLAG_CONNECTION_ERROR_MASK)
  {
    case SOCKET_FLAG_CONNECTION_NO_ERROR            : pIOControl->UseAs.GetLocalStatus.StatusCode = SOCK_LOCAL_STATUS_CODE_NO_ERROR;       break;
    case SOCKET_FLAG_CONNECTION_ERROR_ADDRESS_IN_USE: pIOControl->UseAs.GetLocalStatus.StatusCode = SOCK_LOCAL_STATUS_CODE_ADDR_IN_USE;    break;
    case SOCKET_FLAG_CONNECTION_INTERNAL_ERROR      :
    default                                         : pIOControl->UseAs.GetLocalStatus.StatusCode = SOCK_LOCAL_STATUS_CODE_INTERNAL_ERROR; break;
  }

  pIOControl->UseAs.GetLocalStatus.UseAs.Tcp.NumberConnections = 0uL;

  for (Index = 0u; Index < _NUMBER_ARRAY_ELEMENTS (Connection [Socket]); Index++)
  {
    pConnection->Status               = SOCK_TCP_CONNECTION_STATUS_UNCONNECTED;
    pConnection->StatusCode           = SOCK_TCP_CONNECTION_STATUS_CODE_NO_ERROR;
    pConnection->RemoteAddr.IpAddress = SOCK_IP_ADDR_ANY;
    pConnection->RemoteAddr.Port      = SOCK_PORT_ANY;

    if (Connection [Socket] [Index].IpAddress != SOCK_IP_ADDR_ANY)
    {
      switch (Connection [Socket] [Index].Status)
      {
        case CONNECTION_STATUS_NO_ERROR: break;

        case CONNECTION_STATUS_CONNECTED:
        {
          pConnection->Status     = SOCK_TCP_CONNECTION_STATUS_CONNECTED;
          pConnection->StatusCode = SOCK_TCP_CONNECTION_STATUS_CODE_NO_ERROR;
          break;
        }

        case CONNECTION_STATUS_CLOSED_LOCALLY:
        {
          pConnection->Status     = SOCK_TCP_CONNECTION_STATUS_UNCONNECTED;
          pConnection->StatusCode = SOCK_TCP_CONNECTION_STATUS_CODE_NO_ERROR;
          break;
        }

        case CONNECTION_STATUS_CONNECTING:
        {
          pConnection->Status     = SOCK_TCP_CONNECTION_STATUS_CONNECTING;
          pConnection->StatusCode = SOCK_TCP_CONNECTION_STATUS_CODE_NO_ERROR;
          break;
        }

        case CONNECTION_STATUS_CLOSED_REMOTELY:
        {
          pConnection->Status     = SOCK_TCP_CONNECTION_STATUS_UNCONNECTED;
          pConnection->StatusCode = SOCK_TCP_CONNECTION_STATUS_CODE_CLOSED_REMOTELY;
          break;
        }

        case CONNECTION_STATUS_ABORTED_REMOTELY:
        {
          pConnection->Status     = SOCK_TCP_CONNECTION_STATUS_UNCONNECTED;
          pConnection->StatusCode = SOCK_TCP_CONNECTION_STATUS_CODE_ABORTED_REMOTELY;
          break;
        }

        case CONNECTION_STATUS_TIMEOUT:
        {
          pConnection->Status     = SOCK_TCP_CONNECTION_STATUS_UNCONNECTED;
          pConnection->StatusCode = SOCK_TCP_CONNECTION_STATUS_CODE_TIMEOUT;
          break;
        }

        case CONNECTION_STATUS_CONNECT_REJECTED:
        {
          pConnection->Status     = SOCK_TCP_CONNECTION_STATUS_UNCONNECTED;
          pConnection->StatusCode = SOCK_TCP_CONNECTION_STATUS_CODE_CONNECT_REJECTED;
          break;
        }

        case CONNECTION_STATUS_INTERNAL_ERROR:
        default                              :
        {
          pConnection->Status     = SOCK_TCP_CONNECTION_STATUS_UNCONNECTED;
          pConnection->StatusCode = SOCK_TCP_CONNECTION_STATUS_CODE_INTERNAL_ERROR;
          break;
        }
      }

      pConnection->RemoteAddr.IpAddress = Connection [Socket] [Index].IpAddress;
      pConnection->RemoteAddr.Port      = Connection [Socket] [Index].Port;

      pIOControl->UseAs.GetLocalStatus.UseAs.Tcp.NumberConnections += 1uL;
      pConnection++;
    }
  }

  return (Result);
}

/*===========================================================================*/

static T_SOCKET_RESULT socket_ioctl_get_send_status
  (
    unsigned short            Socket,
    T_SDAI_SOCK_IO_CONTROL*   pIOControl
  )
  /*-------------------------------------------------------------------------*/
{
  T_SOCKET_INTERFACE*   pSocketInterface;
  T_SOCKET_RESULT       Result = SOCK_ERR_NOT_SUPPORTED;


  _TRACE (("socket_ioctl_get_send_status"));

  pSocketInterface = interface_lock_data (SOCKET_INTERFACE);

  if (pSocketInterface == NULL)
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_NOT_SUPPORTED);
  }

  if (Socket >= _NUMBER_ARRAY_ELEMENTS (pSocketInterface->Admin.Socket))
  {
    Result = SOCK_ERR_INVALID_SOCKET_ID;
  }
  else
  {
    U32   Flags = _CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->Admin.Socket [Socket].Flags);


    if (! (Flags & SOCKET_FLAG_CREATED))
    {
      Result = SOCK_ERR_SOCKET_NOT_CREATED;
    }
    else if ((Flags & SOCKET_FLAG_CONNECTION_STATUS_MASK) != SOCKET_FLAG_CONNECTION_STATUS_CONNECTED)
    {
      Result = SOCK_ERR_SOCKET_NOT_CONNECTED;
    }
    else
    {
      U16   FifoReadOffset  = _CONVERT_U16_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketReq.FifoReadOffset);
      U16   FifoWriteOffset = _CONVERT_U16_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketReq.FifoWriteOffset);


      pIOControl->UseAs.GetSendStatus.Status = SOCK_SEND_STATUS_NOT_BUSY;

      switch (Flags & SOCKET_FLAG_SEND_STATUS_MASK)
      {
        case SOCKET_FLAG_SEND_STATUS_NO_ERROR        : pIOControl->UseAs.GetSendStatus.StatusCode = SOCK_SEND_STATUS_CODE_NO_ERROR;         break;
        case SOCKET_FLAG_SEND_STATUS_NO_BUFFER       : pIOControl->UseAs.GetSendStatus.StatusCode = SOCK_SEND_STATUS_CODE_NO_BUFFERS;       break;
        case SOCKET_FLAG_SEND_STATUS_HOST_UNREACHABLE: pIOControl->UseAs.GetSendStatus.StatusCode = SOCK_SEND_STATUS_CODE_HOST_UNREACHABLE; break;
        case SOCKET_FLAG_SEND_STATUS_REJECTED        : pIOControl->UseAs.GetSendStatus.StatusCode = SOCK_SEND_STATUS_CODE_REJECTED;         break;
        case SOCKET_FLAG_SEND_STATUS_ERROR           :
        default                                      : pIOControl->UseAs.GetSendStatus.StatusCode = SOCK_SEND_STATUS_CODE_SEND_ERROR;       break;
      }

      pIOControl->UseAs.GetSendStatus.UseAs.Tcp.RemoteAddr.IpAddress = SOCK_IP_ADDR_ANY;
      pIOControl->UseAs.GetSendStatus.UseAs.Tcp.RemoteAddr.Port      = SOCK_PORT_ANY;

      while (FifoReadOffset != FifoWriteOffset)
      {
        T_APPL_SOCKET_DATA_HEADER*    pHeader = (T_APPL_SOCKET_DATA_HEADER*) &pSocketInterface->ApplFifoData [FifoReadOffset];
        T_APPL_SOCKET_DATA_HEADER     Header;


        if ((FifoReadOffset + sizeof (T_APPL_SOCKET_DATA_HEADER)) < INTERNAL_SOCKET_FIFO_SIZE)
        {
          memcpy ((U8*) &Header, (const U8*) pHeader, sizeof (Header));
        }
        else
        {
          U8*   pSrc = (U8*) pHeader;
          U8*   pDest = (U8*) &Header;

          U16   Length = (INTERNAL_SOCKET_FIFO_SIZE - FifoReadOffset);
          U16   RemainingLength = (sizeof (T_APPL_SOCKET_DATA_HEADER) - Length);


          memcpy (pDest, pSrc, Length);
          memcpy ((pDest + Length), &pSocketInterface->ApplFifoData [0], RemainingLength);
        }

        if (Socket == _CONVERT_U16_TO_APPL_BYTE_ORDERING (Header.Socket))
        {
          pIOControl->UseAs.GetSendStatus.Status                         = SOCK_SEND_STATUS_BUSY;
          pIOControl->UseAs.GetSendStatus.UseAs.Tcp.RemoteAddr.IpAddress = _CONVERT_U32_TO_STACK_BYTE_ORDERING (Header.RemoteAddress);
          pIOControl->UseAs.GetSendStatus.UseAs.Tcp.RemoteAddr.Port      = _CONVERT_U16_TO_STACK_BYTE_ORDERING (Header.RemotePort);
          break;
        }

        FifoReadOffset += (_ALIGN4 (U16, _CONVERT_U16_TO_APPL_BYTE_ORDERING (Header.DataLength)) + sizeof (T_APPL_SOCKET_DATA_HEADER));
        FifoReadOffset  = (FifoReadOffset >= INTERNAL_SOCKET_FIFO_SIZE) ? (FifoReadOffset - INTERNAL_SOCKET_FIFO_SIZE) : FifoReadOffset;
      }

      Result = SOCK_SUCCESS;
    }
  }

  interface_unlock_data (SOCKET_INTERFACE);

  return (Result);
}

/*===========================================================================*/

static T_SOCKET_RESULT socket_ioctl_get_receive_status
  (
    unsigned short            Socket,
    T_SDAI_SOCK_IO_CONTROL*   pIOControl
  )
  /*-------------------------------------------------------------------------*/
{
  T_SOCKET_INTERFACE*   pSocketInterface;
  U32                   Flags;


  _TRACE (("socket_ioctl_get_receive_status"));

  pSocketInterface = interface_lock_data (SOCKET_INTERFACE);

  if (pSocketInterface == NULL)
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_NOT_SUPPORTED);
  }

  Flags = _CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->Admin.Socket [Socket].Flags);

  if ((Flags & SOCKET_FLAG_CONNECTION_STATUS_MASK) != SOCKET_FLAG_CONNECTION_STATUS_CONNECTED)
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_SOCKET_NOT_CONNECTED);
  }

  interface_unlock_data (SOCKET_INTERFACE);

  pIOControl->UseAs.GetReceiveStatus.Status = SOCK_RECEIVE_STATUS_CODE_NO_ERROR;

  if (SocketRcvBuffer [Socket].DataLength == 0u)
  {
    pIOControl->UseAs.GetReceiveStatus.StatusCode                     = SOCK_RECEIVE_STATUS_NO_DATA_RECEIVED;
    pIOControl->UseAs.GetReceiveStatus.UseAs.Tcp.RemoteAddr.IpAddress = SOCK_IP_ADDR_ANY;
    pIOControl->UseAs.GetReceiveStatus.UseAs.Tcp.RemoteAddr.Port      = SOCK_PORT_ANY;
  }
  else
  {
    pIOControl->UseAs.GetReceiveStatus.StatusCode                     = SOCK_RECEIVE_STATUS_DATA_RECEIVED;
    pIOControl->UseAs.GetReceiveStatus.UseAs.Tcp.RemoteAddr.IpAddress = SocketRcvBuffer [Socket].RemoteAddress;
    pIOControl->UseAs.GetReceiveStatus.UseAs.Tcp.RemoteAddr.Port      = SocketRcvBuffer [Socket].RemotePort;
  }

  return (SOCK_SUCCESS);
}

/*===========================================================================*/

static T_SOCKET_RESULT socket_ioctl_close_connection
  (
    unsigned short            Socket,
    T_SDAI_SOCK_IO_CONTROL*   pIOControl
  )
  /*-------------------------------------------------------------------------*/
{
  T_SOCKET_INTERFACE*   pSocketInterface;
  T_SOCKET_RESULT       Result;
  U32                   Flags;


  _TRACE (("socket_ioctl_close_connection"));

  pSocketInterface = interface_lock_data (SOCKET_INTERFACE);

  if (pSocketInterface == NULL)
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_NOT_SUPPORTED);
  }

  if (Socket >= _NUMBER_ARRAY_ELEMENTS (pSocketInterface->Admin.Socket))
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_INVALID_SOCKET_ID);
  }

  Flags = _CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->Admin.Socket [Socket].Flags);

  if (! (Flags & SOCKET_FLAG_CREATED))
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_SOCKET_NOT_CREATED);
  }

  if ((Flags & SOCKET_FLAG_CONNECTION_STATUS_MASK) != SOCKET_FLAG_CONNECTION_STATUS_CONNECTED)
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_SOCKET_NOT_CONNECTED);
  }

  if (! (_CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketReq.EventReg) & APPL_EVENT_CLOSE_CONNECTION_REQ))
  {
    pSocketInterface->SocketReq.Connection.IpAddress = _CONVERT_U32_TO_STACK_BYTE_ORDERING (pIOControl->UseAs.CloseTcpConnection.RemoteAddr.IpAddress);
    pSocketInterface->SocketReq.Connection.Port      = _CONVERT_U16_TO_STACK_BYTE_ORDERING (pIOControl->UseAs.CloseTcpConnection.RemoteAddr.Port);
    pSocketInterface->SocketReq.Connection.Socket    = _CONVERT_U16_TO_STACK_BYTE_ORDERING (Socket);

    interrupt_lock ();
    pSocketInterface->SocketReq.EventReg |= _CONVERT_U32_TO_STACK_BYTE_ORDERING (APPL_EVENT_CLOSE_CONNECTION_REQ);
    interrupt_unlock ();

    interface_unlock_data (SOCKET_INTERFACE);
    interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SOCKET_REQ);

    Result = SOCK_SUCCESS;
  }
  else
  {
    /* close connection is request is already pending */
    interface_unlock_data (SOCKET_INTERFACE);
    Result = SOCK_ERR_TEMPORARY_NOT_EXECUTABLE;
  }

  return (Result);
}

/*===========================================================================*/

static T_SOCKET_RESULT socket_ioctl_tcp_accept_filter
  (
    unsigned short            Socket,
    T_SDAI_SOCK_IO_CONTROL*   pIOControl
  )
  /*-------------------------------------------------------------------------*/
{
  T_SOCKET_INTERFACE*   pSocketInterface;
  T_SOCKET_RESULT       Result;
  U32                   Flags;
  unsigned              Index;


  _TRACE (("socket_ioctl_tcp_accept_filter"));

  pSocketInterface = interface_lock_data (SOCKET_INTERFACE);

  if (pSocketInterface == NULL)
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_NOT_SUPPORTED);
  }

  if (Socket >= _NUMBER_ARRAY_ELEMENTS (pSocketInterface->Admin.Socket))
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_INVALID_SOCKET_ID);
  }

  Flags = _CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->Admin.Socket [Socket].Flags);

  if (! (Flags & SOCKET_FLAG_CREATED))
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_SOCKET_NOT_CREATED);
  }

  if ((Flags & SOCKET_FLAG_CONNECTION_STATUS_MASK) == SOCKET_FLAG_CONNECTION_STATUS_CONNECTED)
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_SOCKET_CONNECTED);
  }

  if (! (Flags & SOCK_FLAG_TYPE_TCP))
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_INVALID_ARGUMENT);
  }

  if (! (_CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketReq.EventReg) & APPL_EVENT_ACCEPT_FILTER_REQ))
  {
    pSocketInterface->SocketReq.AcceptFilter.Socket              = _CONVERT_U16_TO_STACK_BYTE_ORDERING (Socket);
    pSocketInterface->SocketReq.AcceptFilter.NumberFilterEntries = _CONVERT_U16_TO_STACK_BYTE_ORDERING (pIOControl->UseAs.TcpAcceptFilter.NumberEntries);

    for (Index = 0; Index < _MIN (pIOControl->UseAs.TcpAcceptFilter.NumberEntries, _NUMBER_ARRAY_ELEMENTS (pIOControl->UseAs.TcpAcceptFilter.Filter)); Index++)
    {
      pSocketInterface->SocketReq.AcceptFilter.Filter [Index] = _CONVERT_U32_TO_STACK_BYTE_ORDERING (pIOControl->UseAs.TcpAcceptFilter.Filter [Index]);
    }

    interrupt_lock ();
    pSocketInterface->SocketReq.EventReg |= _CONVERT_U32_TO_STACK_BYTE_ORDERING (APPL_EVENT_ACCEPT_FILTER_REQ);
    interrupt_unlock ();

    interface_unlock_data (SOCKET_INTERFACE);
    interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SOCKET_REQ);

    Result = SOCK_SUCCESS;
  }
  else
  {
    /* accept filer is request is already pending */
    interface_unlock_data (SOCKET_INTERFACE);
    Result = SOCK_ERR_TEMPORARY_NOT_EXECUTABLE;
  }

  return (Result);
}

/*===========================================================================*/

static T_SOCKET_RESULT socket_ioctl_add_or_del_udp_multicast
  (
    unsigned short            Socket,
    T_SDAI_SOCK_IO_CONTROL*   pIOControl,
    BOOL                      AddMulticast
  )
  /*-------------------------------------------------------------------------*/
{
  T_SOCKET_INTERFACE*   pSocketInterface;
  T_SOCKET_RESULT       Result;
  U32                   Flags;
  U32                   EventReg;


  _TRACE (("socket_ioctl_add_or_del_udp_multicast"));

  pSocketInterface = interface_lock_data (SOCKET_INTERFACE);

  if (pSocketInterface == NULL)
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_NOT_SUPPORTED);
  }

  if (Socket >= _NUMBER_ARRAY_ELEMENTS (pSocketInterface->Admin.Socket))
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_INVALID_SOCKET_ID);
  }

  Flags = _CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->Admin.Socket [Socket].Flags);

  if (! (Flags & SOCKET_FLAG_CREATED))
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_SOCKET_NOT_CREATED);
  }

  if (Flags & SOCK_FLAG_TYPE_TCP)
  {
    interface_unlock_data (SOCKET_INTERFACE);
    return (SOCK_ERR_INVALID_ARGUMENT);
  }

  EventReg = (AddMulticast) ? APPL_EVENT_ADD_MULTICAST_REQ : APPL_EVENT_DEL_MULTICAST_REQ;

  if (! (_CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketReq.EventReg) & EventReg))
  {
    if (AddMulticast)
    {
      pSocketInterface->SocketReq.AddMulticast.Socket    = _CONVERT_U16_TO_STACK_BYTE_ORDERING (Socket);
      pSocketInterface->SocketReq.AddMulticast.IpAddress = _CONVERT_U32_TO_STACK_BYTE_ORDERING (pIOControl->UseAs.AddMulticast.IpAddress);
    }
    else
    {
      pSocketInterface->SocketReq.DelMulticast.Socket    = _CONVERT_U16_TO_STACK_BYTE_ORDERING (Socket);
      pSocketInterface->SocketReq.DelMulticast.IpAddress = _CONVERT_U32_TO_STACK_BYTE_ORDERING (pIOControl->UseAs.DelMulticast.IpAddress);
    }

    interrupt_lock ();
    pSocketInterface->SocketReq.EventReg = _CONVERT_U32_TO_STACK_BYTE_ORDERING (EventReg);
    interrupt_unlock ();

    interface_unlock_data (SOCKET_INTERFACE);
    interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SOCKET_REQ);

    Result = SOCK_SUCCESS;
  }
  else
  {
    /* accept filer is request is already pending */
    interface_unlock_data (SOCKET_INTERFACE);
    Result = SOCK_ERR_TEMPORARY_NOT_EXECUTABLE;
  }

  return (Result);
}

/*===========================================================================*/

static void socket_copy_data_to_fifo
  (
    T_SOCKET_INTERFACE*     pSocketInterface,
    unsigned short          Socket,
    T_SDAI_SOCK_ADDR*       pRemoteAddress,
    const unsigned char*    pData,
    U16                     DataLength,
    U16                     FifoWriteOffset
  )
  /*-------------------------------------------------------------------------*/
{
  U32   PacketsWritten;
  U16   FreeFifoSize;


  _TRACE (("socket_copy_data_to_fifo"));

  FreeFifoSize = (INTERNAL_SOCKET_FIFO_SIZE - FifoWriteOffset);

  if (FreeFifoSize >= (_ALIGN4 (U16, DataLength) + sizeof (T_APPL_SOCKET_DATA_HEADER)))
  {
    T_APPL_SOCKET_DATA_HEADER*    pHeader = (T_APPL_SOCKET_DATA_HEADER*) &pSocketInterface->ApplFifoData [FifoWriteOffset];


    pHeader->Socket        = _CONVERT_U16_TO_STACK_BYTE_ORDERING (Socket);
    pHeader->DataLength    = _CONVERT_U16_TO_STACK_BYTE_ORDERING (DataLength);
    pHeader->RemoteAddress = _CONVERT_U32_TO_STACK_BYTE_ORDERING (pRemoteAddress->IpAddress);
    pHeader->RemotePort    = _CONVERT_U16_TO_STACK_BYTE_ORDERING (pRemoteAddress->Port);
    pHeader->Alignment     = 0u;

    memcpy (&pSocketInterface->ApplFifoData [FifoWriteOffset + sizeof (T_APPL_SOCKET_DATA_HEADER)], pData, DataLength);

    FifoWriteOffset += _ALIGN4 (U16, (DataLength + sizeof (T_APPL_SOCKET_DATA_HEADER)));
    FifoWriteOffset  = (FifoWriteOffset == INTERNAL_SOCKET_FIFO_SIZE ) ? 0u : FifoWriteOffset;
  }
  else if (FreeFifoSize >= sizeof (T_APPL_SOCKET_DATA_HEADER))
  {
    T_APPL_SOCKET_DATA_HEADER*    pHeader = (T_APPL_SOCKET_DATA_HEADER*) &pSocketInterface->ApplFifoData [FifoWriteOffset];


    pHeader->Socket        = _CONVERT_U16_TO_STACK_BYTE_ORDERING (Socket);
    pHeader->DataLength    = _CONVERT_U16_TO_STACK_BYTE_ORDERING (DataLength);
    pHeader->RemoteAddress = _CONVERT_U32_TO_STACK_BYTE_ORDERING (pRemoteAddress->IpAddress);
    pHeader->RemotePort    = _CONVERT_U16_TO_STACK_BYTE_ORDERING (pRemoteAddress->Port);
    pHeader->Alignment     = 0u;

    FreeFifoSize -= sizeof (T_APPL_SOCKET_DATA_HEADER);
    memcpy (&pSocketInterface->ApplFifoData [FifoWriteOffset + sizeof (T_APPL_SOCKET_DATA_HEADER)], pData, FreeFifoSize);
    memcpy (&pSocketInterface->ApplFifoData [0], (pData + FreeFifoSize), (DataLength - FreeFifoSize));

    FifoWriteOffset = _ALIGN4 (U16, (DataLength - FreeFifoSize));
  }
  else
  {
    T_APPL_SOCKET_DATA_HEADER   Header;


    Header.Socket        = _CONVERT_U16_TO_STACK_BYTE_ORDERING (Socket);
    Header.DataLength    = _CONVERT_U16_TO_STACK_BYTE_ORDERING (DataLength);
    Header.RemoteAddress = _CONVERT_U32_TO_STACK_BYTE_ORDERING (pRemoteAddress->IpAddress);
    Header.RemotePort    = _CONVERT_U16_TO_STACK_BYTE_ORDERING (pRemoteAddress->Port);
    Header.Alignment     = 0u;

    memcpy (&pSocketInterface->ApplFifoData [FifoWriteOffset], (U8*) &Header, FreeFifoSize);

    FreeFifoSize = (sizeof (T_APPL_SOCKET_DATA_HEADER) - FreeFifoSize);

    memcpy (&pSocketInterface->ApplFifoData [0], (((U8*) &Header) + (sizeof (T_APPL_SOCKET_DATA_HEADER) - FreeFifoSize)), FreeFifoSize);
    memcpy (&pSocketInterface->ApplFifoData [FreeFifoSize], pData, DataLength);

    FifoWriteOffset = _ALIGN4 (U16, (DataLength + FreeFifoSize));
  }

  PacketsWritten = _CONVERT_U32_TO_APPL_BYTE_ORDERING (pSocketInterface->SocketReq.PacketsWritten);

  pSocketInterface->SocketReq.PacketsWritten  = _CONVERT_U32_TO_STACK_BYTE_ORDERING (PacketsWritten + 1uL);
  pSocketInterface->SocketReq.FifoWriteOffset = _CONVERT_U16_TO_STACK_BYTE_ORDERING (FifoWriteOffset);

  return;
}

/*===========================================================================*/

void socket_copy_data_from_fifo
  (
    T_SOCKET_INTERFACE*     pSocketInterface,
    U16                     FifoReadOffset,
    U16                     FifoWriteOffset,
    U32                     PacketsRead,
    U32                     PacketsWritten
  )
  /*-------------------------------------------------------------------------*/
{
  _TRACE (("socket_copy_data_from_fifo"));

  while ( (FifoReadOffset != FifoWriteOffset) || (PacketsRead != PacketsWritten) )
  {
    T_APPL_SOCKET_DATA_HEADER*    pHeader = (T_APPL_SOCKET_DATA_HEADER*) &pSocketInterface->StackFifoData [FifoReadOffset];

    T_APPL_SOCKET_DATA_HEADER     Header;

    U32                           IpAddress;

    U16                           Socket;
    U16                           Port;
    U16                           DataLength;


    if ((FifoReadOffset + sizeof (T_APPL_SOCKET_DATA_HEADER)) < INTERNAL_SOCKET_FIFO_SIZE)
    {
      memcpy ((U8*) &Header, (const U8*) pHeader, sizeof (Header));
    }
    else
    {
      U8*   pSrc = (U8*) pHeader;
      U8*   pDest = (U8*) &Header;

      U16   Length = (INTERNAL_SOCKET_FIFO_SIZE - FifoReadOffset);
      U16   RemainingLength = (sizeof (T_APPL_SOCKET_DATA_HEADER) - Length);


      memcpy (pDest, pSrc, Length);
      memcpy ((pDest + Length), &pSocketInterface->StackFifoData [0], RemainingLength);
    }

    FifoReadOffset += sizeof (T_APPL_SOCKET_DATA_HEADER);
    FifoReadOffset  = (FifoReadOffset >= INTERNAL_SOCKET_FIFO_SIZE) ? (FifoReadOffset - INTERNAL_SOCKET_FIFO_SIZE) : FifoReadOffset;
    IpAddress       = _CONVERT_U32_TO_APPL_BYTE_ORDERING (Header.RemoteAddress);
    Port            = _CONVERT_U16_TO_APPL_BYTE_ORDERING (Header.RemotePort);
    Socket          = _CONVERT_U16_TO_APPL_BYTE_ORDERING (Header.Socket);
    DataLength      = _CONVERT_U16_TO_APPL_BYTE_ORDERING (Header.DataLength);

    _ASSERT (Socket < _NUMBER_ARRAY_ELEMENTS (SocketRcvBuffer));
    _ASSERT (DataLength < sizeof (SocketRcvBuffer [Socket].Data));

    if ( (SocketRcvBuffer [Socket].DataLength == 0u                                                    ) ||
         ((SocketRcvBuffer [Socket].DataLength + DataLength < sizeof (SocketRcvBuffer [Socket].Data)) &&
          (SocketRcvBuffer [Socket].RemoteAddress == IpAddress                                        ) &&
          (SocketRcvBuffer [Socket].RemotePort == Port                                                )) )
    {
      if ((FifoReadOffset + DataLength) <= INTERNAL_SOCKET_FIFO_SIZE)
      {
        memcpy (&SocketRcvBuffer [Socket].Data [SocketRcvBuffer [Socket].DataLength], &pSocketInterface->StackFifoData [FifoReadOffset], DataLength);
      }
      else
      {
        U8*   pSrc = &pSocketInterface->StackFifoData [FifoReadOffset];
        U8*   pDest = &SocketRcvBuffer [Socket].Data [SocketRcvBuffer [Socket].DataLength];

        U16   Length = (INTERNAL_SOCKET_FIFO_SIZE - FifoReadOffset);
        U16   RemainingLength = (DataLength - Length);


        memcpy (pDest, pSrc, Length);
        memcpy ((pDest + Length), &pSocketInterface->StackFifoData [0], RemainingLength);
      }

      SocketRcvBuffer [Socket].DataLength   += DataLength;
      SocketRcvBuffer [Socket].RemoteAddress = IpAddress;
      SocketRcvBuffer [Socket].RemotePort    = Port;

      _BITSET_SET_BIT (SocketDataReceived, Socket);

      /*---------------------------------------------------------------------*/

      FifoReadOffset += _ALIGN4 (U16, DataLength);
      FifoReadOffset  = (FifoReadOffset >= INTERNAL_SOCKET_FIFO_SIZE) ? (FifoReadOffset - INTERNAL_SOCKET_FIFO_SIZE) : FifoReadOffset;

      PacketsRead += 1uL;

      pSocketInterface->SocketInd.PacketsRead    = _CONVERT_U32_TO_STACK_BYTE_ORDERING (PacketsRead);
      pSocketInterface->SocketInd.FifoReadOffset = _CONVERT_U16_TO_STACK_BYTE_ORDERING (FifoReadOffset);
    }
    else
    {
      break;
    }
  }

  return;
}

#endif /* SDAI_INCLUDE_SOCKET */

#endif /* DOXYINTERN */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
