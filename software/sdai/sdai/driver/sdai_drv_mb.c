/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

#include <string.h>

#include "sdai.h"

#include "cfg_cc.h"

#include "sdai_ext.h"
#include "sdai_interface.h"

#ifdef SDAI_INCLUDE_SOCKET
  #include "sdai_socket.h"
  #include "socket_interface.h"
#endif

#include "sdai_drv.h"

#include "porting.h"

#ifdef SDAI_INCLUDE_MBAK

/******************************************************************************
DEFINES
******************************************************************************/

/******************************************************************************
TYPEDEFS
******************************************************************************/

/** \brief Holds the MODBUS related informations in the MB driver Module */
typedef struct _T_SDAI_DRV_MB
{
  int                 ShutdownPending;              /**< Indicates a pending shutdown command */
  int                 ExceptionPending;             /**< Indicates a pending exception */

  U16                 CurrentSlot;                  /**< The next free slot in the device */
  U8*                 pIODataBase;                  /**< Start address of the IO data area */

  int                 PlugComplete;                 /**< User signaled plugging complete */

} T_SDAI_DRV_MB;

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

static U8     sdai_drv_mb_deinit                    (T_SDAI_BACKEND_DRIVER*);
static U8     sdai_drv_mb_get_version               (T_SDAI_BACKEND_DRIVER*, struct SDAI_VERSION_DATA*);
static U8     sdai_drv_mb_get_state                 (T_SDAI_BACKEND_DRIVER*, U16*, struct SDAI_ADDSTATUS_DATA* pAddStatus);
static U8     sdai_drv_mb_plug_unit                 (T_SDAI_BACKEND_DRIVER*, U8, U8, U8, U32*, const struct SDAI_CFG_DATA_EXT*);

static U8     sdai_drv_mb_get_data                  (T_SDAI_BACKEND_DRIVER*, U32, U8*, U8*);
static U8     sdai_drv_mb_set_data                  (T_SDAI_BACKEND_DRIVER*, U32, U8, const U8*);

static U8     sdai_drv_mb_write_response            (T_SDAI_BACKEND_DRIVER*, const struct SDAI_WRITE_RES* pWrite);
static U8     sdai_drv_mb_read_response             (T_SDAI_BACKEND_DRIVER*, const struct SDAI_READ_RES* pRead);

static U8     sdai_drv_mb_proto_start               (T_SDAI_BACKEND_DRIVER*);
static int    sdai_drv_mb_plugging_complete         (T_SDAI_BACKEND_DRIVER*);

static U8     sdai_drv_mb_init_watchdog             (T_SDAI_BACKEND_DRIVER*, const U16);
static U8     sdai_drv_mb_check_and_trigger_watchdog(T_SDAI_BACKEND_DRIVER*);

static U8     sdai_drv_mb_lock_resources            (T_SDAI_BACKEND_DRIVER*, U16);
static U8     sdai_drv_mb_unlock_resources          (T_SDAI_BACKEND_DRIVER*, U16);

static int    sdai_drv_mb_handle_stack_signal       (U16, void*);
static void   sdai_drv_mb_handle_service_ind        (T_SDAI_BACKEND_DRIVER*);
static void   sdai_drv_mb_handle_data_ind           (T_SDAI_BACKEND_DRIVER*);

static U8     sdai_drv_mb_calculate_data_size       (U8 UnitType, U16 NumberElements, U8* pInputSize, U8* pOutputSize);

/******************************************************************************
PUBLIC DATA
******************************************************************************/

/******************************************************************************
PRIVATE DATA
******************************************************************************/

static T_SDAI_DRV_MB                   SdaiBackendDriverMB;        /**< Local instance of MB driver structure */
static struct SDAI_OUTPUT_CHANGED_IND  OutputChangeInd;            /**< Holds informations of changed units for data cbk */

/******************************************************************************
PUBLIC FUNCTIONS
******************************************************************************/

/******************************************************************************
PROTECTED FUNCTIONS
******************************************************************************/

/**
 * @desc The function sdai_drv_mb_init() initializes the interface and the Modbus
 *       related part of the SDAI and and creates the Modbus task.
 *
 * @return Result
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_INTERNAL |
 *           #SDAI_ERR_INVALID_ARGUMENT |
 *           #SDAI_ERR_PROTOCOL_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pApplicationInitData
 * - type : struct SDAI_INIT*
 * - range: whole address space
 */
U8 sdai_drv_mb_init (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, struct SDAI_INIT* pApplicationInitData)
{
  T_INTERFACE_MANAGEMENT_DATA*    pManagementData;
  U8                              Result = SDAI_ERR_INTERNAL;
  U32                             IpAddr;
  U32                             Netmask;
  U32                             Gateway;

  _TRACE (("sdai_drv_mb_init"));

  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pApplicationInitData);

  memcpy ((U8*) &IpAddr,  pApplicationInitData->Ident.UseAs.Mb.Address, sizeof (IpAddr));
  memcpy ((U8*) &Netmask, pApplicationInitData->Ident.UseAs.Mb.Netmask, sizeof (Netmask));
  memcpy ((U8*) &Gateway, pApplicationInitData->Ident.UseAs.Mb.Gateway, sizeof (Gateway));

  IpAddr  = convert_network_u32_to_processor_u32 (IpAddr);
  Netmask = convert_network_u32_to_processor_u32 (Netmask);
  Gateway = convert_network_u32_to_processor_u32 (Gateway);

  if (sdai_check_ip_settings (IpAddr, Netmask, Gateway) != IP_SETTINGS_OK)
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  if (pApplicationInitData->Info.Flags & SDAI_DYNAMIC_IO_CONFIGURATION)
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  /* @brief The interface of the driver is initialized with default functions. So
   * we have to overwrite only functions we support in the modbus implementation.
   */
  pSdaiBackendDriver->Interface.deinit            = sdai_drv_mb_deinit;
  pSdaiBackendDriver->Interface.get_version       = sdai_drv_mb_get_version;
  pSdaiBackendDriver->Interface.get_state         = sdai_drv_mb_get_state;
  pSdaiBackendDriver->Interface.plug_unit         = sdai_drv_mb_plug_unit;

  pSdaiBackendDriver->Interface.get_data          = sdai_drv_mb_get_data;
  pSdaiBackendDriver->Interface.set_data          = sdai_drv_mb_set_data;

  pSdaiBackendDriver->Interface.write_response    = sdai_drv_mb_write_response;
  pSdaiBackendDriver->Interface.read_response     = sdai_drv_mb_read_response;

  pSdaiBackendDriver->Interface.proto_start       = sdai_drv_mb_proto_start;
  pSdaiBackendDriver->Interface.plugging_complete = sdai_drv_mb_plugging_complete;

  pSdaiBackendDriver->Interface.init_watchdog     = sdai_drv_mb_init_watchdog;

  pSdaiBackendDriver->Interface.lock_resources    = sdai_drv_mb_lock_resources;
  pSdaiBackendDriver->Interface.unlock_resources  = sdai_drv_mb_unlock_resources;

  pSdaiBackendDriver->pPrivateData                = &SdaiBackendDriverMB;

  /*-------------------------------------------------------------------------*/

  SdaiBackendDriverMB.CurrentSlot     = 0u;
  SdaiBackendDriverMB.PlugComplete    = 0;
  SdaiBackendDriverMB.ShutdownPending = 0u;

  Result = interface_init
  (
    sdai_drv_mb_handle_stack_signal,
    NULL,
    INTERFACE_APPLICATION,
    pSdaiBackendDriver,
    SDAI_BACKEND_MODBUS
  );

  if (Result == SDAI_SUCCESS)
  {
    SdaiBackendDriverMB.pIODataBase = interface_get_iodata_base ();

    pManagementData = interface_lock_data (MANAGEMENT_INTERFACE);

    if (pManagementData != NULL)
    {
      pManagementData->SupportedCallbacks = 0u;

      if (pApplicationInitData->Callback.DataCbk != NULL)      { pManagementData->SupportedCallbacks |= DATA_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.WriteReqCbk != NULL)  { pManagementData->SupportedCallbacks |= WRITE_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.ReadReqCbk != NULL)   { pManagementData->SupportedCallbacks |= READ_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.ExceptionCbk != NULL) { pManagementData->SupportedCallbacks |= EXCEPTION_CALLBACK_SUPPORTED; }

      Result = SDAI_SUCCESS;
    }

    interface_unlock_data (MANAGEMENT_INTERFACE);
  }

  return (Result);
}

/******************************************************************************
PRIVATE FUNCTIONS
******************************************************************************/

/**
 * @desc The function sdai_drv_mb_deinit() deinitializes the interface and shuts down
 *       the Modbus stack.
 *
 * @return Result
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_STACK_NOT_RDY
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_drv_mb_deinit (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_SDAI_DRV_MB*    pSdaiBackendDriverMB;


  _TRACE (("sdai_drv_mb_deinit"));

  _ASSERT_PTR (pSdaiBackendDriver);
  pSdaiBackendDriverMB = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverMB->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pSdaiBackendDriverMB->ShutdownPending = 1;

  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_STOP_STACK);

  /* wait for backend shutting down */
  interface_deinit (INTERFACE_APPLICATION);

  pSdaiBackendDriver->pPrivateData = NULL;

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_mb_get_version() returns the version string of the
 *       Modbus stack.
 *
 * @return Result
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_STACK_NOT_RDY
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pVersionData
 * - type : struct SDAI_VERSION_DATA*
 * - range: whole address space
 */
static U8 sdai_drv_mb_get_version (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, struct SDAI_VERSION_DATA* pVersionData)
{
  T_SDAI_DRV_MB*              pSdaiBackendDriverMB;
  T_INTERFACE_STATUS_DATA*    pStatusData;


  _TRACE (("sdai_drv_mb_get_version"));

  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pVersionData);

  pSdaiBackendDriverMB = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverMB->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  pStatusData = interface_lock_data (STATUS_INTERFACE);
  memcpy (pVersionData, &pStatusData->VersionData, sizeof (struct SDAI_VERSION_DATA));
  interface_unlock_data (STATUS_INTERFACE);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_mb_get_state() returns the current state of the Modbus
 *       protocol stack.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_STACK_NOT_RDY |
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[out] pState
 * - type : U16*
 * - range: #SDAI_STATE_MASK_ONLINE                |
 *          #SDAI_STATE_MASK_CONNECTED             |
 *          #SDAI_STATE_MASK_DOUBLE_ADDRESS_ERROR  |
 *          #SDAI_STATE_MASK_HARDWARE_ERROR        |
 *          #SDAI_STATE_MASK_SECPROM_ERROR         |
 *          #SDAI_STATE_MASK_APPL_WATCHDOG_EXPIRED |
 *          #SDAI_STATE_MASK_PROTOCOL_ERROR
 * @param[out] pAddStatus
 * - type : struct SDAI_ADDSTATUS_DATA*
 * - range: whole address space
 */
static U8 sdai_drv_mb_get_state (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U16 *pState, struct SDAI_ADDSTATUS_DATA* pAddStatus)
{
  T_SDAI_DRV_MB*              pSdaiBackendDriverMB;
  T_INTERFACE_STATUS_DATA*    pStatusData;


  _TRACE (("sdai_drv_mb_get_state"));

  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pState);

  pSdaiBackendDriverMB = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverMB->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pStatusData = interface_lock_data (STATUS_INTERFACE);
  *pState = pStatusData->StackStatus;

  if (pAddStatus != NULL)
  {
    memcpy (pAddStatus, &pStatusData->StackAddStatus, sizeof (struct SDAI_ADDSTATUS_DATA));
  }

  interface_unlock_data (STATUS_INTERFACE);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_mb_plug_unit() makes the Modbus specific part of
 *       plugging. First the function verifies the passed parameters. If invalid
 *       or if the user tries to plug a unit which overlaps with another unit, a error
 *       is returned. If successfull a free unit is allocated within the unit interface
 *       and the SDAI unit ID is calculated and returned to the application. The passed
 *       parameters InputSize and OutputSize are ignored. The necessary data size will
 *       be calculated based on the specified start address and the number of data elements
 *       for the given unit type.
 *
 * @return Result
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_STACK_NOT_RDY |
 *           #SDAI_ERR_INVALID_ARGUMENT |
 *           #SDAI_ERR_UNKNOWN_UNIT_TYPE |
 *           #SDAI_ERR_MAX_UNIT_REACHED
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] UnitType defines the unit type or signals plugging complete.
 * - type : U8
 * - range: #SDAI_MB_UNIT_TYPE_DISCRETE_INPUT |
 *          #SDAI_MB_UNIT_TYPE_COILS |
 *          #SDAI_MB_UNIT_TYPE_INPUT_REGISTER |
 *          #SDAI_MB_UNIT_TYPE_HOLDING_REGISTER
 * @param[in] InputSize This parameter is ignored
 * - type : U8
 * - range: 0 to 255
 * @param[in] OutputSize This parameter is ignored
 * - type : U8
 * - range: 0 to 255
 * @param[out] pId Pointer to U32 to store new generated unique ID.
 * - type : U32*
 * - range: whole address space
 * @param[in] pCfgData modbus specific configuration data
 * - type : SDAI_CFG_DATA*
 * - range: whole address space
 */
static U8 sdai_drv_mb_plug_unit (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U8 UnitType, U8 InputSize, U8 OutputSize, U32* pId, const struct SDAI_CFG_DATA_EXT* pCfgData)
{
  T_SDAI_DRV_MB*      pSdaiBackendDriverMB;
  T_INTERFACE_UNIT*   pUnit;
  U16                 UnitIndex = 0xFFFF;
  U8                  Result;


  _TRACE (("sdai_drv_mb_plug_unit"));

  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverMB = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverMB->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  *pId = SDAI_INVALID_ID;

  /* if users signals plugging complete we don't want to check the args */
  if (UnitType == SDAI_UNIT_TYPE_PLUGGING_COMPLETE)
  {
    pSdaiBackendDriverMB->PlugComplete = 1;
    return (pSdaiBackendDriver->Interface.proto_start (pSdaiBackendDriver));
  }

  if (pCfgData == NULL)
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  if (pCfgData->UseAs.Mb.NumberElements < SDAI_MB_MIN_NUMBER_ELEMENTS)
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  if ( (UnitType != SDAI_MB_UNIT_TYPE_DISCRETE_INPUT  ) &&
       (UnitType != SDAI_MB_UNIT_TYPE_COILS           ) &&
       (UnitType != SDAI_MB_UNIT_TYPE_INPUT_REGISTER  ) &&
       (UnitType != SDAI_MB_UNIT_TYPE_HOLDING_REGISTER) )
  {
    return (SDAI_ERR_UNKNOWN_UNIT_TYPE);
  }

  Result = sdai_drv_mb_calculate_data_size (UnitType, pCfgData->UseAs.Mb.NumberElements, &InputSize, &OutputSize);

  if (Result != SDAI_SUCCESS)
  {
    return (Result);
  }

  /* check if the data overlaps with other already plugged units */
  for (UnitIndex = 0; UnitIndex < pSdaiBackendDriverMB->CurrentSlot; UnitIndex++)
  {
    pUnit = interface_lock_unit (UnitIndex);

    if (pUnit != NULL)
    {
      if (UnitType == pUnit->Type)
      {
        U16   NumberElements;
        U16   ExistingStartAddress;
        U16   ExistingEndAddress;
        U16   RequestedStartAddress;
        U16   RequestedEndAddress;

        NumberElements       = pUnit->ProtocolExtention;
        ExistingStartAddress = _SDAI_MB_ID_TO_ADDRESS  (U16, pUnit->Id);
        ExistingEndAddress   = ExistingStartAddress + (NumberElements - 1);

        RequestedStartAddress = pCfgData->UseAs.Mb.StartAddress;
        RequestedEndAddress   = RequestedStartAddress + (pCfgData->UseAs.Mb.NumberElements - 1);

        if ( ((RequestedStartAddress >= ExistingStartAddress) && (RequestedStartAddress <= ExistingEndAddress)) ||
             ((RequestedEndAddress   >= ExistingStartAddress) && (RequestedEndAddress   <= ExistingEndAddress)) ||
             ((RequestedStartAddress <= ExistingStartAddress) && (RequestedEndAddress   >= ExistingEndAddress)) )
        {
          /* one or more of the requested data elements are already assigned to another unit */
          interface_unlock_unit (UnitIndex);
          return (SDAI_ERR_INVALID_ARGUMENT);
        }
      }
    }

    interface_unlock_unit (UnitIndex);
  }

  /*-------------------------------------------------------------------------*/

  UnitIndex = 0xFFFF;

  pUnit = interface_get_free_unit (InputSize, OutputSize, &UnitIndex);

  if (pUnit != NULL)
  {
    _ASSERT ((pUnit->DataLengthInput == InputSize) && (pUnit->DataLengthOutput == OutputSize));

    pUnit->Id                = _SDAI_MB_ADDRESS_AND_INDEX_TO_ID (pCfgData->UseAs.Mb.StartAddress, UnitIndex);
    pUnit->Type              = UnitType;
    pUnit->ProtocolExtention = pCfgData->UseAs.Mb.NumberElements;
    pUnit->InternalFlags     = (pCfgData->InterfaceType & SDAI_INTERFACE_MASK) | UNIT_INTERNAL_FLAG_IN_USE;

    pSdaiBackendDriverMB->CurrentSlot += 1u;
    *pId                               = pUnit->Id;

    interface_unlock_unit (_SDAI_MB_ID_TO_INDEX (U16, pUnit->Id));

    return (SDAI_SUCCESS);
  }

  return (SDAI_ERR_MAX_UNIT_REACHED);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_mb_get_data() reads the current output or input data for the unit
 *       defined by Id. This function can be used on input and output units.
 *
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *       SDAI_ERR_NOSUCHUNIT |
 *       SDAI_ERR_STACK_NOT_RDY
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] Id
 * - type : U32
 * - range: ???
 * @param[out] pStatus
 * - type : U8*
 * - range: whole address space
 * @param[out] pData
 * - type : U8*
 * - range: whole address space
 * @remarks
 */
static U8 sdai_drv_mb_get_data (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U32 Id, U8* pStatus, U8* pData)
{
  T_SDAI_DRV_MB*      pSdaiBackendDriverMB;
  T_INTERFACE_UNIT*   pUnit;
  U16                 UnitIndex;


  _TRACE (("sdai_drv_mb_get_data"));

  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverMB = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverMB->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  UnitIndex = _SDAI_MB_ID_TO_INDEX (U16, Id);
  pUnit     = interface_lock_unit (UnitIndex);

  if (pUnit == NULL)
  {
    interface_unlock_unit (UnitIndex);
    *pStatus = SDAI_DATA_STATUS_INVALID;
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (Id != pUnit->Id)
  {
    interface_unlock_unit (UnitIndex);
    *pStatus = SDAI_DATA_STATUS_INVALID;
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (pUnit->DataLengthOutput > 0u)
  {
    *pStatus = pUnit->StatusOutput;

    _ASSERT (pUnit->DataOutputOffset != 0xFFFF);
    memcpy (pData, &pSdaiBackendDriverMB->pIODataBase [pUnit->DataOutputOffset], pUnit->DataLengthOutput);
  }
  else if (pUnit->DataLengthInput > 0u)
  {
    *pStatus = pUnit->StatusInput;

    _ASSERT (pUnit->DataInputOffset != 0xFFFF);
    memcpy (pData, &pSdaiBackendDriverMB->pIODataBase [pUnit->DataInputOffset], pUnit->DataLengthInput);
  }
  else
  {
    /* Unit has no outputs or inputs configured.
     */
    *pStatus = SDAI_DATA_STATUS_INVALID;
  }

  interface_unlock_unit (UnitIndex);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_mb_set_data() sets the input data for the unit defined by
 *       Id. This function can be used on input units and also on output units. The reason is
 *       that Holding Registers and Coils will be assigned to output units. Because Holding
 *       Registers and Coils can be used for both input and output data, we have to allow
 *       setting of input data for these units.
 *
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *       SDAI_ERR_NOSUCHUNIT |
 *       SDAI_ERR_INVALID_ARGUMENT |
 *       SDAI_ERR_STACK_NOT_RDY
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] Id
 * - type : U32
 * - range: ???
 * @param[in] Status
 * - type : U8
 * - range: ???
 * @param[in] pData
 * - type : U8*
 * - range: whole address space
 */
static U8 sdai_drv_mb_set_data (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U32 Id, U8 Status, const U8* pData)
{
  T_SDAI_DRV_MB*      pSdaiBackendDriverMB;
  T_INTERFACE_UNIT*   pUnit;
  U16                 UnitIndex;


  _TRACE (("sdai_drv_mb_set_data"));

  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverMB = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverMB->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  if (pData == NULL)
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  /*-------------------------------------------------------------------------*/

  UnitIndex = _SDAI_MB_ID_TO_INDEX (U16, Id);
  pUnit     = interface_lock_unit (UnitIndex);

  if (pUnit == NULL)
  {
    interface_unlock_unit (UnitIndex);
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (Id != pUnit->Id)
  {
    interface_unlock_unit (UnitIndex);
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (pUnit->InternalFlags & SDAI_INTERFACE_MASK)
  {
    interface_unlock_unit (UnitIndex);
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (pUnit->DataLengthInput > 0u)
  {
    pUnit->StatusInput = (Status == SDAI_DATA_STATUS_VALID) ? SDAI_DATA_STATUS_VALID : SDAI_DATA_STATUS_INVALID;

    _ASSERT (pUnit->DataInputOffset != 0xFFFF);
    memcpy (&pSdaiBackendDriverMB->pIODataBase [pUnit->DataInputOffset], pData, pUnit->DataLengthInput);
  }
  else if (pUnit->DataLengthOutput > 0u)
  {
    _ASSERT ( (pUnit->Type == SDAI_MB_UNIT_TYPE_COILS) || (pUnit->Type == SDAI_MB_UNIT_TYPE_HOLDING_REGISTER) );
    _ASSERT (pUnit->DataOutputOffset != 0xFFFF);

    memcpy (&pSdaiBackendDriverMB->pIODataBase [pUnit->DataOutputOffset], pData, pUnit->DataLengthOutput);
  }

  interface_unlock_unit (UnitIndex);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_mb_write_response() handles the Modbus specific
 * part of the write response. It checks the parameters, copies the data to the
 * shared ram and sends a signal to the stack.
 *
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY |
 *           SDAI_ERR_SERVICE_PENDING |
 *           SDAI_ERROR
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pWrite
 * - type : const struct SDAI_WRITE_RES*
 * - range: whole address space
 * @remarks
 */
static U8 sdai_drv_mb_write_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_WRITE_RES* pWrite)
{
  T_SDAI_DRV_MB*                  pSdaiBackendDriverMB;
  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  struct SDAI_WRITE_RES*          pServiceWrite;


  _TRACE (("sdai_drv_mb_write_response"));

  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pWrite);

  pSdaiBackendDriverMB = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverMB->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  pServiceWrite = &pServiceData->UseAs.Write;

  pServiceData->ServiceType              = SERVICE_REQ_RES_WRITE;
  pServiceWrite->UseAs.Mb.Service        = pWrite->UseAs.Mb.Service;
  pServiceWrite->UseAs.Mb.StartAddress   = pWrite->UseAs.Mb.StartAddress;
  pServiceWrite->UseAs.Mb.NumberElements = pWrite->UseAs.Mb.NumberElements;
  pServiceWrite->ErrorCode               = pWrite->ErrorCode;

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_mb_read_response() handles the Modbus specific
 * part of the read response. It checks the parameters, copies the data to the
 * shared ram and sends a signal to the stack.
 *
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY |
 *           SDAI_ERR_SERVICE_PENDING |
 *           SDAI_ERROR
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pRead
 * - type : const struct SDAI_READ_RES*
 * - range: whole address space
 */
static U8 sdai_drv_mb_read_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_READ_RES* pRead)
{
  T_SDAI_DRV_MB*                  pSdaiBackendDriverMB;
  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  struct SDAI_READ_RES*           pServiceRead;


  _TRACE (("sdai_drv_mb_read_response"));

  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pRead);

  pSdaiBackendDriverMB = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverMB->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  pServiceRead = &pServiceData->UseAs.Read;

  pServiceData->ServiceType             = SERVICE_REQ_RES_READ;
  pServiceRead->UseAs.Mb.Service        = pRead->UseAs.Mb.Service;
  pServiceRead->UseAs.Mb.StartAddress   = pRead->UseAs.Mb.StartAddress;
  pServiceRead->UseAs.Mb.NumberElements = pRead->UseAs.Mb.NumberElements;
  pServiceRead->ErrorCode               = pRead->ErrorCode;
  pServiceRead->Length                  = pRead->Length;

  memcpy ((pServiceRead + 1), (pRead + 1), pRead->Length);

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_mb_proto_start() starts the Modbus stack. This function is called
 *       when the applications signals "plugging complete".
 *
 * @return Result
 * - type  : U8
 * - values: #SDAI_SUCCESS             |
 *           #SDAI_ERR_SERVICE_PENDING |
 *           #SDAI_ERR_INTERNAL
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_drv_mb_proto_start (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;


  _TRACE (("sdai_drv_mb_proto_start"));

  _ASSERT_PTR (pSdaiBackendDriver);

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  pServiceData->ServiceType = SERVICE_REQ_RES_INIT;

  memcpy
  (
    &pServiceData->UseAs.Init.Device,
    &pSdaiBackendDriver->ApplicationInitData.Info,
    sizeof (pServiceData->UseAs.Init.Device)
  );

  memcpy
  (
    &pServiceData->UseAs.Init.Ident,
    &pSdaiBackendDriver->ApplicationInitData.Ident,
    sizeof (pServiceData->UseAs.Init.Ident)
  );

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_START_STACK);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_mb_plugging_complete() returns the plugging state of
 *       modules.
 * @return
 * - type  : int
 * - values: 0, 1
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static int sdai_drv_mb_plugging_complete (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_SDAI_DRV_MB*      pSdaiBackendDriverMB;


  _TRACE (("sdai_drv_mb_plugging_complete"));

  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverMB = pSdaiBackendDriver->pPrivateData;

  return (pSdaiBackendDriverMB->PlugComplete);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_mb_init_watchdog()
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_drv_mb_init_watchdog (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const U16 WatchdogFactor)
{
  T_INTERFACE_MANAGEMENT_DATA*    pManagementData;
  U8                              Result = SDAI_ERR_COM_NOT_SUPPORTED;


  _TRACE (("sdai_drv_mb_init_watchdog"));

  _ASSERT_PTR (pSdaiBackendDriver);

  if (check_watchdog_supported () == SDAI_SUCCESS)
  {
    pManagementData = interface_lock_data (MANAGEMENT_INTERFACE);

    pManagementData->WatchdogFactor          = (WatchdogFactor * 100);
    pManagementData->WatchdogRunning         = 0;
    pManagementData->WatchdogFlagStack       = 0;
    pManagementData->WatchdogFlagApplication = 0;

    /* install the real trigger function */
    pSdaiBackendDriver->Interface.check_trigger_watchdog = sdai_drv_mb_check_and_trigger_watchdog;

    interface_unlock_data (MANAGEMENT_INTERFACE);
    interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_INIT_WATCHDOG);

    Result = SDAI_SUCCESS;
  }

  return (Result);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_mb_check_and_trigger_watchdog()
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS | #SDAI_ERR_WATCHDOG_EXPIRED
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_drv_mb_check_and_trigger_watchdog (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_INTERFACE_MANAGEMENT_DATA*    pManagementData;
  U8                              Result = SDAI_SUCCESS;


  _TRACE (("sdai_drv_mb_check_and_trigger_watchdog"));

  _ASSERT_PTR (pSdaiBackendDriver);
  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);

  pManagementData = interface_lock_data (MANAGEMENT_INTERFACE);

  /* Trigger WD */
  pManagementData->WatchdogFlagApplication = 0;

  if (pManagementData->WatchdogRunning == 1)
  {
    /* Check WD */
    pManagementData->WatchdogFlagStack += 1;

    if (pManagementData->WatchdogFlagStack > 2)
    {
      handle_stack_watchdog_expired ();
      Result = SDAI_ERR_WATCHDOG_EXPIRED;
    }
  }
  else
  {
    pManagementData->WatchdogRunning = 1;
  }

  interface_unlock_data (MANAGEMENT_INTERFACE);

  return (Result);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_mb_lock_resources()
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS | #SDAI_ERR_INTERNAL
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] Resources Specifies which resources should be unlocked
 * - type : U16
 * - range: #SDAI_SHARED_RESOURCE_FLASH |
 *          #SDAI_SHARED_RESOURCE_MDIO
 */
static U8 sdai_drv_mb_lock_resources (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U16 Resource)
{
  _TRACE (("sdai_drv_mb_lock_resources"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);

  if (Resource == SDAI_SHARED_RESOURCE_MDIO)
  {
    if (interface_lock_data (SHARED_MDIO_INTERFACE) != NULL)
    {
      return (SDAI_SUCCESS);
    }
  }

  return (SDAI_ERR_INTERNAL);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_mb_unlock_resources()
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] Resources Specifies which resources should be unlocked
 * - type : U16
 * - range: #SDAI_SHARED_RESOURCE_FLASH |
 *          #SDAI_SHARED_RESOURCE_MDIO
 */
static U8 sdai_drv_mb_unlock_resources (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U16 Resource)
{
  _TRACE (("sdai_drv_mb_unlock_resources"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);

  if (Resource == SDAI_SHARED_RESOURCE_MDIO)
  {
    interface_unlock_data (SHARED_MDIO_INTERFACE);
  }

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_mb_handle_stack_signal() handels the signals between the stack and
 *       the application part of the SDAI.
 * @return
 * - type  : int
 * - values: 0
 * @param[in] Signal
 * - type : U16 int
 * - range: INTERFACE_SIGNAL_SERVICE_IND
 * @param[in] pContext
 * - type : VOID*
 * - range: whole address space
 */
static int sdai_drv_mb_handle_stack_signal (U16 Signal, void* pContext)
{
  T_SDAI_BACKEND_DRIVER*    pSdaiBackendDriver = pContext;


  _TRACE (("sdai_drv_mb_handle_stack_signal"));

  if (Signal & INTERFACE_SIGNAL_SERVICE_IND)
  {
    sdai_drv_mb_handle_service_ind (pSdaiBackendDriver);
  }

  if (Signal & INTERFACE_SIGNAL_DATA_IND)
  {
    sdai_drv_mb_handle_data_ind (pSdaiBackendDriver);
  }

#ifdef SDAI_INCLUDE_SOCKET
  if (Signal & INTERFACE_SIGNAL_SOCKET_IND)
  {
    sdai_drv_sock_handle_socket_ind (Signal);
  }
#endif

  return 0;
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_mb_handle_data_ind() handels the incoming
 *       meassages in the data indication channel (Unit interface).
 * @return
 * - type  : VOID
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static void sdai_drv_mb_handle_data_ind (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_SDAI_DRV_MB*      pSdaiBackendDriverMB;
  T_INTERFACE_UNIT*   pUnit;
  U16                 UnitIndex;
  U16                 NumberChangedUnits = 0;


  _TRACE (("sdai_drv_mb_handle_data_ind"));

  _ASSERT_PTR (pSdaiBackendDriver);
  pSdaiBackendDriverMB = pSdaiBackendDriver->pPrivateData;

  pUnit = interface_lock_unit (0);

  for (UnitIndex = 0; UnitIndex < pSdaiBackendDriverMB->CurrentSlot; UnitIndex++)
  {
    if (pUnit->InternalFlags & UNIT_INTERNAL_FLAG_OUTPUT_CHANGED)
    {
      pUnit->InternalFlags &= ~UNIT_INTERNAL_FLAG_OUTPUT_CHANGED;

      OutputChangeInd.Id [NumberChangedUnits] = pUnit->Id;
      NumberChangedUnits++;
    }

    pUnit++;
  }

  interface_unlock_unit (0);

  if (NumberChangedUnits > 0)
  {
    OutputChangeInd.NumberUnits = NumberChangedUnits;

    if (pSdaiBackendDriver->ApplicationInitData.Callback.DataCbk != NULL)
    {
      pSdaiBackendDriver->ApplicationInitData.Callback.DataCbk (&OutputChangeInd);
    }
  }
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_mb_handle_service_ind() handels the incoming
 *       meassages in the service indication channel.
 * @return
 * - type  : VOID
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static void sdai_drv_mb_handle_service_ind (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_SDAI_DRV_MB*              pSdaiBackendDriverMB;
  T_INTERFACE_SERVICE_IND*    pServiceData;


  _TRACE (("sdai_drv_mb_handle_service_ind"));

  _ASSERT_PTR (pSdaiBackendDriver);
  pSdaiBackendDriverMB = pSdaiBackendDriver->pPrivateData;

  pServiceData = interface_lock_data (SERVICE_IND_INTERFACE);

  if (pServiceData != NULL)
  {
    switch (pServiceData->ServiceType)
    {
      case SERVICE_IND_EXCEPTION:
      {
        if (pSdaiBackendDriver->ApplicationInitData.Callback.ExceptionCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.ExceptionCbk (&pServiceData->UseAs.Exception);
        }

        pSdaiBackendDriverMB->ExceptionPending = 1;
        break;
      }

      case SERVICE_IND_WRITE:
      {
        if (pSdaiBackendDriver->ApplicationInitData.Callback.WriteReqCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.WriteReqCbk (&pServiceData->UseAs.Write);
        }

        break;
      }

      case SERVICE_IND_READ:
      {
        if (pSdaiBackendDriver->ApplicationInitData.Callback.ReadReqCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.ReadReqCbk (&pServiceData->UseAs.Read);
        }

        break;
      }
    }

    pServiceData->ServiceType = SERVICE_IND_FREE;

    /* If pending services on stack side, signal that channel is free */
    if (pServiceData->ServicesPending != 0x00)
    {
      interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_CHANNEL_FREE);
    }
  }

  interface_unlock_data (SERVICE_IND_INTERFACE);

  return;
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_mb_calculate_data_size() calculates the necessary unit's IO data size
 *       for the passed number of data elements.
 *
 * @return Result
 * - type : U8
 * - range: #SDAI_SUCCESS              |
 *          #SDAI_ERR_INVALID_ARGUMENT |
 *          #SDAI_ERR_UNKNOWN_UNIT_TYPE
 * @param[in] UnitType
 * - type : U8
 * - range: whole address space
 * @param[out] pInputSize
 * - type : U8*
 * - range: whole address space
 * @param[out] pOutputSize
 * - type : U8*
 * - range: whole address space
 */
static U8 sdai_drv_mb_calculate_data_size (U8 UnitType, U16 NumberElements, U8* pInputSize, U8* pOutputSize)
{
  U8    Result = SDAI_SUCCESS;


  _TRACE (("sdai_drv_mb_calculate_data_size"));

  _ASSERT_PTR (pInputSize);
  _ASSERT_PTR (pOutputSize);

  *pInputSize  = 0;
  *pOutputSize = 0;

  switch (UnitType)
  {
    case SDAI_MB_UNIT_TYPE_DISCRETE_INPUT:
    {
      if (NumberElements > SDAI_MB_MAX_NUMBER_DISCRETE_INPUTS)
      {
        Result = SDAI_ERR_INVALID_ARGUMENT;
      }
      else
      {
        *pInputSize  = (NumberElements / 8);
        *pInputSize += ((NumberElements % 8) > 0) ? 1u : 0u; /* add the remainder */
      }

      break;
    }

    case SDAI_MB_UNIT_TYPE_COILS:
    {
      if (NumberElements > SDAI_MB_MAX_NUMBER_COILS)
      {
        Result = SDAI_ERR_INVALID_ARGUMENT;
      }
      else
      {
        /* Coils can be used for both input and output data. But the SDAI interface did not
           allow a common input/output unit with input and output data located to the same
           memory address. Here we assign Coils to a output unit. Within function sdai_set_data()
           we will then allow setting of the input data.
         */
        *pOutputSize  = (NumberElements / 8);
        *pOutputSize += ((NumberElements % 8) > 0) ? 1u : 0u; /* add the remainder */
      }

      break;
    }

    case SDAI_MB_UNIT_TYPE_INPUT_REGISTER:
    {
      if (NumberElements > SDAI_MB_MAX_NUMBER_INPUT_REGISTERS)
      {
        Result = SDAI_ERR_INVALID_ARGUMENT;
      }
      else
      {
        *pInputSize = (NumberElements * 2);
      }

      break;
    }

    case SDAI_MB_UNIT_TYPE_HOLDING_REGISTER:
    {
      if (NumberElements > SDAI_MB_MAX_NUMBER_HOLDING_REGISTERS)
      {
        Result = SDAI_ERR_INVALID_ARGUMENT;
      }
      else
      {
        /* Holding Registers can be used for both input and output data. But the SDAI
           interface did not allow a common input/output unit with input and output data
           located to the same memory address. Here we assign Holding Registers to a output
           unit. Within function sdai_set_data() we will then allow setting of the input data.
         */
        *pOutputSize = (NumberElements * 2);
      }

      break;
    }

    default:
    {
      Result = SDAI_ERR_UNKNOWN_UNIT_TYPE;
      break;
    }
  }

  return (Result);
}

#endif  /* SDAI_INCLUDE_MBAK */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
