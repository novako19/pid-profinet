/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

#include <string.h>

#include "sdai.h"

#include "cfg_cc.h"

#include "sdai_ext.h"
#include "sdai_interface.h"

#ifdef SDAI_INCLUDE_SOCKET
  #include "sdai_socket.h"
  #include "socket_interface.h"
#endif

#include "sdai_drv.h"

#include "porting.h"

#ifdef SDAI_INCLUDE_EIP

/******************************************************************************
DEFINES
******************************************************************************/

/* vendor specific assembly instance ID ranges
 */
#define ASM_INSTANCE_VENDSPEC_MIN                 0x0064
#define ASM_INSTANCE_VENDSPEC_MAX                 0x00C7

/* reserved instance IDs
 */
#define ASM_INSTANCE_ID_NOT_SUPPORTED             0x0000
#define ASM_INSTANCE_ID_RESERVED_INPUT_ONLY       0x00C5
#define ASM_INSTANCE_ID_RESERVED_LISTEN_ONLY      0x00C6
#define ASM_INSTANCE_ID_RESERVED_OUTPUT_ONLY      0x00C7

/******************************************************************************
TYPEDEFS
******************************************************************************/

typedef struct _T_SDAI_DRV_EIP
{
  int                 ShutdownPending;              /**< indicates a pending shutdown command */
  int                 ExceptionPending;             /**< indicates a pending exception */

  U16                 CurrentSlot;                  /**< the next free slot in the device */

  U16                 DefaultInstanceId;            /**< the next free default assembly instance ID */
  U8*                 pIODataBase;                  /**< */

  int                 PlugComplete;                 /**< user signaled plugging complete */

} T_SDAI_DRV_EIP;

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

static U8     sdai_drv_eip_deinit                    (T_SDAI_BACKEND_DRIVER*);
static U8     sdai_drv_eip_get_version               (T_SDAI_BACKEND_DRIVER*, struct SDAI_VERSION_DATA*);
static U8     sdai_drv_eip_get_state                 (T_SDAI_BACKEND_DRIVER*, U16*, struct SDAI_ADDSTATUS_DATA*);
static U8     sdai_drv_eip_plug_unit                 (T_SDAI_BACKEND_DRIVER*, U8, U8, U8, U32*, const struct SDAI_CFG_DATA_EXT*);

static U8     sdai_drv_eip_get_data                  (T_SDAI_BACKEND_DRIVER*, U32, U8*, U8*);
static U8     sdai_drv_eip_set_data                  (T_SDAI_BACKEND_DRIVER*, U32, U8, const U8*);

static U8     sdai_drv_eip_write_response            (T_SDAI_BACKEND_DRIVER*, const struct SDAI_WRITE_RES*);
static U8     sdai_drv_eip_read_response             (T_SDAI_BACKEND_DRIVER*, const struct SDAI_READ_RES*);
static U8     sdai_drv_eip_control_response          (T_SDAI_BACKEND_DRIVER*, const struct SDAI_CONTROL_RES*);

static U8     sdai_drv_eip_proto_start               (T_SDAI_BACKEND_DRIVER*);
static int    sdai_drv_eip_plugging_complete         (T_SDAI_BACKEND_DRIVER*);

static U8     sdai_drv_eip_init_watchdog             (T_SDAI_BACKEND_DRIVER*, const U16);
static U8     sdai_drv_eip_check_and_trigger_watchdog(T_SDAI_BACKEND_DRIVER*);

static U8     sdai_drv_eip_lock_resources            (T_SDAI_BACKEND_DRIVER*, U16);
static U8     sdai_drv_eip_unlock_resources          (T_SDAI_BACKEND_DRIVER*, U16);

static int    sdai_drv_eip_handle_stack_signal       (U16, void*);
static void   sdai_drv_eip_handle_service_ind        (T_SDAI_BACKEND_DRIVER*);
static void   sdai_drv_eip_handle_data_ind           (T_SDAI_BACKEND_DRIVER*);

/******************************************************************************
PUBLIC DATA
******************************************************************************/

/******************************************************************************
PRIVATE DATA
******************************************************************************/

static T_SDAI_DRV_EIP                  SdaiBackendDriverEIP;       /**< local instance of EIP driver struct */
static struct SDAI_OUTPUT_CHANGED_IND  OutputChangedInd;           /**< Holds informations of changed units for data cbk */

/******************************************************************************
PUBLIC FUNCTIONS
******************************************************************************/

/******************************************************************************
PROTECTED FUNCTIONS
******************************************************************************/

/**
 * The function sdai_eip_init() initializes the EtherNet/IP backend.
 *
 * @return Result
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_INTERNAL |
 *           #SDAI_ERR_INVALID_ARGUMENT |
 *           #SDAI_ERR_PROTOCOL_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pApplicationInitData
 * - type : struct SDAI_INIT*
 * - range: whole address space
 */
U8 sdai_drv_eip_init (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, struct SDAI_INIT* pApplicationInitData)
{
  T_INTERFACE_MANAGEMENT_DATA*    pManagementData;
  U8                              Result = SDAI_ERR_INTERNAL;


  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pApplicationInitData);

  if ( ((pApplicationInitData->Ident.UseAs.Eip.Flags & SDAI_EIP_STARTUP_CONFIG_MASK) == SDAI_EIP_USE_CONFIG_FROM_NV_STORAGE)  ||
       ((pApplicationInitData->Ident.UseAs.Eip.Flags & SDAI_EIP_STARTUP_CONFIG_MASK) == SDAI_EIP_USE_CONFIG_FROM_HW_SETTINGS) )
  {
    U32   IpAddr;
    U32   Netmask;
    U32   Gateway;

    memcpy ((U8*) &IpAddr,  pApplicationInitData->Ident.UseAs.Eip.Address, sizeof (IpAddr));
    memcpy ((U8*) &Netmask, pApplicationInitData->Ident.UseAs.Eip.Netmask, sizeof (Netmask));
    memcpy ((U8*) &Gateway, pApplicationInitData->Ident.UseAs.Eip.Gateway, sizeof (Gateway));

    IpAddr  = convert_network_u32_to_processor_u32 (IpAddr);
    Netmask = convert_network_u32_to_processor_u32 (Netmask);
    Gateway = convert_network_u32_to_processor_u32 (Gateway);

    /* check the passed network settings */
    if (sdai_check_ip_settings (IpAddr, Netmask, Gateway) != IP_SETTINGS_OK)
    {
      return (SDAI_ERR_INVALID_ARGUMENT);
    }
  }
  /* else: the network settings are obtained via DHCP */

  if (pApplicationInitData->Info.Flags & SDAI_DYNAMIC_IO_CONFIGURATION)
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  /* @brief The interface of the driver is initialized with default function. So
   * we have to overwrite only functions we support in the EtherNet/IP implementation.
   */
  pSdaiBackendDriver->Interface.deinit            = sdai_drv_eip_deinit;
  pSdaiBackendDriver->Interface.get_version       = sdai_drv_eip_get_version;
  pSdaiBackendDriver->Interface.get_state         = sdai_drv_eip_get_state;
  pSdaiBackendDriver->Interface.plug_unit         = sdai_drv_eip_plug_unit;

  pSdaiBackendDriver->Interface.get_data          = sdai_drv_eip_get_data;
  pSdaiBackendDriver->Interface.set_data          = sdai_drv_eip_set_data;

  pSdaiBackendDriver->Interface.write_response    = sdai_drv_eip_write_response;
  pSdaiBackendDriver->Interface.read_response     = sdai_drv_eip_read_response;
  pSdaiBackendDriver->Interface.control_response  = sdai_drv_eip_control_response;

  pSdaiBackendDriver->Interface.proto_start       = sdai_drv_eip_proto_start;
  pSdaiBackendDriver->Interface.plugging_complete = sdai_drv_eip_plugging_complete;

  pSdaiBackendDriver->Interface.init_watchdog     = sdai_drv_eip_init_watchdog;

  pSdaiBackendDriver->Interface.lock_resources    = sdai_drv_eip_lock_resources;
  pSdaiBackendDriver->Interface.unlock_resources  = sdai_drv_eip_unlock_resources;

  pSdaiBackendDriver->pPrivateData                = &SdaiBackendDriverEIP;

  /*-------------------------------------------------------------------------*/

  SdaiBackendDriverEIP.CurrentSlot       = 0u;
  SdaiBackendDriverEIP.DefaultInstanceId = ASM_INSTANCE_VENDSPEC_MIN;
  SdaiBackendDriverEIP.PlugComplete      = 0;
  SdaiBackendDriverEIP.ShutdownPending   = 0u;

  Result = interface_init
           (
             sdai_drv_eip_handle_stack_signal,
             NULL,
             INTERFACE_APPLICATION,
             pSdaiBackendDriver,
             SDAI_BACKEND_EIPS
           );

  if (Result == SDAI_SUCCESS)
  {
    SdaiBackendDriverEIP.pIODataBase = interface_get_iodata_base ();

    pManagementData = interface_lock_data (MANAGEMENT_INTERFACE);

    if (pManagementData != NULL)
    {
      pManagementData->SupportedCallbacks = 0u;

      if (pApplicationInitData->Callback.DataCbk != NULL)      { pManagementData->SupportedCallbacks |= DATA_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.IdentDataCbk != NULL) { pManagementData->SupportedCallbacks |= IDENT_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.WriteReqCbk != NULL)  { pManagementData->SupportedCallbacks |= WRITE_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.ReadReqCbk != NULL)   { pManagementData->SupportedCallbacks |= READ_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.ControlReqCbk != NULL){ pManagementData->SupportedCallbacks |= CONTROL_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.ExceptionCbk != NULL) { pManagementData->SupportedCallbacks |= EXCEPTION_CALLBACK_SUPPORTED; }
    }

    interface_unlock_data (MANAGEMENT_INTERFACE);
  }

  return (Result);
}

/******************************************************************************
PRIVATE FUNCTIONS
******************************************************************************/

/**
 * The function sdai_drv_eip_deinit() shuts down the EtherNet/IP stack and
 * frees the data structure.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS
 */
static U8 sdai_drv_eip_deinit (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_SDAI_DRV_EIP*    pSdaiBackendDriverEIP;


  pSdaiBackendDriverEIP = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverEIP->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pSdaiBackendDriverEIP->ShutdownPending = 1;

  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_STOP_STACK);

  /* wait for backend shutting down */
  interface_deinit (INTERFACE_APPLICATION);

  pSdaiBackendDriver->pPrivateData   = NULL;

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_eip_get_version() returns the version string of the
 * EtherNet/IP stack.
 * @return
 * - type  : char*
 * - values: ???
 */
static U8 sdai_drv_eip_get_version (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, struct SDAI_VERSION_DATA* pVersionData)
{
  T_SDAI_DRV_EIP*              pSdaiBackendDriverEIP;

  T_INTERFACE_STATUS_DATA*     pStatusData;


  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pVersionData);

  pSdaiBackendDriverEIP = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverEIP->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  pStatusData = interface_lock_data (STATUS_INTERFACE);
  memcpy(pVersionData, &pStatusData->VersionData, sizeof(struct SDAI_VERSION_DATA));
  interface_unlock_data (STATUS_INTERFACE);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_eip_get_state() returns the current state of the EtherNet/IP
 * protocol stack.
 *
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS |
 *           #SDAI_ERR_STACK_NOT_RDY |
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[out] pState
 * - type : U16*
 * - range: #SDAI_STATE_MASK_ONLINE                |
 *          #SDAI_STATE_MASK_CONNECTED             |
 *          #SDAI_STATE_MASK_DOUBLE_ADDRESS_ERROR  |
 *          #SDAI_STATE_MASK_HARDWARE_ERROR        |
 *          #SDAI_STATE_MASK_SECPROM_ERROR         |
 *          #SDAI_STATE_MASK_APPL_WATCHDOG_EXPIRED |
 *          #SDAI_STATE_MASK_PROTOCOL_ERROR
 * @param[out] pAddStatus
 * - type : struct SDAI_ADDSTATUS_DATA*
 * - range: whole address space
 */
static U8 sdai_drv_eip_get_state (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U16* pState, struct SDAI_ADDSTATUS_DATA* pAddStatus)
{
  T_SDAI_DRV_EIP*              pSdaiBackendDriverEIP;

  T_INTERFACE_STATUS_DATA*     pStatusData;


  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pState);

  pSdaiBackendDriverEIP = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverEIP->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pStatusData = interface_lock_data (STATUS_INTERFACE);

  *pState = pStatusData->StackStatus;

  if (pAddStatus != NULL)
  {
    memcpy (pAddStatus, &pStatusData->StackAddStatus, sizeof (struct SDAI_ADDSTATUS_DATA));
  }

  interface_unlock_data (STATUS_INTERFACE);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_eip_plug_unit() makes the EtherNet/IP specific part of
 * plugging.
 * It calculates the ID for the unit and returns it to the application.
 * @return
 * - type  : U32
 * - values: ???
 * @param[in] UnitType
 * - type : U8
 * - range: SDAI_UNIT_TYPE_INPUT | SDAI_UNIT_TYPE_OUTPUT | SDAI_EIP_UNIT_TYPE_CONFIGURATION
 * @param[in] InputSize defines the input data length of the unit in bytes.
 * - type : U8
 * - range: 0 to 255
 * @param[in] OutputSize defines the output data length of the unit in bytes.
 * - type : U8
 * - range: 0 to 255
 * @param[out] pUnit
 * - type : SDAI_UNIT *
 * - range: whole address space
 */
static U8 sdai_drv_eip_plug_unit (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U8 UnitType, U8 InputSize, U8 OutputSize, U32* pId, const struct SDAI_CFG_DATA_EXT* pCfgData)
{
  T_SDAI_DRV_EIP*      pSdaiBackendDriverEIP;
  T_INTERFACE_UNIT*    pUnit;
  U16                  UnitIndex = 0xFFFF;
  U16                  InstanceId = ASM_INSTANCE_ID_NOT_SUPPORTED;


  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverEIP = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverEIP->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  *pId = SDAI_INVALID_ID;

  /* if users signals plugging complete we don't want to check the args */
  if(UnitType == SDAI_UNIT_TYPE_PLUGGING_COMPLETE)
  {
    pSdaiBackendDriverEIP->PlugComplete = 1;

    return (pSdaiBackendDriver->Interface.proto_start (pSdaiBackendDriver));
  }

  if (pCfgData != NULL)
  {
    InstanceId = pCfgData->UseAs.Eip.InstanceId;

    if ( (InstanceId < SDAI_EIP_MIN_INST_ID                 ) ||
         (InstanceId > SDAI_EIP_MAX_INST_ID                 ) ||
         (InstanceId == ASM_INSTANCE_ID_RESERVED_INPUT_ONLY ) ||
         (InstanceId == ASM_INSTANCE_ID_RESERVED_LISTEN_ONLY) ||
         (InstanceId == ASM_INSTANCE_ID_RESERVED_OUTPUT_ONLY) )
    {
      return (SDAI_ERR_INVALID_ARGUMENT);
    }
  }

  if ( (UnitType != SDAI_UNIT_TYPE_GENERIC_INPUT)     &&
       (UnitType != SDAI_UNIT_TYPE_GENERIC_OUTPUT)    &&
       (UnitType != SDAI_EIP_UNIT_TYPE_CONFIGURATION) )
  {
    return (SDAI_ERR_UNKNOWN_UNIT_TYPE);
  }

  /*-------------------------------------------------------------------------*/

  pUnit = interface_get_free_unit (InputSize, OutputSize, &UnitIndex);

  if (pUnit != NULL)
  {
    _ASSERT ((pUnit->DataLengthInput == InputSize) && (pUnit->DataLengthOutput == OutputSize));
    _ASSERT (((pUnit->DataInputOffset != 0xFFFF) && (InputSize > 0)) ||
             ((pUnit->DataOutputOffset != 0xFFFF) && (OutputSize > 0)));

    if (pCfgData == NULL)
    {
      if ( (UnitType == SDAI_UNIT_TYPE_GENERIC_INPUT)     ||
           (UnitType == SDAI_UNIT_TYPE_GENERIC_OUTPUT)    ||
           (UnitType == SDAI_EIP_UNIT_TYPE_CONFIGURATION) )
      {
        _ASSERT (pSdaiBackendDriverEIP->DefaultInstanceId <= ASM_INSTANCE_VENDSPEC_MAX);

        /* set default instance ID for I/O assembly
         */
        InstanceId = pSdaiBackendDriverEIP->DefaultInstanceId;
        pSdaiBackendDriverEIP->DefaultInstanceId += 1u;
      }
    }

    pUnit->Id                = _SDAI_EIP_INDEX_AND_INSTANCE_TO_ID (UnitIndex, InstanceId);
    pUnit->Type              = UnitType;
    pUnit->ProtocolExtention = InstanceId;
    pUnit->InternalFlags     = (pCfgData->InterfaceType & SDAI_INTERFACE_MASK) | UNIT_INTERNAL_FLAG_IN_USE;

    pSdaiBackendDriverEIP->CurrentSlot += 1u;
    *pId                               = pUnit->Id;

    /* interface_lock_unit is already called by interface_get_free_unit */
    interface_unlock_unit (_SDAI_EIP_ID_TO_INDEX (U16, pUnit->Id));

    return (SDAI_SUCCESS);
  }

  return (SDAI_ERR_MAX_UNIT_REACHED);
}

/*===========================================================================*/

/**
 * The function sdai_drv_eip_get_data() gets the current input or output data
 * for the unit defined by Id. This function can be used on input and output units.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS | SDAI_ERR_NOSUCHUNIT
 * @param[in] Id
 * - type : U32
 * - range: ???
 * @param[out] pStatus
 * - type : U8*
 * - range: whole address space
 * @param[out] pData
 * - type : U8*
 * - range: whole address space
 */
static U8 sdai_drv_eip_get_data (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U32 Id, U8* pStatus, U8* pData)
{
  T_SDAI_DRV_EIP*     pSdaiBackendDriverEIP;
  T_INTERFACE_UNIT*   pUnit;
  U16                 UnitIndex;


  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverEIP = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverEIP->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  UnitIndex = _SDAI_EIP_ID_TO_INDEX (U16, Id);
  pUnit     = interface_lock_unit (UnitIndex);

  if (pUnit == NULL)
  {
    interface_unlock_unit (UnitIndex);
    *pStatus = SDAI_DATA_STATUS_INVALID;
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (Id != pUnit->Id)
  {
    interface_unlock_unit (UnitIndex);
    *pStatus = SDAI_DATA_STATUS_INVALID;
    return (SDAI_ERR_NOSUCHUNIT);
  }

  *pStatus = pUnit->StatusOutput;

  _ASSERT (pUnit->DataOutputOffset != 0xFFFF);
  memcpy(pData, &pSdaiBackendDriverEIP->pIODataBase [pUnit->DataOutputOffset], pUnit->DataLengthOutput);

  interface_unlock_unit (UnitIndex);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_eip_set_data() sets the input data for the unit defined by
 * Id. This function can only be used on input units.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS | SDAI_ERR_NOSUCHUNIT
 * @param[in] Id
 * - type : U32
 * - range: ???
 * @param[in] Status
 * - type : U8
 * - range: ???
 * @param[in] pData
 * - type : U8*
 * - range: whole address space
 */
static U8 sdai_drv_eip_set_data (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U32 Id, U8 Status, const U8* pData)
{
  T_SDAI_DRV_EIP*     pSdaiBackendDriverEIP;
  T_INTERFACE_UNIT*   pUnit;
  U16                 UnitIndex;


  _ASSERT_PTR (pSdaiBackendDriver);
  _AVOID_UNUSED_WARNING (Status);

  pSdaiBackendDriverEIP = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverEIP->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  UnitIndex = _SDAI_EIP_ID_TO_INDEX (U16, Id);
  pUnit     = interface_lock_unit (UnitIndex);

  if (pUnit == NULL)
  {
    interface_unlock_unit (UnitIndex);
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (Id != pUnit->Id)
  {
    interface_unlock_unit (UnitIndex);
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (pUnit->InternalFlags & SDAI_INTERFACE_MASK)
  {
    interface_unlock_unit (UnitIndex);
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (pUnit->DataLengthInput > 0u)
  {
    if (pData == NULL)
    {
      interface_unlock_unit (UnitIndex);
      return (SDAI_ERR_INVALID_ARGUMENT);
    }

    _ASSERT (pUnit->DataInputOffset != 0xFFFF);
    memcpy (&pSdaiBackendDriverEIP->pIODataBase [pUnit->DataInputOffset], pData, pUnit->DataLengthInput);
  }

  interface_unlock_unit (UnitIndex);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_eip_write_response() handles the EtherNet/IP specific
 * part of the write response. It checks the parameters, copies the data to the
 * shared ram and sends a signal to the stack.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY |
 *           SDAI_ERR_SERVICE_PENDING |
 *           SDAI_ERROR
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pWrite
 * - type : const struct SDAI_WRITE_RES*
 * - range: whole address space
 * @remarks
 */
static U8 sdai_drv_eip_write_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_WRITE_RES* pWrite)
{
  T_SDAI_DRV_EIP*                 pSdaiBackendDriverEIP;

  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  struct SDAI_WRITE_RES*          pServiceWrite;


  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pWrite);

  pSdaiBackendDriverEIP = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverEIP->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  pServiceWrite = &pServiceData->UseAs.Write;

  pServiceData->ServiceType            = SERVICE_REQ_RES_WRITE;
  pServiceWrite->UseAs.Eip.ClassId     = pWrite->UseAs.Eip.ClassId;
  pServiceWrite->UseAs.Eip.InstanceId  = pWrite->UseAs.Eip.InstanceId;
  pServiceWrite->UseAs.Eip.AttributeId = pWrite->UseAs.Eip.AttributeId;
  pServiceWrite->ErrorCode             = pWrite->ErrorCode;

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_eip_read_response() handles the EtherNet/IP specific
 * part of the read response. It checks the parameters, copies the data to the
 * shared ram and sends a signal to the stack.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY |
 *           SDAI_ERR_SERVICE_PENDING |
 *           SDAI_ERROR
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pRead
 * - type : const struct SDAI_READ_RES*
 * - range: whole address space
 */
static U8 sdai_drv_eip_read_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_READ_RES* pRead)
{
  T_SDAI_DRV_EIP*                 pSdaiBackendDriverEIP;

  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  struct SDAI_READ_RES*           pServiceRead;


  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pRead);

  pSdaiBackendDriverEIP = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverEIP->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  pServiceRead = &pServiceData->UseAs.Read;

  pServiceData->ServiceType           = SERVICE_REQ_RES_READ;
  pServiceRead->UseAs.Eip.ClassId     = pRead->UseAs.Eip.ClassId;
  pServiceRead->UseAs.Eip.InstanceId  = pRead->UseAs.Eip.InstanceId;
  pServiceRead->UseAs.Eip.AttributeId = pRead->UseAs.Eip.AttributeId;
  pServiceRead->ErrorCode             = pRead->ErrorCode;
  pServiceRead->Length                = pRead->Length;

  memcpy ((pServiceRead + 1), (pRead + 1), pRead->Length);

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_eip_control_response() handles the EtherNet/IP specific
 * part of the control response. It checks the parameters, copies the data to the
 * shared ram and sends a signal to the stack.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY |
 *           SDAI_ERR_SERVICE_PENDING |
 *           SDAI_ERROR
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pControl
 * - type : const struct SDAI_CONTROL_RES*
 * - range: whole address space
 */
static U8 sdai_drv_eip_control_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_CONTROL_RES* pControl)
{
  T_SDAI_DRV_EIP*                 pSdaiBackendDriverEIP;

  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  struct SDAI_CONTROL_RES*        pServiceControl;


  _ASSERT_PTR (pControl);
  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverEIP = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverEIP->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  pServiceControl = &pServiceData->UseAs.Control;

  pServiceData->ServiceType              = SERVICE_REQ_RES_CONTROL;
  pServiceControl->UseAs.Eip.ClassId     = pControl->UseAs.Eip.ClassId;
  pServiceControl->UseAs.Eip.InstanceId  = pControl->UseAs.Eip.InstanceId;
  pServiceControl->UseAs.Eip.AttributeId = pControl->UseAs.Eip.AttributeId;
  pServiceControl->ControlCode           = pControl->ControlCode;
  pServiceControl->ErrorCode             = pControl->ErrorCode;
  pServiceControl->Length                = pControl->Length;

  memcpy ((pServiceControl + 1), (pControl + 1), pControl->Length);

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_eip_start() starts the EtherNet/IP stack. This function is called
 * when the applications signals "plugging complete".
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS | SDAI_ERR_COMMAND_PENDING
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_drv_eip_proto_start (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;


  _ASSERT_PTR (pSdaiBackendDriver);

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  pServiceData->ServiceType = SERVICE_REQ_RES_INIT;

  memcpy
  (
    &pServiceData->UseAs.Init.Device,
    &pSdaiBackendDriver->ApplicationInitData.Info,
    sizeof (pServiceData->UseAs.Init.Device)
  );

  memcpy
  (
    &pServiceData->UseAs.Init.Ident,
    &pSdaiBackendDriver->ApplicationInitData.Ident,
    sizeof (pServiceData->UseAs.Init.Ident)
  );

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_START_STACK);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_eip_plugging_complete() returns the plugging state of
 * modules.
 * @return
 * - type  : int
 * - values: 0, 1
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static int sdai_drv_eip_plugging_complete (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_SDAI_DRV_EIP*      pSdaiBackendDriverEIP;


  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverEIP = pSdaiBackendDriver->pPrivateData;

  return (pSdaiBackendDriverEIP->PlugComplete);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_eip_init_watchdog()
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS | SDAI_ERR_COM_NOT_SUPPORTED
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_drv_eip_init_watchdog (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const U16 WatchdogFactor)
{
  T_INTERFACE_MANAGEMENT_DATA*    pManagementData;
  U8                              Result = SDAI_ERR_COM_NOT_SUPPORTED;

  _TRACE (("sdai_drv_eip_init_watchdog"));

  _ASSERT_PTR (pSdaiBackendDriver);

  if (check_watchdog_supported () == SDAI_SUCCESS)
  {
    pManagementData = interface_lock_data (MANAGEMENT_INTERFACE);

    pManagementData->WatchdogFactor          = (WatchdogFactor * 100);
    pManagementData->WatchdogRunning         = 0;
    pManagementData->WatchdogFlagStack       = 0;
    pManagementData->WatchdogFlagApplication = 0;

    /* install the real trigger function */
    pSdaiBackendDriver->Interface.check_trigger_watchdog = sdai_drv_eip_check_and_trigger_watchdog;

    interface_unlock_data (MANAGEMENT_INTERFACE);
    interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_INIT_WATCHDOG);

    Result = SDAI_SUCCESS;
  }

  return (Result);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_eip_check_and_trigger_watchdog()
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS | #SDAI_ERR_WATCHDOG_EXPIRED
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_drv_eip_check_and_trigger_watchdog (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_INTERFACE_MANAGEMENT_DATA*    pManagementData;
  U8                              Result = SDAI_SUCCESS;


  _TRACE (("sdai_drv_eip_check_and_trigger_watchdog"));

  _ASSERT_PTR (pSdaiBackendDriver);
  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);

  pManagementData = interface_lock_data (MANAGEMENT_INTERFACE);

  /* Trigger WD */
  pManagementData->WatchdogFlagApplication = 0;

  if (pManagementData->WatchdogRunning == 1)
  {
    /* Check WD */
    pManagementData->WatchdogFlagStack += 1;

    if (pManagementData->WatchdogFlagStack > 2)
    {
      handle_stack_watchdog_expired ();
      Result = SDAI_ERR_WATCHDOG_EXPIRED;
    }
  }
  else
  {
    pManagementData->WatchdogRunning = 1;
  }

  interface_unlock_data (MANAGEMENT_INTERFACE);

  return (Result);
}


/*===========================================================================*/

/**
 * @desc The function sdai_drv_eip_lock_resources()
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS | #SDAI_ERR_INTERNAL
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] Resources Specifies which resources should be unlocked
 * - type : U16
 * - range: #SDAI_SHARED_RESOURCE_FLASH |
 *          #SDAI_SHARED_RESOURCE_MDIO
 */
static U8 sdai_drv_eip_lock_resources (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U16 Resource)
{
  _TRACE (("sdai_drv_eip_lock_resources"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);

  if (Resource == SDAI_SHARED_RESOURCE_FLASH)
  {
    if (interface_lock_data (RESOURCE_INTERFACE) != NULL)
    {
      return (SDAI_SUCCESS);
    }
  }
  else if (Resource == SDAI_SHARED_RESOURCE_MDIO)
  {
    if (interface_lock_data (SHARED_MDIO_INTERFACE) != NULL)
    {
      return (SDAI_SUCCESS);
    }
  }

  return (SDAI_ERR_INTERNAL);
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_eip_unlock_resources()
 * @return
 * - type  : U8
 * - values: #SDAI_SUCCESS
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] Resources Specifies which resources should be unlocked
 * - type : U16
 * - range: #SDAI_SHARED_RESOURCE_FLASH |
 *          #SDAI_SHARED_RESOURCE_MDIO
 */
static U8 sdai_drv_eip_unlock_resources (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U16 Resource)
{
  _TRACE (("sdai_drv_eip_unlock_resources"));

  _AVOID_UNUSED_WARNING (pSdaiBackendDriver);

  if (Resource == SDAI_SHARED_RESOURCE_FLASH)
  {
    interface_unlock_data (RESOURCE_INTERFACE);
  }
  else if (Resource == SDAI_SHARED_RESOURCE_MDIO)
  {
    interface_unlock_data (SHARED_MDIO_INTERFACE);
  }

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_eip_int_callback() handels the messages between the stack and
 * the application part of the SDAI.
 * @return
 * - type  : int
 * - values: 0
 * @param[in] Signal
 * - type : U16 int
 * - range: INTERFACE_SIGNAL_SERVICE_IND
 * @param[in] pContext
 * - type : VOID*
 * - range: whole address space
 */
static int sdai_drv_eip_handle_stack_signal (U16 Signal, void* pContext)
{
  T_SDAI_BACKEND_DRIVER*    pSdaiBackendDriver = pContext;


  if (Signal & INTERFACE_SIGNAL_SERVICE_IND)
  {
    sdai_drv_eip_handle_service_ind (pSdaiBackendDriver);
  }

  if (Signal & INTERFACE_SIGNAL_DATA_IND)
  {
    sdai_drv_eip_handle_data_ind (pSdaiBackendDriver);
  }

#ifdef SDAI_INCLUDE_SOCKET
  if (Signal & INTERFACE_SIGNAL_SOCKET_IND)
  {
    sdai_drv_sock_handle_socket_ind (Signal);
  }
#endif

  return 0;
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_eip_handle_data_ind() handels the incoming
 *       meassages in the data indication channel (Unit interface).
 * @return
 * - type  : VOID
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static void sdai_drv_eip_handle_data_ind (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_SDAI_DRV_EIP*     pSdaiBackendDriverEIP;
  T_INTERFACE_UNIT*   pUnit;
  U16                 UnitIndex;
  U16                 NumberChangedUnits = 0;


  _TRACE (("sdai_drv_eip_handle_data_ind"));

  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverEIP = pSdaiBackendDriver->pPrivateData;

  pUnit = interface_lock_unit (0);

  for (UnitIndex = 0; UnitIndex < pSdaiBackendDriverEIP->CurrentSlot; UnitIndex++)
  {
    if (pUnit->InternalFlags & UNIT_INTERNAL_FLAG_OUTPUT_CHANGED)
    {
      pUnit->InternalFlags &= ~UNIT_INTERNAL_FLAG_OUTPUT_CHANGED;

      OutputChangedInd.Id [NumberChangedUnits] = pUnit->Id;
      NumberChangedUnits++;
    }

    pUnit++;
  }

  interface_unlock_unit (0);

  if (NumberChangedUnits > 0)
  {
    OutputChangedInd.NumberUnits = NumberChangedUnits;

    if (pSdaiBackendDriver->ApplicationInitData.Callback.DataCbk != NULL)
    {
      pSdaiBackendDriver->ApplicationInitData.Callback.DataCbk (&OutputChangedInd);
    }
  }

  return;
}

/*===========================================================================*/

/**
 * The function sdai_drv_eip_handle_service_ind() handels the messages between the stack and
 * the application part of the SDAI.
 * @return
 * - type  : VOID
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static void sdai_drv_eip_handle_service_ind (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_SDAI_DRV_EIP*             pSdaiBackendDriverEIP;
  T_INTERFACE_SERVICE_IND*    pServiceData;


  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverEIP = pSdaiBackendDriver->pPrivateData;

  pServiceData = interface_lock_data (SERVICE_IND_INTERFACE);

  if (pServiceData != NULL)
  {
    switch (pServiceData->ServiceType)
    {
      case SERVICE_IND_EXCEPTION:
      {
        if (pSdaiBackendDriver->ApplicationInitData.Callback.ExceptionCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.ExceptionCbk (&pServiceData->UseAs.Exception);
        }

        pSdaiBackendDriverEIP->ExceptionPending = 1;
        break;
      }

      case SERVICE_IND_WRITE:
      {
        if (pSdaiBackendDriver->ApplicationInitData.Callback.WriteReqCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.WriteReqCbk (&pServiceData->UseAs.Write);
        }

        break;
      }

      case SERVICE_IND_READ:
      {
        if (pSdaiBackendDriver->ApplicationInitData.Callback.ReadReqCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.ReadReqCbk (&pServiceData->UseAs.Read);
        }

        break;
      }

      case SERVICE_IND_IDENT:
      {
        if (pSdaiBackendDriver->ApplicationInitData.Callback.IdentDataCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.IdentDataCbk (&pServiceData->UseAs.Ident);
        }

        break;
      }

      case SERVICE_IND_CONTROL:
      {
        if (pSdaiBackendDriver->ApplicationInitData.Callback.ControlReqCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.ControlReqCbk (&pServiceData->UseAs.Control);
        }

        break;
      }
    }

    pServiceData->ServiceType = SERVICE_IND_FREE;
  }

  interface_unlock_data (SERVICE_IND_INTERFACE);

  return;
}

#endif  /* SDAI_INCLUDE_EIP */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
