/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

#include <string.h>

#include "sdai.h"

#include "cfg_cc.h"

#include "sdai_ext.h"
#include "sdai_interface.h"

#ifdef SDAI_INCLUDE_SOCKET
  #include "sdai_socket.h"
  #include "socket_interface.h"
#endif

#include "sdai_drv.h"

#include "porting.h"

#ifdef SDAI_INCLUDE_ECAT

/******************************************************************************
DEFINES
******************************************************************************/

/******************************************************************************
TYPEDEFS
******************************************************************************/

typedef struct _T_SDAI_DRV_ECAT
{
  int                         ShutdownPending;              /**< indicates a pending shutdown command */
  int                         ExceptionPending;             /**< indicates a pending exception */

  U16                         HighestUnitIndex;             /**< The highest used unit index */
  U16                         DefaultTxPdoIndex;            /**< the next free default TxPDO index number */
  U16                         DefaultRxPdoIndex;            /**< the next free default RxPDO index number */
  struct SDAI_ECAT_CFG_DATA   EcatCfgData [SDAI_MAX_UNITS]; /**< ECAT specific configuration data for each unit */

  U8*                         pIODataBase;                  /**< */

  int                         PlugComplete;                 /**< user signaled plugging complete */

} T_SDAI_DRV_ECAT;

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

static U8     sdai_drv_ecat_deinit                    (T_SDAI_BACKEND_DRIVER*);
static U8     sdai_drv_ecat_get_version               (T_SDAI_BACKEND_DRIVER*, struct SDAI_VERSION_DATA*);
static U8     sdai_drv_ecat_get_state                 (T_SDAI_BACKEND_DRIVER*, U16*, struct SDAI_ADDSTATUS_DATA*);
static U8     sdai_drv_ecat_plug_unit                 (T_SDAI_BACKEND_DRIVER*, U8, U8, U8, U32*, const struct SDAI_CFG_DATA_EXT*);
static U8     sdai_drv_ecat_pull_unit                 (T_SDAI_BACKEND_DRIVER*, U32);

static U8     sdai_drv_ecat_get_data                  (T_SDAI_BACKEND_DRIVER*, U32, U8*, U8*);
static U8     sdai_drv_ecat_set_data                  (T_SDAI_BACKEND_DRIVER*, U32, U8, const U8*);

static U8     sdai_drv_ecat_write_response            (T_SDAI_BACKEND_DRIVER*, const struct SDAI_WRITE_RES*);
static U8     sdai_drv_ecat_read_response             (T_SDAI_BACKEND_DRIVER*, const struct SDAI_READ_RES*);
static U8     sdai_drv_ecat_control_response          (T_SDAI_BACKEND_DRIVER*, const struct SDAI_CONTROL_RES*);

static U8     sdai_drv_ecat_proto_start               (T_SDAI_BACKEND_DRIVER*);
static int    sdai_drv_ecat_plugging_complete         (T_SDAI_BACKEND_DRIVER*);

static int    sdai_drv_ecat_handle_stack_signal       (U16, void*);
static int    sdai_drv_ecat_handle_synch_signal       (U8, void*);
static void   sdai_drv_ecat_handle_service_ind        (T_SDAI_BACKEND_DRIVER*);
static void   sdai_drv_ecat_handle_data_ind           (T_SDAI_BACKEND_DRIVER*, U16 Signal);
static U8     sdai_drv_ecat_diagnosis_request         (T_SDAI_BACKEND_DRIVER*, const struct SDAI_DIAGNOSIS_REQ*);

/******************************************************************************
PUBLIC DATA
******************************************************************************/

/******************************************************************************
PRIVATE DATA
******************************************************************************/

static T_SDAI_DRV_ECAT                  SdaiBackendDriverECAT;      /**< local instance of ECAT driver struct */
static struct SDAI_OUTPUT_CHANGED_IND   OutputChangedInd;           /**< holds informations of changed units for data cbk */
T_INTERFACE_SERVICE_IND                 ServiceIndBuffer;           /**< temporary buffer for service indications */
static struct SDAI_SYNC_SIGNAL          SyncSignalBuffer;           /**< temporary buffer for synch signals */

/******************************************************************************
PUBLIC FUNCTIONS
******************************************************************************/

/******************************************************************************
PROTECTED FUNCTIONS
******************************************************************************/

/**
 * The function sdai_ecat_init() initializes the EtherCAT backend.
 * @return
 * - type  : struct SDAI_BACKEND_DRV*
 * - values: whole address space
 * @param[in] InitData
 * - type : SDAI_INIT*
 * - range: whole address space
 */
U8 sdai_drv_ecat_init (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, struct SDAI_INIT* pApplicationInitData)
{
  T_INTERFACE_MANAGEMENT_DATA*    pManagementData;
  U8                              Result = SDAI_ERR_INTERNAL;


  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pApplicationInitData);

  if (pApplicationInitData->Ident.UseAs.Ecat.Flags & SDAI_ECAT_ENABLE_EOE)
  {
    U32   IpAddr;
    U32   Netmask;
    U32   Gateway;

    memcpy ((U8*) &IpAddr,  pApplicationInitData->Ident.UseAs.Ecat.Address, sizeof (IpAddr));
    memcpy ((U8*) &Netmask, pApplicationInitData->Ident.UseAs.Ecat.Netmask, sizeof (Netmask));
    memcpy ((U8*) &Gateway, pApplicationInitData->Ident.UseAs.Ecat.Gateway, sizeof (Gateway));

    IpAddr  = convert_network_u32_to_processor_u32 (IpAddr);
    Netmask = convert_network_u32_to_processor_u32 (Netmask);
    Gateway = convert_network_u32_to_processor_u32 (Gateway);

    /* check the passed network settings */
    if (sdai_check_ip_settings (IpAddr, Netmask, Gateway) != IP_SETTINGS_OK)
    {
      return (SDAI_ERR_INVALID_ARGUMENT);
    }
  }

  /* If signals SYNC0 and/or SYNC1 shall be supported a user callback must be available to process
     these synchronization signals.
   */
  if (pApplicationInitData->Info.UseAs.Ecat.SyncFlags & (SDAI_ECAT_SYNC0_SUPPORTED | SDAI_ECAT_SYNC1_SUPPORTED))
  {
    if (pApplicationInitData->Callback.SyncSignalCbk == NULL)
    {
      return (SDAI_ERR_INVALID_ARGUMENT);
    }
  }

  /* @brief The interface of the driver is initialized with default function. So
   * we have to overwrite only functions we support in the EtherCAT implementation.
   */
  pSdaiBackendDriver->Interface.deinit             = sdai_drv_ecat_deinit;
  pSdaiBackendDriver->Interface.get_version        = sdai_drv_ecat_get_version;
  pSdaiBackendDriver->Interface.get_state          = sdai_drv_ecat_get_state;
  pSdaiBackendDriver->Interface.plug_unit          = sdai_drv_ecat_plug_unit;
  pSdaiBackendDriver->Interface.pull_unit          = sdai_drv_ecat_pull_unit;

  pSdaiBackendDriver->Interface.get_data           = sdai_drv_ecat_get_data;
  pSdaiBackendDriver->Interface.set_data           = sdai_drv_ecat_set_data;

  pSdaiBackendDriver->Interface.write_response     = sdai_drv_ecat_write_response;
  pSdaiBackendDriver->Interface.read_response      = sdai_drv_ecat_read_response;
  pSdaiBackendDriver->Interface.control_response   = sdai_drv_ecat_control_response;

  pSdaiBackendDriver->Interface.proto_start        = sdai_drv_ecat_proto_start;
  pSdaiBackendDriver->Interface.plugging_complete  = sdai_drv_ecat_plugging_complete;

  pSdaiBackendDriver->Interface.diagnosis_request  = sdai_drv_ecat_diagnosis_request;

  pSdaiBackendDriver->pPrivateData                 = &SdaiBackendDriverECAT;

  /*-------------------------------------------------------------------------*/

  memset (SdaiBackendDriverECAT.EcatCfgData, 0, sizeof (SdaiBackendDriverECAT.EcatCfgData));
  memset (&ServiceIndBuffer, 0, sizeof (ServiceIndBuffer));

  SdaiBackendDriverECAT.PlugComplete      = 0;
  SdaiBackendDriverECAT.ShutdownPending   = 0u;
  SdaiBackendDriverECAT.HighestUnitIndex  = 0;
  SdaiBackendDriverECAT.DefaultTxPdoIndex = SDAI_ECAT_MIN_TXPDO_INDEX;
  SdaiBackendDriverECAT.DefaultRxPdoIndex = SDAI_ECAT_MIN_RXPDO_INDEX;

  Result = interface_init
  (
    sdai_drv_ecat_handle_stack_signal,
    sdai_drv_ecat_handle_synch_signal,
    INTERFACE_APPLICATION,
    pSdaiBackendDriver,
    SDAI_BACKEND_ECAT
  );

  if (Result == SDAI_SUCCESS)
  {
    SdaiBackendDriverECAT.pIODataBase = interface_get_iodata_base ();

    pManagementData = interface_lock_data (MANAGEMENT_INTERFACE);

    if (pManagementData != NULL)
    {
      pManagementData->SupportedCallbacks = 0u;

      if (pApplicationInitData->Callback.DataCbk != NULL)       { pManagementData->SupportedCallbacks |= DATA_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.WriteReqCbk != NULL)   { pManagementData->SupportedCallbacks |= WRITE_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.ReadReqCbk != NULL)    { pManagementData->SupportedCallbacks |= READ_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.ControlReqCbk != NULL) { pManagementData->SupportedCallbacks |= CONTROL_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.ExceptionCbk != NULL)  { pManagementData->SupportedCallbacks |= EXCEPTION_CALLBACK_SUPPORTED; }
      if (pApplicationInitData->Callback.SyncSignalCbk != NULL) { pManagementData->SupportedCallbacks |= SYNC_SIGNAL_CALLBACK_SUPPORTED; }

      pManagementData->SupportedCallbacks &= SDAI_INTERFACE_ALLOWED_CALLBACK_MASK;

      pManagementData->BackEnd   = SDAI_BACKEND_ECAT;
      pManagementData->ApplReady = SDAI_INTERFACE_APPLICATION_READY;
    }

    interface_unlock_data (MANAGEMENT_INTERFACE);
  }

  return (Result);
}

/******************************************************************************
PRIVATE FUNCTIONS
******************************************************************************/

/**
 * The function sdai_drv_ecat_deinit() stops the EtherCAT Slave Controller and
 * frees the data structure.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS
 */
static U8 sdai_drv_ecat_deinit (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_SDAI_DRV_ECAT*    pSdaiBackendDriverECAT;


  pSdaiBackendDriverECAT = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverECAT->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pSdaiBackendDriverECAT->ShutdownPending = 1;

  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_STOP_STACK);

  /* wait for backend shutting down */
  interface_deinit (INTERFACE_APPLICATION);

  pSdaiBackendDriver->pPrivateData = NULL;

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_ecat_get_version() returns the version string of the
 * EtherCAT slave controller, SDAI and EtherCAT mapping layer.
 * @return
 * - type  : char*
 * - values: ???
 */
static U8 sdai_drv_ecat_get_version (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, struct SDAI_VERSION_DATA* pVersionData)
{
  T_SDAI_DRV_ECAT*             pSdaiBackendDriverECAT;
  T_INTERFACE_STATUS_DATA*     pStatusData;


  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pVersionData);

  pSdaiBackendDriverECAT = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverECAT->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  pStatusData = interface_lock_data (STATUS_INTERFACE);
  memcpy (pVersionData, &pStatusData->VersionData, sizeof(struct SDAI_VERSION_DATA));
  interface_unlock_data (STATUS_INTERFACE);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_ecat_get_state() returns the current state of the EtherCAT
 * slave.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY
 * @pre [enter precondition here]
 * @post [enter postcondition here]
 * @remarks [enter remarks, i.e. compatibility issues, here]
 */
static U8 sdai_drv_ecat_get_state (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U16* pState, struct SDAI_ADDSTATUS_DATA* pAddStatus)
{
  T_SDAI_DRV_ECAT*             pSdaiBackendDriverECAT;
  T_INTERFACE_STATUS_DATA*     pStatusData;


  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pState);

  pSdaiBackendDriverECAT = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverECAT->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pStatusData = interface_lock_data (STATUS_INTERFACE);

  *pState = pStatusData->StackStatus;

  if (pAddStatus != NULL)
  {
    memcpy (pAddStatus, &pStatusData->StackAddStatus, sizeof (struct SDAI_ADDSTATUS_DATA));
  }

  interface_unlock_data (STATUS_INTERFACE);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_ecat_plug_unit() makes the EtherCAT specific part of
 * plugging.
 * It calculates the ID for the unit and returns it to the application.
 * @return
 * - type  : U32
 * - values: ???
 * @param[in] UnitType
 * - type : U8
 * - range: SDAI_UNIT_TYPE_INPUT | SDAI_UNIT_TYPE_OUTPUT
 * @param[in] InputSize defines the input data length of the unit in bytes.
 * - type : U8
 * - range: 0 to 255
 * @param[in] OutputSize defines the output data length of the unit in bytes.
 * - type : U8
 * - range: 0 to 255
 * @param[out] pUnit
 * - type : SDAI_UNIT *
 * - range: whole address space
 */
static U8 sdai_drv_ecat_plug_unit (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U8 UnitType, U8 InputSize, U8 OutputSize, U32* pId, const struct SDAI_CFG_DATA_EXT* pCfgData)
{
  T_SDAI_DRV_ECAT*                pSdaiBackendDriverECAT;
  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  T_INTERFACE_UNIT*               pUnit;
  U16                             UnitIndex = 0xFFFF;
  U16                             PdoIndex  = 0;

  volatile U16                    ServiceType;


  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverECAT = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverECAT->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  /* if users signals plugging complete we don't want to check the args */
  if(UnitType == SDAI_UNIT_TYPE_PLUGGING_COMPLETE)
  {
    pSdaiBackendDriverECAT->PlugComplete = 1;

    return (pSdaiBackendDriver->Interface.proto_start (pSdaiBackendDriver));
  }

  /*-------------------------------------------------------------------------*/

  if (pSdaiBackendDriver->ApplicationInitData.Info.Flags & SDAI_DYNAMIC_IO_CONFIGURATION)
  {
    if (*pId != SDAI_INVALID_ID)
    {
      UnitIndex = interface_find_unit (*pId, pSdaiBackendDriverECAT->HighestUnitIndex);

      if (UnitIndex == 0xFFFF)
      {
        return (SDAI_ERR_NOSUCHUNIT);
      }
    }
    else
    {
      UnitIndex = interface_find_free_unit (SDAI_MAX_UNITS);

      if (UnitIndex == 0xFFFF)
      {
        return (SDAI_ERR_MAX_UNIT_REACHED);
      }
    }
  }
  else
  {
    *pId = SDAI_INVALID_ID;
  }

  /*-------------------------------------------------------------------------*/

  if ( (UnitType != SDAI_UNIT_TYPE_GENERIC_INPUT)  &&
       (UnitType != SDAI_UNIT_TYPE_GENERIC_OUTPUT) )
  {
    return (SDAI_ERR_UNKNOWN_UNIT_TYPE);
  }

  if (pCfgData != NULL)
  {
    PdoIndex = pCfgData->UseAs.Ecat.Index;

    if ( (UnitType == SDAI_UNIT_TYPE_GENERIC_INPUT) &&
         ((PdoIndex < SDAI_ECAT_MIN_TXPDO_INDEX) ||
          (PdoIndex > SDAI_ECAT_MAX_TXPDO_INDEX) )  )
    {
      return (SDAI_ERR_INVALID_ARGUMENT);
    }

    if ( (UnitType == SDAI_UNIT_TYPE_GENERIC_OUTPUT) &&
         ((PdoIndex < SDAI_ECAT_MIN_RXPDO_INDEX) ||
          (PdoIndex > SDAI_ECAT_MAX_RXPDO_INDEX) )   )
    {
      return (SDAI_ERR_INVALID_ARGUMENT);
    }

    if (pCfgData->UseAs.Ecat.NumberMappedObjects > SDAI_ECAT_MAX_NUMBER_MAPPED_OBJECTS)
    {
      return (SDAI_ERR_INVALID_ARGUMENT);
    }

    if ( !(pSdaiBackendDriver->ApplicationInitData.Info.Flags & SDAI_DYNAMIC_IO_CONFIGURATION) &&
         (pCfgData->UseAs.Ecat.Flags & SDAI_UNIT_FLAG_DYNAMIC_PDO_MAPPING)                     )
    {
      return (SDAI_ERR_INVALID_ARGUMENT);
    }
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  pUnit = interface_get_free_unit (InputSize, OutputSize, &UnitIndex);

  if (pUnit != NULL)
  {
    _ASSERT ((pUnit->DataLengthInput == InputSize) && (pUnit->DataLengthOutput == OutputSize));
    _ASSERT (((pUnit->DataInputOffset != 0xFFFF) && (InputSize > 0)) ||
             ((pUnit->DataOutputOffset != 0xFFFF) && (OutputSize > 0)));

    pServiceData->ServiceType = SERVICE_REQ_RES_PLUG_UNIT;

    /* copy the EtherCAT specific configuration
     */
    if (pCfgData != NULL)
    {
      memcpy
      (
        &pServiceData->UseAs.PlugUnit.CfgData.UseAs.Ecat,
        &pCfgData->UseAs.Ecat,
        sizeof (pServiceData->UseAs.PlugUnit.CfgData.UseAs.Ecat)
      );
    }
    else
    {
      /* set default PDO index number
       */
      if (UnitType == SDAI_UNIT_TYPE_GENERIC_INPUT)
      {
        _ASSERT (pSdaiBackendDriverECAT->DefaultTxPdoIndex <= SDAI_ECAT_MAX_TXPDO_INDEX);

        PdoIndex = pSdaiBackendDriverECAT->DefaultTxPdoIndex;
        pSdaiBackendDriverECAT->DefaultTxPdoIndex += 1u;
      }
      else
      {
        _ASSERT (UnitType == SDAI_UNIT_TYPE_GENERIC_OUTPUT);
        _ASSERT (pSdaiBackendDriverECAT->DefaultRxPdoIndex <= SDAI_ECAT_MAX_RXPDO_INDEX);

        PdoIndex = pSdaiBackendDriverECAT->DefaultRxPdoIndex;
        pSdaiBackendDriverECAT->DefaultRxPdoIndex += 1u;
      }

      /* no application objects to map */
      pServiceData->UseAs.PlugUnit.CfgData.UseAs.Ecat.NumberMappedObjects = 0;
      pServiceData->UseAs.PlugUnit.CfgData.UseAs.Ecat.Index               = PdoIndex;
    }

    pServiceData->UseAs.PlugUnit.UnitIndex = UnitIndex;

    pUnit->Id                = _SDAI_ECAT_UNIT_INDEX_AND_OBJ_INDEX_TO_ID (UnitIndex, PdoIndex);
    pUnit->Type              = UnitType;
    pUnit->InternalFlags     = (pCfgData->InterfaceType & SDAI_INTERFACE_MASK) | UNIT_INTERNAL_FLAG_IN_USE;

    pSdaiBackendDriverECAT->HighestUnitIndex = _MAX(pSdaiBackendDriverECAT->HighestUnitIndex, UnitIndex);
    *pId                                     = pUnit->Id;

    /* interface_lock_unit is already called by interface_get_free_unit */
    interface_unlock_unit (_SDAI_ECAT_ID_TO_UNIT_INDEX (U16, pUnit->Id));

    /* Send the EtherCAT specific configuration to the stack */
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

    /* Wait until service data are taken over by the stack */
    do
    {
      pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

      if (pServiceData == NULL)
      {
        interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
        return (SDAI_ERR_INTERNAL);
      }

      ServiceType = (volatile U16) pServiceData->ServiceType;
      interface_unlock_data (SERVICE_REQ_RES_INTERFACE);

    } while (ServiceType != SERVICE_REQ_RES_FREE);

    return (SDAI_SUCCESS);
  }
  else
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  }

  return (SDAI_ERR_MAX_UNIT_REACHED);
}

/*===========================================================================*/

/**
 * The function sdai_drv_ecat_pull_unit()
 * @return
 * - type  : U8
 * - values: ???
 * @param[in] U32
 * - type : U32
 * - range: whole range
 */
static U8 sdai_drv_ecat_pull_unit (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U32 Id)
{
  T_SDAI_DRV_ECAT*                pSdaiBackendDriverECAT;
  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  T_INTERFACE_UNIT*               pUnit;

  volatile U16                    ServiceType;
  U16                             UnitIndex;


  _TRACE (("sdai_drv_ecat_pull_unit"));

  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverECAT = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverECAT->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  UnitIndex = interface_find_unit (Id, pSdaiBackendDriverECAT->HighestUnitIndex);

  if (UnitIndex == 0xFFFF)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_NOSUCHUNIT);
  }

  pUnit = interface_lock_unit (UnitIndex);
  pUnit->InternalFlags = 0;

  if (UnitIndex == pSdaiBackendDriverECAT->HighestUnitIndex)
  {
    do
    {
      pSdaiBackendDriverECAT->HighestUnitIndex--;
      pUnit--;

    } while ( (pSdaiBackendDriverECAT->HighestUnitIndex > 0)      &&
              !(pUnit->InternalFlags & UNIT_INTERNAL_FLAG_IN_USE) );
  }

  interface_unlock_unit (UnitIndex);

  pServiceData->ServiceType = SERVICE_REQ_RES_PULL_UNIT;

  pServiceData->UseAs.PullUnit.UnitIndex = UnitIndex;

  /* Send the EtherCAT specific configuration to the stack */
  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  /* Wait until service data are taken over by the stack */
  do
  {
    pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

    if (pServiceData == NULL)
    {
      interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
      return (SDAI_ERR_INTERNAL);
    }

    ServiceType = (volatile U16) pServiceData->ServiceType;
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);

  } while (ServiceType != SERVICE_REQ_RES_FREE);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_ecat_pull_unit()
 * @return
 * - type  : U8
 * - values: ???
 * @param[in] U32
 * - type : U32
 * - range: whole range
 */
static U8 sdai_drv_ecat_diagnosis_request (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_DIAGNOSIS_REQ* pDiagnosis)
{
  T_SDAI_DRV_ECAT*                pSdaiBackendDriverECAT;
  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;

  volatile U16                    ServiceType;


  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverECAT = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverECAT->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  pServiceData->ServiceType = SERVICE_REQ_RES_DIAGNOSIS;
  memcpy (&pServiceData->UseAs.Diagnosis, pDiagnosis, (sizeof (struct SDAI_DIAGNOSIS_REQ) + pDiagnosis->Length));

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  /* Wait until service data are taken over by the stack */
  do
  {
    pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

    if (pServiceData == NULL)
    {
      interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
      return (SDAI_ERR_INTERNAL);
    }

    ServiceType = (volatile U16) pServiceData->ServiceType;
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);

  } while (ServiceType != SERVICE_REQ_RES_FREE);

  return (SDAI_SUCCESS);
}



/*===========================================================================*/

/**
 * The function sdai_drv_ecat_get_data() gets the current output or input data
 * for the unit defined by Id. This function can be used on input and output units.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS | SDAI_ERR_NOSUCHUNIT
 * @param[in] Id
 * - type : U32
 * - range: ???
 * @param[out] pStatus
 * - type : U8*
 * - range: whole address space
 * @param[out] pData
 * - type : U8*
 * - range: whole address space
 */
static U8 sdai_drv_ecat_get_data (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U32 Id, U8* pStatus, U8* pData)
{
  T_SDAI_DRV_ECAT*    pSdaiBackendDriverECAT;
  T_INTERFACE_UNIT*   pUnit;
  U16                 UnitIndex;


  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverECAT = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverECAT->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  UnitIndex = _SDAI_ECAT_ID_TO_UNIT_INDEX (U16, Id);
  pUnit     = interface_lock_unit (UnitIndex);

  if (pUnit == NULL)
  {
    interface_unlock_unit (UnitIndex);
    *pStatus = SDAI_DATA_STATUS_INVALID;
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (Id != pUnit->Id)
  {
    interface_unlock_unit (UnitIndex);
    *pStatus = SDAI_DATA_STATUS_INVALID;
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (pUnit->DataLengthOutput > 0u)
  {
    *pStatus = pUnit->StatusOutput;

    _ASSERT (pUnit->DataOutputOffset != 0xFFFF);
    memcpy (pData, &pSdaiBackendDriverECAT->pIODataBase [pUnit->DataOutputOffset], pUnit->DataLengthOutput);
  }
  else if (pUnit->DataLengthInput > 0u)
  {
    *pStatus = pUnit->StatusInput;

    _ASSERT (pUnit->DataInputOffset != 0xFFFF);
    memcpy (pData, &pSdaiBackendDriverECAT->pIODataBase [pUnit->DataInputOffset], pUnit->DataLengthInput);
  }
  else
  {
    /* Unit has no outputs or inputs configured.
     */
    *pStatus = SDAI_DATA_STATUS_INVALID;
  }

  interface_unlock_unit (UnitIndex);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_ecat_set_data() sets the input data for the unit defined by
 * Id. This function can only be used on input units.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS | SDAI_ERR_NOSUCHUNIT
 * @param[in] Id
 * - type : U32
 * - range: ???
 * @param[in] Status
 * - type : U8
 * - range: ???
 * @param[in] pData
 * - type : U8*
 * - range: whole address space
 */
static U8 sdai_drv_ecat_set_data (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U32 Id, U8 Status, const U8* pData)
{
  T_SDAI_DRV_ECAT*    pSdaiBackendDriverECAT;
  T_INTERFACE_UNIT*   pUnit;
  U16                 UnitIndex;


  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverECAT = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverECAT->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  if (pData == NULL)
  {
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  /*-------------------------------------------------------------------------*/

  UnitIndex = _SDAI_ECAT_ID_TO_UNIT_INDEX (U16, Id);
  pUnit     = interface_lock_unit (UnitIndex);

  if (pUnit == NULL)
  {
    interface_unlock_unit (UnitIndex);
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (Id != pUnit->Id)
  {
    interface_unlock_unit (UnitIndex);
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (pUnit->InternalFlags & SDAI_INTERFACE_MASK)
  {
    interface_unlock_unit (UnitIndex);
    return (SDAI_ERR_NOSUCHUNIT);
  }

  if (pUnit->DataLengthInput > 0u)
  {
    pUnit->StatusInput = (Status == SDAI_DATA_STATUS_VALID) ? SDAI_DATA_STATUS_VALID : SDAI_DATA_STATUS_INVALID;

    _ASSERT (pUnit->DataInputOffset != 0xFFFF);
    memcpy (&pSdaiBackendDriverECAT->pIODataBase [pUnit->DataInputOffset], pData, pUnit->DataLengthInput);
  }

  interface_unlock_unit (UnitIndex);

  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_UPDATE_INPUT_DATA);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_ecat_write_response() handles the EtherCAT specific
 * part of the write response. It checks the parameters, copies the data to the
 * shared ram and sends a signal to the stack.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY |
 *           SDAI_ERR_SERVICE_PENDING |
 *           SDAI_ERROR
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pWrite
 * - type : const struct SDAI_WRITE_RES*
 * - range: whole address space
 * @remarks
 */
static U8 sdai_drv_ecat_write_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_WRITE_RES* pWrite)
{
  T_SDAI_DRV_ECAT*                pSdaiBackendDriverECAT;

  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  struct SDAI_WRITE_RES*          pServiceWrite;


  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pWrite);

  pSdaiBackendDriverECAT = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverECAT->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  pServiceWrite = &pServiceData->UseAs.Write;

  pServiceData->ServiceType = SERVICE_REQ_RES_WRITE;
  pServiceWrite->ErrorCode  = pWrite->ErrorCode;

  memcpy (&pServiceWrite->UseAs.Ecat, &pWrite->UseAs.Ecat, sizeof (pServiceWrite->UseAs.Ecat));

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_ecat_read_response() handles the EtherCAT specific
 * part of the read response. It checks the parameters, copies the data to the
 * shared ram and sends a signal to the stack.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY |
 *           SDAI_ERR_SERVICE_PENDING |
 *           SDAI_ERROR
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pRead
 * - type : const struct SDAI_READ_RES*
 * - range: whole address space
 */
static U8 sdai_drv_ecat_read_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_READ_RES* pRead)
{
  T_SDAI_DRV_ECAT*                pSdaiBackendDriverECAT;

  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  struct SDAI_READ_RES*           pServiceRead;


  _ASSERT_PTR (pSdaiBackendDriver);
  _ASSERT_PTR (pRead);

  pSdaiBackendDriverECAT = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverECAT->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  pServiceRead = &pServiceData->UseAs.Read;

  pServiceData->ServiceType = SERVICE_REQ_RES_READ;
  pServiceRead->ErrorCode   = pRead->ErrorCode;
  pServiceRead->Length      = pRead->Length;

  memcpy (&pServiceRead->UseAs.Ecat, &pRead->UseAs.Ecat, sizeof (pServiceRead->UseAs.Ecat));
  memcpy ((pServiceRead + 1), (pRead + 1), pRead->Length);

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_ecat_control_response() handles the EtherCAT specific
 * part of the control response. It checks the parameters, copies the data to the
 * shared ram and sends a signal to the stack.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS |
 *           SDAI_ERR_STACK_NOT_RDY |
 *           SDAI_ERR_SERVICE_PENDING |
 *           SDAI_ERROR
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 * @param[in] pControl
 * - type : const struct SDAI_CONTROL_RES*
 * - range: whole address space
 */
static U8 sdai_drv_ecat_control_response (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, const struct SDAI_CONTROL_RES* pControl)
{
  T_SDAI_DRV_ECAT*                pSdaiBackendDriverECAT;

  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;
  struct SDAI_CONTROL_RES*        pServiceControl;


  _ASSERT_PTR (pControl);
  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverECAT = pSdaiBackendDriver->pPrivateData;

  if (pSdaiBackendDriverECAT->ShutdownPending)
  {
    return (SDAI_ERR_STACK_NOT_RDY);
  }

  /*-------------------------------------------------------------------------*/

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  /*-------------------------------------------------------------------------*/

  if ((pControl->ControlCode != SDAI_ECAT_CONTROL_FILE_OPEN_REQ)  &&
      (pControl->ControlCode != SDAI_ECAT_CONTROL_FILE_CLOSE_REQ) &&
      (pControl->ControlCode != SDAI_CONTROL_CODE_CFG_DATA_INFO)  )
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INVALID_ARGUMENT);
  }

  pServiceControl = &pServiceData->UseAs.Control;

  pServiceData->ServiceType    = SERVICE_REQ_RES_CONTROL;
  pServiceControl->ControlCode = pControl->ControlCode;
  pServiceControl->ErrorCode   = pControl->ErrorCode;
  pServiceControl->Length      = 0;

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_SERVICE_REQ_RES);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_ecat_start() starts the EtherCAT backend. This function is called
 * when the applications signals "plugging complete".
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS | SDAI_ERR_COMMAND_PENDING
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static U8 sdai_drv_ecat_proto_start (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_INTERFACE_SERVICE_REQ_RES*    pServiceData;


  _ASSERT_PTR (pSdaiBackendDriver);

  pServiceData = interface_lock_data (SERVICE_REQ_RES_INTERFACE);

  if (pServiceData == NULL)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_INTERNAL);
  }

  if (pServiceData->ServiceType != SERVICE_REQ_RES_FREE)
  {
    interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
    return (SDAI_ERR_SERVICE_PENDING);
  }

  pServiceData->ServiceType = SERVICE_REQ_RES_INIT;

  memcpy
  (
    &pServiceData->UseAs.Init.Device,
    &pSdaiBackendDriver->ApplicationInitData.Info,
    sizeof (pServiceData->UseAs.Init.Device)
  );

  memcpy
  (
    &pServiceData->UseAs.Init.Ident,
    &pSdaiBackendDriver->ApplicationInitData.Ident,
    sizeof (pServiceData->UseAs.Init.Ident)
  );

  interface_unlock_data (SERVICE_REQ_RES_INTERFACE);
  interface_send_signal (INTERFACE_APPLICATION, INTERFACE_SIGNAL_START_STACK);

  return (SDAI_SUCCESS);
}

/*===========================================================================*/

/**
 * The function sdai_drv_ecat_plugging_complete() returns the plugging state of
 * modules.
 * @return
 * - type  : int
 * - values: 0, 1
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static int sdai_drv_ecat_plugging_complete (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_SDAI_DRV_ECAT*      pSdaiBackendDriverECAT;


  _ASSERT_PTR (pSdaiBackendDriver);

  pSdaiBackendDriverECAT = pSdaiBackendDriver->pPrivateData;

  return (pSdaiBackendDriverECAT->PlugComplete);
}

/*===========================================================================*/

/**
 * The function sdai_drv_ecat_int_callback() handels the messages between the stack and
 * the application part of the SDAI.
 * @return
 * - type  : int
 * - values: 0
 * @param[in] Signal
 * - type : U16 int
 * - range: INTERFACE_SIGNAL_SERVICE_IND | INTERFACE_SIGNAL_DATA_IND
 * @param[in] pContext
 * - type : VOID*
 * - range: whole address space
 */
static int sdai_drv_ecat_handle_stack_signal (U16 Signal, void* pContext)
{
  T_SDAI_BACKEND_DRIVER*    pSdaiBackendDriver = pContext;


  if (Signal & INTERFACE_SIGNAL_SERVICE_IND)
  {
    sdai_drv_ecat_handle_service_ind (pSdaiBackendDriver);
  }

  if ( (Signal & INTERFACE_SIGNAL_DATA_IND   ) ||
       (Signal & INTERFACE_SIGNAL_DATA_UPDATE) )
  {
    sdai_drv_ecat_handle_data_ind (pSdaiBackendDriver, Signal);
  }

#ifdef SDAI_INCLUDE_SOCKET
  if (Signal & INTERFACE_SIGNAL_SOCKET_IND)
  {
    sdai_drv_sock_handle_socket_ind (Signal);
  }
#endif

  return 0;
}

/*===========================================================================*/

/**
 * The function sdai_drv_ecat_handle_synch_signal() handles the SYNCH interrupt signal.
 * The respective callback of the SDAI application will be invoked.
 * the application part of the SDAI.
 * @return
 * - type  : int
 * - values: 0
 * @param[in] Signal
 * - type : U16 int
 * - range: INTERFACE_SIGNAL_SERVICE_IND
 * @param[in] pContext
 * - type : VOID*
 * - range: whole address space
 */
static int sdai_drv_ecat_handle_synch_signal (U8 Signal, void* pContext)
{
  T_SDAI_BACKEND_DRIVER*    pSdaiBackendDriver = pContext;


  _ASSERT ( (Signal & SDAI_SYNC_SIGNAL_0) || (Signal & SDAI_SYNC_SIGNAL_1) );

  if (pSdaiBackendDriver->ApplicationInitData.Callback.SyncSignalCbk != NULL)
  {
    SyncSignalBuffer.UseAs.Ecat.SyncStatus = 0;

    if (Signal & SDAI_SYNC_SIGNAL_0)
    {
      SyncSignalBuffer.UseAs.Ecat.SyncStatus |= SDAI_ECAT_SYNC0_ACTIVE;
    }

    if (Signal & SDAI_SYNC_SIGNAL_1)
    {
      SyncSignalBuffer.UseAs.Ecat.SyncStatus |= SDAI_ECAT_SYNC1_ACTIVE;
    }

    pSdaiBackendDriver->ApplicationInitData.Callback.SyncSignalCbk (&SyncSignalBuffer);
  }

  return 0;
}

/*===========================================================================*/

/**
 * @desc The function sdai_drv_ecat_handle_data_ind() handels the incoming
 *       meassages in the data indication channel (Unit interface).
 * @return
 * - type  : VOID
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static void sdai_drv_ecat_handle_data_ind (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver, U16 Signal)
{
  T_INTERFACE_UNIT*   pUnit;
  U16                 UnitIndex;
  U16                 NumberChangedUnits = 0;
  T_SDAI_DRV_ECAT*    pSdaiBackendDriverECAT;


  _TRACE (("sdai_drv_ecat_handle_data_ind"));

  pSdaiBackendDriverECAT = pSdaiBackendDriver->pPrivateData;

  pUnit = interface_lock_unit (0);

  for (UnitIndex = 0; UnitIndex <= pSdaiBackendDriverECAT->HighestUnitIndex; UnitIndex++)
  {
    if (pUnit->InternalFlags & UNIT_INTERNAL_FLAG_OUTPUT_CHANGED)
    {
      pUnit->InternalFlags &= ~UNIT_INTERNAL_FLAG_OUTPUT_CHANGED;

      OutputChangedInd.Id [NumberChangedUnits] = pUnit->Id;
      NumberChangedUnits++;
    }
    else if ( (Signal == INTERFACE_SIGNAL_DATA_UPDATE      ) &&
              (pUnit->Type == SDAI_UNIT_TYPE_GENERIC_OUTPUT) )
    {
      OutputChangedInd.Id [NumberChangedUnits] = pUnit->Id;
      NumberChangedUnits++;
    }

    pUnit++;
  }

  interface_unlock_unit (0);

  if (NumberChangedUnits > 0)
  {
    OutputChangedInd.NumberUnits = NumberChangedUnits;

    if (pSdaiBackendDriver->ApplicationInitData.Callback.DataCbk != NULL)
    {
      pSdaiBackendDriver->ApplicationInitData.Callback.DataCbk (&OutputChangedInd);
    }
  }

  return;
}

/*===========================================================================*/

/**
 * The function sdai_drv_ecat_handle_service_ind() handels the messages between the stack and
 * the application part of the SDAI.
 * @return
 * - type  : VOID
 * @param[in] pSdaiBackendDriver
 * - type : T_SDAI_BACKEND_DRIVER*
 * - range: whole address space
 */
static void sdai_drv_ecat_handle_service_ind (T_SDAI_BACKEND_DRIVER* pSdaiBackendDriver)
{
  T_INTERFACE_SERVICE_IND*    pServiceData;
  T_SDAI_DRV_ECAT*            pSdaiBackendDriverECAT;

  pServiceData = interface_lock_data (SERVICE_IND_INTERFACE);

  pSdaiBackendDriverECAT = pSdaiBackendDriver->pPrivateData;

  if (pServiceData != NULL)
  {
    switch (pServiceData->ServiceType)
    {
      case SERVICE_IND_EXCEPTION:
      {
        memcpy (&ServiceIndBuffer.UseAs.Exception, &pServiceData->UseAs.Exception, sizeof (ServiceIndBuffer.UseAs.Exception));

        pServiceData->ServiceType = SERVICE_IND_FREE;
        interface_unlock_data (SERVICE_IND_INTERFACE);

        if (pSdaiBackendDriver->ApplicationInitData.Callback.ExceptionCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.ExceptionCbk (&ServiceIndBuffer.UseAs.Exception);
        }

        pSdaiBackendDriverECAT->ExceptionPending = 1;
        break;
      }

      case SERVICE_IND_WRITE:
      {
        memcpy (&ServiceIndBuffer.UseAs.Write, &pServiceData->UseAs.Write, sizeof (ServiceIndBuffer.UseAs.Write));

        if (ServiceIndBuffer.UseAs.Write.Length > 0)
        {
          _ASSERT (ServiceIndBuffer.UseAs.Write.Length <= (SERVICE_SIZE - sizeof (ServiceIndBuffer.UseAs.Write)));
          memcpy (&ServiceIndBuffer.UseAs.Write + 1, &pServiceData->UseAs.Write + 1, ServiceIndBuffer.UseAs.Write.Length);
        }

        pServiceData->ServiceType = SERVICE_IND_FREE;
        interface_unlock_data (SERVICE_IND_INTERFACE);

        if (pSdaiBackendDriver->ApplicationInitData.Callback.WriteReqCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.WriteReqCbk (&ServiceIndBuffer.UseAs.Write);
        }

        break;
      }

      case SERVICE_IND_READ:
      {
        memcpy (&ServiceIndBuffer.UseAs.Read, &pServiceData->UseAs.Read, sizeof (ServiceIndBuffer.UseAs.Read));

        pServiceData->ServiceType = SERVICE_IND_FREE;
        interface_unlock_data (SERVICE_IND_INTERFACE);

        if (pSdaiBackendDriver->ApplicationInitData.Callback.ReadReqCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.ReadReqCbk (&ServiceIndBuffer.UseAs.Read);
        }

        break;
      }

      case SERVICE_IND_CONTROL:
      {
        memcpy (&ServiceIndBuffer.UseAs.Control, &pServiceData->UseAs.Control, sizeof (ServiceIndBuffer.UseAs.Control));

        if (ServiceIndBuffer.UseAs.Control.Length > 0)
        {
          _ASSERT (ServiceIndBuffer.UseAs.Control.Length <= (SERVICE_SIZE - sizeof (ServiceIndBuffer.UseAs.Control)));
          memcpy (&ServiceIndBuffer.UseAs.Control + 1, &pServiceData->UseAs.Control + 1, ServiceIndBuffer.UseAs.Control.Length);
        }

        pServiceData->ServiceType = SERVICE_IND_FREE;
        interface_unlock_data (SERVICE_IND_INTERFACE);

        if(pSdaiBackendDriver->ApplicationInitData.Callback.ControlReqCbk != NULL)
        {
          pSdaiBackendDriver->ApplicationInitData.Callback.ControlReqCbk (&ServiceIndBuffer.UseAs.Control);
        }

        break;
      }

      default:
      {
        pServiceData->ServiceType = SERVICE_IND_FREE;
        interface_unlock_data (SERVICE_IND_INTERFACE);
        break;
      }
    }
  }

  return;
}

#endif  /* SDAI_INCLUDE_ECAT */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
