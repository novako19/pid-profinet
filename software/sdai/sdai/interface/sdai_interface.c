/******************************************************************************

SOFTING AG
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

#include <string.h>

#include "sdai.h"

#include "cfg_cc.h"

#ifdef PROCESSOR_STACK

  #ifdef SDAI_INCLUDE_ECAT
    #include "ecat_user.h"
  #endif

  #ifdef SDAI_INCLUDE_PNAK
    #include "profinet.h"
    #include "pnak.h"
  #endif

  #ifdef SDAI_INCLUDE_EIP
    #include "eips_user.h"
  #endif

  #ifdef SDAI_INCLUDE_MBAK
    #include "modbus.h"
    #include "mbak.h"
  #endif

  #ifdef SDAI_INCLUDE_EPL
    #include "epl_user.h"
  #endif

  #ifdef SDAI_INCLUDE_PB_DP
    #include "protocol.h"
    #include "psak_typ.h"
    #include "psak.h"
  #endif

#endif /* PROCESSOR_STACK */

#include "sdai_interface.h"

#ifdef SDAI_INCLUDE_SOCKET
  #include "sdai_socket.h"
  #include "socket_interface.h"
#endif

#include "porting.h"

/******************************************************************************
DEFINES
******************************************************************************/

/*---------------------------------------------------------------------------*/

#define SOCKET_IF_MEMORY_OFFSET     (0x00001000uL)  /**< Offset of the socket interface within the DPRAM. The first
                                                         4 kB of the DPRAM are used by the SDAI interface. */

/******************************************************************************
TYPEDEFS
******************************************************************************/

/** \brief Holds the application specific data in the interface */
typedef struct _T_SDAI_INTERFACE_APPLICATION
{
  void*   pPrivateData;                     /**< Private data structure from the application side */

  int (*handle_signal) (U16, void*);        /**< Function pointer to the signal handler function from the application side */
  int (*handle_synch_signal) (U8, void*);   /**< Function pointer to the synch signal handler function from the application side */

  T_INTERFACE_UNIT    Units [SDAI_MAX_UNITS];             /**< local Units area, used in case of defragmentation */
  U8                  ProcessData [SDAI_MAX_IO_DATA_SIZE];/**< local Process data area, used in case of defragmentation */

} T_SDAI_INTERFACE_APPLICATION;

/*---------------------------------------------------------------------------*/

/** \brief Holds the stack specific data in the interface */
typedef struct _T_SDAI_INTERFACE_STACK
{
  void*   pPrivateData;                 /**< Private data structure from the stack side */

  int (*handle_signal) (U16, void*);    /**< Function pointer to the signal handler function from the stack side */

} T_SDAI_INTERFACE_STACK;

/*---------------------------------------------------------------------------*/

/** \brief Holds the stack and application specific data in the interface */
typedef struct _T_SDAI_INTERFACE
{
  T_SDAI_INTERFACE_APPLICATION    Application;    /**< The application specific data */
  T_SDAI_INTERFACE_STACK          Stack;          /**< The stack specific data */

} T_SDAI_INTERFACE;

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

static void     interface_defrag_io_memory    (void);

/*=== PROFINET FUNCTION INTERFACE ===========================================*/

#ifdef PROCESSOR_STACK
#ifdef SDAI_INCLUDE_PNAK

/*--- initialization/termination functions ----------------------------------*/

void             (*p_pnak_init)                            (void);
void             (*p_pnak_term)                            (void);

T_PNAK_RESULT    (*p_pnak_start_stack)                     (unsigned short, const T_PNAK_MODE_ID);
T_PNAK_RESULT    (*p_pnak_stop_stack)                      (unsigned short);

/*--- General interface functions -------------------------------------------*/

T_PNAK_RESULT    (*p_pnak_wait_for_multiple_objects)       (unsigned short, T_PNAK_WAIT_OBJECT*, T_PNAK_TIMEOUT);
T_PNAK_RESULT    (*p_pnak_set_multiple_objects)            (unsigned short, const T_PNAK_WAIT_OBJECT);
T_PNAK_RESULT    (*p_pnak_mask_multiple_objects)           (unsigned short, const T_PNAK_WAIT_OBJECT);

T_PNAK_RESULT    (*p_pnak_get_exception)                   (unsigned short, T_PNAK_EXCEPTION*);
T_PNAK_RESULT    (*p_pnak_get_version)                     (unsigned short, T_PNAK_VERSIONS*);

T_PNAK_RESULT    (*p_pnak_snmp_get_data)                   (unsigned short, T_PNAK_OID*, void*, unsigned long*, unsigned long*, unsigned char, PN_BOOL);
T_PNAK_RESULT    (*p_pnak_snmp_check_data)                 (unsigned short, T_PNAK_OID*, void*, unsigned long, unsigned long, unsigned char);
T_PNAK_RESULT    (*p_pnak_snmp_set_data)                   (unsigned short, T_PNAK_OID*, void*, unsigned long, unsigned long);

/*--- Service interface functions -------------------------------------------*/

T_PNAK_RESULT    (*p_pnak_send_service_req_res)            (unsigned short, const T_PNAK_SERVICE_REQ_RES*);
T_PNAK_RESULT    (*p_pnak_get_service_ind)                 (unsigned short, T_PNAK_SERVICE_IND*);
T_PNAK_RESULT    (*p_pnak_get_service_con)                 (unsigned short, T_PNAK_SERVICE_CON*);

/*--- Event interface functions ---------------------------------------------*/

T_PNAK_RESULT    (*p_pnak_set_mode)                        (unsigned short, const T_PNAK_EVENT_SET_MODE*);
T_PNAK_RESULT    (*p_pnak_set_device_state)                (unsigned short, const T_PNAK_EVENT_SET_DEVICE_STATE*);

T_PNAK_RESULT    (*p_pnak_get_state)                       (unsigned short, T_PNAK_EVENT_STATE*);
T_PNAK_RESULT    (*p_pnak_get_device_state_ind)            (unsigned short, T_PNAK_EVENT_DEVICE_STATE*);
T_PNAK_RESULT    (*p_pnak_get_alarm_ind)                   (unsigned short, T_PNAK_EVENT_ALARM*);
T_PNAK_RESULT    (*p_pnak_get_alarm_ack_ind)               (unsigned short, T_PNAK_EVENT_ALARM_ACK*);
T_PNAK_RESULT    (*p_pnak_get_ptcp_ind)                    (unsigned short, PN_U32*);

/*--- Data interface functions ----------------------------------------------*/

T_PNAK_RESULT    (*p_pnak_set_iocr_data)                   (unsigned short, unsigned short, const unsigned char*, unsigned short, unsigned char);

T_PNAK_RESULT    (*p_pnak_get_iocr_data)                   (unsigned short, unsigned short, unsigned char*, unsigned short*, unsigned char*, unsigned char*);
T_PNAK_RESULT    (*p_pnak_get_consumer_data_changed_ind)   (unsigned short, T_PNAK_DATA_CONSUMER_DATA_CHANGED*);
T_PNAK_RESULT    (*p_pnak_get_provider_data_updated)       (unsigned short, T_PNAK_DATA_PROVIDER_DATA_UPDATED*);

T_PNAK_RESULT    (*p_pnak_register_provider_callback)      (unsigned short, T_PROVIDER_CALLBACK);
T_PNAK_RESULT    (*p_pnak_unregister_provider_callback)    (unsigned short);

T_PNAK_RESULT    (*p_pnak_register_consumer_callback)      (unsigned short, T_CONSUMER_CALLBACK);
T_PNAK_RESULT    (*p_pnak_unregister_consumer_callback)    (unsigned short);

#endif /* SDAI_INCLUDE_PNAK */
#endif /* PROCESSOR_STACK */

/*=== PROFIBUS DP FUNCTION INTERFACE ========================================*/

#ifdef PROCESSOR_STACK
#ifdef SDAI_INCLUDE_PB_DP

/*--- initialization/termination functions ----------------------------------*/

void             (*p_psak_init)                            (void);
void             (*p_psak_term)                            (void);

T_PSAK_RESULT    (*p_psak_start_profistack)                (unsigned short, T_MODE_ID);
T_PSAK_RESULT    (*p_psak_stop_profistack)                 (unsigned short);

/*--- General interface functions -------------------------------------------*/

T_PSAK_RESULT    (*p_psak_wait_for_multiple_objects)       (unsigned short, T_PSAK_WAIT_OBJECT*, T_TIMEOUT);
T_PSAK_RESULT    (*p_psak_set_multiple_objects)            (unsigned short, T_PSAK_WAIT_OBJECT, T_DELAY_TIME);

T_PSAK_RESULT    (*p_psak_get_exception)                   (unsigned short, T_PSAK_EXCEPTION*);
T_PSAK_RESULT    (*p_psak_get_version)                     (unsigned short, T_VERSION*);
T_PSAK_RESULT    (*p_psak_retrigger_watchdog)              (unsigned short);

/*--- Service interface functions -------------------------------------------*/

T_PSAK_RESULT    (*p_psak_send_service_req_res)            (unsigned short, const T_SERVICE_REQ_RES*);
T_PSAK_RESULT    (*p_psak_get_service_req_res_result)      (unsigned short, T_SERVICE_REQ_RES_RESULT*);
T_PSAK_RESULT    (*p_psak_get_service_con)                 (unsigned short, T_SERVICE_CON_IND*);
T_PSAK_RESULT    (*p_psak_get_service_ind)                 (unsigned short, T_SERVICE_CON_IND*);
T_PSAK_RESULT    (*p_psak_enable_service_indication)       (unsigned short, T_ENABLE_SERVICE_IND);

/*--- Event interface functions ---------------------------------------------*/

T_PSAK_RESULT    (*p_psak_set_slave_operation_mode)        (unsigned short, const T_DPS_EVENT_SET_OPERATION_MODE*);
T_PSAK_RESULT    (*p_psak_get_slave_state)                 (unsigned short, T_DPS_EVENT_SLAVE_STATE*);
T_PSAK_RESULT    (*p_psak_get_c2_abort_ind)                (unsigned short, T_DPM_EVENT_C2_ABORT*);

/*--- Data interface functions ----------------------------------------------*/

T_PSAK_RESULT    (*p_psak_get_slave_data)                  (unsigned short, T_SLAVE_ADDRESS, T_DATA_OFFSET, T_DATA_SIZE*, T_SLAVE_DATA*);
T_PSAK_RESULT    (*p_psak_set_slave_data)                  (unsigned short, T_SLAVE_ADDRESS, T_DATA_OFFSET, T_DATA_SIZE, const T_SLAVE_DATA*);

/*--- Statistic interface functions -----------------------------------*/

T_PSAK_RESULT    (*p_psak_get_statistic)                   (unsigned short, T_STATISTIC*);

#endif /* SDAI_INCLUDE_PB_DP */
#endif /* PROCESSOR_STACK */

/*=== ETHERNET/IP FUNCTION INTERFACE ========================================*/

#ifdef PROCESSOR_STACK
#ifdef SDAI_INCLUDE_EIP

void              (*p_eips_user_init)                             (void);
void              (*p_eips_user_term)                             (void);
void              (*p_eips_user_start_stack)                      (void);
void              (*p_eips_user_stop_stack)                       (void);
void              (*p_eips_user_main_loop)                        (void);
void              (*p_eips_user_handle_pending_service_req_res)   (void);

#endif /* SDAI_INCLUDE_EIP */
#endif /* PROCESSOR_STACK */

/*=== ETHERCAT FUNCTION INTERFACE ==========================================*/

#ifdef PROCESSOR_STACK
#ifdef SDAI_INCLUDE_ECAT

void              (*p_ecat_user_init)                             (void);
void              (*p_ecat_user_term)                             (void);
void              (*p_ecat_user_start_stack)                      (void);
void              (*p_ecat_user_stop_stack)                       (void);
void              (*p_ecat_user_main_loop)                        (void);
void              (*p_ecat_user_handle_pending_service_req_res)   (void);
void              (*p_ecat_user_update_input_data)                (void);

#endif /* SDAI_INCLUDE_ECAT */
#endif /* PROCESSOR_STACK */

/*=== MODBUS FUNCTION INTERFACE ============================================*/

#ifdef PROCESSOR_STACK
#ifdef SDAI_INCLUDE_MBAK

/*--- initialization/termination functions ----------------------------------*/

void             (*p_mbak_init)                            (void);
void             (*p_mbak_term)                            (void);

T_MBAK_RESULT    (*p_mbak_start_stack)                     (unsigned short, const T_MBAK_MODE_ID);
T_MBAK_RESULT    (*p_mbak_stop_stack)                      (unsigned short);

/*--- General interface functions -------------------------------------------*/

T_MBAK_RESULT    (*p_mbak_wait_for_multiple_objects)       (unsigned short, T_MBAK_WAIT_OBJECT*, T_MBAK_TIMEOUT);
T_MBAK_RESULT    (*p_mbak_set_multiple_objects)            (unsigned short, const T_MBAK_WAIT_OBJECT);
T_MBAK_RESULT    (*p_mbak_mask_multiple_objects)           (unsigned short, const T_MBAK_WAIT_OBJECT);

T_MBAK_RESULT    (*p_mbak_get_exception)                   (unsigned short, T_MBAK_EXCEPTION*);
T_MBAK_RESULT    (*p_mbak_get_version)                     (unsigned short, T_MBAK_VERSIONS*);

/*--- Service interface functions -------------------------------------------*/

T_MBAK_RESULT    (*p_mbak_send_service_req_res)            (unsigned short, const T_MBAK_SERVICE_REQ_RES*);
T_MBAK_RESULT    (*p_mbak_get_service_ind)                 (unsigned short, T_MBAK_SERVICE_IND*);
T_MBAK_RESULT    (*p_mbak_get_service_con)                 (unsigned short, T_MBAK_SERVICE_CON*);

/*--- Event interface functions ---------------------------------------------*/

T_MBAK_RESULT    (*p_mbak_set_mode)                        (unsigned short, const T_MBAK_EVENT_SET_MODE*);
T_MBAK_RESULT    (*p_mbak_set_device_state)                (unsigned short, const T_MBAK_EVENT_SET_DEVICE_STATE*);

T_MBAK_RESULT    (*p_mbak_get_state)                       (unsigned short, T_MBAK_EVENT_STATE*);
T_MBAK_RESULT    (*p_mbak_get_device_state_ind)            (unsigned short, T_MBAK_EVENT_DEVICE_STATE*);

#endif /* SDAI_INCLUDE_MBAK */
#endif /* PROCESSOR_STACK */

/*=== Powerlink FUNCTION INTERFACE =========================================*/

#ifdef PROCESSOR_STACK
#ifdef SDAI_INCLUDE_EPL

void              (*p_epl_user_init)                             (void);
void              (*p_epl_user_term)                             (void);
void              (*p_epl_user_start_stack)                      (void);
void              (*p_epl_user_stop_stack)                       (void);
void              (*p_epl_user_main_loop)                        (void);
void              (*p_epl_user_handle_pending_service_req_res)   (void);

#endif /* SDAI_INCLUDE_EPL */
#endif /* PROCESSOR_STACK */
/******************************************************************************
PRIVATE DATA
******************************************************************************/

static T_SDAI_INTERFACE           SdaiInterface;              /**< Instance of the interface structure*/
static T_SHARED_RAM_INTERFACE*    pSharedRamInterface;        /**< pointer to the SDAI shared ram */

#ifdef SDAI_INCLUDE_SOCKET
static T_SOCKET_INTERFACE*        pSocketSharedRamInterface;  /**< pointer to the socket shared ram */
#endif

/******************************************************************************
PUBLIC FUNCTIONS
******************************************************************************/

/**
 * @desc The function interface_init() initializes the shared ram interface and
 *       the mutex. When the function is called from the application side it clears
 *       the shared ram and sets the service channels as free. Then it starts the
 *       protocol stack main task and stores the event handler and the private data
 *       from the application. Called from the stack side it only stores the private
 *       data and the signal handler.
 * @return
 * - type  : VOID
 * @param[in] handle_signal
 * - type : Function Pointer
 * - range: whole address space
 * @param[in] InterfaceSide
 * - type : int
 * - range: #INTERFACE_APPLICATION | #INTERFACE_STACK
 * @param[in] pPrivateData
 * - type : void*
 * - range: whole address space
 * @param[in] task_main_loop
 * - type : Function Pointer
 * - range: whole address space
 * @param[in] BackEnd
 * - type : U8
 * - range:
 */
U8 interface_init(int (*handle_signal) (U16, void*), int (*handle_synch_signal) (U8, void*), int InterfaceSide, void* pPrivateData, U8 BackEnd)
{
  BOOL    EnableSynchSignal;


  _TRACE (("interface_init"));

/* Set the function pointers to the stack API functions. In case of Windows a DLL will be loaded.
   In that case the function pointers will be set by function mutex_init() in the porting layer.
 */

#if !defined WIN32 && !defined _WIN32 && !defined __linux

#ifdef PROCESSOR_STACK

#ifdef SDAI_INCLUDE_PNAK
  p_pnak_init                          = pnak_init;
  p_pnak_term                          = pnak_term;
  p_pnak_start_stack                   = pnak_start_stack;
  p_pnak_stop_stack                    = pnak_stop_stack;
  p_pnak_wait_for_multiple_objects     = pnak_wait_for_multiple_objects;
  p_pnak_set_multiple_objects          = pnak_set_multiple_objects;
  p_pnak_mask_multiple_objects         = pnak_mask_multiple_objects;
  p_pnak_get_exception                 = pnak_get_exception;
  p_pnak_get_version                   = pnak_get_version;
  p_pnak_snmp_get_data                 = pnak_snmp_get_data;
  p_pnak_snmp_check_data               = pnak_snmp_check_data;
  p_pnak_snmp_set_data                 = pnak_snmp_set_data;
  p_pnak_send_service_req_res          = pnak_send_service_req_res;
  p_pnak_get_service_ind               = pnak_get_service_ind;
  p_pnak_get_service_con               = pnak_get_service_con;
  p_pnak_set_mode                      = pnak_set_mode;
  p_pnak_set_device_state              = pnak_set_device_state;
  p_pnak_get_state                     = pnak_get_state;
  p_pnak_get_device_state_ind          = pnak_get_device_state_ind;
  p_pnak_get_alarm_ind                 = pnak_get_alarm_ind;
  p_pnak_get_alarm_ack_ind             = pnak_get_alarm_ack_ind;
  p_pnak_get_ptcp_ind                  = pnak_get_ptcp_ind;
  p_pnak_set_iocr_data                 = pnak_set_iocr_data;
  p_pnak_get_iocr_data                 = pnak_get_iocr_data;
  p_pnak_get_consumer_data_changed_ind = pnak_get_consumer_data_changed_ind;
  p_pnak_get_provider_data_updated     = pnak_get_provider_data_updated;
  p_pnak_register_provider_callback    = pnak_register_provider_callback;
  p_pnak_unregister_provider_callback  = pnak_unregister_provider_callback;
  p_pnak_register_consumer_callback    = pnak_register_consumer_callback;
  p_pnak_unregister_consumer_callback  = pnak_unregister_consumer_callback;
#endif /* SDAI_INCLUDE_PNAK */

  /*-------------------------------------------------------------------------*/

#ifdef SDAI_INCLUDE_PB_DP
  p_psak_init                          = psak_init;
  p_psak_term                          = psak_term;
  p_psak_start_profistack              = psak_start_profistack;
  p_psak_stop_profistack               = psak_stop_profistack;
  p_psak_wait_for_multiple_objects     = psak_wait_for_multiple_objects;
  p_psak_set_multiple_objects          = psak_set_multiple_objects;
  p_psak_get_exception                 = psak_get_exception;
  p_psak_get_version                   = psak_get_version;
  p_psak_retrigger_watchdog            = psak_retrigger_watchdog;
  p_psak_send_service_req_res          = psak_send_service_req_res;
  p_psak_get_service_req_res_result    = psak_get_service_req_res_result;
  p_psak_get_service_con               = psak_get_service_con;
  p_psak_get_service_ind               = psak_get_service_ind;
  p_psak_enable_service_indication     = psak_enable_service_indication;
  p_psak_set_slave_operation_mode      = psak_set_slave_operation_mode;
  p_psak_get_c2_abort_ind              = psak_get_c2_abort_ind;
  p_psak_get_slave_state               = psak_get_slave_state;
  p_psak_get_slave_data                = psak_get_slave_data;
  p_psak_set_slave_data                = psak_set_slave_data;
  p_psak_get_statistic                 = psak_get_statistic;
#endif /* SDAI_INCLUDE_PB_DP */

  /*-------------------------------------------------------------------------*/

#ifdef SDAI_INCLUDE_EIP
  p_eips_user_init                           = eips_user_init;
  p_eips_user_term                           = eips_user_term;
  p_eips_user_start_stack                    = eips_user_start_stack;
  p_eips_user_stop_stack                     = eips_user_stop_stack;
  p_eips_user_main_loop                      = eips_user_main_loop;
  p_eips_user_handle_pending_service_req_res = eips_user_handle_pending_service_req_res;
#endif /* SDAI_INCLUDE_EIP */

  /*-------------------------------------------------------------------------*/

#ifdef SDAI_INCLUDE_ECAT
  p_ecat_user_init                           = ecat_user_init;
  p_ecat_user_term                           = ecat_user_term;
  p_ecat_user_start_stack                    = ecat_user_start_stack;
  p_ecat_user_stop_stack                     = ecat_user_stop_stack;
  p_ecat_user_main_loop                      = ecat_user_main_loop;
  p_ecat_user_handle_pending_service_req_res = ecat_user_handle_pending_service_req_res;
  p_ecat_user_update_input_data              = ecat_user_update_input_data;
#endif /* SDAI_INCLUDE_ECAT */

  /*-------------------------------------------------------------------------*/

#ifdef SDAI_INCLUDE_MBAK
  p_mbak_init                      = mbak_init;
  p_mbak_term                      = mbak_term;
  p_mbak_start_stack               = mbak_start_stack;
  p_mbak_stop_stack                = mbak_stop_stack;
  p_mbak_wait_for_multiple_objects = mbak_wait_for_multiple_objects;
  p_mbak_set_multiple_objects      = mbak_set_multiple_objects;
  p_mbak_mask_multiple_objects     = mbak_mask_multiple_objects;
  p_mbak_get_exception             = mbak_get_exception;
  p_mbak_get_version               = mbak_get_version;
  p_mbak_send_service_req_res      = mbak_send_service_req_res;
  p_mbak_get_service_ind           = mbak_get_service_ind;
  p_mbak_get_service_con           = mbak_get_service_con;
  p_mbak_set_mode                  = mbak_set_mode;
  p_mbak_set_device_state          = mbak_set_device_state;
  p_mbak_get_state                 = mbak_get_state;
  p_mbak_get_device_state_ind      = mbak_get_device_state_ind;
#endif /* SDAI_INCLUDE_MBAK */

  /*-------------------------------------------------------------------------*/

#ifdef SDAI_INCLUDE_EPL
  p_epl_user_init                           = epl_user_init;
  p_epl_user_term                           = epl_user_term;
  p_epl_user_start_stack                    = epl_user_start_stack;
  p_epl_user_stop_stack                     = epl_user_stop_stack;
  p_epl_user_main_loop                      = epl_user_main_loop;
  p_epl_user_handle_pending_service_req_res = epl_user_handle_pending_service_req_res;
#endif /* SDAI_INCLUDE_EPL */

#endif /* PROCESSOR_STACK */
#endif /* ! WIN32 */

  /*-------------------------------------------------------------------------*/

  EnableSynchSignal   = (handle_synch_signal == NULL) ? FALSE : TRUE;
  pSharedRamInterface = init_resources (InterfaceSide, EnableSynchSignal);

#ifdef SDAI_INCLUDE_SOCKET
  /* socket interface is located directly at the end of the SDAI interface */
  pSocketSharedRamInterface = (T_SOCKET_INTERFACE*) (((U8*) pSharedRamInterface) + SOCKET_IF_MEMORY_OFFSET);
#endif

  if (InterfaceSide == INTERFACE_APPLICATION)
  {
    volatile U32*   pDPRAMStart = (volatile U32*) pSharedRamInterface;
    volatile U32*   pMagicKey   = (volatile U32*) (((U8*) pSharedRamInterface) + SOCKET_IF_MEMORY_OFFSET);


    *pDPRAMStart  = 0xDEADBEEF;
    *pDPRAMStart ^= 0x5AA5A55A;

    /* we have defined a address range of 64 kByte, if the dpram is only 4 kByte the
     * magic value we write should be mirrored each 4 kByte
     */
    if ( (*pMagicKey == *pDPRAMStart) || (*pMagicKey == 0xDEADBEEFuL) || (*pMagicKey == 0xFFFFFFFFuL) )
    {
#ifdef SDAI_INCLUDE_SOCKET
      pSocketSharedRamInterface = NULL;
#endif
    }

    memset (pSharedRamInterface, 0, sizeof (T_SHARED_RAM_INTERFACE));

    pSharedRamInterface->ManagementData.ApplReady         = 0u;
    pSharedRamInterface->ManagementData.StackReady        = 0u;
    pSharedRamInterface->ManagementData.RemainingDataSize = sizeof (pSharedRamInterface->ProcessData);
    pSharedRamInterface->ManagementData.BackEnd           = BackEnd;
    pSharedRamInterface->StatusData.PulseWidth            = SDAI_INTERRUPT_DEFAULT_PULSE_WIDTH;
    pSharedRamInterface->ServiceReqRes.ServiceType        = SERVICE_REQ_RES_FREE;
    pSharedRamInterface->ServiceInd.ServiceType           = SERVICE_IND_FREE;

    pSharedRamInterface->ProviderStatusGood               = SDAI_DATA_STATUS_VALID;
    pSharedRamInterface->ProviderStatusBad                = SDAI_DATA_STATUS_INVALID;

#ifdef SDAI_INCLUDE_SOCKET
    if (pSocketSharedRamInterface != NULL)
    {
      memset (pSocketSharedRamInterface, 0, sizeof (T_SOCKET_INTERFACE));
    }
#endif

    SdaiInterface.Application.handle_signal       = handle_signal;
    SdaiInterface.Application.handle_synch_signal = handle_synch_signal;
    SdaiInterface.Application.pPrivateData        = pPrivateData;

    create_stack_task (sdai_protocol_stack_task_main_loop);

    return ((pSharedRamInterface->ManagementData.StackReady == SDAI_INTERFACE_STACK_READY) ? SDAI_SUCCESS : SDAI_ERR_PROTOCOL_NOT_SUPPORTED);
  }
  else
  {
    _ASSERT (InterfaceSide == INTERFACE_STACK);

    SdaiInterface.Stack.handle_signal = handle_signal;
    SdaiInterface.Stack.pPrivateData  = pPrivateData;

    #ifdef PROCESSOR_STACK
    perfopt_config_input_interrupt((U32)&pSharedRamInterface->ManagementData.InterruptSignalAppl, INTERFACE_SIGNAL_UPDATE_INPUT_DATA, FALSE, FALSE);
    #endif

    return (SDAI_SUCCESS);
  }
}

/*===========================================================================*/

/**
 * @desc The function interface_deinit() stops the protocol stack, sets the
 *       shared ram to 0x00 and clears the local interface variables.
 * @return
 * - type  : VOID
 * @param[in] InterfaceSide
 * - type : int
 * - range: #INTERFACE_APPLICATION | #INTERFACE_STACK
 * @pre The interface_init() function must be called before the first call of
 *      this function.
 * @post After the call of interface_deinit() the function interface_init() must
 *       be called befor using the interface again.
 */
void interface_deinit (int InterfaceSide)
{
  _TRACE (("interface_deinit"));

  if (InterfaceSide == INTERFACE_APPLICATION)
  {
    delete_stack_task ();
    term_resources (INTERFACE_APPLICATION);

    memset (pSharedRamInterface, 0, sizeof (T_SHARED_RAM_INTERFACE));

    SdaiInterface.Application.handle_signal = NULL;
    SdaiInterface.Application.pPrivateData  = NULL;
  }
  else
  {
    _ASSERT (InterfaceSide == INTERFACE_STACK);

    delete_stack_task_finish ();
    term_resources (INTERFACE_STACK);

    SdaiInterface.Stack.handle_signal = NULL;
    SdaiInterface.Stack.pPrivateData  = NULL;
  }

  return;
}

/*===========================================================================*/

/**
 * @desc The function interface_get_dpram_base() returns the base address of the DPRAM.
 * @return
 * - type  : VOID
 */
U8* interface_get_dpram_base (void)
{
  _TRACE (("interface_get_dpram_base"));

  return ((U8*) pSharedRamInterface);
}

/*===========================================================================*/

/**
 * @desc The function interface_get_iodata_base() returns the base address of the IO Data
 *       area in the DPRAM.
 * @return
 * - type  : VOID
 */
U8* interface_get_iodata_base (void)
{
  _TRACE (("interface_get_iodata_base"));

  return (&pSharedRamInterface->ProcessData [0]);
}

/*===========================================================================*/

/**
 * @desc The function interface_get_status_good_address() returns the address of
 * the fixed status value in the DPRAM.
 * @return
 * - type  : VOID
 */
U8* interface_get_status_good_address (void)
{
  _TRACE (("interface_get_status_good_address"));

  return (&pSharedRamInterface->ProviderStatusGood);
}

/*===========================================================================*/

/**
 * @desc The function interface_wait_application_startup_finished() waits for the application ready signal.
 *       This function is called from the stack side. The stack must call this function
 *       before any other access to the shared RAM. The function must return the requested Backend.
 * @return
 * - type  : U8
 * - range: #SDAI_BACKEND_EIPS   |
 *          #SDAI_BACKEND_PN     |
 *          #SDAI_BACKEND_PB_DP  |
 *          #SDAI_BACKEND_ECAT   |
 *          #SDAI_BACKEND_MODBUS
 */
U8 interface_wait_application_startup_finished (void)
{
  _TRACE (("interface_wait_application_startup_finished"));

  return (wait_application_startup_finished ());
}

/*===========================================================================*/

/**
 * @desc The function interface_stack_initialized() sets the stack ready flag.
 *       This function is called from the stack side. The stack must call
 *       this function after initialization is complete.
 * @return
 * - type  : VOID
 */
void interface_stack_initialized (U8 Status)
{
  _TRACE (("interface_stack_initialized"));

  mutex_lock(MANAGEMENT_INTERFACE);
  pSharedRamInterface->StatusData.StackStatus |= SDAI_STATE_MASK_INIT;

  pSharedRamInterface->ManagementData.StackReady = Status;
  mutex_unlock(MANAGEMENT_INTERFACE);

  create_stack_task_finished ();
  return;
}

/*===========================================================================*/

/**
 * @desc The function interface_lock_data() locks an area of the shared ram and
 *       returns a pointer to the start of the area. If the area does not exist
 *       the function returns NULL. If the mutex is not available the function blocks
 *       until the mutex is free.
 * @return
 * - type  : VOID*
 * - values: NULL | whole address space
 * @param[in] InterfaceArea
 * - type : T_INTERFACE_AREA
 * - range: #MANAGEMENT_INTERFACE      |
 *          #STATUS_INTERFACE          |
 *          #SERVICE_REQ_RES_INTERFACE |
 *          #SERVICE_IND_INTERFACE     |
 *          #RESOURCE_INTERFACE        |
 *          #SHARED_MDIO_INTERFACE     |
 *          #UNIT_INTERFACE
 * @pre The interface_init() function musst be called before the first call of
 *      this function.
 * @post When the function returns, the specified area of the shared ram is locked, so the
 *       interface_unlock_data() function musst be called when the work in the interface is
 *       done.
 */
void* interface_lock_data (T_INTERFACE_AREA InterfaceArea)
{
  void*   pInterface = NULL;


  _TRACE (("interface_lock_data"));

  mutex_lock (InterfaceArea);

  switch (InterfaceArea)
  {
    case MANAGEMENT_INTERFACE     : pInterface = &pSharedRamInterface->ManagementData; break;
    case STATUS_INTERFACE         : pInterface = &pSharedRamInterface->StatusData;     break;
    case SERVICE_REQ_RES_INTERFACE: pInterface = &pSharedRamInterface->ServiceReqRes;  break;
    case SERVICE_IND_INTERFACE    : pInterface = &pSharedRamInterface->ServiceInd;     break;
    case UNIT_INTERFACE           : pInterface = &pSharedRamInterface->Units [0];      break;
#ifdef SDAI_INCLUDE_SOCKET
    case SOCKET_INTERFACE         : pInterface = pSocketSharedRamInterface;            break;
#endif
    case RESOURCE_INTERFACE       : pInterface = (void*)0xFFFFFFFF;                    break;
    case SHARED_MDIO_INTERFACE    : pInterface = (void*)0xFFFFFFFF;                    break;

    default: break;
  }

  return (pInterface);
}

/*===========================================================================*/

/**
 * @desc The function interface_unlock_data() unlock an area of the shared ram.
 * @return
 * - type  : VOID
 * @param[in] InterfaceArea
 * - type : T_INTERFACE_AREA
 * - range: #MANAGEMENT_INTERFACE      |
 *          #STATUS_INTERFACE          |
 *          #SERVICE_REQ_RES_INTERFACE |
 *          #SERVICE_IND_INTERFACE     |
 *          #RESOURCE_INTERFACE        |
 *          #SHARED_MDIO_INTERFACE     |
 *          #UNIT_INTERFACE
 * @pre The interface_init() function musst be called before the first call of
 *      this function.
 */
void interface_unlock_data (T_INTERFACE_AREA InterfaceArea)
{
  _TRACE (("interface_unlock_data"));

  mutex_unlock (InterfaceArea);
}

/*===========================================================================*/

/**
 * @desc The function interface_get_free_unit() returns a pointer to the next
 *       free unit in the shared ram interface. First it is checked if there
 *       are free units left and also if there is enough free space for the
 *       IO data. If both ckecks are passed the data pointers are set and the
 *       pointer to the initialized unit is returned. If the checks fail the
 *       function return NULL. The function also return the unit index. This is
 *       a consecutive number which is increased by 1 with each new unit.
 * @return
 * - type  : T_INTERFACE_UNIT*
 * - values: NULL | whole address space
 * @param[in] InputSize
 * - type  : U8
 * - range : whole range is valid
 * @param[in] OutputSize
 * - type  : U8
 * - range : whole range is valid
 * @param[out] pUnitIndex
 * - type  : U16*
 * - range : whole address space
 * @pre The interface_init() function musst be called before the first call of
 *      this function.
 * @post When the function returns, the unit area of the shared ram is locked, so the
 *       interface_unlock_unit() function musst be called when the unit data are
 *       completely filled
 */
T_INTERFACE_UNIT* interface_get_free_unit (U8 InputSize, U8 OutputSize, U16* pUnitIndex)
{
  T_INTERFACE_MANAGEMENT_DATA*    pManagementData;
  T_INTERFACE_UNIT*               pUnit = NULL;


  _TRACE (("interface_get_free_unit"));

  _ASSERT (pUnitIndex != NULL);

  pManagementData = interface_lock_data (MANAGEMENT_INTERFACE);

  if (pManagementData != NULL)
  {
    U16   DataPosition;

    if (*pUnitIndex == 0xFFFF)
    {
      U16   UnitIndex = 0;


      /* find a free unit */
      pManagementData->NumberOfUnits = 0u;

      pUnit = interface_lock_data (UNIT_INTERFACE);

      for (UnitIndex = 0; UnitIndex < _NUMBER_ARRAY_ELEMENTS (pSharedRamInterface->Units); UnitIndex++)
      {
        /* count number of used units */
        if (pUnit[UnitIndex].InternalFlags & UNIT_INTERNAL_FLAG_IN_USE)
        {
          pManagementData->NumberOfUnits += 1u;
        }
        else if (*pUnitIndex == 0xFFFF) /* take the first free unit */
        {
          *pUnitIndex = UnitIndex;
        }
      }

      interface_unlock_data (UNIT_INTERFACE);

      if ( pManagementData->NumberOfUnits >= _NUMBER_ARRAY_ELEMENTS (pSharedRamInterface->Units) )
      {
        interface_unlock_data (MANAGEMENT_INTERFACE);
        return (NULL);
      }

      pManagementData->NumberOfUnits += 1u;
    }

    if (pManagementData->RemainingDataSize < (InputSize + OutputSize))
    {
      interface_defrag_io_memory ();

      if (pManagementData->RemainingDataSize < (InputSize + OutputSize))
      {
        interface_unlock_data (MANAGEMENT_INTERFACE);
        return (NULL);
      }
    }

    pUnit = interface_lock_unit (*pUnitIndex);
    _ASSERT_PTR (pUnit);

    pUnit->DataInputOffset  = 0xFFFF;
    pUnit->DataLengthInput  = 0;
    pUnit->DataOutputOffset = 0xFFFF;
    pUnit->DataLengthOutput = 0;

    /*---------------------------------------------------------------------*/

    if(InputSize != 0)
    {
      DataPosition = sizeof (pSharedRamInterface->ProcessData) - pManagementData->RemainingDataSize;

      pUnit->DataInputOffset = DataPosition;
      pUnit->DataLengthInput = InputSize;

      pManagementData->RemainingDataSize -= InputSize;
    }

    if(OutputSize != 0)
    {
      DataPosition = sizeof (pSharedRamInterface->ProcessData) - pManagementData->RemainingDataSize;

      pUnit->DataOutputOffset = DataPosition;
      pUnit->DataLengthOutput = OutputSize;

      pManagementData->RemainingDataSize -= OutputSize;
    }
  }

  interface_unlock_data (MANAGEMENT_INTERFACE);
  return (pUnit);
}

/*===========================================================================*/

/**
 * @desc The function interface_lock_unit() returns a pointer to the unit in the
 *       shared ram specified by "UnitIndex" and locks the unit area.
 * @return
 * - type  : T_INTERFACE_UNIT*
 * - values: NULL | whole address space
 * @param[in] UnitIndex
 * - type : U16
 * - range: ???
 * @pre The interface_init() function musst be called before the first call of
 *      this function.
 * @post When the function returns, the unit area of the shared ram is locked, so the
 *       interface_unlock_unit() function musst be called when the work on the unit
 *       is done.
 */
T_INTERFACE_UNIT* interface_lock_unit (U16 UnitIndex)
{
  T_INTERFACE_UNIT*   pUnit;


  _TRACE (("interface_lock_unit"));

  pUnit = interface_lock_data (UNIT_INTERFACE);
  _ASSERT_PTR (pUnit);

  pUnit = (UnitIndex < _NUMBER_ARRAY_ELEMENTS (pSharedRamInterface->Units)) ? &pUnit [UnitIndex] : NULL;

  return (pUnit);
}

/*===========================================================================*/

/**
 * @desc The function interface_unlock_unit() unlocks the unit area of the
 *       shared ram.
 * @return
 * - type  : VOID
 * @param[in] UnitIndex
 * - type : U16
 * - range: ???
 * @pre The interface_init() function musst be called before the first call of
 *      this function.
 */
void interface_unlock_unit (U16 UnitIndex)
{
  _TRACE (("interface_unlock_unit"));

  _AVOID_UNUSED_WARNING (UnitIndex);

  interface_unlock_data (UNIT_INTERFACE);

  return;
}

/*===========================================================================*/

/**
 * @desc The function interface_find_unit() searches for the unit with the specified
 *       Id. If found the function returns the unit index, otherwise 0xFFFF
 * @return
 * - type  : U16
 * - values: (0 to SDAI_MAX_UNITS-1) || 0xFFFF
 * @param[in] Id
 * - type : U32
 * - range: whole range
 * @param[in] MaxIndex
 * - type : U16
 * - range: whole range
 * @pre The interface_init() function musst be called before the first call of
 *      this function.
 */
U16 interface_find_unit (U32 Id, U16 MaxIndex)
{
  T_INTERFACE_UNIT*   pUnit;
  U16                 UnitIndex = 0;


  _TRACE (("interface_find_unit"));

  _ASSERT(MaxIndex <= SDAI_MAX_UNITS);

  pUnit = interface_lock_data (UNIT_INTERFACE);
  _ASSERT_PTR (pUnit);

  for (UnitIndex = 0; UnitIndex <= MaxIndex; UnitIndex++)
  {
    if ( (pUnit[UnitIndex].InternalFlags & UNIT_INTERNAL_FLAG_IN_USE) && (pUnit[UnitIndex].Id == Id) )
    {
      interface_unlock_data (UNIT_INTERFACE);
      return (UnitIndex);
    }
  }

  interface_unlock_data (UNIT_INTERFACE);

  return (0xFFFF);
}


/*===========================================================================*/

/**
 * @desc The function interface_find_free_unit() searches for the first free unit.
 *       If found the function returns the unit index, otherwise 0xFFFF
 * @return
 * - type  : U16
 * - values: (0 to SDAI_MAX_UNITS-1) || 0xFFFF
 * @param[in] Id
 * - type : U32
 * - range: whole range
 * @param[in] MaxIndex
 * - type : U16
 * - range: whole range
 * @pre The interface_init() function musst be called before the first call of
 *      this function.
 */
U16 interface_find_free_unit (U16 MaxIndex)
{
  T_INTERFACE_UNIT*   pUnit;
  U16                 UnitIndex = 0;


  _TRACE (("interface_find_unit"));

  _ASSERT(MaxIndex <= SDAI_MAX_UNITS);

  pUnit = interface_lock_data (UNIT_INTERFACE);
  _ASSERT_PTR (pUnit);

  for (UnitIndex = 0; UnitIndex <= MaxIndex; UnitIndex++)
  {
    if (! (pUnit[UnitIndex].InternalFlags & UNIT_INTERNAL_FLAG_IN_USE) )
    {
      interface_unlock_data (UNIT_INTERFACE);
      return (UnitIndex);
    }
  }

  interface_unlock_data (UNIT_INTERFACE);

  return (0xFFFF);
}

/*===========================================================================*/

/**
 * @desc The function interface_send_signal() handels incoming signals from the
 *       application and the stack and sends them to the other side.
 * @return
 * - type  : VOID
 * @param[in] InterfaceSide
 * - type : int
 * - range: #INTERFACE_APPLICATION | #INTERFACE_STACK
 * @param[in] Signal
 * - type : U16
 * - range: #INTERFACE_SIGNAL_START_STACK |
 *          #INTERFACE_SIGNAL_STOP_STACK  |
 *          #INTERFACE_SIGNAL_SERVICE_IND |
 *          #INTERFACE_SIGNAL_SERVICE_REQ_RES
 */
void interface_send_signal (int InterfaceSide, U16 Signal)
{
  T_INTERFACE_MANAGEMENT_DATA* pManagementData;

  _TRACE (("interface_send_signal"));

  pManagementData = interface_lock_data (MANAGEMENT_INTERFACE);
  _ASSERT_PTR (pManagementData);

  interrupt_lock ();

  if (InterfaceSide == INTERFACE_APPLICATION)
  {
    pManagementData->InterruptSignalAppl |= Signal;

    interface_unlock_data (MANAGEMENT_INTERFACE);

    send_signal_application (SDAI_INTERRUPT_SIGNAL);
  }
  else
  {
    _ASSERT (InterfaceSide == INTERFACE_STACK);

    pManagementData->InterruptSignalStack |= Signal;

    interface_unlock_data (MANAGEMENT_INTERFACE);

    send_signal_stack (SDAI_INTERRUPT_SIGNAL);
  }

  interrupt_unlock ();

  return;
}

/*===========================================================================*/

/**
 * @desc The function interface_receive_signal() handles incoming signals from the
 *       application and the stack and sends them to the other side.
 * @return
 * - type  : VOID
 * @param[in] InterfaceSide
 * - type : int
 * - range: #INTERFACE_APPLICATION | #INTERFACE_STACK
 */
void interface_receive_signal (int InterfaceSide)
{
  T_INTERFACE_MANAGEMENT_DATA* pManagementData;
  U16                          Signal;

  _TRACE (("interface_receive_signal"));

  pManagementData = interface_lock_data (MANAGEMENT_INTERFACE);
  _ASSERT_PTR (pManagementData);

  if (InterfaceSide == INTERFACE_APPLICATION)
  {
    _ASSERT_PTR (SdaiInterface.Stack.handle_signal);

    Signal = pManagementData->InterruptSignalAppl;
    pManagementData->InterruptSignalAppl = 0;

    interface_unlock_data (MANAGEMENT_INTERFACE);

    SdaiInterface.Stack.handle_signal (Signal, SdaiInterface.Stack.pPrivateData);
  }
  else
  {
    _ASSERT (InterfaceSide == INTERFACE_STACK);
    _ASSERT_PTR (SdaiInterface.Application.handle_signal);

    Signal = pManagementData->InterruptSignalStack;
    pManagementData->InterruptSignalStack = 0;

    interface_unlock_data (MANAGEMENT_INTERFACE);

    SdaiInterface.Application.handle_signal (Signal, SdaiInterface.Application.pPrivateData);
  }

  return;
}

/*===========================================================================*/

/**
 * @desc The function interface_receive_data_signal() handles incoming data from the
 *       stack and sends them to the other side.
 * @return
 * - type  : VOID
 * @param[in] InterfaceSide
 * - type : int
 * - range: #INTERFACE_APPLICATION | #INTERFACE_STACK
 */
void interface_receive_data_signal (int InterfaceSide)
{
  _TRACE (("interface_receive_data_signal"));

  if (InterfaceSide == INTERFACE_STACK)
  {
    SdaiInterface.Application.handle_signal (INTERFACE_SIGNAL_DATA_UPDATE, SdaiInterface.Application.pPrivateData);
  }

  return;
}

/*===========================================================================*/

/**
 * @desc The function interface_receive_synch_signal() handles an received SYNCH interrupt
 *       signal from the RTE subsystem.
 * @return
 * - type  : VOID
 * @param[in] Signal
 * - type : U8
 * - range: protocol specific
 */
void interface_receive_synch_signal (U8 Signal)
{
  _TRACE (("interface_receive_synch_signal"));

  if (SdaiInterface.Application.handle_synch_signal != NULL)
  {
    SdaiInterface.Application.handle_synch_signal (Signal, SdaiInterface.Application.pPrivateData);
  }

  return;
}

/******************************************************************************
PROTECTED FUNCTIONS
******************************************************************************/

/******************************************************************************
PRIVATE FUNCTIONS
******************************************************************************/

/**
 * @desc The function interface_defrag_io_memory() stores the unit management data
 *       and the original process data locally. Afterwards the unit management data
 *       is recalculated to skip gaps between several units and the original process
 *       data is restored at the new location.
 * @return
 * - type  : void
 */
void interface_defrag_io_memory (void)
{
  T_INTERFACE_UNIT*   pUnit;

  U16                 UnitIndex;
  U16                 Offset;
  U16                 DataPosition;

  U8                  Length;



  pUnit = interface_lock_data (UNIT_INTERFACE);

  memcpy (&SdaiInterface.Application.Units [0], pUnit, sizeof (SdaiInterface.Application.Units));

  for (UnitIndex = 0; UnitIndex < SDAI_MAX_UNITS; UnitIndex++)
  {
    Length = pUnit [UnitIndex].DataLengthOutput;
    Offset = pUnit [UnitIndex].DataOutputOffset;

    if ( (Length > 0u) && (Offset < (sizeof (SdaiInterface.Application.ProcessData) - Length)) )
    {
      memcpy (&SdaiInterface.Application.ProcessData [Offset], &pSharedRamInterface->ProcessData [Offset], Length);
    }

    /*-----------------------------------------------------------------------*/

    Length = pUnit [UnitIndex].DataLengthInput;
    Offset = pUnit [UnitIndex].DataInputOffset;

    if ( (Length > 0u) && (Offset < (sizeof (SdaiInterface.Application.ProcessData) - Length)) )
    {
      memcpy (&SdaiInterface.Application.ProcessData [Offset], &pSharedRamInterface->ProcessData [Offset], Length);
    }

    /*-----------------------------------------------------------------------*/

    pUnit [UnitIndex].DataOutputOffset = 0xFFFFu;
    pUnit [UnitIndex].DataInputOffset  = 0xFFFFu;
  }

  /*-------------------------------------------------------------------------*/

  /* now all process data is available again */
  pSharedRamInterface->ManagementData.RemainingDataSize = sizeof (pSharedRamInterface->ProcessData);

  for (UnitIndex = 0; UnitIndex < SDAI_MAX_UNITS; UnitIndex++)
  {
    Length = pUnit [UnitIndex].DataLengthInput;
    Offset = SdaiInterface.Application.Units [UnitIndex].DataInputOffset;

    if(Length > 0u)
    {
      DataPosition = sizeof (pSharedRamInterface->ProcessData) - pSharedRamInterface->ManagementData.RemainingDataSize;
      pUnit [UnitIndex].DataInputOffset                      = DataPosition;
      pSharedRamInterface->ManagementData.RemainingDataSize -= Length;

      memcpy (&pSharedRamInterface->ProcessData [DataPosition], &SdaiInterface.Application.ProcessData [Offset], Length);
    }

    /*-----------------------------------------------------------------------*/

    Length = pUnit [UnitIndex].DataLengthOutput;
    Offset = SdaiInterface.Application.Units [UnitIndex].DataOutputOffset;

    if(Length > 0u)
    {
      DataPosition = sizeof (pSharedRamInterface->ProcessData) - pSharedRamInterface->ManagementData.RemainingDataSize;
      pUnit [UnitIndex].DataOutputOffset                     = DataPosition;
      pSharedRamInterface->ManagementData.RemainingDataSize -= Length;

      memcpy (&pSharedRamInterface->ProcessData [DataPosition], &SdaiInterface.Application.ProcessData [Offset], Length);
    }
  }

  interface_unlock_data (UNIT_INTERFACE);

  return;
}

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
