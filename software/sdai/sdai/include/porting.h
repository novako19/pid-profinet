/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

#ifndef __PORTING_H__
#define __PORTING_H__

/******************************************************************************
DEFINES
******************************************************************************/

#define __STRINGIFY(x)      #x
#define _STRINGIFY(x)       __STRINGIFY(x)
#define _PRJ_LABEL          _STRINGIFY(LABEL)

#define STRING_SDAI_VERSION _PRJ_LABEL

#undef STRING_COPYRIGHT
#define STRING_COPYRIGHT          "Copyright (C) 2005-2015 SOFTING Industrial Automation GmbH. All Rights Reserved."
#define STRING_VERSION            _PRJ_LABEL " " __DATE__ " "
#define STRING_FW_VERSION         "SimpleDeviceAPI " STRING_VERSION "\n"

#undef _DEFINE_COPYRIGHT
#define _DEFINE_COPYRIGHT()                     volatile const char SdaiCopyright [] = \
                                                    STRING_COPYRIGHT
#undef _DEFINE_FIRMWARE_VERSION
#define _DEFINE_FIRMWARE_VERSION()              volatile const char SdaiFirmwareVersion [] = \
                                                    STRING_FW_VERSION

/*===========================================================================*/

#define SDAI_INTERRUPT_SIGNAL         0x01
#define SDAI_ECAT_SYNCH0_SIGNAL       0x02
#define SDAI_ECAT_SYNCH1_SIGNAL       0x04

/*===========================================================================*/

#if defined (PROCESSOR_STACK) && defined (SDAI_INCLUDE_SOCKET)
  #define OS_EVENT                      ((U16) 0x0001u)

  #define OS_EVENT_BIT_TIMER            0
  #define OS_EVENT_BIT_TASK_SPEC        1

  #define OS_EVENT_TIMER                (OS_EVENT << OS_EVENT_BIT_TIMER)

  #define FILTER_TYPE_UDP_PORT          0x0001u
  #define FILTER_TYPE_TCP_PORT          0x0002u
  #define FILTER_TYPE_IP_ADDR           0x0003u
#endif

/*---------------------------------------------------------------------------*/

#define ASSERT_CONCAT_(a, b) a##b
#define ASSERT_CONCAT(a, b) ASSERT_CONCAT_(a, b)
/* These can't be used after statements in c89. */
#ifdef __COUNTER__
  #define STATIC_ASSERT(e,m) \
    enum { ASSERT_CONCAT(static_assert_, __COUNTER__) = 1/(!!(e)) };
#else
  /* This can't be used twice on the same line so ensure if using in headers
   * that the headers are not included twice (by wrapping in #ifndef...#endif)
   * Note it doesn't cause an issue when used on same line of separate modules
   * compiled with gcc -combine -fwhole-program.  */
  #define STATIC_ASSERT(e,m) \
    enum { ASSERT_CONCAT(assert_line_, __LINE__) = 1/(!!(e)) };
#endif

/******************************************************************************
TYPEDEFS
******************************************************************************/

#ifdef SDAI_INCLUDE_SOCKET
  typedef int   SOCKET;

  typedef struct _T_ETH_FILTER_DATA
  {
    union
    {
      U16                   UdpPort;
      U16                   TcpPort;
      U32                   IpAddress;
    } UseAs;

  } T_ETH_FILTER_DATA;
#endif

/*===========================================================================*/

typedef enum _T_SORT_POSITION
{
  FIRST_ENTRY = 1,
  LAST_ENTRY,
  ASCENDING_SOURCE_OFFSET,
  ASCENDING_DESTINATION_OFFSET,
  ASCENDING_SELECTOR_VALUE

} T_SORT_POSITION;

/******************************************************************************
GLOBAL DATA
******************************************************************************/

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

extern T_SHARED_RAM_INTERFACE*    init_resources                          (int InterfaceSide, BOOL EnableSynchSignal);
extern void                       term_resources                          (int InterfaceSide);

extern void                       enable_sdai_interrupt                   (void);
extern void                       disable_sdai_interrupt                  (void);

extern void                       mutex_lock                              (T_INTERFACE_AREA) _LOCATED_INTO_FAST_MEMORY;
extern void                       mutex_unlock                            (T_INTERFACE_AREA) _LOCATED_INTO_FAST_MEMORY;
extern void                       data_mutex_lock                         (void);
extern void                       data_mutex_unlock                       (void);
extern void                       interrupt_lock                          (void);
extern void                       interrupt_unlock                        (void);

extern U8                         check_watchdog_supported                (void);
extern void                       init_watchdog_trigger                   (U16, void (*) (U32, void*));
extern void                       handle_appl_watchdog_expired            (void);
extern void                       handle_stack_watchdog_expired           (void);

extern U8                         wait_application_startup_finished       (void);

extern void                       create_stack_task                       (void (*) (void));
extern void                       create_stack_task_finished              (void);
extern void                       delete_stack_task                       (void);
extern void                       delete_stack_task_finish                (void);

extern void                       sdai_protocol_stack_task_main_loop      (void);

#ifdef SDAI_INCLUDE_SOCKET
  extern void                     create_socket_task                      (void (*) (void));
  extern void                     delete_socket_task                      (void);
  extern void                     delete_socket_task_finish               (void);
  extern void                     release_socket_task                     (void);

  #ifdef PROCESSOR_STACK
    extern void                   event_set                               (U16 Event);
    extern U16                    event_wait                              (U16 EventsToMatch);
    extern void                   start_timer                             (U32 Timeout, U8 Cyclic);
    extern void                   stop_timer                              (void);

    extern BOOL                   hardware_eth_set_filter                 (U16, T_ETH_FILTER_DATA*);
    extern BOOL                   hardware_eth_delete_filter              (U16, T_ETH_FILTER_DATA*);
  #endif
#endif

extern void                       hardware_get_mac_version                (U8*);
extern void                       porting_set_mac_addresses               (U8*, U8*, U8*);
extern U8*                        porting_get_peripheral_bridge_base      (void);

extern void                       send_signal_stack                       (U8 Signal) _LOCATED_INTO_FAST_MEMORY;
extern void                       send_signal_application                 (U8 Signal);

#ifndef convert_network_u32_to_processor_u32
extern U32                        convert_network_u32_to_processor_u32    (U32);
#endif

#ifndef convert_network_u16_to_processor_u16
extern U16                        convert_network_u16_to_processor_u16    (U16);
#endif

#ifndef convert_processor_u32_to_network_u32
extern U32                        convert_processor_u32_to_network_u32    (U32);
#endif

#ifndef convert_processor_u16_to_network_u16
extern U16                        convert_processor_u16_to_network_u16    (U16);
#endif

extern void                       sec_if_lock                             (void);
extern void                       sec_if_unlock                           (void);
extern void                       sec_if_transfer                         (U16, U8*, U8*);

extern BOOL                       perfopt_enabled                         (void);
extern void                       perfopt_config_input_interrupt          (U32, U32, BOOL, BOOL);
extern void                       perfopt_config_output_interrupt         (U32, U32, BOOL, BOOL);
extern void                       perfopt_update_input_descriptor         (void);
extern void                       perfopt_flush_input_descriptor          (BOOL);
extern void                       perfopt_update_output_descriptor        (void);
extern void                       perfopt_flush_output_descriptor         (BOOL);
extern void                       perfopt_add_input_descriptor            (U16, U16, U8, U16, BOOL, BOOL, T_SORT_POSITION);
extern void                       perfopt_add_output_descriptor           (U16, U16, U8, U16, BOOL, BOOL, T_SORT_POSITION);
extern void                       perfopt_init                            (void);

/*****************************************************************************/

#endif /* __PORTING_H__ */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
