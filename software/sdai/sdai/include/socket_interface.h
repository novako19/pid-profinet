/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

#ifndef __SOCKET_INTERFACE_H__
#define __SOCKET_INTERFACE_H__

/******************************************************************************
HOST SOCKET INTERFACE
******************************************************************************/

#define STATE_CHANGED_CALLBACK_SUPPORTED        0x0001u       /**< Bit to indicate application supports state changed cbk */
#define DATA_RECEIVED_CALLBACK_SUPPORTED        0x0002u       /**< Bit to indicate application supports data received cbk */

typedef struct _T_SOCKET_MANAGEMENT_IF
{
  U32   KeepAliveTimeout;
  U16   SupportedCallbacks;
  U8    Alignment [2];
  U8    Reserved [8];

} T_SOCKET_MANAGEMENT_IF;

/*=== SOCKET ADMIN INTERFACE ================================================*/

#define SOCKET_FLAG_USER_MASK                         0x0000FFFFuL
#define SOCKET_FLAG_CONNECTION_STATUS_UNCONNECTED     0x00000000uL
#define SOCKET_FLAG_CONNECTION_STATUS_CONNECTING      0x00010000uL
#define SOCKET_FLAG_CONNECTION_STATUS_CONNECTED       0x00020000uL
#define SOCKET_FLAG_CONNECTION_STATUS_MASK            (SOCKET_FLAG_CONNECTION_STATUS_UNCONNECTED | \
                                                      SOCKET_FLAG_CONNECTION_STATUS_CONNECTING   | \
                                                      SOCKET_FLAG_CONNECTION_STATUS_CONNECTED)

#define SOCKET_FLAG_SEND_STATUS_NO_ERROR              0x00000000uL
#define SOCKET_FLAG_SEND_STATUS_NO_BUFFER             0x00040000uL
#define SOCKET_FLAG_SEND_STATUS_HOST_UNREACHABLE      0x00080000uL
#define SOCKET_FLAG_SEND_STATUS_REJECTED              0x00100000uL
#define SOCKET_FLAG_SEND_STATUS_ERROR                 0x00200000uL
#define SOCKET_FLAG_SEND_STATUS_MASK                  (SOCKET_FLAG_SEND_STATUS_NO_ERROR        | \
                                                      SOCKET_FLAG_SEND_STATUS_NO_BUFFER        | \
                                                      SOCKET_FLAG_SEND_STATUS_HOST_UNREACHABLE | \
                                                      SOCKET_FLAG_SEND_STATUS_REJECTED         | \
                                                      SOCKET_FLAG_SEND_STATUS_ERROR)

#define SOCKET_FLAG_CONNECTION_NO_ERROR               0x00000000uL
#define SOCKET_FLAG_CONNECTION_ERROR_ADDRESS_IN_USE   0x00400000uL
#define SOCKET_FLAG_CONNECTION_INTERNAL_ERROR         0x00800000uL
#define SOCKET_FLAG_CONNECTION_ERROR_MASK             (SOCKET_FLAG_CONNECTION_NO_ERROR            | \
                                                      SOCKET_FLAG_CONNECTION_ERROR_ADDRESS_IN_USE | \
                                                      SOCKET_FLAG_CONNECTION_INTERNAL_ERROR)

#define SOCKET_FLAG_CREATED                           0x80000000uL

typedef struct _T_SOCKET_ADMIN_DATA
{
  U32   Flags;

  U32   LocalAddress;
  U32   RemoteAddress;

  U16   LocalPort;
  U16   RemotePort;

} T_SOCKET_ADMIN_DATA;

/*---------------------------------------------------------------------------*/

typedef struct _T_SOCKET_ADMIN_IF
{
  T_SOCKET_ADMIN_DATA   Socket [MAX_NUMBER_SUPPORTED_SOCKETS];

} T_SOCKET_ADMIN_IF;

/*=== SOCKET REQ INTERFACE ==================================================*/

/*--- APPL-EVENT-REQ --------------------------------------------------------*/

#define APPL_EVENT_CREATE_SOCKET_REQ_BIT              0
#define APPL_EVENT_CLOSE_SOCKET_REQ_BIT               1
#define APPL_EVENT_SOCKET_SEND_REQ_BIT                2
#define APPL_EVENT_CLOSE_CONNECTION_REQ_BIT           3
#define APPL_EVENT_ACCEPT_FILTER_REQ_BIT              4
#define APPL_EVENT_ADD_MULTICAST_REQ_BIT              5
#define APPL_EVENT_DEL_MULTICAST_REQ_BIT              6
#define APPL_EVENT_SOCKET_STATE_CHANGED_IND_ACK_BIT   7

#define APPL_EVENT_CREATE_SOCKET_REQ                  ((U32) (1 << APPL_EVENT_CREATE_SOCKET_REQ_BIT           ))
#define APPL_EVENT_CLOSE_SOCKET_REQ                   ((U32) (1 << APPL_EVENT_CLOSE_SOCKET_REQ_BIT            ))
#define APPL_EVENT_SOCKET_SEND_REQ                    ((U32) (1 << APPL_EVENT_SOCKET_SEND_REQ_BIT             ))
#define APPL_EVENT_CLOSE_CONNECTION_REQ               ((U32) (1 << APPL_EVENT_CLOSE_CONNECTION_REQ_BIT        ))
#define APPL_EVENT_ACCEPT_FILTER_REQ                  ((U32) (1 << APPL_EVENT_ACCEPT_FILTER_REQ_BIT           ))
#define APPL_EVENT_ADD_MULTICAST_REQ                  ((U32) (1 << APPL_EVENT_ADD_MULTICAST_REQ_BIT           ))
#define APPL_EVENT_DEL_MULTICAST_REQ                  ((U32) (1 << APPL_EVENT_DEL_MULTICAST_REQ_BIT           ))
#define APPL_EVENT_SOCKET_STATE_CHANGED_IND_ACK       ((U32) (1 << APPL_EVENT_SOCKET_STATE_CHANGED_IND_ACK_BIT))


#define APPL_EVENT_SOCKET_REQ_MASK                    (APPL_EVENT_CREATE_SOCKET_REQ           | \
                                                      APPL_EVENT_CLOSE_SOCKET_REQ             | \
                                                      APPL_EVENT_SOCKET_SEND_REQ              | \
                                                      APPL_EVENT_CLOSE_CONNECTION_REQ         | \
                                                      APPL_EVENT_ACCEPT_FILTER_REQ            | \
                                                      APPL_EVENT_ADD_MULTICAST_REQ            | \
                                                      APPL_EVENT_DEL_MULTICAST_REQ            | \
                                                      APPL_EVENT_SOCKET_STATE_CHANGED_IND_ACK )

/* adapt MAX_NUMBER_SOCKET_REQ_EVENTS to the real used number of events */
#define MAX_NUMBER_SOCKET_REQ_EVENTS                  (APPL_EVENT_SOCKET_STATE_CHANGED_IND_ACK_BIT + 1)

/*---------------------------------------------------------------------------*/

typedef struct _T_CLOSE_CONNECTION
{
  U32   IpAddress;

  U16   Port;
  U16   Socket;

} T_CLOSE_CONNECTION;

/*---------------------------------------------------------------------------*/

typedef struct _T_ACCEPT_FILTER
{
  U16   Socket;
  U16   NumberFilterEntries;

  U32   Filter [SOCK_MAX_NUMBER_FILTER_ENTRIES];

} T_ACCEPT_FILTER;

/*---------------------------------------------------------------------------*/

typedef struct _T_ADD_OR_DEL_MULTICAST
{
  U32   IpAddress;

  U16   Alignment;
  U16   Socket;

} T_ADD_OR_DEL_MULTICAST;

/*---------------------------------------------------------------------------*/

typedef struct _T_SOCKET_REQ_IF
{
  volatile U32                  EventReg;

  U8                            CreateSocket [8];
  U8                            CloseSocket [8];
  T_CLOSE_CONNECTION            Connection;
  T_ACCEPT_FILTER               AcceptFilter;
  T_ADD_OR_DEL_MULTICAST        AddMulticast;
  T_ADD_OR_DEL_MULTICAST        DelMulticast;

  U16                           FifoReadOffset;
  U16                           FifoWriteOffset;

  U32                           PacketsRead;
  U32                           PacketsWritten;

  U8                            Alignment [60];

} T_SOCKET_REQ_IF; /* sizeof() == 128 */

/*=== SOCKET IND INTERFACE ==================================================*/

/*--- APPL-EVENT-IND --------------------------------------------------------*/

#define APPL_EVENT_SOCKET_STATE_CHANGED_IND_BIT       0
#define APPL_EVENT_SOCKET_DATA_RECEIVED_IND_BIT       1

#define APPL_EVENT_SOCKET_STATE_CHANGED_IND           ((U32) (1 << APPL_EVENT_SOCKET_STATE_CHANGED_IND_BIT))
#define APPL_EVENT_SOCKET_DATA_RECEIVED_IND           ((U32) (1 << APPL_EVENT_SOCKET_DATA_RECEIVED_IND_BIT))

/* adapt MAX_NUMBER_SOCKET_IND_EVENTS to the real used number of events */
#define MAX_NUMBER_SOCKET_IND_EVENTS                  (APPL_EVENT_SOCKET_DATA_RECEIVED_IND_BIT + 1)

#define APPL_EVENT_SOCKET_IND_MASK                    (APPL_EVENT_SOCKET_STATE_CHANGED_IND | \
                                                       APPL_EVENT_SOCKET_DATA_RECEIVED_IND)

/*---------------------------------------------------------------------------*/

#define CONNECTION_STATUS_NO_ERROR                    0x0000u
#define CONNECTION_STATUS_CONNECTED                   0x0001u
#define CONNECTION_STATUS_INTERNAL_ERROR              0x0002u
#define CONNECTION_STATUS_CLOSED_REMOTELY             0x0003u
#define CONNECTION_STATUS_ABORTED_REMOTELY            0x0004u
#define CONNECTION_STATUS_TIMEOUT                     0x0005u
#define CONNECTION_STATUS_CONNECT_REJECTED            0x0006u
#define CONNECTION_STATUS_OFFLINE                     0x0007u
#define CONNECTION_STATUS_CONNECTING                  0x0008u
#define CONNECTION_STATUS_CLOSED_LOCALLY              0x0009u

typedef struct _T_CONNECTION
{
  U32   IpAddress;

  U16   Port;
  U16   Status;

} T_CONNECTION;

/*---------------------------------------------------------------------------*/

typedef struct _T_SOCKET_STATE
{
  U16             Socket;
  U16             Alignment;

  T_CONNECTION    Connection [MAX_NUMBER_CONNECTIONS_PER_SOCKET];

} T_SOCKET_STATE;

/*---------------------------------------------------------------------------*/

typedef struct _T_SOCKET_IND_IF
{
  volatile U32            EventReg;

  T_SOCKET_STATE          SocketState;

  U16                     FifoReadOffset;
  U16                     FifoWriteOffset;

  U32                     PacketsRead;
  U32                     PacketsWritten;

  U8                      Alignment [88];

} T_SOCKET_IND_IF; /* sizeof() == 128 */

/*=== APPLICATTION SOCKET INTERFACE =========================================*/

#define INTERNAL_SOCKET_FIFO_SIZE (U16)(SOCKET_FIFO_SIZE + sizeof (T_APPL_SOCKET_DATA_HEADER))

typedef struct _T_APPL_SOCKET_DATA_HEADER
{
  U16   Socket;
  U16   DataLength;

  U32   RemoteAddress;

  U16   RemotePort;
  U16   Alignment;

} T_APPL_SOCKET_DATA_HEADER;

typedef struct _T_SOCKET_INTERFACE
{
  T_SOCKET_MANAGEMENT_IF    Management;                                /* 0x0000 - 0x000F,    16   */

  T_SOCKET_ADMIN_IF         Admin;                                     /* 0x0010 - 0x040F,    1024 */

  T_SOCKET_REQ_IF           SocketReq;                                 /* 0x60EC - 0x61EB,   256 */
  T_SOCKET_IND_IF           SocketInd;                                 /* 0x61EC - 0x63AB,   448 */

  U8                        ApplFifoData [INTERNAL_SOCKET_FIFO_SIZE];  /* 0x870C - 0x???? */
  U8                        StackFifoData [INTERNAL_SOCKET_FIFO_SIZE]; /* 0x870C - 0x???? */

} T_SOCKET_INTERFACE;

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

extern void sock_if_receive_signal (void);
extern void sdai_socket_if_stop (void);
extern void sdai_socket_if_start (T_SDAI_SOCK_NETWORK_SETTINGS* pNetworkSettings);

#endif /* __SOCKET_INTERFACE_H__ */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */

