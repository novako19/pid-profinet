/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

#ifndef __SDAI_EXT_H__
#define __SDAI_EXT_H__

/******************************************************************************
DEFINES
******************************************************************************/

/*------------------------------*/
/* extensions of the SDAI       */
/*------------------------------*/

#define   sdai_extension_init(pApplicationInitData)       SDAI_SUCCESS
#define   sdai_extension_deinit()                         SDAI_SUCCESS

/*------------------------------*/
/* customer-specific extensions */
/*------------------------------*/

/******************************************************************************
TYPEDEFS
******************************************************************************/

/*------------------------------*/
/* extensions of the SDAI       */
/*------------------------------*/

typedef enum _T_IP_CHECK_RESULT
{
  IP_SETTINGS_OK = 0,
  INVALID_IP_ADDRESS,
  INVALID_SUBNETMASK,
  IP_ADDRESS_AND_ROUTER_MISMATCH,
  INVALID_ROUTER_ADDRESS

} T_IP_CHECK_RESULT;

/*------------------------------*/
/* customer-specific extensions */
/*------------------------------*/

/******************************************************************************
FUNCTIONS
******************************************************************************/

/*------------------------------*/
/* extensions of the SDAI       */
/*------------------------------*/

extern T_IP_CHECK_RESULT sdai_check_ip_settings             (U32 IpAddress, U32 NetMask, U32 DefaultRouter);
extern U8                sdai_pn_verify_device_name         (U8*);

/*------------------------------*/
/* customer-specific extensions */
/*------------------------------*/

#endif /* __SDAI_EXT_H__ */

/*****************************************************************************/
