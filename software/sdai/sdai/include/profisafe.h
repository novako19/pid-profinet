/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

#ifndef __PROFISAFE_H__
#define __PROFISAFE_H__

/******************************************************************************
DEFINES
******************************************************************************/

/******************************************************************************
TYPEDEFS
******************************************************************************/

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

#define profisafe_init(pInstanceData)                                         \
  do { _AVOID_UNUSED_WARNING (pInstanceData); } while (0)

#define profisafe_module_init(pInstanceData, Slot, SubSlot, DataLength)       \
  do { _AVOID_UNUSED_WARNING (pInstanceData);                                 \
    _AVOID_UNUSED_WARNING (Slot);                                             \
    _AVOID_UNUSED_WARNING (SubSlot);                                          \
    _AVOID_UNUSED_WARNING (DataLength); } while (0)

#define profisafe_update_output_data(pData, Status, Slot, SubSlot, Length)    \
  do { _AVOID_UNUSED_WARNING (pData);                                         \
    _AVOID_UNUSED_WARNING (Status);                                           \
    _AVOID_UNUSED_WARNING (Slot);                                             \
    _AVOID_UNUSED_WARNING (SubSlot);                                          \
    _AVOID_UNUSED_WARNING (Length); } while (0)

#define profisafe_update_input_data(pInstanceData, pData, pStatus, Slot, SubSlot, Length) \
  do { _AVOID_UNUSED_WARNING (pInstanceData);                                             \
    _AVOID_UNUSED_WARNING (Slot);                                                         \
    _AVOID_UNUSED_WARNING (SubSlot);                                                      \
    *pStatus = PNAK_IOXS_STATUS_DATA_BAD;                                                 \
    memset (pData, 0, Length); } while (0)

#define profisafe_set_parameter_data(pInstanceData, Slot, SubSlot, Index, Length, pData) \
  do { _AVOID_UNUSED_WARNING (pInstanceData);                                            \
    _AVOID_UNUSED_WARNING (Slot);                                                        \
    _AVOID_UNUSED_WARNING (SubSlot);                                                     \
    _AVOID_UNUSED_WARNING (Index);                                                       \
    _AVOID_UNUSED_WARNING (Length);                                                      \
    _AVOID_UNUSED_WARNING (pData); } while (0)

#define profisafe_get_parameter_data(pInstanceData, Slot, SubSlot, Index, Length) \
  do { _AVOID_UNUSED_WARNING (pInstanceData);                                     \
    _AVOID_UNUSED_WARNING (Slot);                                                 \
    _AVOID_UNUSED_WARNING (SubSlot);                                              \
    _AVOID_UNUSED_WARNING (Index);                                                \
    _AVOID_UNUSED_WARNING (Length); } while (0)

#define profisafe_alarm_ack(pInstanceData, Slot, SubSlot, Priority, Specifier, AlarmType) \
  do { _AVOID_UNUSED_WARNING (pInstanceData);                                             \
    _AVOID_UNUSED_WARNING (Slot);                                                         \
    _AVOID_UNUSED_WARNING (SubSlot);                                                      \
    _AVOID_UNUSED_WARNING (Priority);                                                     \
    _AVOID_UNUSED_WARNING (Specifier);                                                    \
    _AVOID_UNUSED_WARNING (AlarmType); } while (0)

#define profisafe_set_data_available(Status) \
  do { _AVOID_UNUSED_WARNING (Status); } while (0)

/*****************************************************************************/

#endif /* __PROFISAFE_H__ */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
