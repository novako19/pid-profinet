/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

#ifndef __SDAI_DRV_H__
#define __SDAI_DRV_H__

/******************************************************************************
DEFINES
******************************************************************************/

/******************************************************************************
TYPEDEFS
******************************************************************************/

struct _T_SDAI_BACKEND_DRIVER;

/** \brief This structure holds the function pointers from the used driver */
typedef struct _I_SDAI_BACKEND_DRIVER
{
  /* user interface */
  U8    (*deinit)                         (struct _T_SDAI_BACKEND_DRIVER*);                                                    /**< Function pointer to the deinit function from the driver */
  U8    (*get_version)                    (struct _T_SDAI_BACKEND_DRIVER*, struct SDAI_VERSION_DATA*);                         /**< Function pointer to the get_version function from the driver */
  U8    (*get_state)                      (struct _T_SDAI_BACKEND_DRIVER*, U16*, struct SDAI_ADDSTATUS_DATA*);                 /**< Function pointer to the get_state function from the driver */
  U8    (*plug_unit)                      (struct _T_SDAI_BACKEND_DRIVER*, U8, U8, U8, U32*, const struct SDAI_CFG_DATA_EXT*); /**< Function pointer to the plug_unit function from the driver */
  U8    (*pull_unit)                      (struct _T_SDAI_BACKEND_DRIVER*, U32);                                               /**< Function pointer to the pull_unit function from the driver */

  /* data interface */
  U8    (*get_data)                       (struct _T_SDAI_BACKEND_DRIVER*, U32, U8*, U8*);      /**< Function pointer to the get_data function from the driver */
  U8    (*set_data)                       (struct _T_SDAI_BACKEND_DRIVER*, U32, U8, const U8*); /**< Function pointer to the set_data function from the driver */
  U8    (*process_data_update_finished)   (struct _T_SDAI_BACKEND_DRIVER*);                     /**< Function pointer to */

  /* service interface */
  U8    (*alarm_request)                  (struct _T_SDAI_BACKEND_DRIVER*, const struct SDAI_ALARM_REQ*);     /**< Function pointer to the alarm_request function from the driver */
  U8    (*diagnosis_request)              (struct _T_SDAI_BACKEND_DRIVER*, const struct SDAI_DIAGNOSIS_REQ*); /**< Function pointer to the diagnosis_request function from the driver */
  U8    (*write_response)                 (struct _T_SDAI_BACKEND_DRIVER*, const struct SDAI_WRITE_RES*);     /**< Function pointer to the write_response function from the driver */
  U8    (*read_response)                  (struct _T_SDAI_BACKEND_DRIVER*, const struct SDAI_READ_RES*);      /**< Function pointer to the read_response function from the driver */
  U8    (*control_response)               (struct _T_SDAI_BACKEND_DRIVER*, const struct SDAI_CONTROL_RES*);   /**< Function pointer to the control_response function from the driver */

  U8    (*proto_start)                    (struct _T_SDAI_BACKEND_DRIVER*);                  /**< Function pointer to the proto_start function from the driver */
  int   (*plugging_complete)              (struct _T_SDAI_BACKEND_DRIVER*);                  /**< Function pointer to the plugging_complete function from the driver */

  U8    (*init_watchdog)                  (struct _T_SDAI_BACKEND_DRIVER*, const U16);       /**< Function pointer to */
  U8    (*check_trigger_watchdog)         (struct _T_SDAI_BACKEND_DRIVER*);                  /**< Function pointer to */

  U8    (*lock_resources)                 (struct _T_SDAI_BACKEND_DRIVER*, U16);             /**< Function pointer to */
  U8    (*unlock_resources)               (struct _T_SDAI_BACKEND_DRIVER*, U16);             /**< Function pointer to */

} I_SDAI_BACKEND_DRIVER;

/*===========================================================================*/

/** \brief This structure holds all data from the driver needed outside the driver */
typedef struct _T_SDAI_BACKEND_DRIVER
{
  I_SDAI_BACKEND_DRIVER   Interface;           /**< Holds the functions from the used driver module */

  struct SDAI_INIT        ApplicationInitData; /**< Local copy of the init data passed by the application */

  void*                   pPrivateData;        /**< Holds the private data structure from the used driver module */

} T_SDAI_BACKEND_DRIVER;

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

#ifdef SDAI_INCLUDE_PNAK
  extern U8   sdai_drv_pn_init            (T_SDAI_BACKEND_DRIVER*, struct SDAI_INIT* pInitData);
#else
  #define sdai_drv_pn_init(pSdaiBackendDriver, pApplicationInitData)        SDAI_ERR_PROTOCOL_NOT_SUPPORTED
#endif

/*===========================================================================*/

#ifdef SDAI_INCLUDE_EIP
  extern U8   sdai_drv_eip_init           (T_SDAI_BACKEND_DRIVER*, struct SDAI_INIT* pInitData);
#else
  #define sdai_drv_eip_init(pSdaiBackendDriver, pApplicationInitData)       SDAI_ERR_PROTOCOL_NOT_SUPPORTED
#endif

/*===========================================================================*/

#ifdef SDAI_INCLUDE_ECAT
  extern U8   sdai_drv_ecat_init                (T_SDAI_BACKEND_DRIVER*, struct SDAI_INIT* pInitData);
#else
  #define sdai_drv_ecat_init(pSdaiBackendDriver, pApplicationInitData)      SDAI_ERR_PROTOCOL_NOT_SUPPORTED
#endif

/*===========================================================================*/

#ifdef SDAI_INCLUDE_EPL
  extern U8   sdai_drv_epl_init                (T_SDAI_BACKEND_DRIVER*, struct SDAI_INIT* pInitData);
#else
  #define sdai_drv_epl_init(pSdaiBackendDriver, pApplicationInitData)       SDAI_ERR_PROTOCOL_NOT_SUPPORTED
#endif

/*===========================================================================*/

#ifdef SDAI_INCLUDE_MBAK
  extern U8   sdai_drv_mb_init        (T_SDAI_BACKEND_DRIVER*, struct SDAI_INIT* pInitData);
#else
  #define sdai_drv_mb_init(pSdaiBackendDriver, pApplicationInitData)        SDAI_ERR_PROTOCOL_NOT_SUPPORTED
#endif

/*===========================================================================*/

#ifdef SDAI_INCLUDE_PB_DP
  extern U8   sdai_drv_pbdp_init          (T_SDAI_BACKEND_DRIVER*, struct SDAI_INIT* pInitData);
#else
  #define sdai_drv_pbdp_init(pSdaiBackendDriver, pApplicationInitData)      SDAI_ERR_PROTOCOL_NOT_SUPPORTED
#endif

/*===========================================================================*/

#ifdef SDAI_INCLUDE_SOCKET
  extern T_SOCKET_RESULT      sdai_drv_sock_init                (T_SDAI_SOCK_INIT* pSocketInit);
  extern T_SOCKET_RESULT      sdai_drv_sock_term                (void);
  extern T_SOCKET_RESULT      sdai_drv_sock_create              (U32 Flags, T_SDAI_SOCK_ADDR* pRemoteAddress, T_SDAI_SOCK_ADDR* pLocalAddress, unsigned short* pSocket);
  extern T_SOCKET_RESULT      sdai_drv_sock_close               (U16 Socket);
  extern T_SOCKET_RESULT      sdai_drv_sock_send                (U16 Socket, T_SDAI_SOCK_ADDR* pRemoteAddress, U16 DataLength, const unsigned char* pData);
  extern T_SOCKET_RESULT      sdai_drv_sock_receive             (U16 Socket, T_SDAI_SOCK_ADDR* pRemoteAddress, U16* pDataLength, unsigned char* pData);
  extern T_SOCKET_RESULT      sdai_drv_sock_ioctl               (U16 Socket, T_SDAI_SOCK_IO_CONTROL* pIOControl);

  extern void                 sdai_drv_sock_handle_socket_ind   (U16 Signal);
#endif

#endif /* __SDAI_DRV_H__ */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
