/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

#ifndef __SDAI_INTERFACE_H__
#define __SDAI_INTERFACE_H__

/******************************************************************************
DEFINES and TYPEDEFS
******************************************************************************/

#define INTERFACE_SIGNAL_START_STACK            0x0001u /**< Signal start to the stack */
#define INTERFACE_SIGNAL_STOP_STACK             0x0002u /**< Signal stop to the stack */
#define INTERFACE_SIGNAL_SERVICE_IND            0x0004u /**< Signal service indication */
#define INTERFACE_SIGNAL_SERVICE_REQ_RES        0x0008u /**< Signal service request response */
#define INTERFACE_SIGNAL_INIT_WATCHDOG          0x0010u /**< Signal start watchdog */
#define INTERFACE_SIGNAL_UPDATE_INPUT_DATA      0x0020u /**< Signal new input data */
#define INTERFACE_SIGNAL_SERVICE_CHANNEL_FREE   0x0040u /**< Signal service channel is free */
#define INTERFACE_SIGNAL_DATA_IND               0x0080u /**< Signal Data indication (e.g. Data changed) */
#define INTERFACE_SIGNAL_DATA_UPDATE            0x0100u /**< Signal Data update (e.g. Data changed) */

#ifdef SDAI_INCLUDE_SOCKET
#define INTERFACE_SIGNAL_SOCKET_REQ             0x0100u /**< Signal a socket service request */
#define INTERFACE_SIGNAL_SOCKET_IND             0x0200u /**< Signal a socket service indication */
#endif

#define INTERFACE_APPLICATION                   0       /**< Indicates a call from the application side of the interface */
#define INTERFACE_STACK                         1       /**< Indicates a call from the stack side of the interface */

/*---------------------------------------------------------------------------*/

#define SERVICE_SIZE           SDAI_SERVICE_SIZE                              /**< The size of the service channels in the interface */
#define INTERFACE_IO_DATA_SIZE (SDAI_MAX_IO_DATA_SIZE + (2 * SDAI_MAX_UNITS)) /**< The size of the IO data area in the interface */

/*---------------------------------------------------------------------------*/

/** \brief Defines for the different areas in the shared ram */
typedef enum _T_INTERFACE_AREA
{
  MANAGEMENT_INTERFACE = 1,  /**< Specifies the management ares */
  STATUS_INTERFACE,          /**< Specifies the status area */
  SERVICE_REQ_RES_INTERFACE, /**< Specifies the req/res channel */
  SERVICE_IND_INTERFACE,     /**< Specifies the indication channel */
  UNIT_INTERFACE,            /**< Specifies the units area */
  RESOURCE_INTERFACE,        /**< Specifies the resources area */
  SHARED_MDIO_INTERFACE,     /**< Specifies the shared mdio area */
  SOCKET_INTERFACE,          /**< Specifies the socket interface */

  MAX_NUMBER_INTERFACE_AREAS /**< The number of areas in the shared ram */

} T_INTERFACE_AREA;

/*---------------------------------------------------------------------------*/

#ifndef SDAI_INTERFACE_ALLOWED_CALLBACK_MASK
#define SDAI_INTERFACE_ALLOWED_CALLBACK_MASK    0xFFFFu /**< */
#endif

/*---------------------------------------------------------------------------*/

#define IDENT_CALLBACK_SUPPORTED        0x0001u         /**< Bit to indicate application supports ident cbk */
#define ALARM_ACK_CALLBACK_SUPPORTED    0x0002u         /**< Bit to indicate application supports alarm ack cbk */
#define EXCEPTION_CALLBACK_SUPPORTED    0x0004u         /**< Bit to indicate application supports exception cbk */
#define ALARM_CALLBACK_SUPPORTED        0x0008u         /**< Bit to indicate application supports alarm cbk (for future use) */
#define WRITE_CALLBACK_SUPPORTED        0x0010u         /**< Bit to indicate application supports write cbk */
#define READ_CALLBACK_SUPPORTED         0x0020u         /**< Bit to indicate application supports read cbk */
#define DATA_CALLBACK_SUPPORTED         0x0040u         /**< Bit to indicate application supports data cbk */
#define CONTROL_CALLBACK_SUPPORTED      0x0080u         /**< Bit to indicate application supports control cbk */
#define SYNC_SIGNAL_CALLBACK_SUPPORTED  0x0100u         /**< Bit to indicate application supports sync signal cbk */

/*---------------------------------------------------------------------------*/

#define SDAI_INTERFACE_APPLICATION_READY        0xA5    /**< Indicates application ready. The stack starts init phase */
#define SDAI_INTERFACE_STACK_READY              0x5A    /**< Indicates stack ready after stack finish init phase */
#define SDAI_INTERFACE_PROTOCOL_NOT_SUPPORTED   0x55    /**< Indicates invalid protocol after stack finish init phase */

/*---------------------------------------------------------------------------*/

#ifndef SDAI_INTERRUPT_DEFAULT_PULSE_WIDTH
#define SDAI_INTERRUPT_DEFAULT_PULSE_WIDTH    0x00u
#endif

/*===========================================================================*/

/** \brief Holds the management informations in the interface */
typedef struct _T_INTERFACE_MANAGEMENT_DATA
{
  U16           SupportedCallbacks;                           /**< Bitfield indicates the callbacks supported by the application */

  U16           NumberOfUnits;                                /**< Number of plugged units */
  S16           RemainingDataSize;                            /**< Remaining free size for the IO data */

  volatile U8   StackReady;                                   /**< signals stack ready to the application */
  volatile U8   ApplReady;                                    /**< singals appl ready to the stack */

  volatile U16  ByteOrdering;                                 /**< for future use */

  volatile U16  InterruptSignalStack;                         /**< Signals from stack to application */
  volatile U16  InterruptSignalAppl;                          /**< Signals from application to stack */

  volatile U16  WatchdogFactor;                               /**< Current used watchdog factor */
  volatile U8   WatchdogFlagApplication;                      /**< Flag for application watchdog */
  volatile U8   WatchdogFlagStack;                            /**< Flag for stack watchdog */
  volatile U8   WatchdogRunning;                              /**< Status of watchdog */

  volatile U8   BackEnd;                                      /**< specifies the backend protocol.
                                                                   Valid values for the BackEnd: \ref BackEnd */

} T_INTERFACE_MANAGEMENT_DATA; /* sizeof () = 20 */

/*===========================================================================*/

/** \brief Holds the status informations in the interface */
typedef struct _T_INTERFACE_STATUS_DATA
{
  U16                        StackStatus;                      /**< Holds the current state of the protocoll stack */
  U8                         PulseWidth;                       /**< Configures the pulse width of the interrupt in microseconds, 0 mean falling/rising edge for IRQ */
  U8                         InterfaceVersion;                 /**< The version of the SDAI interface the stack expects */

  struct SDAI_ADDSTATUS_DATA StackAddStatus;                   /**< Holds additional status informations over the stack */
  struct SDAI_VERSION_DATA   VersionData;                      /**< Holds the version string of the protocoll stack */

} T_INTERFACE_STATUS_DATA; /* sizeof () = 596 */

/*===========================================================================*/

/** \brief Holds the informations for a init request in the interface */
typedef struct _T_SDAI_SERVICE_INIT
{
  struct SDAI_IDENT_DATA    Ident;                    /**< Holds the ident data */
  struct SDAI_DEVICE_DATA   Device;                   /**< Holds the device data */

} T_SDAI_SERVICE_INIT;

/*---------------------------------------------------------------------------*/

/** \brief Holds the informations for a plug unit request in the interface */
typedef struct _T_SDAI_SERVICE_PLUG_UNIT
{
  U16                        UnitIndex;                  /**< Index of the unit within the unit intrerface */
  U8                         Alignment[2];
  struct SDAI_CFG_DATA_EXT   CfgData;                    /**< Holds the configuration data */

} T_SDAI_SERVICE_PLUG_UNIT;

/*---------------------------------------------------------------------------*/

/** \brief Holds the informations for a pull unit request in the interface */
typedef struct _T_SDAI_SERVICE_PULL_UNIT
{
  U16                     UnitIndex;                  /**< Index of the unit within the unit intrerface */
  U8                      Alignment[2];
  struct SDAI_CFG_DATA    CfgData;                    /**< Holds the configuration data */

} T_SDAI_SERVICE_PULL_UNIT;

/*===========================================================================*/

#define SERVICE_REQ_RES_FREE                                1u            /**< indicates service req res channel is free */
#define SERVICE_REQ_RES_ALARM                               2u            /**< indicates an alarm req service */
#define SERVICE_REQ_RES_DIAGNOSIS                           3u            /**< indicates a diagnosis req service */
#define SERVICE_REQ_RES_WRITE                               4u            /**< indicates a write res service */
#define SERVICE_REQ_RES_READ                                5u            /**< indicates a read res service */
#define SERVICE_REQ_RES_INIT                                6u            /**< indicates an init req service */
#define SERVICE_REQ_RES_CONTROL                             7u            /**< indicates an control res service */
#define SERVICE_REQ_RES_PLUG_UNIT                           8u            /**< indicates a plug unit req service */
#define SERVICE_REQ_RES_PULL_UNIT                           9u            /**< indicates a pull unit req service */

#define SERVICE_REQ_RES_RESULT_OK                           0u
#define SERVICE_REQ_RES_RESULT_ERR_WRONG_STATE              1u
#define SERVICE_REQ_RES_RESULT_ERR_INVALID_PARAMETER        2u
#define SERVICE_REQ_RES_RESULT_ERR_NO_PARALLEL_SERVICES     3u
#define SERVICE_REQ_RES_RESULT_ERR_NOT_SUPPORTED            4u

/*---------------------------------------------------------------------------*/

/** \brief Holds the information of an service request response in the interface */
typedef struct _T_INTERFACE_SERVICE_REQ_RES
{
  U16                           ServiceType;            /**< Indicates the service type currently stored in the structure */

  U8                            ServiceResult;          /**< Indicates the local result of the service */
  U8                            Alignment;

  /** \brief Union to handle the different services */
  union
  {
    U8                          Service [SERVICE_SIZE]; /**< Defines the minimum size of the service req res channel */
    T_SDAI_SERVICE_INIT         Init;                   /**< Holds the data for an init request service */
    T_SDAI_SERVICE_PLUG_UNIT    PlugUnit;               /**< Holds the data for a plug unit request */
    T_SDAI_SERVICE_PULL_UNIT    PullUnit;               /**< Holds the data for a pull unit request */
    struct SDAI_ALARM_REQ       Alarm;                  /**< Holds the data for an alarm request service */
    struct SDAI_DIAGNOSIS_REQ   Diagnosis;              /**< Holds the data for a diagnosis request service */
    struct SDAI_WRITE_RES       Write;                  /**< Holds the data for a write response service */
    struct SDAI_READ_RES        Read;                   /**< Holds the data for a read response service */
    struct SDAI_CONTROL_RES     Control;                /**< Holds the data for a control response service */

  } UseAs;

} T_INTERFACE_SERVICE_REQ_RES; /* sizeof () = 516 */

/*===========================================================================*/

#define SERVICE_IND_FREE                1u            /**< Indicates service ind channel is free     */
#define SERVICE_IND_IDENT               2u            /**< Indicates an ident indication service     */
#define SERVICE_IND_EXCEPTION           3u            /**< Indicates an exception indication service */
#define SERVICE_IND_WRITE               4u            /**< Indicates a write indication service      */
#define SERVICE_IND_READ                5u            /**< Indicates a read indication service       */
#define SERVICE_IND_ALARM_ACK           6u            /**< Indicates an alarm ack indication service */
#define SERVICE_IND_ALARM               7u            /**< Indicates an alarm indication service (for future use) */
#define SERVICE_IND_CONTROL             8u            /**< Indicates a control indication service    */

/*---------------------------------------------------------------------------*/

/** \brief Holds the informations for a service indication in the interface */
typedef struct _T_INTERFACE_SERVICE_IND
{
  U16                            ServiceType;            /**< Indicates the service type currently stored in the structure */

  U8                             ServicesPending;        /**< Indicates that there is at least one more service pending from stack */
  U8                             Alignment;

  /** \brief Union to handle the different services */
  union
  {
    U8                           Service [SERVICE_SIZE]; /**< Defines the minimum size of the service indication channel */
    struct SDAI_IDENT_DATA       Ident;                  /**< Holds the data for an ident indication */
    struct SDAI_EXCEPTION_DATA   Exception;              /**< Holds the data for an exception indication */
    struct SDAI_ALARM_ACK_IND    AlarmAck;               /**< Holds the data for an alarm ack indication */
    struct SDAI_WRITE_IND        Write;                  /**< Holds the data for a write indication */
    struct SDAI_READ_IND         Read;                   /**< Holds the data for a read indication */
    struct SDAI_CONTROL_IND      Control;                /**< Holds the data for a control indication */

  } UseAs;

} T_INTERFACE_SERVICE_IND; /* sizeof () = 516 */

/*===========================================================================*/

#define UNIT_INTERNAL_FLAG_OUTPUT_CHANGED       0x01   /* signals changed output data for this unit */
#define UNIT_INTERNAL_FLAG_IN_USE               0x02   /* unit is in use */
#define UNIT_INTERNAL_FLAG_CFG_MISMATCH         0x04   /* the module configuration received from the remote controller
                                                          is different as the actually plugged module from the SDAI application */
#define UNIT_INTERNAL_FLAG_INTERFACE_SECONDARY  0x10   /* data are transfered to secondary interface */
#define UNIT_INTERNAL_FLAG_SUBSTITUTION_ACTIVE  0x20   /* substitute data are passed to the application */
#define UNIT_INTERNAL_FLAG_PERIPHERAL           0x40   /* unit uses the peripheral interface to access io data */

/*---------------------------------------------------------------------------*/

#define _SDAI_PN_EXTENTION_TO_INPUT_LENGTH(Type, Extention) \
                                                (Type) ((Extention) & 0x00FFu) /* low byte is used as real length */
#define _SDAI_PN_EXTENTION_TO_OUTPUT_LENGTH(Type, Extention) \
                                                (Type) (((Extention) >> 8) & 0x00FFu) /* high byte is used as real length */

#define _SDAI_PN_OUTPUT_AND_INPUT_LENGTH_TO_EXTENTION(OutputLength, InputLength) \
                                                (((OutputLength) << 8) | (InputLength)) /* convert input/output length to protocol extention */

/*---------------------------------------------------------------------------*/

/** \brief Holds the informations of one unit in the interface */
typedef struct _T_INTERFACE_UNIT
{
  U32   Id;                             /**< The unique Id of the unit */

  U16   ProtocolExtention;              /**< Protocol specific data of the unit */
  U8    Type;                           /**< The type of the unit */

  U8    StatusInput;                    /**< The status of the input data */
  U8    StatusOutput;                   /**< The status of the output data */

  U8    DataLengthInput;                /**< The input data length of the unit */
  U8    DataLengthOutput;               /**< The output data length of the unit*/

  U8    InternalFlags;                  /**< Holding internal status of the unit */

  U16   DataInputOffset;                /**< Pointer to the IO data of the unit */
  U16   DataOutputOffset;               /**< Pointer to the IO data of the unit */

} T_INTERFACE_UNIT; /* sizeof () = 16 */

/*===========================================================================*/

/** \brief Defines the layout of the shared ram */
typedef struct _T_SHARED_RAM_INTERFACE
{
  T_INTERFACE_MANAGEMENT_DATA   ManagementData;                        /**< Management area of the shared ram */         /* 0x0000 - 0x0013, 20 */
  T_INTERFACE_STATUS_DATA       StatusData;                            /**< Status area of the shared ram */             /* 0x0014 - 0x0267, 596 */

  T_INTERFACE_SERVICE_REQ_RES   ServiceReqRes;                         /**< Service req res channel of the shared ram */ /* 0x0268 - 0x046B, 516 */
  T_INTERFACE_SERVICE_IND       ServiceInd;                            /**< Service ind channel of the shared ram */     /* 0x046C - 0x066F, 516 */

  T_INTERFACE_UNIT              Units [SDAI_MAX_UNITS];                /**< Units area of the shared ram */              /* 0x0674 - 0x086F, 1040 */

  U8                            ProcessData [INTERFACE_IO_DATA_SIZE];  /**< Process data area of the shared ram */       /* 0x0A84 - 0x0F05, 1154 */

  U8                            ProviderStatusGood;
  U8                            ProviderStatusBad;

} T_SHARED_RAM_INTERFACE;

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

#ifndef _LOCATED_INTO_FAST_MEMORY
#define _LOCATED_INTO_FAST_MEMORY
#endif

extern U8                   interface_wait_application_startup_finished   (void);
extern void                 interface_stack_initialized                   (U8);

extern U8                   interface_init                                (int (*) (U16, void*), int (*) (U8, void*), int, void*, U8);
extern void                 interface_deinit                              (int);
extern void                 interface_protocol_supported                  (void);
extern void                 interface_protocol_not_supported              (void);

extern void*                interface_lock_data                           (T_INTERFACE_AREA) _LOCATED_INTO_FAST_MEMORY;
extern void                 interface_unlock_data                         (T_INTERFACE_AREA) _LOCATED_INTO_FAST_MEMORY;

extern U8*                  interface_get_dpram_base                      (void);
extern U8*                  interface_get_iodata_base                     (void);
extern U8*                  interface_get_status_good_address             (void);
extern T_INTERFACE_UNIT*    interface_get_free_unit                       (U8, U8, U16*);
extern T_INTERFACE_UNIT*    interface_lock_unit                           (U16) _LOCATED_INTO_FAST_MEMORY;
extern void                 interface_unlock_unit                         (U16) _LOCATED_INTO_FAST_MEMORY;
extern U16                  interface_find_unit                           (U32, U16);
extern U16                  interface_find_free_unit                      (U16);

extern void                 interface_send_signal                         (int, U16) _LOCATED_INTO_FAST_MEMORY;
extern void                 interface_receive_signal                      (int);
extern void                 interface_receive_data_signal                 (int);
extern void                 interface_receive_synch_signal                (U8);

#endif /* _SDAI_INTERFACE_H__ */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */

