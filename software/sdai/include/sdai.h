/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

/******************************************************************************
MAINPAGE
******************************************************************************/

/**
 * @mainpage
 *
 * \copyright
 * 2005-2015 SOFTING Industrial Automation GmbH @n @n
 * No part of these instructions may be reproduced (printed material, photocopies,
 * microfilm or other method) or processed, copied or distributed using electronic
 * systems in any form whatsoever without prior written permission of SOFTING Industrial
 * Automation GmbH. The producer reserves the right to make changes to the scope of supply
 * as well as changes to technical data, even without prior notice. A great deal of attention
 * was made to the quality and functional integrity in designing, manufacturing and
 * testing the system. However, no liability can be assumed for potential errors that
 * might exist or for their effects. Should you find errors, please inform your
 * distributor of the nature of the errors and the circumstances under which they occur.
 * We will be responsive to all reasonable ideas and will follow up on them,
 * taking measures to improve the product, if necessary.
 * We call your attention to the fact that the company name and trademark as
 * well as product names are, as a rule, protected by trademark, patent and product
 * brand laws. @n @n
 * All rights reserved.
 *
 * \b Content
 * - @subpage sdai_intro
 * - @subpage sdai_portability
 * - @subpage sdai_descr
 *   - \ref sdai_overview
 *   - \ref sdai_io_configuration
 *   - \ref sdai_event_handling
 *   - \ref sdai_error_handling
 *   - \ref sdai_socket_interface
 *   - \ref sdai_io_performance_optimization
 * - @subpage supported_protocols
 *   - \ref pnio
 *   - \ref eip
 *   - \ref ecat
 *   - \ref mb
 *   - \ref epl
 *   - \ref pbdp
 * - @subpage sdai_reference
 *   - \ref SDAI-Functions
 *   - \ref Socket-Functions
 * - @subpage supported_phys
 * - @subpage version_history
 */

/******************************************************************************
SUBPAGES
******************************************************************************/

/** @page sdai_intro Introduction
 * The common device interface SDAI offers a comfortable application interface to the communication protocols.
 * The SDAI disburdens the user from a lot of tasks and provides a lot of automatisms. The complex structure
 * and functionality of the various communication protocols with their special characteristics will be hidden
 * as far as possible from the user. The user can focus on processing the I/O data, handling of diagnosis and
 * alarm messages and the parameterization of the device. This allows an easy implementation of the device application
 * and a high fault tolerance can be achieved because the application is isolated from the special characteristics
 * of the subordinated communication protocol.
 *
 */

/******************************************************************************/

/** @page sdai_portability Portability
 * The SDAI is designed for portation to very different hardware and operating system platforms. It does
 * not matter whether the application and the protocol stack are running on the same processor or whether
 * they are executed on separate processors.
 *
 * The following figure shows the structure for single-processor systems. This solution will be typically used
 * for simple device applications and in case of low performance requirements.
 *
 * @image html 1prozessor.jpg
 *
 * The following figure shows the structure for dual-processor systems. This solution will be typically used
 * for complex device applications or in case of high performance requirements. Here it is possible that the
 * application can serve other communication interfaces while the disturbance of the realtime communication
 * protocol is minimized.
 *
 * @image html 2prozessor.jpg
 *
 * The application interface is the same on both systems. Only the scope of application and surrounding conditions
 * as well as the desired performance are crucial factors when a decision is made on which system to use.
 *
 */

/******************************************************************************/

/** @page sdai_descr Application Interface Description
 *
 * The basic functionality of the application interface is described in the following chapters:
 * - @subpage sdai_overview
 * - @subpage sdai_io_configuration
 * - @subpage sdai_event_handling
 * - @subpage sdai_error_handling
 * - @subpage sdai_socket_interface
 * - @subpage sdai_io_performance_optimization
 *
 * A detailed description of all functions, data structures and definitions can be found here: \ref sdai_reference.
 *
 */

/******************************************************************************/

/** @page sdai_overview Overview
 * The SDAI provides the following main functions:
 * - initialize/terminate the SDAI
 * - plugging of I/O modules
 * - starting the protocol stack and changing to Online
 * - reading protocol stack status informations
 * - module specific access to the I/O data
 * - signalization of asynchronous events, e.g. service requests, via user callbacks
 * - sending responses to service requests
 * - watchdog supervision of the application and the protocol stack
 *
 * The following figure shows a typical sequence for the usage of the application interface.
 *
 * @image html sdai_sequence_en.jpg
 *
 */

/******************************************************************************/

/** @page sdai_io_configuration Process Data configuration
 * @section io_cfg_overview Overview
 *
 * The SDAI allows a modular organization of the process data. Via the functions sdai_plug_unit() and/or sdai_plug_unit_ext() the user
 * can create a logical data unit and with the function sdai_pull_unit() delete a logical data unit.
 *
 * @image html sdai_unit_plugging.jpg
 *
 * For each plugged unit a unique identification number (Unit ID) will be generated. With the Unit ID the application
 * can afterwards access the process data in a module specific way. The validity of a unit's process data will be signaled
 * by a status information parameter. The following figure shows how to access the process data of one input and one
 * output unit.
 *
 * @image html sdai_unit_access.jpg
 *
 * The units created by the SDAI will be mapped specifically to each communication protocol:
 * - \ref pn_plug_unit
 * - \ref eip_plug_unit
 * - \ref ecat_plug_unit
 * - \ref mb_plug_unit
 * - \ref epl_plug_unit
 * - \ref pbdp_plug_unit
 *
 * @section io_cfg_static Static IO Configuration
 *
 * The number of plugged modules and the assignment to specific slots is fixed by the application. Also the IO data layout of a module
 * is fixed by the application. A change of the IO configuration via the network is not possible. If a different IO configuration
 * is requested via the network (different module assignment or data layout) it is automatically rejected by the SDAI.
 *
 * @section io_cfg_dynamic Dynamic IO Configuration
 *
 * Dynamic IO configuration is also supported by the SDAI. It allows the following changes to the IO configuration during runtime:
 * - \ref io_cfg_variable
 * - \ref io_cfg_selectable
 *
 * For such devices the application must set the flag #SDAI_DYNAMIC_IO_CONFIGURATION within parameter Flags of structure #SDAI_DEVICE_DATA
 * when calling sdai_init(). If the configuration is changed via the network the following actions are performed for each changed I/O unit:
 * -# The SDAI signals the new IO configuration to the application by calling the callback function ControlReqCbk() with the control code #SDAI_CONTROL_CODE_CFG_DATA_INFO.
 * -# The application checks the new I/O configurations and, if accepted, calls sdai_plug_unit() or sdai_plug_unit_ext() again with the received configuration parameters.
 *    The new IO configuration for the respective module is taken over and the process data RAM is reorganized. The SDAI sets the status of the respective
 *    unit to #SDAI_DATA_STATUS_INVALID.
 * -# To finish the reconfiguration for the respective I/O module the application has to call sdai_control_response() with the error code
 *    set to #SDAI_SERVICE_SUCCESS in case of a successful reconfiguration or to #SDAI_SERVICE_ERR_WRONG_CFG_DATA if the new configuration is
 *    not supported or invalid.
 * -# The application calls the function sdai_set_data() with the parameter Status set to #SDAI_DATA_STATUS_VALID. This must be done if the
 *    type of respective unit is either #SDAI_UNIT_TYPE_GENERIC_INPUT or #SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT.
 *
 * There may exist protocol specific restrictions regarding the support of dynamic IO configuration, see \ref supported_protocols
 * for more details.
 *
 * @subsection io_cfg_variable Changing the IO data layout of a module
 * The module assignment (number and slot) is fixed by the device application. The device application specifies either no or a standard process
 * variable mapping for each module. The process variable mapping can then be changed via the network, e.g. adding or removing a process variable.
 * The possibilities of the process variable mapping to specific modules are specified by the device description.
 *
 * Typical Applications:
 * - Servo Drives are supporting very different operating modes (positioner, speed control, homing, interpolated position, ...).
 *   For each operating mode a different I/O data layout is required.
 * - Process Automation
 *
 * @subsection io_cfg_selectable Changing the assignment of a module to a specific slot
 * The data layout of each IO module is fixed by the device application. The device application plugs either no or standard modules at startup.
 * The module assignment can then be changed via the network. Possible changes are:
 * - Replacing a already used slot with a new module
 * - Adding a new module to an empty slot
 * - Removing Modules
 *
 * The module assignment may also be changed by the device application during runtime. For removing a module the function sdai_pull_unit() is provided.
 * A unique selection and slot allocation for multiple identical module types is ensured. The possibilities of the module assignment to specific slots
 * are specified by the device description.
 *
 * Typical Applications:
 * - Modular IO devices with a common backplane bus (e.g. Remote IO Devices)
 * - Process Automation
 *
 * @subsection io_cfg_consistency Consistency of IO data
 * By default the SDAI provides data consistency on unit basis. This means that the input and output data are updated consistent by the functions sdai_set_data()
 * and sdai_get_data(). If you spread the data over more than one unit, between updating the data of the units by these functions, new output data can
 * arrive or the input data can be sent.
 *
 * When setting the flag #SDAI_DATA_CONSISTENCY_IO_IMAGE the SDAI provides data consistency over the whole IO image. This means that the input data are not
 * directly updated by the call of sdai_set_data() but with the call of the function sdai_process_data_update_finished() consistently over all units at the
 * end of the update process. This feature is currently not supported by all protocols.
 *
 */

/******************************************************************************/

/** @page sdai_event_handling Event Processing
 * The signalization of asynchronous protocol stack events is done via user callback functions. Therefore the
 * user can register the supported callbacks during initialization of the SDAI within the structure #SDAI_CALLBACKS.
 * When an event occurs the SDAI informs the application by calling the respective user callback function. On a dual-processor system the callback
 * functions are called from interrupt context so the execution time of the callbacks should be as short as possible. It is not recommended to
 * to do the event processing directly in the callback functions but just copy the information and delegate the processing to the application task.
 *
 * The following events can be signaled via separate callback functions to the application:
 * - Identification data changed
 * - Fatal error occurred
 * - Output data changed
 * - write request received
 * - read request received
 * - control request received
 * - alarm acknowledge received
 * - synchronization signal received
 *
 * These events will be mapped specifically to each communication protocol:
 * - \ref pn_init_data
 * - \ref eip_init_data
 * - \ref ecat_init_data
 * - \ref mb_init_data
 * - \ref epl_init_data
 * - \ref pbdp_init_data
 *
 */

/******************************************************************************/

/** @page sdai_error_handling Diagnosis and Alarm Handling
 * The SDAI supports the signalization of diagnosis and alarm messages to the remote controller. A set of predefined standard
 * error/status information is already provided by the SDAI. In addition the application can also send manufacturer specific
 * diagnosis and alarm messages.
 *
 * How the SDAI Diagnosis and Alarm Handling will be mapped to each communication protocol is described here:
 * - \ref pn_diag_alarm
 * - \ref eip_diag_alarm
 * - \ref ecat_diag_alarm
 * - \ref mb_diag_alarm
 * - \ref epl_diag_alarm
 * - \ref pbdp_diag_alarm
 *
 * @section sdai_alarm Alarm Handling
 *
 * Alarms are used to signal status informations to the remote controller. Such problems are only related to the controlled process
 * and never cause a failure of the device. Examples:
 * - Upper limit value exceeded
 * - Status changed, e.g. Ready/Run/Stop
 * - A relevant parameter has been changed locally or by a engineering system
 * - The ownership of a module has been changed
 * - A module has been replaced by a wrong one
 *
 * For that purpose the SDAI provides the function sdai_alarm_request() which sends one alarm to the remote controller.
 * Alarms are always acknowledged to be sure that the information is received by the remote controller application. The SDAI
 * allows one parallel alarm for each alarm type and alarm priority. This means before an alarm of the same type and priority can be send again,
 * the acknowledgment of the previous alarm must be received from the remote controller. If there are more then one outstanding alarms for the same type
 * and priority the application must handle the queuing of the alarms. When the alarm acknowledge has been received, the
 * SDAI calls the callback function AlarmAckCbk() to inform the device application.
 *
 * In addition the SDAI may generate alarms automatically for some module related changes, e.g. an IO module has been pulled or plugged.
 *
 * There may be stronger limitations for number of parallel alarm for the different protocols:
 * - \ref pn_diag_alarm
 *
 * @section sdai_diagnosis Diagnosis Handling
 *
 * Diagnosis are used to signal error informations related to the device and not to the process to the remote controller.
 * Such problems may cause a failure of the device. Most of the diagnosis scenarios require intervention through service personal.
 * Examples:
 * - Short Circuit
 * - Voltage error
 * - Current error
 * - Temperature error
 * - Device HW/SW error
 *
 * The diagnosis source shall include information where the diagnosis is located on the device. A diagnosis source (i.e. an I/O unit)
 * can exist only once in the device. It can hold one or more diagnosis information according to severity and type. A diagnosis can be
 * added, removed or changed. Adding a diagnosis means that the application creates a diagnosis for a new source (i.e. an I/O unit) or
 * a new diagnosis type for a known source. A diagnosis is removed, if the reason is gone and the status specifier is set to
 * #SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_DISAPPEARS. If the severity of a diagnosis for a known source and type is modified, the diagnosis
 * is changed.
 *
 * For that purpose the SDAI provides the function sdai_diagnosis_request(). The application can specify the severity of the error according
 * to the NAMUR NE 107 recommendation:
 * - Maintenance required
 * - Out of specification
 * - Check function
 * - Failure
 *
 * <TABLE><TR>
 * <TH>Severity</TH><TH>Specifiers</TH><TH>Example: printer</TH>
 * </TR><TR>
 * <TD>Any</TD><TD>#SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_DISAPPEARS</TD><TD>Printing pages is possible without any restrictions.</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_SEVERITY_MAINTENANCE_REQUIRED</TD><TD>#SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS</TD><TD>The cartridge is getting empty. Printing pages is possible without any restrictions. The cartridge has to be replaced before long.</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_SEVERITY_OUT_OF_SPECIFICATION</TD><TD>#SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS</TD><TD>The cartridge is almost empty. Printing pages is possible with reduced chroma. The cartridge should be replaced.</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_SEVERITY_CHECK_FUNCTION</TD><TD>#SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS</TD><TD>The cartridge is almost empty. The original quality of the printout can no longer be guaranteed. The cartridge should be replaced.</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_SEVERITY_FAILURE</TD><TD>#SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS</TD><TD>The print cartridge is empty. Printing pages is not longer possible. The cartridge has to be replaced.</TD>
 * </TR></TABLE>
 *
 * The next chart shows how the Printer example could be implemented with the SDAI
 *
 * @msc
 * hscale = "1.7";
 * "Application", "Stack", "Controller";
 * "Application" box "Application"    [ label = "Printer cartridge reaches maintenance required level", textbgcolour="silver"];
 * "Application"=>"Stack"             [ label = "sdai_diagnosis_request(SDAI_DIAGNOSIS_SEVERITY_MAINTENANCE_REQUIRED, SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS)", URL="\ref sdai_diagnosis_request()" ];
 * "Stack" abox "Stack"               [ label = "store diagnosis entry" ];
 * "Stack">>"Controller"              [ label = "send diagnosis message (SDAI_DIAGNOSIS_SEVERITY_MAINTENANCE_REQUIRED)" ];
 * "Controller">>"Stack"              [ label = "diagnosis acknowledge" ];
 * ...;
 * "Application" box "Application"    [ label = "Printer cartridge reaches Out of specification level", textbgcolour="silver"];
 * "Application"=>"Stack"             [ label = "sdai_diagnosis_request(SDAI_DIAGNOSIS_SEVERITY_OUT_OF_SPECIFICATION, SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS)", URL="\ref sdai_diagnosis_request()" ];
 * "Stack" abox "Stack"               [ label = "update diagnosis entry" ];
 * "Stack">>"Controller"              [ label = "send diagnosis message (SDAI_DIAGNOSIS_SEVERITY_OUT_OF_SPECIFICATION)" ];
 * "Controller">>"Stack"              [ label = "diagnosis acknowledge" ];
 * ...;
 * "Application" box "Application"    [ label = "Printer cartridge reaches failure level", textbgcolour="silver"];
 * "Application"=>"Stack"             [ label = "sdai_diagnosis_request(SDAI_DIAGNOSIS_SEVERITY_FAILURE, SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS)", URL="\ref sdai_diagnosis_request()" ];
 * "Stack" abox "Stack"               [ label = "update diagnosis entry" ];
 * "Stack">>"Controller"              [ label = "send diagnosis message (SDAI_DIAGNOSIS_SEVERITY_FAILURE)" ];
 * "Controller">>"Stack"              [ label = "diagnosis acknowledge" ];
 * ...;
 * "Application" box "Application"    [ label = "Printer cartridge is replaced", textbgcolour="silver"];
 * "Application"=>"Stack"             [ label = "sdai_diagnosis_request(Any, SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_DISAPPEARS)", URL="\ref sdai_diagnosis_request()" ];
 * "Stack" abox "Stack"               [ label = "remove diagnosis entry" ];
 * "Stack">>"Controller"              [ label = "send diagnosis message (SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_DISAPPEARS)" ];
 * "Controller">>"Stack"              [ label = "diagnosis acknowledge" ];
 * @endmsc
 *
 */

/******************************************************************************/

/** @page sdai_io_performance_optimization I/O data performance optimization
 *
 * For use cases which require very fast I/O data update times, e.g. motion control applications, a hardware acceleration in logic has been implemented.
 * If this feature is enabled within the FPGA design the latency until the I/O data are updated from the SDAI Dual-Ported RAM to the MAC and vice versa is significantly decreased.
 * This feature costs additional RAM and logic cells. So it can only be used if enough resources are available.
 *
 * \ref supported_protocols shows for which protocol the I/O data update performance optimization is supported.
 */

/******************************************************************************/

/** @page supported_protocols Supported Protocols
 * The following communication protocols can be used with the SDAI:
 * - @subpage pnio
 * - @subpage eip
 * - @subpage ecat
 * - @subpage mb
 * - @subpage epl
 * - @subpage pbdp
 *
 * The following table shows which extended SDAI features are supported by the protocols:
 *
 * <TABLE><TR>
 * <TH>Protocol</TH><TH>Variable IO<br>Configuration</TH><TH>Alarm<br>Handling</TH><TH>Diagnosis<br>Handling</TH><TH>Socket<br>Interface</TH><TH>IO Data<br>Performance<br>Optimization</TH>
 * </TR><TR>
 * <TD>PROFINET IO</TD><TD>+</TD><TD>+</TD><TD>+</TD><TD>+</TD><TD>+</TD>
 * </TR><TR>
 * <TD>Ethernet/IP</TD><TD></TD><TD></TD><TD></TD><TD>+</TD><TD></TD>
 * </TR><TR>
 * <TD>EtherCAT</TD><TD>+</TD><TD></TD><TD>+</TD><TD>+</TD><TD>+</TD>
 * </TR><TR>
 * <TD>Modbus TCP</TD><TD></TD><TD></TD><TD></TD><TD>+</TD><TD></TD>
 * </TR><TR>
 * <TD>Ethernet Powerlink</TD><TD>+</TD><TD></TD><TD></TD><TD>+</TD><TD></TD>
 * </TR><TR>
 * <TD>PROFIBUS DP</TD><TD>+</TD><TD></TD><TD>+</TD><TD></TD><TD></TD>
 * </TR></TABLE>
 */

/******************************************************************************/

/** @page sdai_reference Application Interface Reference
 *
 * The following list shows all SDAI header files needed by the application. A detailed documentation
 * of all definitions, data structures and functions can be found in these files:
 * - \ref sdai.h \n
 *   Contains all common definitions and data structures valid for all protocols. It also includes all available SDAI functions which can be used by the application: \ref SDAI-Functions. \n\n
 * - \ref sdai_ecat.h \n
 *   Contains all EtherCAT specific definitions and data structures. More informations about Softing's EtherCAT solution can be found here: \ref ecat. \n\n
 * - \ref sdai_eip.h \n
 *   Contains all EtherNet/IP specific definitions and data structures. More informations about Softing's EtherNet/IP solution can be found here: \ref eip. \n\n
 * - \ref sdai_epl.h \n
 *   Contains all Ethernet Powerlink specific definitions and data structures. More informations about Softing's Ethernet Powerlink solution can be found here: \ref epl. \n\n
 * - \ref sdai_mb.h \n
 *   Contains all Modbus TCP specific definitions and data structures. More informations about Softing's Modbus TCP solution can be found here: \ref mb. \n\n
 * - \ref sdai_pn.h \n
 *   Contains all PROFINET IO specific definitions and data structures. More informations about Softing's PROFINET IO solution can be found here: \ref pnio. \n\n
 * - \ref sdai_pbdp.h \n
 *   Contains all PROFIBUS DP specific definitions and data structures. More informations about Softing's PROFIBUS solution can be found here: \ref pbdp. \n\n
 * - \ref sdai_socket.h \n
 *   Contains all definitions and data structures of the Socket Interface. It also includes all available Socket functions which can be used by the application: \ref Socket-Functions. \n\n
 *
 */

/******************************************************************************/

/** @page supported_phys Supported PHY's
 *
 * The following table shows which PHY is supported and recommended for which protocol:
 *
 *
 * <TABLE><TR>
 * <TH>Protocol</TH><TH>National<br>DP83640</TH><TH>Micrel<br>KSZ8051MLN</TH><TH>Marvell<br>88E1111</TH><TH>Renesas<br>uPD 6062x/6061x</TH><TH>Texas Instruments<br>TLK 105/110</TH>
 * </TR><TR>
 * <TD>PROFINET IO <b>(RT)</b></TD><TD><center>+</center></TD><TD><center>+</center></TD><TD><center>+</center></TD><TD><center>+</center></TD><TD><center>+</center></TD>
 * </TR><TR>
 * <TD>PROFINET IO <b>(IRT)</b></TD><TD></TD><TD></TD><TD></TD><TD><center>+</center></TD><TD></TD>
 * </TR><TR>
 * <TD>Ethernet/IP</TD><TD><center>+</center></TD><TD><center>+</center></TD><TD><center>+</center></TD><TD><center>+</center></TD><TD><center>+</center></TD>
 * </TR><TR>
 * <TD>EtherCAT</TD><TD><center>+</center></TD><TD><center>+</center></TD><TD><center>+</center></TD><TD><center>+</center></TD><TD><center>+</center></TD>
 * </TR><TR>
 * <TD>Modbus TCP</TD><TD><center>+</center></TD><TD><center>+</center></TD><TD><center>+</center></TD><TD><center>+</center></TD><TD><center>+</center></TD>
 * </TR><TR>
 * <TD>Ethernet Powerlink</TD><TD><center>+</center></TD><TD><center>+</center></TD><TD><center>+</center></TD><TD><center>+</center></TD><TD><center>+</center></TD>
 * </TR><TR>
 * <TD>PROFIBUS DP</TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD>
 * </TR></TABLE>
 *
 * \note <b>It is strongly recommended to use the Renesas PHY's uPD 6062x/6061x for PROFINET IRT. Other PHY's may
 *          also work, but are not tested by Softing.</b>
 */

/******************************************************************************/

/** @page version_history Version History
 * <TABLE><TR>
 * <TH>Date:</TH><TH>Version:</TH><TH>Description:</TH><TH>Adaptation of application required:</TH></TR>
 * <TR><TD>11.03.2008</TD><TD>1.00</TD><TD>
 * - First release
 * </TD><TD></TD></TR>
 * <TR><TD>05.08.2008</TD><TD>1.01</TD><TD>
 * - Parameter to function sdai_get_state() added
 * - Stack Status #SDAI_STATE_MASK_SECPROM_ERROR added
 * - Parameter of function sdai_get_version() changed
 * </TD><TD>\ref supported_protocols "all protocols"</TD></TR>
 * <TR><TD>12.09.2008</TD><TD>1.02</TD><TD>
 * - Parameter to function sdai_plug_unit() added
 * - Unit type #SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT added
 * - ServiceCode added to structure SDAI_EIP_ADDR_DESCR
 * - \ref eip Control codes changed
 * - \ref eip Overview and mapping description added
 * - \ref pnio Overview and mapping description added
 * </TD><TD>\ref supported_protocols "all protocols"</TD></TR>
 * <TR><TD>30.10.2008</TD><TD>1.03</TD><TD>
 * - sdai_check_and_trigger_watchdog() function and sdai_init_watchdog() function added
 * - Alignment of all structures in sdai.h
 * </TD><TD></TD></TR>
 * <TR><TD>23.01.2009</TD><TD>1.10</TD><TD>
 * - \ref ecat slave functionality added
 * - Documentation structure changed
 * - Data type of parameters Type and VendorId of structure SDAI_DEVICE_DATA changed
 * </TD><TD>\ref ecat</TD></TR>
 * <TR><TD>16.02.2009</TD><TD>1.11</TD><TD>
 * - \ref ecat_acyclic added
 * - Common service error codes defined
 * </TD><TD>\ref supported_protocols "all protocols"</TD></TR>
 * <TR><TD>30.04.2009</TD><TD>1.12</TD><TD>
 * - Additional status information for \ref eip added
 * </TD><TD></TD></TR>
 * <TR><TD>03.05.2010</TD><TD>1.20</TD><TD>
 * - \ref mb server functionality added
 * - SDAI common and protocol specific description enhanced
 * - Documentation structure changed
 * - Alarm Callback removed
 * - \ref pnio specific unit types removed
 * - New data type #BOOL defined
 * - Interface configuration flags for \ref eip added
 * - \ref eip and \ref pnio Control codes added
 * - Support sending of data with the sdai_control_response() function
 * - All \ref eip services have been mapped to the SDAI
 * - \ref pnio stack states moved to structure SDAI_PN_ADDSTATUS_DATA
 * - \ref PnUnitFlags "PROFINET IO Unit Flags" added
 * </TD><TD>\ref pnio <BR> \ref eip <BR> \ref mb</TD></TR>
 * <TR><TD>26.05.2010</TD><TD>1.21</TD><TD>
 * - Ethernet over EtherCAT (EoE) functionality added
 * </TD><TD>\ref ecat</TD></TR>
 * <TR><TD>06.07.2010</TD><TD>1.22</TD><TD>
 * - Documentation of configuration units enhanced
 * - \ref eip specific status data added (SDAI_EIP_ADDSTATUS_DATA)
 * </TD><TD>\ref eip</TD></TR>
 * <TR><TD>07.04.2011</TD><TD>1.23</TD><TD>
 * - \ref eip specific service error codes added
 * </TD><TD>\ref eip</TD></TR>
 * <TR><TD>24.08.2011</TD><TD>1.24</TD><TD>
 * - \ref eip specific status informations added
 * - QoS configuration for \ref eip supported
 * - Enable/Disable ACD for \ref eip
 * </TD><TD>\ref eip</TD></TR>
 * <TR><TD>18.11.2011</TD><TD>1.25</TD><TD>
 * - \ref eip Quick Connect feature added
 * - Ethernet Port configuration enhanced
 * - Control codes for reset emulation added
 * </TD><TD>\ref eip</TD></TR>
 * <TR><TD>28.12.2012</TD><TD>1.35</TD><TD>
 * - \ref epl Controlled Node functionality added
 * - Documentation structure changed and reformatted
 * - \ref sdai_socket_interface added
 * </TD><TD>\ref epl</TD></TR>
 * <TR><TD>10.05.2013</TD><TD>1.36</TD><TD>
 * - ProductCode defined as a common SDAI parameter
 * - For \ref pnio the DeviceId is now mapped to ProductCode
 * - Additional \ref pnio alarm types defined
 * - \ref pnio control code #SDAI_PN_CONTROL_PARAM_END added
 * - Functions sdai_lock_resources() and sdai_unlock_resources() added
 * - \ref ecat_foe support added
 * - \ref pnio IRT support added
 * </TD><TD>\ref supported_protocols "all protocols"</TD></TR>
 * <TR><TD>14.05.2013</TD><TD>1.40</TD><TD>
 * - Interface for \ref pbdp slave functionality added
 * - Interface for \ref io_cfg_dynamic added
 * - \ref sdai_error_handling reworked
 * - Parameter \ref Flags "Flags" added to structure SDAI_DEVICE_DATA
 * - Parameter \ref AlarmType "AlarmType" added to structure SDAI_ALARM_ACK_IND
 * - Parameter AlarmSpecifier removed from structure SDAI_ALARM_REQ
 * - Function sdai_pull_unit() added
 * </TD><TD>\ref supported_protocols "all protocols"</TD></TR>
 * <TR><TD>27.06.2013</TD><TD>1.41</TD><TD>
 * - \ref ecat_io_cfg_dynamic "Variable PDO mapping and assignment" for \ref ecat added
 * - \ref ecat_diag_alarm "Emergency handling" for \ref ecat added
 * - Explicit devices identification for \ref ecat added
 * - Some small improvements to Socket interface
 * - Parameter MappingVersionString renamed to MacVersionString in structure SDAI_VERSION_DATA
 * - \ref PnUnitFlags "PROFINET IO Unit Flags" renamed
 * - \ref pnio control code SDAI_PN_CONTROL_CONNECT_INFO replaced with common code #SDAI_CONTROL_CODE_CFG_DATA_INFO
 * - Combined documentation for SDAI and SDAI Socket interface
 * - Documentation enhanced
 * - New column added to version history that shows which protocols are affacted by the changes.
 * </TD><TD>\ref supported_protocols "all protocols"</TD></TR>
 * <TR><TD>16.09.2013</TD><TD>1.50</TD><TD>
 * - \ref sdai_socket_interface enabled for \ref eip
 * - Removed \ref eip specific network parameters
 * - Combined ethernet port status for \ref eip and \ref pnio in structure #SDAI_ADDSTATUS_DATA
 * - #SDAI_ADDSTATUS_PERFORMANCE_DATA added to structure #SDAI_ADDSTATUS_DATA
 * </TD><TD>\ref eip <BR> \ref pnio</TD></TR>
 * <TR><TD>14.10.2013</TD><TD>1.60</TD><TD>
 * - \ref sdai_socket_interface enabled for \ref mb
 * - Cyclic DPV0 services supported for \ref pbdp
 * - Automatic baudrate detection supported for \ref pbdp
 * </TD><TD>\ref pbdp <BR> \ref mb</TD></TR>
 * <TR><TD>04.11.2013</TD><TD>1.61</TD><TD>
 * - \ref pbdp_diag_alarm "DPV0 diagnosis" for \ref pbdp supported
 * - Sync/Freeze mode for \ref pbdp supported
 * - SDAI error codes #SDAI_ERR_WRONG_STATE and #SDAI_ERR_MAX_DIAG_REACHED added
 * </TD><TD>\ref pbdp</TD></TR>
 * <TR><TD>20.11.2013</TD><TD>1.62</TD><TD>
 * - \ref pb_io_cfg_dynamic for \ref pbdp supported
 * - Parameter Flags of structure #SDAI_PBDP_CFG_DATA splitted to InputFlags and OutputFlags
 * </TD><TD>\ref pbdp</TD></TR>
 * <TR><TD>11.12.2013</TD><TD>1.63</TD><TD>
 * - Acyclic MS1 Read/Write services for \ref pbdp supported
 * - Use common control code #SDAI_CONTROL_CODE_CFG_DATA_INFO for \ref epl dynamic unit configuration
 * </TD><TD>\ref pbdp <BR> \ref epl</TD></TR>
 * <TR><TD>15.04.2014</TD><TD>1.64</TD><TD>
 * - Acyclic MS2 Read/Write/Initiate/Abort services for \ref pbdp supported
 * - Parameter CommRef added to structure #SDAI_PBDP_ADDR_DESCR
 * - PROFIBUS control codes #SDAI_PBDP_CONTROL_CODE_INITIATE and #SDAI_PBDP_CONTROL_CODE_ABORT added
 * </TD><TD>\ref pbdp</TD></TR>
 * <TR><TD>20.10.2014</TD><TD>1.70</TD><TD>
 * - \ref pn_cfg_dynamic "Dynamic IO Configuration" for \ref pnio supported
 * - \ref pn_data_substitute "Data substitution" for \ref pnio supported
 * - Behavior for #SDAI_CONTROL_CODE_CFG_DATA_INFO for \ref pnio changed
 * - Error Code #SDAI_STATE_MASK_NON_VOLATILE_STORAGE_ERROR added
 * - #SDAI_MAX_UNITS increased to 65
 * - \ref sdai_io_performance_optimization for \ref ecat supported
 * - Exception handling unified (see #SDAI_EXCEPTION_DATA)
 * </TD><TD>\ref supported_protocols "all protocols"</TD></TR>
 * <TR><TD>12.11.2014</TD><TD>1.71</TD><TD>
 * - Maximum data size for a \ref pnio unit limited to 254 byte (see \ref PnUnitLimits)
 * - \ref sdai_io_performance_optimization for \ref pnio supported
 * - New data consistency handling added for \ref pnio (see \ref io_cfg_consistency)
 * </TD><TD>\ref pnio</TD></TR>
 * <TR><TD>26.01.2015</TD><TD>1.72</TD><TD>
 * - Functions sdai_lock_resources() and sdai_unlock_resources() changed to lock different shared resources
 * </TD><TD>\ref supported_protocols "all protocols"</TD></TR>
 * <TR><TD>20.02.2015</TD><TD>1.73</TD><TD>
 * - Support of \ref pnio "PROFINET application profiles" for  I/O-units added (see \ref PnProfileId)
 * </TD><TD>\ref pnio</TD></TR>
 * <TR><TD>04.05.2015</TD><TD>1.75</TD><TD>
 * - Stack Status #SDAI_STATE_MASK_SECPROM_ERROR replace by the common license issue
 *   notification #SDAI_STATE_MASK_LICENSE_ERROR (the new definition covers ALL licensing issues)
 * </TD><TD>\ref supported_protocols "all protocols"</TD></TR>
 * </TABLE>
 *
 */

#ifndef __SDAI_H__
#define __SDAI_H__

/******************************************************************************
DEFINES
******************************************************************************/

#define SDAI_MAX_PORTS          2    /**< Maximum number of supported Ethernet Ports */

#define SDAI_SERVICE_SIZE       512  /**< The size of the service channels in the interface */

/*--- Backend Protocol ------------------------------------------------------*/

/** \anchor BackEnd @name Backend Type
* Definitions for the selection of the SDAI backend protocol */
//@{
/* SDAI backend type selection */
#define SDAI_BACKEND_EIPS       1    /**< Use EtherNet/IP stack */
#define SDAI_BACKEND_PN         2    /**< Use PROFINET IO stack */
#define SDAI_BACKEND_PB_DP      3    /**< Use PROFIBUS DP stack */
#define SDAI_BACKEND_ECAT       4    /**< Use EtherCAT stack */
#define SDAI_BACKEND_MODBUS     5    /**< Use Modbus TCP stack */
#define SDAI_BACKEND_EPL        6    /**< Use Powerlink stack */
//@}

/*--- Unit definitions ------------------------------------------------------*/

/** \anchor UnitType @name Unit Types
* SDAI common unit types. For protocol specific unit types see also \ref EipUnitType and \ref MbUnitType */
//@{
#define SDAI_UNIT_TYPE_HEAD                         1    /**< indicates the head unit */
#define SDAI_UNIT_TYPE_GENERIC_INPUT                2    /**< indicates a input unit */
#define SDAI_UNIT_TYPE_GENERIC_OUTPUT               3    /**< indicates a output unit */
#define SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT          4    /**< indicates a input and output unit */
#define SDAI_UNIT_TYPE_PLUGGING_COMPLETE            255  /**< indicates plugging units complete */
//@}

/** \anchor DataStatus @name Data Status
* Definitions for SDAI common unit data states */
//@{
#define SDAI_DATA_STATUS_VALID                      0x80 /**< data status for valid data */
#define SDAI_DATA_STATUS_INVALID                    0x00 /**< data status for invalid data */
//@}

/** \anchor UnitLimits @name Unit Limits
* Definitions for SDAI common unit limits */
//@{
#define SDAI_MAX_UNITS                              65   /**< Maximum number of units, one head unit and additionally upto 64 io-units */
#define SDAI_MAX_IO_DATA_SIZE                       1024 /**< Maximum overall size in bytes of the IO data area */
#define SDAI_MAX_UNIT_IO_DATA_SIZE                  255  /**< Maximum IO data size in bytes of one unit */
//@}

#define SDAI_INVALID_ID                             0xFFFFFFFFuL   /**< Value of unit ID marked as invalid */

/*--- Identification / Device Information -----------------------------------*/

/** \anchor NetworkData @name Network Parameter
 *  Definitions for network specific parameter. */
//@{
#define SDAI_IP_UNUSED          0   /**< Indicates that the IP address must be configured via DCP, DHCP or BOOTP */
#define SDAI_NETMASK_UNUSED     0   /**< Indicates that the netmask must be configured via DCP, DHCP or BOOTP */
#define SDAI_GATEWAY_UNUSED     0   /**< Indicates that the gateway address must be configured via DCP, DHCP or BOOTP */
//@}

/** @name Ident Data
 *  Definitions for identification data */
//@{
#define SDAI_DEVNAME_MAX_LEN    240  /**< Maximum device name length */
#define SDAI_IFNAME_MAX_LEN     20   /**< Maximum interface name length */
//@}

/** @name Device Data
 *  Definitions for device data */
//@{
#define SDAI_PRODUCTNAME_MAX_LEN    20  /**< Maximum length of the product name */
#define SDAI_ORDERID_MAX_LEN        20  /**< Maximum length of the order id */
//@}

/** @name Version
 *  Definitions version information */
//@{
#define SDAI_VERSION_STRING_LENGTH  16    /**< Maximum length of the version strings */
//@}

/** \anchor Flags @name Interface Configuration Flags
* Bitfield holding definitions for the configuration of the device:
*/
//@{
#define SDAI_DYNAMIC_IO_CONFIGURATION           0x01  /**< IO configuration can be changed via the network. The new IO configuration is signaled
                                                           to the application via the ControlReq callback function. */
#define SDAI_DATA_CONSISTENCY_UNIT              0x00  /**< Consistency of the IO data is ensured for a single IO-unit. This is the default behavior. */
#define SDAI_DATA_CONSISTENCY_IO_IMAGE          0x02  /**< Consistency for the whole IO data image is ensured. An update of input data for one or
                                                           more units requires a call of sdai_process_data_update_finished() to signal the end of the
                                                           update process to the fielbus stack. */
#define SDAI_APPL_HANDLES_INTERFACE_AND_PORTS   0x04  /**< The device application defines and handles the ethernet interface and ethernet ports.
                                                           This option can be used only with PROFINET. The ethernet interface and ethernet ports are
                                                           known as system-defined submodules. The subslot range is defined from 0x8000 to 0x80ff.
                                                           An application can use these submodules only at slot 0. To use this option #SDAI_DYNAMIC_IO_CONFIGURATION
                                                           has to be set too and the macro _SDAI_PN_SLOT_AND_SYSTEM_DEFINED_SUBSLOT_TO_ID(Slot, SubSlot)
                                                           has to be used to define the related Id of a unit. */
//@}

/*--- Error Codes -----------------------------------------------------------*/

/** \anchor ErrorCode @name SDAI Error Codes
* Definitions for SDAI common Error Codes */
//@{
/* SDAI common error codes */
#define SDAI_SUCCESS                                0u   /**< General success return value */
#define SDAI_ERR_INTERNAL                           1u   /**< An internal error occured in the SDAI */
#define SDAI_ERR_NOSUCHUNIT                         2u   /**< The requestet id is not present */
#define SDAI_ERR_MAX_UNIT_REACHED                   3u   /**< Unit maximum reached */
#define SDAI_ERR_INVALID_SIZE                       4u   /**< Given size is invalid */
#define SDAI_ERR_UNKNOWN_UNIT_TYPE                  5u   /**< Given unit type not valid */
#define SDAI_ERR_RUNTIME_PLUG                       6u   /**< User tried to plug unit at runtime */
#define SDAI_ERR_INVALID_ARGUMENT                   7u   /**< Passed argument is invalid */
#define SDAI_ERR_SERVICE_PENDING                    8u   /**< There is already an user service pending */
#define SDAI_ERR_STACK_NOT_RDY                      9u   /**< The Stack is not ready to handle this command */
#define SDAI_ERR_COM_NOT_SUPPORTED                  10u  /**< Command not supported */
#define SDAI_ERR_INVALID_ADDR_DESCR                 11u  /**< Passed address description is invalid, e.g.
                                                              the addressed object did not exist */
#define SDAI_ERR_WATCHDOG_EXPIRED                   12u  /**< Stack watchdog is expired */
#define SDAI_ERR_PROTOCOL_NOT_SUPPORTED             13u  /**< The backend protocol selected by the application is not supported. */
#define SDAI_ERR_PROTOCOL_LIBRARY_NOT_LOADED        14u  /**< The selected backend protocol library is not loaded. */
#define SDAI_ERR_WRONG_STATE                        15u  /**< The requested service could not be executed in the current state */
#define SDAI_ERR_MAX_DIAG_REACHED                   16u  /**< The maximum supported number of parallel diagnosis messages/entries is reached. */
//@}

/** \anchor ServiceErrorCode @name Service Error Codes
* Definitions for SDAI common Service Error Codes. For protocol specific service error codes see also \ref EipServiceErrorCode */
//@{
#define SDAI_SERVICE_SUCCESS                     0x0000  /**< Service was successfully performed */
#define SDAI_SERVICE_ERR_OBJECT_NOT_EXISTING     0x0001  /**< The addressed object dos not exist */
#define SDAI_SERVICE_ERR_INVALID_SIZE            0x0002  /**< Invalid length in the request */
#define SDAI_SERVICE_ERR_STATE_CONFLICT          0x0003  /**< The requested service can not be performed in the current state */
#define SDAI_SERVICE_ERR_APPLICATION             0x0004  /**< Application specific error occured */
#define SDAI_SERVICE_ERR_RESOURCE                0x0005  /**< Resource error occured, e.g. resources unavailable */
#define SDAI_SERVICE_ERR_NOT_SUPPORTED           0x0006  /**< The requested service is not supported */
#define SDAI_SERVICE_ERR_ACCESS_DENIED           0x0007  /**< The access was denied, e.g. the addressed object is read-only */
#define SDAI_SERVICE_ERR_INVALID_PARAMETER       0x0008  /**< Invalid parameter in the request */
#define SDAI_SERVICE_ERR_FILE_NOT_FOUND          0x0009  /**< The file specified by filename could not be found in the filesystem. */
#define SDAI_SERVICE_ERR_DISK_FULL               0x000A  /**< There is no space left on the storage device to write the file. */
#define SDAI_SERVICE_ERR_WRONG_CFG_DATA          0x000B  /**< This error is returned when receiving a invalid I/O configuration from the remote controller.
                                                              It can only be set in response to the control indication with control code
                                                              #SDAI_CONTROL_CODE_CFG_DATA_INFO. Examples:
                                                              - A different module than the actually inserted module is requested.
                                                              - It is not allowed to plug the new module in the respective slot.
                                                              - The requested I/O data layout is not supported. */
//@}

/*--- Control Codes ---------------------------------------------------------*/

/** \anchor ControlCode @name Control Codes
 *  Definitions for SDAI common Control Codes. For protocol specific control codes see also \ref PnControlCode, \ref EipControlCode, \ref EplControlCode,
 *  \ref EcatControlCode and \ref PbControlCode */
//@{
#define SDAI_CONTROL_RESET_TO_DEFAULTS                  0x0001  /**< Return as closely as possible to the factory default configuration. */
#define SDAI_CONTROL_RESET_TO_DEFAULTS_AND_POWER_CYCLE  0x0002  /**< Return as closely as possible to the factory default configuration,
                                                                     then emulate cycling power as closely as possible. */
#define SDAI_CONTROL_POWER_CYCLE                        0x0003  /**< Emulate as closely as possible cycling power on the device. */
#define SDAI_CONTROL_UPDATE_FIRMWARE                    0x0004  /**< For future use. */
#define SDAI_CONTROL_CODE_CFG_DATA_INFO                 0x0005  /**< This control indication is signalled to the application if the IO configuration for one IO unit
                                                                     has been changed, e.g. new process variable mapping. This indication is either signalled when the configuration
                                                                     specified by the application is taken over or when the configuration has been dynamically changed
                                                                     via the network. See also #SDAI_CONTROL_CFG_DATA_INFO. */
//@}

/*--- Stack Status ----------------------------------------------------------*/

/** \anchor StackStatus @name Stack Status
* Definitions for SDAI common and protocol specific stack states */
//@{
/* SDAI common stack states */
#define SDAI_STATE_MASK_INIT                        0x0001   /**< Stack is initialized */
#define SDAI_STATE_MASK_ONLINE                      0x0002   /**< Stack is online (device is accessible via the network interface) */
#define SDAI_STATE_MASK_CONNECTED                   0x0004   /**< Stack is connected (communication with the master) */
#define SDAI_STATE_MASK_DOUBLE_ADDRESS_ERROR        0x0008   /**< Conflict of addresses detected. The network communication may be disturbed. */
#define SDAI_STATE_MASK_HARDWARE_ERROR              0x0010   /**< Possible causes are faulty access to the MAC or interrupted data streams */
#define SDAI_STATE_MASK_LICENSE_ERROR               0x0020   /**< Security EEPROM not existing or requested options not supported. The Stack is running in evaluation mode and
                                                                  may stop function after the evaluation time expires */
#define SDAI_STATE_MASK_SECPROM_ERROR               SDAI_STATE_MASK_LICENSE_ERROR /**< Security EEPROM is defined as deprecated and will be removed in a future version. Use #SDAI_STATE_MASK_LICENSE_ERROR instead */

#define SDAI_STATE_MASK_APPL_WATCHDOG_EXPIRED       0x0040   /**< Application watchdog is expired */
#define SDAI_STATE_MASK_PROTOCOL_ERROR              0x0080   /**< A protocol specific error has been occurred. This protocol error
                                                                  can be read via the additional status information provided
                                                                  within the structure #SDAI_ADDSTATUS_DATA. */
#define SDAI_STATE_MASK_NON_VOLATILE_STORAGE_ERROR  0x0100   /**< An error with the non volatile storage occured. The meaning of this depends on the selected non volatile storage device */

#define SDAI_STATE_ERROR_MASK                       (SDAI_STATE_MASK_DOUBLE_ADDRESS_ERROR  | \
                                                     SDAI_STATE_MASK_HARDWARE_ERROR        | \
                                                     SDAI_STATE_MASK_LICENSE_ERROR         | \
                                                     SDAI_STATE_MASK_APPL_WATCHDOG_EXPIRED | \
                                                     SDAI_STATE_MASK_PROTOCOL_ERROR        | \
                                                     SDAI_STATE_MASK_NON_VOLATILE_STORAGE_ERROR)
//@}

/** \anchor LinkState @name Link States
 * Link States of the Ethernet port */
//@{
#define SDAI_ADDSTATUS_LINK_UP                         0x01 /**< Link is up */
#define SDAI_ADDSTATUS_LINK_DOWN                       0x00 /**< Link is down */
//@}

/** \anchor NegotiateState @name Autonegotiate States
 * Autonegotiate States of the Ethernet port */
//@{
#define SDAI_ADDSTATUS_AUTO_NEGO_IN_PROGRESS                 0x01uL  /**< Auto-negotiation in progress. */
#define SDAI_ADDSTATUS_AUTO_NEGO_FAILED_SPEED_NOT_DETECTED   0x02uL  /**< Auto-negotiation and speed detection failed. Using default values for speed and duplex.
                                                                          Default values are product-dependent; recommended defaults are 10Mbps and half duplex. */
#define SDAI_ADDSTATUS_AUTO_NEGO_FAILED_SPEED_DETECTED       0x03uL  /**< Auto negotiation failed but detected speed. Duplex was defaulted. Default
                                                                          value is product-dependent; recommended default is half duplex. */
#define SDAI_ADDSTATUS_AUTO_NEGO_SUCCESSFULL                 0x04uL  /**< Successfully negotiated speed and duplex. */
#define SDAI_ADDSTATUS_AUTO_NEGO_NOT_ATTEMPTED               0x05uL  /**< Auto-negotiation not attempted. Forced speed and duplex. */
//@}

/** \anchor ConnectionType @name Connection Types
 * Connection Types of the Ethernet port */
//@{
#define SDAI_ADDSTATUS_CONNECTION_100MBIT_FULLDUPLEX   0x03 /**< Current connection is 100Mbit Full Duplex */
#define SDAI_ADDSTATUS_CONNECTION_100MBIT_HALFDUPLEX   0x01 /**< Current connection is 100Mbit Half Duplex */
#define SDAI_ADDSTATUS_CONNECTION_10MBIT_FULLDUPLEX    0x02 /**< Current connection is 10Mbit Full Duplex */
#define SDAI_ADDSTATUS_CONNECTION_10MBIT_HALFDUPLEX    0x00 /**< Current connection is 10Mbit Half Duplex */
//@}

/** \anchor InterfaceStatus @name TCP/IP Interface Status
 * Status of the TCP/IP network interface */
//@{
#define SDAI_TCPIP_IF_STATUS_NOT_CONFIGURED   0x00uL  /**< The TCP/IP Interface has not been configured */
#define SDAI_TCPIP_IF_STATUS_STATIC_VALID     0x01uL  /**< Valid network configuration obtained from the application
                                                           (static IP address, e.g. stored in non-volatile memory) */
#define SDAI_TCPIP_IF_STATUS_DHCP_VALID       0x02uL  /**< Valid network configuration obtained from DHCP */
#define SDAI_TCPIP_IF_STATUS_DHCP_FAILED      0x03uL  /**< No valid network configuration obtained from DHCP. In this state the
                                                           TCP/IP Interface is down. The application needs to call sdai_deinit() and
                                                           sdai_init() again to restart activation of the TCP/IP interface. */
//@}

/*--- Alarm handling --------------------------------------------------------*/

/** \anchor AlarmPrio @name Alarm Priorities
* Definitions for common alarm priorities. */
//@{
#define SDAI_ALARM_PRIO_LOW                      0x01u     /**< Low priority of alarm */
#define SDAI_ALARM_PRIO_HIGH                     0x02u     /**< High priority of alarm */
//@}

/** \anchor AlarmType @name Alarm Types
* Definitions for common alarm types. */
//@{
#define SDAI_ALARM_PROCESS                       0x0002u   /**< Signals an event in the connected process */
#define SDAI_ALARM_STATUS                        0x0005u   /**< Signals the change of the state of a module */
#define SDAI_ALARM_UPDATE                        0x0006u   /**< Signals the change of a parameter in a module */
//@}

/*--- Diagnosis handling ----------------------------------------------------*/

/** \anchor DiagType diagnosis types @name Diagnosis Types
 * Definition for common diagnosis types.  */
//@{
#define SDAI_DIAGNOSIS_TYPE_SHORT_CIRCUIT              0x00000001uL /**< A short circut occures within the device or its peripherals. */
#define SDAI_DIAGNOSIS_TYPE_INPUT_UNDERVOLTAGE         0x00000002uL /**< An undervoltage at one of the input modules has been detected. */
#define SDAI_DIAGNOSIS_TYPE_INPUT_OVERVOLTAGE          0x00000003uL /**< An overvoltage at one of the input modules has been detected. */
#define SDAI_DIAGNOSIS_TYPE_OUTPUT_UNDERVOLTAGE        0x00000004uL /**< An undervoltage at one of the output modules has been detected. */
#define SDAI_DIAGNOSIS_TYPE_OUTPUT_OVERVOLTAGE         0x00000005uL /**< An overvoltage at one of the output modules has been detected. */
#define SDAI_DIAGNOSIS_TYPE_DEVICE_UNDERVOLTAGE        0x00000006uL /**< An undervoltage at the device itself has been detected. */
#define SDAI_DIAGNOSIS_TYPE_DEVICE_OVERVOLTAGE         0x00000007uL /**< An overvoltage at the device itself has been detected. */
#define SDAI_DIAGNOSIS_TYPE_INPUT_UNDERCURRENT         0x00000008uL /**< An undercurrent at one of the input modules has been detected. */
#define SDAI_DIAGNOSIS_TYPE_INPUT_OVERCURRENT          0x00000009uL /**< An overcurrent at one of the input modules has been detected. */
#define SDAI_DIAGNOSIS_TYPE_OUTPUT_UNDERCURRENT        0x0000000AuL /**< An undercurrent at one of the output modules has been detected. */
#define SDAI_DIAGNOSIS_TYPE_OUTPUT_OVERCURRENT         0x0000000BuL /**< An overcurrent at one of the output modules has been detected. */
#define SDAI_DIAGNOSIS_TYPE_DEVICE_UNDERCURRENT        0x0000000CuL /**< An undercurrent at the device itself has been detected. */
#define SDAI_DIAGNOSIS_TYPE_DEVICE_OVERCURRENT         0x0000000DuL /**< An overcurrent at the device itself has been detected. */
#define SDAI_DIAGNOSIS_TYPE_OVERLOAD                   0x0000000EuL /**< The device is in an overload situation. */
#define SDAI_DIAGNOSIS_TYPE_OVERTEMPERATURE            0x0000000FuL /**< The temperature upper limit of the allowed operating range is exceeded. */
#define SDAI_DIAGNOSIS_TYPE_UNDERTEMPERATURE           0x00000010uL /**< The temperature lower limit of the allowed operating range is exceeded.  */
#define SDAI_DIAGNOSIS_TYPE_AMBIENTE_OVERTEMPERATURE   0x00000011uL /**< The upper limit of the specified ambiente temperature is exceeded. */
#define SDAI_DIAGNOSIS_TYPE_AMBIENTE_UNDERTEMPERATURE  0x00000012uL /**< The lower limit of the specified ambiente temperature is exceeded. */
#define SDAI_DIAGNOSIS_TYPE_LINE_BREAK                 0x00000013uL /**< A line break at one of the peripherals has been detected. */
#define SDAI_DIAGNOSIS_TYPE_UPPER_LIMIT_EXCEEDED       0x00000014uL /**< An upper limit of the operating range is exceeded. */
#define SDAI_DIAGNOSIS_TYPE_LOWER_LIMIT_EXCEEDED       0x00000015uL /**< A lower limit of the operating range is exceeded. */
#define SDAI_DIAGNOSIS_TYPE_GENERIC                    0x00000016uL /**< An unspecific problem has been detected. */
#define SDAI_DIAGNOSIS_TYPE_SIMULATION_ACTIVE          0x00000017uL /**< Input and/or outputs are in simulation mode. */
#define SDAI_DIAGNOSIS_TYPE_PRM_MISSING                0x00000018uL /**< Parameters for correct working of peripherals or device are missing. */
#define SDAI_DIAGNOSIS_TYPE_PRM_FAULT                  0x00000019uL /**< Parameter mismatch for correct working of peripherals or device. */
#define SDAI_DIAGNOSIS_TYPE_POWER_SUPPLY_FAULT         0x0000001AuL /**< Power supply issue detected. */
#define SDAI_DIAGNOSIS_TYPE_FUSE_BLOWN                 0x0000001BuL /**< Blown fuse detected. */
#define SDAI_DIAGNOSIS_TYPE_COMMUNICATION_FAULT        0x0000001CuL /**< Internal communication problems, e.g. backplane, detected. */
#define SDAI_DIAGNOSIS_TYPE_GROUND_FAULT               0x0000001DuL /**< No connection to ground or to high resistance to ground. */
#define SDAI_DIAGNOSIS_TYPE_REFERENCE_POINT_LOST       0x0000001EuL /**< Lost of reference point. */
#define SDAI_DIAGNOSIS_TYPE_SAMPLING_ERROR             0x0000001FuL /**< Sampling not possible, e.g lost sampling clock detected. */
#define SDAI_DIAGNOSIS_TYPE_THRESHOLD_WARNING          0x00000020uL /**< Threshold exceeded. */
#define SDAI_DIAGNOSIS_TYPE_OUTPUT_DISABLED            0x00000021uL /**< Output signals disabled. */
#define SDAI_DIAGNOSIS_TYPE_SAFETY_EVENT               0x00000022uL /**< Safety event occured. */
#define SDAI_DIAGNOSIS_TYPE_EXTERNAL_FAULT             0x00000023uL /**< External fault detected. */
#define SDAI_DIAGNOSIS_TYPE_TEMPORARY_FAULT            0x00000024uL /**< Temporary fault detected. */
#define SDAI_DIAGNOSIS_TYPE_DEVICE_HARDWARE            0x00000025uL /**< Hardware related issue detected. */
#define SDAI_DIAGNOSIS_TYPE_DEVICE_SOFTWARE            0x00000026uL /**< Software related issue detected. */
#define SDAI_DIAGNOSIS_TYPE_MONITORING                 0x00000027uL /**< Internal monitoring, e.g. internal communication or watchdog, failed. */

#define SDAI_DIAGNOSIS_TYPE_PROTOCOL_SPECIFIC          0x80000000uL /**< The common diagnosis types does not match. Decode the type protocol specific and see also \ref PNDiagType and \ref EcatDiagType */
//@}

/** \anchor DiagSeverity @name Diagnosis Severity
 * Definitions for diagnosis severity */
//@{
#define SDAI_DIAGNOSIS_SEVERITY_MAINTENANCE_REQUIRED    0x0001u   /**< Although the output signal is valid, the wear reserve is nearly exhausted or a function will
                                                                       soon be restricted due to operational conditions e.g. aging of a pH-electrode. */
#define SDAI_DIAGNOSIS_SEVERITY_OUT_OF_SPECIFICATION    0x0002u   /**< Deviations from the permissible ambient or process conditions determined by the device itself through
                                                                       self-monitoring or faults in the device itself indicate that the measuring uncertainty of sensors
                                                                       or deviations from the set value in actuators is probably greater than expected under operating conditions. */
#define SDAI_DIAGNOSIS_SEVERITY_CHECK_FUNCTION          0x0003u   /**< Output signal temporarily invalid (e.g. frozen or simulated) due to ongoing work on the device.  */
#define SDAI_DIAGNOSIS_SEVERITY_FAILURE                 0x0004u   /**< Output signal invalid due to malfunction in the field device or its peripherals.  */
//@}

/** \anchor DiagStatus @name Diagnosis Status Specifier
 * Definitions for diagnosis status specifier */
//@{
#define SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS       0x0001u   /**< Error has been detected */
#define SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_DISAPPEARS    0x0002u   /**< The related error has been fixed or the error cause disappears */
//@}

/*--- SDAI Interface Types --------------------------------------------------*/

/** \anchor InterfaceType @name Interface Types
 * Specifies the interface on which the I/O data are transferred. */
//@{
#define SDAI_INTERFACE_PRIMARY        0x00       /**< The I/O data of the SDAI unit are transferred via the default DPRAM interface. */
#define SDAI_INTERFACE_SECONDARY      0x10       /**< A secondary interface (e.g. SPI) is used to transfer the I/O data to a second external processor, e.g. a DSP. */
#define SDAI_INTERFACE_MASK           0x10

#define SDAI_INTERFACE_BUS            SDAI_INTERFACE_PRIMARY
#define SDAI_INTERFACE_SPI            SDAI_INTERFACE_SECONDARY
//@}

#define SDAI_PERIPHERAL_OFFSET_DPRAM  0xFFFF

/*--- SDAI Shared Resources ------------------------------------------------*/

/** \anchor SharedResources @name Shared Resources
 * Specifies which shared resources should be used. */
//@{
#define SDAI_SHARED_RESOURCE_FLASH 0x0001       /**< Used to lock the FLASH. */
#define SDAI_SHARED_RESOURCE_MDIO  0x0002       /**< Used to guarant exclusive access rigth to the MDIO interface. */
//@}

/*--- Basic Data Types ------------------------------------------------------*/

/** \anchor DataType @name Data Types
*  Data type definitions */
//@{
#ifndef S8
  #define S8  signed char
#endif

#ifndef U8
  #define U8  unsigned char
#endif

#ifndef S16
  #define S16 signed short
#endif

#ifndef U16
  #define U16 unsigned short
#endif

#ifndef S32
#if defined __x86_64__
  #define S32 signed int
#else
  #define S32 signed long
#endif
#endif

#ifndef U32
#if defined __x86_64__
  #define U32 unsigned int
#else
  #define U32 unsigned long
#endif
#endif

#ifndef BOOL
  #define BOOL unsigned char
#endif

#ifndef TRUE
  #define TRUE 0xFF
#endif

#ifndef FALSE
  #define FALSE 0x00
#endif

typedef U8     T_BITSET64 [8];
typedef U8     T_BITARRAY64 [64];

//@}

/*--- Bitfield operations ---------------------------------------------------*/

#define _BITSET_RESET_BIT(pBS,BitNr)     ((pBS) [(unsigned) ((BitNr) >> 3)] &= (U8) (~(1u << ((BitNr) & 7u))))
#define _BITSET_SET_BIT(pBS,BitNr)       ((pBS) [(unsigned) ((BitNr) >> 3)] |= (U8)  (1u << ((BitNr) & 7u)))
#define _BITSET_GET_BIT(pBS,BitNr)      (((pBS) [(unsigned) ((BitNr) >> 3)]) & (U8)  (1u << ((BitNr) & 7u) ))

#define _BITSET_IS_BIT_SET(pBS,BitNr)   ((BOOL) _BITSET_GET_BIT(pBS,BitNr))

/******************************************************************************
INCLUDES
******************************************************************************/

/* protocol specific definitions and structures */
#include "sdai_ecat.h"
#include "sdai_eip.h"
#include "sdai_epl.h"
#include "sdai_mb.h"
#include "sdai_pbdp.h"
#include "sdai_pn.h"

/******************************************************************************
TYPEDEFS
******************************************************************************/

/** \brief Device identification data. */
/** Items describe the devices identification within the selected network. This structure is also used when the backend
    signals the change of one of the attributes. */
struct SDAI_IDENT_DATA
{
  char     DevName [SDAI_DEVNAME_MAX_LEN];        /**< The name of the Device */
  char     IfName [SDAI_IFNAME_MAX_LEN];          /**< The name of the used interface. Usage depends on platform and protocol. Unused by the IdentDataCbk callback */

  union
  {
    struct SDAI_PN_IDENT_DATA   Pn;               /**< PROFINET IO specific ident data */
    struct SDAI_EIP_IDENT_DATA  Eip;              /**< EtherNet/IP specific ident data */
    struct SDAI_PBDP_IDENT_DATA PbDp;             /**< PROFIBUS DP specific ident data */
    struct SDAI_ECAT_IDENT_DATA Ecat;             /**< EtherCAT specific ident data */
    struct SDAI_MB_IDENT_DATA   Mb;               /**< Modbus TCP specific ident data */
    struct SDAI_EPL_IDENT_DATA  Epl;              /**< Powerlink specific ident data */

  } UseAs;                                        /**< The protocol specific ident data */
};

/** \brief holds general information about the device. */
struct SDAI_DEVICE_DATA
{
  U32     SerialNumber;                           /**< The serial number of the device */
  U32     VendorID;                               /**< The vendor ID of the device */
  U32     Type;                                   /**< The device Type */
  U32     ProductCode;                            /**< Identifies a particular product */

  U32     Flags;                                  /**< Configurations Flags for the device. Valid values: \ref Flags */

  union
  {
    struct SDAI_PN_DEVICE_DATA    Pn;             /**< PROFINET IO specific device data */
    struct SDAI_EIP_DEVICE_DATA   Eip;            /**< EtherNet/IP specific device data */
    struct SDAI_PBDP_DEVICE_DATA  PbDp;           /**< PROFIBUS DP specific device data */
    struct SDAI_ECAT_DEVICE_DATA  Ecat;           /**< EtherCAT specific device data */
    struct SDAI_MB_DEVICE_DATA    Mb;             /**< Modbus specific device data */
    struct SDAI_EPL_DEVICE_DATA   Epl;            /**< Powerlink specific device data */

  } UseAs;                                        /**< The protocol specific device data */

  char    ProductName [SDAI_PRODUCTNAME_MAX_LEN]; /**< The product name of the device */
  char    OrderId [SDAI_ORDERID_MAX_LEN];         /**< The order ID of the device */
};

/*---------------------------------------------------------------------------*/

/** \brief holds the Exception Data. Please contact Softing with the information from this structure */
struct SDAI_EXCEPTION_DATA
{
  U16   Code;                           /**< Undocumented */

  U16   ChannelId;                      /**< Undocumented */
  U16   ModuleId;                       /**< Undocumented */
  U16   FileId;                         /**< Undocumented */
  U16   Line;                           /**< Undocumented */
  U16   ErrorId;                        /**< Undocumented */

  U32   Parameter;                      /**< Undocumented */
  U8    String [64];                    /**< Undocumented */
};

/*---------------------------------------------------------------------------*/

/** \brief Holds the informations of an incoming output changed indication */
struct SDAI_OUTPUT_CHANGED_IND
{
  U32   NumberUnits;          /**< Holds the number of units in the following array for which the output data have been changed */
  U32   Id [SDAI_MAX_UNITS];  /**< Holds the unique Id for units with changed output data */
};

/*---------------------------------------------------------------------------*/

/** \brief This structure holds the protocol specific configuration data */
struct SDAI_CFG_DATA
{
  U8    InterfaceType;  /**< Specifies the interface on which the I/O data are transferred. Valid values: \ref InterfaceType */
  U8    Alignment [3];

  union
  {
    struct SDAI_PN_CFG_DATA     Pn;    /**< PROFINET IO specific configuration data */
    struct SDAI_EIP_CFG_DATA    Eip;   /**< EtherNet/IP specific configuration data */
    struct SDAI_PBDP_CFG_DATA   PbDp;  /**< PROFIBUS DP specific configuration data */
    struct SDAI_ECAT_CFG_DATA   Ecat;  /**< EtherCAT specific configuration data */
    struct SDAI_MB_CFG_DATA     Mb;    /**< Modbus specific configuration data */
    struct SDAI_EPL_CFG_DATA    Epl;   /**< Powerlink specific configuration data */

  } UseAs;
};

/*---------------------------------------------------------------------------*/

/** \brief This structure holds the protocol specific configuration data */
struct SDAI_CFG_DATA_EXT
{
  U8    InterfaceType;  /**< Specifies the interface on which the I/O data are transferred. Valid values: \ref InterfaceType */
  U8    Alignment;

  U16   PeripheralOffsetInput;  /**< Specifies the Offset of the io in the address windows that is seen by the peripheral bridge. Set to #SDAI_PERIPHERAL_OFFSET_DPRAM to use stanard behaviour */
  U16   PeripheralOffsetOutput; /**< Specifies the Offset of the io in the address windows that is seen by the peripheral bridge. Set to #SDAI_PERIPHERAL_OFFSET_DPRAM to use stanard behaviour */

  union
  {
    struct SDAI_PN_CFG_DATA     Pn;    /**< PROFINET IO specific configuration data */
    struct SDAI_EIP_CFG_DATA    Eip;   /**< EtherNet/IP specific configuration data */
    struct SDAI_PBDP_CFG_DATA   PbDp;  /**< PROFIBUS DP specific configuration data */
    struct SDAI_ECAT_CFG_DATA   Ecat;  /**< EtherCAT specific configuration data */
    struct SDAI_MB_CFG_DATA     Mb;    /**< Modbus specific configuration data */
    struct SDAI_EPL_CFG_DATA    Epl;   /**< Powerlink specific configuration data */

  } UseAs;
};

/*---------------------------------------------------------------------------*/

/** \brief Holds the Informations of an incoming write request. */
/** The service-specific data block is attached directly to the end of the structure SDAI_WRITE_IND. */
struct SDAI_WRITE_IND
{
  union
  {
    struct SDAI_PN_ADDR_DESCR         Pn;     /**< PROFINET IO specific address description */
    struct SDAI_EIP_ADDR_DESCR        Eip;    /**< EtherNet/IP specific address description */
    struct SDAI_PBDP_ADDR_DESCR       PbDp;   /**< PROFIBUS DP specific address description */
    struct SDAI_ECAT_SERVICE_HEADER   Ecat;   /**< EtherCAT specific service header */
    struct SDAI_MB_SERVICE_HEADER     Mb;     /**< Modbus specific service header */
    struct SDAI_EPL_ADDR_DESCR        Epl;    /**< Powerlink specific address description */

  } UseAs;

  U16   Length;                         /**< Number of bytes to be written to the data block */

  U8    Alignment [2];
/*
  U8    Data []; */                     /**< Data to be written */
};

/** \brief Holds the Informations of an outgoing write response. */
/** The protocol specific address description parameters shall be identical with the address description of the
    indication's data block. */
struct SDAI_WRITE_RES
{
  union
  {
    struct SDAI_PN_ADDR_DESCR         Pn;     /**< PROFINET IO specific address description */
    struct SDAI_EIP_ADDR_DESCR        Eip;    /**< EtherNet/IP specific address description */
    struct SDAI_PBDP_ADDR_DESCR       PbDp;   /**< PROFIBUS DP specific address description */
    struct SDAI_ECAT_SERVICE_HEADER   Ecat;   /**< EtherCAT specific service header */
    struct SDAI_MB_SERVICE_HEADER     Mb;     /**< Modbus specific service header */
    struct SDAI_EPL_ADDR_DESCR        Epl;    /**< Powerlink specific address description */

  } UseAs;

  U16   ErrorCode;                      /**< Execution result of the write request. Valid values for the ErrorCode: \ref ServiceErrorCode  */
  U8    Alignment [2];
};

/*---------------------------------------------------------------------------*/

/** \brief Holds the Informations of an incoming read request */
struct SDAI_READ_IND
{
  union
  {
    struct SDAI_PN_ADDR_DESCR         Pn;     /**< PROFINET IO specific address description */
    struct SDAI_EIP_ADDR_DESCR        Eip;    /**< EtherNet/IP specific address description */
    struct SDAI_PBDP_ADDR_DESCR       PbDp;   /**< PROFIBUS DP specific address description */
    struct SDAI_ECAT_SERVICE_HEADER   Ecat;   /**< EtherCAT specific service header */
    struct SDAI_MB_SERVICE_HEADER     Mb;     /**< Modbus specific service header */
    struct SDAI_EPL_ADDR_DESCR        Epl;    /**< Powerlink specific address description */

  } UseAs;

  U16   Length;                         /**< Maximum number of bytes to be read from the data block */

  U8    Alignment [2];
};

/** \brief Holds the Informations of an outgoing read response. */
/** The protocol specific address description parameters shall be identical with the address description of the
    indication's data block. The service-specific data block is attached directly to the end of the
    structure SDAI_READ_RES. */
struct SDAI_READ_RES
{
  union
  {
    struct SDAI_PN_ADDR_DESCR         Pn;     /**< PROFINET IO specific address description */
    struct SDAI_EIP_ADDR_DESCR        Eip;    /**< EtherNet/IP specific address description */
    struct SDAI_PBDP_ADDR_DESCR       PbDp;   /**< PROFIBUS DP specific address description */
    struct SDAI_ECAT_SERVICE_HEADER   Ecat;   /**< EtherCAT specific service header */
    struct SDAI_MB_SERVICE_HEADER     Mb;     /**< Modbus specific service header */
    struct SDAI_EPL_ADDR_DESCR        Epl;    /**< Powerlink specific address description */

  } UseAs;

  U16   ErrorCode;                      /**< Execution result of the read request. Valid values for the ErrorCode: \ref ServiceErrorCode */
  U16   Length;                         /**< Actual length of the data block that is read */
/*
  U8    Data []; */                     /**< The read data */
};

/*---------------------------------------------------------------------------*/

/** \brief Holds the informations about the actually used IO configuration data for one IO unit. */
/** This data block is attached directly to the end of the structure #SDAI_CONTROL_IND, if the ControlCode is set to #SDAI_CONTROL_CFG_DATA_INFO.
    In that case the application has to call sdai_plug_unit() or sdai_plug_unit_ext() again with the passed parameter values of this structure to takeover the new IO configuration and to
    reorganize the process data RAM within the SDAI.
  */
struct SDAI_CONTROL_CFG_DATA_INFO
{
  U32                     Id;           /**< The Id of the unit for which the passed IO configuration data are valid */
  U8                      UnitType;     /**< Actually used unit type. */
  U8                      InputSize;    /**< Actually used input size. */
  U8                      OutputSize;   /**< Actually used output size. */
  U8                      Alignment;
  struct SDAI_CFG_DATA    CfgData;      /**< Actually used protocol specific IO configuration data */
};

/** \brief Holds the informations of an incoming control indication. */
/** The service-specific data block is attached directly to the end of the structure SDAI_CONTROL_IND. */
struct SDAI_CONTROL_IND
{
  union
  {
    struct SDAI_PN_ADDR_DESCR         Pn;     /**< PROFINET IO specific address description */
    struct SDAI_EIP_ADDR_DESCR        Eip;    /**< EtherNet/IP specific address description */
    struct SDAI_PBDP_ADDR_DESCR       PbDp;   /**< PROFIBUS DP specific address description */

    /* EtherCAT specific data not existing */
    /* Modbus specific data not existing */
    /* Powerlink specific data not existing */

  } UseAs;

  U32   ControlCode;                    /**< Action request to process. Valid values for the ControlCode: \ref ControlCode */
  U16   Length;                         /**< The length of the control data */

  U8    Alignment [2];

/*
  U8    Data []; */                     /**< The control data */
};

/** \brief Holds the Informations of an outgoing control response. */
/** The protocol specific address description parameters shall be identical with the address description of the
    indication's data block. */
struct SDAI_CONTROL_RES
{
  union
  {
    struct SDAI_PN_ADDR_DESCR         Pn;     /**< PROFINET IO specific address description */
    struct SDAI_EIP_ADDR_DESCR        Eip;    /**< EtherNet/IP specific address description */
    struct SDAI_PBDP_ADDR_DESCR       PbDp;   /**< PROFIBUS DP specific address description */

    /* EtherCAT specific data not existing */
    /* Modbus specific data not existing */
    /* Powerlink specific data not existing */

  } UseAs;

  U32   ControlCode;                    /**< Action request which has been processed. Identical with the ControlCode of the indication's data block. */
  U16   ErrorCode;                      /**< execution result of the control request. Valid values for the ErrorCode: \ref ServiceErrorCode  */
  U16   Length;                         /**< Data length in bytes of optional service data which shall be send with the control response */

/*
  U8    Data []; */                     /**< Optional service data which shall be send with the control response */
};

/*---------------------------------------------------------------------------*/

/** \brief This structure holds the protocol specific synchronization data */
struct SDAI_SYNC_SIGNAL
{
  union
  {
    /* PROFINET IO specific synchronization data not existing */
    /* EtherNet/IP specific synchronization data not existing */
    /* PROFIBUS DP specific synchronization data not existing */
    /* Powerlink specific synchronization data not existing */
    struct SDAI_ECAT_SYNC_DATA   Ecat;   /* EtherCAT specific synchronization data */

  } UseAs;
};

/*---------------------------------------------------------------------------*/

/** \brief holds the informations of an incoming alarm ack indication */
struct SDAI_ALARM_ACK_IND
{
  U32   Id;                             /**< The Id of the unit the outstanding alarm is acknowledged */
  U16   AlarmType;                      /**< The type of the acknowledged alarm. Valid values: \ref AlarmType */
  U16   AlarmPriority;                  /**< The priority of the acknowledged alarm. Valid values: \ref AlarmPrio */

  U16   ErrorCodeCount;                 /**< The number of 8 bit error codes received from the controller following after this structure */
  U8    Alignment [2];

/*
  U8    ErrorCodes []; */               /**< The received error codes */
};

/** \brief holds the informations of an outgoing alarm request */
struct SDAI_ALARM_REQ
{
  U32   Id;                             /**< The Id of the unit the alarm is for. May be set to SDAI_INVALID_ID if not related to a specific unit. */
  U16   AlarmType;                      /**< The alarm type. Valid values: \ref AlarmType */
  U16   AlarmPriority;                  /**< The priority of the alarm. Valid values: \ref AlarmPrio */

  U16   Length;                         /**< The length of the alarm data. The alarm data must be attached directly at the end of the structure #SDAI_ALARM_REQ. */
  U8    Alignment [2];

/*
  U8    Data []; */                     /**< The alarm data */
};

/*---------------------------------------------------------------------------*/

/** \brief holds the informations of an outgoing diagnosis request */
struct SDAI_DIAGNOSIS_REQ
{
  U32   Id;                             /**< The Id of the unit the diagnosis is for. May be set to SDAI_INVALID_ID if not related to a specific unit. */
  U32   Type;                           /**< Specifies the type of the diagnosis. Defines: \ref DiagType */
  U16   Severity;                       /**< The severity of the diagnosis. Defines: \ref DiagSeverity */
  U16   StatusSpecifier;                /**< Status information of the diagnosis. Defines: \ref DiagStatus */

  U16   Length;                         /**< The length of the diagnosis data. The diagnosis data must be attached directly at the end of the structure #SDAI_DIAGNOSIS_REQ. */
  U8    Alignment [2];

/*
  U8    Data []; */                     /**< The diagnosis data */
};

/*---------------------------------------------------------------------------*/

/** \brief This structure holds the performance data. */
struct SDAI_ADDSTATUS_PERFORMANCE_DATA
{
  U8   CpuLoad100ms;                    /**< Avarage CPU load in the the last 100ms */
  U8   CpuLoad1s;                       /**< Avarage CPU load in the the last 1s */
  U8   CpuLoad10s;                      /**< Avarage CPU load in the the last 10s */

  U8   Alignment;
};

/** \brief This structure holds the state of an Ethernet port. */
struct SDAI_ADDSTATUS_ETH_PORT_DATA
{
  U8  LinkState;        /**< Link state of the port. Valid values for the LinkState: \ref LinkState */
  U8  ConnectionType;   /**< Current connection type for this port (10/100 Mbit, FD/HD). Only valid if LinkState is up. Valid values for the ConnectionType: \ref ConnectionType */
  U8  NegotiationState; /**< Indicates the state of port auto-negotiation. Valid values for the NegotiationState: \ref NegotiateState */
  U8  Alignment1;
  U16 CableLength;      /**< The length of the cable connected to this port in meter. Only valid if LinkState is up. A value of 0xFFFF means cable length could not be detected */
  U8  Alignment2 [2];
};

/** \brief This structure holds the status data of an Ethernet interface. */
struct SDAI_ADDSTATUS_ETH_INTERFACE_DATA
{
  U8                                    InterfaceStatus;              /**< Status of the TCP/IP network interface. Valid values for the InterfaceStatus: \ref InterfaceStatus  */

  U8                                    NumberExistingEthernetPorts;  /**< The number of existing physical Ethernet ports */
  struct SDAI_ADDSTATUS_ETH_PORT_DATA   EthPortData [SDAI_MAX_PORTS]; /**< Array holding the state of the Ethernet ports with "NumberExistingEthernetPorts" valid entries. */
};

/** \brief This structure holds the protocol specific additional status informations. */
struct SDAI_ADDSTATUS_DATA
{
  union
  {
    struct SDAI_PN_ADDSTATUS_DATA     Pn;    /**< PROFINET IO specific additional status data */
    struct SDAI_EIP_ADDSTATUS_DATA    Eip;   /**< EtherNet/IP specific additional status data */
    struct SDAI_PBDP_ADDSTATUS_DATA   PbDp;  /**< PROFIBUS DP specific additional status data */
    struct SDAI_ECAT_ADDSTATUS_DATA   Ecat;  /**< EtherCAT specific additional status data */
    struct SDAI_MB_ADDSTATUS_DATA     Mb;    /**< Modbus specific additional status data */
    struct SDAI_EPL_ADDSTATUS_DATA    Epl;   /**< Powerlink specific additional status data */

  } UseAs;

  struct SDAI_ADDSTATUS_PERFORMANCE_DATA     PerformanceData; /**< Performance data of the stack CPU */
  struct SDAI_ADDSTATUS_ETH_INTERFACE_DATA   InterfaceData;   /**< Ethernet interface data. No valid data for Profibus, Powerlink and EtherCAT */
};

/*---------------------------------------------------------------------------*/

/** \brief This structure holds version strings from the SDAI, Stack and Media Access Control. */
struct SDAI_VERSION_DATA
{
  U8  SdaiVersionString [SDAI_VERSION_STRING_LENGTH];               /**< Version of the SDAI */
  U8  MacVersionString [SDAI_VERSION_STRING_LENGTH];                /**< Version of Media Access Control (MAC) e.g. Switch/PB IP Core */
  U8  StackVersionString [SDAI_VERSION_STRING_LENGTH];              /**< Version of the protocol stack and mapping layer */
};

/*---------------------------------------------------------------------------*/

/** \brief The Callback structure holds the functions implemented by the user which
  * are called by the SDAI on the corresponding event.
  *
  * The data for these events are stored within the service indication interface of the backend stack.
  * During the execution of a callback function the SDAI locks the access to the service indication
  * interface. If now the protocol stack tries to access the service indication interface (e.g. to signal changed
  * output data) the stack will be blocked until the interface is unlocked. Therefore the
  * runtime of the backend stack depends on the actions performed by the user-specific callback
  * functions. Communication timeouts are possible causing cyclic/acyclic connections to
  * terminate if actions are performed that take a long time, e.g. waiting for operating
  * system resources, delaying the task or printing log messages. To avoid these communication
  * timeouts the user shall only copy the data and return as fast as possible. For each function
  * not implemented by the application a NULL pointer shall be set. (see also \ref sdai_event_handling)
  */
struct SDAI_CALLBACKS
{
  void (*IdentDataCbk)    (struct SDAI_IDENT_DATA*);          /**< Called when identification data has been changed */
  void (*ExceptionCbk)    (struct SDAI_EXCEPTION_DATA*);      /**< Called when a Exception has been occurred */
  void (*DataCbk)         (struct SDAI_OUTPUT_CHANGED_IND*);  /**< Called when new output data are available */
  void (*AlarmAckCbk)     (struct SDAI_ALARM_ACK_IND*);       /**< Called when an alarm is acknowlegded */
  void (*WriteReqCbk)     (struct SDAI_WRITE_IND*);           /**< Called on incoming write request */
  void (*ReadReqCbk)      (struct SDAI_READ_IND*);            /**< Called on incoming read request */
  void (*ControlReqCbk)   (struct SDAI_CONTROL_IND*);         /**< Called on incoming control request */
  void (*SyncSignalCbk)   (struct SDAI_SYNC_SIGNAL*);         /**< Called on incoming synchronization signal */
};

/*---------------------------------------------------------------------------*/

/** \brief This structure holds the initialization information */
struct SDAI_INIT
{
  U8                        BackEnd;      /**< specifies the backend protocol. Valid values for the BackEnd: \ref BackEnd */
  U8                        Alignment [3];
  struct SDAI_IDENT_DATA    Ident;        /**< Identification data for the Device */
  struct SDAI_DEVICE_DATA   Info;         /**< General Informations about the Device */
  struct SDAI_CALLBACKS     Callback;     /**< The callback functions implemented by the user */
};

/*---------------------------------------------------------------------------*/

#ifndef _AVOID_UNUSED_WARNING
#define _AVOID_UNUSED_WARNING(Parameter)           do { (void) (Parameter); } while (0)
#endif

/******************************************************************************
FUNCTIONS
******************************************************************************/

#ifdef __cplusplus
  extern "C" {
#endif
/** \anchor SDAI-Functions @name SDAI Functions
* All SDAI functions which can be used by the application */
//@{
extern U8   sdai_init                         (struct SDAI_INIT*);
extern U8   sdai_deinit                       (void);
extern U8   sdai_plug_unit                    (U8, U8, U8, U32*, const struct SDAI_CFG_DATA*);
extern U8   sdai_plug_unit_ext                (U8, U8, U8, U32*, const struct SDAI_CFG_DATA_EXT*);
extern U8   sdai_pull_unit                    (U32);
extern U8   sdai_get_version                  (struct SDAI_VERSION_DATA*);
extern U8   sdai_get_state                    (U16*, struct SDAI_ADDSTATUS_DATA*);

extern U8   sdai_get_data                     (U32, U8*, U8*);
extern U8   sdai_set_data                     (U32, U8, const U8*);
extern U8   sdai_process_data_update_finished (void);

extern U8   sdai_alarm_request                (const struct SDAI_ALARM_REQ*);
extern U8   sdai_diagnosis_request            (const struct SDAI_DIAGNOSIS_REQ*);
extern U8   sdai_write_response               (const struct SDAI_WRITE_RES*);
extern U8   sdai_read_response                (const struct SDAI_READ_RES*);
extern U8   sdai_control_response             (const struct SDAI_CONTROL_RES*);

extern U8   sdai_init_watchdog                (const U16);
extern U8   sdai_check_and_trigger_watchdog   (void);

extern U8   sdai_lock_resources               (U16);
extern U8   sdai_unlock_resources             (U16);
//@}
#ifdef __cplusplus
  }
#endif

#endif /* __SDAI_H__ */

/*****************************************************************************/
