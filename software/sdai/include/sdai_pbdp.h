/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

/**
 *
 * @page pbdp PROFIBUS DP
 *
 * \tableofcontents
 *
 * @section pbdpoverview Overview
 * PROFIBUS DP is an open bus system according to the international standard IEC 61158. It is suitable for fast cyclic
 * and acyclic data exchange between an automation system and the decentralized periphery.
 *
 * @subsection pbdp_features Features
 * - Cyclic PROFIBUS DP/V0 services
 * - PROFIBUS Master Class 2 (MS2) Services of DP/V1
 *   - Read
 *   - Write
 *   - Initiate
 *   - Abort
 * - PROFIBUS Master Class 1 (MS1) Services of DP/V1
 *   - Read
 *   - Write
 *   - Alarm-Handling (<b>not yet supported</b>)
 * - Slave address can be changed by the master (<b>not yet supported</b>)
 * - Sync/Freeze functionality supported
 * - Dynamic I/O configuration supported (<b>partially supported, see also \ref pb_io_cfg_dynamic</b>)
 * - Automatic baudrate detection supported
 * - "Module Status" and "Identifier-related" diagnosis information automatically created and managed by SDAI
 * - Identification & Maintenance services supported
 * - DP/V2 clock synchronization (<b>not yet supported</b>)
 * - Demo Application with a dedicated device description file aimed at quickly getting you started with
 *   implementing your own PROFIBUS DP slave application
 *
 * \note Features marked with <b>"not yet supported"</b> are not supported by the actual version but are planned for future releases.
 *
 * @section pbdpmapping Mapping PROFIBUS DP to SDAI
 * In the following chapters the mapping of the PROFIBUS DP Slave Protocol Software to the SDAI is described.
 *
 * @subsection pbdp_init_data PROFIBUS DP Initialization
 * The structure #SDAI_INIT contains identification data, device specific data and a set of application specific
 * callback functions. The identification data are described by the structure #SDAI_IDENT_DATA and device specific
 * data are described by the structure #SDAI_DEVICE_DATA. These parameters are mapped to PROFIBUS DP in the following way:
 *
 * <TABLE><TR>
 * <TH>SDAI</TH><TH>PROFIBUS DP</TH><TH>Description</TH>
 * </TR><TR>
 * <TD>DevName</TD><TD>---</TD><TD>Ignored (not used by PROFIBUS DP)</TD>
 * </TR><TR>
 * <TD>IfName</TD><TD>---</TD><TD>Ignored (not used by PROFIBUS DP)</TD>
 * </TR><TR>
 * <TD>SerialNumber</TD><TD>I&M0, Par. SERIAL_NUMBER</TD><TD>Mapped to a visible string of max. 16 octets</TD>
 * </TR><TR>
 * <TD>VendorID</TD><TD>I&M0, Par. MANUFACTURER_ID</TD><TD>Set to 0x0000 if not existing.</TD>
 * </TR><TR>
 * <TD>Type</TD><TD>I&M0, Par. PROFILE_SPECIFIC_TYPE</TD><TD></TD>
 * </TR><TR>
 * <TD>ProductCode</TD><TD>---</TD><TD>Ignored (not used by PROFIBUS DP)</TD>
 * </TR><TR>
 * <TD>SoftwareRevision</TD><TD>I&M0, Par. SOFTWARE_REVISION</TD><TD></TD>
 * </TR><TR>
 * <TD>HardwareRevision</TD><TD>I&M0, Par. HARDWARE_REVISION</TD><TD></TD>
 * </TR><TR>
 * <TD>RevisionCounter</TD><TD>I&M0, Par. REVISION_COUNTER</TD><TD></TD>
 * </TR><TR>
 * <TD>ProfileIdentNumber</TD><TD>I&M0, Par. PROFILE_ID</TD><TD></TD>
 * </TR><TR>
 * <TD>ProductName</TD><TD>---</TD><TD>Ignored (not used by PROFIBUS DP)</TD>
 * </TR><TR>
 * <TD>OrderId</TD><TD>I&M0, Par. ORDER_ID</TD><TD></TD>
 * </TR></TABLE>
 *
 * The structure #SDAI_CALLBACKS is used to register all supported application specific callback functions.
 * The following table shows which SDAI callback functions are used by PROFIBUS DP. If not supported by the
 * application the user shall set the respective function pointer to NULL.
 *
 * <TABLE><TR>
 * <TH>SDAI Callback</TH><TH>PROFIBUS DP</TH>
 * </TR><TR>
 * <TD>IdentDataCbk()</TD><TD>Called when the network parameter (e.g. Slave address) are changed</TD>
 * </TR><TR>
 * <TD>ExceptionCbk()</TD><TD>Called when a fatal error occured</TD>
 * </TR><TR>
 * <TD>DataCbk()</TD><TD>Called when the output data or status of a unit has been changed</TD>
 * </TR><TR>
 * <TD>AlarmAckCbk()</TD><TD>Called when an outstanding alarm is acknowledged by the DP-master</TD>
 * </TR><TR>
 * <TD>WriteReqCbk()</TD><TD>Called when a write request is received</TD>
 * </TR><TR>
 * <TD>ReadReqCbk()</TD><TD>Called when a read request is received</TD>
 * </TR><TR>
 * <TD>ControlReqCbk()</TD><TD>Called when the IO configuration for a module has been changed</TD>
 * </TR><TR>
 * <TD>SyncSignalCbk()</TD><TD>Not used (set function pointer always to NULL)</TD>
 * </TR></TABLE>
 *
 * @subsection pbdp_plug_unit PROFIBUS DP plugging of units
 * I/O modules are assigned to a specific slot. This assignment is done by the SDAI automatically in the order
 * of plugging the SDAI units by the application. That is the first unit will be assigned to slot 0, the second
 * to slot 1, and so on.
 *
 * PROFIBUS DP also allows "empty slots". E.g. if no I/O module is plugged to a specific slot the application may set the
 * unit type to #SDAI_PBDP_UNIT_TYPE_EMPTY_MODULE and the parameters InputSize and OutputSize to 0 when calling sdai_plug_unit()
 * at startup. When removing a module at runtime the application can call the function sdai_pull_unit() to remove the respective
 * module. In that case the SDAI automatically signals the missing module via a "module status" diagnosis.
 *
 * The SDAI automatically creates the PROFIBUS configuration identifier based on the parameters passed with the sdai_plug_unit() call.
 * With that function call the application can optionally specify the data types for each process variable mapped
 * to a module. Alternatively manufacturer specific configuration data can be specified. For that purpose the structure
 * #SDAI_PBDP_CFG_DATA is provided. The parameters InputFlags and OutputFlags of this structure also allows to specify the data consistency and the
 * format of the IO data (byte or word structure). The following table shows some examples how the parameters of the function
 * sdai_plug_unit() are mapped to the PROFIBUS configuration identifier:
 *
 * <TABLE><TR>
 * <TH>UnitType</TH><TH>InputSize</TH><TH>OutputSize</TH><TH>InputFlags</TH><TH>OutputFlags</TH><TH>PROFIBUS Configuration Identifier</TH>
 * </TR><TR>
 * <TD>#SDAI_UNIT_TYPE_GENERIC_INPUT</TD><TD>1</TD><TD>0</TD><TD>0</TD><TD>0</TD><TD>0x10</TD>
 * </TR><TR>
 * <TD>#SDAI_UNIT_TYPE_GENERIC_OUTPUT</TD><TD>0</TD><TD>1</TD><TD>0</TD><TD>0</TD><TD>0x20</TD>
 * </TR><TR>
 * <TD>#SDAI_UNIT_TYPE_GENERIC_INPUT</TD><TD>2</TD><TD>0</TD><TD>#SDAI_PBDP_CFG_FLAG_DATA_LENGTH_IN_WORDS</TD><TD>0</TD><TD>0x50</TD>
 * </TR><TR>
 * <TD>#SDAI_UNIT_TYPE_GENERIC_INPUT</TD><TD>128</TD><TD>0</TD><TD>#SDAI_PBDP_CFG_FLAG_MODULE_CONSISTENCY</TD><TD>0</TD><TD>0x40, 0xFF</TD>
 * </TR><TR>
 * <TD>#SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT</TD><TD>2</TD><TD>2</TD><TD>#SDAI_PBDP_CFG_FLAG_DATA_LENGTH_IN_WORDS</TD><TD>#SDAI_PBDP_CFG_FLAG_DATA_LENGTH_IN_WORDS</TD><TD>0x70</TD>
 * </TR><TR>
 * <TD>#SDAI_PBDP_UNIT_TYPE_EMPTY_MODULE</TD><TD>0</TD><TD>0</TD><TD>0</TD><TD>0</TD><TD>0x00 (empty slot)</TD>
 * </TR></TABLE>
 *
 * If possible, the SDAI always uses the compact format when creating the PROFIBUS configuration identifier. The special identifier format is only used
 * in the following cases:
 * - Manufacturer specific configuration data are available
 * - Data length for input or output data cannot be described by the compact format. This is the case if the IO data length is greater than 32 byte.
 * - Data length of input and output data are not equal for a module of type #SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT (with the compact format only a common length is possible).
 * - InputFlags and OutputFlags (consistency and format) are not equal for a module of type #SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT (with the compact format only common flags are possible).
 *
 * By calling sdai_plug_unit() a Unit ID is automatically generated. This Unit ID consists of the PROFIBUS DP
 * slot number. See also \ref PbUnitId. The maximum I/O data size for one unit is limited to #SDAI_PBDP_MAX_UNIT_IO_DATA_SIZE.
 * Also the maximum overall I/O data size for all units is limited by the size of the Data-Exchange frames which is
 * 244 bytes for input and 244 bytes for output data.
 *
 * @subsection pb_io_cfg_dynamic PROFIBUS DP Dynamic IO Configuration
 * The PROFIBUS DP implementation supports either a static I/O configuration with one or more fixed I/O modules
 * plugged by the application (see also \ref io_cfg_static). For advanced device applications differences between
 * default and configured modules may be possible. For that purpose also a dynamic I/O configuration is
 * supported (see \ref io_cfg_dynamic). This allows the DP-master to change the I/O data layout of a existing
 * module as well as the assignment of I/O modules to specific slots. During startup the DP-master sends a configuration frame
 * (Chk_Cfg request) with the required I/O configuration. This frame contains one configuration identifier for each slot.
 * If static configuration is activated by the application the cyclic I/O connection can only be established if the
 * received configuration is equal to the I/O configuration defined by the application. If dynamic configuration is activated,
 * the application is informed about the changed I/O configuration for each slot via the callback function ControlReqCbk().
 * When the application can check the received configuration. The following configuration scenarios are supported:
 * -# New configuration is accepted and taken over. The application calls sdai_plug_unit() with the new configuration parameters
 *    and afterwards sdai_control_response() with the error code set to #SDAI_SERVICE_SUCCESS. The slave enters data
 *    exchange state in that case.
 * -# New configuration is accepted but \b not taken over. This may be the case if the requested I/O Module is supported,
 *    but a different one is physically plugged. In that case the application also has to call sdai_plug_unit(). But now
 *    the configuration data of the physically plugged module is set by the application. Afterwards sdai_control_response() is called
 *    with the error code set to #SDAI_SERVICE_SUCCESS. The slave enters data exchange state and the SDAI signals a "module status"
 *    diagnosis to the DP-master. The "module status" diagnosis shows for which slot a configuration mismatch has been occurred and
 *    thus a easy and fast detection of the configuration mismatch for modular slaves is possible (see also \ref pbdp_diag_alarm).
 * -# New configuration is rejected. This may be the case if the requested I/O Module is not supported or the configuration
 *    for a module is invalid. In that case the application calls sdai_control_response() with the error code set to #SDAI_SERVICE_ERR_WRONG_CFG_DATA.
 *    A configuration error is signalled to the DP-master within the diagnosis frame and the slave did not change to the
 *    data exchange state.
 *
 * \note Currently the IO configuration can be changed during runtime only by the DP-master. Dynamically changing the IO configuration
 *       by the slave application, e.g. when a new module is physically plugged, is not yet supported. Thus the function sdai_plug_unit()
 *       shall only be called in response to the control indication #SDAI_CONTROL_CODE_CFG_DATA_INFO. In addition the function sdai_pull_unit()
 *       is not yet supported.
 *
 * @subsection pbdp_acyclic PROFIBUS DP Acyclic Communication
 * The extended functions for acyclic communication enable master and slaves to transmit acyclic Read and Write functions
 * or to acknowledge alarms, parallel to the cyclic exchange of process data. Thus, without disturbing the operation of
 * a running plant, the device parameters of the slave device can be optimized and the current status of the device can
 * be read out by the DP-master. In this manner, PROFIBUS-DP fulfills the requirements of complex devices which must be
 * parameterized also during operation.
 *
 * For addressing the acyclic data, PROFIBUS proceeds on the assumption of a modular structure. Slaves with no modular physical
 * structure must nevertheless present themselves as structured, i.e. as logical functional units. All data available for
 * acyclic Read or Write access are regarded as belonging to modules and can be addressed by means of slot and index. The slot
 * number addresses the module; the index addresses a specific data block within the module. The use of slot and index is defined
 * in the individual profile specifications. With the data length information in the Read or Write request, it is possible to
 * read or write only parts of a data block. In case of successful data access, the slave gives a positive response. If data access
 * was denied, the data in the negative response classify the error, see also \ref pbdp_errors.
 *
 * Softing's PROFIBUS DP slave implementation supports the acyclic MS1 as well as the MS2 communication of the DPV1 protocol enhancements:
 * - A Master Class 2, typically an engineering tool, establishes a MS2 connection to the slave to access the device parameters
 *   via Read/Write services. The application is informed when the DP-master requests the establishment of an acyclic class 2 connection via
 *   the Initiate service. With the service indication data the SDAI defines the communication reference, see structure #SDAI_PBDP_ADDR_DESCR.
 *   If subsequently a Master Class 2 Read/Write service is received from the service partner, the SDAI sets the corresponding communication
 *   reference in the indication data.
 * - A Master Class 1, typically a PLC, accesses the device parameters via Read/Write services. In addition the slave can send alarms
 *   to inform the DP-master about process-related problems. The acyclic MS1 communication is bound to the cyclic data exchange connection
 *   and only enabled if DPV1 mode is selected by the DP-master.
 *
 * The SDAI supports the I&M functionality (Identification & Maintenance) required for all slaves supporting the DPV1 protocol enhancements.
 * Therefore the I&M0 object is implemented by the SDAI showing informations related to the whole device. The parameters of this object
 * are set with the initialization parameters passed by the application, see \ref pbdp_init_data for more informations. For full I&M
 * support, e.g. for each I/O module a separate I&M object shall be supported, the application can set the flag #SDAI_PBDP_DISABLE_INTERNAL_IM_SUPPORT within
 * parameter Flags of structure #SDAI_PBDP_IDENT_DATA.
 *
 * @subsection pbdp_control_code PROFIBUS DP Control Codes
 * If a control indication is signalled to the application via the ControlReqCbk() the parameter ControlCode of the structure #SDAI_CONTROL_IND
 * specifies the action which shall be performed.The following control codes are supported:
 * - #SDAI_CONTROL_CODE_CFG_DATA_INFO \n
 *   Used to signal a changed I/O configuration to the application. See also \ref pb_io_cfg_dynamic.
 * - #SDAI_PBDP_CONTROL_CODE_PRM_DATA \n
 *   Used to transfer user specific parameterization data to the application. These data are received from the DP-master during establishment
 *   of the cyclic I/O connection via the Set_Prm frame. The application can check the received parameterization data and either accept
 *   (error code set to #SDAI_SERVICE_SUCCESS) or reject it (error code set to #SDAI_SERVICE_ERR_INVALID_PARAMETER). If rejected,
 *   the stack generates a diagnosis signalling a parameterization fault to the DP-master.
 * - #SDAI_PBDP_CONTROL_CODE_INITIATE \n
 *   Used to signal an Initiate request received from the DP-master to establish an acyclic MS2 connection.
 * - #SDAI_PBDP_CONTROL_CODE_ABORT \n
 *   An acyclic MS2 connection has been aborted either by the DP-master or locally by the DP-slave.
 *
 * @subsection pbdp_errors PROFIBUS DP Service Error Codes
 * The following table shows how the SDAI common service error codes (see also \ref ServiceErrorCode) are mapped
 * to PROFIBUS DP specific service error codes. These error codes can be set by the application when responding to acyclic
 * Read and Write service requests.
 *
 * <TABLE><TR>
 * <TH>SDAI Service Error Code</TH><TH>Error Code 1</TH><TH>Error Code 2</TH><TH>Comment</TH>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_SUCCESS</TD><TD>0x00</TD><TD>0x00</TD><TD>---</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_OBJECT_NOT_EXISTING</TD><TD>0xB2</TD><TD>0x00</TD><TD>invalid slot</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_INVALID_SIZE</TD><TD>0xB1</TD><TD>0x00</TD><TD>write length error</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_STATE_CONFLICT</TD><TD>0xB5</TD><TD>0x00</TD><TD>---</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_APPLICATION</TD><TD>0xA0/0xA1</TD><TD>0x00</TD><TD>error read/error write</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_RESOURCE</TD><TD>0xC2</TD><TD>0x00</TD><TD>resource busy</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_NOT_SUPPORTED</TD><TD>0xA9</TD><TD>0x00</TD><TD>feature not supported</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_ACCESS_DENIED</TD><TD>0xB6</TD><TD>0x00</TD><TD>---</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_INVALID_PARAMETER</TD><TD>0xB0</TD><TD>0x00</TD><TD>invalid index</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_FILE_NOT_FOUND</TD><TD>0xC3</TD><TD>0x00</TD><TD>resource unavailable</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_DISK_FULL</TD><TD>0xC3</TD><TD>0x00</TD><TD>resource unavailable</TD>
 * </TR></TABLE>
 *
 * The SDAI common service error codes can be used for all protocols. However, the application may require the support of specific
 * PROFIBUS error codes for wrong access to application objects which are not completely covered by the SDAI common service error codes.
 * For that purpose the application can directly specify the PROFIBUS DP specific Error Code 1 within the field #SDAI_PBDP_ERROR_CODE_1_MASK of
 * the parameter ErrorCode. Also the application can specify the Error Code 2 within the field #SDAI_PBDP_ERROR_CODE_2_MASK
 * of the parameter ErrorCode. The SDAI common service error code is used if the field #SDAI_PBDP_ERROR_CODE_1_MASK of the parameter
 * ErrorCode is set to 0.
 *
 * @subsection pbdp_diag_alarm PROFIBUS DP Diagnosis and Alarms
 * The SDAI realizes a diagnosis structure in a standardized hierarchical manner as recommended by the
 * PROFIBUS Guideline: Part 3 Diagnosis and Alarms, V1.00. The SDAI allows transferring diagnosis data
 * according to DPV0 or DPV1 specification. Depending on the supported diagnosis format of the automation system
 * the respective structure is used. The selection is made by the DP-master via the Set-Prm telegram based on the
 * used GSD file.
 *
 * The following figure shows the diagnosis structure in \b DPV0 mode:
 *
 * @image html PbDpv0DiagModel.png
 *
 * The structure starts with a standard diagnosis block. This diagnosis information is managed by the protocol stack,
 * typically required for the startup of the cyclic I/O connection. This structure is followed by a device related
 * diagnosis block showing manufacturer specific informations related to the whole slave device. The structure is described
 * by the GSD file. A identifier-related diagnosis block follows which contains a bitfield with one bit for every I/O module.
 * If set, the respective module has an error. The identifier-related diagnosis block is automatically created and
 * managed by the SDAI. However, these informations may not be sufficient to fix an error. Thus the application can provide
 * additional diagnosis information. For that purpose a channel-related diagnosis may follow which shows failures related
 * to an I/O channel, e.g. short circuit, undervoltage, overvoltage, overtemperature.
 *
 * \b Note: The application has to create the device related diagnosis block at startup by calling the function sdai_init(),
 * see structure #SDAI_PBDP_DEVICE_DATA. If not, the device related diagnosis block is missing and the first structure attached
 * after the standard diagnosis block is the identifier-related diagnosis block.
 *
 * The following figure shows the diagnosis structure in \b DPV1 mode:
 *
 * @image html PbDpv1DiagModel.png
 *
 * The structure starts with a standard diagnosis block. This diagnosis information is managed by the protocol stack,
 * typically required for the startup of the cyclic I/O connection. This structure is followed by a identifier-related
 * diagnosis block containing a bitfield with one bit for every I/O module. If set, the respective module has
 * an error. A "Module Status" block follows which provides more informations about the status of an I/O module, e.g.
 * Module ok and data valid, Module correct and data invalid, wrong module or no module plugged. These module related
 * diagnosis informations are automatically created and managed by the SDAI. However, these informations may not be
 * sufficient to fix an error. Thus the application can provide additional diagnosis information. For that purpose either a
 * "diagnosis alarm" or a "status message" may be used to provide more specialized diagnosis information. The structure
 * is application specific and must be described by the GSD file. However, also the channel-related diagnosis may be used here.
 * In addition further application specific Status and Alarm messages may be used by the application.
 *
 * To inform the DP-master about errors which may cause a failure of the device the function sdai_diagnosis_request() is provided.
 * The PROFIBUS DP specific diagnosis data are defined by structure #SDAI_PBDP_DIAGNOSIS_DATA. This structure must be attached
 * directly at the end of the structure #SDAI_DIAGNOSIS_REQ which holds the protocol-independent diagnosis informations.
 *
 * For status informations related to the controlled process the function sdai_alarm_request() is provided. The PROFIBUS DP specific
 * alarm data are defined by structure #SDAI_PBDP_ALARM_DATA. The PROFIBUS DP specific alarm specifier can be set via the parameter
 * AlarmPriority of structure #SDAI_ALARM_REQ in the following way:
 * - #SDAI_ALARM_PRIO_LOW mapped to "Fault disappeared and slot ok"
 * - #SDAI_ALARM_PRIO_HIGH mapped to "Fault appeared and slot is not ok"
 * - #SDAI_PBDP_ALARM_PRIO_NONE mapped to "No further differentiation"
 *
 * The structure #SDAI_PBDP_ALARM_DATA must be attached directly at the end of the structure #SDAI_ALARM_REQ which holds the
 * protocol-independent alarm informations.
 *
 * The following table shows which SDAI function is used to transfer the respective diagnosis/alarm information:
 *
 * <TABLE><TR>
 * <TH>PROFIBUS DP Diagnosis/Alarm</TH><TH>SDAI Function</TH><TH>Comment</TH>
 * </TR><TR>
 * <TD>Channel Related Diagnosis</TD><TD>sdai_diagnosis_request()</TD><TD></TD>
 * </TR><TR>
 * <TD>Device Related Diagnosis</TD><TD>sdai_diagnosis_request()</TD><TD></TD>
 * </TR><TR>
 * <TD>Diagnosis Alarm</TD><TD>sdai_diagnosis_request()</TD><TD>Alarm Acknowledge from DP-master handled by SDAI</TD>
 * </TR><TR>
 * <TD>Status Messages</TD><TD>sdai_diagnosis_request()</TD><TD></TD>
 * </TR><TR>
 * <TD>Alarm Messages</TD><TD>sdai_alarm_request()</TD><TD>"Diagnosis Alarm" send via sdai_diagnosis_request()</TD>
 * </TR></TABLE>
 *
 * \b Note: The SDAI deletes all active diagnosis entries if the cyclic connection to the DP-master is terminated. This is necessary
 * to avoid invalid diagnosis entries in the case the module configuration or the diagnosis model (DPV0 vs. DPV1) is changed by the DP-master. Thus
 * the application has to create these diagnosis messages again if the cyclic connection is reestablished. A restart of the cyclic connection
 * can be detected by reading the PROFIBUS specific status information via function sdai_get_state().
 *
 * The SDAI common severity is mapped to the Ext_Diag flag of the PROFIBUS diagnosis response frame. If set, a failure is signaled to the DP-master.
 * If not set, either no extended diagnosis data are available or only a warning / status message is signaled to the DP-master. The Ext_Diag flag
 * is set if the application has created at least one error with the SDAI severity set to either #SDAI_DIAGNOSIS_SEVERITY_FAILURE or #SDAI_DIAGNOSIS_SEVERITY_CHECK_FUNCTION.
 * The Ext_Diag flag will only be reset if \b all errors with the severity #SDAI_DIAGNOSIS_SEVERITY_FAILURE and #SDAI_DIAGNOSIS_SEVERITY_CHECK_FUNCTION
 * have been fixed.
 *
 * The SDAI common diagnosis type (see also \ref DiagType) is mapped to the error type field of a channel related diagnosis (see
 * also structure #SDAI_PBDP_CHANNEL_RELATED_DIAG). The following table shows how this mapping is realized.
 *
 * <TABLE><TR>
 * <TH>SDAI Diagnosis Type</TH><TH>Channel Error Type</TH><TH>Description</TH>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_SHORT_CIRCUIT</TD><TD>0x01</TD><TD>Short Circuit</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_INPUT_UNDERVOLTAGE</TD><TD>0x02</TD><TD>Undervoltage</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_INPUT_OVERVOLTAGE</TD><TD>0x03</TD><TD>Overvoltage</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OUTPUT_UNDERVOLTAGE</TD><TD>0x02</TD><TD>Undervoltage</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OUTPUT_OVERVOLTAGE</TD><TD>0x03</TD><TD>Overvoltage</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_DEVICE_UNDERVOLTAGE</TD><TD>0x02</TD><TD>Undervoltage</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_DEVICE_OVERVOLTAGE</TD><TD>0x03</TD><TD>Overvoltage</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_INPUT_UNDERCURRENT</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_INPUT_OVERCURRENT</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OUTPUT_UNDERCURRENT</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OUTPUT_OVERCURRENT</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_DEVICE_UNDERCURRENT</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_DEVICE_OVERCURRENT</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OVERLOAD</TD><TD>0x04</TD><TD>Overload</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OVERTEMPERATURE</TD><TD>0x05</TD><TD>Overtemperature</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_UNDERTEMPERATURE</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_AMBIENTE_OVERTEMPERATURE</TD><TD>0x05</TD><TD>Overtemperature</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_AMBIENTE_UNDERTEMPERATURE</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_LINE_BREAK</TD><TD>0x06</TD><TD>Line Break</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_UPPER_LIMIT_EXCEEDED</TD><TD>0x07</TD><TD>Upper limit exceeded</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_LOWER_LIMIT_EXCEEDED</TD><TD>0x08</TD><TD>Lower limit exceeded</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_GENERIC</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_SIMULATION_ACTIVE</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_PRM_MISSING</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_PRM_FAULT</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_POWER_SUPPLY_FAULT</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_FUSE_BLOWN</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_COMMUNICATION_FAULT</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_GROUND_FAULT</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_REFERENCE_POINT_LOST</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_SAMPLING_ERROR</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_THRESHOLD_WARNING</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OUTPUT_DISABLED</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_SAFETY_EVENT</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_EXTERNAL_FAULT</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_TEMPORARY_FAULT</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_DEVICE_HARDWARE</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_DEVICE_SOFTWARE</TD><TD>0x09</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_MONITORING</TD><TD>0x09</TD><TD>Error</TD>
 * </TR></TABLE>
 *
 * If the common diagnosis types does not match the application needs, it is also possible to create manufacturer
 * specific PROFIBUS channel errors. For that purpose the bit #SDAI_DIAGNOSIS_TYPE_PROTOCOL_SPECIFIC
 * in the diagnosis type must be set. The manufacturer specific PROFIBUS channel error is set by the application
 * in the low byte. The range for manufacturer specific error types is from 16 - 31. In that case the manufacturer
 * specific PROFIBUS channel error must be described within the GSD file. This may also be applicable to provide
 * better error informations for common SDAI diagnosis type which are not supported by the PROFIBUS protocol, e.g.
 * #SDAI_DIAGNOSIS_TYPE_DEVICE_HARDWARE will be mapped to the general error code 0x09 by default. Example:
 * \code{.c}
 *
 * #define PB_MANUFACTURER_CHANNEL_ERROR_TYPE_DEVICE_HARDWARE   (16)
 *
 * struct T_DEMO_DIAGNOSIS_REQ
 * {
 *   struct SDAI_DIAGNOSIS_REQ          DiagReq;
 *   struct SDAI_PBDP_DIAGNOSIS_DATA    DiagPbDpData;
 * };
 *
 * static struct T_DEMO_DIAGNOSIS_REQ    Diagnosis;
 *
 * void profibus_send_diagnosis (void)
 * {
 *   Diagnosis.DiagReq.Type                                     = (SDAI_DIAGNOSIS_TYPE_PROTOCOL_SPECIFIC | PB_MANUFACTURER_CHANNEL_ERROR_TYPE_DEVICE_HARDWARE);
 *   Diagnosis.DiagReq.Severity                                 = SDAI_DIAGNOSIS_SEVERITY_FAILURE;
 *   Diagnosis.DiagReq.StatusSpecifier                          = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS;
 *   Diagnosis.DiagReq.Length                                   = sizeof (SDAI_PBDP_DIAGNOSIS_DATA);
 *
 *   Diagnosis.DiagPbDpData.DiagType                            = SDAI_PBDP_DIAG_TYPE_CHANNEL_RELATED;
 *   Diagnosis.DiagPbDpData.UseAs.ChannelDiag.ChannelNumber     = 0;
 *   Diagnosis.DiagPbDpData.UseAs.ChannelDiag.ChannelProperties = (SDAI_PBDP_CHANNEL_TYPE_1_BIT | SDAI_PBDP_CHANNEL_SELECTION_INPUT);
 *
 *   sdai_diagnosis_request (&Diagnosis.DiagReq);
 *   return;
 * }
 *
 * \endcode
 *
 */

#ifndef __SDAI_PBDP_H__
#define __SDAI_PBDP_H__

/******************************************************************************
DEFINES
******************************************************************************/

/** \anchor PbUnitId @name SDAI Unit ID macros for PROFIBUS DP
* Format of the SDAI Unit ID for PROFIBUS DP:
\verbatim
   ---------------------------------------------------------------
   |            Not used         | PB Slot equv. SDAI unit index |
   ---------------------------------------------------------------
   32         High Word          16           Low Word           0
\endverbatim
*/
//@{
#define _SDAI_PBDP_ID_TO_SLOT(Type, Id)         ((Type) ((Id) & 0x000000FFuL))   /**< Converts the SDAI Id to the PROFIBUS DP Slot */
#define _SDAI_PBDP_ID_TO_INDEX(Type, Id)        _SDAI_PBDP_ID_TO_SLOT(Type, Id)  /**< Converts the SDAI Id to the unit index used in the interface */
//@}

/*---------------------------------------------------------------------------*/

/** \anchor PbIdentFlags @name PROFIBUS DP specific interface configuration
* Bitfield holding definitions for PROFIBUS DP specific interface configuration:
*/
//@{
#define SDAI_PBDP_SET_SLAVE_ADDR_SUPPORTED      0x01  /**< The slave address can be changed via the network. A new slave address is signaled to the
                                                           application via the IdentData callback function. Thus a respective callback function must
                                                           be registered (see #SDAI_CALLBACKS) if this feature is activated. */
#define SDAI_PBDP_DISABLE_INTERNAL_IM_SUPPORT   0x02  /**< By default, all acyclic Read/Write requests to slot 0, index 255 are processed internally
                                                           by the SDAI to handle access to the I&M0 object. If this flag is set all acyclic Read/Write
                                                           requests are passed to the application. In that case the internal I&M support is deactivated
                                                           and the application has to implement the required I&M objects. */
//@}

/** \anchor SlaveAddr @name Slave Address
 * PROFIBUS DP specific slave address definitions */
//@{
#define SDAI_PBDP_DEFAULT_SLAVE_ADDRESS                 126   /**< Reserved delivery address for stations allowing the address to be set via the network.
                                                                   IO data exchange is not possible with this address. Within parameter Flags the bit
                                                                   SDAI_PBDP_SET_SLAVE_ADDR_SUPPORTED must be set by the application. */
//@}

/* Definitions for PROFIBUS DP specific device data */
#define SDAI_PBDP_VERSION_MAX_LEN      32         /**< Maximum length of the PROFIBUS DP SW/HW version string */

/*---------------------------------------------------------------------------*/

#define SDAI_PBDP_UNIT_TYPE_EMPTY_MODULE                    254  /**< indicates an empty slot */

#define SDAI_PBDP_MAX_UNIT_IO_DATA_SIZE                     128  /**< Maximum PROFIBUS DP specific IO data size in bytes for one IO module */
#define SDAI_PBDP_MAX_MANUFACTURER_SPEC_CFG_DATA_LENGTH     14   /**< Maximum number of manufacturer specific configuration data for one IO module. */

/** \anchor PbCfgFlags @name PROFIBUS DP IO Configuration Flags
 * Bitfield holding PROFIBUS DP specific definitions for IO Configuration Flags. These flags are set within the configuration identifier
   of the respective module and transferred via the configuration frame. */
//@{
#define SDAI_PBDP_CFG_FLAG_MODULE_CONSISTENCY               0x01u /**< The SDAI application can provide data consistency for the whole module.
                                                                       Note: This is a feature of the SDAI application. The SDAI always guarantees module consistency. */
#define SDAI_PBDP_CFG_FLAG_DATA_LENGTH_IN_WORDS             0x02u /**< The length of the IO data specified within the configuration identifier is in words.
                                                                       If not set the IO data length is in bytes. */
//@}


/** \anchor PbDataTypes @name PROFIBUS DP Data Types
 * PROFIBUS DP specific defintions for standard data types. See IEC61158 part 5 and 6 for a description of these standard data types. */
//@{
#define SDAI_PBDP_DATA_TYPE_BOOLEAN                         1u
#define SDAI_PBDP_DATA_TYPE_INTEGER8                        2u
#define SDAI_PBDP_DATA_TYPE_INTEGER16                       3u
#define SDAI_PBDP_DATA_TYPE_INTEGER32                       4u
#define SDAI_PBDP_DATA_TYPE_INTEGER64                       55u
#define SDAI_PBDP_DATA_TYPE_UNSIGNED8                       5u
#define SDAI_PBDP_DATA_TYPE_UNSIGNED16                      6u
#define SDAI_PBDP_DATA_TYPE_UNSIGNED32                      7u
#define SDAI_PBDP_DATA_TYPE_UNSIGNED64                      56u
#define SDAI_PBDP_DATA_TYPE_FLOAT32                         8u
#define SDAI_PBDP_DATA_TYPE_FLOAT64                         15u
#define SDAI_PBDP_DATA_TYPE_VISIBLE_STING                   9u
#define SDAI_PBDP_DATA_TYPE_OCTET_STRING                    10u
#define SDAI_PBDP_DATA_TYPE_DATE                            50u
#define SDAI_PBDP_DATA_TYPE_TIME_OF_DAY_WITH_IND            12u
#define SDAI_PBDP_DATA_TYPE_TIME_OF_DAY_WITHOUT_IN          52u
#define SDAI_PBDP_DATA_TYPE_TIME_DIFFERENCE_WITH_IND        53u
#define SDAI_PBDP_DATA_TYPE_TIME_DIFFERENCE_WITHOUT_IND     54u
#define SDAI_PBDP_DATA_TYPE_NETWORK_TIME                    58u
#define SDAI_PBDP_DATA_TYPE_NETWORK_TIME_DIFFERENCE         59u
#define SDAI_PBDP_DATA_TYPE_TIME_DIFFERENCE_WITHOUT_IND     54u
//@}

/*---------------------------------------------------------------------------*/

#define SDAI_PBDP_MAX_MANUFACTURER_SPEC_PRM_DATA_LENGTH     234   /**< Maximum number of manufacturer specific parameterization data. */

/** \anchor PbPrm @name PROFIBUS DP Parameterization
 * Bitfield holding PROFIBUS DP specific definitions for parameterization data */
//@{
#define SDAI_PBDP_DPV1_ENABLED            0x80    /**< Slave is conform to PROFIBUS DP-V1 extensions. DP-V1 specific parameterization and
                                                       DP-V1 diagnosis model is supported. */
#define SDAI_PBDP_DPV1_CHECK_CFG_MODE     0x40    /**< Activate reduced IO configuration check. If this bit is zero the check of configuration data
                                                       received from the DP-master is done as described in the IEC 61158. If the bit is set the check
                                                       of the configuration data might be done in a different user specific way. For example a different
                                                       plugged module might be accepted even if the received module configuration is different (see also
                                                       \ref pb_io_cfg_dynamic). */
//@}

/*---------------------------------------------------------------------------*/

/* Definitions for the acyclic communication */
#define SDAI_PBDP_MAX_NUMBER_MS2_CONNECTIONS  0x03u  /**< Number of supported MS2 connections which can be used in parallel */
#define SDAI_PBDP_COMM_REF_MS1_CONNECTION     0xFFu  /**< Will be set as communication reference if a Read/Write service request is received from a Class 1 Master, typically a PLC. */

/*---------------------------------------------------------------------------*/

/** \anchor PbControlCode @name PROFIBUS DP specific Control Codes
 * PROFIBUS DP specific definitions for Control Codes. */
//@{
#define SDAI_PBDP_CONTROL_CODE_PRM_DATA       0x0100  /**< This control indication is signaled to the application if new parameterization data
                                                           are received from the DP-master via the Set_Prm request frame. The parameters of structure #SDAI_PBDP_ADDR_DESCR
                                                           are not used if this control code is set. The structure #SDAI_PBDP_CONTROL_PRM_DATA contains the received
                                                           parameterization data. */
#define SDAI_PBDP_CONTROL_CODE_INITIATE       0x0200  /**< This control indication is signaled to the application if an Initiate request is received from the DP-master to
                                                           establish a new acyclic MS2 connection. A service-specific data block does not exist. */
#define SDAI_PBDP_CONTROL_CODE_ABORT          0x0300  /**< This control indication is signaled to the application if an acyclic MS2 connection has been aborted.
                                                           A service-specific data block does not exist. */
//@}

/*---------------------------------------------------------------------------*/

#define SDAI_PBDP_NO_MASTER_ADDRESS     0xFF  /**< Indicates that the slave has not been assigned to a master. */

/** \anchor PbSlaveStatus @name PROFIBUS DP Slave Status
 * Stack states of the PROFIBUS DP Slave cyclic state machine */
//@{
#define SDAI_PBDP_SLAVE_STATE_IDLE            0x01  /**< The slave has not yet detected a transfer rate or no bus traffic is taking place. */
#define SDAI_PBDP_SLAVE_STATE_WAIT_PRM        0x02  /**< The slave could detect a valid transfer rate but has not yet received any parameterization data from the master. */
#define SDAI_PBDP_SLAVE_STATE_WAIT_CFG        0x03  /**< Parameterizing the slave has been concluded and configuration data are awaited. */
#define SDAI_PBDP_SLAVE_STATE_DATA_EXCHANGE   0x04  /**< The slave was configured successfully and is exchanging process data. */
//@}

/** \anchor PbDiagStatus @name PROFIBUS DP Diagnostic Status
 * Bitfield containing diagnostic flags currently set within the diagnosis response telegram. The following bits will be set normally during startup
 * of the cyclic connection to indicate a parameterization or configuration error. */
//@{
#define SDAI_PBDP_DIAG_STATE_OK               0x00  /**< No diagnostic flag is set. */
#define SDAI_PBDP_DIAG_STATE_PRM_FAULT        0x01  /**< A parameterization fault has been detected. */
#define SDAI_PBDP_DIAG_STATE_CFG_FAULT        0x02  /**< A configuration fault has been detected.  */
#define SDAI_PBDP_DIAG_STATE_NOT_SUPPORTED    0x04  /**< The features requested by the DP-master are not supported. */
#define SDAI_PBDP_DIAG_STATE_FAULT_IS_ACTIVE  0x08  /**< A failure is active (device/module/channel). This bit is mapped to the EXT_DIAG flag of the diagnosis response telegram. */
//@}

/** \anchor PbBaudRate @name PROFIBUS DP Baudrates
 * PROFIBUS DP specific baudrates */
//@{
#define SDAI_PBDP_KBAUD_9_6                   0x00u
#define SDAI_PBDP_KBAUD_19_2                  0x01u
#define SDAI_PBDP_KBAUD_93_75                 0x02u
#define SDAI_PBDP_KBAUD_187_5                 0x03u
#define SDAI_PBDP_KBAUD_500                   0x04u
#define SDAI_PBDP_MBAUD_1_5                   0x06u
#define SDAI_PBDP_MBAUD_3                     0x07u
#define SDAI_PBDP_MBAUD_6                     0x08u
#define SDAI_PBDP_MBAUD_12                    0x09u
#define SDAI_PBDP_KBAUD_45_45                 0x0Bu
#define SDAI_PBDP_AUTOBAUD                    0xFFu   /**< The transfer rate is automatically detected by the protocol stack. */
#define SDAI_PBDP_NO_BUS_TRAFFIC              0xFFu   /**< There is no bus traffic or the transfer rate was not detected.*/
//@}

/** \anchor PbMasterStatus @name PROFIBUS DP Master Status
 * Current operting state of a PROFIBUS DP master */
//@{
#define SDAI_PBDP_MASTER_STATE_UNKNOWN        0x00  /**< This slave has not been parameterized by a master and thus the state of the master is unkwown. */
#define SDAI_PBDP_MASTER_STATE_CLEAR          0x01  /**< The master is in operating state CLEAR; the output data are therefore set to failsafe state. */
#define SDAI_PBDP_MASTER_STATE_OPERATE        0x02  /**< The slave could detect a valid transfer rate but has not yet received any parameterization data from the master. */
//@}

/*---------------------------------------------------------------------------*/

/** \anchor PbServiceErrorCode @name PROFIBUS DP Service Error Codes
* Definitions for PROFIBUS DP specific service error codes. */
//@{
#define SDAI_PBDP_ERROR_CODE_1_MASK       0xFF00  /**< Bitmask to specify the PROFIBUS DP Error Code 1. The possible values are specified
                                                       by the PROFIBUS specification or by the application profile. */
#define SDAI_PBDP_ERROR_CODE_2_MASK       0x00FF  /**< Bitmask to specify the PROFIBUS DP Error Code 1. The possible values are
                                                       manufacturer specific. */
//@}

/*---------------------------------------------------------------------------*/

#define SDAI_PBDP_MAX_ALARM_DATA_LENGTH          59u    /**< maximum length of alarm data for one unit */
#define SDAI_PBDP_ALARM_PRIO_NONE              0x00u    /**< The parameter AlarmPriority of structure #SDAI_ALARM_REQ can be set to this value
                                                             to signal "no further differentiation" for the alarm specifier. */

/*---------------------------------------------------------------------------*/

#define SDAI_PBDP_MAX_STATUS_DATA_LENGTH         59u    /**< maximum length of status data for one unit */
#define SDAI_PBDP_STATUS_SPECIFIER_NONE        0x00u    /**< The parameter StatusSpecifier of structure #SDAI_DIAGNOSIS_REQ can be set to this value
                                                             to signal "no further differentiation" via the status message. */
#define SDAI_PBDP_STATUS_TYPE_STATUS_MESSAGE   0x01u    /**< The status message allows to send application specific status data. It can be used instead of an diagnosis
                                                             alarm. In that case the data structure of the status message corresponds directly to the diagnosis alarm.
                                                             However, it does not require the support of the alarm mechanism by the DP-master. Only the guarantee of delivery
                                                             may be affected. */

/** \anchor PbDiagType @name PROFIBUS Diagnosis Types
 * PROFIBUS DP specific diagnosis types */
//@{
#define SDAI_PBDP_DIAG_TYPE_CHANNEL_RELATED    0x01u   /**< A channel related diagnosis shows errors related to one channel of a single modul. */
#define SDAI_PBDP_DIAG_TYPE_DEVICE_RELATED     0x02u   /**< A device related diagnosis shows errors related to the whole slave device. Only supported in DPV0 mode. */
#define SDAI_PBDP_DIAG_TYPE_DIAG_ALARM         0x03u   /**< A "Diagnosis Alarm" may be used instead of a channel related diagnosis to show
                                                            specialized informations. Only supported in DPV1 mode. */
#define SDAI_PBDP_DIAG_TYPE_STATUS             0x04u   /**< Signals error or status informations related to a module or to the whole slave device. Only supported in DPV1 mode. */
//@}

#define SDAI_PBDP_MAX_DEVICE_DIAG_LENGTH        100u   /**< maximum length of device related diagnostic data */
#define SDAI_PBDP_MAX_CHANNEL_NUMBER             63u   /**< highest channel number to be used for channel related diagnostics */

/** \anchor PbChannelProperty @name PROFIBUS Channel Properties
* Bitfield holding definitions for the channel properties:
\verbatim
 +-------------------------------------------------------------------------+
 |    7        6        5        4        3        2        1        0     |
 |    |        |        |        |        |        |        |        |     |
 |    -------------------        ----------        -------------------     |
 |          unused              Channel Type          Channel Size         |
 +-------------------------------------------------------------------------+
\endverbatim */
//@{
#define SDAI_PBDP_CHANNEL_SIZE_UNSPECIFIC          0x00u   /**< unspecific, may be used for any size */
#define SDAI_PBDP_CHANNEL_SIZE_1_BIT               0x01u   /**< channel size is one bit */
#define SDAI_PBDP_CHANNEL_SIZE_2_BIT               0x02u   /**< channel size is two bits */
#define SDAI_PBDP_CHANNEL_SIZE_4_BIT               0x03u   /**< channel size is four bits */
#define SDAI_PBDP_CHANNEL_SIZE_8_BIT               0x04u   /**< channel size is one byte */
#define SDAI_PBDP_CHANNEL_SIZE_16_BIT              0x05u   /**< channel size is one word */
#define SDAI_PBDP_CHANNEL_SIZE_32_BIT              0x06u   /**< channel size is two words */
#define SDAI_PBDP_CHANNEL_SIZE_MASK                0x07u

#define SDAI_PBDP_CHANNEL_TYPE_INPUT               0x08u   /**< input channel */
#define SDAI_PBDP_CHANNEL_TYPE_OUTPUT              0x10u   /**< output channel */
#define SDAI_PBDP_CHANNEL_TYPE_INPUT_AND_OUTPUT    0x18u   /**< input and output channel */
#define SDAI_PBDP_CHANNEL_TYPE_MASK                0x18u
//@}

/******************************************************************************
STRUCTURES
******************************************************************************/

/** \brief This structure holds PROFIBUS DP specific channel related diagnosis data. The channel related diagnosis
           informs about errors of channels within modules. The channel error type is specified by the application
           via the SDAI common diagnosis type (see also \ref DiagType). \ref pbdp_diag_alarm describes the mapping
           to the PROFIBUS DP channel error type.
  */
struct SDAI_PBDP_CHANNEL_RELATED_DIAG
{
  U8    ChannelNumber;      /**< Number of the affected channel. Valid values: 0 - #SDAI_PBDP_MAX_CHANNEL_NUMBER */
  U8    ChannelProperties;  /**< Specifies the properties of the channel. Valid values: \ref PbChannelProperty */
  U8    Alignment [2];
};

/** \brief This structure holds PROFIBUS DP specific device related diagnosis data. */
/** This diagnosis shall only be used if DPV1 mode is \b not selected by the master (see also #SDAI_PBDP_CONTROL_PRM_DATA).
    Because this diagnosis is not related to a specific module the passed unit ID of structure #SDAI_DIAGNOSIS_REQ is ignored.
    Also the parameter Type of structure #SDAI_DIAGNOSIS_REQ is ignored. Thus the application is responsible to only set
    the StatusSpecifier to #SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_DISAPPEARS when \b all device related errors are disappeared.
    The header byte for the device related diagnosis is generated automatically by the SDAI.
  */
struct SDAI_PBDP_DEVICE_RELATED_DIAG
{
  U8      Length;        /**< Length of the following device related diagnosis data. Valid values: 0 - #SDAI_PBDP_MAX_DEVICE_DIAG_LENGTH */
  U8      Alignment [3];

  U8      Data [SDAI_PBDP_MAX_DEVICE_DIAG_LENGTH];  /**< Manufacturer specific diagnostic data */
};

/** \brief This structure holds PROFIBUS DP specific diagnosis data for a Diagnostic alarm. */
/** The structure of the diagnostic alarm data is manufacturer specific and defined within the GSD file.
    If DPV1 mode is \b not selected by the master (see also #SDAI_PBDP_CONTROL_PRM_DATA) the SDAI
    generates automatically a identifier related diagnosis to signal to the DP-master that this module
    has a new diagnosis. However, in that case the passed diagnostic data are ignored because it cannot
    be transferred via the diagnostic frame.
  */
struct SDAI_PBDP_DIAG_ALARM
{
  U8      Length;        /**< Length of the following device related diagnosis data. Valid values: 0 - #SDAI_PBDP_MAX_ALARM_DATA_LENGTH */
  U8      Alignment [3];

  U8      Data [SDAI_PBDP_MAX_ALARM_DATA_LENGTH];  /**< Manufacturer specific diagnostic alarm data  */
};

/** \brief This structure holds data for PROFIBUS DP specific status messages. */
/** Usage is only possible if DPV1 mode is selected by the master (see also #SDAI_PBDP_CONTROL_PRM_DATA).
  */
struct SDAI_PBDP_STATUS_DATA
{
  U16   StatusType;                              /**< The type of the status message. Valid values: #SDAI_PBDP_STATUS_TYPE_STATUS_MESSAGE or 32 - 126 for manufacturer specific status messages. */
  U8    Length;                                  /**< Length of the following status data. Valid values: 0 - #SDAI_PBDP_MAX_STATUS_DATA_LENGTH */
  U8    Data [SDAI_PBDP_MAX_STATUS_DATA_LENGTH]; /**< Manufacturer specific status data */
};

/** \brief This structure holds PROFIBUS DP specific diagnosis data. */
struct SDAI_PBDP_DIAGNOSIS_DATA
{
  U32   DiagType;   /**< Defines the format of the following diagnosis data. Valid values: \ref PbDiagType */

  union
  {
    struct SDAI_PBDP_CHANNEL_RELATED_DIAG       ChannelDiag;
    struct SDAI_PBDP_DEVICE_RELATED_DIAG        DeviceDiag;
    struct SDAI_PBDP_DIAG_ALARM                 DiagAlarm;
    struct SDAI_PBDP_STATUS_DATA                Status;

  } UseAs;
};

/*---------------------------------------------------------------------------*/

/** \brief This structure holds PROFIBUS DP specific alarm data. */
/** Usage is only possible if DPV1 mode is selected by the master (see also #SDAI_PBDP_CONTROL_PRM_DATA).
    To send manufacturer specific alarm messages the parameter AlarmType of structure #SDAI_ALARM_REQ must
    be set to a manufacturer specific value (value range: 32 - 126).
  */
struct SDAI_PBDP_ALARM_DATA
{
  U8    Length;            /**< Length of the following alarm data. Valid values: 0 - #SDAI_PBDP_MAX_ALARM_DATA_LENGTH */
  U8    Alignment [2];

  U8    Data [SDAI_PBDP_MAX_ALARM_DATA_LENGTH];  /**< Manufacturer specific alarm data */
};

/*---------------------------------------------------------------------------*/

/** \brief PROFIBUS DP network specific parameter */
struct SDAI_PBDP_IDENT_DATA
{
  U16     Flags;                    /**< PROFIBUS specific interface configuration flags. Valid values: \ref PbIdentFlags */
  U16     IdentNumber;              /**< PNO identification number. The ident number must correspond with the GSD file. */
  U8      SlaveAddress;             /**< Station address of the DP slave. Valid values: 0 - 125 and #SDAI_PBDP_DEFAULT_SLAVE_ADDRESS  */
  U8      Baudrate;                 /**< If set to #SDAI_PBDP_AUTOBAUD the transfer rate is detected automatically by the protocol stack.
                                      * For fast startup applications alternatively a fixed value for the transfer rate can be set by the
                                      * application. In that case if the master uses a different baudrate bus communication is not possible
                                      * and #SDAI_PBDP_NO_BUS_TRAFFIC is signalled via function sdai_get_state(). Valid values: \ref PbBaudRate */
  U8      Alignment [2];
};

/** \brief PROFIBUS DP specific parameter SW version. */
/* This structure specifies the software version of a device or module according to the PROFIBUS Profile Guideline for I&M functions.
   Example: V1.0.0 => Type = 'V', x = 1, y = 0, z = 0. See Profile Guideline for more details.
 */
struct SDAI_PBDP_SW_REVISION
{
  char    Type;   /**< Type Recognition, e.g. 'P' = Prototype, 'V' = officially released */
  U8      x;      /**< Functional enhancement */
  U8      y;      /**< Bug fix */
  U8      z;      /**< Internal change (no impact on function) */
};

/** \brief Powerlink device specific parameter. */
/* See also \ref epl_init_data. */
struct SDAI_PBDP_DEVICE_DATA
{
  struct SDAI_PBDP_SW_REVISION            SoftwareRevision;         /**< The software version of the device */
  U16                                     HardwareRevision;         /**< The hardware version of the device */
  U16                                     RevisionCounter;          /**< Indicates a change of the hardware or of device parameters. */

  U16                                     ProfileIdentNumber;       /**< This parameter identifies a profile definition uniquely. All devices
                                                                      * using the same profile definition have to use the same Profile Ident Number.
                                                                      * The value 0 indicates that no profile is supported. */
  U16                                     ProfileFeatureSupported;  /**< DP-master and DP-slave inform each other about the supported service
                                                                      * functionality regarding the used profile definition. This is done during
                                                                      * establishment of a acyclic MS2 connection. The meaning of the defined bits is
                                                                      * profile or vendor specific. */

  struct SDAI_PBDP_DEVICE_RELATED_DIAG    DeviceDiag;               /**< Initial device-related diagnosis block. This block will only be used if the DP-master
                                                                      * did \b not select DPV1 mode via the Set-Prm frame (see also #SDAI_PBDP_CONTROL_PRM_DATA).
                                                                      * The length parameter of the device-related diagnosis block can be set to 0 if DPV0 mode
                                                                      * shall not be supported. */
};

/*---------------------------------------------------------------------------*/

/** \brief PROFIBUS DP specific address description */
struct SDAI_PBDP_ADDR_DESCR
{
  U8    CommRef;                        /**< The communication reference represents a logical communication channel to the service partner.
                                          *  Valid values: 0 - #SDAI_PBDP_MAX_NUMBER_MS2_CONNECTIONS for acyclic MS2 connections or
                                          *  #SDAI_PBDP_COMM_REF_MS1_CONNECTION for the acyclic MS1 connection. */
  U8    Slot;                           /**< Addresses the slot of the data block to be read */
  U8    Index;                          /**< Addresses the the data block to be read */

  U8    Alignment;
};

/*---------------------------------------------------------------------------*/

/** \brief PROFIBUS DP specific configuration data.

    This structure holds PROFIBUS DP specific configuration data for one IO module. Manufacturer specific configuration data
    can be specified which are used by the special identifier format and are transferred via the configuration frame. Here,
    DPV1 recommends to specify the data types for the process variables mapped to the IO module. Simple data types are coded
    in a byte which contains the code of the data type. Sequences of data types are coded as a list of simple data types.
    The sum of the lengths of the data types has to correspond to the input- or output data length. Data types with variable length
    (visible string, octet string, time of day, time difference) cannot be handled in a sequence, but as single element.

     Example: Data = { 0x08, 0x05, 0x08, 0x05 }; \n
     Variable 1: Float32 \n
     Variable 2: Unsigned8 \n
     Variable 3: Float32 \n
     Variable 4: Unsigned8

    The standard data types defined by DPV1 can be found here: \ref PbDataTypes
 */
struct SDAI_PBDP_CFG_DATA
{
  U8    InputFlags;                                              /**< PROFIBUS specific configuration flags for the input data. Valid values: \ref PbCfgFlags */
  U8    OutputFlags;                                             /**< PROFIBUS specific configuration flags for the output data. Valid values: \ref PbCfgFlags */

  U8    Alignment [3];

  U8    Length;                                                  /**< Length of the following data. Valid values: 0 - #SDAI_PBDP_MAX_MANUFACTURER_SPEC_CFG_DATA_LENGTH */
  U8    Data [SDAI_PBDP_MAX_MANUFACTURER_SPEC_CFG_DATA_LENGTH];  /**< Manufacturer specific configuration data */
};

/*---------------------------------------------------------------------------*/

/** \brief Holds the informations about received parameterization data. */
/** The standard as well as the DPV1 specific parameterization data are already handled by the SDAI.
    This data block is attached directly to the end of the structure #SDAI_CONTROL_IND, if the ControlCode
    is set to #SDAI_PBDP_CONTROL_CODE_PRM_DATA.
 */
struct SDAI_PBDP_CONTROL_PRM_DATA
{
  U32   Dpv1Status;    /**< Bitfield holding DPV1 specific parameterization data. Valid values: \ref PbPrm */

  U8    Length;        /**< Length of the following manufacturer specific parameterization data. Valid values: 0 - #SDAI_PBDP_MAX_MANUFACTURER_SPEC_PRM_DATA_LENGTH */
  U8    Alignment [3];

  U8    Data [SDAI_PBDP_MAX_MANUFACTURER_SPEC_PRM_DATA_LENGTH];  /**< Manufacturer specific parameterization data */
};

/*---------------------------------------------------------------------------*/

/** \brief This structure holds PROFIBUS DP specific additional status informations. */
struct SDAI_PBDP_ADDSTATUS_DATA
{
  U8    SlaveStatus;        /**< Actual operating status of the slave. Valid values: \ref PbSlaveStatus */
  U8    DiagStatus;         /**< Diagnositic flags indicating prm/cfg errors. Valid values: \ref PbDiagStatus */

  U8    MasterStatus;       /**< Actual operating state of the master which has parameterized this DP-Slave. Valid values: \ref PbMasterStatus */
  U8    MasterAddress;      /**< Address of the DP-Master which has parameterized this DP-Slave. Valid values: 0 - 125 and #SDAI_PBDP_NO_MASTER_ADDRESS */

  U8    DetectedBaudRate;   /**< Detected transfer rate. Valid values: \ref PbBaudRate */

  U8    Alignment [2];
};

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

#endif /* __SDAI_PBDP_H__ */

/*****************************************************************************/
