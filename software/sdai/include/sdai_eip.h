/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

/**
 *
 * @page eip EtherNet/IP
 *
 * \tableofcontents
 *
 * @section eip_overview Overview
 * The Softing EtherNet/IP Adapter Protocol Software realizes a small and portable
 * protocol stack for building EtherNet/IP applications. The Simple Device Application
 * Interface (SDAI) provides the services to communicate with the EtherNet/IP Adapter
 * Protocol Software. This application interface allows you to implement most of the
 * device profiles currently specified by the ODVA or to realize vendor-specific devices.
 *
 * EtherNet/IP is a widely supported, high-level industrial application layer protocol
 * for industrial automation applications. Built on the standard TCP/IP protocol suite,
 * EtherNet/IP uses all the traditional Ethernet hardware and software to define an
 * application layer protocol that structures the task of configuring, accessing and
 * controlling industrial automation devices.
 *
 * @subsection eip_features Features
 * - Multiple Ethernet interfaces supported, e.g. for devices with embedded switch technology (to support linear or ring topology)
 * - Device Level Ring (DLR) functionality supported (announce-based ring node)
 * - Quality of Service (QoS) supported
 * - Quick Connect supported
 * - Support for up to 10 concurrent I/O Connections
 * - Support for up to 10 concurrent Encapsulation Sessions
 * - Support for up to 2 concurrent Explicit Messaging Connections for each Encapsulation Session
 * - Unconnected Explicit Messaging supported
 * - UDP available for encapsulation protocol commands ListServices, ListTargets and ListIdentity
 * - Standard generic device support for CIP and Encapsulated layers
 * - Integration of application objects possible via SDAI
 * - Support for multiple Input/Output assemblies (customizable)
 * - PCCC emulation to communicate with legacy Allen-Bradley PLCs (optional)
 * - IPv4 Address Conflict Detection for EtherNet/IP Devices
 * - Demo Application with a dedicated EDS file aimed at quickly getting you started with
 *   implementing your own EtherNet/IP applications
 * - Support for Module Status LED, Network Status LED and IO Status LED
 *
 * @subsection eip_objects Supported Objects
 *
 * <TABLE><TR>
 * <TH>Object</TH><TH>Class ID</TH><TH>Description</TH>
 * </TR><TR>
 * <TD>Identity</TD><TD>(0x01)</TD><TD>Manages the identity data that identifies the device on an EtherNet/IP network</TD>
 * </TR><TR>
 * <TD>Message Router</TD><TD>(0x02)</TD><TD>Manages Explicit Message Traffic and routes messages to the correct destination</TD>
 * </TR><TR>
 * <TD>Assembly</TD><TD>(0x04)</TD><TD>Combines attribute data from one or more objects to create a data assembly accessible over the network</TD>
 * </TR><TR>
 * <TD>Connection Manager</TD><TD>(0x06)</TD><TD>Manages the Explicit And I/O Connections</TD>
 * </TR><TR>
 * <TD>Device Level Ring</TD><TD>(0x47)</TD><TD>Provides the configuration and status information interface for the DLR protocol</TD>
 * </TR><TR>
 * <TD>QoS</TD><TD>(0x48)</TD><TD>Configuration of certain QoS-related behaviors in EtherNet/IP devices</TD>
 * </TR><TR>
 * <TD>TCP/IP Interface</TD><TD>(0xF5)</TD><TD>Manages the connection to the TCP/IP protocol stack</TD>
 * </TR><TR>
 * <TD>Ethernet Link</TD><TD>(0xF6)</TD><TD>Manages communications with the physical Ethernet connection</TD>
 * </TR></TABLE>
 *
 * @section eip_mapping Mapping EtherNet/IP to SDAI
 * In the following chapters the mapping of the EtherNet/IP Adapter Protocol Software to the SDAI is described.
 *
 * @subsection eip_init_data EtherNet/IP Initialization
 * The structure #SDAI_INIT contains identification data, device specific data and a set of application specific
 * callback functions. The identification data are described by the structure #SDAI_IDENT_DATA. The SDAI common
 * parameter DevName of this structure is not used for EtherNet/IP. The device specific data are described by the
 * structure #SDAI_DEVICE_DATA. These parameters are mapped to the attributes of the Identity Object (Class ID: 0x01)
 * in the following way:
 *
 * <TABLE><TR>
 * <TH>SDAI</TH><TH>EtherNet/IP</TH><TH>Description</TH>
 * </TR><TR>
 * <TD>VendorID</TD><TD>Vendor ID (Attr ID: 0x01)</TD><TD>Only the Low-Word (Bits 0-15) is used</TD>
 * </TR><TR>
 * <TD>Type</TD><TD>Device Type (Attr ID: 0x02)</TD><TD>Only the Low-Word (Bits 0-15) is used</TD>
 * </TR><TR>
 * <TD>ProductCode</TD><TD>Product Code (Attr ID: 0x03)</TD><TD></TD>
 * </TR><TR>
 * <TD>DeviceVersion</TD><TD>Revision (Attr ID: 0x04)</TD><TD>The High-Byte of DeviceVersion is the Major Revision.<br>
 *                                                            The Low-Byte of DeviceVersion is the Minor Revision.<br>
 *                                                            Example DeviceVersion = 0x0200 means 2.00</TD>
 * </TR><TR>
 * <TD>SerialNumber</TD><TD>Serial Number (Attr ID: 0x06)</TD><TD></TD>
 * </TR><TR>
 * <TD>ProductName</TD><TD>Product Name (Attr ID: 0x07)</TD><TD></TD>
 * </TR><TR>
 * <TD>OrderId</TD><TD>not used</TD><TD></TD>
 * </TR></TABLE>
 *
 * The structure #SDAI_CALLBACKS is used to register all supported application specific callback functions.
 * The following table shows which SDAI callback functions are used by EtherNet/IP. If not supported by the
 * application the user shall set the respective function pointer to NULL.
 *
 * <TABLE><TR>
 * <TH>SDAI Callback</TH><TH>EtherNet/IP</TH>
 * </TR><TR>
 * <TD>IdentDataCbk()</TD><TD>Called when the network parameter (e.g. IP address) are changed</TD>
 * </TR><TR>
 * <TD>ExceptionCbk()</TD><TD>Called when a fatal error occurred</TD>
 * </TR><TR>
 * <TD>DataCbk()</TD><TD>Called when the output data or status of a unit has been changed</TD>
 * </TR><TR>
 * <TD>AlarmAckCbk()</TD><TD>Not used (set function pointer always to NULL)</TD>
 * </TR><TR>
 * <TD>WriteReqCbk()</TD><TD>Called when a explicit messaging service is received</TD>
 * </TR><TR>
 * <TD>ReadReqCbk()</TD><TD>Called when a explicit messaging service is received</TD>
 * </TR><TR>
 * <TD>ControlReqCbk()</TD><TD>Called when a explicit messaging service is received</TD>
 * </TR><TR>
 * <TD>SyncSignalCbk()</TD><TD>Not used (set function pointer always to NULL)</TD>
 * </TR></TABLE>
 *
 * @subsection eip_plug_unit EtherNet/IP plugging of units
 * In case of EtherNet/IP a SDAI unit is equivalent to an assembly object. A assembly object contains data attributes
 * of one or more application objects which will be transmitted cyclically as a single data unit. An assembly object
 * is identified with a unique instance identifier. This instance identifier has to be specified by the user
 * within the structure #SDAI_EIP_CFG_DATA. The instance identifier of the respective assembly object must match
 * to the corresponding assembly object in the EtherNet/IP EDS file. The following figure shows an example for
 * the mapping of SDAI I/O Units to EtherNet/IP assembly objects.
 *
 * @image html ethernetip_data_mapping.jpg
 *
 * By calling sdai_plug_unit() a Unit ID is automatically generated. This Unit ID consists of the EtherNet/IP
 * instance identifier and the SDAI unit index. The high word of the Unit ID is the SDAI unit index. The low word
 * of the Unit ID is the instance Identifier of the assembly or configuration assembly object. See also \ref EipUnitId.
 *
 * The instance identifier 0xC5 - 0xC7 are reserved and can not be used for application-defined assembly objects.
 * If the user tries to plug a unit with a instance identifier value of 0xC5 - 0xC7 this request is rejected.
 *
 * - The instance ID 0xC5 is used for Input-Only connections to realize a heartbeat for the output direction.
 *   For Input-Only connections the instance ID of the consuming I/O Path within EDS file must be set to 0xC5.
 *
 * - The instance ID 0xC6 is used for Listen-Only connections to realize a heartbeat for the output direction.
 *   For Listen-Only connections the instance ID of the consuming I/O Path within EDS file must be set to 0xC6.
 *
 * - The instance ID 0xC7 is used for Output-Only connections to realize a heartbeat for the input direction.
 *   For Output-Only connections the instance ID of the producing I/O Path within EDS file must be set to 0xC7.
 *
 * @subsection eip_io_cfg_dynamic EtherNet/IP Dynamic IO Configuration
 *
 * Dynamic IO configuration is not supported by EtherNet/IP.
 *
 * @subsection eip_acyclic EtherNet/IP Acyclic Communication
 * Acyclic communication is realized by EtherNet/IP with explicit messages. Explicit messages contain addressing
 * and service information that directs the receiving device to perform a certain service (action) on a specific
 * part (e.g. an attribute) of a device. The EtherNet/IP addressing information is described by the structure
 * #SDAI_EIP_ADDR_DESCR. The Explicit messaging service codes are defined by the CIP and EtherNet/IP
 * specification. There exist CIP common, object class specific and vendor specific service codes. The following
 * table shows the mapping of the CIP common services to SDAI callback functions:
 *
 * <TABLE><TR>
 * <TH>Service Code</TH><TH>Service Name</TH><TH>SDAI Callback function</TH>
 * </TR><TR>
 * <TD>0x00</TD><TD>Reserved</TD><TD>---</TD>
 * </TR><TR>
 * <TD>0x01</TD><TD>Get_Attributes_All</TD><TD>ReadReqCbk()</TD>
 * </TR><TR>
 * <TD>0x02</TD><TD>Set_Attributes_All</TD><TD>WriteReqCbk()</TD>
 * </TR><TR>
 * <TD>0x03</TD><TD>Get_Attributes_List</TD><TD>ControlReqCbk()</TD>
 * </TR><TR>
 * <TD>0x04</TD><TD>Set_Attributes_List</TD><TD>ControlReqCbk()</TD>
 * </TR><TR>
 * <TD>0x05</TD><TD>Reset</TD><TD>ControlReqCbk()</TD>
 * </TR><TR>
 * <TD>0x06</TD><TD>Start</TD><TD>ControlReqCbk()</TD>
 * </TR><TR>
 * <TD>0x07</TD><TD>Stop</TD><TD>ControlReqCbk()</TD>
 * </TR><TR>
 * <TD>0x08</TD><TD>Create</TD><TD>ControlReqCbk()</TD>
 * </TR><TR>
 * <TD>0x09</TD><TD>Delete</TD><TD>ControlReqCbk()</TD>
 * </TR><TR>
 * <TD>0x0A</TD><TD>Multiple_Service_Packet</TD><TD>ControlReqCbk()</TD>
 * </TR><TR>
 * <TD>0x0B-0x0C</TD><TD>Reserved</TD><TD>---</TD>
 * </TR><TR>
 * <TD>0x0D</TD><TD>Apply_Attributes</TD><TD>ControlReqCbk()</TD>
 * </TR><TR>
 * <TD>0x0E</TD><TD>Get_Attribute_Single</TD><TD>ReadReqCbk()</TD>
 * </TR><TR>
 * <TD>0x0F</TD><TD>Reserved</TD><TD>---</TD>
 * </TR><TR>
 * <TD>0x10</TD><TD>Set_Attribute_Single</TD><TD>WriteReqCbk()</TD>
 * </TR><TR>
 * <TD>0x11</TD><TD>Find_Next_Object_Instance</TD><TD>ControlReqCbk()</TD>
 * </TR><TR>
 * <TD>0x12-0x13</TD><TD>Reserved</TD><TD>---</TD>
 * </TR><TR>
 * <TD>0x15</TD><TD>Restore</TD><TD>ControlReqCbk()</TD>
 * </TR><TR>
 * <TD>0x16</TD><TD>Save</TD><TD>ControlReqCbk()</TD>
 * </TR><TR>
 * <TD>0x17</TD><TD>No Operation (NOP)</TD><TD>ControlReqCbk()</TD>
 * </TR><TR>
 * <TD>0x18</TD><TD>Get_Member</TD><TD>ReadReqCbk()</TD>
 * </TR><TR>
 * <TD>0x19</TD><TD>Set_Member</TD><TD>WriteReqCbk()</TD>
 * </TR><TR>
 * <TD>0x1A</TD><TD>Insert_Member</TD><TD>ControlReqCbk()</TD>
 * </TR><TR>
 * <TD>0x1B</TD><TD>Remove_Member</TD><TD>ControlReqCbk()</TD>
 * </TR><TR>
 * <TD>0x1C</TD><TD>GroupSync</TD><TD>ControlReqCbk()</TD>
 * </TR><TR>
 * <TD>0x1D-0x31</TD><TD>Reserved</TD><TD>---</TD>
 * </TR></TABLE>
 * All other service codes are mapped to the ControlReqCbk() of the SDAI.
 *
 * @subsection eip_control_code EtherNet/IP Control Codes
 * If a control indication is signaled to the application via the ControlReqCbk() the parameter
 * ControlCode of the structure #SDAI_CONTROL_IND specifies the action which shall be performed.
 * The SDAI common control code #SDAI_CONTROL_RESET_TO_DEFAULTS is currently not supported for
 * EtherNet/IP. The control code #SDAI_EIP_CONTROL_EM_SERVICE_REQ is used to send a received
 * explicit messaging service request to the application (see also \ref eip_acyclic). Control indications
 * are also used to indicate the network and I/O connection status. If supported by the device
 * these status indications may be used to set EtherNet/IP specific LEDs. Therefore it is necessary
 * that the device supports two bi-color LEDs. The Network LED is used to indicate the Ethernet Link
 * status and to signal basic connection status informations. The following control codes are used
 * for the network LED:
 * - #SDAI_EIP_CONTROL_NETWORK_LED_OFF
 * - #SDAI_EIP_CONTROL_NETWORK_LED_FLASH_GREEN
 * - #SDAI_EIP_CONTROL_NETWORK_LED_STEADY_GREEN
 * - #SDAI_EIP_CONTROL_NETWORK_LED_FLASH_RED
 * - #SDAI_EIP_CONTROL_NETWORK_LED_STEADY_RED
 *
 * The I/O LED is used to indicate status informations for I/O connections. The following control
 * codes are used for the I/O LED:
 * - #SDAI_EIP_CONTROL_IO_LED_FLASH_GREEN
 * - #SDAI_EIP_CONTROL_IO_LED_STEADY_GREEN
 *
 * The control code #SDAI_EIP_CONTROL_LED_TEST is optionally used to test the functionality of all supported LEDs.
 * This action is only indicated once during system initialization. The following test sequence is recommended:
 *
 * -# Turn first indicator Green, all other indicators off
 * -# Leave first indicator on Green for approximately 0.25 second
 * -# Turn first indicator on Red for approximately 0.25 second
 * -# Turn first indicator off
 * -# Turn second indicator on Green for approx. 0.25 second
 * -# Turn second indicator on Red for approx. 0.25 second
 * -# Turn second indicator off
 *
 * @subsection eip_errors EtherNet/IP Service Error Codes
 * The following table shows how the SDAI common service error codes (see also \ref ServiceErrorCode) are mapped
 * to EtherNet/IP specific service error codes (CIP General Status Code). This error mapping is needed for explicit
 * messaging responses (Read, Write and Control response).
 *
 * <TABLE><TR>
 * <TH>SDAI Service Error Code</TH><TH>CIP General Status Code</TH><TH>Status Name</TH>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_SUCCESS</TD><TD>0x00</TD><TD>Success</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_OBJECT_NOT_EXISTING</TD><TD>0x05</TD><TD>Path destination unknown</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_INVALID_SIZE</TD><TD>0x20</TD><TD>Invalid parameter</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_STATE_CONFLICT</TD><TD>0x0C</TD><TD>Object State Conflict</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_APPLICATION</TD><TD>0x1F</TD><TD>Vendor specific error</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_RESOURCE</TD><TD>0x02</TD><TD>Resource unavailable</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_NOT_SUPPORTED</TD><TD>0x08</TD><TD>Service not supported</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_ACCESS_DENIED</TD><TD>0x0E</TD><TD>Attribute not settable</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_INVALID_PARAMETER</TD><TD>0x20</TD><TD>Invalid parameter</TD>
 * </TR></TABLE>
 *
 * The SDAI common service error codes can be used for all protocols. However, the EtherNet/IP protocol conformance test may require
 * specific error codes for wrong accesses to application objects which are not completely covered by the SDAI common service error codes.
 * For that purpose the application can directly specify the CIP general status code within the field #SDAI_EIP_GENERAL_STATUS_CODE_MASK of
 * the parameter ErrorCode. Also the application can specify the CIP extended status code within the field #SDAI_EIP_EXTENDED_STATUS_CODE_MASK
 * of the parameter ErrorCode. The SDAI common service error code is used if the field #SDAI_EIP_GENERAL_STATUS_CODE_MASK of the parameter
 * ErrorCode is set to 0.
 *
 * @subsection eip_diag_alarm EtherNet/IP Diagnosis and Alarms
 * These features are not not supported by EtherNet/IP. Thus the functions sdai_alarm_request(), sdai_diagnosis_request()
 * and the AlarmAckCbk() are not supported.
 *
 */

#ifndef __SDAI_EIP_H__
#define __SDAI_EIP_H__

/******************************************************************************
DEFINES
******************************************************************************/

/** \anchor EipUnitId @name SDAI Unit ID macros for EtherNet/IP
* Format of the SDAI Unit ID for EtherNet/IP:
\verbatim
    ------------------------------------------------------
    |     SDAI Unit Index     | EIP Assembly Instance ID |
    ------------------------------------------------------
    32      High Word         16        Low Word         0
\endverbatim
*/
//@{
#define _SDAI_EIP_INDEX_AND_INSTANCE_TO_ID(Index, Inst)  ((U32) ((((U32)(Index)) << 16) | (Inst)))     /**< Converts the EIP instance id and the SDAI Unit Index to the SDAI Id */
#define _SDAI_EIP_ID_TO_INDEX(Type, Id)                   ((Type) ((Id >> 16) & 0x0000FFFFuL))         /**< Converts the SDAI Id to the SDAI Unit Index */
#define _SDAI_EIP_ID_TO_INSTANCE(Type, Id)                ((Type) ((Id) & 0x0000FFFFuL))               /**< Converts the SDAI Id to the EIP instance id */
//@}

/** \anchor EipUnitType @name EtherNet/IP Unit Types
* EtherNet/IP specific unit types */
//@{
#define SDAI_EIP_UNIT_TYPE_CONFIGURATION            254   /**< indicates a configuration unit */
//@}

/** \anchor EipFlags @name EtherNet/IP specific interface configuration
* Bitfield holding definitions for EtherNet/IP specific interface configuration:
\verbatim
 +-------------------------------------------------------------------------+
 |    7        6        5        4        3        2        1        0     |
 |    |        |        |        |        |        |        |        |     |
 |    -------------------      Enable  Support   Select     ----------     |
 |          unused              QC       QC       ACD        Startup       |
 |                                                         Configuration   |
 +-------------------------------------------------------------------------+
\endverbatim
*/
//@{
/* Startup Configuration  */
#define SDAI_EIP_USE_CONFIG_FROM_NV_STORAGE   0x00  /**< The device shall use the interface configuration values previously stored in
                                                         non-volatile memory. If the user sets this flag when calling sdai_init() a
                                                         valid unique IP address must be passed to the SDAI. */
#define SDAI_EIP_USE_CONFIG_FROM_HW_SETTINGS  0x01  /**< The device shall use the interface configuration values obtained via hw settings
                                                         (e.g. DIP switches). If the user sets this flag when calling sdai_init() a
                                                         valid unique IP address must be passed to the SDAI. */
#define SDAI_EIP_GET_CONFIG_FROM_DHCP         0x02  /**< The device shall obtain its interface configuration values via DHCP upon start-up. */
#define SDAI_EIP_STARTUP_CONFIG_MASK          0x03  /**< Bitmask to specify how the device shall obtain its initial startup configuration. */

/* Select ACD */
#define SDAI_EIP_ENABLE_ACD                   0x00  /**< Enables the IPv4 address conflict detection. */
#define SDAI_EIP_DISABLE_ACD                  0x04  /**< Disables the IPv4 address conflict detection. */

/* Quick Connect */
#define SDAI_EIP_QC_NOT_SUPPORTED             0x00  /**< Indicates that this device did not support the Quick Connect feature.
                                                         The Quick Connect attribute (attr. ID 12) of the TCP/IP object is not supported.
                                                         The settings indicated by the Enable QC flag will be ignored. */
#define SDAI_EIP_QC_SUPPORTED                 0x08  /**< Indicates that this device supports the Quick Connect feature.
                                                         The Quick Connect attribute (attr. ID 12) of the TCP/IP object is supported.
                                                         The Enable QC flag is checked to enable/disable the Quick Connect feature. */
#define SDAI_EIP_ENABLE_QC                    0x10  /**< Enables the Quick Connect feature. */
#define SDAI_EIP_DISABLE_QC                   0x00  /**< Disables the Quick Connect feature. */
//@}

/** \anchor EipControlCode @name EtherNet/IP IO Control Codes
 * EtherNet/IP specific definitions for Control Codes. */
//@{
#define SDAI_EIP_CONTROL_EM_SERVICE_REQ           0x0100  /**< A explicit messaging request is received. Depending on the
                                                               received service request additional service data may be available. */
#define SDAI_EIP_CONTROL_LED_TEST                 0x0200  /**< Test the functionality of all supported LEDs during initialization.
                                                               A recommended test procedure is described by \ref eip_control_code. */
#define SDAI_EIP_CONTROL_NETWORK_LED_OFF          0x0300  /**< Switch network LED off. This indicates the Ethernet Link is not active */
#define SDAI_EIP_CONTROL_NETWORK_LED_FLASH_GREEN  0x0400  /**< Set the network LED to green and flash the LED with a frequency of 1Hz.
                                                               This indicates the Ethernet Link is active and no connections are established. */
#define SDAI_EIP_CONTROL_NETWORK_LED_STEADY_GREEN 0x0500  /**< Set the network LED to steady green. This indicates the Ethernet Link
                                                               is active and one or more connections are established. */
#define SDAI_EIP_CONTROL_NETWORK_LED_FLASH_RED    0x0600  /**< Set the network LED to red and flash the LED with a frequency of 1Hz.
                                                               This indicates the Ethernet Link is active and any connection has
                                                               been timed out. */
#define SDAI_EIP_CONTROL_NETWORK_LED_STEADY_RED   0x0700  /**< Set the network LED to steady red. This indicates the Ethernet Link is
                                                               active and a IP address conflict has been detected. */
#define SDAI_EIP_CONTROL_IO_LED_FLASH_GREEN       0x0800  /**< Set the I/O LED to green and flash the LED with a frequency of 1Hz.
                                                               This indicates that any established I/O connection is in idle mode. */
#define SDAI_EIP_CONTROL_IO_LED_STEADY_GREEN      0x0900  /**< Set the I/O LED to steady green. This indicates that all established
                                                               I/O connections are in run mode. */
//@}

/** \anchor AcdStatus @name ACD Status
 * ACD definitions */
//@{
#define SDAI_EIP_ACD_NO_CONFLICT            0x00u   /**< No address conflict detected */
#define SDAI_EIP_ACD_INITIAL_PROBING        0x01u   /**< Conflict in state INITIAL_PROBING detected */
#define SDAI_EIP_ACD_ONGOING_DETECTION      0x02u   /**< Conflict in state ONGOING_DETECTION detected */
#define SDAI_EIP_ACD_SEMI_ACTIVE_PROBING    0x03u   /**< Conflict in state SEMI_ACTIVE_PROBING detected */
//@}

/** \anchor EipServiceErrorCode @name EtherNet/IP Service Error Codes
* Definitions for EtherNet/IP specific service error codes. */
//@{
#define SDAI_EIP_GENERAL_STATUS_CODE_MASK        0xFF00  /**< Bitmask to specify the CIP general status code. The possible values are specified
                                                              by the CIP specification, see Volume 1, appendix B. If this field is set to 0 the
                                                              common service error code is used. */
#define SDAI_EIP_EXTENDED_STATUS_CODE_MASK       0x00FF  /**< Bitmask to specify the CIP extended status code. The possible values are specified
                                                              by the CIP specification. Only used if the general status field is set to values > 0. */
//@}

/* EtherNet/IP specific address description */
#define SDAI_EIP_NO_CLASS_ID      0xFFFF  /**< class ID not existing or invalid */
#define SDAI_EIP_NO_INST_ID       0xFFFF  /**< instance ID not existing or invalid */
#define SDAI_EIP_NO_ATTR_ID       0xFFFF  /**< attribute ID not existing or invalid */
#define SDAI_EIP_NO_SERVICE_CODE  0xFF    /**< service code not existing or invalid */

/** \anchor InstanceIdRange @name Instance ID Ranges
 * EtherNet/IP specific limits for the Assembly Instance ID */
//@{
/* EtherNet/IP specific limits for the configuration data. */
#define SDAI_EIP_MIN_INST_ID   0x01   /**< minimum instance ID value for EtherNet/IP assembly objects */
#define SDAI_EIP_MAX_INST_ID   0xFF   /**< maximum instance ID value for EtherNet/IP assembly objects */
//@}

/** \anchor NetworkTopology @name Network Topology
 * Network Topology definitions */
//@{
#define SDAI_EIP_NETWORK_TOPOLOGY_LINEAR          0x00u   /**< linear topology mode */
#define SDAI_EIP_NETWORK_TOPOLOGY_RING            0x01u   /**< ring topology mode */
//@}

/** \anchor NetworkStatus @name Network Status
 * Network Status definitions */
//@{
#define SDAI_EIP_NETWORK_STATUS_NORMAL            0x00    /**< Normal operation in both Ring and Linear Network Topology modes. */
#define SDAI_EIP_NETWORK_STATUS_RING_FAULT        0x01    /**< Ring Fault. A ring fault has been detected. Valid only when Network Topology is Ring. */
#define SDAI_EIP_NETWORK_STATUS_UNEXPECTED_LOOP   0x02    /**< Unexpected Loop Detected. A loop has been detected in the network. Valid only when the
                                                               Network Topology is Linear. */
//@}

/** \anchor CurrentAcdStatus @name Current ACD Status
 * ACD definitions */
//@{
#define SDAI_EIP_ACD_NO_ADDRESS_CONFLICT_DETECTED   0x00u   /**< No address conflict active. Configured ip address is still in use. */
#define SDAI_EIP_ACD_ADDRESS_CONFLICT_DETECTED      0x01u   /**< Address conflict active. Configured ip address is either still in use
                                                                 (defense successful) or given up (defense failed). This is indicated
                                                                 by the flag #SDAI_EIP_ACD_ADDRESS_GIVEN_UP. */
#define SDAI_EIP_ACD_ADDRESS_IN_USE                 0x00u   /**< The configured ip address is still in use. */
#define SDAI_EIP_ACD_ADDRESS_GIVEN_UP               0x02u   /**< Device has given up its ip address due to an address conflict.  */
//@}

/******************************************************************************
STRUCTURES
******************************************************************************/

/** \brief EtherNet/IP network specific parameter */
struct SDAI_EIP_IDENT_DATA
{
  U8                            Flags;                       /**< EtherNet/IP specific interface configuration flags. Valid values: \ref EipFlags */

  U8                            Address [4];                 /**< The IP-Address of the Device. The user may set #SDAI_IP_UNUSED if not available. */
  U8                            Netmask [4];                 /**< The Netmask of the Device. The user may set #SDAI_NETMASK_UNUSED if not available. */
  U8                            Gateway [4];                 /**< The Gateway of the Device. The user may set #SDAI_GATEWAY_UNUSED if not available. */

  U8                            MacAddressDevice [6];        /**< The MAC address of the device */

  U8                            Alignment [2];

};

/*---------------------------------------------------------------------------*/

/** \brief This structure holds informations related to the last address conflict detected.

           This information is made available to the application via the function sdai_get_state(). The specification demands to store
           this information in a non-volatile memory. Thus it can be read out after a power cycle via attribute LastConflictDetected of
           the TCP/IP Interface object. This information must be passed at startup by the user via the function sdai_init() to the SDAI.
           For the first startup of the stack the structure content shall be set to 0. */
struct SDAI_EIP_ACD_LAST_CONFLICT
{
  U8    ConflictStatus;     /**< State of ACD state machine when the last conflict was detected. Valid values: \ref AcdStatus */
  U8    RemoteMacAddr [6];  /**< MAC address of remote node from the ARP PDU in which a conflict was detected. */
  U8    Alignment;
};

/** \brief EtherNet/IP device specific parameter */
struct SDAI_EIP_DEVICE_DATA
{
  U16                                 DeviceVersion;     /**< The version of the device */
  U8                                  Alignment [2];
};

/*---------------------------------------------------------------------------*/

/** \brief EtherNet/IP specific address description */
struct SDAI_EIP_ADDR_DESCR
{
  U16   ClassId;        /**< Identifies the object class accessible from the network. The ClassId shall be ignored if set to #SDAI_EIP_NO_CLASS_ID. */
  U16   InstanceId;     /**< Identifies an object instance among all instances of the same class. The InstanceId shall be ignored if set to #SDAI_EIP_NO_INST_ID. */
  U16   AttributeId;    /**< Identifies the class or instance attribute. The AttributeId shall be ignored if set to #SDAI_EIP_NO_ATTR_ID. */

  /** Identifies the Service Code of the explicit messaging service. The ServiceCode shall be ignored if set to #SDAI_EIP_NO_SERVICE_CODE.
      See also \ref eip_acyclic for a description of the EtherNet/IP service codes and the mapping to the SDAI callback functions. */
  U8    ServiceCode;

  U8    Alignment;
};

/*---------------------------------------------------------------------------*/

/** \brief EtherNet/IP specific configuration data. */
/** See also \ref eip_plug_unit. */
struct SDAI_EIP_CFG_DATA
{
  U16   InstanceId; /**< Instance ID of the EtherNet/IP assembly object. \n \n Valid values for the Instance ID: #SDAI_EIP_MIN_INST_ID <= InstanceId <= #SDAI_EIP_MAX_INST_ID */
  U8    Alignment [2];
};

/*---------------------------------------------------------------------------*/

/** \brief This structure holds statistic informations for CIP connections. */
struct SDAI_EIP_CIP_CONNECTION_STATS
{
  U16  NumberActiveEmConnections;     /**< Current number of active CIP Explicit Messaging connections. */
  U16  NumberSupportedEmConnections;  /**< Maximum number of supported CIP Explicit Messaging connections. */
  U32  NumberObservedEmConnections;   /**< Total number of all currently and previously established CIP Explicit Messaging connections. */

  U16  NumberActiveIoConnections;     /**< Current number of active CIP I/O Messaging connections. */
  U16  NumberSupportedIoConnections;  /**< Maximum number of supported CIP I/O Messaging connections. */
  U32  NumberObservedIoConnections;   /**< Total number of all currently and previously established CIP I/O Messaging connections. */

  U32  NumberOpenRequests;            /**< Total number of CIP connection open requests received. */
  U32  NumberOpenRequestErrors;       /**< Total number of CIP connection open requests errors. */
  U32  NumberCloseRequests;           /**< Total number of CIP connection close requests received. */
  U32  NumberCloseRequestErrors;      /**< Total number of CIP connection close requests errors. */

  U32  NumberTimeouts;                /**< Total number of CIP connection timeouts. */
};

/** \brief This structure holds statistic informations for TCP connections. */
struct SDAI_EIP_TCP_CONNECTION_STATS
{
  U16  NumberActiveTcpConnections;    /**< Current number of active TCP connections. */
  U16  NumberSupportedTcpConnections; /**< Maximum number of supported TCP connections. */
  U32  NumberObservedTcpConnections;  /**< Total number of all currently and previously established TCP connections. */
};

/** \brief This structure holds statistic informations for CIP Explicit Messaging communication. */
struct SDAI_EIP_CIP_EM_STATS
{
  U32  NumberConnectedMessagesSent;       /**< Total number of connected CIP Explicit Messages sent. */
  U32  NumberConnectedMessagesReceived;   /**< Total number of connected CIP Explicit Messages received. */
  U32  NumberUnconnectedMessagesSent;     /**< Total number of unconnected CIP Explicit Messages sent. */
  U32  NumberUnconnectedMessagesReceived; /**< Total number of unconnected CIP Explicit Messages received. */
};

/** \brief This structure holds statistic informations for CIP I/O Messages. */
struct SDAI_EIP_CIP_IO_STATS_NUMBER_MESSAGES
{
  /** Number of Class 1 UDP packets sent by the device. */
  U32  NumberMessagesSent;

  /** Number of Class 1 UDP packets the device has received. */
  U32  NumberMessagesReceived;

  /** Number of Class 1 UDP packets inhibited by the device. Packets are inhibited if a COS (Change-Of-State) module
      produces packets faster than 25% of the connections RPI. */
  U32  NumberMessagesInhibited;

  /** Number of Class 1 UDP packets rejected by the device. These packets were messages received and then
      rejected, e.g. because the connection was closed or a invalid packet format has been detected. */
  U32  NumberMessagesRejected;

  /** Number of Class 1 UDP packets not received in order. Each UDP packet has a sequence number and if a packet
      is missing (corrupted or dropped), the module will recognize this packet loss upon receipt of the next packet. */
  U32  NumberMessagesMissed;
};

/** \brief This structure holds statistic informations for CIP I/O Messaging communication. */
struct SDAI_EIP_CIP_IO_STATS
{
  U32                                             TotalExpectedPacketRate; /**< Sum of the theoretical packets/second of all connections based on the RPI. */

  struct SDAI_EIP_CIP_IO_STATS_NUMBER_MESSAGES    PerSecond;  /**< CIP I/O Messages statistics within the last one-second snapshot. */
  struct SDAI_EIP_CIP_IO_STATS_NUMBER_MESSAGES    Total;      /**< Cumulative CIP I/O Messages statistics. */
};

/** \brief This structure holds the address of the active ring supervisor. */
struct SDAI_EIP_SUPERVISOR_ADDR
{
  U8    IpAddress [4];   /**< IP address of the ring supervisor. */
  U8    MacAddress [6];  /**< MAC address of the ring supervisor. */
  U8    Alignment [2];
};

/** \brief This structure holds the ring status. */
struct SDAI_EIP_RING_STATUS
{
  U8                                NetworkTopology;        /**< Indicates the current network topology mode. Valid values: \ref NetworkTopology */
  U8                                NetworkStatus;          /**< Indicates the current network status. Valid values: \ref NetworkStatus */
  U8                                Alignment [2];
  struct SDAI_EIP_SUPERVISOR_ADDR   ActiveRingSupervisor;   /**< Contains the address of the active ring supervisor. */
};

/** \brief This structure holds informations related to address conflict detection.

           In case of an IP address conflict the attribute LastAddrConflict is filled by the stack providing informations on the conflicting state.
           The specification demands to store the information given by the LastAddrConflict attribute in a non-volatile memory. Thus it can be read
           out after a power cycle via attribute LastConflictDetected of the TCP/IP Interface object. This information must be passed at startup by
           the user via the function sdai_init() to the SDAI. For the first startup of the stack the structure content shall be set to 0. */
struct SDAI_EIP_ACD_STATUS
{
  U8                                  CurrentStatus;      /**< Bitfield showing the current ACD status. Valid values: \ref CurrentAcdStatus */
  U8                                  Alignment [3];
  struct SDAI_EIP_ACD_LAST_CONFLICT   LastAddrConflict;   /**< Contains informations related to the last address conflict. */
};

/** \brief This structure holds the EtherNet/IP specific additional status informations. */
struct SDAI_EIP_ADDSTATUS_DATA
{
  struct SDAI_EIP_CIP_CONNECTION_STATS    CipConnectionStats;           /**< Statistic informations for CIP connections */
  struct SDAI_EIP_TCP_CONNECTION_STATS    TcpConnectionStats;           /**< Statistic informations for TCP connections */
  struct SDAI_EIP_CIP_EM_STATS            CipEmStats;                   /**< Statistic informations for CIP Explicit Messaging */
  struct SDAI_EIP_CIP_IO_STATS            CipIoStats;                   /**< Statistic informations for CIP I/O Messaging */

  struct SDAI_EIP_RING_STATUS             RingStatus;  /**< Shows informations about the current ring status */
  struct SDAI_EIP_ACD_STATUS              AcdStatus;   /**< Shows informations related to the last address conflict detected */
};

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

#endif /* __SDAI_EIP_H__ */

/*****************************************************************************/
