/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

/**
 *
 * @page mb Modbus TCP
 *
 * \tableofcontents
 *
 * @section mboverview Overview
 * The Softing Modbus TCP Server Protocol Software realizes a small and portable
 * protocol stack for building Modbus applications. The Simple Device Application
 * Interface (SDAI) provides the services to communicate with the Modbus TCP Server
 * Protocol Software.
 *
 * Modbus is a communication protocol developed by Modicon for use with its programmable
 * logic controllers (PLCs). It has become a de facto standard communications protocol in
 * industry, and is now the most commonly available means of connecting industrial electronic
 * devices. Built on the standard TCP/IP protocol suite, Modbus TCP is based on a master/slave
 * and client/server architecture.
 *
 * @section mbmapping Mapping Modbus TCP to SDAI
 * In the following chapters the mapping of the Modbus TCP Server Protocol Software to the SDAI is described.
 *
 * @subsection mb_init_data Modbus Initialization
 * The structure #SDAI_INIT contains identification data, device specific data and a set of application specific
 * callback functions. The identification data are described by the structure #SDAI_IDENT_DATA. The SDAI common
 * parameter DevName of this structure is not used for Modbus TCP. The device specific data are described by the
 * structure #SDAI_DEVICE_DATA. These parameters are mapped to the object categories readable via the Modbus service
 * Read Device Identification (FC = 43/14) in the following way:
 *
 * <TABLE><TR>
 * <TH>SDAI</TH><TH>Modbus TCP</TH><TH>Description</TH>
 * </TR><TR>
 * <TD>SerialNumber</TD><TD>---</TD><TD>Ignored (not used by Modbus TCP)</TD>
 * </TR><TR>
 * <TD>VendorID</TD><TD>---</TD><TD>Ignored (not used by Modbus TCP</TD>
 * </TR><TR>
 * <TD>Type</TD><TD>---</TD><TD>Ignored (not used by Modbus TCP</TD>
 * </TR><TR>
 * <TD>VendorName</TD><TD>VendorName (Object ID: 0x00)</TD><TD>basic device identification</TD>
 * </TR><TR>
 * <TD>ProductCode</TD><TD>ProductCode (Object ID: 0x01)</TD><TD>basic device identification</TD>
 * </TR><TR>
 * <TD>MajorMinorRevision</TD><TD>MajorMinorRevision (Object ID: 0x02)</TD><TD>basic device identification</TD>
 * </TR><TR>
 * <TD>ProductName</TD><TD>ProductName (Object ID: 0x04)</TD><TD>regular device identification</TD>
 * </TR><TR>
 * <TD>OrderId</TD><TD>---</TD><TD>Ignored (not used by Modbus TCP</TD>
 * </TR></TABLE>
 *
 * The structure #SDAI_CALLBACKS is used to register all supported application specific callback functions.
 * The following table shows which SDAI callback functions are used by Modbus TCP. If not supported by the
 * application the user shall set the respective function pointer to NULL.
 *
 * <TABLE><TR>
 * <TH>SDAI Callback</TH><TH>Modbus TCP</TH>
 * </TR><TR>
 * <TD>IdentDataCbk()</TD><TD>Not used (set function pointer always to NULL)</TD>
 * </TR><TR>
 * <TD>ExceptionCbk()</TD><TD>Called when a fatal error occured</TD>
 * </TR><TR>
 * <TD>DataCbk()</TD><TD>Called when the output data or status of a unit has been changed</TD>
 * </TR><TR>
 * <TD>AlarmAckCbk()</TD><TD>Not used (set function pointer always to NULL)</TD>
 * </TR><TR>
 * <TD>WriteReqCbk()</TD><TD>Called when either the Write Single Coil (FC = 5), Write Multiple Coils (FC = 15),
 *                            Write Single Register (FC = 6) or Write Multiple Registers (FC = 16) services are received</TD>
 * </TR><TR>
 * <TD>ReadReqCbk()</TD><TD>Called when either the Read Coil (FC = 1), Read Discrete Input (FC = 2),
 *                          Read Holding Register (FC = 3) or Read Input Register (FC = 4) services are received</TD>
 * </TR><TR>
 * <TD>ControlReqCbk()</TD><TD>Not used (set function pointer always to NULL)</TD>
 * </TR><TR>
 * <TD>SyncSignalCbk()</TD><TD>Not used (set function pointer always to NULL)</TD>
 * </TR></TABLE>
 *
 * @subsection mb_plug_unit Modbus plugging of units
 * The Modbus data model is based on the 4 tables Discrete Coils (Read/Write, Single Bit),
 * Discrete Input (Read Only, Single Bit), Analog Input Registers (Read Only, 16 Bit Word) and
 * Analog Holding Registers (Read/Write, 16 Bit Word). All data, this means process data and also
 * device parameters are organized in these tables. For each table the Modbus protocol provides specific
 * services. The addressing of a data element is specified by the address parameter of the Modbus service.
 * It is possible to access either a single data element or multiple data elements concurrently.
 *
 * Because the Modbus data model did not distinguish between process data and device parameter, it is the job
 * of the user to define which data shall be assigned to a SDAI I/O unit. When plugging a SDAI I/O unit the user
 * can specify the mapping of the unit to a single data element or to a contiguous block of multiple data
 * elements via the structure #SDAI_MB_CFG_DATA. By calling sdai_plug_unit() a Unit ID is automatically
 * generated. This Unit ID consists of the start address of the first data element and the SDAI unit index.
 * The high word of the Unit ID is the start address of the first data element. The low word of the Unit ID is
 * the SDAI unit index. See also \ref MbUnitId. The mapping to a specific table is specified by the following unit types:
 *
 * - #SDAI_MB_UNIT_TYPE_DISCRETE_INPUT
 * - #SDAI_MB_UNIT_TYPE_COILS
 * - #SDAI_MB_UNIT_TYPE_INPUT_REGISTER
 * - #SDAI_MB_UNIT_TYPE_HOLDING_REGISTER
 *
 * The following figure shows an example for the mapping of SDAI I/O Units to the Modbus data model.
 *
 * @image html modbus_data_mapping.jpg
 *
 * @subsection mb_io_cfg_dynamic Modbus Dynamic IO Configuration
 *
 * Dynamic IO configuration is not supported by Modbus TCP.
 *
 * @subsection mb_service_access Modbus Service Data Access
 * Device parameters and process data are organized in the same Modbus tables. Thus the same Modbus services are used
 * to access these data. All read/write service requests to data elements not assigned to an SDAI I/O unit (see also
 * \ref mb_plug_unit) will be mapped to SDAI callback functions. The addressing and service informations of the
 * received Modbus request will be described by the structure #SDAI_MB_SERVICE_HEADER. The following table shows
 * the mapping of the Modbus function codes to SDAI callback functions:
 *
 * <TABLE><TR>
 * <TH>Function Code</TH><TH>Service Name</TH><TH>SDAI Callback function</TH>
 * </TR><TR>
 * <TD>0x01</TD><TD>Read Coils</TD><TD>ReadReqCbk()</TD>
 * </TR><TR>
 * <TD>0x02</TD><TD>Read Discrete Inputs</TD><TD>ReadReqCbk()</TD>
 * </TR><TR>
 * <TD>0x03</TD><TD>Read Holding Registers</TD><TD>ReadReqCbk()</TD>
 * </TR><TR>
 * <TD>0x04</TD><TD>Read Input Registers</TD><TD>ReadReqCbk()</TD>
 * </TR><TR>
 * <TD>0x05</TD><TD>Write Single Coil</TD><TD>WriteReqCbk()</TD>
 * </TR><TR>
 * <TD>0x06</TD><TD>Write Single Register</TD><TD>WriteReqCbk()</TD>
 * </TR><TR>
 * <TD>0x0F</TD><TD>Write Multiple Coils</TD><TD>WriteReqCbk()</TD>
 * </TR><TR>
 * <TD>0x10</TD><TD>Write Multiple Registers</TD><TD>WriteReqCbk()</TD>
 * </TR></TABLE>
 * Other Modbus services are not mapped to the SDAI service interface.
 *
 * @subsection mb_errors Modbus Service Error Codes
 * The following table shows how the SDAI common service error codes (see also \ref ServiceErrorCode) are mapped
 * to Modbus specific service error codes. This error mapping is needed for Read and Write responses.
 *
 * <TABLE><TR>
 * <TH>SDAI Service Error Code</TH><TH>Exception Code</TH><TH>Comment</TH>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_SUCCESS</TD><TD>00</TD><TD>no error</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_OBJECT_NOT_EXISTING</TD><TD>02</TD><TD>ILLEGAL DATA ADDRESS</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_INVALID_SIZE</TD><TD>02</TD><TD>ILLEGAL DATA ADDRESS</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_STATE_CONFLICT</TD><TD>01</TD><TD>ILLEGAL FUNCTION</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_APPLICATION</TD><TD>04</TD><TD>SLAVE DEVICE FAILURE</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_RESOURCE</TD><TD>06</TD><TD>SLAVE DEVICE BUSY</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_NOT_SUPPORTED</TD><TD>01</TD><TD>ILLEGAL FUNCTION</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_INVALID_PARAMETER</TD><TD>03</TD><TD>ILLEGAL DATA VALUE</TD>
 * </TR><TR>
 * <TD>all others</TD><TD>04</TD><TD>SLAVE DEVICE FAILURE</TD>
 * </TR></TABLE>
 *
 * @subsection mb_diag_alarm Modbus TCP Diagnosis and Alarms
 * These features are not supported by Modbus TCP. Thus the functions sdai_alarm_request(), sdai_diagnosis_request()
 * and the AlarmAckCbk() are not supported.
 *
 */

#ifndef __SDAI_MB_H__
#define __SDAI_MB_H__

/******************************************************************************
DEFINES
******************************************************************************/

/** \anchor MbUnitId @name SDAI Unit ID macros for Modbus
* Format of the SDAI Unit ID for Modbus:
\verbatim
   ---------------------------------------------------------------
   |          MB Address         |         SDAI unit index       |
   ---------------------------------------------------------------
   32         High Word          16           Low Word           0
\endverbatim
*/
//@{
#define _SDAI_MB_ADDRESS_AND_INDEX_TO_ID(Address, Index)    ((U32) ((((U32) (Address)) << 16) | (U32) (Index))) /**< Converts the MB address and SDAI index to the SDAI Id */
#define _SDAI_MB_ID_TO_ADDRESS(Type, Id)                    ((Type) ((Id >> 16) & 0x0000FFFF))                  /**< Converts the SDAI Id to the MB address */
#define _SDAI_MB_ID_TO_INDEX(Type, Id)                      ((Type) ((Id & 0x0000FFFF)))                        /**< Converts the SDAI Id to the unit index used in the interface */
//@}

/** \anchor MbUnitType @name Modbus Unit Types
* Modbus specific unit types */
//@{
#define SDAI_MB_UNIT_TYPE_DISCRETE_INPUT            254   /**< indicates an area of discrete inputs */
#define SDAI_MB_UNIT_TYPE_COILS                     253   /**< indicates an area of coils - can be used both for inputs and outputs */
#define SDAI_MB_UNIT_TYPE_INPUT_REGISTER            252   /**< indicates an area of input registers */
#define SDAI_MB_UNIT_TYPE_HOLDING_REGISTER          251   /**< indicates an area of holding registers - can be used both for inputs and outputs */
//@}

/*---------------------------------------------------------------------------*/

/* Definitions for Modbus specific device data */
#define SDAI_MB_VENDOR_NAME_MAX_LEN      60         /**< Maximum length of the Modbus vendor name string */
#define SDAI_MB_PRODUCT_CODE_MAX_LEN     20         /**< Maximum length of the Modbus product code string */
#define SDAI_MB_REVISION_MAX_LEN         20         /**< Maximum length of the Modbus major/minor revision string */


/*---------------------------------------------------------------------------*/

#define SDAI_MB_SERVICE_CLASS_READ        0x10
#define SDAI_MB_SERVICE_CLASS_WRITE       0x20

/** \anchor ModbusServiceCodes @name Modbus Services
 *  Definitions for Modbus specific Services */
//@{
/** The Read Coil service is used to read the ON/OFF status of discrete coils (Modbus FC = 1). */
#define SDAI_MB_SERVICE_READ_COIL                 (SDAI_MB_SERVICE_CLASS_READ | 0x1)

/** The Read Discrete Input service is used to read the ON/OFF status of discrete inputs (Modbus FC = 2). */
#define SDAI_MB_SERVICE_READ_DISCRETE_INPUT       (SDAI_MB_SERVICE_CLASS_READ | 0x2)

/** The Read Holding Register service is used to read the content of output holding registers (Modbus FC = 3). */
#define SDAI_MB_SERVICE_READ_HOLDING_REGISTER     (SDAI_MB_SERVICE_CLASS_READ | 0x3)

/** The Read Input Register service is used to read the content of input registers (Modbus FC = 4). */
#define SDAI_MB_SERVICE_READ_INPUT_REGISTER       (SDAI_MB_SERVICE_CLASS_READ | 0x4)

/** The Write Coil service is used to set a single or a block of outputs to either ON or OFF.
    The Write Single Coil service (Modbus FC = 5) shall be processed if the parameter NumberElements of
    structure #SDAI_MB_SERVICE_HEADER is set to one. The Write Multiple Coils service (Modbus FC = 15)
    shall be processed if the parameter NumberElements is set to a value greater than one. */
#define SDAI_MB_SERVICE_WRITE_COIL                (SDAI_MB_SERVICE_CLASS_WRITE | 0x1)

/** The Write Register service is used to write a single or a block of output holding registers.
    The Write Single Register service (Modbus FC = 6) shall be processed if the parameter NumberElements of
    structure #SDAI_MB_SERVICE_HEADER is set to one. The Write Multiple Registers service (Modbus FC = 16)
    shall be processed if the parameter NumberElements is set to a value greater than one. */
#define SDAI_MB_SERVICE_WRITE_REGISTER            (SDAI_MB_SERVICE_CLASS_WRITE | 0x2)
//@}

/*---------------------------------------------------------------------------*/

/** \anchor NumberElementsRange @name Modbus specific limits
 * Modbus specific limits for the configuration of data elements */
//@{
#define SDAI_MB_MIN_NUMBER_ELEMENTS           (1)                                 /**< minimum number of data elements */

#define SDAI_MB_MAX_NUMBER_DISCRETE_INPUTS    (SDAI_MAX_UNIT_IO_DATA_SIZE * 8)    /**< maximum number of discrete inputs */
#define SDAI_MB_MAX_NUMBER_COILS              SDAI_MB_MAX_NUMBER_DISCRETE_INPUTS  /**< maximum number of coils */

#define SDAI_MB_MAX_NUMBER_INPUT_REGISTERS    (SDAI_MAX_UNIT_IO_DATA_SIZE / 2)    /**< maximum number of input registers */
#define SDAI_MB_MAX_NUMBER_HOLDING_REGISTERS  SDAI_MB_MAX_NUMBER_INPUT_REGISTERS  /**< maximum number of holding registers */
//@}

/*---------------------------------------------------------------------------*/

/** \anchor MbStatus @name Modbus Stack Status
 * Modbus specific Stack Status */
//@{
#define SDAI_MB_STATE_OK                       0x01    /**< The state of the Modbus Protocol Software is faultless */
#define SDAI_MB_STATE_TCP_FAILURE              0x02    /**< The TCP stack has reported an internal unrecoverable error */
#define SDAI_MB_STATE_DUPLICATE_NAME_ERROR     0x03    /**< Conflict of station names. The station name of the Modbus stack
                                                            is not unique in the network. Communication is not possible. */
#define SDAI_MB_STATE_FAILURE                  0x04    /**< General error code */
//@}

#define MODBUS_DEFAULT_TCP_PORT                502     /**< The default Modbus TCP Port */

/******************************************************************************
STRUCTURES
******************************************************************************/

/** \brief Modbus TCP network specific parameter */
struct SDAI_MB_IDENT_DATA
{
  U8      Address [4];                           /**< The IP-Address of the Device. */
  U8      Netmask [4];                           /**< The Netmask of the Device. */
  U8      Gateway [4];                           /**< The Gateway of the Device. The user may set #SDAI_GATEWAY_UNUSED if not available. */

  U8      MacAddressDevice [6];                  /**< The MAC address of the device */

  U16     TCPListeningPort;                      /**< Additional TCP Port the Modbus Stack listens to if it differs from MODBUS_DEFAULT_TCP_PORT */
};

/*---------------------------------------------------------------------------*/

/** \brief Modbus specific device parameter. */
/** See also \ref mb_init_data. */
struct SDAI_MB_DEVICE_DATA
{
  char    VendorName [SDAI_MB_VENDOR_NAME_MAX_LEN];       /**< The vendor name of the device */
  char    ProductCode [SDAI_MB_PRODUCT_CODE_MAX_LEN];     /**< The product code of the device */
  char    MajorMinorRevision [SDAI_MB_REVISION_MAX_LEN];  /**< Major/Minor revision number of the device */
};

/*---------------------------------------------------------------------------*/

/** \brief Modbus specific address description */
struct SDAI_MB_SERVICE_HEADER
{
  U16   Service;            /**< Specifies the Modbus service. Valid values: \ref ModbusServiceCodes */
  U16   StartAddress;       /**< Start address of the first data element to be accessed */
  U16   NumberElements;     /**< Number of contiguous data elements */

  U8    Alignment [2];
};

/*---------------------------------------------------------------------------*/

/** \brief Modbus specific configuration data. */
/** See also \ref mb_plug_unit. */
struct SDAI_MB_CFG_DATA
{
  U16   StartAddress;       /**< Start address of the first data element assigned to the unit */

  /** Number of contiguous data elements. \n\n
      Valid values for units of type #SDAI_MB_UNIT_TYPE_DISCRETE_INPUT:\n
      #SDAI_MB_MIN_NUMBER_ELEMENTS <= NumberElements <= #SDAI_MB_MAX_NUMBER_DISCRETE_INPUTS \n\n
      Valid values for units of type #SDAI_MB_UNIT_TYPE_COILS:\n
      #SDAI_MB_MIN_NUMBER_ELEMENTS <= NumberElements <= #SDAI_MB_MAX_NUMBER_COILS \n\n
      Valid values for units of type #SDAI_MB_UNIT_TYPE_INPUT_REGISTER:\n
      #SDAI_MB_MIN_NUMBER_ELEMENTS <= NumberElements <= #SDAI_MB_MAX_NUMBER_INPUT_REGISTERS \n\n
      Valid values for units of type #SDAI_MB_UNIT_TYPE_HOLDING_REGISTER:\n
      #SDAI_MB_MIN_NUMBER_ELEMENTS <= NumberElements <= #SDAI_MB_MAX_NUMBER_HOLDING_REGISTERS */
  U16   NumberElements;
};

/*---------------------------------------------------------------------------*/

/** \brief This structure holds the Modbus specific additional status informations. */
struct SDAI_MB_ADDSTATUS_DATA
{
  U8    ProtocolState;    /**< Indicates the last error detected by the Modbus stack. Valid values for the ProtocolState: \ref MbStatus */
  U8    Alignment [3];
};

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

#endif /* __SDAI_MB_H__ */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
