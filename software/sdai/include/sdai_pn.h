/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

/**
 *
 * @page pnio PROFINET IO
 *
 * \tableofcontents
 *
 * @section pnoverview Overview
 * PROFINET is the Ethernet-based automation standard of PROFIBUS & PROFINET International (PI).
 * PROFINET IO is used for data exchange between I/O controllers (PLC, etc.) and I/O devices (field
 * devices). PROFINET IO uses the proven communication model and application view of PROFIBUS DP
 * and extends it by Ethernet as the communication medium. Among other benefits, this provides a greater
 * bandwidth and allows more stations on the network. The PROFINET IO specifications define a protocol
 * and an application interface for exchanging I/O data, alarms and diagnostics and for transmitting data
 * records and logbook information.
 * To exchange I/O data and alarms, PROFINET IO directly uses the Ethernet protocol. This real-time (RT)
 * solution allows response times in the range of 1 ms, which corresponds to today's PROFIBUS DP
 * applications. If it has to be even faster and if data exchange should be performed isochronous (IRT), a
 * special hardware is used, which also supports switch functions. "Normal" Ethernet communication is of
 * course also possible when using this dedicated hardware. The solution consists in reserving bandwidth for
 * the isochronous data exchange and bandwidth for non real-time data.
 *
 * @subsection pn_features Features
 * - PROFINET Device accordant Confomance Class A/B/C
 * - MRP Client
 * - Multicast Provider and Subscriber
 * - Fast Start Up
 * - Shared device
 * - Shared input
 * - 2 ARs
 * - Relative forwarder
 * - PTCP
 * - Minimum cycle time 250 &mu;s
 * - up to 8 kByte configuration data per unit (SDAI default 500 byte)
 * - up to 8 kByte parameter data per unit (SDAI default 500 byte)
 * - 1024 Byte I/O data
 * - PROFINET application profiles
 * - Demo Application with a dedicated GSDML file aimed at quickly getting you started with
 *   implementing your own PROFINET IO application
 *
 * Some of the features need special hardware support and may not be available on all platforms.
 *
 * @section pnmapping Mapping PROFINET IO to SDAI
 * In the following chapters the mapping of the PROFINET IO Device Protocol Software to the SDAI is described.
 *
 * @subsection pn_init_data PROFINET IO Initialization
 * Mapping of the SDAI init data to the PROFINET IO parameter:
 *
 * <TABLE><TR>
 * <TH>SDAI</TH><TH>PROFINET IO</TH><TH>Description</TH>
 * </TR><TR>
 * <TD>DevName</TD><TD>NameOfStation</TD><TD></TD>
 * </TR><TR>
 * <TD>IfName</TD><TD>---</TD><TD>Specifies the used ethernet interface. Not used on all platforms</TD>
 * </TR><TR>
 * <TD>VendorID</TD><TD>Vendor ID</TD><TD>Only the Low-Word (Bits 0-15) is used</TD>
 * </TR><TR>
 * <TD>ProductCode</TD><TD>Device ID</TD><TD>Only the Low-Word (Bits 0-15) is used</TD>
 * </TR><TR>
 * <TD>SoftwareRevision</TD><TD>Software Revision</TD><TD>Used for I&M0</TD>
 * </TR><TR>
 * <TD>HardwareRevision</TD><TD>Hardware Revision</TD><TD>Used for I&M0</TD>
 * </TR><TR>
 * <TD>RevisionCounter</TD><TD>Revision Counter</TD><TD>Used for I&M0</TD>
 * </TR><TR>
 * <TD>SerialNumber</TD><TD>Serial Number</TD><TD>Used for I&M0</TD>
 * </TR><TR>
 * <TD>ProductName</TD><TD>Type of Station</TD><TD>Used for I&M0</TD>
 * </TR><TR>
 * <TD>OrderId</TD><TD>Order ID</TD><TD>Used for I&M0</TD>
 * </TR></TABLE>
 *
 * The structure #SDAI_CALLBACKS is used to register all supported application specific callback functions.
 * The following table shows which SDAI callback functions are used by PROFINET IO. If not supported by the
 * application the user shall set the respective function pointer to NULL.
 *
 * <TABLE><TR>
 * <TH>SDAI Callback</TH><TH>PROFINET IO</TH>
 * </TR><TR>
 * <TD>IdentDataCbk()</TD><TD>Called when the network parameter (e.g. IP address) are changed</TD>
 * </TR><TR>
 * <TD>ExceptionCbk()</TD><TD>Called when a fatal error occurred</TD>
 * </TR><TR>
 * <TD>DataCbk()</TD><TD>Called when the output data or status of a unit has been changed</TD>
 * </TR><TR>
 * <TD>AlarmAckCbk()</TD><TD>Called when an outstanding alarm is acknowledged by the controller</TD>
 * </TR><TR>
 * <TD>WriteReqCbk()</TD><TD>Called when a write request is received</TD>
 * </TR><TR>
 * <TD>ReadReqCbk()</TD><TD>Called when a read request is received</TD>
 * </TR><TR>
 * <TD>ControlReqCbk()</TD><TD>Called when a "Blink" or "reset to factory default" command is received</TD>
 * </TR><TR>
 * <TD>SyncSignalCbk()</TD><TD>Called when a synchronization signal is generated (only IRT)</TD>
 * </TR></TABLE>
 *
 * @subsection pn_plug_unit PROFINET IO plugging of units
 * The SDAI unit will be mapped to an IO-Data object. A IO-Data object is assigned to a specific
 * slot/subslot. For the first unit the application must always plug a head module of type #SDAI_UNIT_TYPE_HEAD,
 * which has to be assigned to slot 0. This module has a special meaning by holding the system defined submodules (e.g. Interface
 * and Ethernet ports) and normally has no IO data.
 *
 * By calling sdai_plug_unit() or sdai_plug_unit_ext() an unique Unit ID is either automatically generated or defined by the application.
 * This Unit ID consists of the PROFINET IO slot and subslot. The high word of the Unit ID is the subslot. The low word of the Unit ID is the slot.
 * See also \ref PnUnitId.
 *
 * The assignment to a specific slot/subslot of the IO modules is either done by the SDAI automatically in the order
 * of plugging the SDAI units or can be done by the application. The decision has to be made when plugging the head
 * module by either passing #SDAI_INVALID_ID as unit ID for automatic assignment or by passing the unit ID representing
 * slot 0 subslot 1 for manual assignment (#SDAI_DYNAMIC_IO_CONFIGURATION must be set).<BR>
 * If automatic assignment is selected the first unit will be assigned to slot 0, the second to slot 1, and so on.  For the subslot
 * the SDAI always uses 1. Thus it is not possible to assign an I/O module to a specific subslot or to assign multiple submodules to a slot.<BR>
 * If manual assignment is selected the application can define the slot and subslot of a unit by passing the untit ID representing the
 * slot/subslot configuration.
 *
 * For the unique identification of a module the user has to specify
 * the respective Application-Profile and Identnumbers within the structure #SDAI_PN_CFG_DATA.
 *
 * The Application-Profile of a module/submodule must be mapped to the related Profile-API in the PROFINET GSDML. A device
 * can support more than one application profile (e.g. some modules/submodule use #SDAI_PN_DEFAULT_PROFILE and others use
 * #SDAI_PN_DRIVE_PROFILE). The following table shows how the SDAI Application-Profiles (see also \ref PnProfileId) are mapped
 * to PROFINET IO specific application profiles.
 *
 * <TABLE><TR>
 * <TH>SDAI Application Profile</TH><TH>PN Application Profile</TH><TH>PN Application Profile API</TH><TH>Comment</TH>
 * </TR><TR>
 * <TD>#SDAI_PN_DEFAULT_PROFILE</TD><TD>Default Profile</TD><TD>0x00000000</TD>
 * <TD>If the device (module/submodule) does not support any kind of application profile, this is the default. This Profile
 * covers all "PROFINET Common profiles" (i.e. PROFIsafe, PROFIenergy, Redundancy, ...). It is not necessary to add the
 * parameter API to the GSDML description of the related submodule, because this is the implicite value (if not specified).</TD>
 * </TR><TR>
 * <TD>#SDAI_PN_DRIVE_PROFILE</TD><TD>PROFIdrive Profile</TD><TD>0x00003A00</TD>
 * <TD>If the device (module/submodule) supports PROFIdrive an appropriate application class shall be selected. For more
 * informations check the PROFIdrive specification at the PI-website. The parameter API="0x00003A00" has to be added to the
 * GSDML description of the related submodule.</TD>
 * </TR><TR>
 * <TD>#SDAI_PN_ENCODER_PROFILE</TD><TD>Encoder Profile</TD><TD>0x00003D00</TD>
 * <TD>If the device (module/submodule) supports the Encoder profile an appropriate application class shall be choosen. For more
 * informations check the Encoder specification at the PI-website. The parameter API="0x00003D00" has to be added to the
 * GSDML description of the related submodule.</TD>
 * </TR><TR>
 * <TD>#SDAI_PN_INTELLIGENT_PUMP_PROFILE</TD><TD>Intelligent pump Profile</TD><TD>0x00005D00</TD>
 * <TD>If the device (module/submodule) supports the intelligent pump profile an appropriate pump class (i.e. generic, vacuum, ...)
 * shall be choosen. For more informations check the intelligent pump specification at the PI-website. The parameter
 * API="0x00005D00" has to be added to the GSDML description of the related submodule.</TD>
 * </TR></TABLE>
 *
 * The ident number of the respective module/submodule must match to the corresponding module/submodule in the PROFINET GSDML file.
 * The following figure shows an example for the mapping of SDAI I/O Units to PROFINET IO slots.
 *
 * @image html profinet_data_mapping.jpg
 *
 * @subsection pn_cfg_dynamic PROFINET IO Dynamic IO Configuration
 *
 * Both types of Dynamic IO Configuration
 * - \ref io_cfg_variable
 * - \ref io_cfg_selectable
 *
 * are supported by PROFINET IO. To use dynamic IO configuration #SDAI_DYNAMIC_IO_CONFIGURATION must be set when calling sdai_init().
 * In case of changing the data layout of IO module(s) the ident number of the related
 * module does not match the ident number requested by the PROFINET IO controller. If the application has initialized
 * the module with #SDAI_PN_UNIT_FLAG_SUPPORT_MODULE_SUBSTITUTION, the requested direction of the data (input and/or
 * output) matches and the requested length of the process data is less or equal to the provided one, the expected
 * module layout replaces the configured one. Afterwards the application is informed about the new process data layout
 * via a Control indication with the control code #SDAI_CONTROL_CODE_CFG_DATA_INFO. The second type of Dynamic IO
 * Configuration allows the application to adapt the whole process data configuration to the requested configuration
 * of the PROFINET IO controller. It gives the flexibility to change the number of IO module(s) at runtime and adapt the
 * whole IO Configuration to the configured configuration of the PROFINET IO controller. The application uses sdai_pull_unit()
 * and sdai_plug_unit() or sdai_plug_unit_ext() to handle the adaptation.
 * The allowed changes of the IO Configuration and the supported IO Configuration have to be described in the device
 * description file.
 *
 * @subpage pn_dynamic_samples
 *
 * @subsection pn_connect PROFINET IO Connection establishment
 *
 * The connection establishment is mainly handled automatically by the SDAI but the application is informed about important
 * steps and can influence the behavior.
 *
 * @subpage pn_connect_samples
 *
 * 1. The connection establishment starts with the detection of the device by the controller and setting of the IP settings. Therefor the controller sends
 *    DCP ident requests with the name of the device. The ident requests are automatically answered by the SDAI if the name matches. After
 *    device is detected the controller sets the IP settings if necessary with a DPC set request. The new settings are automatically used by
 *    the SDAI and the application is informed about the new settings with the IdentDataCbk.
 * 2. The next step is that the controller sends a connect request with the connection settings. The most important part of this is the expected IO
 *    configuration. The SDAI informs the application about the expected configuration by calling the ControlReqCbk with code #SDAI_CONTROL_CODE_CFG_DATA_INFO
 *    for every submodule. If #SDAI_DYNAMIC_IO_CONFIGURATION is set the application has to response to the callback with a call to sdai_control_response().
 *    Before the response the application can adapt the configuration by using sdai_plug_unit() or sdai_plug_unit_ext(). With the sdai_control_response() for the last submodule the stack checks the
 *    configuration and generates the connect response with a ModuleDiffBlock if needed.
 * 3. If defined in the GSDML file the controller transfers the parameterization data to the device. The application has to answer the WriteReqCbk() with a call to
 *    sdai_write_response(). It is possible to answer with an error code (\ref ServiceErrorCode) but this would lead to an interruption of the connection establishment
 *    what is not intended by PROFINET IO. The device should always accept the parameterization data!
 * 4. Finally the controller signals end of parameterization with a control request (PARAM_END). This is signaled to the application by a call of the ControlReqCbk(SDAI_PN_CONTROL_PARAM_END).
 *    To finalize connection establishment the application has to answer this with calling sdai_control_response() to trigger the stack to send
 *    the control request (APPL_READY). Before this the status of all IO data has to be set to #SDAI_DATA_STATUS_VALID with sdai_set_data() (see \ref pn_data_status).
 *
 * @subsection pn_data_status PROFINET IO Data status
 * The SDAI holds a status for every input/output unit. This status is set together with the data when calling sdai_set_data() and received when calling sdai_get_data().
 * For PROFINET IO the status is mapped to the IOPS (provider status) for input submodules and shows the IOCS (consumer status) for output submodules. The status is transfered
 * unchanged from the network to the application for output unit and from the application to the network for input units so it can be set to any value. Defined values for the status
 * are \ref DataStatus and \ref PnDataStatus. A change of the status from #SDAI_DATA_STATUS_INVALID to #SDAI_DATA_STATUS_VALID during an open connection must be signaled by a
 * #SDAI_PN_ALARM_RETURN_SUBMODULE alarm.
 *
 * @subsection pn_data_substitute PROFINET IO Data substitution
 * SDAI supports data substitution for output units as defined by the PROFINET IO specification. This feature can be enabled on a per unit basis by setting the
 * Unit Flag #SDAI_PN_UNIT_FLAG_SUPPORT_DATA_SUBSTITUTION. If this flag is not set, data and status are forwarded to the application as received from the controller.
 * PROFINET IO defines three different strategies for the output data if the controller doesn't deliver valid data for any reason. The substitute
 * strategy is always used when enabled and no valid data are received. Reasons are connection to the controller is interrupted or IOCS is not valid. If substitution is active, the
 * status of an Unit received by sdai_get_data() is always #SDAI_DATA_STATUS_VALID and the data are set according the configured substitution strategy.
 *
 * The different strategies are:
 * - <b>Zero:</b> The output data are set to all 0x00.
 * - <b>LastValue:</b> The output data are freezed to the last valid value received from the controller.
 * - <b>ReplacementValue:</b> The substitute value is set by the controller during connection establishment.
 *
 * The default substitution strategy after initialization until it is changed by a controller is "Zero". Which of the substitution strategies are really supported by a device
 * has to be stated in the GSDML file and can also be restricted.
 *
 * @subsection pn_errors PROFINET IO Service Error Codes
 * The following table shows how the SDAI common service error codes (see also \ref ServiceErrorCode) are mapped
 * to PROFINET IO specific service error codes. This error mapping is needed for Read and Write responses.
 *
 * <TABLE><TR>
 * <TH>SDAI Service Error Code</TH><TH>PN Error Code 1</TH><TH>PN Error Code 2</TH><TH>Comment</TH>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_SUCCESS</TD><TD>0x00</TD><TD>0x00</TD><TD>-</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_OBJECT_NOT_EXISTING</TD><TD>0xB2</TD><TD>0x07</TD><TD>invalid slot/subslot</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_INVALID_SIZE</TD><TD>0xB1</TD><TD>0xB1</TD><TD>write length error</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_STATE_CONFLICT</TD><TD>0xB5</TD><TD>0xB5</TD><TD>-</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_APPLICATION</TD><TD>0xA0/0xA1</TD><TD>0xA0/0xA1</TD><TD>error read/error write</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_RESOURCE</TD><TD>0xC2</TD><TD>0xC2</TD><TD>resource busy</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_NOT_SUPPORTED</TD><TD>0xA9</TD><TD>0xA9</TD><TD>feature not supported</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_ACCESS_DENIED</TD><TD>0xB6</TD><TD>0xB6</TD><TD>-</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_INVALID_PARAMETER</TD><TD>0xB0</TD><TD>0x0A</TD><TD>Invalid index</TD>
 * </TR></TABLE>
 *
 * @subsection pn_control_code PROFINET IO Control Codes
 * If a control indication is signalled to the application via the ControlReqCbk() the parameter ControlCode of the structure #SDAI_CONTROL_IND
 * specifies the action which shall be performed. Only the SDAI common control code #SDAI_CONTROL_RESET_TO_DEFAULTS and #SDAI_CONTROL_CODE_CFG_DATA_INFO
 * are used by PROFINET IO to signal reset to factory settings and a changed I/O configuration to the application. In addition the PROFINET IO specific control code #SDAI_PN_CONTROL_BLINK
 * and #SDAI_PN_CONTROL_PARAM_END is supported.
 *
 * @subsection pn_diag_alarm PROFINET IO Diagnosis and Alarms
 *
 * Diagnosis is a basic feature of PROFINET. It enables the user of a device or the operator of a plant to be notified about
 * a maintenance request or a fault of a specific device. For device vendors and manufacturers it is strongly recommended to
 * read the PROFINET guideline "Diagnosis for PROFINET IO" from the PI-website to understand and implement the diagnosis concept.
 *
 * The following table shows how the SDAI common severity (see also \ref DiagSeverity) are mapped
 *
 * <TABLE><TR>
 * <TH>SDAI Diagnosis Severity</TH><TH>PN Diagnosis Type</TH>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_SEVERITY_MAINTENANCE_REQUIRED</TD><TD>Maintenance Required</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_SEVERITY_OUT_OF_SPECIFICATION</TD><TD>Maintenance Demanded</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_SEVERITY_CHECK_FUNCTION</TD><TD>Maintenance Demanded</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_SEVERITY_FAILURE</TD><TD>Diagnosis</TD>
 * </TR></TABLE>
 *
 * If the common diagnosis types does not match the application needs or a (profile) specific diagnosis message
 * (e.g. channel-, ext-channel or qualified-channel dianosis) shall be generated, it is possible to create any possible
 * diagnosis message by setting the value #SDAI_DIAGNOSIS_TYPE_PROTOCOL_SPECIFIC in the diagnosis type.
 *
 * The following table shows how the SDAI diagnosis types are mapped
 *
 * <TABLE><TR>
 * <TH>SDAI Diagnosis Type</TH><TH>Channel Error Type</TH><TH>Description</TH>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_SHORT_CIRCUIT</TD><TD>0x0001</TD><TD>Short circuit</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_INPUT_UNDERVOLTAGE</TD><TD>0x0002</TD><TD>Undervoltage</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_INPUT_OVERVOLTAGE</TD><TD>0x0003</TD><TD>Overvoltage</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OUTPUT_UNDERVOLTAGE</TD><TD>0x0002</TD><TD>Undervoltage</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OUTPUT_OVERVOLTAGE</TD><TD>0x0003</TD><TD>Overvoltage</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_DEVICE_UNDERVOLTAGE</TD><TD>0x0002</TD><TD>Undervoltage</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_DEVICE_OVERVOLTAGE</TD><TD>0x0003</TD><TD>Overvoltage</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_INPUT_UNDERCURRENT</TD><TD>0x0009</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_INPUT_OVERCURRENT</TD><TD>0x0009</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OUTPUT_UNDERCURRENT</TD><TD>0x0009</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OUTPUT_OVERCURRENT</TD><TD>0x0009</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_DEVICE_UNDERCURRENT</TD><TD>0x0009</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_DEVICE_OVERCURRENT</TD><TD>0x0009</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OVERLOAD</TD><TD>0x0004</TD><TD>Overload</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OVERTEMPERATURE</TD><TD>0x0005</TD><TD>Overtemperature</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_UNDERTEMPERATURE</TD><TD>0x0009</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_AMBIENTE_OVERTEMPERATURE</TD><TD>0x0005</TD><TD>Overtemperature</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_AMBIENTE_UNDERTEMPERATURE</TD><TD>0x0009</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_LINE_BREAK</TD><TD>0x0006</TD><TD>Line break</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_UPPER_LIMIT_EXCEEDED</TD><TD>0x0007</TD><TD>Upper limit value exceeded</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_LOWER_LIMIT_EXCEEDED</TD><TD>0x0008</TD><TD>Lower limit value exceeded</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_GENERIC</TD><TD>0x0009</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_SIMULATION_ACTIVE</TD><TD>0x000A</TD><TD>Simulation active</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_PRM_MISSING</TD><TD>0x000F</TD><TD>The channel needs (additional) parameters. No or to less parameters are written</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_PRM_FAULT</TD><TD>0x0010</TD><TD>Parameterization fault. Wrong or to many parameters are written</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_POWER_SUPPLY_FAULT</TD><TD>0x0011</TD><TD>Power supply fault</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_FUSE_BLOWN</TD><TD>0x0012</TD><TD>Fuse blown / open</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_COMMUNICATION_FAULT</TD><TD>0x0013</TD><TD>Communication fault. Sequence number wrong / sequence wrong</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_GROUND_FAULT</TD><TD>0x0014</TD><TD>Ground fault</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_REFERENCE_POINT_LOST</TD><TD>0x0015</TD><TD>Reference point lost</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_SAMPLING_ERROR</TD><TD>0x0016</TD><TD>Process event lost / sampling error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_THRESHOLD_WARNING</TD><TD>0x0017</TD><TD>Threshold warning</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OUTPUT_DISABLED</TD><TD>0x0018</TD><TD>Output disabled</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_SAFETY_EVENT</TD><TD>0x0019</TD><TD>Safety event</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_EXTERNAL_FAULT</TD><TD>0x001A</TD><TD>External fault</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_TEMPORARY_FAULT</TD><TD>0x001F</TD><TD>Temporary fault</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_DEVICE_HARDWARE</TD><TD>0x0009</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_DEVICE_SOFTWARE</TD><TD>0x0009</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_MONITORING</TD><TD>0x0009</TD><TD>Error</TD>
 * </TR><TR>
 * <TD>all others</TD><TD>0x0009</TD><TD>Error</TD>
 * </TR></TABLE>
 *
 * @subsection pn_system_defined_submodules PROFINET IO Interface- and Port-Submodule Handling
 *
 * PROFINET defines the range 0x8000 to 0x8FFF of submodule numbers to be used as Interface- and Port-submodules. These submodules are
 * called "System-Defined-Submodules". Normally they do not provide/consume process data except the always available submodule data status.
 * The interface- and port-submodules are used to adjust and/or check physical and logical network settings or parameters. So an
 * engineering tool is able to adjust the link-state of a specific port-module to down independing from the fact if there is a cable
 * plugged or not. Another use case for this feature is the membership of the device in a MRP domain. If the interface- and port-
 * submodules are parameterized by the engineering, they can detect if the MRP domain exists and if the MRP domain is the expected one.
 * So any unexpected network behavior or unexpected network node can be detected and the PLC respective engineering tool can be
 * notified about this.
 *
 * @subsubsection pn_system_defined_submodules_protocol_ip Protocol-IP
 *
 * In customer solutions which use Softings Protocol-IP, the interface- and port-submodule are handled completely by the PROFINET-subsystem.
 * Any use of #SDAI_APPL_HANDLES_INTERFACE_AND_PORTS and/or #_SDAI_PN_SLOT_AND_SYSTEM_DEFINED_SUBSLOT_TO_ID is prohibited in this case.
 *
 * @subsubsection pn_system_defined_submodules_customer Customer specific platforms
 *
 * Customer which use a solution where the Simple-Device-Application-Interface and the PROFINET Stack from Softing is adapted to a target
 * platform without Protocol-IP can take over control of the "System-Defined-Submodules" via application. This requires that the SDAI library
 * knows that the application is responsible for these submodules to avoid any initialization of the interface- and port-submodules
 * during startup.
 *
 * @paragraph pn_system_defined_submodules_customer_make Compile time
 *
 * During compilation time NUMBER_SYSTEM_DEFINED_SUBMODULES has to be set to zero. This can be done via compiler command line
 * option or in configuration file cfg_cc.h in the target platform folder (see also SDAI-Porting-Manual.chm for information).
 *
 * @paragraph pn_system_defined_submodules_customer_init Initialization
 *
 * The initialization of SDAI has to follow exactly the procedure as it is described in sdai_init(). The
 * flags #SDAI_APPL_HANDLES_INTERFACE_AND_PORTS and #SDAI_DYNAMIC_IO_CONFIGURATION have to be assigned to initialization @ref Flags.
 * This is required to pass the responsibility for coding of the Unit-Id used for sdai_plug_unit() and sdai_pull_unit() from
 * SDAI to the application.
 *
 * @paragraph pn_system_defined_submodules_customer_plug_sys Plugging of Interface- and Port-Submodules
 *
 * The use of interface- and port-submodules require the plugging of these submodules into the head-module. The head-module is represented
 * by slot 0. So an application has to calculate the Unit-Id for the interface-submodule with _SDAI_PN_SLOT_AND_SYSTEM_DEFINED_SUBSLOT_TO_ID(0, 0x8000).
 * The Unit-Id for port-submodules can be assigned comparable by using formula _SDAI_PN_SLOT_AND_SYSTEM_DEFINED_SUBSLOT_TO_ID(0, 0x80XX), where XX is
 * a value from 0x01 to 0x1F.@n@n
 * It is recommended to use the @ref UnitType #SDAI_UNIT_TYPE_GENERIC_INPUT for "System-Defined-Submodules with data length zero.@n@n
 * @note Currently there is a limitation in the PROFINET stack which supports upto 32 port-submodules. There could be gaps but the absolute
 * range is limited from 1 to 31.
 *
 * @paragraph pn_system_defined_submodules_customer_plug_io Plugging of Input-/Output-Submodules
 *
 * The use of standard Input-/Output-Submodules does not change.
 *
 * @paragraph pn_system_defined_submodules_customer_sample Sample Code
 *
 * Sample code for in initialization:@n
 * @code{.c}
 *
 * ...
 *
 * InitData.Info.Flags                      = (SDAI_DYNAMIC_IO_CONFIGURATION | SDAI_DATA_CONSISTENCY_IO_IMAGE);
 * InitData.Info.Flags                     |= SDAI_APPL_HANDLES_INTERFACE_AND_PORTS;
 *
 * InitData.Info.VendorID                   = PN_VENDOR_ID_SOFTING;
 * InitData.Info.UseAs.Pn.SoftwareRevision  = PN_SOFTWARE_REVISION;
 * InitData.Info.UseAs.Pn.HardwareRevision  = PN_HARDWARE_REVISION;
 *
 * ...
 *
 * Result = sdai_init (&InitData));
 *
 * ...
 *
 * @endcode
 *
 * Sample code for in plug units:@n
 * @code{.c}
 *
 * ...
 *
 * // plug first the head module into slot 0 and subslot 1
 * profinet_add_unit (Unit, SDAI_UNIT_TYPE_HEAD, 0, 0, 0x00000200, 0x00000000,
 *                    _SDAI_PN_SLOT_AND_SUBSLOT_TO_ID(0, 1), SDAI_PN_UNIT_FLAG_SUPPORT_MODULE_SUBSTITUTION,
 *                    SDAI_PN_DEFAULT_PROFILE, SDAI_PERIPHERAL_OFFSET_DPRAM, SDAI_PERIPHERAL_OFFSET_DPRAM);
 *
 * ...
 *
 * Result = sdai_plug_unit_ext (Unit.Type, Unit.InputSize, Unit.OutputSize, &Unit.Id, &Unit.CfgData);
 *
 * ...
 *
 * // plug the interface submodule into slot 0 and subslot 0x8000
 * profinet_add_unit (Unit, SDAI_UNIT_TYPE_GENERIC_INPUT, 0, 0, 0x00000200, 0x00000001,
 *                    _SDAI_PN_SLOT_AND_SYSTEM_DEFINED_SUBSLOT_TO_ID(0, 0x8000), 0u,
 *                    SDAI_PN_DEFAULT_PROFILE, SDAI_PERIPHERAL_OFFSET_DPRAM, SDAI_PERIPHERAL_OFFSET_DPRAM);
 *
 * ...
 *
 * Result = sdai_plug_unit_ext (Unit.Type, Unit.InputSize, Unit.OutputSize, &Unit.Id, &Unit.CfgData);
 *
 * ...
 *
 * // plug the port-1 submodule into slot 0 and subslot 0x8001
 * profinet_add_unit (Unit, SDAI_UNIT_TYPE_GENERIC_INPUT, 0, 0, 0x00000200, 0x00000002,
 *                    _SDAI_PN_SLOT_AND_SYSTEM_DEFINED_SUBSLOT_TO_ID(0, 0x8001), 0u,
 *                    SDAI_PN_DEFAULT_PROFILE, SDAI_PERIPHERAL_OFFSET_DPRAM, SDAI_PERIPHERAL_OFFSET_DPRAM);
 *
 * ...
 *
 * Result = sdai_plug_unit_ext (Unit.Type, Unit.InputSize, Unit.OutputSize, &Unit.Id, &Unit.CfgData);
 *
 * ...
 *
 * // plug the port-2 submodule into slot 0 and subslot 0x8002
 * profinet_add_unit (Unit, SDAI_UNIT_TYPE_GENERIC_INPUT, 0, 0, 0x00000200, 0x00000003,
 *                    _SDAI_PN_SLOT_AND_SYSTEM_DEFINED_SUBSLOT_TO_ID(0, 0x8002), 0u,
 *                    SDAI_PN_DEFAULT_PROFILE, SDAI_PERIPHERAL_OFFSET_DPRAM, SDAI_PERIPHERAL_OFFSET_DPRAM);
 *
 * ...
 *
 * Result = sdai_plug_unit_ext (Unit.Type, Unit.InputSize, Unit.OutputSize, &Unit.Id, &Unit.CfgData);
 *
 * ...
 *
 * @endcode
 */

/**
 *
 * @page pn_connect_samples Example for typical connection establishment in PROFINET IO
 *
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI", "PROFINET Controller";
 * "Application"=>"SDAI"    [ label = "sdai_init(SDAI_DYNAMIC_IO_CONFIGURATION)", URL="\ref sdai_init()" ];
 * "Application"=>"SDAI"    [ label = "sdai_plug_unit(X)", URL="\ref sdai_plug_unit()" ];
 * ...;
 * "Application"=>"SDAI"    [ label = "sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)", URL="\ref sdai_plug_unit()"];
 *
 * ---  [ label = "Stack online" ];
 * ...;
 * ---  [ label = "1. DCP services to detect device and set ident data" ];
 * "PROFINET Controller"=>"SDAI"           [ label = "IdentReq" ];
 * "SDAI"=>"PROFINET Controller"           [ label = "IdentRes" ];
 * "PROFINET Controller"=>"SDAI"           [ label = "DcpSetReq" ];
 * "SDAI"=>>"Application"                  [ label = "IdentDataCbk(SDAI_IDENT_DATA)" ];
 * "SDAI"=>"PROFINET Controller"           [ label = "DcpSetRes" ];
 * ---  [ label = "2. Start of connection establishment" ];
 * "PROFINET Controller"=>"SDAI"           [ label = "ConnectReq" ];
 * "SDAI"=>>"Application"                  [ label = "ControlReqCbk(SDAI_CONTROL_CFG_DATA_INFO) 1" ];
 * "Application"=>"SDAI"                   [ label = "sdai_control_response(SDAI_SERVICE_SUCCESS)", URL="\ref sdai_control_response()" ];
 * ...;
 * "SDAI"=>>"Application"                  [ label = "ControlReqCbk(SDAI_CONTROL_CFG_DATA_INFO) n" ];
 * "Application"=>"SDAI"                   [ label = "sdai_control_response(SDAI_SERVICE_SUCCESS)", URL="\ref sdai_control_response()" ];
 * "SDAI"=>"PROFINET Controller"           [ label = "ConnectRes" ];
 * ---  [ label = "3. Parameterization of device" ];
 * "PROFINET Controller"=>"SDAI"           [ label = "WriteReq 1" ];
 * "SDAI"=>>"Application"                  [ label = "WriteReqCbk(SDAI_WRITE_IND)" ];
 * "Application"=>"SDAI"                   [ label = "sdai_write_response(SDAI_SERVICE_SUCCESS)", URL="\ref sdai_write_response()" ];
 * "SDAI"=>"PROFINET Controller"           [ label = "WriteRes" ];
 * ...;
 * "PROFINET Controller"=>"SDAI"           [ label = "WriteReq n" ];
 * "SDAI"=>>"Application"                  [ label = "WriteReqCbk(SDAI_WRITE_IND)" ];
 * "Application"=>"SDAI"                   [ label = "sdai_write_response(SDAI_SERVICE_SUCCESS)", URL="\ref sdai_write_response()" ];
 * "SDAI"=>"PROFINET Controller"           [ label = "WriteRes" ];
 * ---  [ label = "4. End of connection establishment" ];
 * "PROFINET Controller"=>"SDAI"           [ label = "ControlReq(PRM_END)" ];
 * "SDAI"=>>"Application"                  [ label = "ControlReqCbk(SDAI_PN_CONTROL_PARAM_END)" ];
 * "SDAI"=>"PROFINET Controller"           [ label = "ControlRes(PRM_END)" ];
 * ...;
 * "Application"=>"SDAI"                   [ label = "sdai_control_response(SDAI_SERVICE_SUCCESS)", URL="\ref sdai_control_response()" ];
 * "SDAI"=>"PROFINET Controller"           [ label = "ControlReq(APPL_READY)" ];
 * "PROFINET Controller"=>"SDAI"           [ label = "ControlRes(APPL_READY)" ];
 * @endmsc
 *
 */

/**
 *
 * @page pn_dynamic_samples Examples for Dynamic I/O Configuration in PROFINET IO
 *
 * <B> The sequence chart shows an Example how to adapt to the IO configuration requested by a PROFINET controller during connection establishment.
 * The configuration consists of 3 modules (1 matching and 2 wrong). </B>
 *
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI", "PROFINET Controller";
 * "Application"=>"SDAI"    [ label = "sdai_init(SDAI_DYNAMIC_IO_CONFIGURATION)", URL="\ref sdai_init()" ];
 * "Application"=>"SDAI"    [ label = "sdai_plug_unit(X)", URL="\ref sdai_plug_unit()" ];
 * ...;
 * "Application"=>"SDAI"    [ label = "sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)", URL="\ref sdai_plug_unit()"];
 *
 * ---  [ label = "Stack online" ];
 * ...;
 * "PROFINET Controller"=>"SDAI"           [ label = "ConnectReq from Controller with expected modules" ];
 * "SDAI"=>>"Application"                  [ label = "ControlReqCbk(SDAI_CONTROL_CFG_DATA_INFO) for 1. module" ];
 * "Application" abox "Application"        [ label = "Expected and configured module match (Is is allowed to call sdai_plug_unit() with the same configuration)"];
 * "Application"=>"SDAI"                   [ label = "sdai_control_response(SDAI_SERVICE_SUCCESS)", URL="\ref sdai_control_response()" ];
 * "SDAI"=>>"Application"                  [ label = "ControlReqCbk(SDAI_CONTROL_CFG_DATA_INFO) for 2. module" ];
 * "Application" abox "Application"        [ label = "Module is wrong. The application replace it with the correct module"];
 * "Application"=>"SDAI"                   [ label = "sdai_plug_unit(new config)", URL="\ref sdai_plug_unit()" ];
 * "Application"=>"SDAI"                   [ label = "sdai_control_response(SDAI_SERVICE_SUCCESS)", URL="\ref sdai_control_response()" ];
 * "SDAI"=>>"Application"                  [ label = "ControlReqCbk(SDAI_CONTROL_CFG_DATA_INFO) for 3. module" ];
 * "Application" abox "Application"        [ label = "Module is wrong. The application does not replace the module"];
 * "Application"=>"SDAI"                   [ label = "sdai_control_response(SDAI_SERVICE_SUCCESS)", URL="\ref sdai_control_response()" ];
 * "SDAI"=>"PROFINET Controller"           [ label = "ConnectRes with ModulDiffBlock for 3. module" ];
 * @endmsc
 *
 * <B> The sequence chart shows an Example how to adapt the IO configuration for a module that is part of an active AR. </B>
 *
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI", "PROFINET Controller";
 * "Application"=>"SDAI"    [ label = "sdai_init(SDAI_DYNAMIC_IO_CONFIGURATION)", URL="\ref sdai_init()" ];
 * "Application"=>"SDAI"    [ label = "sdai_plug_unit(X)", URL="\ref sdai_plug_unit()" ];
 * ...;
 * "Application"=>"SDAI"    [ label = "sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)", URL="\ref sdai_plug_unit()"];
 *
 * ---  [ label = "Stack online" ];
 * ...;
 * ---  [ label = "Connection establishment" ];
 * ...;
 * "Application"=>"SDAI"                   [ label = "sdai_pull_unit(X)", URL="\ref sdai_pull_unit()" ];
 * "SDAI" abox "SDAI"                      [ label = "remove modul from list of available modules. Delete all diagnosis entries for this module"];
 * "SDAI"=>"PROFINET Controller"           [ label = "send PULL alarm" ];
 * ...;
 * "Application"=>"SDAI"                   [ label = "sdai_plug_unit(correct)", URL="\ref sdai_plug_unit()" ];
 * "SDAI" abox "SDAI"                      [ label = "add module to list of modules"];
 * "SDAI"=>"PROFINET Controller"           [ label = "send PLUG alarm" ];
 * ...;
 * "Application"=>"SDAI"                   [ label = "sdai_plug_unit(wrong)", URL="\ref sdai_plug_unit()" ];
 * "SDAI" abox "SDAI"                      [ label = "add module to list of modules"];
 * "SDAI"=>"PROFINET Controller"           [ label = "send PLUG_WRONG_SUBMODULE alarm" ];
 * @endmsc
 *
 */

#ifndef __SDAI_PN_H__
#define __SDAI_PN_H__

/******************************************************************************
DEFINES
******************************************************************************/

/** \anchor PnUnitLimits @name PROFINET IO Unit Limits
* Definitions for ROFINET IO unit limits */
//@{
#define SDAI_PN_MAX_UNIT_IO_DATA_SIZE               254  /**< Maximum data size in bytes of one PROFINET IO unit. This is limited to 254 byte
                                                              due to interoperability issues with some controllers and for efficient handling
                                                              of the IO data and status if the performance optimization is used */
#define SDAI_PN_INVALID_UNIT_IO_DATA_SIZE           255  /**< Marks the received Unit size as Invalid */
//@}

/** \anchor PnUnitId @name ROFINET IO Unit ID macros
* Format of the SDAI Unit ID for PROFINET IO :
\verbatim
   ---------------------------------------------------------------
   |          PN Subslot         |             PN Slot           |
   ---------------------------------------------------------------
   32         High Word          16           Low Word           0
\endverbatim
*/
//@{
#define _SDAI_PN_SLOT_AND_SUBSLOT_TO_ID(Slot, SubSlot) \
  ((U32) ((((U32) (SubSlot)) << 16) | (U32) (Slot & 0x00007FFFuL)))  /**< Converts the PN slot and subslot to the SDAI Id */

#define _SDAI_PN_SLOT_AND_SYSTEM_DEFINED_SUBSLOT_TO_ID(Slot, SubSlot) \
  ((U32) ((((U32) (SubSlot)) << 16) | (U32) (Slot & 0x00008FFFuL)))  /**< Converts the PN slot and system defined subslot to the SDAI Id, use carefull because this is only allowed for interface and port submodule  */

#define _SDAI_PN_ID_TO_SUBSLOT(Type, Id) \
  ((Type) ((Id >> 16) & 0x00007FFFuL)) /**< Converts the SDAI Id to the PN Subslot */

#define _SDAI_PN_ID_TO_SYSTEM_DEFINED_SUBSLOT(Type, Id) \
  ((Type) ((Id >> 16) & 0x00008FFFuL)) /**< Converts the SDAI Id to the PN system defined Subslot, use carefull because this is only allowed for interface and port submodule */

#define _SDAI_PN_ID_TO_SLOT(Type, Id) \
  ((Type) ((Id) & 0x00007FFFuL)) /**< Converts the SDAI Id to the PN Slot */
//@}

/** \anchor PnDataStatus @name PROFINET IO Data Status
* Definitions for PROFINET IO unit data states */
//@{
#define SDAI_PN_DATA_STATUS_INVALID_BY_SUBSLOT      0x00 /**< data status for invalid data detected by subslot */
#define SDAI_PN_DATA_STATUS_INVALID_BY_SLOT         0x20 /**< data status for invalid data detected by slot */
#define SDAI_PN_DATA_STATUS_INVALID_BY_DEVICE       0x40 /**< data status for invalid data detected by IO device */
#define SDAI_PN_DATA_STATUS_INVALID_BY_CONTROLLER   0x60 /**< data status for invalid data detected by IO controller */
//@}

/** \anchor StorePermanent @name PROFINET IO Network Parameter Storage
 *  PROFINET IO specific definitions for storing network specific parameter */
//@{
#define SDAI_STORE_IP_PERMANENT   0x01   /**< Indicates that the received IP settings must be stored permanent */
#define SDAI_STORE_NAME_PERMANENT 0x02   /**< Indicates that the received name must be stored permanent */
//@}

/** \anchor RedundancyState @name PROFINET IO Redundancy States
 *  PROFINET IO specific definitions for redundancy states */
//@{
#define SDAI_PN_REDUNDANCY_ACTIVE    0x00          /**< The redundancy mechanism is active */
#define SDAI_PN_REDUNDANCY_INACTIVE  0x01          /**< The redundancy mechanism is inactive */
//@}

/** \anchor PnUnitFlags @name PROFINET IO Unit Flags
 * PROFINET IO specific unit flags */
//@{
#define SDAI_PN_UNIT_FLAG_ISSAFETY                         0x01       /**< Unit is safty module */
#define SDAI_PN_UNIT_FLAG_SUPPORT_MODULE_SUBSTITUTION      0x02       /**< Unit can be substituteted by controller */
#define SDAI_PN_UNIT_HEAD_FLAG_FULL_IM_SUPPORT             0x04       /**< Only valid for the Head Module. All I&M requests are passed to the Application */
#define SDAI_PN_UNIT_FLAG_SUPPORT_DATA_SUBSTITUTION        0x08       /**< Unit supports setting of Safe State behaviour. See \ref pn_data_substitute */
//@}

/** \anchor PnProfileId @name PROFINET IO Profile Id
 * PROFINET IO specific number of used application profile (API) */
//@{
#define SDAI_PN_DEFAULT_PROFILE                           0x00u       /**< Default API (no profile) (0x00000000) */
#define SDAI_PN_DRIVE_PROFILE                             0x01u       /**< PROFIdrive - Profile Drive Technology (0x00003A00) */
#define SDAI_PN_ENCODER_PROFILE                           0x02u       /**< Encoder API (0x00003D00) */
#define SDAI_PN_INTELLIGENT_PUMP_PROFILE                  0x03u       /**< Intelligent pump profile (0x00005D00) */
//@}

/** \anchor PnControlCode @name PROFINET IO Control Codes
 * PROFINET IO specific definitions for Control Codes. */
//@{
#define SDAI_PN_CONTROL_BLINK                     0x0100  /**< Controller requests the device to blink */
#define SDAI_PN_CONTROL_PARAM_END                 0x0200  /**< Information about received parameter end control from controller. The control response triggers the
                                                               application ready indication */
#define SDAI_PN_CONTROL_RESET_APPLICATION_DATA    0x0300  /**< For future use. Currently not supported. */
#define SDAI_PN_CONTROL_RESET_COMMUNICATION_DATA  0x0400  /**< Reset communication parameter */
#define SDAI_PN_CONTROL_RESET_ENGINEERING_DATA    0x0500  /**< For future use. Currently not supported. */
#define SDAI_PN_CONTROL_RESET_RESET_AND_RESTORE   0x0600  /**< For future use. Currently not supported. */
//@}

/** \anchor PnStatus @name PROFINET IO Stack Status
 * PROFINET IO specific definitions for Stack Status */
//@{
#define SDAI_PN_STATE_OK                       0x01    /**< The state of the PROFINET IO Protocol Software is faultless */
#define SDAI_PN_STATE_NO_PARAMETER             0x02    /**< The PROFINET IO Protocol Software has not been configured */
#define SDAI_PN_STATE_ETH_FAILURE              0x03    /**< Ethernet controller is not existing or not working properly */
#define SDAI_PN_STATE_UDP_FAILURE              0x04    /**< The UDP stack has reported an internal unrecoverable error */
#define SDAI_PN_STATE_FAILURE                  0x05    /**< General error code */
//@}

/** \anchor PNAlarmType @name PROFINET IO Alarm Types
 * PROFINET IO specific Alarm types
 * the pull/plug alarm are not longer available due support of dynamic
 * module assignment. These are handled now inside of the SDAI-Mapping application.
 * SDAI_PN_ALARM_PULL                          0x0003u
 * SDAI_PN_ALARM_PLUG                          0x0004u
 */
//@{
#define SDAI_PN_ALARM_DIAGNOSIS                     0x0001u   /**< Signals an event within a module */
#define SDAI_PN_ALARM_PROCESS                       0x0002u   /**< Signals an event in the connected process */
#define SDAI_PN_ALARM_STATUS                        0x0005u   /**< Signals the change of the state of a module */
#define SDAI_PN_ALARM_UPDATE                        0x0006u   /**< Signals the change of a parameter in a module */
#define SDAI_PN_ALARM_RETURN_SUBMODULE              0x000Bu   /**< Signals the change of the status of a module from invalid to valid (see \ref pn_data_status)*/

#define SDAI_PN_ALARM_USER_STRUCTURE_IDENT_CHANNEL_DIAG                0x8000u
#define SDAI_PN_ALARM_USER_STRUCTURE_IDENT_EXT_CHANNEL_DIAG            0x8002u
#define SDAI_PN_ALARM_USER_STRUCTURE_IDENT_QUAL_CHANNEL_DIAG           0x8003u
//@}

/** \anchor PNDiagType @name PROFINET Diagnosis Types
 * PROFINET IO specific Diagnosis types currently not defined*/
//@{
//@}

/******************************************************************************
STRUCTURES
******************************************************************************/

/** \brief PROFINET IO network specific parameter */
struct SDAI_PN_IDENT_DATA
{
  U8   Address [4];          /**< The IP-Address of the Device. The user may set #SDAI_IP_UNUSED if not available. */
  U8   Netmask [4];          /**< The Netmask of the Device. The user may set #SDAI_NETMASK_UNUSED if not available. */
  U8   Gateway [4];          /**< The Gateway of the Device. The user may set #SDAI_GATEWAY_UNUSED if not available. */

  U8   MacAddressDevice [6]; /**< The MAC address of the device. Unused by the IdentDataCbk callback */
  U8   MacAddressPort1 [6];  /**< The MAC address of Port 1 (only used if device has more than one Port). Unused by the IdentDataCbk callback */
  U8   MacAddressPort2 [6];  /**< The MAC address of Port 2 (only used if device has more than one Port). Unused by the IdentDataCbk callback */

  U8   StorePermanent;       /**< Indicates if the parameters received by the IdentDataCbk must be stored permanent. Unused when calling sdai_init(). Valid values for StorePermanent: \ref StorePermanent  */
  U8   Alignment;
};

/*---------------------------------------------------------------------------*/

/** \brief PROFINET IO device specific parameter */
struct SDAI_PN_DEVICE_DATA
{
  U32   SoftwareRevision;         /**< The software version of the device */
  U16   HardwareRevision;         /**< The hardware version of the device */
  U16   RevisionCounter;          /**< The revision counter of the device */
  U8    RedundancyState;          /**< Set the state of the redundancy. Valid values for the RedundancyState: \ref RedundancyState */
  U8    Alignment [3];
};

/*---------------------------------------------------------------------------*/

/** \brief PROFINET IO specific address description */
struct SDAI_PN_ADDR_DESCR
{
  U32   Id;                       /**< The Id of the unit the request belongs to */
  U16   Index;                    /**< The Index of the data the request belongs to */
  U8    Alignment [2];
};

/*---------------------------------------------------------------------------*/

/** \brief This structure holds the PROFINET IO specific configuration data */
struct SDAI_PN_CFG_DATA
{
  U32   IdentNumber;              /**< The module identnumber referred in the GSDML */
  U32   SubmoduleIdentNumber;     /**< The submodule identnumber referred in the GSDML */
  U8    Flags;                    /**< Additional Informations. Valid values for Flags: \ref PnUnitFlags */
  U8    ProfileId;                /**< Id of the used PROFINET profile for this configuration data: \ref PnProfileId */
  U8    Alignment [2];
};

/*---------------------------------------------------------------------------*/

/** \brief This structure describes the structure of the PROFINET Channel Diagnosis data.
           For a detailed description of the structure parameters see the specification IEC61158 Part5/6. */
struct SDAI_PN_PROFINET_CHANNEL_DIAGNOSIS
{
  U16   UserStructureIdent;
  U16   ChannelNumber;
  U16   ChannelProperties;   /**< This parameter is handled completely by the stack */
  U16   ChannelError;
};

/*---------------------------------------------------------------------------*/

/** \brief This structure describes the structure of the PROFINET IO Ext Channel Diagnosis data.
           For a detailed description of the structure parameters see the specification IEC61158 Part5/6. */
struct SDAI_PN_PROFINET_EXT_CHANNEL_DIAGNOSIS
{
  U16   UserStructureIdent;
  U16   ChannelNumber;
  U16   ChannelProperties;   /**< This parameter is handled completely by the stack */
  U16   ChannelError;
  U16   ExtChannelError;
  U8    Alignment[2];
  U32   ExtChannelAddValue;
};

/*---------------------------------------------------------------------------*/

/** \brief This structure describes the structure of the PROFINET IO Qualified Channel Diagnosis data.
           For a detailed description of the structure parameters see the specification IEC61158 Part5/6. */
struct SDAI_PN_PROFINET_QUALIFIED_CHANNEL_DIAGNOSIS
{
  U16   UserStructureIdent;
  U16   ChannelNumber;
  U16   ChannelProperties;   /**< This parameter is handled completely by the stack */
  U16   ChannelError;
  U16   ExtChannelError;
  U8    Alignment[2];
  U32   ExtChannelAddValue;
  U32   Qualifier;
};

/*---------------------------------------------------------------------------*/

/** \brief This structure holds the cycle time for one unit. */
/** The values are only valid if the there is a connection to the controller */
struct SDAI_PN_UNIT_DATA
{
  U32   Id;         /**< The Id of the unit the cycletime belongs to. */
  U32   CylceTime;  /**< The cycle time of the unit as a multiple of 31,25 us. A value of 0 indicates that there is no connection to the controller for this unit. */
};

/** \brief This structure holds the PROFINET IO specific additional status informations. */
struct SDAI_PN_ADDSTATUS_DATA
{
  U8                            ProtocolState;               /**< Indicates the last error detected by the PROFINET IO stack. Valid values for the ProtocolState: \ref PnStatus */
  U8                            RedundancyState;             /**< Media redundancy state. Valid values for the RedundancyState: \ref RedundancyState */
  U8                            Alignment[2];
  struct SDAI_PN_UNIT_DATA      UnitData [SDAI_MAX_UNITS];   /**< Array holding informations of the units in the order the units are plugged. */
};

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

#endif /* __SDAI_PN_H__ */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
