/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

/**
 *
 * @page ecat EtherCAT
 *
 * \tableofcontents
 *
 * @section ecat_overview Overview
 * EtherCAT is a high performance Ethernet-based fieldbus system. EtherCAT was
 * designed for automation-based applications which require short cycle times and
 * a precise synchronization. The Softing EtherCAT Slave Protocol Software realizes
 * a small and portable protocol stack for building EtherCAT applications.
 *
 * The following figure shows the structure of Softings EtherCAT slave implementation.
 * The Simple Device Application Interface (SDAI) provides the services to communicate with
 * the EtherCAT Slave Protocol Software. The protocol stack implements the application layer
 * and accesses the EtherCAT slave controller (ESC) via a Dual-Ported RAM interface. The
 * ESC implements the EtherCAT Data Link Layer und provides a interface to the EtherCAT Fieldbus.
 * As described in chapter \ref sdai_portability the SDAI application can run on the same
 * or on a separate processor.
 *
 * @image html EtherCAT_Structure.jpg
 *
 * @subsection ecat_features Features
 * - Full Slave Device (Data Link Layer) with two Ethernet ports
 * - 4 Sync Managers
 * - 3 FMMUs
 * - EtherCAT state machine supported
 * - Distributed Clocks supported
 * - Generation of synchronous output signals (SYNC0/1)
 * - Ethernet over EtherCAT (EoE) supported, used for devices with TCP/IP stack (e.g. Web Server)
 * - CANopen over EtherCAT (CoE) for acyclic communication
 * - File access over EtherCAT (FoE) to upload/download file
 * - Object dictionary with standard CoE objects already implemented
 * - Integration of application objects possible via SDAI
 * - SDO Upload/Download and SDO information services supported
 * - Complete SDO access supported
 * - Support for multiple PDOs (customizable)
 * - PDI and Process Data watchdog supported
 * - Writable PDO Mapping and Assignment Objects
 * - EtherCAT Emergencies, Error Register and Diagnosis history object
 * - Station Alias and ID Selector
 * - Demo Application with a dedicated device description file aimed at quickly getting you started with
 *   implementing your own EtherCAT slave application
 *
 * @subsection ecat_objects Supported Objects
 * The following standard CoE objects are already supported by the EtherCAT Slave Protocol Software:
 *
 * <TABLE><TR>
 * <TH>Object</TH><TH>Index</TH><TH>Description</TH>
 * </TR><TR>
 * <TD>Device Type</TD><TD>0x1000</TD><TD>Contains the CoE profile information</TD>
 * </TR><TR>
 * <TD>Error Register</TD><TD>0x1001</TD><TD>Shows if there is a diagnosis active</TD>
 * </TR><TR>
 * <TD>Manufacturer Device Name</TD><TD>0x1008</TD><TD>Name of the device as visible string</TD>
 * </TR><TR>
 * <TD>Manufacturer Hardware Version</TD><TD>0x1009</TD><TD>Hardware Version as visible string</TD>
 * </TR><TR>
 * <TD>Manufacturer Software Version</TD><TD>0x100A</TD><TD>Software Version as visible string</TD>
 * </TR><TR>
 * <TD>Identity</TD><TD>0x1018</TD><TD>Manages the identity data that identifies the device on a EtherCAT network</TD>
 * </TR><TR>
 * <TD>Device Identification Reload Object</TD><TD>0x10E0</TD><TD>Manages the Station Alias and ID Selector</TD>
 * </TR><TR>
 * <TD>Diagnosis History</TD><TD>0x10F3</TD><TD>Holds the diagnosis history</TD>
 * </TR><TR>
 * <TD>RxPDO Mapping 1<br>...<br>RxPDO Mapping 32</TD><TD>0x1600<br>...<br>0x161F</TD><TD>Describes the mapping of application objects to receive PDOs (output data)</TD>
 * </TR><TR>
 * <TD>TxPDO Mapping 1<br>...<br>TxPDO Mapping 32</TD><TD>0x1A00<br>...<br>0x1A1F</TD><TD>Describes the mapping of application objects to transmit PDOs (input data)</TD>
 * </TR><TR>
 * <TD>Sync Manager Communication Type</TD><TD>0x1C00</TD><TD>Describes the usage of the Sync Manager channels</TD>
 * </TR><TR>
 * <TD>Sync Manager 2 PDO Assignment</TD><TD>0x1C12</TD><TD>Defines the Output Data layout by specifying which RxPDOs will be used</TD>
 * </TR><TR>
 * <TD>Sync Manager 3 PDO Assignment</TD><TD>0x1C13</TD><TD>Defines the Input Data layout by specifying which TxPDOs will be used</TD>
 * </TR></TABLE>
 *
 * @section ecat_mapping Mapping EtherCAT to SDAI
 * In the following chapters the mapping of the EtherCAT Slave Protocol Software to the SDAI is described.
 *
 * @subsection ecat_init_data EtherCAT Initialization
 * The structure #SDAI_INIT contains identification data, device specific data and a set of application specific
 * callback functions. The identification data are described by the structure #SDAI_IDENT_DATA and device specific
 * data are described by the structure #SDAI_DEVICE_DATA. These parameters are mapped to the standard EtherCAT
 * objects (see also \ref ecat_objects) in the following way:
 *
 * <TABLE><TR>
 * <TH>SDAI</TH><TH>EtherCAT Object</TH><TH>Description</TH>
 * </TR><TR>
 * <TD>DevName</TD><TD>Manufacturer Device Name</TD><TD></TD>
 * </TR><TR>
 * <TD>IfName</TD><TD>---</TD><TD>Ignored (not used by EtherCAT)</TD>
 * </TR><TR>
 * <TD>VendorID</TD><TD>Identity (SubIdx: 1)</TD><TD></TD>
 * </TR><TR>
 * <TD>ProductCode</TD><TD>Identity (SubIdx: 2)</TD><TD></TD>
 * </TR><TR>
 * <TD>MinorRevisionNumber</TD><TD>Identity (SubIdx: 3)</TD><TD>Copied to Bit 0-15 of the Revision Number (SubIdx 3)</TD>
 * </TR><TR>
 * <TD>MajorRevisionNumber</TD><TD>Identity (SubIdx: 3)</TD><TD>Copied to Bit 16-31 of the Revision Number (SubIdx 3)</TD>
 * </TR><TR>
 * <TD>SerialNumber</TD><TD>Identity (SubIdx: 4)</TD><TD></TD>
 * </TR><TR>
 * <TD>Type</TD><TD>Device Type</TD><TD>Bit 0-15: specifies the device profile (0 if no profile is used)<br>
                                        Bit 16-31: additional profile specific information</TD>
 * </TR><TR>
 * <TD>SoftwareVersion</TD><TD>Manufacturer SW Version</TD><TD></TD>
 * </TR><TR>
 * <TD>HardwareVersion</TD><TD>Manufacturer HW Version</TD><TD></TD>
 * </TR><TR>
 * <TD>SyncFlags</TD><TD>---</TD><TD>Not mapped to a CoE object</TD>
 * </TR><TR>
 * <TD>ProductName</TD><TD>---</TD><TD>Ignored (not used by EtherCAT)</TD>
 * </TR><TR>
 * <TD>OrderId</TD><TD>---</TD><TD>Ignored (not used by EtherCAT)</TD>
 * </TR></TABLE>
 *
 * The structure #SDAI_CALLBACKS is used to register all supported application specific callback functions.
 * The following table shows which SDAI callback functions are used by EtherCAT. If not supported by the
 * application the user shall set the respective function pointer to NULL.
 *
 * <TABLE><TR>
 * <TH>SDAI Callback</TH><TH>EtherCAT</TH>
 * </TR><TR>
 * <TD>IdentDataCbk()</TD><TD>Not used (set function pointer always to NULL)</TD>
 * </TR><TR>
 * <TD>ExceptionCbk()</TD><TD>Called when a fatal error occurred</TD>
 * </TR><TR>
 * <TD>DataCbk()</TD><TD>Called when the output data or status of a unit has been changed</TD>
 * </TR><TR>
 * <TD>AlarmAckCbk()</TD><TD>Not used (set function pointer always to NULL)</TD>
 * </TR><TR>
 * <TD>WriteReqCbk()</TD><TD>Called when a SDO service / FoE file write request is received</TD>
 * </TR><TR>
 * <TD>ReadReqCbk()</TD><TD>Called when a SDO service / FoE file read request is received</TD>
 * </TR><TR>
 * <TD>ControlReqCbk()</TD><TD>Called when a file should be opened/closed over FoE</TD>
 * </TR><TR>
 * <TD>SyncSignalCbk()</TD><TD>Called when a synchronization signal is generated</TD>
 * </TR></TABLE>
 *
 * \note To allow a precise synchronization the SyncSignalCbk() is called directly from ISR context.
 *       Depending on the used RTOS special limitations exist, e.g. some RTOS functions can not be called.
 *
 * @subsection ecat_plug_unit EtherCAT plugging of units
 * In case of EtherCAT a SDAI unit is equivalent to a Process Data Object (PDO). A PDO contains data attributes
 * of one or more application objects which will be transmitted cyclically as a single data unit. The mapping of
 * application objects to PDOs is specified via PDO mapping objects. A PDO mapping object is identified with the
 * CoE object directory index. This object index has to be specified by the user within the structure #SDAI_ECAT_CFG_DATA.
 * Optionally the user can pass a description of the mapped application object(s). These informations must match
 * to the corresponding entries in the EtherCAT device description file. The EtherCAT protocol stack need these
 * informations to instantiate and initialize the corresponding PDO Mapping object and the Sync Manager 2/3 PDO
 * Assignment object (see also \ref ecat_objects). The following figure shows an example for the mapping of SDAI
 * I/O Units to EtherCAT PDOs.
 *
 * @image html ethercat_data_mapping.jpg
 *
 * By calling sdai_plug_unit() or sdai_plug_unit_ext() a PDO is automatically instantiated and added to the object dictionary. This function
 * returns a Unit ID which consists of the EtherCAT index number and the SDAI unit index. The high word of the Unit ID
 * is the SDAI unit index. The low word of the Unit ID is the index number of the PDO Mapping object. See also \ref EcatUnitId.
 *
 * @subsection ecat_io_cfg_dynamic EtherCAT Dynamic IO Configuration
 *
 * Both types of Dynamic IO Configuration
 * - \ref io_cfg_variable
 * - \ref io_cfg_selectable
 *
 * are supported by EtherCAT. In case of changing the data layout of IO module(s) the respective PDO mapping object
 * is accessed by the EtherCAT Master via SDO Download to write the new mapping. Afterwards the application is informed
 * about the new process data layout via a Control indication with the control code #SDAI_CONTROL_CODE_CFG_DATA_INFO. The
 * application can either accept the new configuration or reject it. The second type of Dynamic IO Configuration allows the
 * master to select a subset of the PDOs supported by the application. The master accesses the PDO Assignment Objects to
 * configure the requested PDOs. This is handled completely by the SDAI.
 * It is also allowed to the application to adapt the whole process data configuration during runtime.
 * This gives the flexibility to change the PDO mapping and the PDO assignment objects. The application uses sdai_pull_unit()
 * and sdai_plug_unit() or sdai_plug_unit_ext() functions to handle the adaptation.
 * The allowed changes of the IO Configuration and the supported IO Configuration have to be described in the device
 * description file.
 *
 * @subpage ecat_dynamic
 *
 * @subsection ecat_acyclic EtherCAT Acyclic Communication
 * Acyclic communication is realized by EtherCAT with the application layer protocol CANopen over EtherCAT (CoE).
 * CoE provides the services to access a CANopen object dictionary and its objects. A object is addressed by Index and
 * Subindex. Standard objects which must be supported by every EtherCAT slave are already implemented by the EtherCAT
 * Slave Protocol Software (see also \ref ecat_objects). The user may implement application-specific objects which will
 * be accessed via the SDAI callback functions. The following table shows the mapping of the CoE services to SDAI callback
 * functions:
 *
 * <TABLE><TR>
 * <TH>Service Name</TH><TH>SDAI Callback function</TH>
 * </TR><TR>
 * <TD>SDO Download</TD><TD>WriteReqCbk()</TD>
 * </TR><TR>
 * <TD>SDO Upload</TD><TD>ReadReqCbk()</TD>
 * </TR><TR>
 * <TD>Get OD List</TD><TD>ReadReqCbk()</TD>
 * </TR><TR>
 * <TD>Get Object Description</TD><TD>ReadReqCbk()</TD>
 * </TR><TR>
 * <TD>Get Entry Description</TD><TD>ReadReqCbk()</TD>
 * </TR></TABLE>
 *
 * See also \ref EcatServiceCodes for a description of the CoE services. Segmented service access for the services
 * SDO Download and SDO Upload is not supported. This limits the maximum size of an application object to the
 * maximum data length which can be transferred via the service interface. The size of the service interface
 * is 512 Byte. The maximum data length to be written is therefore limited to: 512 Byte - sizeof (struct SDAI_WRITE_IND).
 * The maximum data length to be read is limited to: 512 Byte - sizeof (struct SDAI_READ_RES). Segmented service
 * access is also not supported for the service Get OD List. This limits the maximum number of application objects
 * to #SDAI_ECAT_MAX_NR_LIST_ENTRIES.
 *
 * @subsection ecat_errors EtherCAT Service Error Codes
 * The following table shows how the SDAI common service error codes (see also \ref ServiceErrorCode) are mapped
 * to EtherCAT specific service error codes (SDO Abort Code). This error mapping is needed for Read and Write responses.
 *
 * <TABLE><TR>
 * <TH>SDAI Service Error Code</TH><TH>FoE Error Code</TH><TH>Comment</TH>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_SUCCESS</TD><TD>0x00000000</TD><TD>No error</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_OBJECT_NOT_EXISTING</TD><TD>0x06020000</TD><TD>Object does not exist in the object directory</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_INVALID_SIZE</TD><TD>0x06070010</TD><TD>Length of service parameter does not match</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_STATE_CONFLICT</TD><TD>0x08000022</TD><TD>Data cannot be transferred or stored to the application because of the present device state</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_APPLICATION</TD><TD>0x08000000</TD><TD>General error</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_RESOURCE</TD><TD>0x05040005</TD><TD>Out of memory</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_NOT_SUPPORTED</TD><TD>0x06010000</TD><TD>Unsupported access to an object (e.g. complete access is not supported)</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_ACCESS_DENIED</TD><TD>0x06010002</TD><TD>Attempt to write to a read only object</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_INVALID_PARAMETER</TD><TD>0x06090030</TD><TD>Value range of parameter exceeded</TD>
 * </TR></TABLE>
 *
 * @subsection ecat_foe EtherCAT FoE Communication
 * Reading or writing files is realized by EtherCAT with the application layer protocol File access over EtherCAT (FoE).
 * FoE provides the service to access a file in reading or writing mode. A file is addressed by its name which is
 * part of the file open operation. Furthermore the file open call specifies the access mode of the file (r/w) and
 * optionally a password based authentification mechanism. Closing a file is initiated by the protocol stack and
 * passes an error code to the application. In case of a written file stream, the files shall be deleted due an error
 * condition on the client side or transport layer. A file stream opened for reading can ignore the error code on close.
 *
 * <TABLE><TR>
 * <TH>Service Name</TH><TH>SDAI Callback function</TH>
 * </TR><TR>
 * <TD>File open</TD><TD>ControlReqCbk()</TD>
 * </TR><TR>
 * <TD>File close</TD><TD>ControlReqCbk()</TD>
 * </TR><TR>
 * <TD>File write</TD><TD>WriteReqCbk()</TD>
 * </TR><TR>
 * <TD>File read</TD><TD>ReadReqCbk()</TD>
 * </TR></TABLE>
 *
 * @subsection ecat_foe_errors EtherCAT FoE Service Error Codes
 * The following table shows how the SDAI common service error codes (see also \ref ServiceErrorCode) are mapped
 * to EtherCAT specific service error codes (FoE Error Code). This error mapping is needed for Control, Read and Write responses.
 *
 * <TABLE><TR>
 * <TH>SDAI Service Error Code</TH><TH>FoE Error Code</TH><TH>Comment</TH>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_SUCCESS</TD><TD>0x0000</TD><TD>No error</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_RESOURCE</TD><TD>0x8003</TD><TD>Disk full</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_DISK_FULL</TD><TD>0x8003</TD><TD>Disk full</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_NOT_SUPPORTED</TD><TD>0x8000</TD><TD>Not defined</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_ACCESS_DENIED</TD><TD>0x8002</TD><TD>Access denied</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_FILE_NOT_FOUND</TD><TD>0x8001</TD><TD>Not found</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_STATE_CONFLICT</TD><TD>0x8008</TD><TD>Bootstrap only (if not in Bootstrap mode)</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_STATE_CONFLICT</TD><TD>0x8009</TD><TD>Not Bootstrap (if in Bootstrap mode)</TD>
 * </TR><TR>
 * <TD>all others</TD><TD>0x8004</TD><TD>Illegal</TD>
 * </TR></TABLE>
 *
 * @subsection ecat_control_code EtherCAT Control Codes
 * If a control indication is signaled to the application via the ControlReqCbk() the parameter
 * ControlCode of the structure #SDAI_CONTROL_IND specifies the action which shall be performed.
 * None of the SDAI common control codes are currently supported for EtherCAT. The control code
 * #SDAI_ECAT_CONTROL_FILE_OPEN_REQ is used to open a file stream for reading/writing. The control
 * #SDAI_ECAT_CONTROL_FILE_CLOSE_REQ is used to close a file.
 *
 * @subsection ecat_diag_alarm EtherCAT Diagnosis and Alarms
 * Alarms are not supported by EtherCAT. Thus the function sdai_alarm_request() and the AlarmAckCbk() are not supported.
 * Diagnosis message handling is supported by using function sdai_diagnosis_request(). If the application creates a new
 * diagnosis message, it will always be added to the diagnosis history buffer inside of the EtherCAT stack. Also the error
 * register within the object dictionary is updated accordingly. Depending on the parameterization of the diagnosis history
 * object an additional emergency message is sent to the EtherCAT master. To enable this feature the engineering system has to
 * set Bit 0 of the flags object (subindex 5) to 1.
 * The following table shows how the SDAI common diagnosis types (see also \ref DiagType) are mapped
 * to EtherCAT specific emergency error codes. This error mapping is needed for correct diagnosis messages.
 *
 * <TABLE><TR>
 * <TH>SDAI Diagnosis Type</TH><TH>Emergency Error Code</TH><TH>Description</TH>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_SHORT_CIRCUIT</TD><TD>0x9001</TD><TD>external error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_INPUT_UNDERVOLTAGE</TD><TD>0x3001</TD><TD>voltage</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_INPUT_OVERVOLTAGE</TD><TD>0x3002</TD><TD>voltage</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OUTPUT_UNDERVOLTAGE</TD><TD>0x3301</TD><TD>output voltage</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OUTPUT_OVERVOLTAGE</TD><TD>0x3302</TD><TD>output voltage</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_DEVICE_UNDERVOLTAGE</TD><TD>0x3201</TD><TD>voltage inside the device</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_DEVICE_OVERVOLTAGE</TD><TD>0x3202</TD><TD>voltage inside the device</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_INPUT_UNDERCURRENT</TD><TD>0x2101</TD><TD>current, device input side</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_INPUT_OVERCURRENT</TD><TD>0x2102</TD><TD>current, device input side</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OUTPUT_UNDERCURRENT</TD><TD>0x2301</TD><TD>current, device output side</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OUTPUT_OVERCURRENT</TD><TD>0x2302</TD><TD>current, device output side</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_DEVICE_UNDERCURRENT</TD><TD>0x2201</TD><TD>current inside the device</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_DEVICE_OVERCURRENT</TD><TD>0x2202</TD><TD>current inside the device</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OVERLOAD</TD><TD>0x9002</TD><TD>external error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OVERTEMPERATURE</TD><TD>0x4201</TD><TD>device temperature</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_UNDERTEMPERATURE</TD><TD>0x4202</TD><TD>device temperature</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_AMBIENTE_OVERTEMPERATURE</TD><TD>0x4101</TD><TD>ambient temperature</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_AMBIENTE_UNDERTEMPERATURE</TD><TD>0x4102</TD><TD>ambient temperature</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_LINE_BREAK</TD><TD>0x9003</TD><TD>external error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_UPPER_LIMIT_EXCEEDED</TD><TD>0x9004</TD><TD>external error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_LOWER_LIMIT_EXCEEDED</TD><TD>0x9005</TD><TD>external error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_GENERIC</TD><TD>0x1001</TD><TD>generic error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_SIMULATION_ACTIVE</TD><TD>0x6201</TD><TD>device user software error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_PRM_MISSING</TD><TD>0x6202</TD><TD>device user software error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_PRM_FAULT</TD><TD>0x6203</TD><TD>device user software error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_POWER_SUPPLY_FAULT</TD><TD>0x9006</TD><TD>external error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_FUSE_BLOWN</TD><TD>0x9007</TD><TD>external error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_COMMUNICATION_FAULT</TD><TD>0x8101</TD><TD>monitor communication error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_GROUND_FAULT</TD><TD>0x9008</TD><TD>external error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_REFERENCE_POINT_LOST</TD><TD>0x6204</TD><TD>device user software error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_SAMPLING_ERROR</TD><TD>0x9009</TD><TD>external error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_THRESHOLD_WARNING</TD><TD>0x900A</TD><TD>external error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_OUTPUT_DISABLED</TD><TD>0x900B</TD><TD>external error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_SAFETY_EVENT</TD><TD>0x6205</TD><TD>device user software error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_EXTERNAL_FAULT</TD><TD>0x9009</TD><TD>external error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_TEMPORARY_FAULT</TD><TD>0x6206</TD><TD>device user software error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_DEVICE_HARDWARE</TD><TD>0x5001</TD><TD>device hardware error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_DEVICE_SOFTWARE</TD><TD>0x6207</TD><TD>device user software error</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_TYPE_MONITORING</TD><TD>0x8001</TD><TD>monitoring error</TD>
 * </TR><TR>
 * <TD>all others</TD><TD>0xFF00</TD><TD>Device specific</TD>
 * </TR></TABLE>
 *
 * If the common diagnosis types does not match the application needs, it is also possible to create any possible
 * emergency error code in a diagnosis message by setting the #SDAI_DIAGNOSIS_TYPE_PROTOCOL_SPECIFIC extension
 * bit in the diagnosis type (see \ref EcatDiagType for details).
 *
 * The following table shows how the SDAI common severity (see also \ref DiagSeverity) are mapped
 *
 * <TABLE><TR>
 * <TH>SDAI Diagnosis Severity</TH><TH>PN Diagnosis Type</TH>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_SEVERITY_MAINTENANCE_REQUIRED</TD><TD>Info message</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_SEVERITY_OUT_OF_SPECIFICATION</TD><TD>Warning message</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_SEVERITY_CHECK_FUNCTION</TD><TD>Warning message</TD>
 * </TR><TR>
 * <TD>#SDAI_DIAGNOSIS_SEVERITY_FAILURE</TD><TD>Error message</TD>
 * </TR></TABLE>
 */

/**
 *
 * @page ecat_dynamic Examples for Dynamic I/O Configuration in EtherCAT
 *
 * <B> The sequence chart shows some examples of changing the PDO mapping by the EtherCAT master. The adaption has to be
 *     done by the application </B>
 *
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI", "EtherCAT Master";
 * "Application"=>"SDAI"    [ label = "sdai_init(SDAI_DYNAMIC_IO_CONFIGURATION)", URL="\ref sdai_init()" ];
 * "Application"=>"SDAI"    [ label = "sdai_plug_unit(RxPDO1)", URL="\ref sdai_plug_unit()" ];
 * ...;
 * "Application"=>"SDAI"    [ label = "sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)", URL="\ref sdai_plug_unit()"];
 *
 * ---  [ label = "Stack online" ];
 * ...;
 * "Application" box "EtherCAT Master" [ label = "Master writes valid PDO Mapping object with new config", textbgcolour="silver"];
 * "EtherCAT Master"=>"SDAI"           [ label = "write PDO Mapping RxPDO1 Req" ];
 * "SDAI"=>"Application"               [ label = "ControlReqCbk(SDAI_CONTROL_CFG_DATA_INFO)" ];
 * "Application" abox "Application"    [ label = "check config"];
 * "Application"=>"SDAI"               [ label = "sdai_plug_unit(RxPDO1)", URL="\ref sdai_plug_unit()" ];
 * "Application"=>"SDAI"               [ label = "sdai_control_response(SDAI_SERVICE_SUCCESS)", URL="\ref sdai_control_response()" ];
 * "SDAI"=>"EtherCAT Master"           [ label = "write PDO Mapping RxPDO1 Res (Success)" ];
 * ...;
 * "Application" box "EtherCAT Master" [ label = "Master writes valid PDO Mapping object with existing config", textbgcolour="silver"];
 * "EtherCAT Master"=>"SDAI"           [ label = "write PDO Mapping RxPDO1 Req" ];
 * "SDAI"=>"Application"               [ label = "ControlReqCbk(SDAI_CONTROL_CFG_DATA_INFO)" ];
 * "Application" abox "Application"    [ label = "check config (it is allowed to call sdai_plug_unit() with the same config)"];
 * "Application"=>"SDAI"               [ label = "sdai_control_response(SDAI_SERVICE_SUCCESS)", URL="\ref sdai_control_response()" ];
 * "SDAI"=>"EtherCAT Master"           [ label = "write PDO Mapping RxPDO1 Res (Success)" ];
 * ...;
 * "Application" box "EtherCAT Master" [ label = "Master writes valid PDO Mapping object with wrong or invalid config", textbgcolour="silver"];
 * "EtherCAT Master"=>"SDAI"           [ label = "write PDO Mapping RxPDO1 Req" ];
 * "SDAI"=>"Application"               [ label = "ControlReqCbk(SDAI_CONTROL_CFG_DATA_INFO)" ];
 * "Application" abox "Application"    [ label = "check config"];
 * "Application"=>"SDAI"               [ label = "sdai_control_response(SDAI_SERVICE_ERR_WRONG_CFG_DATA)", URL="\ref sdai_control_response()" ];
 * "SDAI"=>"EtherCAT Master"           [ label = "write PDO Mapping RxPDO1 Res (Error)" ];
 * ...;
 * "Application" box "EtherCAT Master" [ label = "Master writes invalid PDO Mapping object", textbgcolour="silver"];
 * "EtherCAT Master"=>"SDAI"           [ label = "write PDO Mapping RxPDOX Req" ];
 * "SDAI"=>"EtherCAT Master"           [ label = "write PDO Mapping RxPDOX Res (Error)" ];
 * @endmsc
 *
 * <B> The sequence chart shows some examples of changing the PDO assignment by the EtherCAT master. This is handled completely
 *     by the SDAI </B>
 *
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI", "EtherCAT Master";
 * "Application"=>"SDAI"    [ label = "sdai_init(SDAI_DYNAMIC_IO_CONFIGURATION)", URL="\ref sdai_init()" ];
 * "Application"=>"SDAI"    [ label = "sdai_plug_unit(X)", URL="\ref sdai_plug_unit()" ];
 * ...;
 * "Application"=>"SDAI"    [ label = "sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)", URL="\ref sdai_plug_unit()"];
 *
 * ---  [ label = "Stack online" ];
 * ...;
 * "Application" box "EtherCAT Master" [ label = "Master writes PDO assignment object with valid config", textbgcolour="silver"];
 * "EtherCAT Master"=>"SDAI"           [ label = "write PDO assignment Req" ];
 * "SDAI" abox "SDAI"                  [ label = "check assignment. Adapt Sync Master settings"];
 * "SDAI"=>"EtherCAT Master"           [ label = "write PDO assignment Res (Success)" ];
 * ...;
 * "Application" box "EtherCAT Master" [ label = "Master writes PDO assignment object with invalid config", textbgcolour="silver"];
 * "EtherCAT Master"=>"SDAI"           [ label = "write PDO assignment Req" ];
 * "SDAI" abox "SDAI"                  [ label = "check assignment"];
 * "SDAI"=>"EtherCAT Master"           [ label = "write PDO assignment Res (Error)" ];
 * @endmsc
 *
 * <B> The sequence chart shows some examples of changing the PDOs by the application. </B>
 *
 * @msc
 * hscale = "1.7";
 * "Application", "SDAI", "EtherCAT Master";
 * "Application"=>"SDAI"    [ label = "sdai_init(SDAI_DYNAMIC_IO_CONFIGURATION)", URL="\ref sdai_init()" ];
 * "Application"=>"SDAI"    [ label = "sdai_plug_unit(X)", URL="\ref sdai_plug_unit()" ];
 * ...;
 * "Application"=>"SDAI"    [ label = "sdai_plug_unit(SDAI_UNIT_TYPE_PLUGGING_COMPLETE)", URL="\ref sdai_plug_unit()"];
 *
 * ---  [ label = "Stack online" ];
 * ...;
 * "Application" box "EtherCAT Master" [ label = "Application removes PDO that is assigned to a Sync Manager", textbgcolour="silver"];
 * ---  [ label = "State OPERATIONAL" ];
 * "Application"=>"SDAI"               [ label = "sdai_pull_unit(X)", URL="\ref sdai_pull_unit()" ];
 * "SDAI" abox "SDAI"                  [ label = "remove assignment and PDO mapping object"];
 * "SDAI"=>"EtherCAT Master"           [ label = "change state to INIT" ];
 * ...;
 * "Application" box "EtherCAT Master" [ label = "Application removes PDO that is not assigned to a Sync Manager", textbgcolour="silver"];
 * ---  [ label = "State OPERATIONAL" ];
 * "Application"=>"SDAI"               [ label = "sdai_pull_unit(X)", URL="\ref sdai_pull_unit()" ];
 * "SDAI" abox "SDAI"                  [ label = "remove assignment and PDO mapping object"];
 * ...;
 * "Application" box "EtherCAT Master" [ label = "Application adds PDO in state > PRE_OP", textbgcolour="silver"];
 * ---  [ label = "State OPERATIONAL" ];
 * "Application"=>"SDAI"               [ label = "sdai_plug_unit(X)", URL="\ref sdai_plug_unit()" ];
 * "SDAI" abox "SDAI"                  [ label = "add PDO mapping object"];
 * ...;
 * "Application" box "EtherCAT Master" [ label = "Application adds PDO in state <= PRE_OP", textbgcolour="silver"];
 * ---  [ label = "State PRE_OP" ];
 * "Application"=>"SDAI"               [ label = "sdai_plug_unit(X)", URL="\ref sdai_plug_unit()" ];
 * "SDAI" abox "SDAI"                  [ label = "add assignment and PDO mapping object"];
 * @endmsc
 *
 */

#ifndef __SDAI_ECAT_H__
#define __SDAI_ECAT_H__

/******************************************************************************
DEFINES
******************************************************************************/

#define SDAI_ECAT_MAILBOX_SIZE       512  /**< The size of the mailbox in the ESC core */

/** \anchor EcatUnitId @name SDAI Unit ID macros for EtherCAT
* Format of the SDAI Unit ID for EtherCAT:
\verbatim
    ------------------------------------------------------
    |     SDAI Unit Index     |    ECAT Object Index     |
    ------------------------------------------------------
    32      High Word         16        Low Word         0
\endverbatim
*/
//@{
#define _SDAI_ECAT_UNIT_INDEX_AND_OBJ_INDEX_TO_ID(UnitIndex, ObjIndex)  ((U32) ((((U32)(UnitIndex)) << 16) | (ObjIndex))) /**< Converts the ECAT Object Index and the SDAI Unit Index to the SDAI Id */
#define _SDAI_ECAT_ID_TO_UNIT_INDEX(Type, Id)                           ((Type) ((Id >> 16) & 0x0000FFFFuL))              /**< Converts the SDAI Id to the SDAI Unit Index */
#define _SDAI_ECAT_ID_TO_OBJ_INDEX(Type, Id)                            ((Type) ((Id) & 0x0000FFFFuL))                    /**< Converts the SDAI Id to the ECAT Object Index */
//@}

/*---------------------------------------------------------------------------*/

/** \anchor EcatFlags @name EtherCAT specific flags
* Definitions for EtherCAT specific interface configuration flags.
*/
//@{
#define SDAI_ECAT_ENABLE_EOE   0x01  /**< Enables the EOE protocol (Ethernet over EtherCAT). The EOE protocol
                                          transparently tunnels ethernet frames over EtherCAT. It is used for
                                          devices with TCP/IP stacks (e.g. web server). When enabling EOE the
                                          EtherCAT stack initializes and starts the used TCP/IP stack. If the application
                                          sets this flag a valid IP address, netmask and virtual MAC address
                                          must be specified. */
#define SDAI_ECAT_ENABLE_FOE   0x02  /**< Enables the FOE protocol (Filetransfer over EtherCAT). The FOE protocol
                                          transmit file streams over EtherCAT (comparable to TFTP). It is used for
                                          upload/download files to/from the device. When enabling FOE the
                                          EtherCAT stack accepts reading/writing of files and forwards the handling
                                          of these streams to the application. */
//@}

/** \anchor SyncFlags @name Synchronization Flags
 *  Definitions for the configuration of synchronization signals */
//@{
#define SDAI_ECAT_SYNC0_NOT_SUPPORTED  0x00       /**< The synchronization signal SYNC0 is not supported */
#define SDAI_ECAT_SYNC0_SUPPORTED      0x01       /**< The synchronization signal SYNC0 is supported  */
#define SDAI_ECAT_SYNC1_NOT_SUPPORTED  0x00       /**< The synchronization signal SYNC1 is not supported */
#define SDAI_ECAT_SYNC1_SUPPORTED      0x02       /**< The synchronization signal SYNC1 is supported  */
//@}

/*---------------------------------------------------------------------------*/

/** \anchor EcatUnitFlags @name EtherCAT Unit Flags
 * EtherCAT specific unit flags */
//@{
#define SDAI_UNIT_FLAG_DYNAMIC_PDO_MAPPING              0x01       /**< The PDO Mapping of the Unit is changable */
//@}

/*---------------------------------------------------------------------------*/

/* Definitions for EtherCAT specific device data */
#define SDAI_ECAT_VERSION_MAX_LEN      20         /**< Maximum length of the EtherCAT SW/HW version string */

/*---------------------------------------------------------------------------*/

/** \anchor EcatControlCode @name EtherCAT IO Control Codes
 * EtherCAT specific definitions for Control Codes. */
//@{
#define SDAI_ECAT_CONTROL_FILE_OPEN_REQ     0x0100  /**< The Open File service is used to open a file stream for reading/writing. */
#define SDAI_ECAT_CONTROL_FILE_CLOSE_REQ    0x0200  /**< The Close File service is used to close an open file. */
//@}

/*---------------------------------------------------------------------------*/

#define SDAI_ECAT_SERVICE_CLASS_SDO         0x10
#define SDAI_ECAT_SERVICE_CLASS_SDO_INFO    0x20
#define SDAI_ECAT_SERVICE_CLASS_FOE         0x40

/** \anchor EcatServiceCodes @name EtherCAT Services
 *  Definitions for EtherCAT specific Services */
//@{
/** The SDO Upload service is used to read data from objects of the Object Dictionary. */
#define SDAI_ECAT_SERVICE_SDO_UPLOAD (SDAI_ECAT_SERVICE_CLASS_SDO | 0x1)

/** The SDO Download service is used to write data to objects of the Object Dictionary. */
#define SDAI_ECAT_SERVICE_SDO_DOWNLOAD (SDAI_ECAT_SERVICE_CLASS_SDO | 0x2)

/** The Get OD List service is used to read object lists existing in the object dictionary. */
#define SDAI_ECAT_SERVICE_GET_OD_LIST (SDAI_ECAT_SERVICE_CLASS_SDO_INFO | 0x1)

/** The Get Object Description service is used to read the description of a object existing in the object dictionary. */
#define SDAI_ECAT_SERVICE_GET_OBJ_DESCR (SDAI_ECAT_SERVICE_CLASS_SDO_INFO | 0x2)

/** The Get Object Description service is used to read the description of a object entry existing in the object dictionary. */
#define SDAI_ECAT_SERVICE_GET_ENTRY_DESCR (SDAI_ECAT_SERVICE_CLASS_SDO_INFO | 0x3)

/** The Read File service is used to read a piece of an open file. */
#define SDAI_ECAT_SERVICE_FILE_READ (SDAI_ECAT_SERVICE_CLASS_FOE | 0x1)

/** The Write File service is used to append data to an open file. */
#define SDAI_ECAT_SERVICE_FILE_WRITE (SDAI_ECAT_SERVICE_CLASS_FOE | 0x2)
//@}

/*---------------------------------------------------------------------------*/

/** \anchor EcatCompleteAccess @name EtherCAT Complete Access
 *  Definitions for EtherCAT specific accesses to objects */
//@{
#define SDAI_ECAT_SINGLE_ACCESS     0x00  /**< Entry addressed with index and subindex will be accessed */
#define SDAI_ECAT_COMPLETE_ACCESS   0x01  /**< Complete object addressed with index will be accessed,
                                               subindex shall be ignored */
//@}

/*---------------------------------------------------------------------------*/

/** \anchor EcatListType @name EtherCAT List Types
 *  Definitions for EtherCAT specific List Types */
//@{
#define SDAI_ECAT_INFO_LIST_TYPE_LENGTH   0   /**< Get number of application objects in the 5 different lists */
#define SDAI_ECAT_INFO_LIST_TYPE_ALL      1   /**< All application objects of the object dictionary shall be delivered in the response. */
#define SDAI_ECAT_INFO_LIST_TYPE_RXPDO    2   /**< Only objects which are mappable in a RxPDO shall be delivered in the response */
#define SDAI_ECAT_INFO_LIST_TYPE_TXPDO    3   /**< Only objects which are mappable in a TxPDO shall be delivered in the response */
#define SDAI_ECAT_INFO_LIST_TYPE_BACKUP   4   /**< Only objects which has to stored for a device replacement shall be delivered in the response */
#define SDAI_ECAT_INFO_LIST_TYPE_SET      5   /**< Only objects which can be used as startup parameter for settings shall be delivered in the response */
//@}

/*---------------------------------------------------------------------------*/

#define SDAI_ECAT_OBJ_NAME_MAX_LEN      240   /**< Maximum length of the EtherCAT object name */
#define SDAI_ECAT_MAX_NR_LIST_ENTRIES   209   /**< Maximum number of list entries used by structure
                                                   #SDAI_ECAT_GET_OD_LIST_RES_DATA.  */

/** \anchor EcatDataType @name EtherCAT Basic Data Types
 *  Definitions for EtherCAT specific basic data types. See EtherCAT specification part 5 and 6
 *  for a description of these basic data types. */
//@{
#define SDAI_ECAT_DATA_TYPE_BOOLEAN         0x0001
#define SDAI_ECAT_DATA_TYPE_INTEGER8        0x0002
#define SDAI_ECAT_DATA_TYPE_INTEGER16       0x0003
#define SDAI_ECAT_DATA_TYPE_INTEGER32       0x0004
#define SDAI_ECAT_DATA_TYPE_UNSIGNED8       0x0005
#define SDAI_ECAT_DATA_TYPE_UNSIGNED16      0x0006
#define SDAI_ECAT_DATA_TYPE_UNSIGNED32      0x0007
#define SDAI_ECAT_DATA_TYPE_REAL32          0x0008
#define SDAI_ECAT_DATA_TYPE_VISIBLE_STRING  0x0009
#define SDAI_ECAT_DATA_TYPE_OCTET_STRING    0x000A
#define SDAI_ECAT_DATA_TYPE_UNICODE_STRING  0x000B
#define SDAI_ECAT_DATA_TYPE_TIME_OF_DAY     0x000C
#define SDAI_ECAT_DATA_TYPE_TIME_DIFFERENCE 0x000D
#define SDAI_ECAT_DATA_TYPE_INTEGER24       0x0010
#define SDAI_ECAT_DATA_TYPE_REAL64          0x0011
#define SDAI_ECAT_DATA_TYPE_INTEGER40       0x0012
#define SDAI_ECAT_DATA_TYPE_INTEGER48       0x0013
#define SDAI_ECAT_DATA_TYPE_INTEGER56       0x0014
#define SDAI_ECAT_DATA_TYPE_INTEGER64       0x0015
#define SDAI_ECAT_DATA_TYPE_UNSIGNED24      0x0016
#define SDAI_ECAT_DATA_TYPE_UNSIGNED40      0x0018
#define SDAI_ECAT_DATA_TYPE_UNSIGNED48      0x0019
#define SDAI_ECAT_DATA_TYPE_UNSIGNED56      0x001A
#define SDAI_ECAT_DATA_TYPE_UNSIGNED64      0x001B
//@}

/** \anchor EcatObjCode @name EtherCAT Object Codes
 *  Definitions for EtherCAT specific object codes */
//@{
#define SDAI_ECAT_OBJCODE_VAR       0x07  /**< general object type: variable */
#define SDAI_ECAT_OBJCODE_ARRAY     0x08  /**< general object type: array */
#define SDAI_ECAT_OBJCODE_RECORD    0x09  /**< general object type: record (structure) */
//@}

/*---------------------------------------------------------------------------*/

/** \anchor EcatAccessRight @name EtherCAT Access Rights
 *  Definitions for EtherCAT specific object access rights */
//@{
#define SDAI_ECAT_ACCESS_READ_WRITE       0x003F  /**< Read and Write access in all states */
#define SDAI_ECAT_ACCESS_READ             0x0007  /**< Read access in all states */
#define SDAI_ECAT_ACCESS_READ_PREOP       0x0001  /**< Read access in state #SDAI_ECAT_STATE_PRE_OP */
#define SDAI_ECAT_ACCESS_READ_SAFEOP      0x0002  /**< Read access in state #SDAI_ECAT_STATE_SAFE_OP */
#define SDAI_ECAT_ACCESS_READ_OP          0x0004  /**< Read access in state #SDAI_ECAT_STATE_OP */
#define SDAI_ECAT_ACCESS_WRITE            0x0038  /**< Write access in all states */
#define SDAI_ECAT_ACCESS_WRITE_PREOP      0x0008  /**< Read access in state #SDAI_ECAT_STATE_PRE_OP */
#define SDAI_ECAT_ACCESS_WRITE_SAFEOP     0x0010  /**< Read access in state #SDAI_ECAT_STATE_SAFE_OP */
#define SDAI_ECAT_ACCESS_WRITE_OP         0x0020  /**< Read access in state #SDAI_ECAT_STATE_OP */
#define SDAI_ECAT_ACCESS_NOT_PDO_MAPPABLE 0x0000  /**< Object is not mappable in a PDO */
#define SDAI_ECAT_ACCESS_RXPDO_MAPPABLE   0x0040  /**< Object is mappable in a RxPDO */
#define SDAI_ECAT_ACCESS_TXPDO_MAPPABLE   0x0080  /**< Object is mappable in a TxPDO */
#define SDAI_ECAT_ACCESS_BACKUP           0x0100  /**< Object can be used for backup */
#define SDAI_ECAT_ACCESS_SETTINGS         0x0200  /**< Object can be used as a startup parameter for settings */
//@}

/*---------------------------------------------------------------------------*/

/** \anchor SyncStatus @name EtherCAT Synchronization Status
 *  Definitions for the EtherCAT Synchronization Status */
//@{
#define SDAI_ECAT_SYNC0_ACTIVE          0x01       /**< The synchronization signal SYNC0 is generated  */
#define SDAI_ECAT_SYNC1_ACTIVE          0x02       /**< The synchronization signal SYNC1 is generated */
//@}

/*---------------------------------------------------------------------------*/

/** \anchor PDOIndex @name EtherCAT PDO Index Number Ranges
 * EtherCAT specific limits for the index number of PDO mapping objects */
//@{
/* EtherACT specific limits for the configuration data. */
#define SDAI_ECAT_MIN_RXPDO_INDEX   0x1600   /**< minimum index number for Receive PDO mapping objects */
#define SDAI_ECAT_MAX_RXPDO_INDEX   0x17FF   /**< maximum index number for Receive PDO mapping objects */
#define SDAI_ECAT_MIN_TXPDO_INDEX   0x1A00   /**< minimum index number for Transmit PDO mapping objects */
#define SDAI_ECAT_MAX_TXPDO_INDEX   0x1BFF   /**< maximum index number for Transmit PDO mapping objects */
//@}

#define SDAI_ECAT_MAX_NUMBER_MAPPED_OBJECTS   32 /**< maximum number of application objects which can be mapped to a PDO object */

/*---------------------------------------------------------------------------*/

/** \anchor AlStatus @name AL Status
 * Stack states of the EtherCAT state machine */
//@{
#define SDAI_ECAT_STATE_INIT      0x01    /**< No communication on application layer. Master has access to the DL information registers. */
#define SDAI_ECAT_STATE_PRE_OP    0x02    /**< Mailbox (acyclic) communication possible. No process data communication. */
#define SDAI_ECAT_STATE_SAFE_OP   0x03    /**< Process data communication, but only inputs are evaluated - outputs remain in safe state. */
#define SDAI_ECAT_STATE_OP        0x04    /**< Inputs and Outputs are valid. */
#define SDAI_ECAT_STATE_BOOT      0x05    /**< Bootstrap mode for firmware download. */
//@}

/** \anchor AlStatusCode @name AL Status Codes
 * Error codes of the EtherCAT state machine */
//@{
#define SDAI_ECAT_STATUS_CODE_NO_ERROR                  0x0000    /**< No error detected */
#define SDAI_ECAT_STATUS_CODE_AL_CONTROL_INVALID        0x0011    /**< Invalid requested state change, e.g. INIT -> OP */
#define SDAI_ECAT_STATUS_CODE_AL_CONTROL_UNKNOWN        0x0012    /**< Unknown requested state */
#define SDAI_ECAT_STATUS_CODE_BOOT_NOT_SUPP             0x0013    /**< Bootstrap not supported */
#define SDAI_ECAT_STATUS_CODE_MBX_CFG_INVALID           0x0016    /**< Invalid mailbox configuration */
#define SDAI_ECAT_STATUS_CODE_NO_VALID_INPUTS_AVAILABLE 0x0018    /**< No valid inputs available */
#define SDAI_ECAT_STATUS_CODE_SM_WATCHDOG               0x001B    /**< Sync manager watchdog expired */
#define SDAI_ECAT_STATUS_CODE_OUTPUT_CFG_INVALID        0x001D    /**< Invalid Output Configuration */
#define SDAI_ECAT_STATUS_CODE_INPUT_CFG_INVALID         0x001E    /**< Invalid Input Configuration */
#define SDAI_ECAT_STATUS_CODE_WATCHDOG_CFG_INVALID      0x001F    /**< Invalid Watchdog Configuration */
#define SDAI_ECAT_STATUS_CODE_DC_SYNCH_CFG_INVALID      0x0030    /**< Invalid DC SYNCH Configuration */
#define SDAI_ECAT_STATUS_CODE_UNSOLICITED_STATE_CHANGE  0x8000    /**< Slave application has changed the EtherCAT state
                                                                       autonomously by calling sdai_deinit() */
//@}

/** \anchor EcatDiagType @name EtherCAT Diagnosis Types
 * EtherCAT specific Diagnosis types */
//@{
#define SDAI_ECAT_DIAGNOSIS_TYPE_DEVICE_INTERNAL_SOFTWARE   (SDAI_DIAGNOSIS_TYPE_PROTOCOL_SPECIFIC | 0x6100)
#define SDAI_ECAT_DIAGNOSIS_TYPE_DEVICE_USER_SOFTWARE       (SDAI_DIAGNOSIS_TYPE_PROTOCOL_SPECIFIC | 0x6200)
#define SDAI_ECAT_DIAGNOSIS_TYPE_DEVICE_SOFTWARE_DATA_SET   (SDAI_DIAGNOSIS_TYPE_PROTOCOL_SPECIFIC | 0x6300)
#define SDAI_ECAT_DIAGNOSIS_TYPE_ADDITIONAL_MODULES         (SDAI_DIAGNOSIS_TYPE_PROTOCOL_SPECIFIC | 0x7000)
#define SDAI_ECAT_DIAGNOSIS_TYPE_MONITORING_COMMUNICATION   (SDAI_DIAGNOSIS_TYPE_PROTOCOL_SPECIFIC | 0x8100)
#define SDAI_ECAT_DIAGNOSIS_TYPE_ADDITIONAL_FUNCTIONS       (SDAI_DIAGNOSIS_TYPE_PROTOCOL_SPECIFIC | 0xF000)
//@}

/******************************************************************************
STRUCTURES
******************************************************************************/

/** \brief This structure holds EtherCAT specific diagnosis parameter data. This structure has to be 32-bit aligned.
 * The application is responsible to add the required gaps to ensure 32-bit alignment of the data */
struct SDAI_ECAT_DIAGNOSIS_PARAMETER
{
  U16     FlagsParameter;   /**< Describe the data type of parameter and the length of the data.
                              * Bit 12-15 = 0: Bit 0-11 = Data type Index of the data type of parameter
                              * Bit 12-15 = 1: Bit 0-11 = size of BYTE-Array in bytes
                              * Bit 12-15 = 2: Bit 0-11 = size of ASCII-String (without ending 0) in bytes
                              * Bit 12-15 = 3: Bit 0-11 = size of Uni-Code-String in bytes
                              * Bit 12-15 = reserved
                              * Parameter directly attached after FlagsParameter. */

/*
  void    Parameter []; */ /**< The diagnosis parameters. Type/Length depends on \ref FlagsParameter. Shall be aligned, if necessary. */

};

/** \brief This structure holds EtherCAT specific diagnosis data. */
struct SDAI_ECAT_DIAGNOSIS_DATA
{
  U16     TextId;                                                         /**< Text ID as reference to the device description file. */
  U16     NumberParameter;                                                /**< The number of attached the diagnosis parameters data. The diagnosis parameter data must be attached directly at the end of the structure #SDAI_ECAT_DIAGNOSIS_DATA. */
/*
  struct SDAI_ECAT_DIAGNOSIS_PARAMETER    Parameter [NumberParameter]; */ /**< The diagnosis parameters. */
};

/** \brief EtherCAT network specific parameter. */
struct SDAI_ECAT_IDENT_DATA
{
  U8      Address [4];           /**< The IP-Address of the Device. Must be set if #SDAI_ECAT_ENABLE_EOE is set within the Flags parameter. */
  U8      Netmask [4];           /**< The Netmask of the Device. Must be set if #SDAI_ECAT_ENABLE_EOE is set within the Flags parameter. */
  U8      Gateway [4];           /**< The Gateway of the Device. The user may set #SDAI_GATEWAY_UNUSED if not available. Must be set if #SDAI_ECAT_ENABLE_EOE is set within the Flags parameter. */

  U8      VirtualMacAddress [6]; /**< The virtual MAC address of the device. Must be set if #SDAI_ECAT_ENABLE_EOE is set within the Flags parameter. */
  U16     DeviceIdSelector;      /**< Specifies EtherCAT device id selector */
  U8      Flags;                 /**< Specifies EtherCAT specific interface configuration flags. Valid values: \ref EcatFlags */

  U8      Alignment[3];
};

/*---------------------------------------------------------------------------*/

/** \brief EtherCAT device specific parameter. */
/** See also \ref ecat_init_data. */
struct SDAI_ECAT_DEVICE_DATA
{
  U8      SoftwareVersion [SDAI_ECAT_VERSION_MAX_LEN];  /**< Manufacturer specific SW version string */
  U8      HardwareVersion [SDAI_ECAT_VERSION_MAX_LEN];  /**< Manufacturer specific HW version string */
  U16     MajorRevisionNumber;                          /**< Major revision number of the device */
  U16     MinorRevisionNumber;                          /**< Minor revision number of the device */
  U8      SyncFlags;                                    /**< Configuration of synchronization signals. If signals SYNC0 and/or SYNC1
                                                          * shall be supported a user callback must be registered during calling
                                                          * sdai_init(). Valid values for the SyncFlags: \ref SyncFlags */
  U8      Alignment [3];
};

/*---------------------------------------------------------------------------*/

/** \brief Holds the header information for a SDO upload service */
struct SDAI_ECAT_SDO_UPLOAD_HEADER
{
  U16   Index;            /**< Index in the object dictionary of the object which shall be read. */
  U8    SubIndex;         /**< SubIndex in the object dictionary of the object which shall be read. */
  U8    CompleteAccess;   /**< Indicates that either all subindices or only the specified subindex shall be read. Valid values: \ref EcatCompleteAccess */
};

/** \brief Holds the header information for a SDO download service */
struct SDAI_ECAT_SDO_DOWNLOAD_HEADER
{
  U16   Index;            /**< Index in the object dictionary of the object which shall be written. */
  U8    SubIndex;         /**< SubIndex in the object dictionary of the object which shall be written. */
  U8    CompleteAccess;   /**< Indicates that either all subindices or only the specified subindex shall be written. Valid values: \ref EcatCompleteAccess */
};

/** \brief Holds the header information for a Get OD List service. */
/** The service data for the Get OD List response are specified by #SDAI_ECAT_GET_OD_LIST_RES_DATA. */
struct SDAI_ECAT_GET_OD_LIST_HEADER
{
  U16   ListType;         /**< Specifies the index list of objects that have at least one entry with the appropriate
                            * object access attribute set. Valid values for ListType: \ref EcatListType */
  U8    Alignment [2];
};

/** \brief Holds the header information for a Get Object Description service. */
/** The service data for the Get Object Description response are specified by #SDAI_ECAT_GET_OBJ_DESCR_RES_DATA. */
struct SDAI_ECAT_GET_OBJ_DESCR_HEADER
{
  U16   Index;  /**< Index in the object dictionary of the object for which the object description shall be read. */
  U8    Alignment [2];
};

/** \brief Holds the header information for a Get Entry Description service. */
/** The service data for the Get Entry Description response are specified by #SDAI_ECAT_GET_ENTRY_DESCR_RES_DATA. */
struct SDAI_ECAT_GET_ENTRY_DESCR_HEADER
{
  U16   Index;      /**< Index in the object dictionary of the object for which the entry description shall be read. */
  U8    SubIndex;   /**< SubIndex in the object dictionary of the object for which the entry description shall be read. */
  U8    Alignment;
};

/** \brief Holds the header information for a File Open service. */
/** The filename is attached directly to the end of the structure SDAI_ECAT_FOE_FILE_OPEN_HEADER. */
struct SDAI_ECAT_FOE_FILE_OPEN_HEADER
{
  U16   NameLength;   /**< Length of the name of the file which should be opened for reading or writing. */
  S8    Mode;         /**< Mode points to one of the following characters: 'r' Open file for reading.  The stream is  positioned  at  the beginning of the file.
                        *                                                  'w' Truncate file to zero length or create file  for  writing. The stream is positioned at the beginning of the file. */
  U8    Alignment;
  U32   Password;     /**< Passwort used for authentification to open a file. */
};

/** \brief Holds the header information for a File Read service. */
struct SDAI_ECAT_FOE_FILE_READ_HEADER
{
  U32   Offset; /**< Offset of the open file, where the reading operation shall start. */
};

/** \brief Holds the header information for a File Close service. */
/** There exists no service data for the File Close response. */
struct SDAI_ECAT_FOE_FILE_CLOSE_HEADER
{
  U16   ErrorCode; /**< ErrorCode of the file operation. */
  U8    Alignment [2];
};

/** \brief Holds the header information for EtherCAT services */
struct SDAI_ECAT_SERVICE_HEADER
{
  U16   Service;  /**< Specifies the EtherCAT service. Valid values: \ref EcatServiceCodes */
  U8    Alignment [2];

  union
  {
    /* SDAI_ECAT_SERVICE_CLASS_SDO */
    struct SDAI_ECAT_SDO_UPLOAD_HEADER        SdoUpload;      /**< Header information for a SDO Upload service */
    struct SDAI_ECAT_SDO_DOWNLOAD_HEADER      SdoDownload;    /**< Header information for a SDO Download service */

    /* SDAI_ECAT_SERVICE_CLASS_SDO_INFO */
    struct SDAI_ECAT_GET_OD_LIST_HEADER       GetOdList;      /**< Header information for a Get OD List service */
    struct SDAI_ECAT_GET_OBJ_DESCR_HEADER     GetObjDescr;    /**< Header information for a Get Object Description service */
    struct SDAI_ECAT_GET_ENTRY_DESCR_HEADER   GetEntryDescr;  /**< Header information for a Get Entry Description service */

    /* SDAI_ECAT_SERVICE_CLASS_FOE */
    struct SDAI_ECAT_FOE_FILE_READ_HEADER     FoeRead;        /**< Header information for a File Read service */

  } ServiceHeader;
};

/*---------------------------------------------------------------------------*/

/** \brief Holds the EtherCAT specific service data of an outgoing Get OD List response. */
/** The list content is specified by parameter ListType of service header #SDAI_ECAT_GET_OD_LIST_HEADER.
    The service data block is attached directly to the end of the structure #SDAI_READ_RES. */
struct SDAI_ECAT_GET_OD_LIST_RES_DATA
{
  U16   NumberEntries;                              /**< Number of entries in the following array */
  U16   IndexList [SDAI_ECAT_MAX_NR_LIST_ENTRIES];  /**< List of object indexes or 5 words with the length of the list types if list type is #SDAI_ECAT_INFO_LIST_TYPE_LENGTH */
};

/** \brief Holds the EtherCAT specific service data of an outgoing Get Object Description response. */
/** The object is addressed by parameter Index specified by the service header #SDAI_ECAT_GET_OBJ_DESCR_HEADER.
    The service data block is attached directly to the end of the structure #SDAI_READ_RES. */
struct SDAI_ECAT_GET_OBJ_DESCR_RES_DATA
{
  U16   DataType;                                 /**< Index of the data type in the object dictionary for this object.
                                                    * Predefined basic data types: \ref EcatDataType.
                                                    * Manufacturer specific data types can be defined from 0x40 - 0x5F. Device profile specific
                                                    * data types are defined from 0x60 - 0x25F (see respective profile specification for details). */
  U8    MaxSubindex;                              /**< Indicates the highest subindex number */
  U8    ObjectCode;                               /**< General type information for this object. Valid values: \ref EcatObjCode */
  char  ObjectName [SDAI_ECAT_OBJ_NAME_MAX_LEN];  /**< Name of the object */
};

/** \brief Holds the EtherCAT specific service data of an outgoing Get Entry Description response. */
/** The object entry is addressed by the parameters Index and SubIndex specified by the service header
    #SDAI_ECAT_GET_ENTRY_DESCR_HEADER. The service data block is attached directly to the end of the
    structure #SDAI_READ_RES. */
struct SDAI_ECAT_GET_ENTRY_DESCR_RES_DATA
{
  U16   DataType;                                 /**< Index of the data type in the object dictionary for this object entry.
                                                    * Predefined basic data types: \ref EcatDataType.
                                                    * Manufacturer specific data types can be defined from 0x40 - 0x5F. Device profile specific
                                                    * data types are defined from 0x60 - 0x25F (see respective profile specification for details). */
  U16   BitLength;                                /**< Length in bits of the object entry value */
  U16   ObjectAccess;                             /**< Specifies the access right to the object entry. Valid values are: \ref EcatAccessRight */
  char  EntryName [SDAI_ECAT_OBJ_NAME_MAX_LEN];   /**< Name of the object */
};

/*---------------------------------------------------------------------------*/

/** \brief EtherCAT specific synchronization data. */
struct SDAI_ECAT_SYNC_DATA
{
  U8   SyncStatus;     /**< Indicates the generated synchronization signal. Valid values for the SyncStatus: \ref SyncStatus */
  U8   Alignment [3];
};

/*---------------------------------------------------------------------------*/

/** \brief EtherCAT specific description of an application object which is mapped to a PDO object. */
struct SDAI_ECAT_OBJ_DESCR
{
  U16    Index;       /**< Index number of the mapped object */
  U8     SubIndex;    /**< Subindex number of the mapped object */
  U8     BitLength;   /**< Length of the mapped object in bits */
};

/** \brief EtherCAT specific configuration data. */
/** See also \ref ecat_plug_unit. */
struct SDAI_ECAT_CFG_DATA
{
  U16                           Index;                                             /**< Index number of the PDO mapping object. \n\n Valid values for output units:
                                                                                     * #SDAI_ECAT_MIN_RXPDO_INDEX <= Index <= #SDAI_ECAT_MAX_RXPDO_INDEX \n
                                                                                     * Valid values for input units:
                                                                                     * #SDAI_ECAT_MIN_TXPDO_INDEX <= Index <= #SDAI_ECAT_MAX_TXPDO_INDEX */

  U8                            Flags;                                             /**< Additional Informations. Valid values for Flags: \ref EcatUnitFlags */

  U8                            NumberMappedObjects;                               /**< Specifies the number of mapped objects which are described in the
                                                                                     * following array. Valid values: 0 - #SDAI_ECAT_MAX_NUMBER_MAPPED_OBJECTS */
  struct SDAI_ECAT_OBJ_DESCR    ObjectDescr [SDAI_ECAT_MAX_NUMBER_MAPPED_OBJECTS]; /**< Description of the objects which are mapped to the PDO */
};

/*---------------------------------------------------------------------------*/

/** \brief This structure holds EtherCAT specific additional status informations. */
struct SDAI_ECAT_ADDSTATUS_DATA
{
  U16   AlStatusCode;                 /**< Indicates the last error detected by the EtherCAT state machine. Depending on the
                                        * occurred error the EtherCAT state machine may execute a state transition. Valid
                                        * values for the AlStatusCode: \ref AlStatusCode */
  U8    AlStatus;                     /**< Status of the EtherCAT state machine. Valid values for the AlStatus: \ref AlStatus */
  U8    ErrorRegister;                /**< Summary information about the collected emergency messages. */
  U8    NumberUnacknowledgeDiagnosis; /**< Counts how many diagnosis messages are new since last acknowledge by the EtherCAT master. */
  U8    Alignment [1];
  U16   UsedStationAlias;             /**< station alias, which is used at the moment to identify the device. */
};

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

#endif /* __SDAI_ECAT_H__ */

/*****************************************************************************/
