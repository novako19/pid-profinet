/******************************************************************************

SOFTING AG
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

 /** @page sdai_socket_interface Socket Interface
 * In addition to the realtime ethernet communication via the Simple Device Application Interface (SDAI)
 * Softing offers a universal Socket Interface for implementation of any TCP or UDP based protocol. It
 * enables the integration of various industrial IT functions into the device:
 *
 * - Embedded Webserver for freely designed websites
 * - FTP-Server for updating the device firmware or for file downloads
 * - E-Mail client for sending E-Mails from the application program
 * - Telnet for virtual terminal connections
 * - Implementation of manufacturer specific application protocols
 *
 * The Socket Interface offers a comfortable application interface which disburdens the user from a lot of
 * tasks and provides a lot of automatisms. The special characteristics of each TCP/IP stack will be hidden
 * from the user. Establishment and management of TCP connections are handled completely by the socket interface.
 * The user can focus on the implementation of the desired IT functionality. This allows an easy implementation
 * of the socket application.
 *
 * \note The socket interface is not supported by all protocols. See \ref supported_protocols
 *
 * The communication via the Socket Interface can be used in parallel and without influencing the realtime
 * communication via the used realtime ethernet protocol. Therefore the realtime ethernet communication
 * will be processed with a higher priority than the communication via the Socket Interface. The network interface
 * is shared between the Socket Interface and the realtime ethernet protocol. Thus a high communication load
 * to be processed by the realtime ethernet protocol limits the available bandwith and the performance of the
 * Socket Interface communication.
 *
 * The following figure shows the structure of Softings Socket Interface implementation.
 * @image html Socket_Structure.jpg
 *
 */

/*****************************************************************************/

#ifndef __SDAI_SOCKET_H__
#define __SDAI_SOCKET_H__

/******************************************************************************
DEFINES
******************************************************************************/

/**@{*/

#define MAX_NUMBER_SUPPORTED_SOCKETS        (U16) 64u       /**< Maximum number of open sockets */
#define SOCKET_FIFO_SIZE                    (3 * 1024)      /**< Maximum size of data to be transfered in a single request */
#define MAX_NUMBER_CONNECTIONS_PER_SOCKET   5u              /**< Maximum number of open connections per socket */

/*--- Socket options --------------------------------------------------------*/

/** \anchor SocketFlags @name Socket Flags
 * Definitions for Socket Flags
 * @{
 */

#define SOCK_FLAG_TYPE_UDP            0x00000000uL                         /**< Socket type UDP */
#define SOCK_FLAG_TYPE_TCP            0x00008000uL                         /**< Socket type TCP */
#define SOCK_FLAG_ENABLE_KEEPALIVE    (SOCK_FLAG_TYPE_TCP | 0x00000001uL)  /**< Enables keep-alive packets to supervise a TCP connection */
#define SOCK_FLAG_ENABLE_BROADCAST    (SOCK_FLAG_TYPE_UDP | 0x00000002uL)  /**< Enables the transmission of broadcast messages over a UDP socket */
#define SOCK_FLAG_ENABLE_MULTICAST    (SOCK_FLAG_TYPE_UDP | 0x00000004uL)  /**< Enables the transmission of multicast messages over a UDP socket */
#define SOCK_FLAG_USER_MASK           0x0000FFFFuL                         /**< Specifies the bits which are valid for the socket application */

/** @} */

/*--- Socket error codes ----------------------------------------------------*/

/** \anchor SocketErrorCode @name Socket Interface Error Codes
 * Definitions for Socket Interface Error Codes
 * @{
 */

typedef enum _T_SOCKET_RESULT
{
  SOCK_SUCCESS                   = 0u,  /**< General success return value */
  SOCK_ERR_INVALID_ARGUMENT,            /**< Passed argument is invalid */
  SOCK_ERR_API_ALREADY_INITIALIZED,     /**< Socket Interface already initialized */
  SOCK_ERR_API_NOT_INITIALIZED,         /**< Socket Interface not initialized */
  SOCK_ERR_INVALID_SOCKET_ID,           /**< The passed socket ID is invalid */
  SOCK_ERR_OUT_OF_SOCKETS,              /**< No free socket available */
  SOCK_ERR_SOCKET_NOT_CREATED,          /**< The specified socket has not been created.  */
  SOCK_ERR_SOCKET_OFFLINE,              /**< The local port associated with the socket is not open. Sending/Receiving of messages is not possible. */
  SOCK_ERR_SOCKET_BUSY,                 /**< A send request is rejected because a previous send request on the specified socket is not completed */
  SOCK_ERR_SOCKET_NOT_CONNECTED,        /**< The request is rejected because the specified TCP connection or no TCP connection has been established for the specified socket. */
  SOCK_ERR_SOCKET_CONNECTED,            /**< The request is rejected because the specified TCP connection has established a connection before the filter was activated. */
  SOCK_ERR_ADDR_IN_USE,                 /**< The passed local address is already in use by another socket. */
  SOCK_ERR_INVALID_LOCAL_ADDR,          /**< The passed local IP address is invalid. It is not possible to bind the socket to this address. Which IP addresses are allowed depends
                                             on the socket configuration (e.g. socket type UDP or TCP) and the network settings adjusted by calling the function sock_init(). */
  SOCK_ERR_INVALID_REMOTE_ADDR,         /**< The passed remote IP address is invalid. It is not possible to send or connect to this address. Which IP addresses are allowed depends
                                             on the socket configuration (e.g. socket type UDP or TCP) and the network settings adjusted by calling the function sock_init(). */
  SOCK_ERR_NO_DATA_RECEIVED,            /**< No data has been received on the specified socket or TCP connection. */
  SOCK_ERR_NOT_SUPPORTED,               /**< The requested action is not supported on the specified socket. */
  SOCK_ERR_TEMPORARY_NOT_EXECUTABLE,    /**< The requested action is temporary not executable on the specified socket due a concurrent pending request. */
  SOCK_ERR_FATAL_ERROR                  /**< A fatal error is reported during execution of function. */

} T_SOCKET_RESULT;

/** @} */

/*---------------------------------------------------------------------------*/

#define SOCK_IP_ADDR_ANY              0x00000000uL  /**< Can be used as local IP address when creating a socket to bind the socket to all the IP addresses (If the system has
                                                         multiple IP addresses) or if the local IP address is unknown. */
#define SOCK_PORT_ANY                 0u            /**< Can be used as local port when creating a TCP Client socket to let the TCP/IP stack select a unique local port from the
                                                         dynamic client port range. */
/** @} */

/******************************************************************************
STRUCTURES
******************************************************************************/

/** \brief Specifies a transport IP address and port.
 * All of the structure data must be specified in network-byte-order (big-endian).
 */
typedef struct _T_SDAI_SOCK_ADDR
{
  U32  IpAddress;      /**< IP address */
  U16  Port;           /**< port number */

  U8   Alignment [2];

} T_SDAI_SOCK_ADDR;

/*---------------------------------------------------------------------------*/

/** \brief Configurable Options for the socket interface
 */
typedef struct _T_SDAI_SOCK_CONFIG_OPTIONS
{
  /** Keep-Alive Timeout in milliseconds. The Keep-Alive Timeout will be used for the supervision of TCP connections. The connection will be aborted if the
      remote connection partner did not show activity within this timeout. The supervision of TCP connections can be enabled/disabled socket-specific when
      creating the socket by calling sock_create(). For UDP sockets the application needs to perform a supervision if necessary. If the timeout value is set to 0
      and #SOCK_FLAG_ENABLE_KEEPALIVE is set when creating a socket, a default value for the Keep-Alive Timeout will be used. */
  U32    Timeout;

} T_SDAI_SOCK_CONFIG_OPTIONS;

/*---------------------------------------------------------------------------*/

/** \brief Network specific parameter
 * All of the structure data must be specified in network-byte-order (big-endian). The values are fallback values if the network settings are not set by the stack.
 * If the stack provides network settings these settings are used. If the network settings of the stack are removed e.g. by a controller/master, the
 * fallback values, if specified, are used
 */
typedef struct _T_SDAI_SOCK_NETWORK_SETTINGS
{
  U32   IpAddress;        /**< The fallback IP-Address of the Device. */
  U32   SubnetMask;       /**< The fallback Netmask of the Device. */
  U32   GatewayAddress;   /**< The fallback Gateway of the Device. */

  U8    MacAddress [6];   /**< The fallback MAC address of the device. */
  U8    Alignment [2];

} T_SDAI_SOCK_NETWORK_SETTINGS;

/*---------------------------------------------------------------------------*/

/** \brief Holds the informations of an incoming data received indication
 */
typedef struct _T_SDAI_SOCK_DATA_RECEIVED_IND
{
  T_BITSET64   Socket;          /**< Bitfield holding the information for which socket new data are available.  */

} T_SDAI_SOCK_DATA_RECEIVED_IND;

/** \brief The Callback structure holds the functions implemented by the user which
 * are called by the SDAI on the corresponding event.
 *
 * The data for these events are stored within the socket indication interface.
 * During the execution of a callback function the SDAI locks the access to the socket indication
 * interface. If now the stack tries to access the indication interface (e.g. to signal additional new data)
 * the stack will be blocked until the interface is unlocked. Therefore the
 * runtime of the stack can depends on the actions performed by the user-specific callback
 * functions. Socket communication can be delayed if actions are performed that take a long time, e.g. waiting for operating
 * system resources, delaying the task or printing log messages. To avoid these the user shall only copy the data and return
 * as fast as possible. For each function not implemented by the application a NULL pointer shall be set. (see also \ref sdai_event_handling)
 */
typedef struct _T_SDAI_SOCK_CALLBACKS
{
  void (*StateChangedCbk)    (U16 Socket);                          /**< Called when the socket state has been changed */
  void (*DataReceivedCbk)    (T_SDAI_SOCK_DATA_RECEIVED_IND*);      /**< Called when new data has been received for one or more sockets. The application
                                                                         has to call sdai_sock_receive() to get the new data. */

} T_SDAI_SOCK_CALLBACKS;

/*---------------------------------------------------------------------------*/

/** \brief This structure holds the initialization data for the socket interface
 */
typedef struct _T_SDAI_SOCK_INIT
{
  T_SDAI_SOCK_NETWORK_SETTINGS   NetworkSettings; /**< Network specific parameter */
  T_SDAI_SOCK_CONFIG_OPTIONS     ConfigOptions;   /**< Configurable Options for the socket interface */
  T_SDAI_SOCK_CALLBACKS          Callback;        /**< The callback functions implemented by the user */

} T_SDAI_SOCK_INIT;

/*---------------------------------------------------------------------------*/

/** \anchor ConnectionStatus @name TCP Connection Status
 * Status of a TCP connection
 * @{
 */
#define SOCK_TCP_CONNECTION_STATUS_UNCONNECTED              0x00000001uL  /**< TCP connection not established */
#define SOCK_TCP_CONNECTION_STATUS_CONNECTING               0x00000002uL  /**< A TCP connection establishment is in progress. This status is only possible for a TCP client socket. */
#define SOCK_TCP_CONNECTION_STATUS_CONNECTED                0x00000003uL  /**< TCP connection established */
/** @} */

/** \anchor ConnectionStatusCode @name TCP Connection Status Codes
 * Error Codes of a TCP connection
 * @{
 */
#define SOCK_TCP_CONNECTION_STATUS_CODE_NO_ERROR            0x00000000uL  /**< No error detected */
#define SOCK_TCP_CONNECTION_STATUS_CODE_INTERNAL_ERROR      0x00000001uL  /**< An internal error occurred in the socket interface causing the connection to abort. */
#define SOCK_TCP_CONNECTION_STATUS_CODE_CLOSED_REMOTELY     0x00000002uL  /**< The connection has been orderly closed by the remote connection partner (TCP FIN received). */
#define SOCK_TCP_CONNECTION_STATUS_CODE_ABORTED_REMOTELY    0x00000003uL  /**< The connection has been aborted by the remote connection partner (TCP RST received), e.g. because
                                                                               the remote partner has detected an error or temporarily crashed. */
#define SOCK_TCP_CONNECTION_STATUS_CODE_TIMEOUT             0x00000004uL  /**< The connection has been aborted because of a network failure or because the remote connection partner failed
                                                                               to respond within the configured supervision timeout (see also T_SDAI_SOCK_CONFIG_OPTIONS). */
#define SOCK_TCP_CONNECTION_STATUS_CODE_CONNECT_REJECTED    0x00000005uL  /**< It was not possible to establish a connection because the destination host rejected the connect request,
                                                                               e.g. because the remote port is closed. This error only occurs for a TCP client socket. To retry connection
                                                                               establishment the application has to close the socket and re-create the socket again. */
/** @} */

/** \brief This structure holds status informations about a single TCP connection
 */
typedef struct _T_SDAI_SOCK_TCP_CONNECTION_LOCAL_STATUS
{
  U32                 Status;       /**< Local status of the TCP connection. Valid values: \ref ConnectionStatus */
  U32                 StatusCode;   /**< Indicates the last error of the TCP connection. Valid values: \ref ConnectionStatusCode */

  T_SDAI_SOCK_ADDR    RemoteAddr;   /**< Address information of the remote connection partner */

} T_SDAI_SOCK_TCP_CONNECTION_LOCAL_STATUS;

/** \brief This structure holds status informations about all TCP connections of a socket
 */
typedef struct _T_SDAI_SOCK_TCP_LOCAL_STATUS_DATA
{
  U32                                       NumberConnections;                               /**< Number of TCP connection for which status info is provided */

  T_SDAI_SOCK_TCP_CONNECTION_LOCAL_STATUS   Connection [MAX_NUMBER_CONNECTIONS_PER_SOCKET];  /**< TCP connection status information */

} T_SDAI_SOCK_TCP_LOCAL_STATUS_DATA;

/** \anchor SocketStatus @name Socket Status
 * Status of a socket
 * @{
 */
#define SOCK_LOCAL_STATUS_CLOSED                            0x00000001uL  /**< The local socket is closed */
#define SOCK_LOCAL_STATUS_OFFLINE                           0x00000002uL  /**< The local port associated with the socket is not open. Sending/Receiving of messages is not possible. */
#define SOCK_LOCAL_STATUS_ONLINE                            0x00000003uL  /**< The local port associated with the socket is open. Sending/Receiving of messages is possible. */
/** @} */

/** \anchor SocketStatusCode @name Socket Status Codes
 * Error Codes of a socket
 * @{
 */
#define SOCK_LOCAL_STATUS_CODE_NO_ERROR                     0x00000000uL  /**< No error detected */
#define SOCK_LOCAL_STATUS_CODE_ADDR_IN_USE                  0x00000001uL  /**< The local address passed with sock_create() is already in use by another socket. */
#define SOCK_LOCAL_STATUS_CODE_INTERNAL_ERROR               0x00000002uL  /**< An internal error occurred in the socket interface causing the socket to close. */
/** @} */

/** \brief This structure holds status informations about a socket
 */
typedef struct _T_SDAI_SOCK_IOC_GET_LOCAL_STATUS
{
  U32   Status;     /**< Common socket status information. Valid values: \ref SocketStatus */
  U32   StatusCode; /**< Common socket error code. Valid values: \ref SocketStatusCode */

  union
  {
    T_SDAI_SOCK_TCP_LOCAL_STATUS_DATA   Tcp;   /**< TCP specific socket status information */
    /* UDP specific socket status information not existing */

  } UseAs;

} T_SDAI_SOCK_IOC_GET_LOCAL_STATUS;

/*---------------------------------------------------------------------------*/

/** \anchor SendStatus @name Send Status
 * Send Status of a socket
 * @{
 */
#define SOCK_SEND_STATUS_NOT_BUSY                           0x00000001uL  /**< No send request produced by the application */
#define SOCK_SEND_STATUS_BUSY                               0x00000002uL  /**< The send request is in progress. */
/** @} */

/** \anchor SendStatusCode @name Send Status Codes
 * Send Error Codes
 * @{
 */
#define SOCK_SEND_STATUS_CODE_NO_ERROR                      0x00000000uL  /**< No error detected */
#define SOCK_SEND_STATUS_CODE_NO_BUFFERS                    0x00000001uL  /**< The TCP/IP stack was unable to allocate an internal buffer or the output queue is full. */
#define SOCK_SEND_STATUS_CODE_HOST_UNREACHABLE              0x00000002uL  /**< The destination host is unreachable, e.g. the destination host is down */
#define SOCK_SEND_STATUS_CODE_REJECTED                      0x00000003uL  /**< The destination host rejected the message, e.g. remote port is closed.
                                                                               This error can only be returned by TCP sockets. */
#define SOCK_SEND_STATUS_CODE_SEND_ERROR                    0x00000004uL  /**< The TCP/IP stack reported a general problem sending the message. */
/** @} */

/** \brief This structure holds TCP specific send status informations
 */
typedef struct _T_SDAI_SOCK_TCP_SEND_STATUS_DATA
{
  T_SDAI_SOCK_ADDR    RemoteAddr;   /**< Address information of the remote connection partner */

} T_SDAI_SOCK_TCP_SEND_STATUS_DATA;

/** \brief This structure holds send status informations
 */
typedef struct _T_SDAI_SOCK_IOC_GET_SEND_STATUS
{
  U32   Status;     /**< Common send status information. Valid values: \ref SendStatus  */
  U32   StatusCode; /**< Common send error code. Valid values: \ref SendStatusCode */

  union
  {
    T_SDAI_SOCK_TCP_SEND_STATUS_DATA    Tcp;    /**< TCP specific send status information */
    /* UDP specific send status information not existing */

  } UseAs;

} T_SDAI_SOCK_IOC_GET_SEND_STATUS;

/*---------------------------------------------------------------------------*/

/** \anchor ReceiveStatus @name Receive Status
 * Receive Status of a socket
 * @{
 */
#define SOCK_RECEIVE_STATUS_NO_DATA_RECEIVED                0x00000000uL  /**< No data on the specified socket or TCP connection has been received. */
#define SOCK_RECEIVE_STATUS_DATA_RECEIVED                   0x00000001uL  /**< New data has been received on the specified socket or TCP connection. */
/** @} */

/** \anchor ReceiveStatusCode @name Receive Status Codes
 * Receive Error Codes
 * @{
 */
#define SOCK_RECEIVE_STATUS_CODE_NO_ERROR                   0x00000000uL  /**< No error detected */
/** @} */

/** \brief This structure holds TCP specific receive status informations
 */
typedef struct _T_SDAI_SOCK_TCP_RECEIVE_STATUS_DATA
{
  T_SDAI_SOCK_ADDR    RemoteAddr;   /**< Address information of the remote connection partner */

} T_SDAI_SOCK_TCP_RECEIVE_STATUS_DATA;

/** \brief This structure holds receive status informations
 */
typedef struct _T_SDAI_SOCK_IOC_GET_RECEIVE_STATUS
{
  U32   Status;     /**< Common receive status information. Valid values: \ref ReceiveStatus  */
  U32   StatusCode; /**< Common receive error code. Valid values: \ref ReceiveStatusCode */

  union
  {
    T_SDAI_SOCK_TCP_RECEIVE_STATUS_DATA   Tcp;    /**< TCP specific receive status information */
    /* UDP specific send status information not existing */

  } UseAs;

} T_SDAI_SOCK_IOC_GET_RECEIVE_STATUS;

/*---------------------------------------------------------------------------*/

/** \brief This structure specifies the TCP connection which shall be aborted
 */
typedef struct _T_SDAI_SOCK_IOC_CLOSE_TCP_CONNECTION
{
  T_SDAI_SOCK_ADDR    RemoteAddr;   /**< Address information of the remote connection partner */

} T_SDAI_SOCK_IOC_CLOSE_TCP_CONNECTION;

/*---------------------------------------------------------------------------*/

#define SOCK_MAX_NUMBER_FILTER_ENTRIES        4u

/** \brief This structure specifies the IP addresses from which a connection establishment shall be accepted
 */
typedef struct _T_SDAI_SOCK_IOC_TCP_ACCEPT_FILTER
{
  U16    NumberEntries;                              /**< number of filter entries */
  U16    Alignment;

  U32    Filter [SOCK_MAX_NUMBER_FILTER_ENTRIES];    /**< IP address */

} T_SDAI_SOCK_IOC_TCP_ACCEPT_FILTER;

/*---------------------------------------------------------------------------*/

/** \brief This structure specifies the IP addresses of the multicast group the application wants to join or leave
 */
typedef struct _T_SDAI_SOCK_IOC_MULTICAST
{
  U32    IpAddress;

} T_SDAI_SOCK_IOC_MULTICAST;

/*---------------------------------------------------------------------------*/

/** \anchor IoctlCommand @name IO Control Commands
 * IO Control Commands for a socket
 * @{
 */
#define SOCK_IOC_GET_LOCAL_STATUS                           0x00000001uL  /**< Get the local socket status */
#define SOCK_IOC_GET_SEND_STATUS                            0x00000002uL  /**< Get the status of a send request */
#define SOCK_IOC_GET_RECEIVE_STATUS                         0x00000003uL  /**< Checks if new data has been received */
#define SOCK_IOC_CLOSE_TCP_CONNECTION                       0x00000004uL  /**< Closes a TCP connection */
#define SOCK_IOC_TCP_ACCEPT_FILTER                          0x00000005uL  /**< Defines which IP Addresses are accepted by the socket */
#define SOCK_IOC_UDP_ADD_MULTICAST                          0x00000006uL  /**< Join a multicast group */
#define SOCK_IOC_UDP_DEL_MULTICAST                          0x00000007uL  /**< Leave a multicast group */
/** @} */

/** \brief This structure holds the data for an IO Control Command
 */
typedef struct _T_SDAI_SOCK_IO_CONTROL
{
  U32   Command;  /**< Specifies the IO Control Command. Valid values: \ref IoctlCommand */

  union
  {
    T_SDAI_SOCK_IOC_GET_LOCAL_STATUS        GetLocalStatus;      /**< Local Socket Status informations */
    T_SDAI_SOCK_IOC_GET_SEND_STATUS         GetSendStatus;       /**< Send Status informations */
    T_SDAI_SOCK_IOC_GET_RECEIVE_STATUS      GetReceiveStatus;    /**< Receive Status informations */
    T_SDAI_SOCK_IOC_CLOSE_TCP_CONNECTION    CloseTcpConnection;  /**< TCP connection which shall be closed */
    T_SDAI_SOCK_IOC_TCP_ACCEPT_FILTER       TcpAcceptFilter;     /**< IP Address which shall be able to connect to the server */
    T_SDAI_SOCK_IOC_MULTICAST               AddMulticast;        /**< Multicast group to join */
    T_SDAI_SOCK_IOC_MULTICAST               DelMulticast;        /**< Multicast group to leave */

  } UseAs;

} T_SDAI_SOCK_IO_CONTROL;

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

#ifdef __cplusplus
  extern "C" {
#endif

/** \anchor Socket-Functions @name Socket Interface Functions
* All SDAI Socket Interface functions which can be used by the application */
//@{

/*--- initialization/termination functions ----------------------------------*/

extern T_SOCKET_RESULT  sdai_sock_init                  (T_SDAI_SOCK_INIT* pSocketInit);
extern T_SOCKET_RESULT  sdai_sock_term                  (void);

/*--- socket interface functions --------------------------------------------*/

extern T_SOCKET_RESULT  sdai_sock_create                (U32 Flags, T_SDAI_SOCK_ADDR* pRemoteAddress, T_SDAI_SOCK_ADDR* pLocalAddress, U16* pSocket);
extern T_SOCKET_RESULT  sdai_sock_close                 (U16 SocketId);
extern T_SOCKET_RESULT  sdai_sock_send                  (U16 SocketId, T_SDAI_SOCK_ADDR* pRemoteAddress, U16 DataLength, const U8* pData);
extern T_SOCKET_RESULT  sdai_sock_receive               (U16 SocketId, T_SDAI_SOCK_ADDR* pRemoteAddress, U16* pDataLength, U8* pData);
extern T_SOCKET_RESULT  sdai_sock_ioctl                 (U16 SocketId, T_SDAI_SOCK_IO_CONTROL* pIOControl);

/*--- socket helper functions -----------------------------------------------*/

extern U32              sdai_sock_htonl                 (U32 ControllerValue);
extern U32              sdai_sock_ntohl                 (U32 NetworkValue);
extern U16              sdai_sock_htons                 (U16 ControllerValue);
extern U16              sdai_sock_ntohs                 (U16 NetworkValue);

//@}

#ifdef __cplusplus
  }
#endif

#endif /* __SDAI_SOCKET_H__ */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
