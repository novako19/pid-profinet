/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

/**
 *
 * @page epl Ethernet Powerlink
 *
 * \tableofcontents
 *
 * @section epl_overview Overview
 * Ethernet Powerlink is a deterministic real-time ethernet protocol managed by the Ethernet
 * POWERLINK Standardization Group (EPSG). The communication profile is based on the CANopen technology,
 * a robust and proven protocol widely used throughout the automation world. The Softing Powerlink Slave
 * Controlled Node Protocol Software realizes a small and portable protocol stack for building
 * Powerlink applications.
 *
 * The following figure shows the structure of Softings Powerlink Controlled Node implementation.
 * The Simple Device Application Interface (SDAI) provides the services to communicate with
 * the Powerlink Protocol Software. The protocol stack implements the Powerlink communication profile
 * and accesses the Powerlink IP Core via a register and Dual-Ported RAM interface. The Powerlink IP Core
 * provides the interface to the Powerlink Fieldbus. It implements the MAC and Hub to realize the Data Link Layer.
 * As described in chapter \ref sdai_portability the SDAI application can run on the same or on a separate processor.
 *
 * @image html Powerlink_Structure.jpg
 *
 * @subsection epl_features Features
 * - Controlled Node Protocol Stack implementing Version 1.1.0 of the DS301 POWERLINK communication profile specification
 * - Poll Response Chaining supported
 * - Cross Traffic supported
 * - Dynamic PDO mapping supported
 * - SDO over Powerlink ASnd and UDP/IP
 * - Low jitter synchronization of the device application to the Powerlink communication cycle possible
 * - Object dictionary with standard objects of the communication profile area already implemented
 * - Integration of application objects possible via SDAI
 * - Implementation of any device profiles possible
 * - Integration of any TCP/UDP based protocols via a dedicated socket interface possible
 * - Demo Application with a dedicated device description file aimed at quickly getting you started with
 *   implementing your own Powerlink Controlled Node application
 *
 * @subsection epl_objects Supported Objects
 * The following objects of the communication profile area are already supported by the Powerlink Protocol Software:
 *
 * <TABLE><TR>
 * <TH>Object</TH><TH>Index</TH><TH>Description</TH>
 * </TR><TR>
 * <TD>NMT_DeviceType_U32</TD><TD>0x1000</TD><TD></TD>
 * </TR><TR>
 * <TD>ERR_ErrorRegister_U8</TD><TD>0x1001</TD><TD></TD>
 * </TR><TR>
 * <TD>NMT_CycleLen_U32</TD><TD>0x1006</TD><TD></TD>
 * </TR><TR>
 * <TD>NMT_ManufactDevName_VS</TD><TD>0x1008</TD><TD></TD>
 * </TR><TR>
 * <TD>NMT_ManufactHwVers_VS</TD><TD>0x1009</TD><TD></TD>
 * </TR><TR>
 * <TD>NMT_ManufactSwVers_VS</TD><TD>0x100A</TD><TD></TD>
 * </TR><TR>
 * <TD>NMT_StoreParam_REC</TD><TD>0x1010</TD><TD></TD>
 * </TR><TR>
 * <TD>NMT_IdentityObject_REC</TD><TD>0x1018</TD><TD></TD>
 * </TR><TR>
 * <TD>CFM_VerifyConfiguration_REC</TD><TD>0x1020</TD><TD></TD>
 * </TR><TR>
 * <TD>NMT_InterfaceGroup_Xh_REC</TD><TD>0x1030</TD><TD></TD>
 * </TR><TR>
 * <TD>SDO_SequLayerTimeout_U32</TD><TD>0x1300</TD><TD></TD>
 * </TR><TR>
 * <TD>PDO_RxCommParam_00h_REC</TD><TD>0x1400</TD><TD>Describes the mapping of application objects to RxPDOs (output data)</TD>
 * </TR><TR>
 * <TD>PDO_RxCommParam_01h_REC</TD><TD>0x1401</TD><TD>for Cross Traffic (optional)</TD>
 * </TR><TR>
 * <TD>PDO_RxCommParam_02h_REC</TD><TD>0x1402</TD><TD>for Cross Traffic (optional)</TD>
 * </TR><TR>
 * <TD>PDO_RxCommParam_03h_REC</TD><TD>0x1403</TD><TD>for Cross Traffic (optional)</TD>
 * </TR><TR>
 * <TD>PDO_RxMappParam_00h_AU64</TD><TD>0x1600</TD><TD>Describes the communication attributes of the TxPDO channels</TD>
 * </TR><TR>
 * <TD>PDO_RxMappParam_01h_AU64</TD><TD>0x1601</TD><TD>for Cross Traffic (optional)</TD>
 * </TR><TR>
 * <TD>PDO_RxMappParam_02h_AU64</TD><TD>0x1602</TD><TD>for Cross Traffic (optional)</TD>
 * </TR><TR>
 * <TD>PDO_RxMappParam_03h_AU64</TD><TD>0x1603</TD><TD>for Cross Traffic (optional)</TD>
 * </TR><TR>
 * <TD>PDO_TxCommParam_00h_REC</TD><TD>0x1800</TD><TD>Describes the mapping of application objects to TxPDOs (input data)</TD>
 * </TR><TR>
 * <TD>PDO_TxMappParam_00h_AU64</TD><TD>0x1A00</TD><TD>Describes the communication attributes of the TxPDO channels</TD>
 * </TR><TR>
 * <TD>DLL_CNLossSoC_REC</TD><TD>0x1C0B</TD><TD></TD>
 * </TR><TR>
 * <TD>DLL_CNCRCError_REC</TD><TD>0x1C0F</TD><TD></TD>
 * </TR><TR>
 * <TD>DLL_LossOfSocTolerance_U32</TD><TD>0x1C14</TD><TD></TD>
 * </TR><TR>
 * <TD>NWL_IpAddrTable_0h_REC</TD><TD>0x1E40</TD><TD></TD>
 * </TR><TR>
 * <TD>NWL_IpGroup_REC</TD><TD>0x1E4A</TD><TD></TD>
 * </TR><TR>
 * <TD>NMT_NodeAssignment_AU32</TD><TD>0x1F81</TD><TD></TD>
 * </TR><TR>
 * <TD>NMT_FeatureFlags_U32</TD><TD>0x1F82</TD><TD></TD>
 * </TR><TR>
 * <TD>NMT_EPLVersion_U8</TD><TD>0x1F83</TD><TD></TD>
 * </TR><TR>
 * <TD>NMT_CurrNMTState_U8</TD><TD>0x1F8C</TD><TD></TD>
 * </TR><TR>
 * <TD>NMT_PResPayloadLimitList_AU16</TD><TD>0x1F8D</TD><TD></TD>
 * </TR><TR>
 * <TD>NMT_EPLNodeID_REC</TD><TD>0x1F93</TD><TD></TD>
 * </TR><TR>
 * <TD>NMT_CycleTiming_REC</TD><TD>0x1F98</TD><TD></TD>
 * </TR><TR>
 * <TD>NMT_CNBasicEthernetTimeout_U32</TD><TD>0x1F99</TD><TD></TD>
 * </TR><TR>
 * <TD>NMT_HostName_VS</TD><TD>0x1F9A</TD><TD></TD>
 * </TR><TR>
 * <TD>NMT_MultiplCycleAssign_AU8</TD><TD>0x1F9B</TD><TD></TD>
 * </TR><TR>
 * <TD>NMT_ResetCmd_U8</TD><TD>0x1F9E</TD><TD></TD>
 * </TR></TABLE>
 *
 * @section epl_mapping Mapping Powerlink to SDAI
 * In the following chapters the mapping of the Powerlink Controlled Node Protocol Software to the SDAI is described.
 *
 * @subsection epl_init_data Powerlink Initialization
 * The structure #SDAI_INIT contains identification data, device specific data and a set of application specific
 * callback functions. The identification data are described by the structure #SDAI_IDENT_DATA and device specific
 * data are described by the structure #SDAI_DEVICE_DATA. These parameters are mapped to the standard Powerlink
 * objects (see also \ref epl_objects) in the following way:
 *
 * <TABLE><TR>
 * <TH>SDAI Ident Data</TH><TH>Powerlink Object</TH>
 * </TR><TR>
 * <TD>DevName</TD><TD>NMT_ManufactDevName_VS</TD>
 * </TR><TR>
 * <TD>IfName</TD><TD>---</TD>
 * </TR><TR>
 * <TD>Flags</TD><TD>NMT_FeatureFlags_U32</TD>
 * </TR><TR>
 * <TD>NodeId</TD><TD>NMT_EPLNodeID_REC.NodeID_U8</TD>
 * </TR><TR>
 * <TD>MacAddressDevice</TD><TD>---</TD>
 * </TR><TR>
 * <TD>SdoSequLayerTimeout</TD><TD>SDO_SequLayerTimeout_U32</TD>
 * </TR><TR>
 * <TD>Gateway</TD><TD>NWL_IpAddrTable_Xh_REC.DefaultGateway_IPAD</TD>
 * </TR><TR>
 * <TD>HostName</TD><TD>NMT_HostName_VSTR</TD>
 * </TR><TR>
 * <TD>ConfDate</TD><TD>CFM_VerifyConfiguration_REC.ConfDate_U32</TD>
 * </TR><TR>
 * <TD>ConfTime</TD><TD>CFM_VerifyConfiguration_REC.ConfTime_U32</TD>
 * </TR><TR>
 * <TD>IfcState</TD><TD>NMT_InterfaceGroup_Xh_REC.InterfaceAdminState_U8</TD>
 * </TR></TABLE>
 * <TABLE><TR>
 * <TH>SDAI Device Data</TH><TH>Powerlink Object</TH>
 * </TR><TR>
 * <TD>SerialNumber</TD><TD>NMT_IdentityObject_REC.SerialNo_U32</TD>
 * </TR><TR>
 * <TD>VendorID</TD><TD>NMT_IdentityObject_REC.VendorId_U32</TD>
 * </TR><TR>
 * <TD>Type</TD><TD>NMT_DeviceType_U32</TD>
 * </TR><TR>
 * <TD>ProductCode</TD><TD>NMT_IdentityObject_REC.ProductCode_U32</TD>
 * </TR><TR>
 * <TD>SoftwareVersion</TD><TD>NMT_ManufactSwVers_VS</TD>
 * </TR><TR>
 * <TD>HardwareVersion</TD><TD>NMT_ManufactHwVers_VS</TD>
 * </TR><TR>
 * <TD>MinorRevisionNumber</TD><TD>NMT_IdentityObject_REC.RevisionNo_U32</TD>
 * </TR><TR>
 * <TD>MajorRevisionNumber</TD><TD>NMT_IdentityObject_REC.RevisionNo_U32</TD>
 * </TR><TR>
 * <TD>ProductName</TD><TD>---</TD>
 * </TR><TR>
 * <TD>OrderId</TD><TD>---</TD>
 * </TR></TABLE>
 *
 * The structure #SDAI_CALLBACKS is used to register all supported application specific callback functions.
 * The following table shows which SDAI callback functions are used by Powerlink. If not supported by the
 * application the user shall set the respective function pointer to NULL.
 *
 * <TABLE><TR>
 * <TH>SDAI Callback</TH><TH>Powerlink</TH>
 * </TR><TR>
 * <TD>IdentDataCbk()</TD><TD>Called when the object NMT_StoreParam_REC is written to save network parameters</TD>
 * </TR><TR>
 * <TD>ExceptionCbk()</TD><TD>Called when a fatal error occurred</TD>
 * </TR><TR>
 * <TD>DataCbk()</TD><TD>Called when the output data or status of a unit has been changed</TD>
 * </TR><TR>
 * <TD>AlarmAckCbk()</TD><TD>Not used (set function pointer always to NULL)</TD>
 * </TR><TR>
 * <TD>WriteReqCbk()</TD><TD>Called when a SDO Download service request is received</TD>
 * </TR><TR>
 * <TD>ReadReqCbk()</TD><TD>Called when a SDO Upload service request is received</TD>
 * </TR><TR>
 * <TD>ControlReqCbk()</TD><TD>Called to:
                               - signal changes of the PDO mapping (control code #SDAI_CONTROL_CODE_CFG_DATA_INFO)
                               - handle write access to object NMT_ReStoreParam_REC (control code #SDAI_CONTROL_RESET_TO_DEFAULTS)
                               - reset application specific parameters (control code #SDAI_EPL_CONTROL_CODE_RESET_APPLICATION)</TD>
 * </TR><TR>
 * <TD>SyncSignalCbk()</TD><TD>Called when a synchronization signal is generated</TD>
 * </TR></TABLE>
 *
 * \note To allow a precise synchronization the SyncSignalCbk() is normally called directly from ISR context.
 *       Depending on the used RTOS special limitations exist, e.g. some RTOS functions can not be called.
 *
 * @subsection epl_plug_unit Powerlink plugging of units
 * In case of Powerlink a SDAI unit is equivalent to a Process Data Object (PDO). A PDO contains data attributes
 * of one or more application objects which will be transmitted cyclically as a single data unit. The mapping of
 * application objects to PDOs is defined via PDO mapping objects. Because this mapping is application specific it
 * must be specified by the user when calling sdai_plug_unit(). Within the passed structure #SDAI_EPL_CFG_DATA a description
 * of the mapped application object(s) has to be set. These informations must match to the corresponding entries in
 * the Powerlink device description file. Optionally a mapping version number can be set to specify the version of
 * the PDO mapping which will be verified during connection establishment. Additionally the  node ID of the corresponding
 * communication parameter must be specified. This is normally the Managing Node. To allow a reception of I/O data
 * from other CNs (cross traffic) also the respective node ID of the other CN can be specified here. The Powerlink
 * protocol stack need these informations to instantiate and initialize the corresponding PDO Mapping and the PDO
 * communication object (see also \ref epl_objects). A CN device can only have 1 TxPDO (input data). The protocol stack
 * supports up to 4 RxPDOs (output data). One RxPDO is reserved for the reception of output data from the Managing Node.
 * Additionally 3 RxPDOs are available to receive output data from other CNs. The following figure shows an example for the mapping of
 * SDAI I/O Units to PDOs.
 *
 * @image html powerlink_data_mapping.jpg
 *
 * By calling sdai_plug_unit() a PDO is automatically instantiated and added to the object dictionary. This function
 * returns a Unit ID which consists of the PDO Mapping object index and the SDAI unit index. The high word of the Unit ID
 * is the SDAI unit index. The low word of the Unit ID is the index number of the PDO Mapping object. See also \ref EplUnitId.
 *
 * @subsection epl_io_cfg_dynamic Powerlink Dynamic IO Configuration
 *
 * A dynamic PDO mapping is supported. In that case the respective PDO mapping object is accessed by the MN via SDO Download to
 * write the new mapping. Afterwards the application is informed about the new process data layout via a Control indication
 * with the control code #SDAI_CONTROL_CODE_CFG_DATA_INFO.
 *
 * @subsection epl_acyclic Powerlink Acyclic Communication
 * Acyclic communication is realized by Powerlink with the application layer protocol CANopen. This protocol provides the services
 * to access the object dictionary and its parameters. A object is addressed by Index and Subindex. Standard objects which must
 * be supported by every Powerlink Controlled Node are already implemented by the protocol stack (see also \ref epl_objects).
 * The user may implement application-specific objects which will be accessed via the SDAI callback functions. The following table
 * shows the mapping of the SDO services to SDAI callback functions:
 *
 * <TABLE><TR>
 * <TH>Service Name</TH><TH>SDAI Callback function</TH>
 * </TR><TR>
 * <TD>SDO Download</TD><TD>WriteReqCbk()</TD>
 * </TR><TR>
 * <TD>SDO Upload</TD><TD>ReadReqCbk()</TD>
 * </TR></TABLE>
 *
 * The size of the service interface is 512 Byte. The maximum data length to be written is therefore limited to:
 * 512 Byte - sizeof (struct SDAI_WRITE_IND). The maximum data length to be read is limited to:
 * 512 Byte - sizeof (struct SDAI_READ_RES).
 *
 * @subsection epl_errors Powerlink Service Error Codes
 * The following table shows how the SDAI common service error codes (see also \ref ServiceErrorCode) are mapped
 * to Powerlink specific service error codes (SDO Abort Code). This error mapping is needed for Read and Write responses.
 *
 * <TABLE><TR>
 * <TH>SDAI Service Error Code</TH><TH>SDO Abort Code</TH><TH>Description</TH>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_SUCCESS</TD><TD>0x00000000</TD><TD>No error</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_OBJECT_NOT_EXISTING</TD><TD>0x06020000</TD><TD>Object does not exist in the object directory</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_INVALID_SIZE</TD><TD>0x06070010</TD><TD>Length of service parameter does not match</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_STATE_CONFLICT</TD><TD>0x08000022</TD><TD>Data cannot be transferred or stored to the application because of the actual device state</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_APPLICATION</TD><TD>0x08000000</TD><TD>General error</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_RESOURCE</TD><TD>0x05040005</TD><TD>Out of memory</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_NOT_SUPPORTED</TD><TD>0x06010000</TD><TD>Unsupported access to an object</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_ACCESS_DENIED</TD><TD>0x06010002</TD><TD>Attempt to write to a read only object</TD>
 * </TR><TR>
 * <TD>#SDAI_SERVICE_ERR_INVALID_PARAMETER</TD><TD>0x06090030</TD><TD>Value range of parameter exceeded</TD>
 * </TR></TABLE>
 *
 * @subsection epl_diag_alarm Powerlink Diagnosis and Alarms
 * These features are not not supported by Powerlink. Thus the functions sdai_alarm_request(), sdai_diagnosis_request()
 * and the AlarmAckCbk() are not supported.
 *
 */

#ifndef __SDAI_EPL_H__
#define __SDAI_EPL_H__

/******************************************************************************
DEFINES
******************************************************************************/

/** \anchor EplUnitId @name SDAI Unit ID macros for Powerlink
* Format of the SDAI Unit ID for Powerlink:
\verbatim
    ------------------------------------------------------
    |     SDAI Unit Index     | PDO Mapping Object Index |
    ------------------------------------------------------
    32      High Word         16        Low Word         0
\endverbatim
*/
//@{
#define _SDAI_EPL_UNIT_INDEX_AND_PDO_INDEX_TO_ID(UnitIndex, PdoIndex)  ((U32) ((((U32)(UnitIndex)) << 16) | (PdoIndex))) /**< Converts the Powerlink PDO Mapping Index and the SDAI Unit Index to the SDAI Id */
#define _SDAI_EPL_ID_TO_UNIT_INDEX(Type, Id)                           ((Type) ((Id >> 16) & 0x0000FFFFuL))              /**< Converts the SDAI Id to the SDAI Unit Index */
#define _SDAI_EPL_ID_TO_PDO_INDEX(Type, Id)                            ((Type) ((Id) & 0x0000FFFFuL))                    /**< Converts the SDAI Id to the Powerlink PDO Mapping Index */
//@}

/*---------------------------------------------------------------------------*/

/** \anchor EplFlags @name Powerlink specific interface configuration
* Bitfield holding definitions for Powerlink specific interface configuration:
*/
//@{
#define SDAI_EPL_NODE_ID_SET_BY_HARDWARE    0x00  /**< The Node ID has been obtained via hw settings (e.g. Rotary switch). */
#define SDAI_EPL_NODE_ID_SET_BY_SOFTWARE    0x01  /**< The Node ID has been obtained via software (e.g. via webserver, from non-volatile memory or a fixed value). */
#define SDAI_EPL_STATIC_PDO_MAPPING         0x00  /**< Activates static PDO Mapping. IO configuration is fixed according to application settings. */
#define SDAI_EPL_DYNAMIC_PDO_MAPPING        0x02  /**< Activates dynamic PDO Mapping. IO configuration can be changed by MN. */
//@}

/** \anchor NodeId @name Node ID values
 *  Definitions for Node ID values. */
//@{
#define SDAI_EPL_CN_NODE_ID_MIN_VALUE                      1  /**< Minimum Node ID value for CNs */
#define SDAI_EPL_CN_NODE_ID_MAX_VALUE                    239  /**< Maximum Node ID value for CNs */
#define SDAI_EPL_MN_NODE_ID                              240  /**< Node ID of the MN */
//@}

#define SDAI_EPL_BASIC_ETHERNET_TIMEOUT_DISABLED  0   /**< Deactivates the Basic Ethernet Timeout Timer. The NMT state machine remains in state
                                                           NOT_ACTIVE in case of a Powerlink frame timeout. */
#define SDAI_EPL_CRC_THRESHOLD_COUNTING_DISABLED  0   /**< Deactivates CRC Threshold Counting. No error reaction will occur in case of CRC errors. */

/* Definitions for Powerlink specific device data */
#define SDAI_EPL_VERSION_MAX_LEN      32         /**< Maximum length of the Powerlink SW/HW version string */
#define SDAI_EPL_HOST_NAME_MAX_LEN    32         /**< Maximum length of the host name string */

/*---------------------------------------------------------------------------*/

/** \anchor PdoLimit @name PDO Limitations
 *  Supported number of PDO instances. */
//@{
#define SDAI_EPL_MAX_NUMBER_RXPDO_UNITS     4     /**< maximum number of supported RxPDOs which can be plugged as output units via sdai_plug_unit(). */
#define SDAI_EPL_MAX_NUMBER_TXPDO_UNITS     1     /**< maximum number of supported TxPDOs which can be plugged as input units via sdai_plug_unit(). */
//@}

#define SDAI_EPL_NODE_ID_MN_RX_UNICAST       0    /**< Node ID 0 is used for the reception of process data from the MN via the Poll Request frame (Unicast). */
#define SDAI_EPL_NODE_ID_MN_RX_MULTICAST     240  /**< Node ID 240 is used for the reception of process data from the MN via the Poll Response frame (Multicast). */
#define SDAI_EPL_MAX_NUMBER_MAPPED_OBJECTS   32   /**< maximum number of application objects which can be mapped to a PDO object */

#define SDAI_EPL_MAPPING_VERSION_NOT_AVAILABLE    0

/*---------------------------------------------------------------------------*/

/** \anchor EplControlCode @name Powerlink Control Codes
 * Powerlink specific definitions for Control Codes. */
//@{
#define SDAI_EPL_CONTROL_CODE_RESET_APPLICATION   0x0101  /**< This control indication is signaled to the application if the NMT command NMT_ResetNode is received.
                                                               The parameters of the manufacturer-specific profile area and of the standardised device profile area
                                                               shall be set to their PowerOn values. */
//@}

/*---------------------------------------------------------------------------*/

/** \anchor EplDataType @name Powerlink Basic Data Types
 *  Definitions for Powerlink specific basic and extended data types. See Powerlink specification for a description of these data types. */
//@{
#define SDAI_EPL_DATA_TYPE_BOOLEAN         0x0001
#define SDAI_EPL_DATA_TYPE_INTEGER8        0x0002
#define SDAI_EPL_DATA_TYPE_INTEGER16       0x0003
#define SDAI_EPL_DATA_TYPE_INTEGER32       0x0004
#define SDAI_EPL_DATA_TYPE_UNSIGNED8       0x0005
#define SDAI_EPL_DATA_TYPE_UNSIGNED16      0x0006
#define SDAI_EPL_DATA_TYPE_UNSIGNED32      0x0007
#define SDAI_EPL_DATA_TYPE_REAL32          0x0008
#define SDAI_EPL_DATA_TYPE_VISIBLE_STRING  0x0009
#define SDAI_EPL_DATA_TYPE_OCTET_STRING    0x000A
#define SDAI_EPL_DATA_TYPE_UNICODE_STRING  0x000B
#define SDAI_EPL_DATA_TYPE_TIME_OF_DAY     0x000C
#define SDAI_EPL_DATA_TYPE_TIME_DIFFERENCE 0x000D
#define SDAI_EPL_DATA_TYPE_INTEGER24       0x0010
#define SDAI_EPL_DATA_TYPE_REAL64          0x0011
#define SDAI_EPL_DATA_TYPE_INTEGER40       0x0012
#define SDAI_EPL_DATA_TYPE_INTEGER48       0x0013
#define SDAI_EPL_DATA_TYPE_INTEGER56       0x0014
#define SDAI_EPL_DATA_TYPE_INTEGER64       0x0015
#define SDAI_EPL_DATA_TYPE_UNSIGNED24      0x0016
#define SDAI_EPL_DATA_TYPE_UNSIGNED40      0x0018
#define SDAI_EPL_DATA_TYPE_UNSIGNED48      0x0019
#define SDAI_EPL_DATA_TYPE_UNSIGNED56      0x001A
#define SDAI_EPL_DATA_TYPE_UNSIGNED64      0x001B
//@}

/*---------------------------------------------------------------------------*/

/** \anchor NmtStatus @name NMT Status
 * Stack states of the Powerlink NMT state machine */
//@{
#define SDAI_EPL_NMT_STATE_INITIALISING             0x0019    /**< This is the first state the POWERLINK node shall enter after Power On */
#define SDAI_EPL_NMT_STATE_RESET_APPLICATION        0x0029    /**< In this state, the parameters of the manufacturer-specific profile area and of the
                                                                   standardised device profile area shall be set to their PowerOn values. For that purpose
                                                                   also a control indication with the control code #SDAI_EPL_CONTROL_CODE_RESET_APPLICATION
                                                                   is signaled to the application. */
#define SDAI_EPL_NMT_STATE_RESET_COMMUNICATION      0x0039    /**< In this state the parameters of the communication profile area are set to their PowerOn values. */
#define SDAI_EPL_NMT_STATE_RESET_CONFIGURATION      0x0079    /**< In this state the configuration parameters set in the object dictionary are used to generate
                                                                   the active device configuration. */
#define SDAI_EPL_NMT_STATE_NOT_ACTIVE               0x011C    /**< The CN only observes network traffic. Transmission of ethernet frames is not possible in this state. */
#define SDAI_EPL_NMT_STATE_PRE_OPERATIONAL_1        0x011D    /**< The CN only responds to SoA AsyncInvite frames received from the MN. Only identification and
                                                                   configuration of the CN is possible in this state. */
#define SDAI_EPL_NMT_STATE_STOPPED                  0x014D    /**< The CN is largely passive. This state is used for controlled shutdown of a selected CN while
                                                                   the system is still running. The node shall not participate in cyclic frame exchange, but still
                                                                   observes SoA frames. */
#define SDAI_EPL_NMT_STATE_PRE_OPERATIONAL_2        0x015D    /**< The CN waits for the configuration to be completed. Cyclic Poll Request and Poll Response frames
                                                                   are send in this state. The received process data will be ignored. The send process data are marked
                                                                   as invalid. */
#define SDAI_EPL_NMT_STATE_READY_TO_OPERATE         0x016D    /**< The CN signals its readiness to operation to the MN. Cyclic Poll Request and Poll Response frames
                                                                   are send in this state. The received process data will be ignored. The send process data are marked
                                                                   as valid. */
#define SDAI_EPL_NMT_STATE_OPERATIONAL              0x01FD    /**< Normal operating state of a CN. Cyclic Poll Request and Poll Response frames are send in this state.
                                                                   The received process data are used. The send process data are marked as valid. */
#define SDAI_EPL_NMT_BASIC_ETHERNET                 0x011E    /**< The CN may perform Legacy Ethernet communication according to IEEE 802.3. There is no POWERLINK
                                                                   specific network traffic control. */
//@}

/******************************************************************************
STRUCTURES
******************************************************************************/


struct SDAI_EPL_IP_SETTINGS
{
  U8    Gateway [4];                            /**< The Gateway of the Device. The user may set #SDAI_GATEWAY_UNUSED if not available. */
  char  HostName [SDAI_EPL_HOST_NAME_MAX_LEN];  /**< DNS host name of type VISIBLE_STRING32 (see Powerlink specification for details). */
};

struct SDAI_EPL_CONFIG_DATA
{
  U32   ConfDate;                            /**< Holds the local configuration date. It contains the number of days since January 1, 1984. */
  U32   ConfTime;                            /**< Holds the local configuration time. It contains the number of ms since midnight. */
  U8    IfcState;                            /**< The current administration state (Down/Up) of the interface. */
  U8    Alignment [3];
};

/** \brief Powerlink network specific parameter */
struct SDAI_EPL_IDENT_DATA
{
  U8                                Flags;                /**< Powerlink specific interface configuration flags. Valid values: \ref EplFlags */

  U8                                NodeId;               /**< The Node ID of the device. Unique identification number within a Powerlink network. The Node ID
                                                            * also specifies the Host ID of the IP address. The Net ID of the IP address is fixed to 192.168.100.
                                                            * Valid values: #SDAI_EPL_CN_NODE_ID_MIN_VALUE - #SDAI_EPL_CN_NODE_ID_MAX_VALUE */

  U8                                MacAddressDevice [6]; /**< The MAC address of the device */

  U32                               SdoSequLayerTimeout;  /**< Timeout value in milliseconds for the connection abort recognition of the SDO sequence layer */
  struct SDAI_EPL_IP_SETTINGS       IpSettings;           /**< IP specific settings */
  struct SDAI_EPL_CONFIG_DATA       CfgData;              /**< Configuration date */
};

/** \brief Powerlink device specific parameter. */
/* See also \ref epl_init_data. */
struct SDAI_EPL_DEVICE_DATA
{
  U8      SoftwareVersion [SDAI_EPL_VERSION_MAX_LEN];  /**< Manufacturer specific SW version string */
  U8      HardwareVersion [SDAI_EPL_VERSION_MAX_LEN];  /**< Manufacturer specific HW version string */
  U16     MajorRevisionNumber;                         /**< Major revision number of the device */
  U16     MinorRevisionNumber;                         /**< Minor revision number of the device */
};

/*---------------------------------------------------------------------------*/

/** \brief Powerlink specific address description */
struct SDAI_EPL_ADDR_DESCR
{
  U16   Index;            /**< Index in the object dictionary of the object which shall be read or written. */
  U8    SubIndex;         /**< SubIndex in the object dictionary of the object which shall be read or written. */
  U8    Alignment;
};

/*---------------------------------------------------------------------------*/

/** \brief Powerlink specific description of an application object which is mapped to a PDO object. */
struct SDAI_EPL_OBJ_DESCR
{
  U16   Index;       /**< Index number of the mapped object */
  U8    SubIndex;    /**< Subindex number of the mapped object */
  U8    Alignment;
  U16   BitOffset;   /**< Offset related to the start of PDO payload in bits */
  U16   BitLength;   /**< Length of the mapped object in bits */
};

/** \brief Powerlink specific configuration data. */
/** See also \ref epl_plug_unit. */
struct SDAI_EPL_CFG_DATA
{
  /** The NodeId is only used for output units (RxPDOs). In that case it defines the node which sends the corresponding frame
      with the process data. This is either the MN or another CN. The following Node IDs can be set by the application: \n
      - The MN sends a Poll Request frame for process data which are only used by this node (Unicast).
        To receive the process data transferred via this frame the NodeId must be set to #SDAI_EPL_NODE_ID_MN_RX_UNICAST.
      - In case of Poll-Response Chaining the MN send a Poll Response frame to all nodes (Multicast) instead of a Poll Request.
        This frame is dedicated to transfer data relevant for groups of CNs. To receive the process data transferred via this
        frame the NodeId must be set to #SDAI_EPL_NODE_ID_MN_RX_MULTICAST.
      - Other CNs are publishing its process data by sending a Poll Response frame (Multicast). To allow a reception of the
        process data from other CNs (direct slave-to-slave communication) the NodeId of the other CN must be set. Valid values:
        #SDAI_EPL_CN_NODE_ID_MIN_VALUE - #SDAI_EPL_CN_NODE_ID_MAX_VALUE
      For input units (TxPDOs) the passed NodeId is ignored. */
  U8                          NodeId;

  U8                          MappingVersion;                                    /**< The Mapping version is used to verify the compatibility of
                                                                                   * TxPDO channels and corresponding RxPDO channels. The assigned
                                                                                   * value of the Mapping version is application specific. */
  U8                          NumberMappedObjects;                               /**< Specifies the number of mapped objects which are described in the
                                                                                   * following array. Valid values: 0 - #SDAI_EPL_MAX_NUMBER_MAPPED_OBJECTS */
  U8                          Alignment;

  struct SDAI_EPL_OBJ_DESCR   ObjectDescr [SDAI_EPL_MAX_NUMBER_MAPPED_OBJECTS];  /**< Description of the objects which are mapped to the PDO */
};

/*---------------------------------------------------------------------------*/

/** \brief This structure holds the Powerlink specific additional status informations. */
struct SDAI_EPL_ADDSTATUS_DATA
{
  U16   NmtStatus;  /**< Status of the Powerlink NMT state machine. Valid values for the NmtStatus: \ref NmtStatus */
  U8    Alignment [2];
};

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

#endif /* __SDAI_EPL_H__ */

/*****************************************************************************/
