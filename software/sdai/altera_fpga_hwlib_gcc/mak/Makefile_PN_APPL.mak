.PHONY: $(COMPONENT) clean_$(COMPONENT)

################################################################################
# target files of this component
                
OBJECTS  = porting_pn_appl.o         \
           sdai_pn_appl.o            \
           sdai_interface_pn_appl.o  \
           sdai_drv_pn_pn_appl.o     \
           sdai_drv_socket_pn_appl.o

LIB = libSdaiPnAppl.a

################################################################################
# defaults

COMPONENT = pn_appl

include ../Makerules.mak

vpath %.c ../../../sdai/api
vpath %.c ../../../sdai/driver
vpath %.c ../../../sdai/interface
vpath %.c ../../

CCFLAGSAPPL +=  -DSDAI_INCLUDE_PNAK -DSDAI_INCLUDE_SOCKET

################################################################################
# dependencies

$(COMPONENT): $(LIB)
$(LIB)      : $(OBJECTS)

clean_$(COMPONENT):
	@echo "[CLEAN] $(COMPONENT)"
	-@rm -f $(LIB) $(OBJECTS)

################################################################################
