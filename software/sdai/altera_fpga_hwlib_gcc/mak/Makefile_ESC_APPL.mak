.PHONY: $(COMPONENT) clean_$(COMPONENT)

################################################################################
# target files of this component
                
OBJECTS  = porting_esc_appl.o        \
           sdai_esc_appl.o           \
           sdai_interface_esc_appl.o \
           sdai_drv_ecat_esc_appl.o  \
           sdai_drv_socket_esc_appl.o

LIB = libSdaiEscAppl.a

################################################################################
# defaults

COMPONENT = esc_appl

include ../Makerules.mak

vpath %.c ../../../sdai/api
vpath %.c ../../../sdai/driver
vpath %.c ../../../sdai/interface
vpath %.c ../../

CCFLAGSAPPL += -DSDAI_INCLUDE_ECAT -DSDAI_INCLUDE_SOCKET

################################################################################
# dependencies

$(COMPONENT): $(LIB)
$(LIB)      : $(OBJECTS)

clean_$(COMPONENT):
	@echo "[CLEAN] $(COMPONENT)"
	-@rm -f $(LIB) $(OBJECTS)

################################################################################
