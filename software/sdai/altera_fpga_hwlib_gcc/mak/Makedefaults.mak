################################################################################
.PHONY: check_environment update_version version
################################################################################
# default values for nios and gcc

export BSP_BASE_PATH = ../../../../Samples/sdai
export EDD_BASE_PATH = ../../../../DeviceDescription

ifeq ($(TARGET),)
# set the default target (others are altera_ink_switch and xilinx_iek_switch)
export TARGET=altera_cvsoc_pn
endif

CROSS_COMPILE = arm-altera-eabi-

export CC      = $(CROSS_COMPILE)gcc
export AR      = $(CROSS_COMPILE)ar
export NM      = $(CROSS_COMPILE)nm
export OBJCOPY = $(CROSS_COMPILE)objcopy
export LINKER  = $(CROSS_COMPILE)ld
export PERL    = perl

SOCEDS_ROOT ?= $(SOCEDS_DEST_ROOT)
HWLIBS_ROOT = $(SOCEDS_ROOT)/ip/altera/hps/altera_hps/hwlib

################################################################################
# target default make settings

export SDAI_INCLUDE_1 = ../../../include
export SDAI_INCLUDE_2 = ../../../sdai/include

export BSP_FLAGS      = --silent --settings=settings.bsp --bsp-dir=.

export CCFLAGSAPPL    = -c -g                              \
                        -I../..                            \
                        -I$(HWLIBS_ROOT)/include           \
                        -I$(SDAI_INCLUDE_1)                \
                        -I$(SDAI_INCLUDE_2)                \
                        -DPROCESSOR_APPL                   \
                        -DSYSTEM_BUS_WIDTH=32              \
                        -W -Wall -Wstrict-prototypes -Wno-trigraphs

export ARFLAGS        = -r -c
export LINKERFLAGS    = -r
export NMFLAGS        = -P
export OBJCOPYFLAGS   =

export MAKEOPTIONS    = --no-print-directory -r

################################################################################
# check environment variables and retrieve version information

first_target: all

check_environment:

################################################################################
