#! /bin/bash
#

clean () {
	make -f Makefile clean
}

###############################################################################

clean_ext () {
  source $1
  make -f Makefile clean
}

###############################################################################

cvsoc () {
  make -f Makefile esc_appl TARGET=altera_cvsoc_esc CROSS_COMPILE=arm-altera-eabi-
  make -f Makefile eip_appl TARGET=altera_cvsoc_eip CROSS_COMPILE=arm-altera-eabi-
  make -f Makefile mb_appl TARGET=altera_cvsoc_mb CROSS_COMPILE=arm-altera-eabi-
  make -f Makefile pn_appl TARGET=altera_cvsoc_pn CROSS_COMPILE=arm-altera-eabi-
  make -f Makefile epl_appl TARGET=altera_cvsoc_epl CROSS_COMPILE=arm-altera-eabi-
}

###############################################################################

build_ext () {
  source $1
  make -f Makefile $BUILD_TEMPLATE BUILD_DIR=$(echo "$BUILD_DIR" | sed 's/\\/\//g')

  if [ ! -d "$INSTALL_DIR"/lib ]; then mkdir -p "$INSTALL_DIR"/lib; fi
  if [ ! -d "$INSTALL_DIR"/include ]; then mkdir -p "$INSTALL_DIR"/include; fi

  install "$BUILD_DIR"/*.a "$INSTALL_DIR"/lib
  install ../../../include/*.h "$INSTALL_DIR"/include
}

###############################################################################

for command in $@; do
  case "$command" in
    cvsoc)
      clean; cvsoc
    ;;
    
    *.conf)
      clean_ext $command; build_ext $command
    ;;
      
    clean)
      clean
    ;;

    *)
      echo "Usage: $0 {cvsoc|clean}"
    ;;
  esac
done

if [ $# -eq 0 ]; then
  echo "Usage: $0 {cvsoc|clean}"
fi

