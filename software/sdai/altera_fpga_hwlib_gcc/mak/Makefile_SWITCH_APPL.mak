.PHONY: $(COMPONENT) clean_$(COMPONENT)

################################################################################
# target files of this component
                
OBJECTS  = porting_switch_appl.o         \
           sdai_switch_appl.o            \
           sdai_interface_switch_appl.o  \
           sdai_drv_eip_switch_appl.o    \
           sdai_drv_mb_switch_appl.o     \
           sdai_drv_pn_switch_appl.o     \
           sdai_drv_socket_switch_appl.o

LIB = libSdaiSwitchAppl.a

################################################################################
# defaults

COMPONENT = switch_appl

include ../Makerules.mak

vpath %.c ../../../sdai/api
vpath %.c ../../../sdai/driver
vpath %.c ../../../sdai/interface
vpath %.c ../../

CCFLAGSAPPL +=  -DSDAI_INCLUDE_EIP -DSDAI_INCLUDE_PNAK -DSDAI_INCLUDE_MBAK -DSDAI_INCLUDE_SOCKET

################################################################################
# dependencies

$(COMPONENT): $(LIB)
$(LIB)      : $(OBJECTS)

clean_$(COMPONENT):
	@echo "[CLEAN] $(COMPONENT)"
	-@rm -f $(LIB) $(OBJECTS)

################################################################################
