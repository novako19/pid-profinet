.PHONY: $(COMPONENT) clean_$(COMPONENT)

################################################################################
# target files of this component
                
OBJECTS  = porting_epl_appl.o        \
           sdai_epl_appl.o           \
           sdai_interface_epl_appl.o \
           sdai_drv_epl_epl_appl.o   \
           sdai_drv_socket_epl_appl.o

LIB = libSdaiEplAppl.a

################################################################################
# defaults

COMPONENT = epl_appl

include ../Makerules.mak

vpath %.c ../../../sdai/api
vpath %.c ../../../sdai/driver
vpath %.c ../../../sdai/interface
vpath %.c ../../

CCFLAGSAPPL += -DSDAI_INCLUDE_EPL -DSDAI_INCLUDE_SOCKET

################################################################################
# dependencies

$(COMPONENT): $(LIB)
$(LIB)      : $(OBJECTS)

clean_$(COMPONENT):
	@echo "[CLEAN] $(COMPONENT)"
	-@rm -f $(LIB) $(OBJECTS)

################################################################################
