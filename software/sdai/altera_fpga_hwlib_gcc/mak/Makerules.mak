################################################################################

include Makesettings.mak

################################################################################
# implicit rules for gcc and target nios

%_eip_appl.o: %.c
	@echo "  [CC]      $<"
	@$(CC) $(CCFLAGSAPPL) -o $*_eip_appl.o $<

libSdaiEipAppl.a:	
	@echo
	@echo "  [AR]      $@"
	@$(AR) $(ARFLAGS) $@ $(shell ls *_eip_appl.o)
	
#-----------------------------------------------------------------------------#

%_epl_appl.o: %.c
	@echo "  [CC]      $<"
	@$(CC) $(CCFLAGSAPPL) -o $*_epl_appl.o $<

libSdaiEplAppl.a:	
	@echo
	@echo "  [AR]      $@"
	@$(AR) $(ARFLAGS) $@ $(shell ls *_epl_appl.o)
	
#-----------------------------------------------------------------------------#

%_esc_appl.o: %.c
	@echo "  [CC]      $<"
	@$(CC) $(CCFLAGSAPPL) -o $*_esc_appl.o $<

libSdaiEscAppl.a:
	@echo
	@echo "  [AR]      $@"
	@$(AR) $(ARFLAGS) $@ $(shell ls *_esc_appl.o)
	
#-----------------------------------------------------------------------------#

%_mb_appl.o: %.c
	@echo "  [CC]      $<"
	@$(CC) $(CCFLAGSAPPL) -o $*_mb_appl.o $<

libSdaiMbAppl.a:	
	@echo
	@echo "  [AR]      $@"
	@$(AR) $(ARFLAGS) $@ $(shell ls *_mb_appl.o)

#-----------------------------------------------------------------------------#

%_pn_appl.o: %.c
	@echo "  [CC]      $<"
	@$(CC) $(CCFLAGSAPPL) -o $*_pn_appl.o $<

libSdaiPnAppl.a:	
	@echo
	@echo "  [AR]      $@"
	@$(AR) $(ARFLAGS) $@ $(shell ls *_pn_appl.o)

#-----------------------------------------------------------------------------#

%_pbdp_appl.o: %.c
	@echo "  [CC]      $<"
	@$(CC) $(CCFLAGSAPPL) -o $*_pbdp_appl.o $<

libSdaiPbsAppl.a:	
	@echo
	@echo "  [AR]      $@"
	@$(AR) $(ARFLAGS) $@ $(shell ls *_pbdp_appl.o)

#-----------------------------------------------------------------------------#

%_switch_appl.o: %.c
	@echo "  [CC]      $<"
	@$(CC) $(CCFLAGSAPPL) -o $*_switch_appl.o $<

libSdaiSwitchAppl.a:	
	@echo
	@echo "  [AR]      $@"
	@$(AR) $(ARFLAGS) $@ $(shell ls *_switch_appl.o)

################################################################################
