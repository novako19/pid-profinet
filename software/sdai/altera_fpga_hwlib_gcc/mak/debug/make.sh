#! /bin/sh
#

clean () {
	make -f Makefile clean
}

###############################################################################

cvsoc () {
  make -f Makefile esc_appl TARGET=altera_cvsoc_esc CROSS_COMPILE=arm-altera-eabi-
  make -f Makefile eip_appl TARGET=altera_cvsoc_eip CROSS_COMPILE=arm-altera-eabi-
  make -f Makefile mb_appl TARGET=altera_cvsoc_mb CROSS_COMPILE=arm-altera-eabi-
  make -f Makefile pn_appl TARGET=altera_cvsoc_pn CROSS_COMPILE=arm-altera-eabi-
  make -f Makefile epl_appl TARGET=altera_cvsoc_epl CROSS_COMPILE=arm-altera-eabi-
}

###############################################################################

for command in $@; do
  case "$command" in
    cvsoc)
      clean; cvsoc
    ;;

    clean)
      clean
    ;;

    *)
      echo "Usage: $0 {cvsoc|clean}"
    ;;
  esac
done

if [ $# -eq 0 ]; then
  echo "Usage: $0 {cvsoc|clean}"
fi

