/******************************************************************************

SOFTING AG
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

#ifndef __CFG_CC_H__
#define __CFG_CC_H__

#include <stdio.h>

#ifdef _DEBUG
#include <assert.h>
#endif

/******************************************************************************
DEFINES
******************************************************************************/

#define SDAI_DPRAM_SIZE                (1024 * 4)      /**< Size of the DPRAM used for the SDAI interface. Default: 4 kB*/

#define SDAI_SOCKET_IF_MEMORY_OFFSET   SDAI_DPRAM_SIZE /**< Offset of the socket interface within the DPRAM. */

/*===========================================================================*/

#ifndef NULL
  #define NULL                                      ((void*) 0)
#endif

#ifndef _ASSERT
  #ifdef _DEBUG
    #define _ASSERT(Expression)                     do { assert ((Expression)); } while (0)
  #else
    #define _ASSERT(Expression)
  #endif
#endif

#ifndef _ASSERT_PTR
  #ifdef _DEBUG
    #define _ASSERT_PTR(Expression)                 do { assert ((Expression) != NULL); } while (0)
  #else
    #define _ASSERT_PTR(Expression)
  #endif
#endif

#ifndef _NUMBER_ARRAY_ELEMENTS
  #define _NUMBER_ARRAY_ELEMENTS(Array)             (sizeof (Array) / sizeof ((Array) [0]))
#endif

/*===========================================================================*/

#undef _TRACE_ENABLE
#undef _DEBUG_ENABLE
#undef _WARN_ENABLE

#ifdef _TRACE_ENABLE
  #undef  _TRACE
  #define _TRACE(Arguments)           do { printf Arguments; printf ("\n"); } while (0)
#else
  #undef  _TRACE
  #define _TRACE(Arguments)           do { ; } while (0)
#endif

#ifdef _DEBUG_ENABLE
  #undef  _DEBUG_LOG
  #define _DEBUG_LOG(Arguments)       do { printf Arguments; printf ("\n"); } while (0)
#else
  #undef  _DEBUG_LOG
  #define _DEBUG_LOG(Arguments)       do { ; } while (0)
#endif

#ifdef _WARN_ENABLE
  #undef  _WARN_LOG
  #define _WARN_LOG(Arguments)       do { printf Arguments; printf ("\n"); } while (0)
#else
  #undef  _WARN_LOG
  #define _WARN_LOG(Arguments)       do { ; } while (0)
#endif

#undef  _ERROR_LOG
#define _ERROR_LOG(Arguments)         do { printf Arguments; printf ("\n"); } while (0)

/*===========================================================================*/

#define _USIGN32_HIGH_WORD(U32)                 ((U16) ((U32) >> 16))
#define _USIGN32_LOW_WORD(U32)                  ((U16)  (U32)       )

#define _USIGN16_HIGH_BYTE(U16)                 ((U8) ((U16) >> 8))
#define _USIGN16_LOW_BYTE(U16)                  ((U8)  (U16)      )

#define _HIGH_LOW_WORDS_TO_USIGN32(High,Low)    ((((U32) (High)) << 16) + (Low))
#define _HIGH_LOW_BYTES_TO_USIGN16(High,Low)    ((U16) ((((U16) (High)) << 8 ) + (Low)))

/*===========================================================================*/

#define _BITSET_RESET_BIT(pBS,BitNr)     ((pBS) [(unsigned) ((BitNr) >> 3)] &= (U8) (~(1u << ((BitNr) & 7u))))
#define _BITSET_SET_BIT(pBS,BitNr)       ((pBS) [(unsigned) ((BitNr) >> 3)] |= (U8)  (1u << ((BitNr) & 7u)))
#define _BITSET_GET_BIT(pBS,BitNr)      (((pBS) [(unsigned) ((BitNr) >> 3)]) & (U8)  (1u << ((BitNr) & 7u) ))

#define _BITSET_IS_BIT_SET(pBS,BitNr)   ((BOOL) _BITSET_GET_BIT(pBS,BitNr))

/*===========================================================================*/

#define CONTROLLER_BYTE_ORDERING_LITTLE_ENDIAN

#ifdef CONTROLLER_BYTE_ORDERING_LITTLE_ENDIAN
  #define _CONVERT_U16_TO_STACK_BYTE_ORDERING(Value16)     (Value16)
  #define _CONVERT_U32_TO_STACK_BYTE_ORDERING(Value32)     (Value32)
#endif

#define _CONVERT_U16_TO_APPL_BYTE_ORDERING(Value16) (_CONVERT_U16_TO_STACK_BYTE_ORDERING (Value16))
#define _CONVERT_U32_TO_APPL_BYTE_ORDERING(Value32) (_CONVERT_U32_TO_STACK_BYTE_ORDERING (Value32))

/*===========================================================================*/

#ifdef CONTROLLER_BYTE_ORDERING_LITTLE_ENDIAN
  #define _CONVERT_NETWORK_U32_TO_PROCESSOR_U32(Value32)    ((U32)((((U32)(Value32))                >> 24) | \
                                                                   ((((U32)(Value32)) & 0x00FF0000) >>  8) | \
                                                                   ((((U32)(Value32)) & 0x0000FF00) <<  8) | \
                                                                    (((U32)(Value32))               << 24) ))

  #define _CONVERT_NETWORK_U16_TO_PROCESSOR_U16(Value16)    ((U16)((U16)(Value16 >> 8) | (U16)(Value16 << 8)))
#else
  #define _CONVERT_NETWORK_U32_TO_PROCESSOR_U32(Value32)    (Value32)
  #define _CONVERT_NETWORK_U16_TO_PROCESSOR_U16(Value16)    (Value16)
#endif

#define _CONVERT_PROCESSOR_U16_TO_NETWORK_U16(Value16) (_CONVERT_NETWORK_U16_TO_PROCESSOR_U16 (Value16))
#define _CONVERT_PROCESSOR_U32_TO_NETWORK_U32(Value32) (_CONVERT_NETWORK_U32_TO_PROCESSOR_U32 (Value32))

/*===========================================================================*/

/**
_LOCATED_INTO_FAST_MEMORY: Is used for locating time critical functions in Tightly Coupled Memory */
#define _LOCATED_INTO_FAST_MEMORY

/*===========================================================================*/

#define _ALIGN4(Type,Value)     ((Type) ((((Type)(Value)) + (Type) 3) & ((Type)(-4))))

/*===========================================================================*/

#ifndef _MAX
  #define _MAX(A,B)                             ((A) > (B) ? (A) : (B))
#endif

#ifndef _MIN
  #define _MIN(A,B)                             ((A) < (B) ? (A) : (B))
#endif

/*===========================================================================*/

#define SDAI_INTERRUPT_SIGNAL        0x01
#define SDAI_DMA_IN_RUNNING_SIGNAL   0x02
#define SDAI_DMA_OUT_RUNNING_SIGNAL  0x04

#define SDAI_SYNC_SIGNAL_0           0x01
#define SDAI_SYNC_SIGNAL_1           0x02

#define SDAI_INTERRUPT_SIGNAL_ALL    (SDAI_INTERRUPT_SIGNAL | SDAI_DMA_OUT_RUNNING_SIGNAL)

#define SDAI_SYNC_SIGNAL_ALL         (SDAI_SYNC_SIGNAL_0 | SDAI_SYNC_SIGNAL_1)

extern void     sdai_memcpy (void*, const void*, unsigned long);
extern void     sdai_memset (void*, unsigned char, unsigned long);

#define memcpy sdai_memcpy
#define memset sdai_memset

/******************************************************************************
TYPEDEFS
******************************************************************************/

/******************************************************************************
GLOBAL DATA
******************************************************************************/

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

#endif /* __CFG_CC_H__ */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
