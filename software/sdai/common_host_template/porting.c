/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.75.00

******************************************************************************/

#include "sdai.h"

#include "cfg_cc.h"

#ifdef SDAI_INCLUDE_SOCKET
  #include "sdai_socket.h"
#endif

#include "sdai_interface.h"
#include "socket_interface.h"
#include "sdai_drv.h"

#include "porting.h"

/**
 * @ingroup template_porting
 * @{
 */

/******************************************************************************
DEFINES
******************************************************************************/

/******************************************************************************
TYPEDEFS
******************************************************************************/

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

/******************************************************************************
GLOBAL DATA
******************************************************************************/

/******************************************************************************
PRIVATE DATA
******************************************************************************/

/******************************************************************************
PUBLIC FUNCTIONS
******************************************************************************/

/**
 * @desc The function @anchor send_signal_stack() is a dummy function and is not used on
 *       application side. This dummy implementation avoids unresolved external
 *       error during linking process.
 * @return
 * - type  : void
 */
void send_signal_stack (U8 Signal)
{
  _TRACE (("send_signal_stack"));

  return;
}

/*===========================================================================*/

/**
 * @desc The function @anchor send_signal_application() sends a signals from the application
 *       to the stack CPU. Therefore the application CPU must generate an edge on the interrupt
 *       pin of the stack CPU.
 * @return
 * - type  : void
 */
void send_signal_application (U8 Signal)
{
  _TRACE (("send_signal_application"));

  /* generate an edge on the interrupt pin of the stack CPU */

  return;
}


/*===========================================================================*/

/**
 * @desc The function @anchor sdai_protocol_stack_task_main_loop() is a dummy function on the
 *       application side. This dummy implementation avoids unresolved external
 *       error during linking process.
 * @return
 * - type  : void
 */
void sdai_protocol_stack_task_main_loop (void)
{
  _TRACE (("sdai_protocol_stack_task_main_loop"));

  return;
}

/*===========================================================================*/

 /**
  * @desc The function @anchor interface_isr() is the interrupt handler on the application CPU if the
  *       The function clears and acknowledges the interrupt and notifies the
  *       "SDAI-interface" module about the signal (see also functions interface_receive_signal()
  *       and interface_receive_data_signal()).
  *       The function parameters and return values have to be adapted to the system.
  *       It can be executed at interrupt or post-interrupt level.
  * @return
  * - type  : void
  */
void interface_isr (...)
{
  _TRACE (("interface_isr"));

  /* read the interrupt source */
  Signal = ...;

  do
  {
    /* acknowledge the interrupt */

    if (Signal & SDAI_DMA_OUT_RUNNING_SIGNAL)
    {
      /* wait until DMA is finished */
      do
      {
        /* reread the interrupt source */

      } while (Signal & SDAI_DMA_OUT_RUNNING_SIGNAL);

      /* clear it again, because the falling edge generates also an IRQ */

      /* notify the SDAI platform independend code about the IRQ */
      interface_receive_data_signal (INTERFACE_STACK);
    }

    if (Signal & SDAI_INTERRUPT_SIGNAL)
    {
      /* notify the SDAI platform independend code about the IRQ */
      interface_receive_signal (INTERFACE_STACK);
    }

    /* reread the interrupt source */
    Signal = ...;

  } while (Signal & SDAI_INTERRUPT_SIGNAL_ALL);

  return;
}

/*===========================================================================*/

/**
  * @desc The function @anchor sync_isr() is the seperate SYNC interrupt handler on the application CPU if the
  *       The function clears and acknowledges the interrupt and notifies the
  *       "SDAI-interface" module about the signal (see also function interface_receive_synch_signal()).
  *       The function parameters and return values have to be adapted to the system.
  *       It can be executed at interrupt or post-interrupt level.
  * @return
  * - type  : void
  */
void sync_isr (...)
{
  _TRACE (("sync_isr"));

  /* read the interrupt source */
  Signal = ...;

  /* acknowledge the interrupt */

  /* notify the SDAI platform independend code about the IRQ */
  interface_receive_synch_signal (Signal);

  return;
}

/*===========================================================================*/

/**
 * @desc The function @anchor interrupt_lock() disables the global interrupt handling.
 * @return
 * - type  : void
 */
void interrupt_lock (void)
{
  _TRACE (("interrupt_lock"));

  return;
}

/*===========================================================================*/

/**
 * @desc The function @anchor interrupt_unlock() enables the global interrupt handling.
 * @return
 * - type  : void
 */
void interrupt_unlock (void)
{
  _TRACE (("interrupt_unlock"));

  return;
}

/*===========================================================================*/

/**
 * @desc The function @anchor init_resources() initializes the necessary resources
 *       needed by the respective interface, e.g. interrupts, mutex cores, event flags,
 *       timers. The function is called from the stack and application side and
 *       returns the address of the memory interface between both sides.
 *
 * @return Returns the base address of the SDAI shared RAM interface
 * - type  : T_SHARED_RAM_INTERFACE*
 * - values: #SDAI_DPRAM_BASE
 * @param[in] InterfaceSide
 * - type : int
 * - range: #INTERFACE_APPLICATION | #INTERFACE_STACK
 * @param[in] EnableSynchSignal
 * - type : BOOL
 * - range: TRUE, FALSE
 * @remarks Check and execute initialization only in case of InterfaceSide == #INTERFACE_APPLICATION
 */
T_SHARED_RAM_INTERFACE* init_resources (int InterfaceSide, BOOL EnableSynchSignal)
{
  _TRACE (("init_resources"));

  /* initialize the interrupts PIOs / GPIO / ... */

  /* register the interrupts */

  /* enabled the interrupts */

  /* clear the hardware mutex cores */

  /* return address of the dual port ram */
  return((T_SHARED_RAM_INTERFACE*) SDAI_DPRAM_BASE);
}

/*===========================================================================*/

/**
 * @desc The function @anchor term_resources() frees used resources by the respective interface,
 *       e.g. interrupts, mutex cores, event flags, timers. The function is called
 *       from the stack and application side.
 * @return
 * - type  : void
 * @param[in] InterfaceSide
 * - type : int
 * - range: #INTERFACE_APPLICATION | #INTERFACE_STACK
 * @remarks Check and execute initialization only in case of InterfaceSide == #INTERFACE_APPLICATION
 */
void term_resources (int InterfaceSide)
{
  _TRACE (("term_resources"));

  /* disable the interrupts */

  /* unregister the interrupts */

  return;
}

/*===========================================================================*/

/**
 * @desc The function @anchor enable_sdai_interrupt() unmask the SDAI hardware interrupt.
 *       Other interrupts are untouched.
 * @return
 * - type  : void
 */
void enable_sdai_interrupt (void)
{
  /* enabled the interrupts used by SDAI */

  return;
}

/*===========================================================================*/

/**
 * @desc The function @anchor disable_sdai_interrupt() mask the SDAI hardware interrupt.
 *       Other interrupts are untouched.
 * @return
 * - type  : void
 */
void disable_sdai_interrupt (void)
{
  /* disabled the interrupts used by SDAI */

  return;
}

/*===========================================================================*/

/**
 * @desc The function @anchor mutex_lock() claims the requested hardware mutex for the CPU.
 *       If the mutex is locked by the other CPU the function blocks until it becomes free.
 *       This function is called from the application and stack side.
 * @return
 * - type  : void
 * @param[in] InterfaceArea
 * - type : T_INTERFACE_AREA
 * - range: #MANAGEMENT_INTERFACE |
 *          #STATUS_INTERFACE |
 *          #SERVICE_REQ_RES_INTERFACE |
 *          #SERVICE_IND_INTERFACE |
 *          #UNIT_INTERFACE |
 *          #RESOURCE_INTERFACE
 */
void mutex_lock (T_INTERFACE_AREA InterfaceArea)
{
  _TRACE (("mutex_lock"));

  /* get address of the hardware mutex */

  /* prepare the requested new mutex state */

  /* loop until the real mutex state matches the requested state */
  do
  {
    /* write the requsted new state to the mutex */

    /* read the current mutex state */

  } while (...);

  return;
}

/*===========================================================================*/

/**
 * @desc The function @anchor mutex_unlock() frees a hardware mutex owned by the CPU.
 *       This function is called from the application and the stack side.
 * @return
 * - type  : void
 * @param[in] InterfaceArea
 * - type : T_INTERFACE_AREA
 * - range: #MANAGEMENT_INTERFACE |
 *          #STATUS_INTERFACE |
 *          #SERVICE_REQ_RES_INTERFACE |
 *          #SERVICE_IND_INTERFACE |
 *          #UNIT_INTERFACE
 */
void mutex_unlock(T_INTERFACE_AREA InterfaceArea)
{
  _TRACE (("mutex_unlock"));

  /* get address of the hardware mutex */

  /* prepare the requested new mutex state by setting the value part to zero */

  /* write the requested new state to the mutex */

  return;
}

/*===========================================================================*/

/**
 * @desc The function @anchor mutex_clear() frees a hardware mutex regardless of which
 *       CPU currently ownes the mutex. This function is called by the application
 *       side on initialization to set the mutex cores to a defined state.
 * @return
 * - type  : void
 * @param[in] InterfaceArea
 * - type : T_INTERFACE_AREA
 * - range: #MANAGEMENT_INTERFACE |
 *          #STATUS_INTERFACE |
 *          #SERVICE_REQ_RES_INTERFACE |
 *          #SERVICE_IND_INTERFACE |
 *          #UNIT_INTERFACE
 */
static void mutex_clear (T_INTERFACE_AREA InterfaceArea)
{
  _TRACE (("mutex_clear"));

  /* get address of the hardware mutex */

  /* read the current state of the mutex and clear the value */

  /* clear the reset bit in the reset register */

  /* write back the state with value 0 so the mutex is free */

  return;
}

/*===========================================================================*/


/**
 * @desc The function @anchor check_watchdog_supported() returns if the watchdog is
 *       supported by the hardware or not.
 * @return
 * - type  : U8
 * - values: SDAI_SUCCESS | SDAI_ERR_INTERNAL
 */
U8 check_watchdog_supported (void)
{
  _TRACE (("check_watchdog_supported"));

  return (SDAI_ERR_INTERNAL);
}

/*===========================================================================*/

/**
 * @desc The function @anchor handle_stack_watchdog_expired() handle platform specific actions
 *       on the expiration of the stack watchdog. It clears the stack ready flag to allow save call
 *       of sdai_deinit() and on the FPGA resets the stack CPU.
 * @return
 * - type  : void
 */
void handle_stack_watchdog_expired ()
{
  _TRACE (("handle_stack_watchdog_expired"));

  return;
}


/*===========================================================================*/

/**
 * @desc The function @anchor create_stack_task() blocks until the stack signals ready.
 *       The function is called from the application side.
 * @return
 * - type  : void
 * @param[in] task_main_loop
 * - type : function pointer
 * - range: whole address space
 */
void create_stack_task (void (*task_main_loop) (void))
{
  _TRACE (("create_stack_task"));

  /* clear application and stack ready flag */

  /* loop until the fieldbus protocol return startup result */
  do
  {
    /* set application ready flag */

  } while (...);

  return;
}

/*===========================================================================*/

/**
 * @desc The function @anchor delete_stack_task() blocks until the stack clears the ready signal.
 *       This function is called from the application side.
 * @return
 * - type  : void
 * @pre The signal #INTERFACE_SIGNAL_STOP_STACK musst be send to the stack CPU
 *      before the call of the funtion.
 */
void delete_stack_task (void)
{
  _TRACE (("delete_stack_task"));

  /* check if stack has cleared the ready flag */

  /* clear application ready flag */

  return;
}

/*===========================================================================*/

/**
 * @desc The function @anchor wait_application_startup_finished() is a dummy function and is not used on
 *       application side. This dummy implementation avoids unresolved external
 *       error during linking process.
 * @return
 * - type  : U8
 */
U8 wait_application_startup_finished (void)
{
  _TRACE (("wait_application_startup_finished"));

  return (0u);
}

/*===========================================================================*/

/**
 * @desc The function @anchor create_stack_task_finished() is a dummy function and is not used on
 *       application side. This dummy implementation avoids unresolved external
 *       error during linking process.
 * @return
 * - type  : void
 */
void create_stack_task_finished (void)
{
  return;
}

/*===========================================================================*/

/**
 * @desc The function @anchor delete_stack_task_finish() is a dummy function and is not used on
 *       application side. This dummy implementation avoids unresolved external
 *       error during linking process.
 * @return
 * - type  : void
 */
void delete_stack_task_finish (void)
{
  _TRACE (("delete_stack_task_finish"));

  return;
}

/*===========================================================================*/

/**
 * @desc The function @anchor convert_network_u32_to_processor_u32() converts data of type U32
 *       stored in network format to the format of this CPU.
 *
 * @return
 * - type : U32
 * - range: whole range
 * @param[in] SrcValue
 * - type : U32
 * - range: whole range
 */
U32 convert_network_u32_to_processor_u32 (U32 SrcValue)
{
  return (_CONVERT_NETWORK_U32_TO_PROCESSOR_U32 (SrcValue));
}

/*===========================================================================*/

/**
 * @desc The function @anchor convert_network_u16_to_processor_u16() converts data of type U16
 *       stored in network format to the format of this CPU.
 *
 * @return
 * - type : U16
 * - range: whole range
 * @param[in] SrcValue
 * - type : U16
 * - range: whole range
 */
U16 convert_network_u16_to_processor_u16 (U16 SrcValue)
{
  return (_CONVERT_NETWORK_U16_TO_PROCESSOR_U16 (SrcValue));
}

/*===========================================================================*/

/**
 * @desc The function @anchor convert_processor_u32_to_network_u32() converts data of type U32
 *       stored in CPU format to the format of this network.
 *
 * @return
 * - type : U32
 * - range: whole range
 * @param[in] SrcValue
 * - type : U32
 * - range: whole range
 */
U32 convert_processor_u32_to_network_u32 (U32 SrcValue)
{
  return (_CONVERT_PROCESSOR_U32_TO_NETWORK_U32 (SrcValue));
}

/*===========================================================================*/

/**
 * @desc The function @anchor convert_processor_u16_to_network_u16() converts data of type U16
 *       stored in CPU format to the format of this network.
 *
 * @return
 * - type : U16
 * - range: whole range
 * @param[in] SrcValue
 * - type : U16
 * - range: whole range
 */
U16 convert_processor_u16_to_network_u16 (U16 SrcValue)
{
  return (_CONVERT_PROCESSOR_U16_TO_NETWORK_U16 (SrcValue));
}

/******************************************************************************
PROTECTED FUNCTIONS
******************************************************************************/

/******************************************************************************
PRIVATE FUNCTIONS
******************************************************************************/

/** @} */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
