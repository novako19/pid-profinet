
/**
 * @mainpage
 *
 * @section copyright_sec Copyright
 * 2005-2015 SOFTING Industrial Automation GmbH @n @n
 * No part of these instructions may be reproduced (printed material, photocopies,
 * microfilm or other method) or processed, copied or distributed using electronic
 * systems in any form whatsoever without prior written permission of SOFTING AG.
 * The producer reserves the right to make changes to the scope of supply as well
 * as changes to technical data, even without prior notice. A great deal of attention
 * was made to the quality and functional integrity in designing, manufacturing and
 * testing the system. However, no liability can be assumed for potential errors that
 * might exist or for their effects. Should you find errors, please inform your
 * distributor of the nature of the errors and the circumstances under which they occur.
 * We will be responsive to all reasonable ideas and will follow up on them,
 * taking measures to improve the product, if necessary.
 * We call your attention to the fact that the company name and trademark as
 * well as product names are, as a rule, protected by trademark, patent and product
 * brand laws. @n
 * All rights reserved.
 *
 * @section contents_sec Contents
 * - @subpage porting_getting_started
 * - Porting Layer
 *   - @subpage porting_startup_handshake
 *   - @subpage porting_shutdown_handshake
 *   - @subpage porting_mutex
 *   - @subpage porting_signal
 *   - @subpage porting_endianness
 *   - @subpage porting_watchdog
 */

/** @page porting_getting_started Getting Started
 * @section porting_general General Porting
 * The SDAI is designed for port to very different hardware and operating system platforms.
 * It does not matter whether the application and the protocol stack are running on the same
 * processor or whether they are executed on separate processors. @n@n
 * The Pictures below show the two general architectures of a SDAI Application.
 * @image html 1prozessor.jpg "SDAI One CPU Architecture"
 * @image html 2prozessor.jpg "SDAI Two CPU Architecture"
 * Out of this flexibility some requirements regarding the portability (depending on the architecture) come up.
 * The aim of this document is to describe the different requirements for a two CPU solution while a porting process.
 * The One CPU architecture is not described due to the fact that this also requires that the related communication
 * stack is ported to the target platform.@n@n
 * The document is structured as follows
 *  - The subsequent sections describe the porting process in general.
 *  - The Pages of the Porting Layer + the Modules Section describe the Porting Interface to adapt in detail.
 *  - Last the adaptation of the Build Process is described.
 * @section porting_arch Architectures
 * As noted above single as well as double CPU environments are possible. As single CPU architectures
 * are proposed for "low performance" applications the double CPU architectures allow more complex
 * applications. The disadvantages of double/multi CPU environments are more complicated synchronization
 * and more complicated process communication. Therefore a very general communication channel is chosen
 * consisting of a common shared-RAM and two notification channels/signals. This allows that the same
 * software can be used at single and double CPU environments with only a few adaptations.@n
 * The picture below shows a more detailed view of a double CPU environment.
 * @image html DpRamArchSdai.png "Detailed SDAI Architecture"
 * Additionally this allows multiple hardware platform combinations. As an example a Hardware Platform can be used
 * which is based on a FPGA with the usage of Softcore Processors.
 * The flexibility given by the FPGA can be used to instantiate single and double CPU applications. As well
 * as double CPU applications with a internal Softcore CPU and a second external CPU. Independend from the
 * Architecture the shared-RAM (DP-RAM, Dual Ported RAM) Interface is always available inside of the FPGA.
 * This also leads to different requirements regarding to the memory interface and synchronization
 * directives.
 * All Hardware Architecture/Platform specific functionalities regarding the communication between two
 * CPUs or Threads is implemented in the so called Porting Layer defined by File <tt>porting.h</tt>.
 * The Task of porting the SDAI Application library is to implement the Functions in the corresponding
 * <tt>porting.c</tt> File with respect to the current Architecture. Further on the first CPU
 * is called the Application CPU which runs the User Application and the second CPU is called the Stack
 * CPU.@n
 * Additionally the Build Process must be adapted e.g. the compiler path and settings.@n
 * The Porting Layer Functions to adapt are described in Chapters:
 * - @subpage porting_startup_handshake
 * - @subpage porting_shutdown_handshake
 * - @subpage porting_mutex
 * - @subpage porting_signal
 * - @subpage porting_endianness
 * - @subpage porting_watchdog
 *
 * These Chapters also include the Requirements regarding to the Architecture.
 *
 * @section porting_example SDAI Organization
 * Appended to this document comes a example implementation and structure of the SDAI named
 * <b>common_host_template</b>. This contains a complete makefile structure as well as a example
 * template for <tt>porting.c</tt>.
 * @verbatim
  ./
  |
  |- common_host_template/
  |  |
  |  |- Makefile                      // Makefile, Build Settings and Rules
  |  |- cfg_cc.h                      // cfg_cc.h Template
  |  |- porting.c                     // porting.c Template
  |
  |- include/                         // General SDAI Includes
  |- sdai/                            // SDAI Source Code
  |  |
  |  |- include                       // SDAI Includes
  |  |- ...
  |
  |- ...
 @endverbatim
 * @section porting_getting_started_reading Further Reading
 * - SDAI Manual
 */

/** @page porting_startup_handshake Startup of Stack & Application with Handshaking
 * @section porting_startup_general General
 * In multi CPU environments the Stack and Application are distributed over 2 CPUs. In order
 * to keep the shared Data (RAM) consistent a special handshaking mechanism is needed at startup.@n
 * Special requirements occur when different types of CPUs work together. In these cases problems
 * like Endianness & non-synchronous Clocks come up.
 * @section porting_startup_requirements Requirements
 * <b></b>
 * - The sequence which CPU is ready earlier or later mustn't have an effect
 * - By default two Flags are defined in the Shared-Ram <tt>StackReady</tt> & <tt>ApplReady</tt>@n (@ref T_INTERFACE_MANAGEMENT_DATA::StackReady "T_SHARED_RAM_INTERFACE::ManagementData.StackReady" & @ref T_INTERFACE_MANAGEMENT_DATA::ApplReady "T_SHARED_RAM_INTERFACE::ManagementData.ApplReady")
 * - The <tt>StackReady</tt> Flag #SDAI_INTERFACE_STACK_READY signals to the Application that the Stack is ready
 * - The <tt>ApplReady</tt> Flag #SDAI_INTERFACE_APPLICATION_READY signals to the Stack that the Application is ready
 * - The Stack startup is completed after the Application is ready
 * - By default the Porting Layer defines three Interface Functions @ref create_stack_task(), @ref create_stack_task_finished() & @ref wait_application_startup_finished()
 * - @ref create_stack_task()
 *   - called from the Application side
 *   - must block until the stack is running and signals <tt>StackReady</tt> via #SDAI_INTERFACE_STACK_READY
 *   - On a multi CPU environment the stack is already running on a second CPU. The task of @ref create_stack_task() is to signal to the Stack that the Application is ready (e.g. set the ApplReady Flag).
 * - @ref create_stack_task_finished()
 *   - called from the Stack side
 *   - The <tt>StackReady</tt> Flag is already set internally in the Stack
 *   - called after the Stack is initialized completely
 * - @ref wait_application_startup_finished()
 *   - called form the Stack side
 *   - used to check whether the Application is ready and waiting for the Stack
 * @section porting_startup_handshake_example Handshaking Example
 * As an example for a startup with handshaking the picture below is given. It shows a sequence of @ref create_stack_task(), @ref create_stack_task_finished() & @ref wait_application_startup_finished()
 * function calls at a two threads/CPU environment. The first one is the Application Thread/CPU represented with color blue. The second one is the Stack Thread/CPU represented with color black.@n
 * In this example the shared variables <tt>StackReady</tt> & <tt>ApplReady</tt> are used for synchronization. The Method @ref wait_application_startup_finished() tests the transition of the <tt>ApplReady</tt> Flag.
 * It clears the flag and waits afterwards until it's set again. Function @ref create_stack_task() must be implemented accordingly and sets the <tt>ApplReady</tt> Flag in a loop until the <tt>StackReady</tt> Flag
 * is set by the stack. This is done Stack internally right before @ref create_stack_task_finished() is called at the Stack CPU.@n
 * Now is ensured that both CPUs are fully initialized and the shared-RAM can be used.
 * @msc
 * hscale = "2";
 * "STACK", "STACK::Interface", "Porting" , "APPLICATION::Interface" [arclinecolor=blue, arctextcolor=blue], "APPLICATION" [arclinecolor=blue, arctextcolor=blue];
 * "STACK"=>"STACK::Interface";
 * "STACK::Interface"=>"Porting" [label="wait_application_startup_finished()"];
 * "Porting"<-"Porting" [label="clear ApplReady"];
 * ---[label="Wait until Application is ready, signaled by ApplReady"];
 * ...;
 * "APPLICATION::Interface"<="APPLICATION";
 * "APPLICATION::Interface"->"APPLICATION::Interface" [label="clear StackReady & ApplReady"];
 * "Porting"<="APPLICATION::Interface" [label="create_stack_task()"];
 * "Porting"->"Porting" [label="set ApplReady",linecolor=blue,textcolor=blue];
 * "STACK::Interface"<<"Porting";
 * "STACK"<<"STACK::Interface";
 * ...;
 * ---[label="Wait until Stack startup finished, signaled by StackReady"];
 * "STACK"=>"STACK::Interface";
 * "STACK::Interface"->"STACK::Interface" [label="set StackReady"];
 * "Porting">>"APPLICATION::Interface" [linecolor=blue,textcolor=blue];
 * "APPLICATION::Interface">>"APPLICATION";
 * "STACK::Interface"=>"Porting" [label="create_stack_task_finished()"];
 * "STACK::Interface"<<"Porting";
 * "STACK"<<"STACK::Interface";
 * @endmsc
 * <CENTER><b>Sequence Chart for the Stack Thread (black) and Application Thread (blue) startup. The handshaking is done using the shared variables <tt>ApplReady</tt> & <tt>StackReady</tt>.</b></CENTER>
 */

/** @page porting_shutdown_handshake Shutdown of Stack & Application with Handshaking
 * @section porting_shutdown_general General
 * In multi CPU environments the Stack and Application are distributed over 2 CPUs. In order
 * to keep the shared Data (RAM) consistent a special handshaking mechanism is needed at shutdown.@n
 * Special requirements occur when different types of CPUs work together. In these cases problems
 * like Endianness & non-synchronous Clocks come up.
 * @section porting_shutdown_requirements Requirements
 * - The sequence which CPU is ready earlier or later mustn't have an effect
 * - By default two Flags are defined in the Shared-Ram <tt>StackReady</tt> & <tt>ApplReady</tt>@n (@ref T_INTERFACE_MANAGEMENT_DATA::StackReady "T_SHARED_RAM_INTERFACE::ManagementData.StackReady" & @ref T_INTERFACE_MANAGEMENT_DATA::ApplReady "T_SHARED_RAM_INTERFACE::ManagementData.ApplReady")
 * - The <tt>StackReady</tt> Flag #SDAI_INTERFACE_STACK_READY signals to the Application that the Stack is ready
 * - The <tt>ApplReady</tt> Flag #SDAI_INTERFACE_APPLICATION_READY signals to the Stack that the Application is ready
 * - The shutdown is initiated from the Application
 * - The shutdown request is signaled to the Stack by the #INTERFACE_SIGNAL_STOP_STACK Signal
 * - By default the Porting Layer defines two Interface Functions @ref delete_stack_task() and @ref delete_stack_task_finish()
 * - @ref delete_stack_task()
 *   - called from the Application side
 *   - must block until the stack is shutdown, signaled by resetting <tt>StackReady</tt> from #SDAI_INTERFACE_STACK_READY to any other value
 * - @ref delete_stack_task_finish()
 *   - called from the Stack side
 *   - must block until the application is shutdown, signaled by resetting <tt>ApplReady</tt> from #SDAI_INTERFACE_APPLICATION_READY to any other value
 * @section porting_shutdown_handshake_example Handshaking Example
 * In the picture below a example shutdown with handshake is shown. In the example a sequence of @ref delete_stack_task() and @ref delete_stack_task_finish() function calls is used on a two CPU environment.
 * The first one is the Application Thread/CPU represented with color blue. The second one is the Stack Thread/CPU represented with color black.@n
 * In this example the shared variables <tt>StackReady</tt> & <tt>ApplReady</tt> are used for synchronization. The Function @ref delete_stack_task() waits until the Stack signals a shutdown by clearing the
 * <tt>StackReady</tt> Flag, afterwards it resets the <tt>ApplReady</tt> Flag. The Function @ref delete_stack_task_finish() must be implemented accordingly and resets the <tt>StackReady</tt> Flag first
 * and waits afterwards until the Application signals shutdown by clearing the <tt>ApplReady</tt> Flag. (Note: Implementing both Functions in the same fashion would result in a deadlock!)
 * @msc
 * hscale = "2";
 * "STACK", "STACK::Interface", "Porting" , "APPLICATION::Interface" [arclinecolor=blue, arctextcolor=blue], "APPLICATION" [arclinecolor=blue, arctextcolor=blue];
 * --- [label="Stack & Application running"];
 * ...;
 * "STACK"<-"APPLICATION" [label="INTERFACE_SIGNAL_STOP_STACK"];
 * "APPLICATION::Interface"<="APPLICATION";
 * "Porting"<="APPLICATION::Interface" [label="delete_stack_task()"];
 * --- [label="Wait until the Stack is shut down, signaled by StackReady",linecolor=blue,textcolor=blue];
 * ...;
 * "STACK" => "STACK::Interface";
 * "STACK::Interface"=>"Porting" [label="delete_stack_task_finish()"];
 * "Porting"->"Porting" [label="clear StackReady"];
 * --- [label="Wait until Application notifies the Stack shut down, signaled by ApplReady"];
 * ...;
 * "Porting"->"Porting" [label="clear ApplReady",linecolor=blue,textcolor=blue];
 * "Porting">>"APPLICATION::Interface" [linecolor=blue,textcolor=blue];
 * "STACK::Interface"<<"Porting";
 * "APPLICATION::Interface">>"APPLICATION";
 * "STACK"<<"STACK::Interface";
 * --- [label="Stack & Application stopped"];
 * @endmsc
 * <CENTER><b>Sequence Chart for the Stack Thread (black) and Application Thread (blue) shutdown. The handshaking is done using the shared variables <tt>ApplReady</tt> & <tt>StackReady</tt>.</b></CENTER>
 */

/** @page porting_mutex Synchronization directives between Stack & Application
 * @section porting_mutex_general General
 * In multi-threaded environments race-conditions can occur if two threads access the same memory simultaneously e.g. Thread 1 writes memory and Thread 2 reads memory. This example results in non-consistent
 * data for Thread 2. The same applies for multi-CPU environments with shared-Resources/shared-RAM. Thus a mutual exclusion, short MUTEX, must be implemented to ensure that the shared-Resource is accessed
 * exclusive only by one Thread/CPU at a time.@n
 * On multi CPU environments mutual exclusion can be implemented by Software with the use of special Commands or with the use of special Hardware Units.
 * @section porting_mutex_requirements Requirements
 * - It mustn't be possible that two resources access the same resource simultaneously
 * - A mutex can be only acquired by one thread/CPU at a time
 * - The mutex must be NOT REENTRANT/RECURSIVE, unlocking the mutex more often than locking it has no bad behavior
 * - The mutual exclusion functions are @ref mutex_lock() and @ref mutex_unlock()
 * - @ref mutex_lock()
 *   - called from Application and Stack side
 *   - lock the mutex of the requested Interface/shared-RAM Area
 * - @ref mutex_unlock()
 *   - called from Application and Stack side
 *   - unlock the mutex of the requested Interface/shared-RAM Area
 *
 * @section porting_mutex_example Implementation Examples
 * <b>Software Solutions:</b>
 * - Disabling Interrupts, Call Scheduler/Dispatcher Lock
 *   - single CPU: applicable
 *   - multi CPU: not applicable
 * - Software Mutex Algorithm e.g. Dekker's algorithm, Peterson's algorithm, ...
 *   - single CPU: applicable
 *   - multi CPU: applicable, but atomic Test-And-Set or Compare-And-Swap, ... operations needed
 *
 * <b>Hardware Solutions:</b>
 * - Softing Mutex Cores, e.g. used at the FPGA Evaluation Board, the implementation differs by the FPGA designs. For further Information see the Mutex_en.pdf.
 *   - single CPU: applicable
 *   - multi CPU: applicable (no special requirements for the CPU)
 *
 * @note Attention must be taken in case of CPUs with Data Cache or Out-of-Order execution. Data Cache must be deactivated and Out-of-Order execution must be limited with memory barriers in order to implement a mutex.
 */

/** @page porting_signal Event notification between Stack & Application
 * @section porting_signal_general General
 * A Event notification is needed when new data was written into the shared-RAM (DP-RAM=Dual-Port-RAM). In contrast to a polling mechanism, no computing power is wasted on the receiving Thread/CPU.@n
 * The Event notifications are bidirectional, namely from Stack to Application and vice versa.
 * @section porting_signal_requirements Requirements
 * - sending a signal/event notification is non-blocking
 * - at the receiving cpu the event is notified by a call of function interface_receive_signal(), interface_receive_data_signal() or interface_receive_synch_signal()
 * - The Functions implementing the event notification are @ref send_signal_stack() and @ref send_signal_application()
 * - @ref send_signal_stack()
 *   - called from stack side
 *   - the event passed as argument must be notified
 * - @ref send_signal_application()
 *   - called from application side
 *   - the event passed as argument must be notified
 * @section porting_signal_example Implementation Example
 * <b>Multi CPU environment:</b>@n
 * At a multi CPU environment the event notification is done by Interrupts. Depending on the Architecture of the Target CPU we need to e.g. generate a specific slope, ... at a IO-Pin.@n The examples below
 * show the sequence generated by a event notification.
 * @msc
 * hscale="2";
 * "STACK", "STACK::Interface", "Porting" , "APPLICATION::Interface" [arclinecolor=blue, arctextcolor=blue], "APPLICATION" [arclinecolor=blue, arctextcolor=blue];
 * "STACK" => "STACK::Interface";
 * "STACK::Interface" => "Porting" [label="send_signal_stack()"];
 * "Porting" -> "Porting" [label="ISR()",linecolor=blue,textcolor=blue];
 * "Porting" -> "APPLICATION" [label="Signal_...",linecolor=blue,textcolor=blue];
 * "STACK::Interface" << "Porting";
 * "STACK" << "STACK::Interface";
 * @endmsc
 * <CENTER><b>Sequence Chart notify the Application Thread (blue) from Stack Thread (black).</b></CENTER>
 * @msc
 * hscale="2";
 * "STACK", "STACK::Interface", "Porting" , "APPLICATION::Interface" [arclinecolor=blue, arctextcolor=blue], "APPLICATION" [arclinecolor=blue, arctextcolor=blue];
 * "APPLICATION::Interface" <= "APPLICATION";
 * "Porting" <= "APPLICATION::Interface" [label="send_signal_application()"];
 * "Porting" -> "Porting" [label="ISR()"];
 * "STACK" <- "Porting" [label="Signal_..."];
 * "Porting" >> "APPLICATION::Interface" [linecolor=blue,textcolor=blue];
 * "APPLICATION::Interface" >> "APPLICATION" [linecolor=blue,textcolor=blue];
 * @endmsc
 * <CENTER><b>Sequence Chart notify the Stack Thread (black) from Application Thread (blue).</b></CENTER>
 */

/** @page porting_endianness Byte-Ordering/Endianness
 * @section porting_endianness_general General
 * The Byte-Ordering/Endianness of the Network Data and the CPU may be different. Thus a method @ref convert_network_u32_to_processor_u32 is defined to convert data to the local format.@n
 * The Byte-Ordering of the current platform must be defined by a
 * @code
#undef PROCESSOR_BYTE_ORDERING_BIG_ENDIAN
#define PROCESSOR_BYTE_ORDERING_LITTLE_ENDIAN // byte ordering of this platform
@endcode
 * OR VICE VERSA@n
 * @code
#undef PROCESSOR_BYTE_ORDERING_LITTLE_ENDIAN
#define PROCESSOR_BYTE_ORDERING_BIG_ENDIAN // byte ordering of this platform
@endcode
 * Block. The conversion is then be done by a Macro
 * @code
#ifdef PROCESSOR_BYTE_ORDERING_LITTLE_ENDIAN
  #define _CONVERT_NETWORK_U32_TO_PROCESSOR_U32(Value32)    ((U32)((((U32)(Value32))                >> 24) | \
                                                                   ((((U32)(Value32)) & 0x00FF0000) >>  8) | \
                                                                   ((((U32)(Value32)) & 0x0000FF00) <<  8) | \
                                                                    (((U32)(Value32))               << 24) ))
#endif

#ifdef PROCESSOR_BYTE_ORDERING_BIG_ENDIAN
  #define _CONVERT_NETWORK_U32_TO_PROCESSOR_U32(Value32)    (Value32)
#endif
 @endcode
 * which is already implemented at the <tt>porting.c</tt> Example. In case of a e.g. special CPU Command (Assembler Command) this can be used instead.
 *
 * @note Currently the data format of the Shared-RAM is little endian. The real implementation of the application part of SDAI assumes that also
 * little endian is used for the application CPU. Automatic big endian support (selectable by compile time) is planned for future versions. Until that
 * it has to be done during the portation process.
 */

/** @page porting_watchdog Watchdog
 * @section porting_watchdog_general General
 * In multi CPU environments the Stack and Application are distributed over 2 CPUs. In order to keep the
 * shared RAM in Error Situations consistent a Watchdog can be implemented at each CPU to keep track of the
 * other CPU. Therefore a Software Implementation is used at each CPU using the shared-RAM.@n
 * @section porting_watchdog_requirements Requirements
 * - in case of a watchdog expiration the corresponding CPU must be reset
 * - the Stack CPU is supervised by the Application CPU and vice versa
 * - the corresponding functions are @ref check_watchdog_supported() and @ref handle_stack_watchdog_expired()
 * - @ref check_watchdog_supported()
 *   - called from stack and application side
 *   - returns whether a Watchdog should be activated or not
 * - @ref handle_stack_watchdog_expired()
 *   - called from application side
 *   - the function should reset the Stack CPU
 *   - resets the handshaking mechanism, see @ref porting_startup_handshake and @ref porting_shutdown_handshake (e.g. clears the StackReady Flag)
 *   - resets the shared-RAM interface e.g. the mutex cores
 */
