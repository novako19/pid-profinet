#! /usr/bin/perl

sub error($)
{
  print "$_[0]\n";
  exit 1;
}

sub buildheader($)
{
  my $header;
  my $filename = $_[0];

  $filename =~ s/\./_/g;
  $filename =~ s/\//_/g;

  $header  = "/******************************************************************************\n\n";
  $header .= "SOFTING Industrial Automation GmbH\n";
  $header .= "Richard-Reitzner-Allee 6\n";
  $header .= "D-85540 Haar\n";
  $header .= "Phone: ++49-89-4 56 56-0\n";
  $header .= "Fax: ++49-89-4 56 56-3 99\n";
  $header .= "http://www.softing.com\n\n";
  $header .= "Copyright (C) SOFTING Industrial Automation GmbH 2005-2014. All Rights Reserved\n\n";
  $header .= "Version: 1.34.00\n\n";
  $header .= "******************************************************************************/\n\n\n";
  $header .= "#ifndef __".uc($filename)."__\n\n";
  $header .= "#define __".uc($filename)."__\n\n";
  
  return $header;
}

sub buildtrailer($)
{
  my $trailer;
  my $filename = $_[0];
  
  $filename =~ s/\./_/g;
  $filename =~ s/\//_/g;

  $trailer  = "\n#endif /* __".uc($filename)."__ */\n\n";
  $trailer .= "/*****************************************************************************/\n";
  $trailer .= "/* vim: set nu ts=2 et nowrap: */\n\n";

  return $trailer;
}


sub file2hex($)
{
  my $hex;
  open (INPUT, "<$_[0]") or error "failed to open $_[0]";
    
  $hex = "";
  
  $counter = 0;
  
  while (<INPUT>)
  {
    $lang = length($_);
    for($i=0;$i<$lang;$i++)
    {
      $char = substr($_,$i,1);
      
      $hex .= "0x" . unpack (H8, $char) . ", ";
      
      $counter++;
     
      if ($counter % 20 == 0)
      {
        $hex .= pack("H*", unpack("H8", "\n"));
      }
    }
  }
  
  chop($hex);
  chop($hex);

  close INPUT;

  return $hex;
}


#
# main
#

if ($#ARGV < 0)
{
  print "USAGE: $0 output.h input\n";
  exit 1;
}

open (OUTPUT, ">$ARGV[0]") or error "failed to create $_ARGV[0]";

#replace every '-' with '_' in the filename
$ARGV[0] =~ s/-/_/g; 

$STRING = buildheader($ARGV[0]);

$SEPERATOR = rindex ($ARGV[0], ".");
$BEGIN     = rindex ($ARGV[0], "/") + 1;
$END       = length ($ARGV[0]) - $SEPERATOR;

$STRING .= "char " . substr ($ARGV[0], $BEGIN, -$END) . $ARGV[2] . "[] = {\n";

$STRING .= file2hex($ARGV[1]);

$STRING .= "};\n";

$STRING .= buildtrailer($ARGV[0]);

print OUTPUT $STRING;

close (OUTPUT);

###EOF###
