/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

var AjaxUpdateTime     = 10000; /* ajax udate time for reinitiating requests (10 sec) */
var AjaxDefaultTimeout = 3000;  /* ajax default timeout (5 sec) */

var PageDiagnosis   = "1";
var PageIoData      = "2";
var PageFirmware    = "3";
var PageOverview    = "4";
var PageNetwork     = "5";
var PageVersion     = "6";
var PageDevice      = "7";

var CurrentReqObject   = null;
var CurrentResFunction = null;

var AjaxTimeoutObj = null;

/****************************************************************************/
/*jslint devel: true, browser: true, white: true, plusplus: true */
/*global ActiveXObject: true */

function util_ajax_loader (Value)
{
  "use strict";

  var elem = document.getElementById('AjaxLoading');

  if (elem === null) { return; }

  if (Value === 'show')
  {
    elem.firstChild.style.visibility = 'inherit';
    elem.lastChild.nodeValue         = "Refresh data ...";
    elem.style.visibility            = 'visible';
  }
  else if (Value === 'stop')
  {
    elem.firstChild.style.visibility = 'hidden';
    elem.lastChild.nodeValue         = "Stopped";
    elem.style.visibility            = 'visible';
  }
  else if (Value === 'disable')
  {
    elem.firstChild.style.visibility = 'hidden';
    elem.lastChild.nodeValue         = "Disabled";
    elem.style.visibility            = 'visible';
  }
  else
  {
    window.setTimeout(function () {document.getElementById('AjaxLoading').style.visibility = 'hidden'; }, 500);
  }

  return;
}

/*==========================================================================*/

function util_delete_table (TableId)
{
  "use strict";

  var Table, TableBody;

  Table     = document.getElementById(TableId);
  TableBody = Table.getElementsByTagName("tbody")[0];

  if (TableBody !== undefined)
  {
    while (TableBody.childNodes.length > 1)
    {
      TableBody.removeChild(TableBody.lastChild);
    }
  }

  return;
}

/*==========================================================================*/

function util_table_add_row (TableBody, StringArray)
{
  "use strict";

  var Row, Cell, ArrayIndex;

  Row = document.createElement ("tr");

  for (ArrayIndex = 0; ArrayIndex < StringArray.length; ArrayIndex++)
  {
    Cell = document.createElement ("td");
    Cell.innerHTML = StringArray [ArrayIndex];
    Row.appendChild (Cell);
  }

  TableBody.appendChild (Row);

  return;
}

/*==========================================================================*/

function util_request_object ()
{
  "use strict";

  var ReqObject = null;

  try
  {
    /* Mozilla, Opera, Safari, IE (v7+) */
    ReqObject = new XMLHttpRequest ();
  }
  catch (Error)
  {
    alert("You are using an unsupported browser version.\n Please update your browser");
  }

  return ReqObject;
}

/*==========================================================================*/

function util_ajax_timeout ()
{
  "use strict";

  CurrentReqObject.abort ();

  util_ajax_loader ("stop");

  alert ("Timeout, automatic update stopped!");

  return;
}


/*==========================================================================*/

function util_ajax_response ()
{
  "use strict";

  if (CurrentReqObject.readyState === 4)
  {
    clearTimeout (AjaxTimeoutObj);

    if (CurrentReqObject.status === 200)
    {
      util_ajax_loader ("hide");
      CurrentResFunction (CurrentReqObject.responseText);
    }
    else if (CurrentReqObject.status === 400)
    {
      util_ajax_loader ("hide");
      alert ("Invalid Data");
    }
  }

  return;
}

/*==========================================================================*/

function util_ajax_request (ReqObject, Type, Data, Page, ResponseFunction, Timeout)
{
  "use strict";

  var ReqUrl = "/ajax_handler", ReqData = "", ContentType = "", Index;

  CurrentReqObject   = ReqObject;
  CurrentResFunction = ResponseFunction;

  if (Type === 'get')
  {
    ContentType = "application/x-www-form-urlencoded";
    ReqUrl     += "?Page=" + Page + "&Timestamp=" + new Date().getTime();

    if (Data) {ReqUrl += Data;}
  }
  else if (Type === 'post')
  {
    ContentType = "application/x-www-form-urlencoded";
    ReqData     = "Page=" + Page + "&Timestamp=" + new Date().getTime();

    if (Data)
    {
      for (Index = 0; Index < Data.length; Index++)
      {
        ReqData += "&" + Data [Index].name + "=" + encodeURIComponent(Data [Index].value);
      }
    }
  }
  else if (Type === 'file')
  {
    Type         = 'post';
    ReqUrl      += "?Page=" + Page + "&Timestamp=" + new Date().getTime() + "&IsFile=true";
    ReqData      = Data;
  }
  else
  {
    alert ("Invalid AJAX Request");
  }

  util_ajax_loader ("show");
  ReqObject.onreadystatechange = util_ajax_response;
  ReqObject.open (Type, ReqUrl);
  if (ContentType !== "") {ReqObject.setRequestHeader ("Content-Type", ContentType);}
  ReqObject.send (ReqData);

  AjaxTimeoutObj = setTimeout (util_ajax_timeout, Timeout);

  return;
}

/*==========================================================================*/

function util_ajax_abort (ReqObject)
{
  "use strict";

  clearTimeout (AjaxTimeoutObj);
  ReqObject.abort ();

  return;
}

/*==========================================================================*/

function util_progress_bar (Bar, Value1, Value2)
{
  "use strict";

  var Obj, Value;

  Value = parseInt (Value1, 10) + parseInt (Value2, 10);
  Obj   = document.getElementById (Bar).firstChild;

  if (Value === 0xFF)
  {
    Obj.nodeValue = '-';
  }
  else
  {
    if (Value >= 100) {Value = 100; Value1 = 100; Value2 = 0;}

    Obj.nodeValue = Value + '%';

    Obj = document.getElementById (Bar + "B");
    Obj.style.width = Value1 + "px";

    if (Value > 90) {Obj.style.backgroundColor="red";}
    else            {Obj.style.backgroundColor="green";}

    Obj = document.getElementById (Bar + "B2");

    if (Obj !== null)
    {
      Obj.style.width = Value2 + "px";

      if (Value > 90) {Obj.style.backgroundColor="red";}
      else            {Obj.style.backgroundColor="mediumseagreen";}
    }
  }

  return;
}

/*==========================================================================*/

function util_toggle_overlay (Box)
{
  "use strict";

  var Overlay = document.getElementById ('overlay'), SpecialBox = document.getElementById (Box);

  Overlay.style.opacity = 0.8;

  if (Overlay.style.display === "block") {Overlay.style.display = "none"; SpecialBox.style.display = "none";}
  else                                   {Overlay.style.display = "block"; SpecialBox.style.display = "block";}

  return;
}

/*==========================================================================*/

function util_limited_string (Input, Limit, AddBreak)
{
  "use strict";

  var Output = Input, Slice;

  if (Input.length > Limit)
  {
    if (AddBreak === false)
    {
      Slice = (Limit / 2) - 1;

      Output  = Input.slice (0, Slice);
      Output += "...";
      Output += Input.slice (-Slice, Input.length);
    }
    else
    {
      Slice  = Limit;
      Output = "";

      while (Slice < Input.length)
      {
        Output += Input.slice ((Slice - Limit), Slice);
        Output += "<br>";

        Slice += Limit;
      }

      Output += Input.slice ((Slice - Limit), Slice);
    }
  }

  return Output;
}

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */