/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

var FirmwareReqObj = null;

/*==========================================================================*/

/*jslint devel: true, browser: true, white: true, plusplus: true*/
/*global util_ajax_request: true, util_request_object: true, PageFirmware: true*/

/*==========================================================================*/

function firmware_handle_response (Response)
{
  "use strict";

  var FirmwareInfo, Result;

  /* we know that eval is evil */
  /*jslint evil: true */
  FirmwareInfo = eval ('(' + Response  + ')');
  /*jslint evil: false */

  Result = document.getElementById ("ResultString");
  Result.firstChild.nodeValue = FirmwareInfo.Result;

  return;
}

/*==========================================================================*/

function firmware_update ()
{
  "use strict";

  var FirmwareFile, ReqData, Result;

  FirmwareFile = document.getElementById ("Firmware");
  ReqData      = new FormData ();

  ReqData.append("Firmware", FirmwareFile.files [0]);

  Result = document.getElementById ("Result");
  Result.style.display = "block";
  Result = document.getElementById ("ResultString");
  Result.firstChild.nodeValue = "Transferring Data";

  util_ajax_request (FirmwareReqObj, 'file', ReqData, PageFirmware, firmware_handle_response, 1200000);

  return (false);
}

/*==========================================================================*/

function init ()
{
  "use strict";

  FirmwareReqObj = util_request_object ();

  return;
}

/*==========================================================================*/

function term()
{
  "use strict";

  util_ajax_abort (FirmwareReqObj);

  return;
}

/*****************************************************************************/
/* vim: set nu ts=4 et nowrap: */