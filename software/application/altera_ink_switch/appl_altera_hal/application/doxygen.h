/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

/** @page platform_porting_descr Altera HAL Porting Layer Description
 *
 * This porting layer implements all functions needed to run the SDAI sample application on a NIOS 2 processor together
 * with the Altera HAL.
 *
 * - @subpage platform_porting_overview
 * - @subpage platform_porting_build
 * - @subpage platform_porting_flash
 *
 * A detailed description of all definitions, data structures and functions can be found in the following chapters:
 * - @subpage platform_defines
 * - @subpage platform_structures
 * - @subpage platform_functions
 *
 * Source code:
 * - @subpage platform_source
 * - @subpage platform_header
 * - @subpage testpattern_source
 * - @subpage epcq256_flash_header
 * - @subpage epcs64_flash_header
 * - @subpage cfi_flash_header
 *
 */

/** @page platform_porting_overview Overview
 *
 * @section platform_porting_overview_boards Supported Boards
 *
 * The following boards are supported by the porting layer for the Altera HAL
 *
 * @subsection platform_target_rtem Softing RTEM
 * \par Used components:
 * - 4 bicolour LEDs
 * - 8 yellow LEDs
 * - 6 push buttons
 * - \ref platform_porting_flash_epcs64 FLASH
 * - RAM
 *
 * \par Known restrictions
 * - none
 *
 * @subsection platform_target_ink Altera INK
 * \par Used components:
 * - 18 switches
 * - 4 push buttons
 * - 18 red LEDs
 * - 9 green LEDs
 * - 8 7-segment displays
 * - 16x2 LCD module
 * - \ref platform_porting_flash_epcs64 FLASH
 * - RAM
 *
 * \par Known restrictions
 * - none
 *
 * @subsection platform_target_cve Cyclone V E FPGA Development Kit
 * \par Used components:
 * - 4 push buttons
 * - 5 green LEDs
 * - 4 DIP switches
 * - 16x2 LCD module
 * - \ref platform_porting_flash_cfi FLASH
 * - RAM
 *
 * \par Known restrictions
 * - none
 *
 * @subsection platform_target_dbc5 EBV DBC5 Development Board
 * \par Used components:
 * - 8 red LEDs
 * - 4 push buttons
 * - 2 7-segment displays
 * - \ref platform_porting_flash_epcq256 FLASH
 * - RAM
 *
 * \par Known restrictions
 * - none
 *
 */

/** @page platform_porting_build Build environment
 *
 * GNU make is used to build the sample application for the Altera HAL. As compiler
 * the GNU Compiler Collection (GCC) tool chain for NIOS II is used that comes
 * with the Altera Quartus installation.
 *
 * @section platform_porting_decription_build_comp Compiler/Linker
 *
 * The application is a managed "Nios II X.X Software Build Tools for Eclipse" project. That means that the compiler settings and linker script are
 * controlled by the SDK. See the Altera documentation for more informationen.
 *
 * @section platform_porting_decription_build_make Makefile
 *
 * You can build the application with default settings by calling <b>make clean all</b> from a "Nios II X.X Command Shell".
 *
 * The following make targets are supported by the makefile
 * - all: Generates the whole application.
 * - clean: Deletes all generated files.
 *
 */

/** @page platform_porting_flash FLASH support
 *
 * Depending on the selected board different flash device are supported.
 *
 * For the \ref platform_target_ink and \ref platform_target_rtem
 * - @subpage platform_porting_flash_epcs64
 *
 * For the \ref platform_target_dbc5
 * - @subpage platform_porting_flash_epcq256
 *
 * For the \ref platform_target_cve
 * - @subpage platform_porting_flash_cfi
 *
 */

/******************************************************************************/

/** @defgroup porting Altera HAL Porting Layer */

/**
 * @defgroup platform_defines Defines
 * @ingroup porting
 */

/**
 * @defgroup platform_testpattern_def Test Pattern
 * @ingroup platform_defines
 */

/**
 * @defgroup platform_flash_def FLASH
 * @ingroup platform_defines
 */

/**
 * @defgroup platform_flash_epcq256_def EPCQ256
 * @ingroup platform_flash_def
 */

/**
 * @defgroup platform_flash_epcs64_def EPCS64
 * @ingroup platform_flash_def
 */

/**
 * @defgroup platform_flash_cfi_def CFI
 * @ingroup platform_flash_def
 */

/**
 * @defgroup platform_structures Data Structures
 * @ingroup porting
 */

/**
 * @defgroup platform_functions Functions
 * @ingroup porting
 */

/**
 * @defgroup platform_testpattern_func Test Pattern
 * @ingroup platform_functions
 */

/**
 * @defgroup platform_files Source Files
 * @ingroup porting
 */

/******************************************************************************/

/**
 * @defgroup platform_source demo_platform.c
 * @ingroup platform_files
 * \includelineno demo_platform.c
 */

/**
 * @defgroup platform_header demo_platform.h
 * @ingroup platform_files
 * \includelineno demo_platform.h
 */

/**
 * @defgroup testpattern_source test_pattern.c
 * @ingroup platform_files
 * \includelineno test_pattern.c
 */

/**
 * @defgroup epcq256_flash_header epcq256_flash_map.h
 * @ingroup platform_files
 * \includelineno epcq256_flash_map.h
 */

/**
 * @defgroup epcs64_flash_header epcs64_flash_map.h
 * @ingroup platform_files
 * \includelineno epcs64_flash_map.h
 */

/**
 * @defgroup cfi_flash_header cfi_flash_map.h
 * @ingroup platform_files
 * \includelineno cfi_flash_map.h
 */

