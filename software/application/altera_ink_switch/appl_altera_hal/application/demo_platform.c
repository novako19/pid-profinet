/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

#include <sys/alt_alarm.h>
#include <sys/alt_flash.h>
#include <sys/alt_irq.h>
#include <sys/alt_cache.h>

#include <priv/alt_busy_sleep.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sdai.h>

#ifdef HARDWARE_DBC5
  #include "../epcq256_flash_map.h"
#elif HARDWARE_CVE
  #include "../cfi_flash_map.h"
#else
  #include "../epcs64_flash_map.h"
#endif

#include "demo_platform.h"
#include "demo.h"

#ifdef PLATFORM_LCD_SUPPORTED
  #include <altera_avalon_lcd_16207.h>
#endif

/******************************************************************************
DEFINES
******************************************************************************/

/** \addtogroup platform_defines */
//@{
#ifdef HARDWARE_RTEM
  #define DEVICE_DESC_NAME "Softing RTEM"
#endif

#ifdef HARDWARE_INK
  #define DEVICE_DESC_NAME "Altera INK"
#endif

#ifdef HARDWARE_CVE
  #define DEVICE_DESC_NAME "Cyclone V E FPGA Development Kit"
#endif

#ifdef HARDWARE_DBC5
  #define DEVICE_DESC_NAME "EBV DBC5 Development Board"
#endif

#define FLASH_EMPTY_COMPARE_VALUE       (0xFF)  /**< the flash contains this value if unprogrammed */
#define FLASH_EMPTY_CHECK_SIZE          (256)   /**< number of bytes to be checked of the flash memory to decide whether the flash is programmed or not. */
//@}

/******************************************************************************
TYPEDEFS
******************************************************************************/

/** \addtogroup platform_structures */
//@{
/** \brief This structure holds data needed for the execution of the demo task. */
typedef struct _T_DEMO_TASK_DESCR
{
  volatile U32   EventFlags;
} T_DEMO_TASK_DESCR;

#if (FLASH_DEVICE_EPCS == 1)
/** \brief This structure describes the layout of the header of a EPCS firmware record. */
typedef struct _T_EPCS_FIRMWARE_HEADER
{
  U32   Length;      /**< The length of the firmware record */
  U32   Destination; /**< The destination address of the firmware record */
} T_EPCS_FIRMWARE_HEADER;
#endif

static U8   FlashBuffer [FLASH_EMPTY_CHECK_SIZE];
static U8   EmptyFlashBuffer [FLASH_EMPTY_CHECK_SIZE];
//@}

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

/** \addtogroup platform_functions Functions */
//@{
#ifdef PLATFORM_LCD_SUPPORTED
static void   platform_lcd_goto            (int, int);
static void   platform_lcd_clear           ();
#endif

#if defined(HARDWARE_INK) || defined(HARDWARE_DBC5)
static void   platform_sevseg_set_dec      (U32);
#endif

#if (FLASH_DEVICE_EPCS == 1)
static void copy_fieldbus_firmware_from_epcs_to_ram   (void);
#endif

#if (FLASH_DEVICE_CFI == 1)
static void copy_fieldbus_firmware_from_cfi_to_ram    (void);
#endif

static void platform_start_fieldbus_processor         (void);
//@}

/******************************************************************************
PUBLIC DATA
******************************************************************************/

#ifdef HARDWARE_CVE
unsigned char                     SimulatedLeds = 0; /**< Variable holding the current value of the simulated LEDs */
#endif

/******************************************************************************
PRIVATE DATA
******************************************************************************/

static T_DEMO_TASK_DESCR          DemoTask;          /**< Demo thread management data */

static alt_alarm                  AlarmData;         /**< Cyclic alarm data */

static alt_flash_fd*              pFlashDevice;      /**< Handle for the FLASH device */
static alt_irq_context            InterruptContext;  /**< Current interrupt context */

#ifdef PLATFORM_LCD_SUPPORTED
static FILE*                      LcdHandle;         /**< Handle for the LCD display */
#endif

#if defined(HARDWARE_INK) || defined(HARDWARE_DBC5)
static U8 SegmentValueMap[] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07,
                               0x7F, 0x6F, 0x77, 0x7C, 0x39, 0x5E, 0x79, 0x71};
                               /* 0,    1,    2,    3,    4,    5,    6,    7,
                                  8,    9,    a,    b,    c,    d,    e,    f */
#endif

/******************************************************************************
FUNCTION IMPLEMENTATION
******************************************************************************/

#ifdef PLATFORM_LCD_SUPPORTED
/**
 * The function platform_lcd_goto() sets the cursor in the LCD to the specified position.
 *
 * @return
 * - type  : void
 * @param[in] Line number of the line
 * - type : int
 * - range: [1..2]
 * @param[in] Column number of the column
 * - type : int
 * - range: [1..16]
 * @remarks Only available on Altera CVE and Altera INK.
 */
static void platform_lcd_goto (int Line, int Column)
{
  fprintf(LcdHandle, "\033[%i;%iH", Line, Column);
  fflush(LcdHandle);

  return;
}

/*===========================================================================*/

/**
 * The function platform_lcd_clear() clears the complete LCD screen.
 *
 * @return
 * - type  : void
 * @remarks Only available on Altera CVE and Altera INK.
 */
static void platform_lcd_clear ()
{
  fprintf (LcdHandle, "\033[2J");
  fflush (LcdHandle);

  return;
}
#endif /* PLATFORM_LCD_SUPPORTED */

/*===========================================================================*/

#if defined(HARDWARE_CVE) || defined(DOXYGEN)
/**
 * The function platform_lcd_simulation_led_off() switches a simulated LED on.
 *
 * @return
 * - type  : void
 * @param[in] LedNr number of the LED
 * - type : int
 * - range: [0..7]
 * @remarks Only available on Altera CVE.
 */
void platform_lcd_simulation_led_on (int LedNr)
{
#ifdef PLATFORM_LCD_SUPPORTED
  platform_lcd_goto (2, (16 - LedNr));

  fprintf (LcdHandle, "*");
  fflush (LcdHandle);

  SimulatedLeds |= (1 << LedNr);
#endif

  return;
}

/*===========================================================================*/

/**
 * The function platform_lcd_simulation_led_off() switches a simulated LED off.
 *
 * @return
 * - type  : void
 * @param[in] LedNr number of the LED
 * - type : int
 * - range: [0..7]
 * @remarks Only available on Altera CVE.
 */
void platform_lcd_simulation_led_off (int LedNr)
{
#ifdef PLATFORM_LCD_SUPPORTED
  platform_lcd_goto (2, (16 - LedNr));

  fprintf (LcdHandle, " ");
  fflush (LcdHandle);

  SimulatedLeds &= ~(1 << LedNr);
#endif

  return;
}

/*===========================================================================*/

/**
 * The function platform_lcd_simulation_led_switch() switches a simulated LED to a defined state.
 * If the bitrepresentation of the LED is 1 then the action is done.
 * if the action is 0 the the LED is switched off, otherwise the LED is switched on.
 *
 * @return
 * - type  : void
 * @param[in] Led LEDs which should be switched
 * - type : unsigned char
 * - range: whole range is valid
 * @param[in] Action Action which should be done
 * - type : int
 * - range: [LED ON != 0, LED OFF = 0]
 * @remarks Only available on Altera CVE.
 */
void platform_lcd_simulation_led_switch (unsigned char Led, int Action)
{
  int   Index = 0;

  while (Led)
  {
    if (Led & 1)
    {
      if (Action) {platform_lcd_simulation_led_on (Index);}
      else        {platform_lcd_simulation_led_off (Index);}
    }

    Index++;
    Led /= 2;
  }

  return;
}

/*===========================================================================*/

/**
 * The function platform_lcd_simulation_led_toggle() toggles the requested LEDs.
 *
 * @return
 * - type  : void
 * @param[in] Led LEDs which should be toggled
 * - type : unsigned char
 * - range: whole range is valid
 * @remarks Only available on Altera CVE.
 */
void platform_lcd_simulation_led_toggle (unsigned char Led)
{
  unsigned   Index;

  for (Index = 0; Index < 8; Index++)
  {
    if (Led & 1)
    {
      if (SimulatedLeds & (1 << Index)) {platform_lcd_simulation_led_off (Index);}
      else                              {platform_lcd_simulation_led_on (Index);}
    }

    Led /= 2;
  }

  return;
}

/*===========================================================================*/

/**
 * The function platform_lcd_simulation_led_set() shows the bit representation of
 * the byte in the LCD display 1-bits are represented by "*" and 0-bits are " ".
 *
 * @return
 * - type  : U8
 * @param[in] Led
 * - type : unsigned char
 * - range: whole range is valid
 * @remarks Only available on Altera CVE.
 */
void platform_lcd_simulation_led_set (unsigned char Led)
{
  unsigned   Index;

  for (Index = 0; Index < 8; Index++)
  {
    if (Led & 1) {platform_lcd_simulation_led_on (Index);}
    else         {platform_lcd_simulation_led_off (Index);}

    Led /= 2;
  }
}
#endif /* HARDWARE_CVE || DOXYGEN */

/*===========================================================================*/

#if defined(HARDWARE_INK) || defined(HARDWARE_DBC5) || defined(DOXYGEN)
/**
 * The function platform_sevseg_set_dec() displays the passed number on the
 * seven segment array.
 *
 * @return
 * - type  : void
 * @param[in] Number
 * - type : U32
 * - range: whole range is valid
 * @remarks Only available on Altera INK.
 */
static void platform_sevseg_set_dec (U32 Number)
{
  U32 Segment;

  for (Segment = 0; Segment < 8; Segment++)
  {
    IOWR(APPL_SUBSYSTEM_SEVEN_SEGMENT_BASE, Segment, SegmentValueMap [Number % 10]);
    Number /= 10;
  }

  return;
}
#endif /* HARDWARE_INK || HARDWARE_DBC5 || DOXYGEN */

/*===========================================================================*/

/**
 * The function platform_read_flash_config() reads the data from the configuration data
 * area in the FLASH memory. The configuration data are stored redundant in the
 * FLASH memory. The function starts with reading of the first area. If the data are
 * invalid it then reads the second area. If both areas are invalid the function returns
 * default values. On success the function returns 0 otherwise 1.
 *
 * @return
 * - type  : U8
 * @param[out] pFlashData
 * - type : T_CONFIG_DATA_APPL*
 * - range: whole address range is valid
 * @param[out] pHwData
 * - type : T_CONFIG_DATA_HW*
 * - range: whole address range is valid
 */
U8 platform_read_flash_config (T_CONFIG_DATA_APPL* pFlashData, T_CONFIG_DATA_HW* pHwData)
{
  U16   Value;

  int   Return;

  _TRACE (("platform_read_flash_config"));

  Value = platform_get_dip_switch ();
  pFlashData->AddressOffset = (U8) Value;

  /* read hw config data */
  if (pFlashDevice != NULL)
  {
    Return = alt_read_flash (pFlashDevice, FLASH_CONF_BOOT_OFFSET, pHwData, sizeof (T_CONFIG_DATA_HW));

    if ((Return != 0) || (pHwData->MagicNumber != FLASH_MAGIC_NUMBER) || (pHwData->Version != FLASH_BOOT_CONFIG_VERSION))
    {
      _ERROR_LOG (("FLASH error: %s\n", "read hwconfig"));

      pHwData->MagicNumber        = FLASH_MAGIC_NUMBER;
      pHwData->Version            = FLASH_BOOT_CONFIG_VERSION;

      pHwData->MacAddress[0]      = 0x00;
      pHwData->MacAddress[1]      = 0x06;
      pHwData->MacAddress[2]      = 0x71;
      pHwData->MacAddress[3]      = 0x06;
      pHwData->MacAddress[4]      = 0x05;
      pHwData->MacAddress[5]      = 0x45;

      pHwData->MacAddressPort1[0] = 0x00;
      pHwData->MacAddressPort1[1] = 0x06;
      pHwData->MacAddressPort1[2] = 0x71;
      pHwData->MacAddressPort1[3] = 0x06;
      pHwData->MacAddressPort1[4] = 0x05;
      pHwData->MacAddressPort1[5] = 0x01;

      pHwData->MacAddressPort2[0] = 0x00;
      pHwData->MacAddressPort2[1] = 0x06;
      pHwData->MacAddressPort2[2] = 0x71;
      pHwData->MacAddressPort2[3] = 0x06;
      pHwData->MacAddressPort2[4] = 0x05;
      pHwData->MacAddressPort2[5] = 0x02;
    }
  }
  else
  {
    _ERROR_LOG (("FLASH error: %s\n", "read hwconfig"));

    pHwData->MagicNumber        = FLASH_MAGIC_NUMBER;
    pHwData->Version            = FLASH_BOOT_CONFIG_VERSION;

    pHwData->MacAddress[0]      = 0x00;
    pHwData->MacAddress[1]      = 0x06;
    pHwData->MacAddress[2]      = 0x71;
    pHwData->MacAddress[3]      = 0x06;
    pHwData->MacAddress[4]      = 0x05;
    pHwData->MacAddress[5]      = 0x45;

    pHwData->MacAddressPort1[0] = 0x00;
    pHwData->MacAddressPort1[1] = 0x06;
    pHwData->MacAddressPort1[2] = 0x71;
    pHwData->MacAddressPort1[3] = 0x06;
    pHwData->MacAddressPort1[4] = 0x05;
    pHwData->MacAddressPort1[5] = 0x01;

    pHwData->MacAddressPort2[0] = 0x00;
    pHwData->MacAddressPort2[1] = 0x06;
    pHwData->MacAddressPort2[2] = 0x71;
    pHwData->MacAddressPort2[3] = 0x06;
    pHwData->MacAddressPort2[4] = 0x05;
    pHwData->MacAddressPort2[5] = 0x02;
  }

  /*-------------------------------------------------------------------------*/

  pHwData->MacAddress[5]      += pFlashData->AddressOffset;
  pHwData->MacAddressPort1[5] += pFlashData->AddressOffset;
  pHwData->MacAddressPort2[5] += pFlashData->AddressOffset;

  /*-------------------------------------------------------------------------*/

  /* read first config data */
  if (pFlashDevice != NULL)
  {
    if (alt_read_flash (pFlashDevice, FLASH_CONF_APPL1_OFFSET, pFlashData, sizeof (T_CONFIG_DATA_APPL)) != 0)
    {
      _ERROR_LOG (("FLASH error: %s\n", "read config 1"));
    }

    if ((pFlashData->MagicNumber == FLASH_MAGIC_NUMBER) && (pFlashData->Version == FLASH_APPL_CONFIG_VERSION))
    {
      /* first data are OK */
      return (0);
    }

    /* if first config data are corrupted, read second one */
    if (alt_read_flash (pFlashDevice, FLASH_CONF_APPL2_OFFSET, pFlashData, sizeof (T_CONFIG_DATA_APPL)) != 0)
    {
      _ERROR_LOG (("FLASH error: %s\n", "read config 2"));
    }

    if ((pFlashData->MagicNumber == FLASH_MAGIC_NUMBER) && (pFlashData->Version == FLASH_APPL_CONFIG_VERSION))
    {
      /* second data are OK */
      return (0);
    }
  }

  _ERROR_LOG (("No valid FLASH data - using default values\n"));

  /*--- no valid flash data, use default values -----------------------------*/

  memset(pFlashData, 0, sizeof (T_CONFIG_DATA_APPL));

  pFlashData->MagicNumber  = FLASH_MAGIC_NUMBER;
  pFlashData->Version      = FLASH_APPL_CONFIG_VERSION;

  pFlashData->BackEnd      = SDAI_DEFAULT_BACKEND;

  pFlashData->IpAddress[0] = 172;
  pFlashData->IpAddress[1] = 20;
  pFlashData->IpAddress[2] = 12;
  pFlashData->IpAddress[3] = 150;

  pFlashData->Netmask[0]   = 255;
  pFlashData->Netmask[1]   = 255;
  pFlashData->Netmask[2]   = 0;
  pFlashData->Netmask[3]   = 0;

  pFlashData->Gateway[0]   = 0;
  pFlashData->Gateway[1]   = 0;
  pFlashData->Gateway[2]   = 0;
  pFlashData->Gateway[3]   = 0;

  pFlashData->ConfigFlags  = 0;
  pFlashData->MailFlags    = (FLAG_MAIL_CONECTION_STATE | FLAG_MAIL_FATAL_ERROR);

  memcpy(&pFlashData->ConfigUsername[0], "config", sizeof("config"));
  memcpy(&pFlashData->ConfigPw[0], "config", sizeof("config"));

  memcpy(&pFlashData->AdminUsername[0], "admin", sizeof("admin"));
  memcpy(&pFlashData->AdminPw[0], "admin", sizeof("admin"));

  pFlashData->AddressOffset = 2;

  pFlashData->EplData.EplFlags            = 0;
  pFlashData->EplData.EplNodeId           = 1;
  pFlashData->EplData.SdoSequLayerTimeout = EPL_SDO_SEQU_LAYER_TIMEOUT_DEFAULT_VALUE;
  pFlashData->EplData.CfgData.IfcState    = 1;

  memset (&pFlashData->IdentMaintenanceData, ' ', sizeof (pFlashData->IdentMaintenanceData));

  return (1);
}

/*===========================================================================*/

/**
 * The function platform_write_flash_config() writes new data to the configuration data
 * area of the FLASH. The configuration data are stored redundant in the FLASH memory.
 * The function first erase and program the first configuration data area and on success
 * then the second area so one of the areas always contains valid data. On success the
 * function returns 0 otherwise 1.
 *
 * @return
 * - type  : U8
 * @param[in] pFlashData
 * - type : T_CONFIG_DATA_APPL*
 * - range: whole address range is valid
 */
U8 platform_write_flash_config (T_CONFIG_DATA_APPL* pFlashData)
{
  _TRACE (("platform_write_flash_config"));

  if (pFlashDevice != NULL)
  {
    /* program first data area */
    if (alt_write_flash (pFlashDevice, FLASH_CONF_APPL1_OFFSET, pFlashData, sizeof (T_CONFIG_DATA_APPL)) != 0)
    {
      _ERROR_LOG (("FLASH error: %s\n", "write config 1"));
      return (1);
    }

    /* program second data area  */
    if (alt_write_flash (pFlashDevice, FLASH_CONF_APPL2_OFFSET, pFlashData, sizeof (T_CONFIG_DATA_APPL)) != 0)
    {
      _ERROR_LOG (("FLASH error: %s\n", "write config 2"));
      return (1);
    }
  }
  else
  {
    return (1);
  }

  return (0);
}

/*===========================================================================*/

/**
 * The function platform_handle_firmware_segment() handles the platform
 * specific part of the firmware update, especially the access to the FLASH.
 * It either erase, write or verifies the requested firmware segment in the FLASH.
 *
 * @return
 * - type  : U8
 * @param[in] Action
 * - type : U8
 * - range: FW_ACTION_ERASE | FW_ACTION_WRITE | FW_ACTION_VERIFY | FW_ACTION_FINISH
 * @param[in] Segment
 * - type : U8
 * - range: 0 to (FLASH_NUMBER_FW_SEGMENTS - 1)
 * @param[in] SegmentSize
 * - type : U32
 * - range: whole range is valid
 * @param[in] pData
 * - type : U8*
 * - range: whole address range is valid
 */
U8 platform_handle_firmware_segment (U8 Action, U8 Segment, U32 SegmentSize, U8* pData)
{
  U32   SegmentOffset;
  U32   SegmentMaxSize;
  U8    Result = 1;

  _TRACE (("platform_handle_firmware_segment"));

  if (pFlashDevice == NULL)
  {
    return (Result);
  }

  if (Segment == 0)
  {
    SegmentOffset  = FLASH_FPGA_IMAGE1_OFFSET;
    SegmentMaxSize = FLASH_FPGA_IMAGE1_SIZE;
  }
  else if (Segment == 1)
  {
    SegmentOffset  = FLASH_FW_IMAGE1_OFFSET;
    SegmentMaxSize = FLASH_FW_IMAGE1_SIZE;
  }
  else if (Segment == 2)
  {
    SegmentOffset  = FLASH_FW_IMAGE2_OFFSET;
    SegmentMaxSize = FLASH_FW_IMAGE2_SIZE;
  }
  else
  {
    return (Result);
  }

  switch (Action)
  {
    case FW_ACTION_ERASE:
    {
      if (alt_erase_flash_block(pFlashDevice, SegmentOffset, SegmentMaxSize) == 0)
      {
        Result = 0;
      }
      else
      {
        _ERROR_LOG (("Erasing firmware segment failed"));
      }

      break;
    }

    case FW_ACTION_WRITE:
    {
      if (alt_write_flash (pFlashDevice, SegmentOffset, pData, SegmentSize) == 0)
      {
        Result = 0;
      }
      else
      {
        _ERROR_LOG (("Writing firmware segment failed"));
      }

      break;
    }

    case FW_ACTION_VERIFY:
    {
      U8*   pVerifyBuffer;

      pVerifyBuffer = malloc (SegmentSize);

      if (pVerifyBuffer != NULL)
      {
        if (alt_read_flash (pFlashDevice, SegmentOffset, pVerifyBuffer, SegmentSize) == 0)
        {
          if (! memcmp (pData, pVerifyBuffer, SegmentSize))
          {
            Result = 0;
          }
          else
          {
            _ERROR_LOG (("Verify firmware segment failed"));
          }
        }
        else
        {
          _ERROR_LOG (("Reading firmware segment failed"));
        }

        free (pVerifyBuffer);
      }
      else
      {
        _WARN_LOG (("Not enough memory to verify firmware segment\n"));
      }

      break;
    }

    case FW_ACTION_FINISH:
    {
      platform_send_event (EVENT_REBOOT);

      break;
    }

    default:
      _WARN_LOG (("Unknown firmware action\n"));
      break;
  }

  return (Result);
}

/*===========================================================================*/

/**
 * The function platform_visualize_data() prints status information on the LCD screen.
 * The displayed data depend on the selected protocol. A LCD Display is only available on
 * \ref platform_target_ink and \ref platform_target_cve
 *
 * <TABLE>
 * <TR><TH>Protocol</TH><TH>Line 1</TH><TH>Line 2</TH></TR>
 * <TR><TD>Ethernet/IP</TD><TD>IP Address</TD><TD>ethernetip_print_status_text() / demo_print_sdai_error_text()</TD></TR>
 * <TR><TD>Modbus TCP</TD><TD>IP Address</TD><TD>modbus_print_status_text() / demo_print_sdai_error_text()</TD></TR>
 * <TR><TD>Powerlink</TD><TD>ID / CN</TD><TD>powerlink_print_status_text() / demo_print_sdai_error_text()</TD></TR>
 * <TR><TD>EtherCAT</TD><TD>ID / Station Alias</TD><TD>ethercat_print_status_text() / demo_print_sdai_error_text()</TD></TR>
 * <TR><TD>PROFINET IO</TD><TD>Name of Station</TD><TD>profinet_print_status_text() / demo_print_sdai_error_text()</TD></TR>
 * <TR><TD>PROFIBUS DP</TD><TD>Address / Baudrate</TD><TD>profibus_print_status_text() / demo_print_sdai_error_text()</TD></TR>
 * </TABLE>
 *
 * @return
 * - type : VOID
 * @param[in] pApplicationData
 * - type : struct T_DEMO_MANAGEMENT_DATA*
 * - range: whole address range is valid
 */
void platform_visualize_data (struct T_DEMO_MANAGEMENT_DATA* pApplicationData)
{
  T_CONFIG_DATA_APPL* pApplConfigData = &pApplicationData->ConfigDataAppl;

  _TRACE (("platform_visualize_data"));

#ifdef PLATFORM_LCD_SUPPORTED
  platform_lcd_clear ();
  platform_lcd_goto (1, 1);

  switch (pApplConfigData->BackEnd)
  {
    case SDAI_BACKEND_PN:
    {
      fwrite (&pApplConfigData->DeviceName, _MIN (strlen ((char*)&pApplConfigData->DeviceName), ALT_LCD_VIRTUAL_WIDTH), 1, LcdHandle);

      platform_lcd_goto (2, 1);
      fprintf (LcdHandle, "%s %s", profinet_print_status_text (), demo_print_sdai_error_text ());
      break;
    }

    default:
    {
      fprintf (LcdHandle, "Unknown protocol selected");
    }
  }

  fflush (LcdHandle);
#else
  _AVOID_UNUSED_WARNING (pApplConfigData);
#endif

#if defined(HARDWARE_INK) || defined(HARDWARE_DBC5)
  platform_sevseg_set_dec (Axis0Reference);
#endif

  return;
}

/*===========================================================================*/

/**
 * The function platform_test_leds() implements a simple running light with the
 * LEDs on the board.
 *
 * @return
 * - type : VOID
 */
void platform_test_leds ()
{
  U8 Leds      = 1;
  U8 CountDown = 0;

  _TRACE (("platform_test_leds"));

  do
  {
    STATUS_LED_SWITCH(Leds, (CountDown == 0 ? LED_ON : LED_OFF));
    DATA_LED_SWITCH(Leds, (CountDown == 0 ? LED_ON : LED_OFF));

    if (Leds == 0x80) {Leds = 1; CountDown = 1;}
    else              {Leds = Leds << 1;}

    platform_busy_sleep (25000);

  } while((Leds != 0x80) || (CountDown == 0));

  STATUS_LED_SWITCH(Leds, LED_OFF);
  DATA_LED_SWITCH(Leds, LED_OFF);

  return;
}

/*===========================================================================*/

/**
 * The function platform_get_buttons() returns the current state of the buttons on
 * the board.
 *
 * @return
 * - type  : U8
 */
U8 platform_get_buttons (void)
{
  U8   ButtonValue = 0;

  _TRACE (("platform_get_buttons"));

#ifdef HARDWARE_RTEM
  ButtonValue = (IORD(APPL_SUBSYSTEM_PIO_BUTTON_BASE, 0) & 0x3F);
#endif

#ifdef HARDWARE_INK
  ButtonValue = (~IORD(APPL_SUBSYSTEM_PIO_KEY_BASE, 0) & 0x0F);
#endif

#ifdef HARDWARE_CVE
  ButtonValue = (~IORD(APPL_SUBSYSTEM_PIO_BUTTON_BASE, 0) & 0x0F);
#endif

#ifdef HARDWARE_DBC5
  ButtonValue = (~IORD(APPL_SUBSYSTEM_PIO_BUTTON_BASE, 0) & 0x0F);
#endif

  return (ButtonValue);
}

/*===========================================================================*/

/**
 * The function platform_get_dip_switch() returns the current state of the dip switches on
 * the board.
 *
 * @return
 * - type  : U16
 */
U16 platform_get_dip_switch (void)
{
  U16   SwitchValue = 0;

  _TRACE (("platform_get_dip_switch"));

#ifdef HARDWARE_RTEM
  SwitchValue = 0u;
#endif

#ifdef HARDWARE_INK
  SwitchValue = (IORD(APPL_SUBSYSTEM_PIO_BUTTON_BASE, 0) & 0xFFFF);
#endif

#ifdef HARDWARE_CVE
  SwitchValue = (~IORD(APPL_SUBSYSTEM_PIO_DIP_SWITCH_BASE, 0) & 0x0F);
#endif

  return (SwitchValue);
}

/*===========================================================================*/

/**
 * The function platform_get_cpu_load() returns the avarage CPU load for the last 100ms,
 * 1s and 10s. Measuring CPU load is not supported for the HAL so the values are set to
 * -1 (0xFF);
 *
 * @return
 * - type  : Void
 * @param[out] pLoad100ms
 * - type : U8*
 * - range: whole address range is valid
 * @param[out] pLoad1s
 * - type : U8*
 * - range: whole address range is valid
 * @param[out] pLoad10s
 * - type : U8*
 * - range: whole address range is valid
 */
void platform_get_cpu_load (U8* pLoad100ms, U8* pLoad1s, U8* pLoad10s)
{
  _TRACE (("platform_get_cpu_load"));

  *pLoad100ms = (U8) -1;
  *pLoad1s    = (U8) -1;
  *pLoad10s   = (U8) -1;

  return;
}

/*===========================================================================*/

/**
 * The function platform_get_hw_data() fills the init structure with the
 * platform dependent data.
 *
 * @return
 * - type  : Void
 * @param[out] pInitData
 * - type : struct SDAI_INIT*
 * - range: whole address range is valid
 */
void platform_get_hw_data (struct SDAI_INIT* pInitData)
{
  _TRACE (("platform_get_hw_data"));

  strncpy (pInitData->Ident.IfName, "eth0", sizeof (pInitData->Ident.IfName));

  pInitData->Info.SerialNumber = 1234567890L;

#ifdef HARDWARE_RTEM
  pInitData->Info.ProductCode = 0x0104;
  strncpy (pInitData->Info.ProductName, "FPGA RTEM CIII", sizeof (pInitData->Info.ProductName));
  strncpy (pInitData->Info.OrderId,     "FCM-NO-4010",    sizeof (pInitData->Info.OrderId));
#endif

#ifdef HARDWARE_INK
  pInitData->Info.ProductCode = 0x0105;
  strncpy (pInitData->Info.ProductName, "Softing ProtocolIP", sizeof (pInitData->Info.ProductName));
  strncpy (pInitData->Info.OrderId,     "PN-Altera-INK",    sizeof (pInitData->Info.OrderId));
#endif

#ifdef HARDWARE_CVE
  pInitData->Info.ProductCode = 0x0107;
  strncpy (pInitData->Info.ProductName, "Softing ProtocolIP", sizeof (pInitData->Info.ProductName));
  strncpy (pInitData->Info.OrderId,     "DK-DEV-5CEA7N",      sizeof (pInitData->Info.OrderId));
#endif

#ifdef HARDWARE_DBC5
  pInitData->Info.ProductCode = 0x0109;
  strncpy (pInitData->Info.ProductName, "Softing ProtocolIP", sizeof (pInitData->Info.ProductName));
  strncpy (pInitData->Info.OrderId,     "DBC5CEFA7",          sizeof (pInitData->Info.OrderId));
#endif

#if defined(IPCORE_ESC) || defined(IPCORE_EPL)
  /* if the device name is empty set a default value */
  if(strlen(pInitData->Ident.DevName) == 0)
  {
    strncpy (pInitData->Ident.DevName, DEVICE_DESC_NAME, sizeof(pInitData->Ident.DevName));
  }
#endif

  return;
}

/*===========================================================================*/

/**
 * The function platform_configure_isochronous_unit() configures the requested unit
 * with the input and output delay values written by a controller. To fulfil the
 * isochronous requirements the unit has normally hardware support.
 *
 * @return
 * - type  : Void
 * @param[in] Unit
 * - type : U8
 * - range: whole range is valid
 * @param[in] InputDelay
 * - type : U32
 * - range: whole range is valid
 * @param[in] OutputDelay
 * - type : U32
 * - range: whole range is valid
 */
void platform_configure_isochronous_unit (U8 Unit, U32 InputDelay, U32 OutputDelay)
{
  _TRACE (("platform_configure_isochronous_unit"));

#ifdef TEST_SUBSYSTEM_PATTERN_ISOCHRONOUS_BASE

  /* in our GSDML files we have defined a base of 1us (T_IO_Base) that means the delay values are
     multiples of these base */
  InputDelay  = (TEST_SUBSYSTEM_PATTERN_ISOCHRONOUS_FREQ / 1000000) * (InputDelay / 1000);
  OutputDelay = (TEST_SUBSYSTEM_PATTERN_ISOCHRONOUS_FREQ / 1000000) * (OutputDelay / 1000);

  if (Unit == PLATFORM_PERIPHERAL_ISOCHRONOUS_VECTOR)
  {
    /* TODO: calculate delays based on test pattern frequency */
    test_pattern_mirror_init (TEST_SUBSYSTEM_PATTERN_ISOCHRONOUS_BASE, InputDelay, OutputDelay);
  }
#endif

  return;
}

/*===========================================================================*/

/**
 * The function platform_send_event() sends an event to the sample application
 * main task.
 *
 * @return
 * - type  : Void
 * @param[in] Event
 * - type : U32
 * - range: #EVENT_OUTPUT_DATA_CHANGED | #EVENT_IDENT_DATA_CHANGED | #EVENT_WRITE_IND_RECEIVED |
            #EVENT_READ_IND_RECEIVED | #EVENT_ALARM_ACK_RECEIVED | #EVENT_CONTROL_IND_RECEIVED |
            #EVENT_EXCEPTION | #EVENT_CYCLIC_TIMER | #EVENT_RESTART_STACK |
            #EVENT_RESTART_NETWORK | #EVENT_STORE_FLASH_DATA
 */
void platform_send_event (U32 Event)
{
  alt_irq_context IntContext;

  _TRACE (("platform_send_event"));

  IntContext = alt_irq_disable_all ();
  DemoTask.EventFlags |= Event;
  alt_irq_enable_all (IntContext);

  return;
}

/*===========================================================================*/

/**
 * The function platform_wait_event() waits for an event
 *
 * @return
 * - type  : U32
 * @param[in] WaitEvents
 * - type : U32
 * - range: #EVENT_OUTPUT_DATA_CHANGED | #EVENT_IDENT_DATA_CHANGED | #EVENT_WRITE_IND_RECEIVED |
            #EVENT_READ_IND_RECEIVED | #EVENT_ALARM_ACK_RECEIVED | #EVENT_CONTROL_IND_RECEIVED |
            #EVENT_EXCEPTION | #EVENT_CYCLIC_TIMER | #EVENT_RESTART_STACK |
            #EVENT_RESTART_NETWORK | #EVENT_STORE_FLASH_DATA
 */
U32 platform_wait_event (U32 WaitEvents)
{
  alt_irq_context   IntContext;
  volatile alt_u32  Events;

  _TRACE (("platform_wait_event"));

  do
  {
    Events = (DemoTask.EventFlags & WaitEvents);

  } while (!Events);

  IntContext = alt_irq_disable_all ();
  DemoTask.EventFlags &= (~Events);
  alt_irq_enable_all (IntContext);

  return ((U32)Events);
}

/*===========================================================================*/

/**
 * The function platform_lock() disables all interrupts.
 *
 * @return
 * - type : void
 */
void platform_lock ()
{
  _TRACE (("platform_lock"));

  InterruptContext = alt_irq_disable_all ();

  return;
}

/*===========================================================================*/

/**
 * The function platform_unlock() enables all interrupts.
 *
 * @return
 * - type : void
 */
void platform_unlock ()
{
  _TRACE (("platform_unlock"));

  alt_irq_enable_all (InterruptContext);

  return;
}

/*===========================================================================*/

/**
 * The function platform_busy_sleep() waits actively for the specified time.
 *
 * @return
 * - type : void
 * @param[in] Delay1us time in microseconds
 * - type : U32
 * - range: whole range is valid
 */
void platform_busy_sleep (U32 Delay1us)
{
  _TRACE (("platform_busy_sleep"));

  alt_busy_sleep (Delay1us);

  return;
}

/**
 * The function platform_cyclic_timer_callback() is called by the HAL
 * when the cyclic timer is expired. The interval can be changed by changing
 * the macro #CYCLE_TIME_IN_MS.
 *
 * @return
 * - type : alt_u32
 * @param[in] DummyData
 * - type : void*
 * - range: whole address range is valid
 */
static alt_u32 platform_cyclic_timer_callback (void* DummyData)
{
  _TRACE (("platform_cyclic_timer_callback"));

  /* retrigger the watchdog */
  PLATFORM_TRIGGER_WATCHDOG();

  platform_send_event (EVENT_CYCLIC_TIMER);

  return (CYCLE_TIME_IN_MS);
}

/*===========================================================================*/

/**
 * The function visualize_startup_process() prints status messages from the boot
 * process.
 *
 * @return
 * - type : void
 * @param[in] pStatus
 * - type : char*
 * - range: whole address range is valid
 * @param[in] pInfo
 * - type : char*
 * - range: whole address range is valid
 */
static void visualize_startup_process (char* pStatus, char* pInfo)
{
#ifdef PLATFORM_LCD_SUPPORTED
  if (pStatus != NULL)
  {
    platform_lcd_clear ();
    platform_lcd_goto (1, 1);
    fprintf (LcdHandle, "%s", pStatus);
  }

  if (pInfo != NULL)
  {
    platform_lcd_goto (2, 1);
    fprintf (LcdHandle, "%s", pInfo);
  }

  fflush (LcdHandle);
#endif

  return;
}

/*===========================================================================*/

/**
 * The function copy_fieldbus_firmware_from_epcs_to_ram() copies the firmware of
 * the fieldbus subsytem from the epcs flash to the ram.
 *
 * @return
 * - type : void
 */
#if (FLASH_DEVICE_EPCS == 1)
static void copy_fieldbus_firmware_from_epcs_to_ram (void)
{
  T_EPCS_FIRMWARE_HEADER    FirmwareHeader;
  U32                       FlashSource = FLASH_FIELDBUS_FW_BASE_ADDRESS;

  char                      Info [16];


  if (pFlashDevice != NULL)
  {
    visualize_startup_process ("Checking flash", NULL);

    if (alt_read_flash (pFlashDevice, FlashSource, &FlashBuffer, sizeof (FlashBuffer)) == 0)
    {
      if (memcmp (FlashBuffer, EmptyFlashBuffer, sizeof (EmptyFlashBuffer)) == 0)
      {
        return;
      }
    }

    /*-----------------------------------------------------------------------*/

    visualize_startup_process ("Copying...", NULL);

    if (alt_read_flash (pFlashDevice, FlashSource, &FirmwareHeader, sizeof (T_EPCS_FIRMWARE_HEADER)) == 0)
    {
      while ( (FirmwareHeader.Length > 0uL) && (FirmwareHeader.Length < 0xffffffffuL) )
      {
        FlashSource += sizeof (T_EPCS_FIRMWARE_HEADER);

        alt_read_flash (pFlashDevice, FlashSource, (U8*) FirmwareHeader.Destination, FirmwareHeader.Length);
        alt_dcache_flush ((void*) FirmwareHeader.Destination, FirmwareHeader.Length);

        FlashSource += FirmwareHeader.Length;

        memset (&Info [0], ' ', sizeof (Info));
        snprintf (&Info [0], sizeof (Info), "RAM 0x%08lx", FirmwareHeader.Destination);
        visualize_startup_process (NULL, &Info [0]);

        alt_read_flash (pFlashDevice, FlashSource, &FirmwareHeader, sizeof (T_EPCS_FIRMWARE_HEADER));
      }
    }

    visualize_startup_process ("Copying...done", NULL);
  }

  return;
}
#endif

/*===========================================================================*/

/**
 * The function copy_fieldbus_firmware_from_cfi_to_ram() copies the firmware of
 * the fieldbus subsytem from the cfi flash to the ram.
 *
 * @return
 * - type : void
 */
#define FLASH_CHUNK_SIZE 1024
#if (FLASH_DEVICE_CFI == 1)
static void copy_fieldbus_firmware_from_cfi_to_ram (void)
{
  U32     FlashSource = FLASH_FIELDBUS_FW_BASE_ADDRESS;
  U32     RamDestination = RAM_FIELDBUS_FW_BASE_ADDRESS;
  U32     RamAddress;

  U32     RamEndAddress;

  char    Info [16];


  FlashSource    = (U32) alt_remap_uncached ((void*) FlashSource, FIELDBUS_FW_SIZE);
  RamDestination = (U32) alt_remap_uncached ((void*) RamDestination, FIELDBUS_FW_SIZE);
  RamEndAddress  = (U32) (RamDestination + FIELDBUS_FW_SIZE);

  if (pFlashDevice != NULL)
  {
    visualize_startup_process ("Checking flash", NULL);

    if (alt_read_flash (pFlashDevice, FlashSource, &FlashBuffer, sizeof (FlashBuffer)) == 0)
    {
      if (memcmp (FlashBuffer, EmptyFlashBuffer, sizeof (EmptyFlashBuffer)) == 0)
      {
        return;
      }
    }

    /*-----------------------------------------------------------------------*/

    visualize_startup_process ("Copying...", "read flash");

    do
    {
      RamAddress = RamDestination;

      /* copy the data to the RAM */
      (void) alt_read_flash (pFlashDevice, FlashSource, (U8*) RamAddress, FLASH_CHUNK_SIZE);

      FlashSource    += FLASH_CHUNK_SIZE;
      RamDestination += FLASH_CHUNK_SIZE;

      memset (&Info [0], ' ', sizeof (Info));
      snprintf (&Info [0], sizeof (Info), "RAM 0x%08lx", RamDestination);
      visualize_startup_process (NULL, &Info [0]);

    } while ( (RamDestination < RamEndAddress                                        ) &&
              (memcmp ((U8*) RamAddress, EmptyFlashBuffer, sizeof (EmptyFlashBuffer))) );

    visualize_startup_process ("Copying...done", NULL);
  }

  return;
}
#endif

/*===========================================================================*/

/**
 * The function platform_start_fieldbus_processor() copies the fieldbus firmware from the flash
 * and starts the fieldbus processor.
 *
 * @return
 * - type : void
 */
static void platform_start_fieldbus_processor (void)
{
  memset (EmptyFlashBuffer, FLASH_EMPTY_COMPARE_VALUE, sizeof (EmptyFlashBuffer));

  visualize_startup_process ("Reset fieldbus subsystem", NULL);
  IOWR (APPL_SUBSYSTEM_PIO_RESET_CPU_FIELDBUS_BASE, 0, 1);
  (void) alt_busy_sleep (1);

#if (FLASH_DEVICE_EPCS == 1)
  copy_fieldbus_firmware_from_epcs_to_ram ();
#elif (FLASH_DEVICE_CFI == 1)
  copy_fieldbus_firmware_from_cfi_to_ram ();
#endif

  (void) alt_busy_sleep (1);
  IOWR (APPL_SUBSYSTEM_PIO_RESET_CPU_FIELDBUS_BASE, 0, 0);
  visualize_startup_process ("Start fieldbus subsystem", NULL);

  return;
}

/*===========================================================================*/

/**
 * The function platform_demo_task() initialize the platform specific data and
 * starts the common demo function
 *
 * @return
 * - type : void
 */
static void platform_demo_task ()
{
  _TRACE (("platform_demo_task"));

  /* get a handle to the LCD driver on the supported platforms */
#ifdef PLATFORM_LCD_SUPPORTED
  LcdHandle = fopen (APPL_SUBSYSTEM_LCD_NAME, "w");
#endif

  /* init the flash API (no locking because stack is not running) */
  #if (FLASH_DEVICE_EPCS == 1) || (FLASH_DEVICE_CFI == 1)
  pFlashDevice = alt_flash_open_dev(FLASH_NAME);
  #endif

  /* disable the reset of the fieldbus CPU */
  platform_start_fieldbus_processor ();

#ifdef __SOFTING_TEST_PATTERN
  test_pattern_init_default ();
#endif

  /* create event flag */
  DemoTask.EventFlags = 0;

#if defined(HARDWARE_INK) || defined(HARDWARE_DBC5)
  platform_sevseg_set_dec (0);
#endif

  /* create cyclic alarm */
  alt_alarm_start (&AlarmData, CYCLE_TIME_IN_MS, platform_cyclic_timer_callback, NULL);

  /* run application main function */
  demo_task_main_function ();

  /* free resources */
  /* nothing to do */

  return;
}

/*===========================================================================*/

/**
 * The main function. It is the entry point of the program. For the Altera HAL no threads
 * are available so the functions just calls the main thread function.
 *
 * @return
 * - type : int
 * - range:
 * @param[in] argc
 * - type : int
 * - range: whole range is valid
 * @param[in] argv
 * - type : char**
 * - range: whole address range is valid
 */
int main (int argc, char** argv)
{
  _TRACE (("main"));

  /* First trigger the watchdog. */
  PLATFORM_TRIGGER_WATCHDOG();

  platform_demo_task ();

  return (0);
}

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */

