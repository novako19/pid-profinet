/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

#ifndef __DEMO_PLATFORM_H__
#define __DEMO_PLATFORM_H__

#include <assert.h>
#include <io.h>
#include <system.h>

/******************************************************************************
DEFINES
******************************************************************************/

/** \addtogroup platform_defines */
//@{
/** \anchor platform_althal_print @name Debug output
  * @note Enabling the log outputs must be used with care! If the NIOS terminal is not connected the application program may be halted when the printf() function is called. */
//@{
#define _TRACE_ENABLE        (0)   /**< define to enable trace log outputs */
#define _DEBUG_ENABLE        (1)   /**< define to enable debug log outputs */
#define _WARN_ENABLE         (1)   /**< define to enable warn log outputs */
#define _ERROR_ENABLE        (1)   /**< define to enable error log outputs */

#define PLATFORM_SLOW_PRINTF (0)   /**< if define is set to 1 means that the used printf implementation
                                        is very slow and has heavy impact on the runtime execution. Setting
                                        the define disable frequently outputs like the current output data.
                                        Status updates are still displayed. It has only impact on the #_DEBUG_ENABLE
                                        outputs */

#if _TRACE_ENABLE
  #define _TRACE(Arguments)       do {printf Arguments;} while (0)
#else
  #define _TRACE(Arguments)
#endif

#if _DEBUG_ENABLE
  #define _DEBUG_LOG(Arguments)   do {printf Arguments;} while (0)
#else
  #define _DEBUG_LOG(Arguments)
#endif

#if _WARN_ENABLE
  #define _WARN_LOG(Arguments)    do {printf Arguments;} while (0)
#else
  #define _WARN_LOG(Arguments)
#endif

#if _ERROR_ENABLE
  #define _ERROR_LOG(Arguments)   do {printf Arguments;} while (0)
#else
  #define _ERROR_LOG(Arguments)
#endif
//@}

/*---------------------------------------------------------------------------*/

#define _ASSERT_ENABLE   (1)

#if _ASSERT_ENABLE
#define _ASSERT(Expression)  assert (Expression);
#else
#define _ASSERT(Expression)
#endif

/*---------------------------------------------------------------------------*/

#define PACK_WORD_ALIGNMENT(Struct)             __attribute__ ((__packed__, aligned (2))) Struct
#define PACK_BYTE_ALIGNMENT(Struct)             __attribute__ ((__packed__, aligned (1))) Struct

/*---------------------------------------------------------------------------*/

#define CYCLE_TIME_IN_MS         10    /**< Cycle time of the demo application in ms. */

/*---------------------------------------------------------------------------*/

#define CONCATx(a,b) a##b
#define CONCAT(a,b) CONCATx(a,b)

/*---------------------------------------------------------------------------*/

#define AXIS0_SET_REFERENCE(x) do{IOWR(APPL_SUBSYSTEM_PIO_AXIS0_REFERENCE_BASE, 0, x);}while(0) /**< sets reference value of AXIS0 regulator */

/*---------------------------------------------------------------------------*/

#if defined(HARDWARE_RTEM) || defined(DOXYGEN)
  /** \anchor platform_althal_rtem @name RTEM definitions
    * Defines specific to the Softing RTEM Evaluation board */
  //@{
  #define PLATFORM_NAME PLATFORM_RTEM

  /*---------------------------------------------------------------------------*/

  #define PLATFORM_RTEM_LED_DEMO_POWER                 0x10   /**< LED H302 green on the RTEM */
  #define PLATFORM_RTEM_LED_DEMO_CONNECTED             0x40   /**< LED H303 green on the RTEM */
  #define PLATFORM_RTEM_LED_DEMO_ERROR                 0x80   /**< LED H303 red on the RTEM */
  #define PLATFORM_RTEM_LED_PROTOCOL_GREEN1            0x01   /**< LED H300 green on the RTEM */
  #define PLATFORM_RTEM_LED_PROTOCOL_YELLOW1           0x08   /**< LED H301 yellow on the RTEM */
  #define PLATFORM_RTEM_LED_PROTOCOL_RED1              0x02   /**< LED H300 red on the RTEM */
  #define PLATFORM_RTEM_LED_PROTOCOL_GREEN2            0x04   /**< LED H301 green on the RTEM */
  #define PLATFORM_RTEM_LED_PROTOCOL_YELLOW2           0x20   /**< LED H302 yellow on the RTEM */
  #define PLATFORM_RTEM_LED_PROTOCOL_RED2              0x00   /**< Unsupported */

  #define PLATFORM_RTEM_LED_ALL                        0xFF   /**< All LEDs on the RTEM */

  /*---------------------------------------------------------------------------*/

  #define PLATFORM_RTEM_STATUS_LED_INIT()              do{ IOWR(APPL_SUBSYSTEM_PIO_LED_RTEM_BASE, 0, 0); }while(0) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_RTEM_STATUS_LED_IS_ON(LED)          (IORD(APPL_SUBSYSTEM_PIO_LED_RTEM_BASE, 0) & LED) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_RTEM_STATUS_LED_SET(VALUE)          do{ IOWR(APPL_SUBSYSTEM_PIO_LED_RTEM_BASE, 0, VALUE); }while(0) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_RTEM_STATUS_LED_SWITCH(LED, ACTION) do{ if(ACTION) {IOWR(APPL_SUBSYSTEM_PIO_LED_RTEM_BASE, 0, IORD(APPL_SUBSYSTEM_PIO_LED_RTEM_BASE, 0) | LED);} else {IOWR(APPL_SUBSYSTEM_PIO_LED_RTEM_BASE, 0, IORD(APPL_SUBSYSTEM_PIO_LED_RTEM_BASE, 0) & ~LED);} }while(0) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_RTEM_STATUS_LED_TOGGLE(LED)         do{ IOWR(APPL_SUBSYSTEM_PIO_LED_RTEM_BASE, 0, IORD(APPL_SUBSYSTEM_PIO_LED_RTEM_BASE, 0) ^ LED);}while(0) /**< see \ref platform_althal_led "LED definitions" */

  /*---------------------------------------------------------------------------*/

  #define PLATFORM_RTEM_DATA_LED_INIT()                do{ IOWR(APPL_SUBSYSTEM_PIO_LED_EVAL_BASE, 0, 0); }while(0) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_RTEM_DATA_LED_IS_ON(LED)            (IORD(APPL_SUBSYSTEM_PIO_LED_EVAL_BASE, 0) & LED) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_RTEM_DATA_LED_SET(VALUE)            do{ IOWR(APPL_SUBSYSTEM_PIO_LED_EVAL_BASE, 0, VALUE); }while(0) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_RTEM_DATA_LED_SWITCH(LED, ACTION)   do{ if(ACTION) {IOWR(APPL_SUBSYSTEM_PIO_LED_EVAL_BASE, 0, IORD(APPL_SUBSYSTEM_PIO_LED_EVAL_BASE, 0) | LED);} else {IOWR(APPL_SUBSYSTEM_PIO_LED_EVAL_BASE, 0, IORD(APPL_SUBSYSTEM_PIO_LED_EVAL_BASE, 0) & ~LED);} }while(0) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_RTEM_DATA_LED_TOGGLE(LED)           do{ IOWR(APPL_SUBSYSTEM_PIO_LED_EVAL_BASE, 0, IORD(APPL_SUBSYSTEM_PIO_LED_EVAL_BASE, 0) ^ LED);}while(0) /**< see \ref platform_althal_led "LED definitions" */

  /*---------------------------------------------------------------------------*/

  #define PLATFORM_RTEM_BUTTON_1                       0x01u  /**< Push button S220 on the evaluation board */
  #define PLATFORM_RTEM_BUTTON_2                       0x02u  /**< Push button S221 on the evaluation board */
  #define PLATFORM_RTEM_BUTTON_3                       0x04u  /**< Push button S222 on the evaluation board */
  #define PLATFORM_RTEM_BUTTON_4                       0x08u  /**< Push button S223 on the evaluation board */
  #define PLATFORM_RTEM_BUTTON_5                       0x10u  /**< Push button S224 on the evaluation board */
  #define PLATFORM_RTEM_BUTTON_6                       0x20u  /**< Push button S225 on the evaluation board */
  #define PLATFORM_RTEM_BUTTON_7                       0x00u  /**< Unsupported */
  #define PLATFORM_RTEM_BUTTON_8                       0x00u  /**< Unsupported */

  /*---------------------------------------------------------------------------*/

  #define FLASH_FIELDBUS_FW_BASE_ADDRESS FLASH_FW_IMAGE1_OFFSET
  #define FIELDBUS_FW_SIZE               FLASH_FW_IMAGE1_SIZE
  //@}
#endif

#if defined(HARDWARE_INK) || defined(DOXYGEN)
  /** \anchor platform_althal_ink @name INK definitions
    * Defines specific to the Altera/Terasic Industrial Networking Kit (INK) */
  //@{
  #define PLATFORM_NAME PLATFORM_INK

  #define PLATFORM_LCD_SUPPORTED

  /*---------------------------------------------------------------------------*/

  #define PLATFORM_INK_LED_DEMO_POWER                 0x01   /**< LED LEDG0 */
  #define PLATFORM_INK_LED_DEMO_CONNECTED             0x02   /**< LED LEDG1 */
  #define PLATFORM_INK_LED_DEMO_ERROR                 0x00   /**< Unsupported */
  #define PLATFORM_INK_LED_PROTOCOL_GREEN1            0x04   /**< LED LEDG2 */
  #define PLATFORM_INK_LED_PROTOCOL_YELLOW1           0x08   /**< LED LEDG3 */
  #define PLATFORM_INK_LED_PROTOCOL_RED1              0x10   /**< LED LEDG4 */
  #define PLATFORM_INK_LED_PROTOCOL_GREEN2            0x20   /**< LED LEDG5 */
  #define PLATFORM_INK_LED_PROTOCOL_YELLOW2           0x40   /**< LED LEDG6 */
  #define PLATFORM_INK_LED_PROTOCOL_RED2              0x80   /**< LED LEDG7 */

  #define PLATFORM_INK_LED_ALL                        0xFF   /**< All green LEDs */

  /*---------------------------------------------------------------------------*/

  #define PLATFORM_INK_STATUS_LED_INIT()              do{ IOWR(APPL_SUBSYSTEM_PIO_LED_GREEN_BASE, 0, 0); }while(0) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_INK_STATUS_LED_IS_ON(LED)          (IORD(APPL_SUBSYSTEM_PIO_LED_GREEN_BASE, 0) & LED) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_INK_STATUS_LED_SET(VALUE)          do{ IOWR(APPL_SUBSYSTEM_PIO_LED_GREEN_BASE, 0, VALUE); }while(0) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_INK_STATUS_LED_SWITCH(LED, ACTION) do{ if(ACTION) {IOWR(APPL_SUBSYSTEM_PIO_LED_GREEN_BASE, 0, IORD(APPL_SUBSYSTEM_PIO_LED_GREEN_BASE, 0) | LED);} else {IOWR(APPL_SUBSYSTEM_PIO_LED_GREEN_BASE, 0, IORD(APPL_SUBSYSTEM_PIO_LED_GREEN_BASE, 0) & ~LED);} }while(0) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_INK_STATUS_LED_TOGGLE(LED)         do{ IOWR(APPL_SUBSYSTEM_PIO_LED_GREEN_BASE, 0, IORD(APPL_SUBSYSTEM_PIO_LED_GREEN_BASE, 0) ^ LED);}while(0) /**< see \ref platform_althal_led "LED definitions" */

  /*---------------------------------------------------------------------------*/

  #define PLATFORM_INK_DATA_LED_INIT()                do{ IOWR(APPL_SUBSYSTEM_PIO_LED_RED_BASE, 0, 0); }while(0) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_INK_DATA_LED_IS_ON(LED)            (IORD(APPL_SUBSYSTEM_PIO_LED_RED_BASE, 0) & ((LED << 8) | LED)) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_INK_DATA_LED_SET(VALUE)            do{ IOWR(APPL_SUBSYSTEM_PIO_LED_RED_BASE, 0, ((VALUE << 8) | VALUE)); }while(0) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_INK_DATA_LED_SWITCH(LED, ACTION)   do{ if(ACTION) {IOWR(APPL_SUBSYSTEM_PIO_LED_RED_BASE, 0, IORD(APPL_SUBSYSTEM_PIO_LED_RED_BASE, 0) | ((LED << 8) | LED));} else {IOWR(APPL_SUBSYSTEM_PIO_LED_RED_BASE, 0, IORD(APPL_SUBSYSTEM_PIO_LED_RED_BASE, 0) & ~((LED << 8) | LED));} }while(0) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_INK_DATA_LED_TOGGLE(LED)           do{ IOWR(APPL_SUBSYSTEM_PIO_LED_RED_BASE, 0, IORD(APPL_SUBSYSTEM_PIO_LED_RED_BASE, 0) ^ ((LED << 8) | LED));}while(0) /**< see \ref platform_althal_led "LED definitions" */

  /*---------------------------------------------------------------------------*/

  #define PLATFORM_INK_BUTTON_1                       0x01u  /**< Push button Key0 */
  #define PLATFORM_INK_BUTTON_2                       0x02u  /**< Push button Key1 */
  #define PLATFORM_INK_BUTTON_3                       0x04u  /**< Push button Key2 */
  #define PLATFORM_INK_BUTTON_4                       0x08u  /**< Push button Key3 */
  #define PLATFORM_INK_BUTTON_5                       0x00u  /**< Unsupported */
  #define PLATFORM_INK_BUTTON_6                       0x00u  /**< Unsupported */
  #define PLATFORM_INK_BUTTON_7                       0x00u  /**< Unsupported */
  #define PLATFORM_INK_BUTTON_8                       0x00u  /**< Unsupported */

  /*---------------------------------------------------------------------------*/

  #define FLASH_FIELDBUS_FW_BASE_ADDRESS FLASH_FW_IMAGE1_OFFSET
  #define FIELDBUS_FW_SIZE               FLASH_FW_IMAGE1_SIZE
  //@}
#endif

#if defined(HARDWARE_CVE) || defined(DOXYGEN)
  /** \anchor platform_althal_cve @name CVE definitions
    * Defines specific to the Altera Cyclone V E Development Kit */
  //@{
  #define PLATFORM_NAME PLATFORM_CVE

  #define PLATFORM_LCD_SUPPORTED

  /*---------------------------------------------------------------------------*/

  #define PLATFORM_CVE_LED_DEMO_POWER                 0x01   /**< LED USER_LED0 */
  #define PLATFORM_CVE_LED_DEMO_CONNECTED             0x02   /**< LED USER_LED1 */
  #define PLATFORM_CVE_LED_DEMO_ERROR                 0x00   /**< Unsupported */
  #define PLATFORM_CVE_LED_PROTOCOL_GREEN1            0x04   /**< LED USER_LED2 */
  #define PLATFORM_CVE_LED_PROTOCOL_YELLOW1           0x08   /**< LED USER_LED3 */
  #define PLATFORM_CVE_LED_PROTOCOL_RED1              0x00   /**< Unsupported */
  #define PLATFORM_CVE_LED_PROTOCOL_GREEN2            0x00   /**< Unsupported */
  #define PLATFORM_CVE_LED_PROTOCOL_YELLOW2           0x00   /**< Unsupported */
  #define PLATFORM_CVE_LED_PROTOCOL_RED2              0x00   /**< Unsupported */

  #define PLATFORM_CVE_LED_ALL                        0x0F   /**< All LEDs */

  /*---------------------------------------------------------------------------*/

  #define PLATFORM_CVE_STATUS_LED_INIT()              do{ IOWR(APPL_SUBSYSTEM_PIO_USER_LED_BASE, 0, 0xF); }while(0) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_CVE_STATUS_LED_IS_ON(LED)          !(IORD(APPL_SUBSYSTEM_PIO_USER_LED_BASE, 0) & LED) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_CVE_STATUS_LED_SET(VALUE)          do{ IOWR(APPL_SUBSYSTEM_PIO_USER_LED_BASE, 0, ~(VALUE)); }while(0) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_CVE_STATUS_LED_SWITCH(LED, ACTION) do{ if(ACTION) {IOWR(APPL_SUBSYSTEM_PIO_USER_LED_BASE, 0, IORD(APPL_SUBSYSTEM_PIO_USER_LED_BASE, 0) & ~LED);} else {IOWR(APPL_SUBSYSTEM_PIO_USER_LED_BASE, 0, IORD(APPL_SUBSYSTEM_PIO_USER_LED_BASE, 0) | (LED & 0xF));} }while(0) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_CVE_STATUS_LED_TOGGLE(LED)         do{ IOWR(APPL_SUBSYSTEM_PIO_USER_LED_BASE, 0, (IORD(APPL_SUBSYSTEM_PIO_USER_LED_BASE, 0) & 0xF) ^ (LED & 0xF));}while(0) /**< see \ref platform_althal_led "LED definitions" */

  /*---------------------------------------------------------------------------*/

  #define PLATFORM_CVE_DATA_LED_INIT()                                                                 /**< Unused */
  #define PLATFORM_CVE_DATA_LED_IS_ON(LED)            (SimulatedLeds & LED)                            /**< The data LEDs are emulated on the LCD display. See \ref platform_althal_led "LED definitions" */
  #define PLATFORM_CVE_DATA_LED_SET(VALUE)            platform_lcd_simulation_led_set(VALUE)           /**< The data LEDs are emulated on the LCD display. See \ref platform_althal_led "LED definitions" */
  #define PLATFORM_CVE_DATA_LED_SWITCH(LED, ACTION)   platform_lcd_simulation_led_switch(LED, ACTION)  /**< The data LEDs are emulated on the LCD display. See \ref platform_althal_led "LED definitions" */
  #define PLATFORM_CVE_DATA_LED_TOGGLE(LED)           platform_lcd_simulation_led_toggle(LED)          /**< The data LEDs are emulated on the LCD display. See \ref platform_althal_led "LED definitions" */

  /*---------------------------------------------------------------------------*/

  #define PLATFORM_CVE_BUTTON_1                       0x01u  /**< Push button USER_PB0 */
  #define PLATFORM_CVE_BUTTON_2                       0x02u  /**< Push button USER_PB1 */
  #define PLATFORM_CVE_BUTTON_3                       0x04u  /**< Push button USER_PB2 */
  #define PLATFORM_CVE_BUTTON_4                       0x08u  /**< Push button USER_PB3 */
  #define PLATFORM_CVE_BUTTON_5                       0x00u  /**< Unsupported */
  #define PLATFORM_CVE_BUTTON_6                       0x00u  /**< Unsupported */
  #define PLATFORM_CVE_BUTTON_7                       0x00u  /**< Unsupported */
  #define PLATFORM_CVE_BUTTON_8                       0x00u  /**< Unsupported */

  /*---------------------------------------------------------------------------*/

  #define RAM_FIELDBUS_FW_BASE_ADDRESS   RAM_BASE
  #define FLASH_FIELDBUS_FW_BASE_ADDRESS FLASH_FW_IMAGE1_OFFSET
  #define FIELDBUS_FW_SIZE               FLASH_FW_IMAGE1_SIZE
  //@}
#endif

#if defined(HARDWARE_DBC5) || defined(DOXYGEN)
  /** \anchor platform_althal_dbc5 @name DBC5 definitions
    * Defines specific to the DBC5 Evaluation board */
  //@{
  #define PLATFORM_NAME PLATFORM_DBC5

  /*---------------------------------------------------------------------------*/

  #define PLATFORM_DBC5_LED_DEMO_POWER                 0x01  /**< - */
  #define PLATFORM_DBC5_LED_DEMO_CONNECTED             0x02  /**< - */
  #define PLATFORM_DBC5_LED_DEMO_ERROR                 0x00  /**< - */
  #define PLATFORM_DBC5_LED_PROTOCOL_GREEN1            0x04  /**< - */
  #define PLATFORM_DBC5_LED_PROTOCOL_YELLOW1           0x08  /**< - */
  #define PLATFORM_DBC5_LED_PROTOCOL_RED1              0x10  /**< - */
  #define PLATFORM_DBC5_LED_PROTOCOL_GREEN2            0x20  /**< - */
  #define PLATFORM_DBC5_LED_PROTOCOL_YELLOW2           0x40  /**< - */
  #define PLATFORM_DBC5_LED_PROTOCOL_RED2              0x80  /**< - */

  #define PLATFORM_DBC5_LED_ALL                        0xFF  /**< - */

  /*---------------------------------------------------------------------------*/

  #define PLATFORM_DBC5_STATUS_LED_INIT()              do{ IOWR(APPL_SUBSYSTEM_PIO_LED_BASE, 0, 0); }while(0) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_DBC5_STATUS_LED_IS_ON(LED)          (IORD(APPL_SUBSYSTEM_PIO_LED_BASE, 0) & LED) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_DBC5_STATUS_LED_SET(VALUE)          do{ IOWR(APPL_SUBSYSTEM_PIO_LED_BASE, 0, VALUE); }while(0) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_DBC5_STATUS_LED_SWITCH(LED, ACTION) do{ if(ACTION) {IOWR(APPL_SUBSYSTEM_PIO_LED_BASE, 0, IORD(APPL_SUBSYSTEM_PIO_LED_BASE, 0) | LED);} else {IOWR(APPL_SUBSYSTEM_PIO_LED_BASE, 0, IORD(APPL_SUBSYSTEM_PIO_LED_BASE, 0) & ~LED);} }while(0) /**< see \ref platform_althal_led "LED definitions" */
  #define PLATFORM_DBC5_STATUS_LED_TOGGLE(LED)         do{ IOWR(APPL_SUBSYSTEM_PIO_LED_BASE, 0, IORD(APPL_SUBSYSTEM_PIO_LED_BASE, 0) ^ LED);}while(0) /**< see \ref platform_althal_led "LED definitions"  */

  /*---------------------------------------------------------------------------*/

  #define PLATFORM_DBC5_DATA_LED_INIT()                do{ ; }while(0)  /**< Unsupported */
  #define PLATFORM_DBC5_DATA_LED_IS_ON(LED)            0                /**< Unsupported */
  #define PLATFORM_DBC5_DATA_LED_SET(VALUE)            do{ ; }while(0)  /**< Unsupported */
  #define PLATFORM_DBC5_DATA_LED_SWITCH(LED, ACTION)   do{ ; }while(0)  /**< Unsupported */
  #define PLATFORM_DBC5_DATA_LED_TOGGLE(LED)           do{ ; }while(0)  /**< Unsupported */

  /*---------------------------------------------------------------------------*/

  #define PLATFORM_DBC5_BUTTON_1                       0x01u  /**< - */
  #define PLATFORM_DBC5_BUTTON_2                       0x02u  /**< - */
  #define PLATFORM_DBC5_BUTTON_3                       0x04u  /**< - */
  #define PLATFORM_DBC5_BUTTON_4                       0x08u  /**< - */
  #define PLATFORM_DBC5_BUTTON_5                       0x00u  /**< - */
  #define PLATFORM_DBC5_BUTTON_6                       0x00u  /**< - */
  #define PLATFORM_DBC5_BUTTON_7                       0x00u  /**< - */
  #define PLATFORM_DBC5_BUTTON_8                       0x00u  /**< - */
  //@}
#endif

/*---------------------------------------------------------------------------*/

/** \anchor platform_althal_led @name LED definitions */
//@{
#define LED_ON                         0x01                                         /**< Indicates LED is on */
#define LED_OFF                        0x00                                         /**< Indicates LED is off */

#define LED_ALL                        CONCAT(PLATFORM_NAME, _LED_ALL)              /**< Combination of all status LEDs that are available on the platform */

#define LED_DEMO_POWER                 CONCAT(PLATFORM_NAME, _LED_DEMO_POWER)       /**< Used as Power LED to signal that the stack is initialized */
#define LED_DEMO_CONNECTED             CONCAT(PLATFORM_NAME, _LED_DEMO_CONNECTED)   /**< Used as Connection LED to signal that a connection is established */
#define LED_DEMO_ERROR                 CONCAT(PLATFORM_NAME, _LED_DEMO_ERROR)       /**< Used as Error LED to signal any errors */
#define LED_PROTOCOL_GREEN1            CONCAT(PLATFORM_NAME, _LED_PROTOCOL_GREEN1)  /**< Usage is protocol specific. See protcol description for more details */
#define LED_PROTOCOL_YELLOW1           CONCAT(PLATFORM_NAME, _LED_PROTOCOL_YELLOW1) /**< Usage is protocol specific. See protcol description for more details */
#define LED_PROTOCOL_RED1              CONCAT(PLATFORM_NAME, _LED_PROTOCOL_RED1)    /**< Usage is protocol specific. See protcol description for more details */
#define LED_PROTOCOL_GREEN2            CONCAT(PLATFORM_NAME, _LED_PROTOCOL_GREEN2)  /**< Usage is protocol specific. See protcol description for more details */
#define LED_PROTOCOL_YELLOW2           CONCAT(PLATFORM_NAME, _LED_PROTOCOL_YELLOW2) /**< Usage is protocol specific. See protcol description for more details */
#define LED_PROTOCOL_RED2              CONCAT(PLATFORM_NAME, _LED_PROTOCOL_RED2)    /**< Usage is protocol specific. See protcol description for more details */

/*---------------------------------------------------------------------------*/

#define STATUS_LED_INIT()              CONCAT(PLATFORM_NAME, _STATUS_LED_INIT)()              /**< Initialize the status LEDs on the platform */
#define STATUS_LED_IS_ON(LED)          CONCAT(PLATFORM_NAME, _STATUS_LED_IS_ON)(LED)          /**< Returns if the selected status LED is on */
#define STATUS_LED_SET(VALUE)          CONCAT(PLATFORM_NAME, _STATUS_LED_SET)(VALUE)          /**< Sets the status LEDs to the new value */
#define STATUS_LED_SWITCH(LED, ACTION) CONCAT(PLATFORM_NAME, _STATUS_LED_SWITCH)(LED, ACTION) /**< Switch the selected status LED to the new state (#LED_ON/#LED_OFF) */
#define STATUS_LED_TOGGLE(LED)         CONCAT(PLATFORM_NAME, _STATUS_LED_TOGGLE)(LED)         /**< Toggles the state of the selected status LED */

/*---------------------------------------------------------------------------*/

#define DATA_LED_INIT()                CONCAT(PLATFORM_NAME, _DATA_LED_INIT)()              /**< Initialize the data LEDs on the platform */
#define DATA_LED_IS_ON(LED)            CONCAT(PLATFORM_NAME, _DATA_LED_IS_ON)(LED)          /**< Returns if the selected data LED is on */
#define DATA_LED_SET(VALUE)            CONCAT(PLATFORM_NAME, _DATA_LED_SET)(VALUE)          /**< Sets the data LEDs to the new value */
#define DATA_LED_SWITCH(LED, ACTION)   CONCAT(PLATFORM_NAME, _DATA_LED_SWITCH)(LED, ACTION) /**< Switch the selected data LED to the new state (#LED_ON/#LED_OFF) */
#define DATA_LED_TOGGLE(LED)           CONCAT(PLATFORM_NAME, _DATA_LED_TOGGLE)(LED)         /**< Toggles the state of the selected data LED */
//@}

/*---------------------------------------------------------------------------*/

/** \anchor Button @name Button definitions */
//@{
#define BUTTON_1               CONCAT(PLATFORM_NAME, _BUTTON_1) /**< Usage is protocol specific. See protcol description for more details */
#define BUTTON_2               CONCAT(PLATFORM_NAME, _BUTTON_2) /**< Usage is protocol specific. See protcol description for more details */
#define BUTTON_3               CONCAT(PLATFORM_NAME, _BUTTON_3) /**< Used to change the speed of the running light sample */
#define BUTTON_4               CONCAT(PLATFORM_NAME, _BUTTON_4) /**< Used to change the speed of the running light sample */
#define BUTTON_5               CONCAT(PLATFORM_NAME, _BUTTON_5) /**< Usage is protocol specific. See protcol description for more details */
#define BUTTON_6               CONCAT(PLATFORM_NAME, _BUTTON_6) /**< Usage is protocol specific. See protcol description for more details */
#define BUTTON_7               CONCAT(PLATFORM_NAME, _BUTTON_7) /**< Usage is protocol specific. See protcol description for more details */
#define BUTTON_8               CONCAT(PLATFORM_NAME, _BUTTON_8) /**< Usage is protocol specific. See protcol description for more details */
//@}

/*---------------------------------------------------------------------------*/

#define FLASH_NUMBER_FW_SEGMENTS    3  /**< Number of segments in the firmware file for this platform */

/*---------------------------------------------------------------------------*/

/* bits 0 to 2 are predefined and used by the demo application to measure
 * execution time of fetching the outputs and setting the input data
 */
#define PERFORMANCE_INDICATOR_OUTPUT_DATA_CALLBACK    0x01u
#define PERFORMANCE_INDICATOR_INPUT_DATA              0x02u
#define PERFORMANCE_INDICATOR_OUTPUT_DATA             0x04u
/* bits 3 to 7 are also connected in the design and can be used on requirement */

#ifdef APPL_SUBSYSTEM_PIO_PERFORMANCE_INDICATOR_BASE
#define FUNCTION_ENTRY(Function) { IOWR (APPL_SUBSYSTEM_PIO_PERFORMANCE_INDICATOR_BASE, 4, Function); } while (0)
#define FUNCTION_EXIT(Function)  { IOWR (APPL_SUBSYSTEM_PIO_PERFORMANCE_INDICATOR_BASE, 5, Function); } while (0)
#else
#define FUNCTION_ENTRY(Function) do { ; } while (0)
#define FUNCTION_EXIT(Function)  do { ; } while (0)
#endif

/*---------------------------------------------------------------------------*/

#define PLATFORM_RESTART()          do {(*(void(*)(void))&__reset)();} while (0)  /**< restarts the platform */

#define PLATFORM_TRIGGER_WATCHDOG() do { ; } while (0)                            /**< not supported for the platform */

/*---------------------------------------------------------------------------*/

#if defined APPL_SUBSYSTEM_INSTRUCTION_TCM_BASE
#define _LOCATED_INTO_INSTRUCTION_FAST_MEMORY  __attribute__((section(".appl_subsystem_instruction_tcm")))
#else
#define _LOCATED_INTO_INSTRUCTION_FAST_MEMORY
#endif

/*---------------------------------------------------------------------------*/

/** \anchor TestPattern @name Test Pattern definitions */
//@{
#if defined(__SOFTING_TEST_PATTERN) || defined(DOXYGEN)
#define PLATFORM_DIRECT_PERIPHERAL_ACCESS

#if defined(TEST_SUBSYSTEM_PATTERN_COUNTER_BASE) || defined(DOXYGEN)
#define PLATFORM_PERIPHERAL_COUNTER_BASE       0x00000000
#define PLATFORM_PERIPHERAL_COUNTER_WIDTH      TEST_SUBSYSTEM_PATTERN_COUNTER_DATA_WIDTH
#define PLATFORM_PERIPHERAL_COUNTER_VECTOR     32u
#endif

#if defined(TEST_SUBSYSTEM_PATTERN_RAMP_BASE) || defined(DOXYGEN)
#define PLATFORM_PERIPHERAL_RAMP_BASE          0x00000010
#define PLATFORM_PERIPHERAL_RAMP_WIDTH         TEST_SUBSYSTEM_PATTERN_RAMP_DATA_WIDTH
#define PLATFORM_PERIPHERAL_RAMP_VECTOR        33u
#endif

#if defined(TEST_SUBSYSTEM_PATTERN_SINUS_BASE) || defined(DOXYGEN)
#define PLATFORM_PERIPHERAL_SINUS_BASE         0x00000020
#define PLATFORM_PERIPHERAL_SINUS_WIDTH        TEST_SUBSYSTEM_PATTERN_SINUS_DATA_WIDTH
#define PLATFORM_PERIPHERAL_SINUS_VECTOR       34u
#endif

#if defined(TEST_SUBSYSTEM_PATTERN_CONSTANT_BASE) || defined(DOXYGEN)
#define PLATFORM_PERIPHERAL_CONSTANT_BASE      0x00000030
#define PLATFORM_PERIPHERAL_CONSTANT_WIDTH     TEST_SUBSYSTEM_PATTERN_CONSTANT_DATA_WIDTH
#define PLATFORM_PERIPHERAL_CONSTANT_VECTOR    35u
#endif

#if defined(TEST_SUBSYSTEM_PATTERN_SINK_BASE) || defined(DOXYGEN)
#define PLATFORM_PERIPHERAL_SINK_BASE          0x00000040
#define PLATFORM_PERIPHERAL_SINK_WIDTH         TEST_SUBSYSTEM_PATTERN_SINK_DATA_WIDTH
#define PLATFORM_PERIPHERAL_SINK_VECTOR        36u
#endif

#if defined(TEST_SUBSYSTEM_PATTERN_MIRROR_BASE) || defined(DOXYGEN)
#define PLATFORM_PERIPHERAL_MIRROR_BASE        0x00000050
#define PLATFORM_PERIPHERAL_MIRROR_WIDTH       TEST_SUBSYSTEM_PATTERN_MIRROR_DATA_WIDTH
#define PLATFORM_PERIPHERAL_MIRROR_VECTOR      37u
#endif

#if defined(TEST_SUBSYSTEM_PATTERN_ISOCHRONOUS_BASE) || defined(DOXYGEN)
#define PLATFORM_PERIPHERAL_ISOCHRONOUS_BASE   0x00000080
#define PLATFORM_PERIPHERAL_ISOCHRONOUS_WIDTH  TEST_SUBSYSTEM_PATTERN_ISOCHRONOUS_DATA_WIDTH
#define PLATFORM_PERIPHERAL_ISOCHRONOUS_VECTOR 60u
#endif
//@}
#endif
//@}

/******************************************************************************
TYPEDEFS
******************************************************************************/

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

/** \addtogroup platform_functions */
//@{
void   platform_lcd_simulation_led_set      (unsigned char);
void   platform_lcd_simulation_led_switch   (unsigned char, int);
void   platform_lcd_simulation_led_toggle   (unsigned char);
//@}

/** \addtogroup platform_testpattern_func */
//@{
#if defined (__SOFTING_TEST_PATTERN) || defined(DOXYGEN)
void   test_pattern_init_default            (void);
BOOL   test_pattern_counter_init            (U32, U32, BOOL, U32);
BOOL   test_pattern_ramp_init               (U32, U32, BOOL, U32, U32);
BOOL   test_pattern_constant_init           (U32, U32, U32);
BOOL   test_pattern_sinus_init              (U32, U32, U32, U32);
BOOL   test_pattern_mirror_init             (U32, U32, U32);
#endif
//@}

/******************************************************************************
GLOBAL DATA
******************************************************************************/

extern void* __reset;

/******************************************************************************
PRIVATE DATA
******************************************************************************/

#endif /* __DEMO_PLATFORM_H__ */

