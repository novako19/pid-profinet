/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

#include <sdai.h>

#include "demo_platform.h"

#if defined (__SOFTING_TEST_PATTERN) || defined(DOXYGEN)

/******************************************************************************
DEFINES
******************************************************************************/

/** \addtogroup platform_testpattern_def */
//@{
#define TEST_PATTERN_TYPE_COUNTER                   1
#define TEST_PATTERN_TYPE_RAMP                      2
#define TEST_PATTERN_TYPE_SINUS                     3
#define TEST_PATTERN_TYPE_CONSTANT                  4
#define TEST_PATTERN_TYPE_SINK                      5
#define TEST_PATTERN_TYPE_MIRROR                    6

#define TEST_PATTERN_READ_VERSION_OFFSET            0uL
#define TEST_PATTERN_READ_TYPE_OFFSET               4uL
#define TEST_PATTERN_WRITE_CONTROL_OFFSET           0uL
#define TEST_PATTERN_WRITE_PARAMETER1_OFFSET        4uL
#define TEST_PATTERN_WRITE_PARAMETER2_OFFSET        8uL
#define TEST_PATTERN_WRITE_PARAMETER3_OFFSET        12uL

#define TEST_PATTERN_CONTROL_ENABLE                 0x00000001
#define TEST_PATTERN_CONTROL_DIRECTION              0x00000002
#define TEST_PATTERN_CONTROL_ENABLE_COUNTER_CHECK   0x00000001
#define TEST_PATTERN_CONTROL_ENABLE_RAMP_CHECK      0x00000002

#define TEST_PATTERN_COUNTER_PRESCALE_OFFSET        TEST_PATTERN_WRITE_PARAMETER1_OFFSET
#define TEST_PATTERN_COUNTER_STEP_OFFSET            TEST_PATTERN_WRITE_PARAMETER2_OFFSET

#define TEST_PATTERN_RAMP_PRESCALE_OFFSET           TEST_PATTERN_WRITE_PARAMETER1_OFFSET
#define TEST_PATTERN_RAMP_STEP_OFFSET               TEST_PATTERN_WRITE_PARAMETER2_OFFSET
#define TEST_PATTERN_RAMP_LIMIT_OFFSET              TEST_PATTERN_WRITE_PARAMETER3_OFFSET

#define TEST_PATTERN_CONSTANT_PRESCALE_OFFSET       TEST_PATTERN_WRITE_PARAMETER1_OFFSET
#define TEST_PATTERN_CONSTANT_VALUE_OFFSET          TEST_PATTERN_WRITE_PARAMETER2_OFFSET

#define TEST_PATTERN_SINUS_PRESCALE_OFFSET          TEST_PATTERN_WRITE_PARAMETER1_OFFSET
#define TEST_PATTERN_SINUS_SCALE_OFFSET             TEST_PATTERN_WRITE_PARAMETER2_OFFSET
#define TEST_PATTERN_SINUS_OFFSET_OFFSET            TEST_PATTERN_WRITE_PARAMETER3_OFFSET

#define TEST_PATTERN_SINK_PRESCALE_OFFSET           TEST_PATTERN_WRITE_PARAMETER1_OFFSET

#define TEST_PATTERN_MIRROR_INPUT_DELAY_OFFSET      TEST_PATTERN_WRITE_PARAMETER1_OFFSET
#define TEST_PATTERN_MIRROR_OUTPUT_DELAY_OFFSET     TEST_PATTERN_WRITE_PARAMETER2_OFFSET
//@}

/******************************************************************************
TYPEDEFS
******************************************************************************/

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

/******************************************************************************
PUBLIC DATA
******************************************************************************/

/******************************************************************************
PRIVATE DATA
******************************************************************************/

/******************************************************************************
FUNCTION IMPLEMENTATION
******************************************************************************/

/**
 * The function test_pattern_counter_init () is called to initialize the
 * counter-test-pattern-generator. This generator runs with the Avalon-system clock.
 * It has the following parameter registers:
 *
 * <TABLE><TR>
 * <TH>Register</TH><TH>Offset</TH><TH>Access</TH><TH>Description</TH></TR>
 * <TR><TD>Version</TD><TD>0</TD><TD>R</TD><TD>
 * Revision of the counter-test-pattern-generator.
 * </TD></TR>
 * <TR><TD>Type</TD><TD>4</TD><TD>R</TD><TD>
 * Type (\ref #TEST_PATTERN_TYPE_COUNTER) of the test-pattern-generator.
 * </TD></TR>
 * <TR><TD>Control</TD><TD>0</TD><TD>W</TD><TD>
 * Enables or disables the counter. Parameterizes the direction of counting (up / down).
 * </TD></TR>
 * <TR><TD>Parameter 1</TD><TD>4</TD><TD>W</TD><TD>
 * The parameter 1 is a prescaler and reduces the input clock (Avalon-system clock)
 * to a lower frequency.
 * </TD></TR>
 * <TR><TD>Parameter 2</TD><TD>8</TD><TD>W</TD><TD>
 * The parameter 2 is the value by which the counter is incremented.
 * </TD></TR>
 * </TABLE>
 *
 * @return
 * - type : BOOL
 * - value: TRUE or FALSE
 * @param[in] BaseAddress Register-Base-Address of the counter
 * - type : U32
 * - range: depends on the QSYS-design.
 * @param[in] Prescale reduces input frequency
 * - type : U32
 * - range: whole range is valid
 * @param[in] Direction parameterizes counter direction
 * - type : BOOL
 * - range: TRUE or FALSE
 * @param[in] Step value which is added/subtracted to/from counter
 * - type : U32
 * - range: whole range is valid
 */
BOOL test_pattern_counter_init (U32 BaseAddress, U32 Prescale, BOOL Direction, U32 Step)
{
  U32 Control = TEST_PATTERN_CONTROL_ENABLE;

  if (IORD (BaseAddress + TEST_PATTERN_READ_TYPE_OFFSET, 0) != TEST_PATTERN_TYPE_COUNTER)
  {
    return (FALSE);
  }

  if (Direction) {Control |= TEST_PATTERN_CONTROL_DIRECTION;}

  IOWR (BaseAddress + TEST_PATTERN_COUNTER_PRESCALE_OFFSET, 0, Prescale);
  IOWR (BaseAddress + TEST_PATTERN_COUNTER_STEP_OFFSET, 0, Step);

  IOWR (BaseAddress + TEST_PATTERN_WRITE_CONTROL_OFFSET, 0, Control);

  return (TRUE);
}

/*===========================================================================*/

/**
 * The function test_pattern_ramp_init () is called to initialize the
 * ramp-test-pattern-generator. This generator runs with the Avalon-system clock.
 * It has the following parameter registers:
 *
 * <TABLE><TR>
 * <TH>Register</TH><TH>Offset</TH><TH>Access</TH><TH>Description</TH></TR>
 * <TR><TD>Version</TD><TD>0</TD><TD>R</TD><TD>
 * Revision of the ramp-test-pattern-generator.
 * </TD></TR>
 * <TR><TD>Type</TD><TD>4</TD><TD>R</TD><TD>
 * Type (\ref #TEST_PATTERN_TYPE_RAMP) of the test-pattern-generator.
 * </TD></TR>
 * <TR><TD>Control</TD><TD>0</TD><TD>W</TD><TD>
 * Enables or disables the ramp. Parameterizes the direction of counting (up / down).
 * </TD></TR>
 * <TR><TD>Parameter 1</TD><TD>4</TD><TD>W</TD><TD>
 * The parameter 1 is a prescaler and reduces the input clock (Avalon-system clock)
 * to a lower frequency.
 * </TD></TR>
 * <TR><TD>Parameter 2</TD><TD>8</TD><TD>W</TD><TD>
 * The parameter 2 is the value by which the counter is incremented.
 * </TD></TR>
 * <TR><TD>Parameter 3</TD><TD>12</TD><TD>W</TD><TD>
 * The parameter 3 is the limit until the ramp counts. If this limit is exceeded, the
 * counter is reset to zero.
 * </TD></TR>
 * </TABLE>
 *
 * @return
 * - type : BOOL
 * - value: TRUE or FALSE
 * @param[in] BaseAddress Register-Base-Address of the ramp
 * - type : U32
 * - range: depends on the QSYS-design.
 * @param[in] Prescale reduces input frequency
 * - type : U32
 * - range: whole range is valid
 * @param[in] Direction parameterizes counter direction
 * - type : BOOL
 * - range: TRUE or FALSE
 * @param[in] Step value which is added/subtracted to/from counter
 * - type : U32
 * - range: whole range is valid
 * @param[in] Limit value which resets the counter to zero
 * - type : U32
 * - range: whole range is valid
 */
BOOL test_pattern_ramp_init (U32 BaseAddress, U32 Prescale, BOOL Direction, U32 Step, U32 Limit)
{
  U32 Control = TEST_PATTERN_CONTROL_ENABLE;

  if (IORD (BaseAddress + TEST_PATTERN_READ_TYPE_OFFSET, 0) != TEST_PATTERN_TYPE_RAMP)
  {
    return (FALSE);
  }

  if (Direction) {Control |= TEST_PATTERN_CONTROL_DIRECTION;}

  IOWR (BaseAddress + TEST_PATTERN_RAMP_PRESCALE_OFFSET, 0, Prescale);
  IOWR (BaseAddress + TEST_PATTERN_RAMP_STEP_OFFSET, 0, Step);
  IOWR (BaseAddress + TEST_PATTERN_RAMP_LIMIT_OFFSET, 0, Limit);

  IOWR (BaseAddress + TEST_PATTERN_WRITE_CONTROL_OFFSET, 0, Control);

  return (TRUE);
}

/*===========================================================================*/

/**
 * The function test_pattern_constant_init () is called to initialize the
 * constant-test-pattern-generator. This generator runs with the Avalon-system clock.
 * It has the following parameter registers:
 *
 * <TABLE><TR>
 * <TH>Register</TH><TH>Offset</TH><TH>Access</TH><TH>Description</TH></TR>
 * <TR><TD>Version</TD><TD>0</TD><TD>R</TD><TD>
 * Revision of the constant-test-pattern-generator.
 * </TD></TR>
 * <TR><TD>Type</TD><TD>4</TD><TD>R</TD><TD>
 * Type (\ref #TEST_PATTERN_TYPE_CONSTANT) of the test-pattern-generator.
 * </TD></TR>
 * <TR><TD>Control</TD><TD>0</TD><TD>W</TD><TD>
 * Enables or disables the constant.
 * </TD></TR>
 * <TR><TD>Parameter 1</TD><TD>4</TD><TD>W</TD><TD>
 * The parameter 1 is a prescaler and reduces the input clock (Avalon-system clock)
 * to a lower frequency.
 * </TD></TR>
 * <TR><TD>Parameter 2</TD><TD>8</TD><TD>W</TD><TD>
 * The parameter 2 is the value which is provided as constant.
 * </TD></TR>
 * </TABLE>
 *
 * @return
 * - type : BOOL
 * - value: TRUE or FALSE
 * @param[in] BaseAddress Register-Base-Address of the counter
 * - type : U32
 * - range: depends on the QSYS-design.
 * @param[in] Prescale reduces input frequency
 * - type : U32
 * - range: whole range is valid
 * @param[in] Value is the provided constant
 * - type : U32
 * - range: whole range is valid
 */
BOOL test_pattern_constant_init (U32 BaseAddress, U32 Prescale, U32 Value)
{
  if (IORD (BaseAddress + TEST_PATTERN_READ_TYPE_OFFSET, 0) != TEST_PATTERN_TYPE_CONSTANT)
  {
    return (FALSE);
  }

  IOWR (BaseAddress + TEST_PATTERN_CONSTANT_PRESCALE_OFFSET, 0, Prescale);
  IOWR (BaseAddress + TEST_PATTERN_CONSTANT_VALUE_OFFSET, 0, Value);

  IOWR (BaseAddress + TEST_PATTERN_WRITE_CONTROL_OFFSET, 0, TEST_PATTERN_CONTROL_ENABLE);

  return (TRUE);
}

/*===========================================================================*/

/**
 * The function test_pattern_sinus_init () is called to initialize the
 * sinus-test-pattern-generator. This generator runs with the Avalon-system clock.
 * It has the following parameter registers:
 *
 * <TABLE><TR>
 * <TH>Register</TH><TH>Offset</TH><TH>Access</TH><TH>Description</TH></TR>
 * <TR><TD>Version</TD><TD>0</TD><TD>R</TD><TD>
 * Revision of the sinus-test-pattern-generator.
 * </TD></TR>
 * <TR><TD>Type</TD><TD>4</TD><TD>R</TD><TD>
 * Type (\ref #TEST_PATTERN_TYPE_SINUS) of the test-pattern-generator.
 * </TD></TR>
 * <TR><TD>Control</TD><TD>0</TD><TD>W</TD><TD>
 * Enables or disables the sinus.
 * </TD></TR>
 * <TR><TD>Parameter 1</TD><TD>4</TD><TD>W</TD><TD>
 * The parameter 1 is a prescaler and reduces the input clock (Avalon-system clock)
 * to a lower frequency.
 * </TD></TR>
 * <TR><TD>Parameter 2</TD><TD>8</TD><TD>W</TD><TD>
 * The parameter 2 is the scale which is multiplied to the sinus value.
 * </TD></TR>
 * <TR><TD>Parameter 3</TD><TD>12</TD><TD>W</TD><TD>
 * The parameter 3 is the offset by which the sinus is shifted in direction of the y-axis.
 * </TD></TR>
 * </TABLE>
 *
 * @return
 * - type : BOOL
 * - value: TRUE or FALSE
 * @param[in] BaseAddress Register-Base-Address of the ramp
 * - type : U32
 * - range: depends on the QSYS-design.
 * @param[in] Prescale reduces input frequency
 * - type : U32
 * - range: whole range is valid
 * @param[in] Scale value which is multiplied to the sinus value
 * - type : U32
 * - range: whole range is valid
 * @param[in] Offset shifts the sinus in direction of y-axis
 * - type : U32
 * - range: whole range is valid
 */
BOOL test_pattern_sinus_init (U32 BaseAddress, U32 Prescale, U32 Scale, U32 Offset)
{
  if (IORD (BaseAddress + TEST_PATTERN_READ_TYPE_OFFSET, 0) != TEST_PATTERN_TYPE_SINUS)
  {
    return (FALSE);
  }

  IOWR (BaseAddress + TEST_PATTERN_SINUS_PRESCALE_OFFSET, 0, Prescale);
  IOWR (BaseAddress + TEST_PATTERN_SINUS_SCALE_OFFSET, 0, Scale);
  IOWR (BaseAddress + TEST_PATTERN_SINUS_OFFSET_OFFSET, 0, Offset);

  IOWR (BaseAddress + TEST_PATTERN_WRITE_CONTROL_OFFSET, 0, TEST_PATTERN_CONTROL_ENABLE);

  return (TRUE);
}

/*===========================================================================*/

/**
 * The function test_pattern_sink_init () is called to initialize the
 * sink-test-pattern-generator. This generator runs with the Avalon-system clock.
 * It has the following parameter registers:
 *
 * <TABLE><TR>
 * <TH>Register</TH><TH>Offset</TH><TH>Access</TH><TH>Description</TH></TR>
 * <TR><TD>Version</TD><TD>0</TD><TD>R</TD><TD>
 * Revision of the sink-test-pattern-generator.
 * </TD></TR>
 * <TR><TD>Type</TD><TD>4</TD><TD>R</TD><TD>
 * Type (\ref #TEST_PATTERN_TYPE_SINK) of the test-pattern-generator.
 * </TD></TR>
 * <TR><TD>Control</TD><TD>0</TD><TD>W</TD><TD>
 * Enables or disables the sink checks (counter/ramp).
 * </TD></TR>
 * </TABLE>
 *
 * @return
 * - type : BOOL
 * - value: TRUE or FALSE
 * @param[in] BaseAddress Register-Base-Address of the ramp
 * - type : U32
 * - range: depends on the QSYS-design.
 * @param[in] Control enables/disables checking of data
 * - type : U32
 * - range: 0 | #TEST_PATTERN_CONTROL_ENABLE_COUNTER_CHECK | #TEST_PATTERN_CONTROL_ENABLE_RAMP_CHECK
 * @param[in] Prescale reduces check frequency
 * - type : U32
 * - range: whole range is valid
 */
BOOL test_pattern_sink_init (U32 BaseAddress, U32 Control, U32 Prescale)
{
  if (IORD (BaseAddress + TEST_PATTERN_READ_TYPE_OFFSET, 0) != TEST_PATTERN_TYPE_SINK)
  {
    return (FALSE);
  }

  IOWR (BaseAddress + TEST_PATTERN_SINK_PRESCALE_OFFSET, 0, Prescale);
  IOWR (BaseAddress + TEST_PATTERN_WRITE_CONTROL_OFFSET, 0, Control);

  return (TRUE);
}

/*===========================================================================*/

/**
 * The function test_pattern_mirror_init () is called to initialize the
 * mirror-test-pattern-generator. This generator runs with the Avalon-system clock.
 * It has the following parameter registers:
 *
 * <TABLE><TR>
 * <TH>Register</TH><TH>Offset</TH><TH>Access</TH><TH>Description</TH></TR>
 * <TR><TD>Version</TD><TD>0</TD><TD>R</TD><TD>
 * Revision of the mirror-test-pattern-generator.
 * </TD></TR>
 * <TR><TD>Type</TD><TD>4</TD><TD>R</TD><TD>
 * Type (\ref #TEST_PATTERN_TYPE_MIRROR) of the test-pattern-generator.
 * </TD></TR>
 * <TR><TD>Control</TD><TD>0</TD><TD>W</TD><TD>
 * Enables or disables the mirror.
 * </TD></TR>
 * <TR><TD>Parameter 1</TD><TD>4</TD><TD>W</TD><TD>
 * The parameter 1 is a the input delay of the isochronous mirror. The delay starts
 * at the begin of the RED-phase. If the delay timer reaches zero the input data is
 * latched from the inputs.
 * </TD></TR>
 * <TR><TD>Parameter 2</TD><TD>8</TD><TD>W</TD><TD>
 * The parameter 2 is a the output delay of the isochronous mirror. The delay starts
 * at the begin of the RED-phase. If the delay timer reaches zero the output data is
 * provided.
 * </TD></TR>
 * </TABLE>
 *
 * @return
 * - type : BOOL
 * - value: TRUE or FALSE
 * @param[in] BaseAddress Register-Base-Address of the ramp
 * - type : U32
 * - range: depends on the QSYS-design.
 * @param[in] InputDelay isochronous input delay
 * - type : U32
 * - range: whole range is valid
 * @param[in] OutputDelay isochronous output delay
 * - type : U32
 * - range: whole range is valid
 */
BOOL test_pattern_mirror_init (U32 BaseAddress, U32 InputDelay, U32 OutputDelay)
{
  if (IORD (BaseAddress + TEST_PATTERN_READ_TYPE_OFFSET, 0) != TEST_PATTERN_TYPE_MIRROR)
  {
    return (FALSE);
  }

  IOWR (BaseAddress + TEST_PATTERN_MIRROR_INPUT_DELAY_OFFSET, 0, InputDelay);
  IOWR (BaseAddress + TEST_PATTERN_MIRROR_OUTPUT_DELAY_OFFSET, 0, OutputDelay);

  IOWR (BaseAddress + TEST_PATTERN_WRITE_CONTROL_OFFSET, 0, TEST_PATTERN_CONTROL_ENABLE);

  return (TRUE);
}

/*===========================================================================*/

/**
 * The function test_pattern_init_default () is called to initialize all available
 * test-pattern-generators with default values.
 *
 * @return
 * - type : void
 */
void test_pattern_init_default (void)
{
  #ifdef TEST_SUBSYSTEM_PATTERN_COUNTER_BASE
  test_pattern_counter_init (TEST_SUBSYSTEM_PATTERN_COUNTER_BASE, (128 * APPL_SUBSYSTEM_TIMER_LOAD_VALUE), FALSE, 1);
  #endif

  #ifdef TEST_SUBSYSTEM_PATTERN_RAMP_BASE
  test_pattern_ramp_init (TEST_SUBSYSTEM_PATTERN_RAMP_BASE, (APPL_SUBSYSTEM_TIMER_LOAD_VALUE / 2), FALSE, 1, 10000);
  #endif

  #ifdef TEST_SUBSYSTEM_PATTERN_CONSTANT_BASE
  test_pattern_constant_init (TEST_SUBSYSTEM_PATTERN_CONSTANT_BASE, (APPL_SUBSYSTEM_TIMER_LOAD_VALUE / 2), 0x08154711);
  #endif

  #ifdef TEST_SUBSYSTEM_PATTERN_SINUS_BASE
  /* The value range of the sinus wave is from -128 to 127. To move it out of the negative we set an offset of 128 */
  test_pattern_sinus_init (TEST_SUBSYSTEM_PATTERN_SINUS_BASE, (APPL_SUBSYSTEM_TIMER_LOAD_VALUE / 8), 0, 128);
  #endif

  #ifdef TEST_SUBSYSTEM_PATTERN_SINK_BASE
  /* The sink can check counter or ramp behavior for the data. */
  test_pattern_sink_init (TEST_SUBSYSTEM_PATTERN_SINK_BASE, TEST_PATTERN_CONTROL_ENABLE_COUNTER_CHECK, 2);
  #endif

  return;
}

#endif /* __SOFTING_TEST_PATTERN */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */

