/*
 * linker.h - Linker script mapping information
 *
 * Machine generated for CPU 'appl_subsystem_cpu' in SOPC Builder design 'qsys_profinet_system'
 * SOPC Builder design path: ../../../../../hardware/fpga/profinet/altera_ink_switch/qsys_profinet_system.sopcinfo
 *
 * Generated: Thu Mar 24 19:48:14 CET 2016
 */

/*
 * DO NOT MODIFY THIS FILE
 *
 * Changing this file will have subtle consequences
 * which will almost certainly lead to a nonfunctioning
 * system. If you do modify this file, be aware that your
 * changes will be overwritten and lost when this file
 * is generated again.
 *
 * DO NOT MODIFY THIS FILE
 */

/*
 * License Agreement
 *
 * Copyright (c) 2008
 * Altera Corporation, San Jose, California, USA.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * This agreement shall be governed in all respects by the laws of the State
 * of California and by the laws of the United States of America.
 */

#ifndef __LINKER_H_
#define __LINKER_H_


/*
 * BSP controls alt_load() behavior in crt0.
 *
 */

#define ALT_LOAD_EXPLICITLY_CONTROLLED


/*
 * Base address and span (size in bytes) of each linker region
 *
 */

#define APPL_SUBSYSTEM_INSTRUCTION_TCM_REGION_BASE 0x2000
#define APPL_SUBSYSTEM_INSTRUCTION_TCM_REGION_SPAN 8192
#define FLASH_REGION_BASE 0x20000020
#define FLASH_REGION_SPAN 2016
#define RAM_REGION_BASE 0x41000000
#define RAM_REGION_SPAN 117440512
#define RESET_REGION_BASE 0x20000000
#define RESET_REGION_SPAN 32
#define RTE_SUBSYSTEM_FIELDBUS_UTILITY_DPRAM_REGION_BASE 0x10000
#define RTE_SUBSYSTEM_FIELDBUS_UTILITY_DPRAM_REGION_SPAN 12288
#define SRAM_REGION_BASE 0x50000000
#define SRAM_REGION_SPAN 2097152


/*
 * Devices associated with code sections
 *
 */

#define ALT_EXCEPTIONS_DEVICE SRAM
#define ALT_RESET_DEVICE FLASH
#define ALT_RODATA_DEVICE SRAM
#define ALT_RWDATA_DEVICE SRAM
#define ALT_TEXT_DEVICE SRAM

#endif /* __LINKER_H_ */
