/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

#ifdef SDAI_SOCKET_INTERFACE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include "sdai.h"
#include "sdai_socket.h"

#include "demo_platform.h"
#include "demo.h"

#include "socket.h"

#ifdef INCLUDE_WEBSERVER
#include "webserver.h"
#endif

/******************************************************************************
DEFINES
******************************************************************************/

/** \addtogroup socket_def */
//@{
#define _CONSOLE_HEADER \
"You are connected to the Softing command shell, Version 1.00 (Type \"help\" for command overview)\r\r\n\r\r\n"

#define _USAGE_HEADER_SOCKET \
"\r\r\n *** SOCKET COMMANDS ***\r\r\n\r\r\n"

#define _USAGE_SOCKET_OVERVIEW \
" socket_overview\r\r\n\tPrints an overview of all sockets\r\r\n"

#define _USAGE_SOCKET_STATUS \
" socket_status SOCKET\r\r\n\tPrints the status of the selected socket [0...(MAX_NUMBER_SUPPORTED_SOCKETS - 1)]\r\r\n"

#define _USAGE_OPEN \
" open \"tcp | udp\" PORT [keepalive] | [broadcast] [multicast]\r\r\n\tOpens a tcp/udp port PORT [0...65535] with broadcast and/or multicast possibilities (udp only) or keepalive(tcp only)\r\r\n"

#define _USAGE_CLOSE \
" close \"tcp | udp\" PORT\r\r\n\tCloses a tcp/udp port PORT [0...65535]\r\r\n"

#define _USAGE_CONNECT \
" connect IP-ADDRESS:PORT\r\r\n\tConnects a tcp server at address IP-ADDRESS and port PORT [0...65535]\r\r\n"

#define _USAGE_DISCONNECT \
" disconnect IP-ADDRESS:PORT [SOCKET]\r\r\n\tDisconnects a tcp connection at/from address IP-ADDRESS and port PORT [0...65535] on socket SOCKET [0...(MAX_NUMBER_SUPPORTED_SOCKETS - 1)]\r\r\n"

#define _USAGE_SEND \
" send \"tcp | udp\" IP-ADDRESS:PORT [LOCALPORT] LENGTH\r\r\n\tSends a tcp/udp packets with LENGTH to remote node at address IP-ADDRESS and port PORT [0...65535]\r\r\n"

#define _USAGE_ECHO \
" echo \"tcp | udp\" PORT\r\r\n\tEnables/disables echo on the TCP/UDP Port\r\r\n"

#define _USAGE_FILTER \
" filter PORT [IP-ADDRESS][:IP-ADDRESS[:IP-ADDRESS[:IP-ADDRESS]]]\r\r\n\tAssigns/drops a tcp connection filter to PORT [0...65535] for up to 4 IP-ADDRESS\r\r\n"

#define _USAGE_ADD_MEM \
" add_membership PORT IP-ADDRESS\r\r\n\tAdd multicast send/receive possibilities for multicast address IP-ADDRESS to PORT [0...65535]\r\r\n"

#define _USAGE_DEL_MEM \
" del_membership PORT IP-ADDRESS\r\r\n\tDrop multicast send/receive possibilities for multicast address IP-ADDRESS from PORT [0...65535]\r\r\n"

#define _USAGE_SOCK_RESTART \
" socket_restart\r\r\n\tstops and restarts the socket API\r\r\n"

/*---------------------------------------------------------------------------*/

#define _USAGE_HEADER_STACK \
"\r\r\n *** RTE STACK COMMANDS ***\r\r\n\r\r\n"

#define _USAGE_STACK_STATUS \
" stack_status\r\r\n\tPrints the stack status\r\r\n"

#define _USAGE_UNITCONFIG \
" stack_print_unitconfig\r\r\n\tprints the current unit configuration\r\r\n"

#define _USAGE_INPUT_DATA \
" stack_input_data 0xUNITID 0xSTATUS 0xVALUE *[0xVALUE]\r\r\n\tsets a new value for the input data\r\r\n"

#define _USAGE_OUTPUT_DATA \
" stack_output_data\r\r\n\tPrints the output data\r\r\n"

#define _USAGE_PLUG \
" stack_plug_unit 0xUNITID TYPE INPUTSIZE OUTPUTSIZE - [CFG_DATA]\r\r\n\tadds a unit to the configuration. The Format of CFG_DATA depends on the used protocol\r\r\n" \
" \t- Profinet: 0xIDENTNUMBER 0xSUBMODULIDENTNUMBER\r\r\n" \
" \t- EtherCAT: 0xPDOINDEX FLAGS NUMBERMAPPEDOBJECTS *[0xINDEX SUBINDEX BITLENGTH]\r\r\n" \
" \t- EthernetIP: -\r\r\n" \
" \t- Powerlink: -\r\r\n" \
" \t- ModbusTCP: -\r\r\n"

#define _USAGE_PULL \
" stack_pull_unit 0xUNITID\r\r\n\tremoves a unit from the configuration\r\r\n"

#define _USAGE_DIAG \
" send_diagnosis 0xUNITID TYPE SEVERITY PARAMETER [appear | disappear] \r\r\n\tsends a diagnosis. The use of PARAMETER depends on the used protocol\r\r\n" \
" \t- Profinet: DIAGNOSIS_INDEX\r\r\n" \
" \t- EtherCAT: TEXT_ID\r\r\n" \
" \t- EthernetIP: -\r\r\n" \
" \t- Powerlink: -\r\r\n" \
" \t- ModbusTCP: -\r\r\n"

#define _USAGE_RESTART_STACK \
" stack_restart [BACKEND]\r\r\n\tstops and restarts the fieldbus stack. With the optional parameter BACKEND the used protocol stack can be changed\r\r\n"

/*---------------------------------------------------------------------------*/

#define _USAGE_FIRMWARE \
" firmware SIZE DATA*\r\r\n\tupdates the firmware of the device\r\r\n"

/*---------------------------------------------------------------------------*/

#define COMMAND_SUCCESS                 0       /**< Command was executed without error */
#define COMMAND_ERROR_FORMAT           -1       /**< Command format is incorrect */
#define COMMAND_ERROR_PARAMETER        -2       /**< Command parameter is incorrect */
#define COMMAND_ERROR_RESSOURCE        -3       /**< Command insufficient ressources */
#define COMMAND_ERROR_COMMAND          -4       /**< Command failed for unknown reason */
//@}

/******************************************************************************
TYPEDEFS
******************************************************************************/

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

/** \addtogroup socket_func */
//@{
static void  send_tcp_server_command_result            (int, T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);

static int   scan_tcp_server_command_socket_overview   (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*);
static int   scan_tcp_server_command_socket_status     (char*, int, T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*);
static int   scan_tcp_server_command_open              (char*, int);
static int   scan_tcp_server_command_close             (char*, int);
static int   scan_tcp_server_command_connect           (char*, int);
static int   scan_tcp_server_command_disconnect        (char*, int);
static int   scan_tcp_server_command_udp_send          (char*, int);
static int   scan_tcp_server_command_tcp_send          (char*, int);
static int   scan_tcp_server_command_udp_echo          (char*, int);
static int   scan_tcp_server_command_tcp_echo          (char*, int);
static int   scan_tcp_server_command_filter            (char*, int);
static int   scan_tcp_server_command_add_membership    (char*, int);
static int   scan_tcp_server_command_del_membership    (char*, int);

static int   scan_tcp_server_command_stack_status      (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*);
static int   scan_tcp_server_command_print_unit_data   (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*);
static int   scan_tcp_server_command_get_output_data   (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*);
static int   scan_tcp_server_command_set_input_data    (char*, int);
static int   scan_tcp_server_command_plug_unit         (char*, int);
static int   scan_tcp_server_command_pull_unit         (char*, int);
static int   scan_tcp_server_command_send_diagnosis    (char*, int);
static int   scan_tcp_server_command_stack_restart     (char*, int);

static int   scan_tcp_server_command_firmware          (char*, int, T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*);

static void  memfill                                   (U8*, U16);
static U32   print_help_string                         (void);
//@}

/******************************************************************************
PUBLIC DATA
******************************************************************************/

extern T_SOCKET_ELEMENT             Socket [MAX_NUMBER_SUPPORTED_SOCKETS];

/******************************************************************************
PRIVATE DATA
******************************************************************************/

static U8                           SocketTxDataBuffer [2 * SOCKET_FIFO_SIZE]; /**< Tx buffer for sending command results */

static T_CONFIG_DATA_APPL*          pFlashConfigData = &ApplManagement.ConfigDataAppl;

/******************************************************************************
FUNCTION IMPLEMENTATION
******************************************************************************/

/**
 * The function send_tcp_server_command_result() send the execution result of the requested operation.
 *
 * @return
 * - type  : void
 * @param[in] Result
 * - type : int
 * - range: whole range is valid
 * @param[in] pSocketEl
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pUsage
 * - type : char*
 * - range: whole address range is valid
 */
static void send_tcp_server_command_result (int Result, T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pUsage)
{
  U32   DataLength = 0;

  if (Result == COMMAND_SUCCESS)
  {
    /* DataLength = sprintf ((char*) &SocketTxDataBuffer[0], "successful\r\r\n"); */
  }
  else
  {
    switch (Result)
    {
      case COMMAND_ERROR_FORMAT:    DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "ERROR: Invalid command format\r\r\n"); break;
      case COMMAND_ERROR_PARAMETER: DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "ERROR: Invalid parameter\r\r\n");      break;
      case COMMAND_ERROR_RESSOURCE: DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "ERROR: Ressource error\r\r\n");        break;
      case COMMAND_ERROR_COMMAND:   DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "ERROR: Command error\r\r\n");          break;
      default:                      DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "ERROR: %d\r\r\n", Result);             break;
    }

    if (pUsage != NULL)
    {
      DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Usage:%s\r\r\n", pUsage);
    }
  }

  socket_send (pSocketEl, pRemoteAddress, DataLength, &SocketTxDataBuffer[0]);

  return;
}

/*===========================================================================*/

/**
 * The function handle_tcp_console_connect() is called when a new client connects to
 * the console server port. It send the console information back to the client.
 *
 * @return
 * - type  : void
 * @param[in] pSocketEl
 * - type : T_SOCKET_ELEMENT*
 * - range: the whole address range is valid
 * @param[in] pRemoteAddress
 * - type : T_SDAI_SOCK_ADDR*
 * - range: the whole address range is valid
 */
void handle_tcp_console_connect (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress)
{
  U8*   pIpAddress;

  U32   DataLength;

  pIpAddress = demo_get_ip_address ();
  DataLength = snprintf ((char*) &SocketTxDataBuffer[0], sizeof(SocketTxDataBuffer), "%s[%u.%u.%u.%u]: ", _CONSOLE_HEADER, pIpAddress [0], pIpAddress [1],
                                                                                                                           pIpAddress [2], pIpAddress [3]);
  socket_send (pSocketEl, pRemoteAddress, DataLength, &SocketTxDataBuffer[0]);

  return;
}

/*===========================================================================*/

/**
 * The function handle_tcp_console_command() scans the input from the command socket and calls a function to execute the command.
 *
 * @return
 * - type  : void
 * @param[in] pData
 * - type : char*
 * - range: the whole address range is valid
 * @param[in] RemainingLength
 * - type : int
 * - range: whole range is valid
 * @param[in] pSocketEl
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 */
void handle_tcp_console_command (char* pData, int RemainingLength, T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress)
{
  char* pSubString;
  U32   DataLength;
  int   Result;

  pData[RemainingLength] = '\0';

  if ( (RemainingLength > (int) strlen ("socket_overview")) && ((pSubString = strstr (pData, "socket_overview")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("socket_overview");
    Result           = scan_tcp_server_command_socket_overview (pSocketEl, pRemoteAddress);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_SOCKET_OVERVIEW);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("socket_status")) && ((pSubString = strstr (pData, "socket_status")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("socket_status");
    Result           = scan_tcp_server_command_socket_status (pSubString, RemainingLength, pSocketEl, pRemoteAddress);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_SOCKET_STATUS);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("open")) && ((pSubString = strstr (pData, "open")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("open");
    Result           = scan_tcp_server_command_open (pSubString, RemainingLength);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_OPEN);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("close")) && ((pSubString = strstr (pData, "close")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("close");
    Result           = scan_tcp_server_command_close (pSubString, RemainingLength);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_CLOSE);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("connect")) && ((pSubString = strstr (pData, "connect")) != NULL) && !strstr(pData, "disconnect"))
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("connect");
    Result           = scan_tcp_server_command_connect (pSubString, RemainingLength);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_CONNECT);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("disconnect")) && ((pSubString = strstr (pData, "disconnect")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("disconnect");
    Result           = scan_tcp_server_command_disconnect (pSubString, RemainingLength);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_DISCONNECT);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("send udp")) && ((pSubString = strstr (pData, "send udp")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("send udp");
    Result           = scan_tcp_server_command_udp_send (pSubString, RemainingLength);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_SEND);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("send tcp")) && ((pSubString = strstr (pData, "send tcp")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("send tcp");
    Result           = scan_tcp_server_command_tcp_send (pSubString, RemainingLength);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_SEND);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("echo udp")) && ((pSubString = strstr (pData, "echo udp")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("echo udp");
    Result           = scan_tcp_server_command_udp_echo (pSubString, RemainingLength);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_ECHO);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("echo tcp")) && ((pSubString = strstr (pData, "echo tcp")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("echo tcp");
    Result           = scan_tcp_server_command_tcp_echo (pSubString, RemainingLength);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_ECHO);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("filter")) && ((pSubString = strstr (pData, "filter")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("filter");
    Result           = scan_tcp_server_command_filter (pSubString, RemainingLength);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_FILTER);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("add_membership")) && ((pSubString = strstr (pData, "add_membership")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("add_membership");
    Result           = scan_tcp_server_command_add_membership (pSubString, RemainingLength);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_ADD_MEM);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("del_membership")) && ((pSubString = strstr (pData, "del_membership")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("del_membership");
    Result           = scan_tcp_server_command_del_membership (pSubString, RemainingLength);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_DEL_MEM);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("socket_restart")) && ((pSubString = strstr (pData, "socket_restart")) != NULL) )
  {
    DataLength = snprintf ((char*) &SocketTxDataBuffer[0], sizeof(SocketTxDataBuffer), "%s", "Please wait the socket console will be restarted!\r\n");
    socket_send (pSocketEl, pRemoteAddress, DataLength, &SocketTxDataBuffer[0]);

    platform_send_event (EVENT_RESTART_NETWORK);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("stack_status")) && ((pSubString = strstr (pData, "stack_status")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("stack_status");
    Result           = scan_tcp_server_command_stack_status (pSocketEl, pRemoteAddress);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_STACK_STATUS);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("stack_print_unitconfig")) && ((pSubString = strstr (pData, "stack_print_unitconfig")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("stack_print_unitconfig");
    Result           = scan_tcp_server_command_print_unit_data (pSocketEl, pRemoteAddress);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_UNITCONFIG);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("stack_input_data")) && ((pSubString = strstr (pData, "stack_input_data")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("stack_input_data");
    Result           = scan_tcp_server_command_set_input_data (pSubString, RemainingLength);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_INPUT_DATA);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("stack_output_data")) && ((pSubString = strstr (pData, "stack_output_data")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("stack_output_data");
    Result           = scan_tcp_server_command_get_output_data (pSocketEl, pRemoteAddress);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_OUTPUT_DATA);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("stack_plug_unit")) && ((pSubString = strstr (pData, "stack_plug_unit")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("stack_plug_unit");
    Result           = scan_tcp_server_command_plug_unit (pSubString, RemainingLength);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_PLUG);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("stack_pull_unit")) && ((pSubString = strstr (pData, "stack_pull_unit")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("stack_pull_unit");
    Result           = scan_tcp_server_command_pull_unit (pSubString, RemainingLength);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_PULL);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("send_diagnosis")) && ((pSubString = strstr (pData, "send_diagnosis")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    pSubString      += strlen ("send_diagnosis");
    Result           = scan_tcp_server_command_send_diagnosis (pSubString, RemainingLength);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_DIAG);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("stack_restart")) && ((pSubString = strstr (pData, "stack_restart")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    RemainingLength -= (int) strlen ("stack_restart");
    pSubString      += strlen ("stack_restart");
    Result           = scan_tcp_server_command_stack_restart (pSubString, RemainingLength);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_RESTART_STACK);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("firmware")) && ((pSubString = strstr (pData, "firmware")) != NULL) )
  {
    RemainingLength -= (int) (pSubString - pData);
    RemainingLength -= (int) strlen ("firmware");
    pSubString      += strlen ("firmware");
    Result           = scan_tcp_server_command_firmware (pSubString, RemainingLength, pSocketEl, pRemoteAddress);

    send_tcp_server_command_result (Result, pSocketEl, pRemoteAddress, _USAGE_FIRMWARE);

    return;
  }

  /*-------------------------------------------------------------------------*/

  if ( (RemainingLength > (int) strlen ("help")) && ((pSubString = strstr (pData, "help")) != NULL) )
  {
    DataLength = print_help_string ();
    socket_send (pSocketEl, pRemoteAddress, DataLength, &SocketTxDataBuffer[0]);

    return;
  }

  /* unknown command */
  DataLength = snprintf ((char*) &SocketTxDataBuffer[0], sizeof(SocketTxDataBuffer), "unknown command, type \"help\" for command overview\r\r\n");
  socket_send (pSocketEl, pRemoteAddress, DataLength, &SocketTxDataBuffer[0]);

  return;
}

/*===========================================================================*/

/**
 * The function tcp_console_prompt() send the console prompt.
 *
 * @return
 * - type  : void
 * @param[in] pSocketEl
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 */
void tcp_console_prompt (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress)
{
  U8*   pIpAddress;

  U32   DataLength;

  pIpAddress = demo_get_ip_address ();
  DataLength = snprintf ((char*) &SocketTxDataBuffer[0], sizeof(SocketTxDataBuffer), "[%u.%u.%u.%u]: ", pIpAddress [0], pIpAddress [1],
                                                                                                        pIpAddress [2], pIpAddress [3]);
  socket_send (pSocketEl, pRemoteAddress, DataLength, &SocketTxDataBuffer[0]);

  return;
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_socket_overview() sends an overview of all sockets.
 *
 * @return
 * - type  : int
 * @param[in] pSocketEl
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 */
static int scan_tcp_server_command_socket_overview (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress)
{
  U32                      DataLength = 0;
  unsigned                 SocketNumber;


  for (SocketNumber = 0; SocketNumber < MAX_NUMBER_SUPPORTED_SOCKETS; SocketNumber++)
  {
    DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Nr: %2d Type: ", SocketNumber);

    if (! (Socket [SocketNumber].Flags & SOCK_USER_FLAG_FREE))
    {
      if (Socket [SocketNumber].Flags & SOCK_FLAG_TYPE_TCP)
      {
        DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "TCP %s\r\n", (Socket [SocketNumber].Flags & SOCK_USER_FLAG_SERVER) ? "Server" : "Client");
      }
      else
      {
        DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "UDP\r\n");
      }
    }
    else
    {
      DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Free Socket\r\n");
    }
  }

  socket_send (pSocketEl, pRemoteAddress, DataLength, &SocketTxDataBuffer[0]);

  return (COMMAND_SUCCESS);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_socket_status() sends the status for the specified socket over
 * the requesting socket.
 *
 * @return
 * - type  : int
 * @param[in] pSubString
 * - type : char*
 * - range: the whole address range is valid
 * @param[in] RemainingLength
 * - type : int
 * - range: whole range is valid
 * @param[in] pSocketEl
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 */
static int scan_tcp_server_command_socket_status (char* pSubString, int RemainingLength, T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress)
{
  U32                                 DataLength = 0;
  T_SDAI_SOCK_IOC_GET_LOCAL_STATUS*   pStatus;
  unsigned                            Connection;
  unsigned                            SocketNumber;


  /* remove leading blanks */
  while ( (*pSubString == ' ') && (RemainingLength-- > 0) ) { pSubString += 1; }

  if (sscanf (pSubString, "%u", &SocketNumber) == 1)
  {
    if (MAX_NUMBER_SUPPORTED_SOCKETS <= SocketNumber)
    {
      return (COMMAND_ERROR_PARAMETER);
    }

    platform_lock ();

    pStatus = &Socket [SocketNumber].SocketStatus.UseAs.GetLocalStatus;

    if (! (Socket [SocketNumber].Flags & SOCK_USER_FLAG_FREE))
    {
      if (Socket [SocketNumber].Flags & SOCK_FLAG_TYPE_TCP)
      {
        DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Type: TCP %s\r\n", (Socket [SocketNumber].Flags & SOCK_USER_FLAG_SERVER) ? "Server" : "Client");
      }
      else
      {
        DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Type: UDP\r\n");
      }

      DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Local IP address: %03d.%03d.%03d.%03d\r\n", ((U8) ((U8*) &Socket [SocketNumber].LocalAddress.IpAddress) [0]),
                                                                                                                 ((U8) ((U8*) &Socket [SocketNumber].LocalAddress.IpAddress) [1]),
                                                                                                                 ((U8) ((U8*) &Socket [SocketNumber].LocalAddress.IpAddress) [2]),
                                                                                                                 ((U8) ((U8*) &Socket [SocketNumber].LocalAddress.IpAddress) [3]));

      DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Local Port: %d\r\n", sdai_sock_ntohs (Socket [SocketNumber].LocalAddress.Port));

      DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Remote IP address: %03d.%03d.%03d.%03d\r\n", ((U8) ((U8*) &Socket [SocketNumber].RemoteAddress.IpAddress) [0]),
                                                                                                                  ((U8) ((U8*) &Socket [SocketNumber].RemoteAddress.IpAddress) [1]),
                                                                                                                  ((U8) ((U8*) &Socket [SocketNumber].RemoteAddress.IpAddress) [2]),
                                                                                                                  ((U8) ((U8*) &Socket [SocketNumber].RemoteAddress.IpAddress) [3]));

      DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Remote Port: %d\r\n", sdai_sock_ntohs (Socket [SocketNumber].RemoteAddress.Port));

      switch (pStatus->Status)
      {
        case SOCK_LOCAL_STATUS_CLOSED : DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Status: Closed\r\n");      break;
        case SOCK_LOCAL_STATUS_OFFLINE: DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Status: Offline\r\n");     break;
        case SOCK_LOCAL_STATUS_ONLINE : DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Status: Online\r\n");      break;
        default: DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Status: Unknown (0x%02lX)\r\n", pStatus->Status); break;
      }

      switch (pStatus->StatusCode)
      {
        case SOCK_LOCAL_STATUS_CODE_NO_ERROR      : DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "StatusCode: No Error\r\n");               break;
        case SOCK_LOCAL_STATUS_CODE_ADDR_IN_USE   : DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "StatusCode: Address already in use\r\n"); break;
        case SOCK_LOCAL_STATUS_CODE_INTERNAL_ERROR: DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "StatusCode: Internal Error\r\n");         break;
        default: DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "StatusCode: Unknown (0x%02lX)\r\n", pStatus->Status);                        break;
      }

      if (pStatus->UseAs.Tcp.NumberConnections != 0)
      {
        for (Connection = 0; Connection < pStatus->UseAs.Tcp.NumberConnections; Connection++)
        {
          T_SDAI_SOCK_TCP_CONNECTION_LOCAL_STATUS*    pConnection = &pStatus->UseAs.Tcp.Connection [Connection];

          DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "  Connection: %2d\r\n", Connection);

          DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "  Remote IP address: %03d.%03d.%03d.%03d\r\n", ((U8) ((U8*) &pConnection->RemoteAddr.IpAddress) [0]),
                                                                                                                        ((U8) ((U8*) &pConnection->RemoteAddr.IpAddress) [1]),
                                                                                                                        ((U8) ((U8*) &pConnection->RemoteAddr.IpAddress) [2]),
                                                                                                                        ((U8) ((U8*) &pConnection->RemoteAddr.IpAddress) [3]));

          DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "  Remote Port: %d\r\n", sdai_sock_ntohs (pConnection->RemoteAddr.Port));

          switch (pConnection->Status)
          {
            case SOCK_TCP_CONNECTION_STATUS_UNCONNECTED: DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "  Status: Unconnected\r\n"); break;
            case SOCK_TCP_CONNECTION_STATUS_CONNECTING : DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "  Status: Connecting\r\n");  break;
            case SOCK_TCP_CONNECTION_STATUS_CONNECTED  : DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "  Status: Connected\r\n");   break;
            default: DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "  Status: Unknown (0x%02lX)\r\n", pConnection->Status);          break;
          }

          switch (pConnection->StatusCode)
          {
            case SOCK_TCP_CONNECTION_STATUS_CODE_NO_ERROR        : DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "  StatusCode: No Error\r\n");               break;
            case SOCK_TCP_CONNECTION_STATUS_CODE_INTERNAL_ERROR  : DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "  StatusCode: Address already in use\r\n"); break;
            case SOCK_TCP_CONNECTION_STATUS_CODE_CLOSED_REMOTELY : DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "  StatusCode: Closed remotely\r\n");        break;
            case SOCK_TCP_CONNECTION_STATUS_CODE_ABORTED_REMOTELY: DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "  StatusCode: Aborted remotely\r\n");       break;
            case SOCK_TCP_CONNECTION_STATUS_CODE_TIMEOUT         : DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "  StatusCode: Connection timeout\r\n");     break;
            case SOCK_TCP_CONNECTION_STATUS_CODE_CONNECT_REJECTED: DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "  StatusCode: Connection rejected\r\n");    break;
            default: DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "  StatusCode: Unknown (0x%02lX)\r\n", pConnection->Status);                               break;
          }
        }
      }
    }
    else
    {
      DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Free Socket\r\n");
    }

    platform_unlock ();

    socket_send (pSocketEl, pRemoteAddress, DataLength, &SocketTxDataBuffer[0]);

    return (COMMAND_SUCCESS);
  }

  return (COMMAND_ERROR_FORMAT);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_open() opens a UDP socket or a TCP server socket.
 *
 * @return
 * - type  : int
 * @param[in] pSubString
 * - type : char*
 * - range: whole address range is valid
 * @param[in] RemainingLength
 * - type : int
 * - range: whole range is valid
 */
static int scan_tcp_server_command_open (char* pSubString, int RemainingLength)
{
  char* pData = pSubString;


  pSubString = (RemainingLength > (int) strlen ("tcp")) ? strstr (pData, "tcp") : NULL;

  if (pSubString != NULL)
  {
    S32   Port;


    pSubString      += strlen ("tcp");
    RemainingLength -= (int) (pSubString - pData);
    pData            = pSubString;

    /* remove leading blanks */
    while ( (*pData == ' ') && (RemainingLength-- > 0) ) { pData += 1; }

    if (sscanf (pData, "%ld", &Port) == 1)
    {
      T_SDAI_SOCK_ADDR   RemoteAddress;
      T_SDAI_SOCK_ADDR   LocalAddress;
      U32                Flags = (SOCK_FLAG_TYPE_TCP | SOCK_USER_FLAG_SERVER);


      if ((Port < 0) || (Port > 0xFFFF))
      {
        return (COMMAND_ERROR_PARAMETER);
      }

      if (RemainingLength >= (int) strlen ("keepalive"))
      {
        pSubString = strstr (pData, "keepalive");
        if (pSubString != NULL)
        {
          Flags |= SOCK_FLAG_ENABLE_KEEPALIVE;
        }
      }

      /* create TCP server socket*/
      RemoteAddress.IpAddress = SOCK_IP_ADDR_ANY;
      RemoteAddress.Port      = SOCK_PORT_ANY;

      LocalAddress.IpAddress  = SOCK_IP_ADDR_ANY;
      LocalAddress.Port       = sdai_sock_htons ((U16)Port);

      if (socket_open (Flags, &RemoteAddress, &LocalAddress) == NULL)
      {
        return (COMMAND_ERROR_COMMAND);
      }

      return (COMMAND_SUCCESS);
    }
  }

  /*-----------------------------------------------------------------------*/

  pSubString = (RemainingLength > (int) strlen ("udp")) ? strstr (pData, "udp") : NULL;

  if (pSubString != NULL)
  {
    S32    Port;

    pSubString      += strlen ("udp");
    RemainingLength -= (int) (pSubString - pData);
    pData            = pSubString;

    /* remove leading blanks */
    while ( (*pData == ' ') && (RemainingLength-- > 0) ) { pData += 1; }

    if (sscanf (pData, "%ld", &Port) == 1)
    {
      T_SDAI_SOCK_ADDR   RemoteAddress;
      T_SDAI_SOCK_ADDR   LocalAddress;
      U32                Flags = SOCK_FLAG_TYPE_UDP;


      if ((Port < 0) || (Port > 0xFFFF))
      {
        return (COMMAND_ERROR_PARAMETER);
      }

      /* remove leading blanks */
      while ( (*pData == ' ') && (RemainingLength-- > 0) ) { pData += 1; }

      if (RemainingLength >= (int) strlen ("multicast"))
      {
        pSubString = strstr (pData, "broadcast");
        if (pSubString != NULL)
        {
          Flags |= SOCK_FLAG_ENABLE_BROADCAST;
        }

        pSubString = strstr (pData, "multicast");
        if (pSubString != NULL)
        {
          Flags |= SOCK_FLAG_ENABLE_MULTICAST;
        }
      }

      /* create UDP socket*/
      RemoteAddress.IpAddress = SOCK_IP_ADDR_ANY;
      RemoteAddress.Port      = SOCK_PORT_ANY;

      LocalAddress.IpAddress  = SOCK_IP_ADDR_ANY;
      LocalAddress.Port       = sdai_sock_htons ((U16)Port);

      if (socket_open (Flags, &RemoteAddress, &LocalAddress) == NULL)
      {
        return (COMMAND_ERROR_COMMAND);
      }

      return (COMMAND_SUCCESS);
    }
  }

  return (COMMAND_ERROR_FORMAT);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_close() closes a TCP server socket or a UDP socket.
 *
 * @return
 * - type  : int
 * @param[in] pSubString
 * - type : char*
 * - range: whole address range is valid
 * @param[in] RemainingLength
 * - type : int
 * - range: whole range is valid
 */
static int scan_tcp_server_command_close (char* pSubString, int RemainingLength)
{
  char*             pData = pSubString;
  T_SOCKET_ELEMENT* pSocketEl;


  /* remove leading blanks */
  while ( (*pData == ' ') && (RemainingLength-- > 0) ) { pData += 1; }

  pSubString = (RemainingLength > (int) strlen ("tcp")) ? strstr (pData, "tcp") : NULL;

  if (pSubString != NULL)
  {
    U16   LocalPort;


    pSubString      += strlen ("tcp");
    RemainingLength -= (int) (pSubString - pData);
    pData            = pSubString;

    /* remove leading blanks */
    while ( (*pData == ' ') && (RemainingLength-- > 0) ) { pData += 1; }

    if (sscanf (pData, "%hu", &LocalPort) == 1)
    {
      pSocketEl = socket_find_socket (SOCK_FIND_TCP_SOCKETS, sdai_sock_htons (LocalPort), SOCK_PORT_ANY, SOCK_IP_ADDR_ANY);

      if (pSocketEl != NULL)
      {
        socket_close(pSocketEl);
      }
      else
      {
        return (COMMAND_ERROR_PARAMETER);
      }
    }
    else
    {
      return (COMMAND_ERROR_FORMAT);
    }

    return (COMMAND_SUCCESS);
  }

  /*-----------------------------------------------------------------------*/

  pSubString = (RemainingLength > (int) strlen ("udp")) ? strstr (pData, "udp") : NULL;

  if (pSubString != NULL)
  {
    U16   LocalPort;


    pSubString      += strlen ("udp");
    RemainingLength -= (int) (pSubString - pData);
    pData            = pSubString;

    /* remove leading blanks */
    while ( (*pData == ' ') && (RemainingLength-- > 0) ) { pData += 1; }

    if (sscanf (pData, "%hu", &LocalPort) == 1)
    {
      pSocketEl = socket_find_socket (SOCK_FIND_UDP_SOCKETS, sdai_sock_htons (LocalPort), SOCK_PORT_ANY, SOCK_IP_ADDR_ANY);

      if (pSocketEl != NULL)
      {
        socket_close(pSocketEl);
      }
      else
      {
        return (COMMAND_ERROR_PARAMETER);
      }
    }
    else
    {
      return (COMMAND_ERROR_FORMAT);
    }
  }

  return (COMMAND_SUCCESS);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_connect() connects to a remote TCP server.
 *
 * @return
 * - type  : int
 * @param[in] pSubString
 * - type : char*
 * - range: whole address range is valid
 * @param[in] RemainingLength
 * - type : int
 * - range: whole range is valid
 */
static int scan_tcp_server_command_connect (char* pSubString, int RemainingLength)
{
  U16   LocalPort;
  int   HighWordHighByte;
  int   HighWordLowByte;
  int   LowWordHighByte;
  int   LowWordLowByte;


  /* remove leading blanks */
  while ( (*pSubString == ' ') && (RemainingLength-- > 0) ) { pSubString += 1; }

  if (sscanf (pSubString, "%3d.%3d.%3d.%3d:%hu", &HighWordHighByte, &HighWordLowByte, &LowWordHighByte, &LowWordLowByte, &LocalPort) == 5)
  {
    U32                Address;
    T_SDAI_SOCK_ADDR   RemoteAddress;
    T_SDAI_SOCK_ADDR   LocalAddress;
    U32                Flags = SOCK_FLAG_TYPE_TCP;

    Address = (((U8) HighWordHighByte << 24) + ((U8) HighWordLowByte << 16) + ((U8) LowWordHighByte << 8) + (U8) LowWordLowByte);

    while ( (*pSubString == ' ') && (RemainingLength-- > 0) ) { pSubString += 1; }

    if (RemainingLength >= (int) strlen ("keepalive"))
    {
      pSubString = strstr (pSubString, "keepalive");
      if (pSubString != NULL)
      {
        Flags |= SOCK_FLAG_ENABLE_KEEPALIVE;
      }
    }

    /* create TCP client socket*/
    RemoteAddress.IpAddress = sdai_sock_htonl (Address);
    RemoteAddress.Port      = sdai_sock_htons (LocalPort);

    LocalAddress.IpAddress  = SOCK_IP_ADDR_ANY;
    LocalAddress.Port       = SOCK_PORT_ANY;

    if (socket_open (Flags, &RemoteAddress, &LocalAddress) == NULL)
    {
      return (COMMAND_ERROR_COMMAND);
    }
  }
  else
  {
    return (COMMAND_ERROR_FORMAT);
  }

  return (COMMAND_SUCCESS);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_disconnect() closes a TCP client connection or
 * a connection to a TCP server.
 *
 * @return
 * - type  : int
 * @param[in] pSubString
 * - type : char*
 * - range: whole address range is valid
 * @param[in] RemainingLength
 * - type : int
 * - range: whole range is valid
 */
static int scan_tcp_server_command_disconnect (char* pSubString, int RemainingLength)
{
  U16                 RemotePort;
  U16                 SocketNumber;
  int                 HighWordHighByte;
  int                 HighWordLowByte;
  int                 LowWordHighByte;
  int                 LowWordLowByte;


  /* remove leading blanks */
  while ( (*pSubString == ' ') && (RemainingLength-- > 0) ) { pSubString += 1; }

  if (sscanf (pSubString, "%3d.%3d.%3d.%3d:%hu", &HighWordHighByte, &HighWordLowByte, &LowWordHighByte, &LowWordLowByte, &RemotePort) == 5)
  {
    T_SDAI_SOCK_TCP_CONNECTION_LOCAL_STATUS ConnectionStatus;
    T_SOCKET_ELEMENT*                       pSocketEl;
    U32                                     Address;

    Address = (((U8) HighWordHighByte << 24) + ((U8) HighWordLowByte << 16) + ((U8) LowWordHighByte << 8) + (U8) LowWordLowByte);

    ConnectionStatus.RemoteAddr.IpAddress = sdai_sock_htonl (Address);
    ConnectionStatus.RemoteAddr.Port      = sdai_sock_htons (RemotePort);

    while ( (*pSubString != ' ') && (RemainingLength-- > 0) ) { pSubString += 1; }
    pSubString += strlen (" ");

    if (sscanf (pSubString, "%hu", &SocketNumber) != 1)
    {
      pSocketEl = socket_find_socket (SOCK_FIND_TCP_SOCKETS, SOCK_PORT_ANY, sdai_sock_htons (RemotePort), sdai_sock_htonl (Address));
    }
    else
    {
      if (SocketNumber < MAX_NUMBER_SUPPORTED_SOCKETS)
      {
        pSocketEl = &Socket [SocketNumber];
      }
      else
      {
        return (COMMAND_ERROR_PARAMETER);
      }
    }

    if (pSocketEl != NULL)
    {
      socket_close_connection (pSocketEl->SocketNumber, &ConnectionStatus);

      /* close the socket if the tcp connection to the remote server has been closed */
      if ((pSocketEl->Flags & (SOCK_FLAG_TYPE_TCP | SOCK_USER_FLAG_SERVER)) == SOCK_FLAG_TYPE_TCP)
      {
        socket_close (pSocketEl);
      }
    }
    else
    {
      return (COMMAND_ERROR_PARAMETER);
    }
  }
  else
  {
    return (COMMAND_ERROR_FORMAT);
  }

  return (COMMAND_SUCCESS);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_udp_send() sends a UDP packet to the
 * specified UDP client.
 *
 * @return
 * - type  : int
 * @param[in] pSubString
 * - type : char*
 * - range: whole address range is valid
 * @param[in] RemainingLength
 * - type : int
 * - range: whole range is valid
 */
static int scan_tcp_server_command_udp_send (char* pSubString, int RemainingLength)
{
  U16   RemotePort;
  U16   LocalPort;
  U16   DataLength;
  int   HighWordHighByte;
  int   HighWordLowByte;
  int   LowWordHighByte;
  int   LowWordLowByte;


  /* remove leading blanks */
  while ( (*pSubString == ' ') && (RemainingLength-- > 0) ) { pSubString += 1; }

  if (sscanf (pSubString, "%3d.%3d.%3d.%3d:%hu %hu %hu", &HighWordHighByte, &HighWordLowByte, &LowWordHighByte, &LowWordLowByte, &RemotePort, &LocalPort, &DataLength) == 7)
  {
    T_SOCKET_ELEMENT*   pSocketEl;
    U32                 Address;

    Address = (((U8) HighWordHighByte << 24) + ((U8) HighWordLowByte << 16) + ((U8) LowWordHighByte << 8) + (U8) LowWordLowByte);

    pSocketEl = socket_find_socket (SOCK_FIND_UDP_SOCKETS, sdai_sock_htons (LocalPort), SOCK_PORT_ANY, SOCK_IP_ADDR_ANY);

    if (pSocketEl != NULL)
    {
      T_SDAI_SOCK_ADDR   RemoteAddress;

      RemoteAddress.IpAddress = sdai_sock_htonl (Address);
      RemoteAddress.Port      = sdai_sock_htons (RemotePort);

      memfill (&SocketTxDataBuffer[0], DataLength);

      socket_send (pSocketEl, &RemoteAddress, DataLength, &SocketTxDataBuffer[0]);
    }
    else
    {
      return (COMMAND_ERROR_PARAMETER);
    }
  }
  else
  {
    return (COMMAND_ERROR_FORMAT);
  }

  return (COMMAND_SUCCESS);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_tcp_send() sends a packet over a TCP server/client socket.
 *
 * @return
 * - type  : int
 * @param[in] pSubString
 * - type : char*
 * - range: whole address range is valid
 * @param[in] RemainingLength
 * - type : int
 * - range: whole range is valid
 */
static int scan_tcp_server_command_tcp_send (char* pSubString, int RemainingLength)
{
  U16   RemotePort;
  U16   DataLength;
  int   HighWordHighByte;
  int   HighWordLowByte;
  int   LowWordHighByte;
  int   LowWordLowByte;


  /* remove leading blanks */
  while ( (*pSubString == ' ') && (RemainingLength-- > 0) ) { pSubString += 1; }

  if (sscanf (pSubString, "%3d.%3d.%3d.%3d:%hu %hu", &HighWordHighByte, &HighWordLowByte, &LowWordHighByte, &LowWordLowByte, &RemotePort, &DataLength) == 6)
  {
    T_SOCKET_ELEMENT*   pSocketEl;
    U32                 Address;

    Address = (((U8) HighWordHighByte << 24) + ((U8) HighWordLowByte << 16) + ((U8) LowWordHighByte << 8) + (U8) LowWordLowByte);

    pSocketEl = socket_find_socket (SOCK_FIND_TCP_SERVER_SOCKETS, SOCK_PORT_ANY, sdai_sock_htons (RemotePort), sdai_sock_htonl (Address));

    if (pSocketEl == NULL)
    {
      pSocketEl = socket_find_socket (SOCK_FIND_TCP_CLIENT_SOCKETS, SOCK_PORT_ANY, sdai_sock_htons (RemotePort), sdai_sock_htonl (Address));
    }

    if (pSocketEl != NULL)
    {
      T_SDAI_SOCK_ADDR   RemoteAddress;

      RemoteAddress.IpAddress = sdai_sock_htonl (Address);
      RemoteAddress.Port      = sdai_sock_htons (RemotePort);

      memfill (&SocketTxDataBuffer[0], DataLength);

      socket_send (pSocketEl, &RemoteAddress, DataLength, &SocketTxDataBuffer[0]);
    }
    else
    {
      return (COMMAND_ERROR_PARAMETER);
    }
  }
  else
  {
    return (COMMAND_ERROR_FORMAT);
  }

  return (COMMAND_SUCCESS);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_udp_echo() enables the echo function on
 * the specified UDP port.
 *
 * @return
 * - type  : int
 * @param[in] pSubString
 * - type : char*
 * - range: whole address range is valid
 * @param[in] RemainingLength
 * - type : int
 * - range: whole range is valid
 */
static int scan_tcp_server_command_udp_echo (char* pSubString, int RemainingLength)
{
  U16   Port;


  /* remove leading blanks */
  while ( (*pSubString == ' ') && (RemainingLength-- > 0) ) { pSubString += 1; }

  if (sscanf (pSubString, "%hu", &Port) == 1)
  {
    T_SOCKET_ELEMENT* pSocketEl;


    pSocketEl = socket_find_socket (SOCK_FIND_UDP_SOCKETS, sdai_sock_htons (Port), SOCK_PORT_ANY, SOCK_IP_ADDR_ANY);

    if (pSocketEl != NULL)
    {
      if (pSocketEl->Flags & SOCK_USER_FLAG_ECHO) {pSocketEl->Flags &= ~SOCK_USER_FLAG_ECHO;}
      else                                        {pSocketEl->Flags |= SOCK_USER_FLAG_ECHO;}
    }
    else
    {
      return (COMMAND_ERROR_PARAMETER);
    }
  }
  else
  {
    return (COMMAND_ERROR_FORMAT);
  }

  return (COMMAND_SUCCESS);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_tcp_echo() enables the echo function on
 * the specified TCP port.
 *
 * @return
 * - type  : int
 * @param[in] pSubString
 * - type : char*
 * - range: whole address range is valid
 * @param[in] RemainingLength
 * - type : int
 * - range: whole range is valid
 */
static int scan_tcp_server_command_tcp_echo (char* pSubString, int RemainingLength)
{
  U16   LocalPort;


  /* remove leading blanks */
  while ( (*pSubString == ' ') && (RemainingLength-- > 0) ) { pSubString += 1; }

  if (sscanf (pSubString, "%hu", &LocalPort) == 1)
  {
    T_SOCKET_ELEMENT* pSocketEl;


    pSocketEl = socket_find_socket (SOCK_FIND_TCP_SERVER_SOCKETS, sdai_sock_htons (LocalPort), SOCK_PORT_ANY, SOCK_IP_ADDR_ANY);

    if (pSocketEl == NULL)
    {
      pSocketEl = socket_find_socket (SOCK_FIND_TCP_CLIENT_SOCKETS, SOCK_PORT_ANY, sdai_sock_htons (LocalPort), SOCK_IP_ADDR_ANY);
    }

    if (pSocketEl != NULL)
    {
      if (pSocketEl->Flags & SOCK_USER_FLAG_ECHO) {pSocketEl->Flags &= ~SOCK_USER_FLAG_ECHO;}
      else                                        {pSocketEl->Flags |= SOCK_USER_FLAG_ECHO;}
    }
    else
    {
      return (COMMAND_ERROR_PARAMETER);
    }
  }
  else
  {
    return (COMMAND_ERROR_FORMAT);
  }

  return (COMMAND_SUCCESS);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_filter() sets an IP Filter for the
 * specified TCP server port.
 *
 * @return
 * - type  : int
 * @param[in] pSubString
 * - type : char*
 * - range: whole address range is valid
 * @param[in] RemainingLength
 * - type : int
 * - range: whole range is valid
 */
static int scan_tcp_server_command_filter (char* pSubString, int RemainingLength)
{
  char*                    pData = pSubString;
  T_SDAI_SOCK_IO_CONTROL   IoCtlStruct;
  T_SOCKET_ELEMENT*        pSocketEl;
  U16                      LocalPort;


  /* remove leading blanks */
  while ( (*pData == ' ') && (RemainingLength-- > 0) ) { pData += 1; }

  if (sscanf (pData, "%hu", &LocalPort) != 1) {return (COMMAND_ERROR_FORMAT);}

  pSocketEl = socket_find_socket (SOCK_FIND_TCP_SERVER_SOCKETS, sdai_sock_htons (LocalPort), SOCK_PORT_ANY, SOCK_IP_ADDR_ANY);

  if(pSocketEl == NULL) {return (COMMAND_ERROR_PARAMETER);}

  /* remove port */
  while ( (*pData != ' ') && (--RemainingLength > 0) ) { pData += 1; }
  /* remove leading blanks */
  while ( (*pData == ' ') && (RemainingLength-- > 0) ) { pData += 1; }

  IoCtlStruct.UseAs.TcpAcceptFilter.NumberEntries = 0u;

  while (RemainingLength > (int) strlen ("0.0.0.0"))
  {
    int   HighWordHighByte;
    int   HighWordLowByte;
    int   LowWordHighByte;
    int   LowWordLowByte;


    if (sscanf (pData, "%3d.%3d.%3d.%3d", &HighWordHighByte, &HighWordLowByte, &LowWordHighByte, &LowWordLowByte) == 4)
    {
      U32 Address = (((U8) HighWordHighByte << 24) + ((U8) HighWordLowByte << 16) + ((U8) LowWordHighByte << 8) + (U8) LowWordLowByte);

      IoCtlStruct.UseAs.TcpAcceptFilter.Filter [IoCtlStruct.UseAs.TcpAcceptFilter.NumberEntries] = sdai_sock_htonl (Address);
      IoCtlStruct.UseAs.TcpAcceptFilter.NumberEntries                                           += 1u;

      pSubString = (RemainingLength > (int) strlen (":")) ? strstr (pData, ":") : NULL;

      if ( (pSubString != NULL) && (IoCtlStruct.UseAs.TcpAcceptFilter.NumberEntries < SOCK_MAX_NUMBER_FILTER_ENTRIES) )
      {
        pSubString      += strlen (":");
        RemainingLength -= (int) (pSubString - pData);
        pData            = pSubString;

        /* remove leading blanks */
        while ( (*pData == ' ') && (RemainingLength-- > 0) ) { pData += 1; }
      }
      else
      {
        break;
      }
    }
    else
    {
      return (COMMAND_ERROR_FORMAT);
    }
  }

  IoCtlStruct.Command = SOCK_IOC_TCP_ACCEPT_FILTER;
  sdai_sock_ioctl(pSocketEl->SocketNumber, &IoCtlStruct);

  return (COMMAND_SUCCESS);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_add_membership() joins a multicast group for
 * the specified UDP port.
 *
 * @return
 * - type  : int
 * @param[in] pSubString
 * - type : char*
 * - range: whole address range is valid
 * @param[in] RemainingLength
 * - type : int
 * - range: whole range is valid
 */
static int scan_tcp_server_command_add_membership (char* pSubString, int RemainingLength)
{
  char*                    pData = pSubString;
  T_SDAI_SOCK_IO_CONTROL   IoCtlStruct;
  T_SOCKET_ELEMENT*        pSocketEl;
  U32                      IpAddress;

  U16                      LocalPort;
  int                      HighWordHighByte;
  int                      HighWordLowByte;
  int                      LowWordHighByte;
  int                      LowWordLowByte;


  /* remove leading blanks */
  while ( (*pData == ' ') && (RemainingLength-- > 0) ) { pData += 1; }

  if (sscanf (pData, "%hu %3d.%3d.%3d.%3d", &LocalPort, &HighWordHighByte, &HighWordLowByte, &LowWordHighByte, &LowWordLowByte) == 5)
  {

    pSocketEl = socket_find_socket (SOCK_FIND_UDP_SOCKETS, sdai_sock_htons (LocalPort), SOCK_PORT_ANY, SOCK_IP_ADDR_ANY);
    if(pSocketEl == NULL) {return (COMMAND_ERROR_PARAMETER);}

    IpAddress = (((U8) HighWordHighByte << 24) + ((U8) HighWordLowByte << 16) + ((U8) LowWordHighByte << 8) + (U8) LowWordLowByte);
    IoCtlStruct.UseAs.AddMulticast.IpAddress = sdai_sock_htonl(IpAddress);

    IoCtlStruct.Command = SOCK_IOC_UDP_ADD_MULTICAST;
    sdai_sock_ioctl(pSocketEl->SocketNumber, &IoCtlStruct);

    return (COMMAND_SUCCESS);
  }

  return (COMMAND_ERROR_FORMAT);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_del_membership() leaves the multicast membership
 * for the specified UDP port.
 *
 * @return
 * - type  : int
 * @param[in] pSubString
 * - type : char*
 * - range: whole address range is valid
 * @param[in] RemainingLength
 * - type : int
 * - range: whole range is valid
 */
static int scan_tcp_server_command_del_membership (char* pSubString, int RemainingLength)
{
  char*                    pData = pSubString;
  T_SDAI_SOCK_IO_CONTROL   IoCtlStruct;
  T_SOCKET_ELEMENT*        pSocketEl;
  U32                      IpAddress;

  U16                      LocalPort;
  int                      HighWordHighByte;
  int                      HighWordLowByte;
  int                      LowWordHighByte;
  int                      LowWordLowByte;


  /* remove leading blanks */
  while ( (*pData == ' ') && (RemainingLength-- > 0) ) { pData += 1; }

  if (sscanf (pData, "%hu %3d.%3d.%3d.%3d", &LocalPort, &HighWordHighByte, &HighWordLowByte, &LowWordHighByte, &LowWordLowByte) == 5)
  {
    pSocketEl = socket_find_socket (SOCK_FIND_UDP_SOCKETS, sdai_sock_htons (LocalPort), SOCK_PORT_ANY, SOCK_IP_ADDR_ANY);
    if(pSocketEl == NULL) {return (COMMAND_ERROR_PARAMETER);}

    IpAddress = (((U8) HighWordHighByte << 24) + ((U8) HighWordLowByte << 16) + ((U8) LowWordHighByte << 8) + (U8) LowWordLowByte);
    IoCtlStruct.UseAs.AddMulticast.IpAddress = sdai_sock_htonl(IpAddress);

    IoCtlStruct.Command = SOCK_IOC_UDP_DEL_MULTICAST;
    sdai_sock_ioctl(pSocketEl->SocketNumber, &IoCtlStruct);

    return (COMMAND_SUCCESS);
  }

  return (COMMAND_ERROR_FORMAT);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_stack_status() sends the stack status over the command socket.
 *
 * @return
 * - type  : int
 * @param[in] pSocketEl
 * - type : T_SOCKET_ELEMENT*
 * - range: the whole address range is valid
 * @param[in] pRemoteAddress
 * - type : T_SDAI_SOCK_ADDR*
 * - range: the whole address range is valid
 */
static int scan_tcp_server_command_stack_status (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress)
{
  U16   DataLength = 0;
  U8    Index;

  DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "SDAI status: ");

  if (ApplManagement.StackStatus == 0)
  {
    DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "UNUSED\r\n");
  }
  else
  {
    if (ApplManagement.StackStatus & SDAI_STATE_MASK_INIT)                  {DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "INIT | ");}
    if (ApplManagement.StackStatus & SDAI_STATE_MASK_ONLINE)                {DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "ONLINE | ");}
    if (ApplManagement.StackStatus & SDAI_STATE_MASK_CONNECTED)             {DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "CONNECTED | ");}
    if (ApplManagement.StackStatus & SDAI_STATE_MASK_DOUBLE_ADDRESS_ERROR)  {DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "IP ADDRESS CONFLICT | ");}
    if (ApplManagement.StackStatus & SDAI_STATE_MASK_HARDWARE_ERROR)        {DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "HW ERROR | ");}
    if (ApplManagement.StackStatus & SDAI_STATE_MASK_LICENSE_ERROR)         {DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "LICENSE ERROR | ");}
    if (ApplManagement.StackStatus & SDAI_STATE_MASK_APPL_WATCHDOG_EXPIRED) {DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "APPLICATION WATCHDOG EXPIRED | ");}
    if (ApplManagement.StackStatus & SDAI_STATE_MASK_PROTOCOL_ERROR)        {DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "PROTOCOL ERROR | ");}

    if (DataLength != (sizeof ("SDAI Status: ") - 1)) {DataLength -= (sizeof (" | ") - 1);}

    DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "\r\n");
  }

  DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "CPU Load: .1s:%hu%% 1s:%hu%% 10s:%hu%%\r\n", ApplManagement.ProtocolStatusData.PerformanceData.CpuLoad100ms,
                                                                                                              ApplManagement.ProtocolStatusData.PerformanceData.CpuLoad1s,
                                                                                                              ApplManagement.ProtocolStatusData.PerformanceData.CpuLoad10s);

  for (Index = 0; Index < ApplManagement.ProtocolStatusData.InterfaceData.NumberExistingEthernetPorts; Index++)
  {
    DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Interface %u:\r\nLink:\t", Index);

    if (ApplManagement.ProtocolStatusData.InterfaceData.EthPortData [Index].LinkState == SDAI_ADDSTATUS_LINK_UP)
    {
      DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Up\r\nMode:\t");

      switch (ApplManagement.ProtocolStatusData.InterfaceData.EthPortData [Index].ConnectionType)
      {
        case SDAI_ADDSTATUS_CONNECTION_100MBIT_FULLDUPLEX:
          DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "100 Mbps/Full Duplex\r\n"); break;
        case SDAI_ADDSTATUS_CONNECTION_100MBIT_HALFDUPLEX:
          DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "100 Mbps/Half Duplex\r\n"); break;
        case SDAI_ADDSTATUS_CONNECTION_10MBIT_FULLDUPLEX:
          DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "10 Mbps/Full Duplex\r\n"); break;
        case SDAI_ADDSTATUS_CONNECTION_10MBIT_HALFDUPLEX:
          DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "10 Mbps/Half Duplex\r\n"); break;
        default:
          DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "unknown\r\n"); break;
      }

      DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "AN Status:\t");

      switch (ApplManagement.ProtocolStatusData.InterfaceData.EthPortData [Index].NegotiationState)
      {
        case SDAI_ADDSTATUS_AUTO_NEGO_IN_PROGRESS:
          DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Autonegotiation in progress\r\n"); break;
        case SDAI_ADDSTATUS_AUTO_NEGO_FAILED_SPEED_NOT_DETECTED:
          DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Autonegotiation and speed detection failed\r\n"); break;
        case SDAI_ADDSTATUS_AUTO_NEGO_FAILED_SPEED_DETECTED:
          DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Autonegotiation failed but speed detected\r\n"); break;
        case SDAI_ADDSTATUS_AUTO_NEGO_SUCCESSFULL:
          DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Successfully negotiated speed and duplex\r\n"); break;
        default:
          DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Forced speed and duplex\r\n"); break;
      }

      if (ApplManagement.ProtocolStatusData.InterfaceData.EthPortData [Index].CableLength == 0xFFFF)
      {
        DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Cable Length:\t-\r\n");
      }
      else
      {
        DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Cable Length:\t%hu\r\n", ApplManagement.ProtocolStatusData.InterfaceData.EthPortData [Index].CableLength);
      }
    }
    else
    {
      DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Down\r\n");
    }
  }

  switch (ApplManagement.ConfigDataAppl.BackEnd)
  {
    case SDAI_BACKEND_PN:
      DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "PROFINET: ");
      switch (ApplManagement.ProtocolStatusData.UseAs.Pn.ProtocolState)
      {
        case SDAI_PN_STATE_OK:           DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "OK\r\n");               break;
        case SDAI_PN_STATE_NO_PARAMETER: DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "No parameter\r\n");     break;
        case SDAI_PN_STATE_ETH_FAILURE:  DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Ethernet failure\r\n"); break;
        case SDAI_PN_STATE_UDP_FAILURE:  DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "UDP failure\r\n");      break;
        case SDAI_PN_STATE_FAILURE:      DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "General failure\r\n");  break;
      }
      break;

    default:
      DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Unknown protocol\r\n");
      break;
  }

  socket_send (pSocketEl, pRemoteAddress, DataLength, &SocketTxDataBuffer[0]);

  return (COMMAND_SUCCESS);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_print_unit_data() sends the current unit configuration
 * back over the requesting socket.
 *
 * @return
 * - type  : int
 * @param[in] pSocketEl
 * - type : T_SOCKET_ELEMENT*
 * - range: the whole address range is valid
 * @param[in] pRemoteAddress
 * - type : T_SDAI_SOCK_ADDR*
 * - range: the whole address range is valid
 */
static int scan_tcp_server_command_print_unit_data (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress)
{
  struct T_DEMO_UNIT*   pUnit;
  unsigned              UnitIndex;
  unsigned              DataLength = 0;


  for (UnitIndex = 0; UnitIndex < _NUMBER_ARRAY_ELEMENTS (ApplManagement.UnitsManagement.UnitList); UnitIndex++)
  {
    pUnit = (ApplManagement.UnitsManagement.UnitList + UnitIndex);

    if (pUnit->Id != SDAI_INVALID_ID)
    {
      DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Unit: %d\r\n\tID: 0x%.8lX\r\n\tType: ", UnitIndex, pUnit->Id);

      switch (pUnit->Type)
      {
        case SDAI_UNIT_TYPE_HEAD               : DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Head");             break;
        case SDAI_UNIT_TYPE_GENERIC_INPUT      : DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Input");            break;
        case SDAI_UNIT_TYPE_GENERIC_OUTPUT     : DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Output");           break;
        case SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT: DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Input/Output");     break;
        case SDAI_MB_UNIT_TYPE_DISCRETE_INPUT  : DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Discrete Input");   break;
        case SDAI_MB_UNIT_TYPE_COILS           : DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Coils");            break;
        case SDAI_MB_UNIT_TYPE_INPUT_REGISTER  : DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Input Register");   break;
        case SDAI_MB_UNIT_TYPE_HOLDING_REGISTER: DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Holding Register"); break;
        default                                : DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "invalid");          break;
      }

      DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "\r\n\tInputSize: %d\r\n\tOutputSize: %d\r\n", pUnit->InputSize, pUnit->OutputSize);

      switch (ApplManagement.ConfigDataAppl.BackEnd)
      {
        case SDAI_BACKEND_PN:
          DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "\tIdentnumber Module: 0x%8.8lX\r\n", pUnit->CfgData.UseAs.Pn.IdentNumber);
          DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "\tIdentnumber Submodule: 0x%8.8lX\r\n", pUnit->CfgData.UseAs.Pn.SubmoduleIdentNumber);
          DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "\tFlags: 0x%X\r\n", pUnit->CfgData.UseAs.Pn.Flags);
          break;

        case SDAI_BACKEND_ECAT:
        case SDAI_BACKEND_EIPS:
        case SDAI_BACKEND_MODBUS:
        case SDAI_BACKEND_EPL:
        default:
          break;
      }
    }
  }

  socket_send (pSocketEl, pRemoteAddress, DataLength, &SocketTxDataBuffer[0]);

  return (COMMAND_SUCCESS);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_set_input_data() changes the input data
 * and status of the selected unit.
 *
 * @return
 * - type  : int
 * @param[in] pSubString
 * - type : char*
 * - range: whole address range is valid
 * @param[in] RemainingLength
 * - type : int
 * - range: whole range is valid
 */
static int scan_tcp_server_command_set_input_data (char* pSubString, int RemainingLength)
{
  U32   UnitId;
  U16   Status;


  /* remove leading blanks */
  while ( (*pSubString == ' ') && (RemainingLength-- > 0) ) { pSubString += 1; }

  if (sscanf (pSubString, "0x%lX 0x%hu", &UnitId, &Status) == 2)
  {
    struct T_DEMO_UNIT*   pUnit = demo_find_unit(UnitId);
    U16                   Data;
    U8                    DataIndex = 0;


    if (pUnit == NULL) {return (COMMAND_ERROR_PARAMETER);}

    if ( (pUnit->Id == SDAI_INVALID_ID) || ((pUnit->InputSize == 0) && (pUnit->Type != SDAI_UNIT_TYPE_HEAD)) )
    {
      return (COMMAND_ERROR_PARAMETER);
    }

    pUnit->InputState = (U8)Status;

    if (pUnit->InputSize != 0)
    {
      /* remove the first 2 parameter */
      pSubString = strstr (pSubString, " ") + 1;
      if ((pSubString = strstr (pSubString, " ")) == NULL) {return (COMMAND_SUCCESS);}
      pSubString++;

      while (sscanf (pSubString, "0x%hu", &Data) == 1)
      {
        pUnit->pInputData [DataIndex++] = (U8)Data;

        pSubString = strstr (pSubString, " ") + 1;

        if ( (pSubString == NULL) || (DataIndex >= pUnit->InputSize) ) {break;}
      }
    }
  }
  else
  {
    return (COMMAND_ERROR_FORMAT);
  }

  return (COMMAND_SUCCESS);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_get_output_data() sends the current output
 * data back over the requesting socket.
 *
 * @return
 * - type  : int
 * @param[in] pSocketEl
 * - type : T_SOCKET_ELEMENT*
 * - range: the whole address range is valid
 * @param[in] pRemoteAddress
 * - type : T_SDAI_SOCK_ADDR*
 * - range: the whole address range is valid
 */
static int scan_tcp_server_command_get_output_data (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress)
{
  struct T_DEMO_UNIT*   pUnit;
  unsigned              UnitIndex;
  unsigned              DataLength = 0;


  for (UnitIndex = 0; UnitIndex < _NUMBER_ARRAY_ELEMENTS (ApplManagement.UnitsManagement.UnitList); UnitIndex++)
  {
    pUnit = (ApplManagement.UnitsManagement.UnitList + UnitIndex);

    if( (pUnit->Id != SDAI_INVALID_ID) && (pUnit->OutputSize != 0) )
    {
      U8   DataIndex;

      DataLength += sprintf ((char*) &SocketTxDataBuffer[DataLength], "Unit: %2d, Status: ", UnitIndex);

      if (pUnit->OutputState == SDAI_DATA_STATUS_VALID)
      {
        DataLength += sprintf((char*) &SocketTxDataBuffer[DataLength], "Valid, Data: ");
      }
      else
      {
        if (pUnit->OutputState == SDAI_DATA_STATUS_INVALID)
        {
          DataLength += sprintf((char*) &SocketTxDataBuffer[DataLength], "Invalid, Data: ");
        }
        else
        {
          DataLength += sprintf((char*) &SocketTxDataBuffer[DataLength], "0x%02X, Data: ", pUnit->OutputState);
        }
      }

      DataIndex = 0;

      while (1)
      {
        if (DataIndex == (pUnit->OutputSize - 1)) {DataLength += sprintf((char*) &SocketTxDataBuffer[DataLength], "0x%02X\r\n", pUnit->pOutputData[DataIndex]); break;}

        DataLength += sprintf((char*) &SocketTxDataBuffer[DataLength], "0x%02X, ", pUnit->pOutputData[DataIndex]);

        DataIndex++;
      }
    }
  }

  socket_send (pSocketEl, pRemoteAddress, DataLength, &SocketTxDataBuffer[0]);

  return (COMMAND_SUCCESS);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_plug_unit() adds a new unit to configuration
 * of the SDAI and application.
 *
 * @return
 * - type  : int
 * @param[in] pSubString
 * - type : char*
 * - range: whole address range is valid
 * @param[in] RemainingLength
 * - type : int
 * - range: whole range is valid
 */
static int scan_tcp_server_command_plug_unit (char* pSubString, int RemainingLength)
{
  U32   UnitId;
  U16   Type;
  U16   InputSize;
  U16   OutputSize;
  U8    SdaiResult;
  char* pData;


  /* remove leading blanks */
  while ( (*pSubString == ' ') && (RemainingLength-- > 0) ) { pSubString += 1; }

  if (sscanf (pSubString, "0x%lX %hu %hu %hu", &UnitId, &Type, &InputSize, &OutputSize) == 4)
  {
    struct T_DEMO_UNIT*          pUnit = demo_find_unit(UnitId);
    static struct SDAI_CFG_DATA  LocalCfgData;


    pData = strstr (pSubString, "-");
    if (pData == NULL) {return (COMMAND_ERROR_FORMAT);}

    pData += strlen ("-");
    while ( (*pData == ' ') && (RemainingLength-- > 0) ) { pData += 1; }

    RemainingLength -= (int) (pData - pSubString);
    pSubString       = pData;

    switch (ApplManagement.ConfigDataAppl.BackEnd)
    {
      case SDAI_BACKEND_PN:
      {
        U32   IdentNumber;
        U32   SubmoduleIdentNumber;


        if (sscanf (pSubString, "0x%lX 0x%lX", &IdentNumber, &SubmoduleIdentNumber) != 2)
        {
          return (COMMAND_ERROR_FORMAT);
        }

        return (profinet_plug_unit (&ApplManagement.UnitsManagement, (U8) Type, (U8) InputSize, (U8) OutputSize, IdentNumber, SubmoduleIdentNumber, UnitId, 0u, SDAI_PN_DEFAULT_PROFILE));
      }

      case SDAI_BACKEND_EIPS:
        break;

      case SDAI_BACKEND_MODBUS:
        break;

      case SDAI_BACKEND_ECAT:
      {
        U16   PdoIndex;
        U16   Flags;
        U16   NumberMappedObjects;


        if (pUnit == NULL) {return (COMMAND_ERROR_PARAMETER);}

        if ( (pUnit->pInputData == NULL) || (pUnit->pOutputData == NULL) ) {return (-3);}

        if (sscanf (pSubString, "0x%hX %hu %hu", &PdoIndex, &Flags, &NumberMappedObjects) == 3)
        {
          U16   Index;
          U16   SubIndex;
          U16   BitLength;
          U8    CfgIndex = 0;


          LocalCfgData.UseAs.Ecat.Index               = PdoIndex;
          LocalCfgData.UseAs.Ecat.Flags               = Flags;
          LocalCfgData.UseAs.Ecat.NumberMappedObjects = NumberMappedObjects;

          pData = strstr (pSubString, "[");
          if (pData == NULL) {return (COMMAND_ERROR_FORMAT);}

          RemainingLength -= (int) (pData - pSubString);
          pSubString       = pData;

          while (RemainingLength > (int)strlen ("[0 0 0]"))
          {
            if (sscanf (pSubString, "[0x%hX %hu %hu]", &Index, &SubIndex, &BitLength) == 3)
            {
              LocalCfgData.UseAs.Ecat.ObjectDescr [CfgIndex].Index     = Index;
              LocalCfgData.UseAs.Ecat.ObjectDescr [CfgIndex].SubIndex  = SubIndex;
              LocalCfgData.UseAs.Ecat.ObjectDescr [CfgIndex].BitLength = BitLength;

              CfgIndex++;
            }
            else
            {
              break;
            }

            pData = strstr (pSubString, "]");
            if (pData == NULL) {return (COMMAND_ERROR_FORMAT);}

            pData += strlen ("]");
            while ( (*pData == ' ') && (RemainingLength-- > 0) ) { pData += 1; }

            RemainingLength -= (int) (pData - pSubString);
            pSubString       = pData;
          }

          if (CfgIndex != NumberMappedObjects) {return (COMMAND_ERROR_PARAMETER);}
        }
        else
        {
          return (COMMAND_ERROR_FORMAT);
        }

        break;
      }

      case SDAI_BACKEND_EPL:
        break;

      default:
        break;
    }

    pUnit->Id                             = UnitId;
    pUnit->Type                           = Type;
    pUnit->InputSize                      = InputSize;
    pUnit->OutputSize                     = OutputSize;
    pUnit->CfgData.PeripheralOffsetInput  = SDAI_PERIPHERAL_OFFSET_DPRAM;
    pUnit->CfgData.PeripheralOffsetOutput = SDAI_PERIPHERAL_OFFSET_DPRAM;


    memcpy (&pUnit->CfgData, &LocalCfgData, sizeof (pUnit->CfgData));

    SdaiResult = sdai_plug_unit_ext (pUnit->Type, pUnit->InputSize, pUnit->OutputSize, &pUnit->Id, &pUnit->CfgData);

    if (SdaiResult != SDAI_SUCCESS)
    {
      pUnit->Id = SDAI_INVALID_ID;
    }

    return (SdaiResult);
  }
  else
  {
    return (COMMAND_ERROR_FORMAT);
  }

  return (COMMAND_SUCCESS);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_pull_unit() removes a unit from the
 * configuration of the SDAI and application.
 *
 * @return
 * - type  : int
 * @param[in] pSubString
 * - type : char*
 * - range: whole address range is valid
 * @param[in] RemainingLength
 * - type : int
 * - range: whole range is valid
 */
static int scan_tcp_server_command_pull_unit (char* pSubString, int RemainingLength)
{
  U32   UnitId;


  /* remove leading blanks */
  while ( (*pSubString == ' ') && (RemainingLength-- > 0) ) { pSubString += 1; }

  if (sscanf (pSubString, "0x%lX", &UnitId) == 1)
  {
    struct T_DEMO_UNIT*   pUnit = demo_find_unit(UnitId);
    U8                    SdaiResult;

    if (pUnit == NULL) {return (COMMAND_ERROR_PARAMETER);}

    switch (ApplManagement.ConfigDataAppl.BackEnd)
    {
      case SDAI_BACKEND_PN:
      {
        return (profinet_pull_unit (&ApplManagement.UnitsManagement, UnitId));
      }

      default:
      {
        SdaiResult = sdai_pull_unit (UnitId);

        if (SdaiResult == SDAI_SUCCESS)
        {
          pUnit->Id          = SDAI_INVALID_ID;
          pUnit->InputState  = SDAI_DATA_STATUS_INVALID;
          pUnit->OutputState = SDAI_DATA_STATUS_INVALID;
          pUnit->InputSize   = 0u;
          pUnit->OutputSize  = 0u;
        }
      }
    }

    return (SdaiResult);
  }
  else
  {
    return (COMMAND_ERROR_FORMAT);
  }

  return (COMMAND_SUCCESS);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_send_diagnosis() sends a diagnosis
 * with the requested parameter.
 *
 * @return
 * - type  : int
 * @param[in] pSubString
 * - type : char*
 * - range: whole address range is valid
 * @param[in] RemainingLength
 * - type : int
 * - range: whole range is valid
 */
static int scan_tcp_server_command_send_diagnosis (char* pSubString, int RemainingLength)
{
  U32   UnitId;
  U32   Type;
  U16   Severity;
  U16   Parameter;
  U8    SdaiResult;

  /* remove leading blanks */
  while ( (*pSubString == ' ') && (RemainingLength-- > 0) ) { pSubString += 1; }

  if (sscanf (pSubString, "0x%lX %li %hu %hi", &UnitId, &Type, &Severity, &Parameter) == 4)
  {
    if (strstr(pSubString,"appear"))
    {
      struct T_DEMO_DIAGNOSIS_REQ Diagnosis;

      Diagnosis.DiagReq.Id          = UnitId;
      Diagnosis.DiagReq.Severity    = Severity;
      Diagnosis.DiagReq.Type        = Type;
      Diagnosis.DiagReq.Length      = sizeof (struct SDAI_ECAT_DIAGNOSIS_DATA);

      if (strstr (pSubString, "disappear"))
      {
        Diagnosis.DiagReq.StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_DISAPPEARS;
      }
      else
      {
        Diagnosis.DiagReq.StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS;
      }

      switch (ApplManagement.ConfigDataAppl.BackEnd)
      {
        case SDAI_BACKEND_PN:
          SdaiResult = profinet_send_diagnosis (2, Parameter, Diagnosis.DiagReq.StatusSpecifier);
          break;

        case SDAI_BACKEND_ECAT:
          Diagnosis.UseAs.EcatDiagData.TextId          = Parameter;
          Diagnosis.UseAs.EcatDiagData.NumberParameter = 0u;

          SdaiResult = sdai_diagnosis_request (&Diagnosis.DiagReq);
          break;

        case SDAI_BACKEND_EIPS:
        case SDAI_BACKEND_MODBUS:
        case SDAI_BACKEND_EPL:
        default:
          SdaiResult = COMMAND_ERROR_FORMAT;
          break;
      }
    }
    else
    {
      SdaiResult = COMMAND_ERROR_FORMAT;
    }
  }
  else
  {
    SdaiResult = COMMAND_ERROR_FORMAT;
  }

  return (SdaiResult);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_stack_restart() restarts the protocol stack.
 *
 * @return
 * - type  : int
 * @param[in] pSubString
 * - type : char*
 * - range: whole address range is valid
 * @param[in] RemainingLength
 * - type : int
 * - range: whole range is valid
 */
static int scan_tcp_server_command_stack_restart (char* pSubString, int RemainingLength)
{
  U16    Backend;

  /* remove leading blanks */
  while ( (*pSubString == ' ') && (RemainingLength-- > 0) ) { pSubString += 1; }

  if (sscanf (pSubString, "%hu", &Backend) == 1)
  {
    if (pFlashConfigData->BackEnd != Backend)
    {
      if ( (Backend != SDAI_BACKEND_EIPS) &&
           (Backend != SDAI_BACKEND_PN)   &&
           (Backend != SDAI_BACKEND_MODBUS) )
      {
        return (COMMAND_ERROR_PARAMETER);
      }

      pFlashConfigData->BackEnd = Backend;

      platform_send_event (EVENT_STORE_FLASH_DATA);
    }
  }

  platform_send_event (EVENT_RESTART_STACK);


  return (COMMAND_SUCCESS);
}

/*===========================================================================*/

/**
 * The function scan_tcp_server_command_firmware() updates the firmware.
 *
 * @return
 * - type  : int
 * @param[in] pSubString
 * - type : char*
 * - range: whole address range is valid
 * @param[in] RemainingLength
 * - type : int
 * - range: whole range is valid
 * @param[in] pSocketEl
 * - type : T_SOCKET_ELEMENT*
 * - range: the whole address range is valid
 * @param[in] pRemoteAddress
 * - type : T_SDAI_SOCK_ADDR*
 * - range: the whole address range is valid
 */
static int scan_tcp_server_command_firmware (char* pSubString, int RemainingLength, T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress)
{
  U32   FirmwareSize;
  U8*   pFirmwareBuffer;
  U32   ResponseLength = 0;
  BOOL  UpdateResult;

  T_SOCKET_RESULT     Result;

  /* remove leading blanks */
  while ( (*pSubString == ' ') && (RemainingLength-- > 0) ) { pSubString += 1; }

  if (sscanf (pSubString, "%lu", &FirmwareSize) == 1)
  {
    while ( (*pSubString != ' ') && (RemainingLength-- > 0) ) { pSubString += 1; }
    pSubString += 1;
    RemainingLength--;

    if (FirmwareSize < (U32)RemainingLength)
    {
      return (COMMAND_ERROR_PARAMETER);
    }

    pFirmwareBuffer = malloc (FirmwareSize);

    if (pFirmwareBuffer != NULL)
    {
      U32   ReceiveLength = RemainingLength;
      U16   DataLength    = _MIN(SOCKET_FIFO_SIZE, (FirmwareSize - ReceiveLength));

      memcpy (pFirmwareBuffer, pSubString, ReceiveLength);

      do
      {
        Result = sdai_sock_receive (pSocketEl->SocketNumber, pRemoteAddress, &DataLength, (pFirmwareBuffer + ReceiveLength));

        if (Result == SOCK_SUCCESS)
        {
          ReceiveLength += DataLength;
          DataLength     = _MIN(SOCKET_FIFO_SIZE, (FirmwareSize - ReceiveLength));
        }

      } while ((ReceiveLength < FirmwareSize) || (Result == SOCK_ERR_NO_DATA_RECEIVED));

      ResponseLength = snprintf ((char*) &SocketTxDataBuffer[0], sizeof(SocketTxDataBuffer), "Received %lu byte, starting update...", ReceiveLength);
      socket_send (pSocketEl, pRemoteAddress, ResponseLength, &SocketTxDataBuffer[0]);

      UpdateResult = firmware_update (pFirmwareBuffer, FirmwareSize);

      free (pFirmwareBuffer);

      if (UpdateResult)
      {
        ResponseLength = snprintf ((char*) &SocketTxDataBuffer[0], sizeof(SocketTxDataBuffer), "failed");
        socket_send (pSocketEl, pRemoteAddress, ResponseLength, &SocketTxDataBuffer[0]);
      }
      else
      {
        ResponseLength = snprintf ((char*) &SocketTxDataBuffer[0], sizeof(SocketTxDataBuffer), "successful");
        socket_send (pSocketEl, pRemoteAddress, ResponseLength, &SocketTxDataBuffer[0]);
      }

      return (COMMAND_SUCCESS);
    }
    else
    {
      return (COMMAND_ERROR_RESSOURCE);
    }

  }
  else
  {
    return (COMMAND_ERROR_FORMAT);
  }

  return (COMMAND_SUCCESS);
}

/*===========================================================================*/

/**
 * The function print_help_string() assembles the help string and returns the
 * length of the string.
 *
 * @return
 * - type  : U32
 */
static U32 print_help_string ()
{
  U32   Length = 0;

  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_HEADER_SOCKET);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_SOCKET_OVERVIEW);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_SOCKET_STATUS);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_OPEN);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_CLOSE);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_CONNECT);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_DISCONNECT);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_SEND);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_ECHO);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_FILTER);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_ADD_MEM);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_DEL_MEM);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_SOCK_RESTART);

  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_HEADER_STACK);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_STACK_STATUS);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_UNITCONFIG);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_INPUT_DATA);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_OUTPUT_DATA);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_PLUG);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_PULL);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_DIAG);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_RESTART_STACK);
  Length += snprintf ((char*) &SocketTxDataBuffer[Length], (sizeof(SocketTxDataBuffer) - Length), "%s", _USAGE_FIRMWARE);

  return (Length);
}

/*===========================================================================*/

/**
 * The function memfill() fills the memory with random data
 *
 * @return
 * - type  : void
 * @param[in] pDest
 * - type : U8*
 * - range: whole address range is valid, except NULL
 * @param[in] Size
 * - type : U16
 * - range: whole range is valid
 */
static void memfill (U8* pDest, U16 Size)
{
  U16 Index;


  for (Index = 0; Index < Size; Index++)
  {
    pDest[Index] = (U8)rand ();
  }

  return;
}

#endif /* SDAI_SOCKET_INTERFACE */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
