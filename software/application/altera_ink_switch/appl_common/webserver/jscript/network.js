/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

var NetworkReqObj     = null;

/****************************************************************************/
/*jslint devel: true, browser: true, white: true, plusplus: true */
/*global util_request_object: true, util_ajax_request: true, util_ajax_abort: true,
         AjaxDefaultTimeout: true, PageNetwork: true*/

function network_handle_response (Response)
{
  "use strict";

  var NetworkData;

  /* we know the eval is evil */
  /*jslint evil: true */
  NetworkData = eval ('(' + Response  + ')');
  /*jslint evil: false */

  document.getElementById("Name").value = NetworkData.Name;

  document.getElementById("Ip").value = NetworkData.Ip;
  document.getElementById("Nm").value = NetworkData.Nm;
  document.getElementById("Gw").value = NetworkData.Gw;

  return;
}

/*==========================================================================*/

function network_submit ()
{
  "use strict";

  var FormElement = document.getElementById ("NetworkForm");

  util_ajax_request (NetworkReqObj, 'post', FormElement, PageNetwork, network_handle_response, AjaxDefaultTimeout);

  return (false);
}

/*==========================================================================*/

function init ()
{
  "use strict";

  NetworkReqObj = util_request_object ();

  util_ajax_request (NetworkReqObj, 'get', null, PageNetwork, network_handle_response, AjaxDefaultTimeout);

  return;
}

/*==========================================================================*/

function term ()
{
  "use strict";

  util_ajax_abort (NetworkReqObj);

  return;
}

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */