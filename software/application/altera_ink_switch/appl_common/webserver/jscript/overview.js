/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

var OverviewReqObj     = null;
var OverviewTimeoutObj = null;

/****************************************************************************/
/*jslint devel: true, browser: true, white: true, plusplus: true*/
/*global util_request_object: true, util_ajax_request: true, util_ajax_abort: true,
         AjaxUpdateTime: true, AjaxDefaultTimeout: true, PageOverview: true*/

/*==========================================================================*/

function overview_handle_response (Response)
{
  "use strict";

  clearTimeout (OverviewTimeoutObj);

  var OverviewInfo, IntStatus, Cell;

  /* we know that eval is evil */
  /*jslint evil: true */
  OverviewInfo = eval ('(' + Response  + ')');
  /*jslint evil: false */

  Cell = document.getElementById ("Proto").firstChild;

  switch (parseInt (OverviewInfo.Proto, 10))
  {
    case 1:
      Cell.nodeValue = "EtherNet/IP"; break;
    case 2:
      Cell.nodeValue = "PROFINET IO"; break;
    case 3:
      Cell.nodeValue = "PROFIBUS DP"; break;
    case 4:
      Cell.nodeValue = "EtherCAT"; break;
    case 5:
      Cell.nodeValue = "ModbusTCP"; break;
    case 6:
      Cell.nodeValue = "Powerlink"; break;
    default:
      Cell.nodeValue = "Unknown"; break;
  }

  IntStatus = parseInt (OverviewInfo.Status, 10);

  Cell = document.getElementById("Status").firstChild;

  /*jslint bitwise: true*/
  if (IntStatus & 0x1F8)    {Cell.nodeValue = "Error";}
  else if (IntStatus & 0x4) {Cell.nodeValue = "Connected";}
  else if (IntStatus & 0x2) {Cell.nodeValue = "Online";}
  else if (IntStatus & 0x1) {Cell.nodeValue = "Initialized";}
  else                      {Cell.nodeValue = "Unknown";}
  /*jslint bitwise: false*/

  document.getElementById ("Name").firstChild.nodeValue = OverviewInfo.Name;

  document.getElementById ("Md").firstChild.nodeValue = OverviewInfo.Md;
  document.getElementById ("Mp1").firstChild.nodeValue = OverviewInfo.Mp1;
  document.getElementById ("Mp2").firstChild.nodeValue = OverviewInfo.Mp2;

  document.getElementById ("Ip").firstChild.nodeValue = OverviewInfo.Ip;
  document.getElementById ("Nm").firstChild.nodeValue = OverviewInfo.Nm;
  document.getElementById ("Gw").firstChild.nodeValue = OverviewInfo.Gw;

  OverviewTimeoutObj = setTimeout (function() {util_ajax_request (OverviewReqObj, 'get', null, PageOverview, overview_handle_response, AjaxDefaultTimeout);}, AjaxUpdateTime);

  return;
}

/*==========================================================================*/

function init ()
{
  "use strict";

  OverviewReqObj = util_request_object ();

  util_ajax_request (OverviewReqObj, 'get', null, PageOverview, overview_handle_response, AjaxDefaultTimeout);

  return;
}

/*==========================================================================*/

function term ()
{
  "use strict";

  util_ajax_abort (OverviewReqObj);
  clearTimeout (OverviewTimeoutObj);

  return;
}

/*****************************************************************************/
/* vim: set nu ts=4 et nowrap: */