/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

var DiagnosisReqObj     = null;
var DiagnosisTimeoutObj = null;

/*==========================================================================*/

/*jslint devel: true, browser: true, white: true, plusplus: true*/
/*global util_ajax_request: true, AjaxUpdateTime: true, AjaxDefaultTimeout: true,
         util_request_object: true, PageDiagnosis: true*/

/*==========================================================================*/

function handle_response_cpuload (CpuName, Data)
{
  "use strict";

  var Obj;

  util_progress_bar (CpuName + "Load100ms", Data.Load100ms, 0);
  util_progress_bar (CpuName + "Load1s", Data.Load1s, 0);
  util_progress_bar (CpuName + "Load10s", Data.Load10s, 0);

  return;
}

/*==========================================================================*/

function handle_response_network (Data)
{
  "use strict";

  var Index, IndexString, Cell;

  Cell = document.getElementById ("Type").firstChild;

  switch (parseInt (Data.Type, 10))
  {
    case 1:
      Cell.nodeValue = "Ethernet";
      Cell = document.getElementById ("State").firstChild;

      switch (parseInt (Data.State, 10))
      {
        case 0:  Cell.nodeValue = "Not Configured"; break;
        case 1:  Cell.nodeValue = "Static IP"; break;
        case 2:  Cell.nodeValue = "DHCP Valid"; break;
        case 3:  Cell.nodeValue = "DHCP Invalid"; break;
        default: Cell.nodeValue = "Unknown"; break;
      }

      break;

    default:
      Cell.nodeValue = "Unknown";
      document.getElementById ("State").firstChild.nodeValue = "-";
      break;

  }

  for (Index = 0; Index < Data.Ports.length; Index++)
  {
    IndexString = Index.toString ();

    if (Data.Ports [Index].Lnk === "1")
    {
      document.getElementById ("Lnk" + IndexString).firstChild.nodeValue = "Up";

      Cell = document.getElementById ("Con" + IndexString).firstChild;

      switch (parseInt (Data.Ports [Index].Con, 10))
      {
        case 0:  Cell.nodeValue = "10MBit/HD"; break;
        case 1:  Cell.nodeValue = "100MBit/HD"; break;
        case 2:  Cell.nodeValue = "10MBit/FD"; break;
        case 3:  Cell.nodeValue = "100MBit/FD"; break;
        default: Cell.nodeValue = "Unknown"; break;
      }

      Cell = document.getElementById ("Neg" + IndexString).firstChild;

      switch (parseInt (Data.Ports [Index].Neg, 10))
      {
        case 1:  Cell.nodeValue = "Progress"; break;
        case 2:  Cell.nodeValue = "Failed"; break;
        case 3:  Cell.nodeValue = "Failed speed detected"; break;
        case 4:  Cell.nodeValue = "Successfull"; break;
        case 5:  Cell.nodeValue = "Disabled"; break;
        default: Cell.nodeValue = "Unknown"; break;
      }

      if (Data.Ports [Index].Len === "65535") {document.getElementById ("Len" + IndexString).firstChild.nodeValue = "-";}
      else                                    {document.getElementById ("Len" + IndexString).firstChild.nodeValue = Data.Ports [Index].Len;}
    }
    else
    {
      document.getElementById ("Lnk" + IndexString).firstChild.nodeValue = "Down";
      document.getElementById ("Con" + IndexString).firstChild.nodeValue = "-";
      document.getElementById ("Neg" + IndexString).firstChild.nodeValue = "-";
      document.getElementById ("Len" + IndexString).firstChild.nodeValue = "-";
    }
  }

  return;
}

/*==========================================================================*/

function handle_response_memory (CpuName, Data)
{
  "use strict";

  var Obj;

  Obj = document.getElementById (CpuName + "Mem").firstChild;

  Obj.nodeValue = Data.All + ' / ' + Data.Used;

  Obj = document.getElementById (CpuName + "MemB");
  Obj.style.width = "20px";

  if (10 > 90) {Obj.style.backgroundColor="red";}
  else                                    {Obj.style.backgroundColor="green";}

  return;
}

/*==========================================================================*/

function handle_response_exception (Data)
{
  "use strict";

  if (Data.Act !== "0")
  {
    document.getElementById ("Cod").firstChild.nodeValue = Data.Cod;
    document.getElementById ("Cha").firstChild.nodeValue = Data.Cha;
    document.getElementById ("Mod").firstChild.nodeValue = Data.Mod;
    document.getElementById ("Fil").firstChild.nodeValue = Data.Fil;
    document.getElementById ("Lin").firstChild.nodeValue = Data.Lin;
    document.getElementById ("Err").firstChild.nodeValue = Data.Err;
    document.getElementById ("Par").firstChild.nodeValue = Data.Par;
    document.getElementById ("Str").firstChild.nodeValue = Data.Str;
  }

  return;
}

/*==========================================================================*/

function diagnosis_handle_response (Response)
{
  "use strict";

  clearTimeout (DiagnosisTimeoutObj);

  var DiagnosisData;

  /* we know that eval is evil */
  /*jslint evil: true */
  DiagnosisData = eval ('(' + Response  + ')');
  /*jslint evil: false */

  //handle_response_memory ("FB", DiagnosisData.FBMem);
  //handle_response_memory ("Appl", DiagnosisData.ApplMem);
  handle_response_cpuload ("FB", DiagnosisData.FBLoad);
  handle_response_cpuload ("Appl", DiagnosisData.ApplLoad);
  handle_response_network (DiagnosisData.Interface);
  handle_response_exception (DiagnosisData.Exception);

  DiagnosisTimeoutObj = setTimeout (function() {util_ajax_request (DiagnosisReqObj, 'get', null, PageDiagnosis, diagnosis_handle_response, AjaxDefaultTimeout);}, AjaxUpdateTime);

  return;
}

/*==========================================================================*/

function init ()
{
  "use strict";

  DiagnosisReqObj = util_request_object ();

  util_ajax_request (DiagnosisReqObj, 'get', null, PageDiagnosis, diagnosis_handle_response, AjaxDefaultTimeout);

  return;
}

/*==========================================================================*/

function term ()
{
  "use strict";

  util_ajax_abort (DiagnosisReqObj);
  clearTimeout (DiagnosisTimeoutObj);

  return;
}

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */