/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

var IoDataReqObj     = null;
var IoDataTimeoutObj = null;

/*==========================================================================*/

/*jslint devel: true, browser: true, white: true, plusplus: true*/
/*global util_ajax_request: true, AjaxUpdateTime: true, AjaxDefaultTimeout: true,
         util_delete_table: true, util_request_object: true, util_ajax_abort: true,
         PageIoData: true*/

/*==========================================================================*/

function add_row (TableBody, Id, Type, StatusIn, StatusOut, DataIn, DataOut)
{
  "use strict";

  var Row, IdCell, TypeCell, StatusInCell, StatusOutCell, SizeInCell, SizeOutCell, DataInCell, DataOutCell, Length, Index, Value, IsInput, IsOutput;

  Row           = document.createElement ("tr");
  IdCell        = document.createElement ("td");
  TypeCell      = document.createElement ("td");
  StatusInCell  = document.createElement ("td");
  StatusOutCell = document.createElement ("td");
  SizeInCell    = document.createElement ("td");
  SizeOutCell   = document.createElement ("td");
  DataInCell    = document.createElement ("td");
  DataOutCell   = document.createElement ("td");

  /*-----------------------------------------------------------------------*/

  IdCell.innerHTML = Id;

  switch (parseInt (Type, 10))
  {
    case 1:
      TypeCell.innerHTML = "Head"; IsInput = true; IsOutput = false; break;
    case 2:
      TypeCell.innerHTML = "Input"; IsInput = true; IsOutput = false; break;
    case 3:
      TypeCell.innerHTML = "Output"; IsInput = false; IsOutput = true; break;
    case 4:
      TypeCell.innerHTML = "Input/Output"; IsInput = true; IsOutput = true; break;
    case 251:
      TypeCell.innerHTML = "Holding Registers"; IsInput = true; IsOutput = true; break;
    case 252:
      TypeCell.innerHTML = "Input Registers"; IsInput = true; IsOutput = false; break;
    case 253:
      TypeCell.innerHTML = "Coils"; IsInput = false; IsOutput = true; break;
    case 254:
      TypeCell.innerHTML = "Discrete Input"; IsInput = true; IsOutput = false; break;
    default:
      TypeCell.innerHTML = "Unknown"; IsInput = false; IsOutput = false; break;
  }

  if      (IsInput === false)                {StatusInCell.innerHTML = "-";}
  else if (parseInt (StatusIn, 16) === 0x80) {StatusInCell.innerHTML = "good (0x" + StatusIn + ")";}
  else                                       {StatusInCell.innerHTML = "bad (0x" + StatusIn + ")";}

  if      (IsOutput === false)                {StatusOutCell.innerHTML = "-";}
  else if (parseInt (StatusOut, 16) === 0x80) {StatusOutCell.innerHTML = "good (0x" + StatusOut + ")";}
  else                                        {StatusOutCell.innerHTML = "bad (0x" + StatusOut + ")";}

  Length = DataIn.length;

  if ((Length === 0) || (IsInput === false))
  {
    SizeInCell.innerHTML = "-";
    DataInCell.innerHTML = "-";
  }
  else
  {
    SizeInCell.innerHTML = Length.toString (10);

    for (Index = 0; Index < Length; Index++)
    {
      Value = parseInt (DataIn [Index], 10);

      if (Index > 0) {DataInCell.innerHTML += ", ";}

      DataInCell.innerHTML += Value.toString (16);
    }
  }

  Length = DataOut.length;

  if ((Length === 0) || (IsOutput === false))
  {
    SizeOutCell.innerHTML = "-";
    DataOutCell.innerHTML = "-";
  }
  else
  {
    SizeOutCell.innerHTML = Length.toString (10);

    for (Index = 0; Index < Length; Index++)
    {
      Value = parseInt (DataOut [Index], 10);

      if (Index > 0) {DataOutCell.innerHTML += ", ";}

      DataOutCell.innerHTML += Value.toString (16);
    }
  }

  /*-----------------------------------------------------------------------*/

  Row.appendChild (IdCell);
  Row.appendChild (TypeCell);
  Row.appendChild (StatusInCell);
  Row.appendChild (StatusOutCell);
  Row.appendChild (SizeInCell);
  Row.appendChild (SizeOutCell);
  Row.appendChild (DataInCell);
  Row.appendChild (DataOutCell);

  TableBody.appendChild (Row);

  return;
}

/*==========================================================================*/

function iodata_handle_response (Response)
{
  "use strict";

  var IoDataInfo, Table, TableBody, Index;

  clearTimeout (IoDataTimeoutObj);

  /* we know that eval is evil */
  /*jslint evil: true */
  IoDataInfo = eval('(' + Response  + ')');
  /*jslint evil: false */

  util_delete_table ("IODataTable");

  Table     = document.getElementById ("IODataTable");
  TableBody = Table.getElementsByTagName ("tbody")[0];

  for (Index = 0; Index < IoDataInfo.IO.length; Index++)
  {
    add_row (TableBody, IoDataInfo.IO [Index].Id, IoDataInfo.IO [Index].Type, IoDataInfo.IO [Index].StatusIn, IoDataInfo.IO [Index].StatusOut, IoDataInfo.IO [Index].DataIn, IoDataInfo.IO [Index].DataOut);
  }

  IoDataTimeoutObj = setTimeout (function() {util_ajax_request (IoDataReqObj, 'get', null, PageIoData, iodata_handle_response, AjaxDefaultTimeout);}, AjaxUpdateTime);

  return;
}

/*==========================================================================*/

function init ()
{
  "use strict";

  IoDataReqObj = util_request_object ();

  util_ajax_request (IoDataReqObj, 'get', null, PageIoData, iodata_handle_response, AjaxDefaultTimeout);

  return;
}

/*==========================================================================*/

function term ()
{
  "use strict";

  util_ajax_abort (IoDataReqObj);
  clearTimeout (IoDataTimeoutObj);

  return;
}

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */