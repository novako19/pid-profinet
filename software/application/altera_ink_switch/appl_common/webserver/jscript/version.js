/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

var VersionReqObj     = null;

/****************************************************************************/
/*jslint devel: true, browser: true, white: true, plusplus: true */
/*global util_request_object: true, util_ajax_request: true,
         AjaxUpdateTime: true, AjaxDefaultTimeout: true, PageVersion: true*/

/*==========================================================================*/

function version_handle_response (Response)
{
  "use strict";

  var VersionData;

  /* we know that eval is evil */
  /*jslint evil: true */
  VersionData = eval('(' + Response  + ')');
  /*jslint evil: false */

  document.getElementById("Appl").firstChild.nodeValue  = VersionData.Appl;
  document.getElementById("Sdai").firstChild.nodeValue  = VersionData.Sdai;
  document.getElementById("Stack").firstChild.nodeValue = VersionData.Stack;
  document.getElementById("Mac").firstChild.nodeValue   = VersionData.Mac;

  return;
}

/*==========================================================================*/

function init()
{
  "use strict";

  VersionReqObj = util_request_object ();

  util_ajax_request (VersionReqObj, 'get', null, PageVersion, version_handle_response, AjaxDefaultTimeout);

  return;
}

/*==========================================================================*/

function term()
{
  "use strict";

  util_ajax_abort (VersionReqObj);

  return;
}

/*****************************************************************************/
/* vim: set nu ts=4 et nowrap: */
