/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

#ifndef __DEMO_H__
#define __DEMO_H__

/******************************************************************************
DEFINES
******************************************************************************/

/** \addtogroup demo_appl_defines */
//@{
#define APPLICATION_VERSION           "1.35.00.0"

#ifndef USIGN32_DECIMAL_FORMAT_STRING
#define USIGN32_DECIMAL_FORMAT_STRING           "%lu"
#endif

#ifndef USIGN32_HEX_FORMAT_STRING
#define USIGN32_HEX_FORMAT_STRING               "%08lX"
#endif

/** \anchor appl_def_eventflag @name Application Event Flags */
//@{
/* SDAI protocol stack callback events */
#define EVENT_OUTPUT_DATA_CHANGED     0x00000001  /**< Will be set by demo_output_data_callback() to indicate changed output data. */
#define EVENT_IDENT_DATA_CHANGED      0x00000002  /**< Will be set by demo_ident_data_callback() to indicate changed ident data. */
#define EVENT_WRITE_IND_RECEIVED      0x00000004  /**< Will be set by demo_write_req_callback() to indicate the reception of a write request. */
#define EVENT_READ_IND_RECEIVED       0x00000008  /**< Will be set by demo_read_req_callback() to indicate the reception of a read request. */
#define EVENT_ALARM_ACK_RECEIVED      0x00000010  /**< Not used. The function profinet_alarm_ack_callback() only resets the AlarmPending flag.  */
#define EVENT_CONTROL_IND_RECEIVED    0x00000020  /**< Will be set by demo_control_req_callback() to indicate the reception of a control request. */
#define EVENT_EXCEPTION               0x00000040  /**< Will be set by demo_exception_callback() to indicate a fatal error. */

/* SDAI socket callback events */
#define EVENT_SOCKET_DATA_RECEIVED    0x00001000  /**< Will be set by demo_socket_data_received_callback() to indicate the receiption of new data on a socket. */
#define EVENT_SOCKET_STATE_CHANGED    0x00002000  /**< Will be set by socket_state_changed_callback() to indicate the change of the state of a socket. */

/* Application specific events */
#define EVENT_CYCLIC_TIMER            0x00010000  /**< Timer event with a cycle time of #CYCLE_TIME_IN_MS for updating input data and status informations. */
#define EVENT_RESTART_STACK           0x00020000  /**< Will be set to restart the protocol stack, e.g. when the network settings for the protocol stack
                                                       have been changed or when a different communication protocol has been selected. */
#define EVENT_RESTART_NETWORK         0x00040000  /**< Will be set to restart the network interface of the application, e.g. when the network settings for
                                                       the application have been changed. */
#define EVENT_STORE_FLASH_DATA        0x00080000  /**< Will be set to store new configuration data, e.g. changed via web server
                                                       or new data received via the demo_ident_data_callback(). */
#define EVENT_TERMINATE               0x00100000  /**< Terminates the sample application */
#define EVENT_REBOOT                  0x00200000  /**< Reboots the sample application */
#define EVENT_PLATFORM_MASK           0xF0000000  /**< Events that can be used by the porting layer of the sample application */

#define EVENT_ALL_EVENTS              ( EVENT_OUTPUT_DATA_CHANGED  | \
                                        EVENT_IDENT_DATA_CHANGED   | \
                                        EVENT_WRITE_IND_RECEIVED   | \
                                        EVENT_READ_IND_RECEIVED    | \
                                        EVENT_CONTROL_IND_RECEIVED | \
                                        EVENT_EXCEPTION            | \
                                        EVENT_ALARM_ACK_RECEIVED   | \
                                        EVENT_CYCLIC_TIMER         | \
                                        EVENT_RESTART_STACK        | \
                                        EVENT_RESTART_NETWORK      | \
                                        EVENT_STORE_FLASH_DATA     | \
                                        EVENT_REBOOT               | \
                                        EVENT_SOCKET_DATA_RECEIVED | \
                                        EVENT_SOCKET_STATE_CHANGED | \
                                        EVENT_TERMINATE            | \
                                        EVENT_PLATFORM_MASK         )

#define EVENT_EXCEPTION_EVENTS        ( EVENT_CYCLIC_TIMER         | \
                                        EVENT_RESTART_STACK        | \
                                        EVENT_RESTART_NETWORK      | \
                                        EVENT_REBOOT               | \
                                        EVENT_SOCKET_DATA_RECEIVED | \
                                        EVENT_SOCKET_STATE_CHANGED | \
                                        EVENT_TERMINATE            | \
                                        EVENT_PLATFORM_MASK         )
//@}

/*---------------------------------------------------------------------------*/

/** \anchor appl_def_cbk @name Callback Handling */
//@{
#define PROTO_FUNC_SUCCESS                0x00   /**< execution of the protocol specific function without error */
#define PROTO_FUNC_ERR_NOT_SUPPORTED      0x01   /**< protocol specific function not supported */
#define PROTO_FUNC_ERR_DATA_NOT_UPDATED   0x02   /**< protocol specific function executed but data where not used */
#define PROTO_FUNC_ERR_NO_RESPONSE        0x03   /**< protocol specific function executed but resonse should be omitted */

//@}

/*---------------------------------------------------------------------------*/

/** \anchor IoDataHandling @name IO data handling
 *  Specifies how the IO data are handled for the first input and output unit. */
//@{
#define IO_DATA_HANDLING_RUNNINGLIGHT   (1)   /**< this is the default behaviour that inplements, together with the
                                                   sample PLC program a simple running ligth */
#define IO_DATA_HANDLING_MIRROR         (0)   /**< The data of the first output unit are directly mirrored to the first input
                                                   unit in the demo_output_data_callback(). This can be used to measure the round-trip
                                                   delay from output to input. All other configured units are not handled */
//@}

/*---------------------------------------------------------------------------*/

/** \anchor appl_def_flashconf @name FLASH Configuration Flags */
//@{
#ifndef FLASH_MAGIC_NUMBER
  #define FLASH_MAGIC_NUMBER             0xA55A                /**< Magic number to mark data in flash as valid */
#endif

#ifndef FLASH_APPL_CONFIG_VERSION
  #define FLASH_APPL_CONFIG_VERSION      0x0101                /**< V1.1 Current version of application config stored in flash */
#endif
#ifndef FLASH_BOOT_CONFIG_VERSION
  #define FLASH_BOOT_CONFIG_VERSION      0x0100                /**< V1.0 Current version of hardware config stored in flash */
#endif

/*---------------------------------------------------------------------------*/

#define FLAG_CONF_APPL_USE_DHCP                         0x00000001  /**< Application uses DHCP to obtain the IP settings */
#define FLAG_CONF_STACK_USE_DHCP                        0x00000002  /**< Protocol stack uses DHCP to obtain the IP settings */
#define FLAG_CONF_STACK_PORT1_DISABLE_AUTO_NEGOTIATION  0x00000004  /**< Disables auto-negotiation. The settings indicated by the speed and duplex mode flags will be used. */
#define FLAG_CONF_STACK_PORT1_FORCE_SPEED_10MHZ         0x00000008  /**< Ethernet speed is forced to 10 MHZ. If not set the speed is forced to 100 MHZ. */
#define FLAG_CONF_STACK_PORT1_DUPLEX_MODE_HD            0x00000010  /**< Ethernet duplex mode is forced to Half Duplex. If not set the duplex mode is forced to Full Duplex. */
#define FLAG_CONF_STACK_DISABLE_EIP_ACD                 0x00000020  /**< Disables the IPv4 address conflict detection used by EtherNet/IP. If not set ACD is enabled. */
#define FLAG_CONF_STACK_ENABLE_EIP_QC                   0x00000080  /**< Enables the Quick Connect feature of EtherNet/IP. If not set Quick Connect is disabled. */
#define FLAG_CONF_STACK_PORT2_DISABLE_AUTO_NEGOTIATION  0x00000100  /**< Disables auto-negotiation. The settings indicated by the speed and duplex mode flags will be used. */
#define FLAG_CONF_STACK_PORT2_FORCE_SPEED_10MHZ         0x00000200  /**< Ethernet speed is forced to 10 MHZ. If not set the speed is forced to 100 MHZ. */
#define FLAG_CONF_STACK_PORT2_DUPLEX_MODE_HD            0x00000400  /**< Ethernet duplex mode is forced to Half Duplex. If not set the duplex mode is forced to Full Duplex. */

#define FLAG_MAIL_CONECTION_STATE                 0x00000001  /**< A mail is send when a change in the connection state occurs */
#define FLAG_MAIL_CONFIG_STATE                    0x00000002  /**< A mail is send when the configuration settings have been changed */
#define FLAG_MAIL_FATAL_ERROR                     0x00000004  /**< A mail is send when a fatal error (exception) has been occured */
#define FLAG_MAIL_DIAG_STATE                      0x00000008  /**< A mail is send when a diagnosis is pending (PROFINET IO only) */

#define USER_PW_LENGTH                            20          /**< The maximum length of the user name and password */

#define EPL_SDO_SEQU_LAYER_TIMEOUT_DEFAULT_VALUE     5000     /**< Powerlink specific. Must be equal to the default values specified within the XDD file */
//@}

/*---------------------------------------------------------------------------*/

/** \anchor appl_def_maillimit @name Mail limitations */
//@{
#define SMTP_MAX_SENDER_LENGTH                    40          /**< maximum length of the sender mail address */
#define SMTP_MAX_RECEIVER_LENGTH                  100         /**< maximum length of the receiver mail address list */
#define SMTP_MAX_SUBJECT_LENGTH                   100         /**< maximum length of subject field for one mail */
#define SMTP_MAX_BODY_LENGTH                      400         /**< maximum length of mail body for one mail */

#define SMTP_MAX_PENDING_MAILS                    15          /**< maximum number of pending mail messages */
//@}

/*---------------------------------------------------------------------------*/

/** \anchor appl_def_firmware @name Firmware handling */
//@{
#define FW_ACTION_ERASE             0x01u
#define FW_ACTION_WRITE             0x02u
#define FW_ACTION_VERIFY            0x03u
#define FW_ACTION_FINISH            0x04u
//@}

/*---------------------------------------------------------------------------*/

/** \anchor appl_def_aux @name Auxiliary defines */
//@{
#define _NUMBER_ARRAY_ELEMENTS(Array)           (sizeof (Array) / sizeof ((Array) [0]))   /**< Returns the number of elements of the Array*/

#ifndef _MAX
  #define _MAX(A,B)                             ((A) > (B) ? (A) : (B))   /**< Returns the bigger value of A and B */
#endif

#ifndef _MIN
  #define _MIN(A,B)                             ((A) < (B) ? (A) : (B))   /**< Returns the smaller value of A and B */
#endif

/*---------------------------------------------------------------------------*/

#define _U32_HIGH_WORD(U32)                     ((U16) ((U32) >> 16))   /**< Returns the high word of the 32 bit parameter */
#define _U32_LOW_WORD(U32)                      ((U16)  (U32)       )   /**< Returns the low word of the 32 bit parameter */

#define _U16_HIGH_BYTE(Value16)                 ((U8) ((Value16) >> 8)) /**< Returns the high byte of the 16 bit parameter */
#define _U16_LOW_BYTE(Value16)                  ((U8) (Value16))        /**< Returns the low byte of the 16 bit parameter */

#define _HIGH_LOW_WORDS_TO_U32(High,Low)        ((U32)((((U32) (High)) << 16) + (Low)))   /**< Returns the combined 32 bit value of the 16 bit parameters High and Low */
#define _HIGH_LOW_BYTES_TO_U16(High,Low)        ((U16)((((U16) (High)) << 8 ) + (Low)))   /**< Returns the combined 16 bit value of the 8 bit parameters High and Low */

/*---------------------------------------------------------------------------*/

#define _ALIGN_TO_16BIT(Type,Value)             ((Type) ((((Type)(Value)) + (Type) 15) & ((Type)(0xFFF0))))
#define _ALIGN_TO_32BIT(Type,Value)             ((Type) ((((Type)(Value)) + (Type) 31) & ((Type)(0xFFE0))))

/*---------------------------------------------------------------------------*/

#define _BIT_LENGTH_TO_BYTE_LENGTH(BitLength)   ((((U8) BitLength) + 7) >> 3)
#define _BIT_OFFSET_TO_BYTE_OFFSET(BitOffset)   (BitOffset >> 3)

/*---------------------------------------------------------------------------*/

#define _MOD_SUPPORTED(ModNum)                  ((U32) (1<<ModNum))
#define _IS_BIT_SET(Num,BitNr)                  ((BOOL) ((Num>>BitNr) & 0x01))
//@}
//@}

/******************************************************************************
TYPEDEFS
******************************************************************************/

/** \addtogroup demo_appl_structures */
//@{
/** \brief This structure holds the POWERLINK specific configuration data in flash */
typedef struct _T_CONFIG_EPL_DATA_APPL
{
  U8                            EplFlags;
  U8                            EplNodeId;                               /**< The Node ID of the device. Unique identification number within a Powerlink network. The Node ID
                                                                              also specifies the Host ID of the IP address. The Net ID of the IP address is fixed to 192.168.100.
                                                                              Valid values: SDAI_EPL_CN_NODE_ID_MIN_VALUE - SDAI_EPL_CN_NODE_ID_MAX_VALUE */
  U32                           SdoSequLayerTimeout;                     /**< Timeout value in milliseconds for the connection abort recognition of the SDO sequence layer */
  char                          HostName [SDAI_EPL_HOST_NAME_MAX_LEN];   /**< DNS host name of type VISIBLE_STRING32 (see Powerlink specification for details). */
  struct SDAI_EPL_CONFIG_DATA   CfgData;                                 /**< Configuration date */

} T_CONFIG_EPL_DATA_APPL;

/** \brief This structure holds the ident and maintenance (I&M) specific configuration data in flash */
typedef struct _T_CONFIG_IDENT_AND_MAINT_DATA
{
  U8   Function [32];
  U8   Location [22];
  U8   Date [16];
  U8   Description [54];

} T_CONFIG_IDENT_AND_MAINT_DATA;

/*---------------------------------------------------------------------------*/

/** \brief This structure holds the configuration data in flash including e.g. the IP Settings and selected protocol */
typedef struct _T_CONFIG_DATA_APPL
{
  U16                             MagicNumber;                                /**< A magic number to mark the data in flash as valid (0xA55A) */

  U8                              BackEnd;                                    /**< The currently selected protocol */
  U8                              BootloaderFlags;                            /**< For future use */

  U16                             Version;                                    /**< The Version of the data stored in flash */

  U32                             NrWriteCycles;                              /**< The number of write cycles */

  U8                              IpAddress[4];                               /**< The IP Address used by the processor */
  U8                              Netmask[4];                                 /**< The Netmask used by the processor */
  U8                              Gateway[4];                                 /**< The Gateway used by the processor */

  /* reserve last byte for '\0' */
  U8                              DeviceName[SDAI_DEVNAME_MAX_LEN + 1];       /**< The current device name */

  U8                              DNSServer[4];                               /**< Currently selected DNS server address */
  U8                              NTPServer[4];                               /**< Currently selected NTP server address */
  U8                              SMTPServer[4];                              /**< Currently selected SMTP server address */

  U32                             ConfigFlags;                                /**< Flags concerning the application configuration. Valid values: \ref appl_def_flashconf */

  /* reserve last byte for '\0' */
  U8                              SMTPSender[SMTP_MAX_SENDER_LENGTH + 1];     /**< Current SMTP sender address used for mails */
  U8                              SMTPReceiver[SMTP_MAX_RECEIVER_LENGTH + 1]; /**< Current SMTP receiver address list used for mails. Each address is separated by a comma. */
  U8                              SMTPSubject[SMTP_MAX_SUBJECT_LENGTH + 1];   /**< For future use */

  U32                             MailFlags;                                  /**< Flags concerning the mail configuration. Valid values: \ref appl_def_flashconf */

  /* reserve last byte for '\0' */
  U8                              ConfigUsername[USER_PW_LENGTH + 1];         /**< Current user name for configuration rights */
  U8                              ConfigPw[USER_PW_LENGTH + 1];               /**< Current password for configuration rights */

  U8                              AdminUsername[USER_PW_LENGTH + 1];          /**< Current user name for administration rights */
  U8                              AdminPw[USER_PW_LENGTH + 1];                /**< Current password for administration rights */

  /* PROFINET specific */
  U8                              AddressOffset;                              /**< Address Offset of the device */

  /* Powerlink specific */
  T_CONFIG_EPL_DATA_APPL          EplData;

  T_CONFIG_IDENT_AND_MAINT_DATA   IdentMaintenanceData;

} T_CONFIG_DATA_APPL;

/*---------------------------------------------------------------------------*/

/** \brief This structure holds the hardware configuration data in flash */
typedef struct _T_CONFIG_DATA_HW
{
  U16 MagicNumber;          /**< A magic number to mark the data in flash as valid (0xA55A) */

  U16 HwType;               /**< A number identifieing the hardware type */

  U32 NrWriteCycles;        /**< Number of write cycles */

  U32 SerialNumber;         /**< Serial number */
  U8  HwVersion[2];         /**< Hardware version */
  U8  HwIdent[18];          /**< Hardware ident string */
  U16 HwFreigaben;          /**< Unused */
  U8  HwSecString1[16];     /**< Unused */
  U8  HwReserved1[8];       /**< Unused */
  U8  HwSecString2[16];     /**< Unused */
  U16 UserLength;           /**< Length of the user data following */

  /* User Data */

  U16 Version;              /**< Version */
  U16 UserReserved;         /**< Reserved */

  U32 Bootcode;             /**< Unique Number identifing the hardware. Used to check firmware image */

  U8  BootVersion[20];      /**< Version number of the boot loader */

  U8  MacAddress[6];        /**< The MAC address */
  U8  MacAddressPort1[6];   /**< The MAC address Port 0 */
  U8  MacAddressPort2[6];   /**< The MAC address Port 1 */

} T_CONFIG_DATA_HW;

/*---------------------------------------------------------------------------*/

/** \brief Local instance of one I/O unit */
struct T_DEMO_UNIT
{
  U32                      Id;                 /**< The unique Id of the unit */

  U8                       Type;               /**< The type of the unit */

  U8                       InputSize;          /**< The input data size of the unit */
  U8                       OutputSize;         /**< The output data size of the unit */

  U8                       InputState;         /**< The input data state of the unit */
  U8                       OutputState;        /**< The output data state of the unit */

  U8                       DataChanged;        /**< Indicates that the I/O data has been changed */

  U8*                      pInputData;         /**< Pointer to the IO data of the unit */
  U8*                      pOutputData;        /**< Pointer to the IO data of the unit */

  struct SDAI_CFG_DATA_EXT CfgData;            /**< SDAI specific configuration data */

  void*                    pProtocolData;      /**< Protocol specific Unit data */
};

/*---------------------------------------------------------------------------*/

/** \brief This structure holds and manages the local copies of the I/O units */
struct T_DEMO_UNITS_MANAGEMENT
{
  U8                  (*proto_set_output_data) (struct T_DEMO_UNIT*);            /**< Is used to avoid a context switch in case of update outputs */
  U8                  (*proto_get_input_data)  (struct T_DEMO_UNIT*);            /**< Is used to avoid a context switch in case of update inputs */

  struct T_DEMO_UNIT* pFirstInputUnit;                                           /**< The first configured input unit */
  struct T_DEMO_UNIT* pFirstOutputUnit;                                          /**< The first configured output unit */

  struct T_DEMO_UNIT  UnitList [SDAI_MAX_UNITS];                                 /**< Array holding the units */

  U8                  IOData [(2 * SDAI_MAX_UNITS * SDAI_MAX_UNIT_IO_DATA_SIZE)];/**< Array holding a local copy of the IO data of the units */
};

/*---------------------------------------------------------------------------*/

/** \brief This structure holds the data of a received write indication */
struct T_DEMO_WRITE_IND
{
  BOOL                      InUse;                     /**< Flag to mark buffer as in use */
  struct SDAI_WRITE_IND     Header;                    /**< Write indication header */
  U8                        Data [SDAI_SERVICE_SIZE];  /**< Write indication data */
};

/** \brief This structure holds the data of a received read indication */
struct T_DEMO_READ_IND
{
  BOOL                      InUse;        /**< Flag to mark buffer as in use */
  struct SDAI_READ_IND      Header;       /**< Read indication header */
};

/** \brief This structure holds the data of a received control indication */
struct T_DEMO_CONTROL_IND
{
  BOOL                      InUse;                    /**< Flag to mark buffer as in use */
  struct SDAI_CONTROL_IND   Header;                   /**< Control indication header */
  U8                        Data [SDAI_SERVICE_SIZE]; /**< Control indication data */
};

/*---------------------------------------------------------------------------*/

/** \brief This structure holds the data for a write response */
struct T_DEMO_WRITE_RES
{
  BOOL                      Pending;                   /**< Flag to mark buffer as pending */
  struct SDAI_WRITE_RES     Header;                    /**< Read response header */
};


/** \brief This structure holds the data for a read response */
struct T_DEMO_READ_RES
{
  BOOL                      Pending;                   /**< Flag to mark buffer as pending */
  struct SDAI_READ_RES      Header;                    /**< Read response header */
  U8                        Data [SDAI_SERVICE_SIZE];  /**< Read response data */
};

/** \brief This structure holds the data of a received control indication */
struct T_DEMO_CONTROL_RES
{
  BOOL                      Pending;                   /**< Flag to mark buffer as pending */
  struct SDAI_CONTROL_RES   Header;                    /**< Control indication header */
  U8                        Data [SDAI_SERVICE_SIZE];  /**< Control indication data */
};

/*---------------------------------------------------------------------------*/

/** \brief This structure holds the data of a received ident indication */
struct T_DEMO_IDENT_DATA
{
  BOOL                      InUse;          /**< Flag to mark buffer as in use */
  struct SDAI_IDENT_DATA    IdentData;      /**< Identification data */
};

/** \brief This structure holds the data of a received exception indication */
struct T_DEMO_EXCEPTION_DATA
{
  BOOL                        InUse;                                  /**< Flag to mark buffer as in use */
  struct SDAI_EXCEPTION_DATA  SdaiExceptionData;                      /**< SDAI specific Exception data */
};

/** \brief This structure holds the data for a diagnosis request */
struct T_DEMO_DIAGNOSIS_REQ
{
  struct SDAI_DIAGNOSIS_REQ   DiagReq;       /**< Common diagnosis data */

  union
  {
    struct SDAI_ECAT_DIAGNOSIS_DATA   EcatDiagData; /**< EtherCAT specific diagnosis data */
    struct SDAI_PBDP_DIAGNOSIS_DATA   PbDpDiagData; /**< PROFIBUS DP specific diagnosis data */

  } UseAs;
};


/*---------------------------------------------------------------------------*/

/** \brief This structure holds management data and status information
 */
struct T_DEMO_MANAGEMENT_DATA
{
  struct SDAI_INIT               SdaiInitData;           /**< The initialization data for the SDAI interface */

  struct T_DEMO_UNITS_MANAGEMENT UnitsManagement;        /**< Current unit configuration */
  T_CONFIG_DATA_APPL             ConfigDataAppl;         /**< The current configuration data (flash data) */
  T_CONFIG_DATA_HW               ConfigDataHw;           /**< The current configuration data (flash data) */
  struct SDAI_VERSION_DATA       VersionData;            /**< Version information about the current stack */

  U16                            StackStatus;            /**< Current stack status */
  struct SDAI_ADDSTATUS_DATA     ProtocolStatusData;     /**< Current additional stack status */

  /* callback functions to implement protocol specific actions on certain events */
  U8 (*proto_get_init_data)             (T_CONFIG_DATA_APPL*, struct SDAI_INIT*);
  U8 (*proto_get_unit_data)             (struct T_DEMO_UNITS_MANAGEMENT*);
  U8 (*proto_update_ident_data)         (struct T_DEMO_IDENT_DATA*);
  U8 (*proto_cyclic_timer)              (void);
  U8 (*proto_button_event)              (U8);
  U8 (*proto_get_input_data)            (struct T_DEMO_UNIT*);
  U8 (*proto_set_output_data)           (struct T_DEMO_UNIT*);
  U8 (*proto_process_write_ind)         (struct T_DEMO_WRITE_IND*, struct T_DEMO_WRITE_RES*);
  U8 (*proto_process_read_ind)          (struct T_DEMO_READ_IND*, struct T_DEMO_READ_RES*);
  U8 (*proto_process_control_ind)       (struct T_DEMO_CONTROL_IND*, struct T_DEMO_CONTROL_RES*);
  U8 (*proto_process_additional_status) (struct SDAI_ADDSTATUS_DATA*);
  U8 (*proto_process_alarm_ack)         (struct SDAI_ALARM_ACK_IND*);
};
//@}

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

/** \addtogroup demo_appl_functions */
//@{
extern void                 demo_task_main_function                     (void);
extern struct T_DEMO_UNIT*  demo_find_unit                              (U32);
extern U8*                  demo_get_ip_address                         (void);
extern void                 demo_application_send_event                 (U32);
extern void                 demo_print_error                            (U8);
extern const char*          demo_print_sdai_error_text                  (void);
extern void                 demo_update_input_data                      (void) _LOCATED_INTO_INSTRUCTION_FAST_MEMORY;
//@}

/*---------------------------------------------------------------------------*/

/** \addtogroup socket_func */
//@{
extern void                 socket_init                                 (void);
extern void                 socket_term                                 (void);
extern void                 socket_process_events                       (U32);
//@}

/*---------------------------------------------------------------------------*/

/** \addtogroup util_func */
//@{
extern U8                   firmware_update                             (U8*, U32);
//@}

/*---------------------------------------------------------------------------*/

/** \addtogroup pn_func */
//@{
extern U8                   profinet_get_unit_data                      (struct T_DEMO_UNITS_MANAGEMENT*);
extern U8                   profinet_get_init_data                      (T_CONFIG_DATA_APPL*, struct SDAI_INIT*);
extern U8                   profinet_cyclic_timer                       (void);
extern U8                   profinet_button_event                       (U8);
extern U8                   profinet_print_additional_status_data       (struct SDAI_ADDSTATUS_DATA*);
extern U8                   profinet_get_input_data                     (struct T_DEMO_UNIT*) _LOCATED_INTO_INSTRUCTION_FAST_MEMORY;
extern U8                   profinet_set_output_data                    (struct T_DEMO_UNIT*) _LOCATED_INTO_INSTRUCTION_FAST_MEMORY;
extern U8                   profinet_process_control_ind                (struct T_DEMO_CONTROL_IND*, struct T_DEMO_CONTROL_RES*);
extern U8                   profinet_process_read_ind                   (struct T_DEMO_READ_IND*, struct T_DEMO_READ_RES*);
extern U8                   profinet_process_write_ind                  (struct T_DEMO_WRITE_IND*, struct T_DEMO_WRITE_RES*);
extern U8                   profinet_update_ident_data                  (struct T_DEMO_IDENT_DATA*);
extern U8                   profinet_send_diagnosis                     (U8, U16, U16);
extern const char*          profinet_print_status_text                  (void);
extern void                 profinet_add_unit                           (struct T_DEMO_UNITS_MANAGEMENT*, U8, U8, U8, U32, U32, U32, U8, U8, U16, U16);
extern U8                   profinet_pull_unit                          (struct T_DEMO_UNITS_MANAGEMENT*, U32);
extern U8                   profinet_plug_unit                          (struct T_DEMO_UNITS_MANAGEMENT*, U8, U8, U8, U32, U32, U32, U8, U8);
//@}

/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/

/** \addtogroup platform_functions Functions */
//@{
extern void                 platform_visualize_data                     (struct T_DEMO_MANAGEMENT_DATA*);
extern void                 platform_test_leds                          (void);
extern U8                   platform_get_buttons                        (void);
extern U16                  platform_get_dip_switch                     (void);
extern void                 platform_get_hw_data                        (struct SDAI_INIT*);
extern void                 platform_get_cpu_load                       (U8*, U8*, U8*);
extern void                 platform_send_event                         (U32);
extern U32                  platform_wait_event                         (U32);
extern void                 platform_lock                               (void);
extern void                 platform_unlock                             (void);
extern void                 platform_busy_sleep                         (U32);
extern U8                   platform_read_flash_config                  (T_CONFIG_DATA_APPL*, T_CONFIG_DATA_HW*);
extern U8                   platform_write_flash_config                 (T_CONFIG_DATA_APPL*);
extern U8                   platform_handle_firmware_segment            (U8, U8, U32, U8*);
extern void                 platform_configure_isochronous_unit         (U8, U32, U32);
//@}

/******************************************************************************
GLOBAL DATA
******************************************************************************/

extern struct T_DEMO_MANAGEMENT_DATA    ApplManagement;
extern U8                               Axis0Reference;

#endif /* __DEMO_H__ */

