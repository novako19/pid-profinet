/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

#include <stdio.h>
#include <string.h>

#include "sdai.h"

#include "demo_platform.h"
#include "demo.h"

/******************************************************************************
DEFINES
******************************************************************************/

/** \addtogroup pn_def */
//@{
/** \anchor pn_def_unit @name Unit handling */
//@{
#define PN_UNIT_FLAG_DIAGNOSIS_PENDING          0x00000001
//@}

/*---------------------------------------------------------------------------*/

/** \anchor pn_def_led @name LED */
//@{
#define PN_LED_MODULE                           (LED_PROTOCOL_GREEN2)
#define PN_LED_MODULE_ERR                       (LED_PROTOCOL_YELLOW1)

#define PN_LED_SIGNAL                           (LED_PROTOCOL_GREEN1 | LED_PROTOCOL_RED1)
//@}

/*---------------------------------------------------------------------------*/

/** \anchor pn_def_iso @name Isochronous */
//@{
#define PN_ISOCHRONOUS_DC_BASE_NANOSECONDS      31500

/* These values must be the same as in the GSDML file */
#define PN_ISOCHRONOUS_T_DC_BASE                1
#define PN_ISOCHRONOUS_T_IO_BASE                1000
//@}

/*---------------------------------------------------------------------------*/

/** \anchor pn_def_dev @name Device type */
//@{
#define PN_VENDOR_ID_SOFTING     0x0117
#define PN_SOFTWARE_REVISION     0x56020100
#define PN_HARDWARE_REVISION     0x0100
//@}

/*---------------------------------------------------------------------------*/

/** \anchor pn_def_acyclic @name Acyclic */
//@{
#define PROFINET_CONFIGURATION_DATA_MAX_SIZE    4u

/* Application defined indices */
#define PN_INDEX_CONFIGURATION_DATA                   (U16) 0x0001u
#define PN_INDEX_COMMAND                              (U16) 0x7FFFu

/* PROFINET IO defined indices */
#define PN_INDEX_SUBSLOT_ISOCHRONOUS_MODE_DATA        (U16) 0x8030u
#define PN_INDEX_IDENT_AND_MAINTENANCE_0              (U16) 0xAFF0u
#define PN_INDEX_IDENT_AND_MAINTENANCE_1              (U16) 0xAFF1u
#define PN_INDEX_IDENT_AND_MAINTENANCE_2              (U16) 0xAFF2u
#define PN_INDEX_IDENT_AND_MAINTENANCE_3              (U16) 0xAFF3u
#define PN_INDEX_IDENT_AND_MAINTENANCE_FILTER         (U16) 0xF840u

/* PROFINET IO defined block types */
#define PN_BLOCK_TYPE_IDENT_AND_MAINTENANCE_0         (U16) 0x0020u
#define PN_BLOCK_TYPE_IDENT_AND_MAINTENANCE_1         (U16) 0x0021u
#define PN_BLOCK_TYPE_IDENT_AND_MAINTENANCE_2         (U16) 0x0022u
#define PN_BLOCK_TYPE_IDENT_AND_MAINTENANCE_3         (U16) 0x0023u
#define PN_BLOCK_TYPE_IM0_FILTER_SUBMODULE            (U16) 0x0030u
#define PN_BLOCK_TYPE_IM0_FILTER_MODULE               (U16) 0x0031u
#define PN_BLOCK_TYPE_IM0_FILTER_DEVICE               (U16) 0x0032u
#define PN_BLOCK_TYPE_ISOCHRONOUS_MODE_DATA           (U16) 0x0204u
//@}

/*---------------------------------------------------------------------------*/

/** \anchor pn_def_alarm @name Alarm/Diagnosis */
//@{
#define PROFINET_ALARM_USER_STRUCTURE_IDENT_SOE                         0x8300u

#define PROFINET_ALARM_CHANNEL_DIAG_CHANNEL_NUMBER_SUBSLOT              0x8000u

#define PROFINET_ALARM_CHANNEL_PROPERTIES_ACCUMULATIVE                  0x0100u
#define PROFINET_ALARM_CHANNEL_PROPERTIES_MAINTENANCE_DIAGNOSIS         0x0000u
#define PROFINET_ALARM_CHANNEL_PROPERTIES_MAINTENANCE_REQUIRED          0x0200u
#define PROFINET_ALARM_CHANNEL_PROPERTIES_MAINTENANCE_DEMANDED          0x0400u
#define PROFINET_ALARM_CHANNEL_PROPERTIES_MAINTENANCE_QUALIFIED         0x0600u
#define PROFINET_ALARM_CHANNEL_PROPERTIES_DIAG_ALL_DISAPPEAR            0x0000u
#define PROFINET_ALARM_CHANNEL_PROPERTIES_DIAG_APPEAR                   0x0800u
#define PROFINET_ALARM_CHANNEL_PROPERTIES_DIAG_DISAPPEAR                0x1000u
#define PROFINET_ALARM_CHANNEL_PROPERTIES_DIAG_DISAPPEAR_REMAIN         0x1800u
#define PROFINET_ALARM_CHANNEL_PROPERTIES_DIRECTION_MANUFACTURER        0x0000u
#define PROFINET_ALARM_CHANNEL_PROPERTIES_DIRECTION_IN                  0x2000u
#define PROFINET_ALARM_CHANNEL_PROPERTIES_DIRECTION_OUT                 0x4000u
#define PROFINET_ALARM_CHANNEL_PROPERTIES_DIRECTION_IN_OUT              (PROFINET_ALARM_CHANNEL_PROPERTIES_DIRECTION_IN | \
                                                                        PROFINET_ALARM_CHANNEL_PROPERTIES_DIRECTION_OUT)

#define PROFINET_CH_ERR_TYPE_UPPER_LIMIT_EXCEEDED                       0x0007u
#define PROFINET_CH_ERR_TYPE_LOWER_LIMIT_EXCEEDED                       0x0008u
#define PROFINET_CH_ERR_TYPE_POWER_SUPPLY_FAULT                         0x0011u
#define PROFINET_CH_ERR_TYPE_SAFETY_EVENT                               0x0019u
#define PROFINET_CH_ERR_TYPE_EXTERNAL_FAULT                             0x001Au
#define PROFINET_CH_ERR_TYPE_TEMPORARY_FAULT                            0x001Fu

#define PROFINET_EXT_CH_ERR_TYPE_ACCUMULATIVE_INFO                      0x8000u

#define PROFIDRIVE_CH_ERR_TYPE_INTERNAL_COMMUNICATION_FAULT             0x900Bu

/*---------------------------------------------------------------------------*/

#define SDAI_DEFAULT_CHANNEL_PROP_DIAGNOSIS     (PROFINET_ALARM_CHANNEL_PROPERTIES_MAINTENANCE_DIAGNOSIS | \
                                                PROFINET_ALARM_CHANNEL_PROPERTIES_DIAG_APPEAR)
#define SDAI_DEFAULT_CHANNEL_PROP_M_REQUIRED    (PROFINET_ALARM_CHANNEL_PROPERTIES_MAINTENANCE_REQUIRED | \
                                                PROFINET_ALARM_CHANNEL_PROPERTIES_DIAG_APPEAR)
#define SDAI_DEFAULT_CHANNEL_PROP_M_DEMANDED    (PROFINET_ALARM_CHANNEL_PROPERTIES_MAINTENANCE_DEMANDED | \
                                                PROFINET_ALARM_CHANNEL_PROPERTIES_DIAG_APPEAR)
#define SDAI_DEFAULT_CHANNEL_PROP_M_QUALIFIED   (PROFINET_ALARM_CHANNEL_PROPERTIES_MAINTENANCE_QUALIFIED | \
                                                PROFINET_ALARM_CHANNEL_PROPERTIES_DIAG_APPEAR)
//@}
//@}

/******************************************************************************
TYPEDEFS
******************************************************************************/

/** \addtogroup pn_struct */
//@{
/** \brief This structure describes the structure of the block header of Profinet read/write services.
           For a detailed description of the structure parameters see "IEC61158 Part5/6". */
typedef struct _T_PN_BLOCK_HEADER
{
  U8   TypeHighByte;
  U8   TypeLowByte;

  U8   LengthHighByte;
  U8   LengthLowByte;

  U8   VersionHighByte;
  U8   VersionLowByte;

} PACK_BYTE_ALIGNMENT (T_PN_BLOCK_HEADER);

/** \brief This structure describes the structure of the Profinet Index Isochronous Mode Data. */
typedef struct _T_PN_ISOCHRONOUS_MODE_DATA
{
  T_PN_BLOCK_HEADER   Header;

  U8                  Padding1;
  U8                  Padding2;

  U8                  SlotNumberHighByte;
  U8                  SlotNumberLowByte;

  U8                  SubslotNumberHighByte;
  U8                  SubslotNumberLowByte;

  U8                  ControllerApplicationCycleFactorHighByte;
  U8                  ControllerApplicationCycleFactorLowByte;

  U8                  TimeDataCycleHighByte;
  U8                  TimeDataCycleLowByte;

  U8                  TimeIOInputHighWordHighByte;
  U8                  TimeIOInputHighWordLowByte;
  U8                  TimeIOInputLowWordHighByte;
  U8                  TimeIOInputLowWordLowByte;

  U8                  TimeIOOutputHighWordHighByte;
  U8                  TimeIOOutputHighWordLowByte;
  U8                  TimeIOOutputLowWordHighByte;
  U8                  TimeIOOutputLowWordLowByte;

  U8                  TimeIOInputValidHighWordHighByte;
  U8                  TimeIOInputValidHighWordLowByte;
  U8                  TimeIOInputValidLowWordHighByte;
  U8                  TimeIOInputValidLowWordLowByte;

  U8                  TimeIOOutputValidHighWordHighByte;
  U8                  TimeIOOutputValidHighWordLowByte;
  U8                  TimeIOOutputValidLowWordHighByte;
  U8                  TimeIOOutputValidLowWordLowByte;

} PACK_BYTE_ALIGNMENT (T_PN_ISOCHRONOUS_MODE_DATA);

/** \brief This structure describes the structure of the Profinet I&M0 FilterDataInfo for THIS sample application. The coding
           of this structure depends on the module/submodule configuration and which of the modules have I&M data.
           For a detailed description see "IEC61158 Part5/6" and "Profile Guidelines Part1: Identification & Maintenance Functions. */
typedef struct _T_PN_IDENT_MAINTENANCE_0_FILTERDATAINFO
{
  T_PN_BLOCK_HEADER   Header;

  U8                  NumberApiHighByte;
  U8                  NumberApiLowByte;

  U8                  NumberApiHighWordHighByte;
  U8                  NumberApiHighWordLowByte;
  U8                  NumberApiLowWordHighByte;
  U8                  NumberApiLowWordLowByte;

  U8                  NumberModulsHighByte;
  U8                  NumberModulsLowByte;

  U8                  SlotNumberHighByte;
  U8                  SlotNumberLowByte;

  U8                  SlotIdentnumberHighWordHighByte;
  U8                  SlotIdentnumberHighWordLowByte;
  U8                  SlotIdentnumberLowWordHighByte;
  U8                  SlotIdentnumberLowWordLowByte;

  U8                  NumberSubmodulesHighByte;
  U8                  NumberSubmodulesLowByte;

  U8                  SubSlotNumberHighByte;
  U8                  SubSlotNumberLowByte;

  U8                  SubSlotIdentnumberHighWordHighByte;
  U8                  SubSlotIdentnumberHighWordLowByte;
  U8                  SubSlotIdentnumberLowWordHighByte;
  U8                  SubSlotIdentnumberLowWordLowByte;

} PACK_BYTE_ALIGNMENT (T_PN_IDENT_MAINTENANCE_0_FILTERDATAINFO);

/*---------------------------------------------------------------------------*/

/** \brief This structure describes the structure of the Profinet I&M_0 read/write index.
           For a detailed description of the structure parameters see "Profile Guidelines Part1: Identification & Maintenance Functions
           and "IEC61158 Part5/6". */
typedef struct _T_PN_IDENT_MAINTENANCE_0
{
  T_PN_BLOCK_HEADER   Header;

  U8                  VendorIdHighByte;
  U8                  VendorIdLowByte;

  U8                  OrderId [20];
  U8                  SerialNumber [16];

  U8                  HwRevisionHighByte;
  U8                  HwRevisionLowByte;

  U8                  SwRevisionHighWordHighByte;
  U8                  SwRevisionHighWordLowByte;
  U8                  SwRevisionLowWordHighByte;
  U8                  SwRevisionLowWordLowByte;

  U8                  RevisionCounterHighByte;
  U8                  RevisionCounterLowByte;

  U8                  ProfileIdHighByte;
  U8                  ProfileIdLowByte;

  U8                  ProfileTypeHighByte;
  U8                  ProfileTypeLowByte;

  U8                  VersionHighByte;
  U8                  VersionLowByte;

  U8                  SupportedHighByte;
  U8                  SupportedLowByte;

} PACK_BYTE_ALIGNMENT (T_PN_IDENT_MAINTENANCE_0);

/*---------------------------------------------------------------------------*/

/** \brief This structure describes the structure of the Profinet I&M_1 read/write index.
           For a detailed description of the structure parameters see "Profile Guidelines Part1: Identification & Maintenance Functions
           and "IEC61158 Part5/6". */
typedef struct _T_PN_IDENT_MAINTENANCE_1
{
  T_PN_BLOCK_HEADER   Header;

  U8                  Function [32];
  U8                  Location [22];

} PACK_BYTE_ALIGNMENT (T_PN_IDENT_MAINTENANCE_1);

/*---------------------------------------------------------------------------*/

/** \brief This structure describes the structure of the Profinet I&M_2 read/write index.
           For a detailed description of the structure parameters see "Profile Guidelines Part1: Identification & Maintenance Functions
           and "IEC61158 Part5/6". */
typedef struct _T_PN_IDENT_MAINTENANCE_2
{
  T_PN_BLOCK_HEADER   Header;

  U8                  Date [16];

} PACK_BYTE_ALIGNMENT (T_PN_IDENT_MAINTENANCE_2);

/*---------------------------------------------------------------------------*/

/** \brief This structure describes the structure of the Profinet I&M_3 read/write index.
           For a detailed description of the structure parameters see "Profile Guidelines Part1: Identification & Maintenance Functions
           and "IEC61158 Part5/6". */
typedef struct _T_PN_IDENT_MAINTENANCE_3
{
  T_PN_BLOCK_HEADER   Header;

  U8                  Description [54];

} PACK_BYTE_ALIGNMENT (T_PN_IDENT_MAINTENANCE_3);

/*---------------------------------------------------------------------------*/

/** \brief This structure describes the structure of the Profinet Sequence of Events timestamp.
           For a detailed description of the structure parameters see the common profile specification SOE. */
typedef struct _T_PN_SOE_TIMESTAMP
{
  U8   StatusHighByte;
  U8   StatusLowByte;

  U8   SecondHighHighByte;
  U8   SecondHighLowByte;

  U8   SecondLowHighWordHighByte;
  U8   SecondLowHighWordLowByte;
  U8   SecondLowLowWordHighByte;
  U8   SecondLowLowWordLowByte;

  U8   NanosecondsHighWordHighByte;
  U8   NanosecondsHighWordLowByte;
  U8   NanosecondsLowWordHighByte;
  U8   NanosecondsLowWordLowByte;

} PACK_BYTE_ALIGNMENT (T_PN_SOE_TIMESTAMP);

/*---------------------------------------------------------------------------*/

/** \brief This structure describes the structure of the Profinet Sequence of Events source of the event.
           For a detailed description of the structure parameters see the common profile specification SOE. */
typedef struct _T_PN_SOE_EVENT_SOURCE
{
  U8   APIHighWordHighByte;
  U8   APIHighWordLowByte;
  U8   APILowWordHighByte;
  U8   APILowWordLowByte;

  U8   SlotHighByte;
  U8   SlotLowByte;

  U8   SubslotHighByte;
  U8   SubslotLowByte;

  U8   DirectionHighByte;
  U8   DirectionLowByte;

} PACK_BYTE_ALIGNMENT (T_PN_SOE_EVENT_SOURCE);

/*---------------------------------------------------------------------------*/

/** \brief This structure describes the structure of the Profinet Sequence of Events extended info.
           For a detailed description of the structure parameters see the common profile specification SOE. */
typedef struct _T_PN_SOE_EXT_INFO
{
  U8   TypeHighByte;
  U8   TypeLowByte;

  U8   LengthHighByte;
  U8   LengthLowByte;

  U8   ValueHighByte;
  U8   ValueLowByte;

} PACK_BYTE_ALIGNMENT (T_PN_SOE_EXT_INFO);

/*---------------------------------------------------------------------------*/

/** \brief This structure describes the structure of the Profinet Sequence of Events data.
           For a detailed description of the structure parameters see the common profile specification SOE. */
typedef struct _T_PN_SOE_ITEM
{
  U8                      BlockTypeHighByte;
  U8                      BlockTypeLowByte;

  U8                      BlockLengthHighByte;
  U8                      BlockLengthLowByte;

  U8                      BlockVersionHighByte;
  U8                      BlockVersionLowByte;

  U16                     Alignment;

  U8                      TypeHighByte;
  U8                      TypeLowByte;

  U8                      LengthHighByte;
  U8                      LengthLowByte;

  T_PN_SOE_TIMESTAMP      TimeStamp;
  T_PN_SOE_EVENT_SOURCE   Source;

  U8                      TagHighByte;
  U8                      TagLowByte;

  U8                      Specifier;
  U8                      Subcode;

  T_PN_SOE_EXT_INFO       ExtInfo;

} PACK_BYTE_ALIGNMENT (T_PN_SOE_ITEM);

/*---------------------------------------------------------------------------*/

/** \brief This structure holds the data for an diagnosis request. */
typedef struct _T_PROFINET_DIAGNOSE_REQ
{
  struct SDAI_DIAGNOSIS_REQ   Header;               /**< Diagnosis request header */

  union
  {
    struct SDAI_PN_PROFINET_CHANNEL_DIAGNOSIS           ChannelDiagnosisData;
    struct SDAI_PN_PROFINET_EXT_CHANNEL_DIAGNOSIS       ExtChannelDiagData;
    struct SDAI_PN_PROFINET_QUALIFIED_CHANNEL_DIAGNOSIS QualifiedChannelDiagData;

  } UseAs;

} T_PROFINET_DIAGNOSE_REQ;

/*---------------------------------------------------------------------------*/

/** \brief This structure holds the data for an alarm request. */
typedef struct _T_PROFINET_ALARM_REQ
{
  struct SDAI_ALARM_REQ   Header;                 /**< Alarm request header */
  U8                      UserStructureIdentHigh;
  U8                      UserStructureIdentLow;

  union
  {
    /* defined in common profile */
    T_PN_SOE_ITEM         SequenceOfEvents;
    U8                    Data;
  } UseAs;

} T_PROFINET_ALARM_REQ;

/*---------------------------------------------------------------------------*/

/** \brief */
typedef struct _T_PN_UNIT_DATA
{
  U32                         Flags;

  U8                          ConfigData [PROFINET_CONFIGURATION_DATA_MAX_SIZE];

  T_PN_ISOCHRONOUS_MODE_DATA  IsochronousData;

} T_PN_UNIT_DATA;
//@}

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

/** \addtogroup pn_func */
//@{
static U8   profinet_send_alarm                   (U8 Unit);
static void profinet_module_led_blink             (U32 Led);
static void profinet_blink                        (void);
//@}

/******************************************************************************
PUBLIC DATA
******************************************************************************/

/******************************************************************************
PRIVATE DATA
******************************************************************************/

static U8                           PnBlink          = 0;    /**< Indicates if the Profinet blinking function has to be executed */

static U8                           AlarmPending     = 0;    /**< Indicates if a Profinet alarm is pending */

static struct SDAI_ADDSTATUS_DATA   TmpAddStatusData;        /**< local copy of the Profinet additional status data */

static T_CONFIG_DATA_APPL*          pFlashConfigData = &ApplManagement.ConfigDataAppl;
static T_CONFIG_DATA_HW*            pFlashHwData     = &ApplManagement.ConfigDataHw;

static U8                           IpAddress [4];

static U8                           NumberUnits;

static T_PN_UNIT_DATA               ProfinetUnitData [SDAI_MAX_UNITS];

T_PROFINET_DIAGNOSE_REQ             Diagnose;
T_PROFINET_ALARM_REQ                Alarm;

/*---------------------------------------------------------------------------*/

static unsigned                  DiagnosisAlarmIndex          = 0u;
static T_PROFINET_DIAGNOSE_REQ   PredefinedDiagnosisAlarms [] =
{
  /* predefined channel diagnosis alarms with different levels of severity -> maintenance required, maintenance demanded and diagnosis */
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_SHORT_CIRCUIT,        .Severity = SDAI_DIAGNOSIS_SEVERITY_FAILURE,              .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_DEVICE_UNDERVOLTAGE,  .Severity = SDAI_DIAGNOSIS_SEVERITY_MAINTENANCE_REQUIRED, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_DEVICE_OVERVOLTAGE,   .Severity = SDAI_DIAGNOSIS_SEVERITY_MAINTENANCE_REQUIRED, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_OVERLOAD,             .Severity = SDAI_DIAGNOSIS_SEVERITY_MAINTENANCE_REQUIRED, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_OVERTEMPERATURE,      .Severity = SDAI_DIAGNOSIS_SEVERITY_MAINTENANCE_REQUIRED, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_LINE_BREAK,           .Severity = SDAI_DIAGNOSIS_SEVERITY_MAINTENANCE_REQUIRED, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_UPPER_LIMIT_EXCEEDED, .Severity = SDAI_DIAGNOSIS_SEVERITY_MAINTENANCE_REQUIRED, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_LOWER_LIMIT_EXCEEDED, .Severity = SDAI_DIAGNOSIS_SEVERITY_MAINTENANCE_REQUIRED, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_GENERIC,              .Severity = SDAI_DIAGNOSIS_SEVERITY_MAINTENANCE_REQUIRED, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_SIMULATION_ACTIVE,    .Severity = SDAI_DIAGNOSIS_SEVERITY_MAINTENANCE_REQUIRED, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_PRM_MISSING,          .Severity = SDAI_DIAGNOSIS_SEVERITY_MAINTENANCE_REQUIRED, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_PRM_FAULT,            .Severity = SDAI_DIAGNOSIS_SEVERITY_OUT_OF_SPECIFICATION, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_POWER_SUPPLY_FAULT,   .Severity = SDAI_DIAGNOSIS_SEVERITY_OUT_OF_SPECIFICATION, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_FUSE_BLOWN,           .Severity = SDAI_DIAGNOSIS_SEVERITY_OUT_OF_SPECIFICATION, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_COMMUNICATION_FAULT,  .Severity = SDAI_DIAGNOSIS_SEVERITY_OUT_OF_SPECIFICATION, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_GROUND_FAULT,         .Severity = SDAI_DIAGNOSIS_SEVERITY_OUT_OF_SPECIFICATION, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_REFERENCE_POINT_LOST, .Severity = SDAI_DIAGNOSIS_SEVERITY_OUT_OF_SPECIFICATION, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_SAMPLING_ERROR,       .Severity = SDAI_DIAGNOSIS_SEVERITY_OUT_OF_SPECIFICATION, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_THRESHOLD_WARNING,    .Severity = SDAI_DIAGNOSIS_SEVERITY_OUT_OF_SPECIFICATION, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_OUTPUT_DISABLED,      .Severity = SDAI_DIAGNOSIS_SEVERITY_OUT_OF_SPECIFICATION, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_SAFETY_EVENT,         .Severity = SDAI_DIAGNOSIS_SEVERITY_OUT_OF_SPECIFICATION, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_EXTERNAL_FAULT,       .Severity = SDAI_DIAGNOSIS_SEVERITY_OUT_OF_SPECIFICATION, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_TEMPORARY_FAULT,      .Severity = SDAI_DIAGNOSIS_SEVERITY_OUT_OF_SPECIFICATION, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = 0 }, },

  /* profile specific channel diagnosis alarms with different levels of severity -> maintenance required, maintenance demanded and diagnosis */
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_PROTOCOL_SPECIFIC, .Severity = SDAI_DIAGNOSIS_SEVERITY_FAILURE, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = (sizeof (struct SDAI_PN_PROFINET_CHANNEL_DIAGNOSIS)), },
    .UseAs  = { .ChannelDiagnosisData = {
                  .UserStructureIdent = SDAI_PN_ALARM_USER_STRUCTURE_IDENT_CHANNEL_DIAG,
                  .ChannelNumber      = PROFINET_ALARM_CHANNEL_DIAG_CHANNEL_NUMBER_SUBSLOT,
                  .ChannelProperties  = SDAI_DEFAULT_CHANNEL_PROP_DIAGNOSIS,
                  .ChannelError       = PROFIDRIVE_CH_ERR_TYPE_INTERNAL_COMMUNICATION_FAULT, } } },

  /* predefined extended channel diagnosis alarms with different levels of severity -> maintenance required, maintenance demanded and diagnosis */
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_PROTOCOL_SPECIFIC, .Severity = SDAI_DIAGNOSIS_SEVERITY_FAILURE, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = (sizeof (struct SDAI_PN_PROFINET_EXT_CHANNEL_DIAGNOSIS)), },
    .UseAs  = { .ExtChannelDiagData = {
                  .UserStructureIdent = SDAI_PN_ALARM_USER_STRUCTURE_IDENT_EXT_CHANNEL_DIAG,
                  .ChannelNumber      = PROFINET_ALARM_CHANNEL_DIAG_CHANNEL_NUMBER_SUBSLOT,
                  .ChannelProperties  = SDAI_DEFAULT_CHANNEL_PROP_M_REQUIRED,
                  .ChannelError       = PROFINET_CH_ERR_TYPE_POWER_SUPPLY_FAULT,
                  .ExtChannelError    = PROFINET_EXT_CH_ERR_TYPE_ACCUMULATIVE_INFO,
                  .ExtChannelAddValue = 0xA55AA55A, } } },

  /* predefined qualified channel diagnosis alarms with different levels of severity -> maintenance required, maintenance demanded and diagnosis */
  { .Header = { .Id = 0, .Type = SDAI_DIAGNOSIS_TYPE_PROTOCOL_SPECIFIC, .Severity = SDAI_DIAGNOSIS_SEVERITY_FAILURE, .StatusSpecifier = SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS, .Length = (sizeof (struct SDAI_PN_PROFINET_QUALIFIED_CHANNEL_DIAGNOSIS)), },
    .UseAs  = { .QualifiedChannelDiagData = {
                  .UserStructureIdent = SDAI_PN_ALARM_USER_STRUCTURE_IDENT_QUAL_CHANNEL_DIAG,
                  .ChannelNumber      = PROFINET_ALARM_CHANNEL_DIAG_CHANNEL_NUMBER_SUBSLOT,
                  .ChannelProperties  = SDAI_DEFAULT_CHANNEL_PROP_M_REQUIRED,
                  .ChannelError       = PROFINET_CH_ERR_TYPE_TEMPORARY_FAULT,
                  .ExtChannelError    = PROFINET_EXT_CH_ERR_TYPE_ACCUMULATIVE_INFO,
                  .ExtChannelAddValue = 0xA55AA55A,
                  .Qualifier          = 0x12345678, } } },
};

/*---------------------------------------------------------------------------*/

static unsigned               AlarmIndex          = 0u;
static T_PROFINET_ALARM_REQ   PredefinedAlarms [] =
{
  { .Header = { .Id = 0, .AlarmType = SDAI_PN_ALARM_PROCESS, .AlarmPriority = SDAI_ALARM_PRIO_HIGH, .Length = (sizeof(T_PN_SOE_ITEM) + 2), },
    .UserStructureIdentHigh = _U16_HIGH_BYTE (PROFINET_ALARM_USER_STRUCTURE_IDENT_SOE), .UserStructureIdentLow = _U16_LOW_BYTE (PROFINET_ALARM_USER_STRUCTURE_IDENT_SOE),
    .UseAs  = { .SequenceOfEvents = { 0 } } },
  { .Header = { .Id = 0, .AlarmType = SDAI_PN_ALARM_STATUS, .AlarmPriority = SDAI_ALARM_PRIO_LOW, .Length = 0, }, },
  { .Header = { .Id = 0, .AlarmType = SDAI_PN_ALARM_UPDATE, .AlarmPriority = SDAI_ALARM_PRIO_LOW, .Length = 0, }, },
};

/******************************************************************************
FUNCTION IMPLEMENTATION
******************************************************************************/

/**
 * The function profinet_alarm_ack_callback() is called when an outstanding alarm is
 * acknowledged and resets the AlarmPending flag. Since we only have one outstanding
 * alarm we do not analyze the parameters.
 *
 * @return
 * - type : void
 * @param[in] pAlarmAckData
 * - type : struct SDAI_ALARM_ACK_IND*
 * - range: whole address range is valid
 */
void profinet_alarm_ack_callback (struct SDAI_ALARM_ACK_IND* pAlarmAckData)
{
  _TRACE (("profinet_alarm_ack_callback\r\n"));

  _AVOID_UNUSED_WARNING(pAlarmAckData);

  AlarmPending = 0;

  return;
}

/*===========================================================================*/

/**
 * The function profinet_module_led_blink() toggles the Module Status LED with 1 Hz.
 *
 * @return
 * - type : void
 * @param[in] Led
 * - type : U8
 * - range: whole range is valid
 */
static void profinet_module_led_blink (U32 Led)
{
  static U32 NumberBlinksModuleStatus = 0;
  static U32 NumberCallsModuleStatus = 0;


  _TRACE(("profinet_module_led_blink\r\n"));

  NumberCallsModuleStatus++;

  if (500 == (NumberCallsModuleStatus * CYCLE_TIME_IN_MS))
  {
    NumberCallsModuleStatus = 0;
    NumberBlinksModuleStatus++;

    STATUS_LED_TOGGLE (Led);

    if (NumberBlinksModuleStatus == 10)
    {
      NumberBlinksModuleStatus = 0;
    }
  }

  return;
}

/*===========================================================================*/

/**
 * The function profinet_blink() handles the Profinet signal command. It toggles
 * an LED with 1 Hz for 3 seconds.
 *
 * @return
 * - type : void
 */
static void profinet_blink (void)
{
  static U32   NumberBlinks = 0;
  static U32   NumberCalls  = 0;


  _TRACE (("profinet_blink\r\n"));

  NumberCalls++;

  if (500 == (NumberCalls * CYCLE_TIME_IN_MS))
  {
    NumberCalls = 0;
    NumberBlinks++;

    STATUS_LED_TOGGLE(PN_LED_SIGNAL);

    if (NumberBlinks == 10)
    {
      NumberBlinks = 0;
      PnBlink = 0;
    }
  }

  return;
}

/*===========================================================================*/

/**
 * The function profinet_send_diagnosis()
 *
 * @return
 * - type : U8
 * @param[in] Unit
 * - type : U8
 * - range: whole range is valid
 * @param[in] DiagnosisIndex
 * - type : U16
 * - range: whole range is valid
 * @param[in] StatusSpecifier
 * - type : U16
 * - range: SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS | SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_DISAPPEARS
 */
U8 profinet_send_diagnosis (U8 Unit, U16 DiagnosisIndex, U16 StatusSpecifier)
{
  struct T_DEMO_UNIT*     pUnit;
  U8                      Result;


  _TRACE (("profinet_send_diagnosis\r\n"));

  pUnit = (ApplManagement.UnitsManagement.UnitList + Unit);

  /* copy the predefined diagnosis data */
  memcpy (&Diagnose, &PredefinedDiagnosisAlarms [DiagnosisIndex], sizeof (Diagnose));

  /* adapt the required header information */
  Diagnose.Header.Id              = pUnit->Id;
  Diagnose.Header.StatusSpecifier = StatusSpecifier;

  if (Diagnose.Header.Type == SDAI_DIAGNOSIS_TYPE_PROTOCOL_SPECIFIC)
  {
    if (StatusSpecifier == SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS)
    {
      Diagnose.UseAs.ChannelDiagnosisData.ChannelProperties = PROFINET_ALARM_CHANNEL_PROPERTIES_DIAG_APPEAR;
    }
    else
    {
      Diagnose.UseAs.ChannelDiagnosisData.ChannelProperties = PROFINET_ALARM_CHANNEL_PROPERTIES_DIAG_DISAPPEAR;
    }

    if ( (pUnit->Type == SDAI_UNIT_TYPE_HEAD) || (pUnit->Type == SDAI_UNIT_TYPE_GENERIC_INPUT) )
    {
      Diagnose.UseAs.ChannelDiagnosisData.ChannelProperties |= PROFINET_ALARM_CHANNEL_PROPERTIES_DIRECTION_IN;
    }
    else if (pUnit->Type == SDAI_UNIT_TYPE_GENERIC_OUTPUT)
    {
      Diagnose.UseAs.ChannelDiagnosisData.ChannelProperties |= PROFINET_ALARM_CHANNEL_PROPERTIES_DIRECTION_OUT;
    }
    else if (pUnit->Type == SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT)
    {
      Diagnose.UseAs.ChannelDiagnosisData.ChannelProperties |= PROFINET_ALARM_CHANNEL_PROPERTIES_DIRECTION_IN_OUT;
    }
  }
  else
  {
    /* For the predefined diagnosis types all parameters are handled by the stack */
  }

  if ((Result = sdai_diagnosis_request (&Diagnose.Header)) != SDAI_SUCCESS)
  {
    demo_print_error (Result);
  }

  return (Result);
}

/*===========================================================================*/

/**
 * The function profinet_send_alarm() sends an alarm to the controller for the given
 * Unit. Alternating with each call it generates a appearing and disappearing diagnosis
 * for the Unit. For Unit 1 the generated alarm is a channel diagnosis and for Unit 2
 * it is a manufacturer specific diagnosis.
 *
 * @return
 * - type : U8
 * @param[in] Unit
 * - type : U8
 * - range: 1 | 2
 */
static U8 profinet_send_alarm (U8 Unit)
{
  struct T_DEMO_UNIT*     pUnit;
  U8                      Result;


  _TRACE (("profinet_send_alarm\r\n"));

  pUnit = (ApplManagement.UnitsManagement.UnitList + Unit);

  if ((AlarmPending != 0) || (!(ApplManagement.StackStatus & SDAI_STATE_MASK_CONNECTED)))
  {
    return (SDAI_SUCCESS);
  }

  /* copy the predefined diagnosis data */
  memcpy (&Alarm, &PredefinedAlarms [AlarmIndex], sizeof (Alarm));

  /* adapt the required header information */
  Alarm.Header.Id = pUnit->Id;

  AlarmIndex += 1u;
  AlarmIndex  = (AlarmIndex == _NUMBER_ARRAY_ELEMENTS (PredefinedAlarms)) ? 0u : AlarmIndex;

  AlarmPending = 1;

  if ((Result = sdai_alarm_request (&Alarm.Header)) != SDAI_SUCCESS)
  {
    AlarmPending = 0;

    demo_print_error (Result);
  }

  /*-------------------------------------------------------------------------*/

  return (Result);
}

/*===========================================================================*/

/**
 * The function demo_update_ident_data() prints the new identification data and stores them
 * permanently, if requested. If the data shall be stored in FLASH the function copies the
 * new data to the pFlashConfigData structure and informs the main task for storing the
 * new values.
 *
 * @return
 * - type : U8
 * - range: PROTO_FUNC_SUCCESS
 * @param[in] pIdentData
 * - type : struct T_DEMO_IDENT_DATA*
 * - range: whole address range is valid
 */
U8 profinet_update_ident_data (struct T_DEMO_IDENT_DATA* pIdentData)
{
  _TRACE (("profinet_update_ident_data\r\n"));

  _DEBUG_LOG ( ("\r\nIdentification data changed\r\n") );

  _DEBUG_LOG ( ("  New Device Name: %s\r\n",(U8*)pIdentData->IdentData.DevName) );

  if (pIdentData->IdentData.UseAs.Pn.StorePermanent & SDAI_STORE_NAME_PERMANENT)
  {
    _DEBUG_LOG ( ("  store Name permanent\r\n") );

    memcpy (pFlashConfigData->DeviceName, (U8*)pIdentData->IdentData.DevName, SDAI_DEVNAME_MAX_LEN);
  }

  _DEBUG_LOG ( ("  New IP Address:  %03d.%03d.%03d.%03d\r\n",  pIdentData->IdentData.UseAs.Pn.Address [0],
                                                               pIdentData->IdentData.UseAs.Pn.Address [1],
                                                               pIdentData->IdentData.UseAs.Pn.Address [2],
                                                               pIdentData->IdentData.UseAs.Pn.Address [3]) );
  _DEBUG_LOG ( ("  New Netmask:     %03d.%03d.%03d.%03d\r\n",  pIdentData->IdentData.UseAs.Pn.Netmask [0],
                                                               pIdentData->IdentData.UseAs.Pn.Netmask [1],
                                                               pIdentData->IdentData.UseAs.Pn.Netmask [2],
                                                               pIdentData->IdentData.UseAs.Pn.Netmask [3]) );
  _DEBUG_LOG ( ("  New Gateway:     %03d.%03d.%03d.%03d\r\n",  pIdentData->IdentData.UseAs.Pn.Gateway [0],
                                                               pIdentData->IdentData.UseAs.Pn.Gateway [1],
                                                               pIdentData->IdentData.UseAs.Pn.Gateway [2],
                                                               pIdentData->IdentData.UseAs.Pn.Gateway [3]) );

  if (pIdentData->IdentData.UseAs.Pn.StorePermanent & SDAI_STORE_IP_PERMANENT)
  {
    _DEBUG_LOG ( ("  store IP permanent\r\n") );

    memcpy (pFlashConfigData->IpAddress, pIdentData->IdentData.UseAs.Pn.Address, sizeof (pFlashConfigData->IpAddress));
    memcpy (pFlashConfigData->Netmask,   pIdentData->IdentData.UseAs.Pn.Netmask, sizeof (pFlashConfigData->Netmask));
    memcpy (pFlashConfigData->Gateway,   pIdentData->IdentData.UseAs.Pn.Gateway, sizeof (pFlashConfigData->Gateway));
  }

  if ( (pIdentData->IdentData.UseAs.Pn.StorePermanent & SDAI_STORE_IP_PERMANENT  ) ||
       (pIdentData->IdentData.UseAs.Pn.StorePermanent & SDAI_STORE_NAME_PERMANENT) )
  {
    platform_send_event (EVENT_STORE_FLASH_DATA);
  }

  if ( (pIdentData->IdentData.UseAs.Pn.Address [0] != IpAddress [0]) ||
       (pIdentData->IdentData.UseAs.Pn.Address [1] != IpAddress [1]) ||
       (pIdentData->IdentData.UseAs.Pn.Address [2] != IpAddress [2]) ||
       (pIdentData->IdentData.UseAs.Pn.Address [3] != IpAddress [3]) )
  {
    memcpy (&IpAddress [0], &pIdentData->IdentData.UseAs.Pn.Address[0], sizeof(IpAddress));

//    platform_send_event (EVENT_RESTART_NETWORK);
  }

  return (PROTO_FUNC_SUCCESS);
}

/*===========================================================================*/

/**
 * The function profinet_process_write_ind() processes the received write indication.
 * The function checks the address parameter and fills the pWriteRes structure with the
 * response data. This sample application supports the Index 1 for the plugged
 * units on which the parameter data are transfered by the Profinet Controller during
 * connection establishment. Additionally the profinet I&M1 to I&M3 indices are supported.
 * This sample implementation rejects all other indices with SDAI_SERVICE_ERR_INVALID_PARAMETER
 * (a real application should handle all needed indices differently).
 *
 * @return
 * - type : U8
 * - range: PROTO_FUNC_SUCCESS
 * @param[in] pWriteInd
 * - type : struct T_DEMO_WRITE_IND*
 * - range: whole address range is valid
 * @param[in] pWriteRes
 * - type : struct T_DEMO_WRITE_RES*
 * - range: whole address range is valid
 */
U8 profinet_process_write_ind (struct T_DEMO_WRITE_IND* pWriteInd, struct T_DEMO_WRITE_RES* pWriteRes)
{
  struct T_DEMO_UNIT*   pUnit;


  _TRACE (("profinet_process_write_ind\r\n"));

  _DEBUG_LOG ( ("\r\nWrite indication:\r\n  Unit ID:  0x" USIGN32_HEX_FORMAT_STRING "\r\n  Index:    0x%04X\r\n  Length:   %d\r\n",
                pWriteInd->Header.UseAs.Pn.Id,
                pWriteInd->Header.UseAs.Pn.Index,
                pWriteInd->Header.Length) );

  pWriteRes->Header.UseAs.Pn.Id     = pWriteInd->Header.UseAs.Pn.Id;
  pWriteRes->Header.UseAs.Pn.Index  = pWriteInd->Header.UseAs.Pn.Index;
  pWriteRes->Header.ErrorCode       = SDAI_SERVICE_ERR_OBJECT_NOT_EXISTING;

  pUnit = demo_find_unit (pWriteInd->Header.UseAs.Pn.Id);

  if (pUnit != NULL)
  {
    struct T_DEMO_UNIT*   pHeadUnit = &ApplManagement.UnitsManagement.UnitList[0u];


    if (pWriteInd->Header.UseAs.Pn.Index == PN_INDEX_CONFIGURATION_DATA)
    {
      T_PN_UNIT_DATA*   pPnUnitData = (T_PN_UNIT_DATA*)pUnit->pProtocolData;


      if (pWriteInd->Header.Length <= sizeof (pPnUnitData->ConfigData))
      {
        memcpy (&pPnUnitData->ConfigData[0], &pWriteInd->Data [0], pWriteInd->Header.Length);

        pWriteRes->Header.ErrorCode = SDAI_SERVICE_SUCCESS;
      }
      else
      {
        pWriteRes->Header.ErrorCode = SDAI_SERVICE_ERR_INVALID_SIZE;
      }
    }
    #ifdef PLATFORM_PERIPHERAL_ISOCHRONOUS_BASE
    else if (pWriteInd->Header.UseAs.Pn.Index == PN_INDEX_SUBSLOT_ISOCHRONOUS_MODE_DATA)
    {
      T_PN_ISOCHRONOUS_MODE_DATA*   pIsochronousData  = (T_PN_ISOCHRONOUS_MODE_DATA*)&pWriteInd->Data [0];

      if ( (pIsochronousData->Header.TypeHighByte != _U16_HIGH_BYTE (PN_BLOCK_TYPE_ISOCHRONOUS_MODE_DATA)           ) ||
           (pIsochronousData->Header.TypeLowByte  != _U16_LOW_BYTE (PN_BLOCK_TYPE_ISOCHRONOUS_MODE_DATA)            ) ||
           (pIsochronousData->Header.LengthHighByte != _U16_HIGH_BYTE (sizeof (T_PN_ISOCHRONOUS_MODE_DATA) - 4)     ) ||
           (pIsochronousData->Header.LengthLowByte != _U16_LOW_BYTE (sizeof (T_PN_ISOCHRONOUS_MODE_DATA) - 4)       ) ||
           (pIsochronousData->Header.VersionHighByte != 0x01) || (pIsochronousData->Header.VersionLowByte != 0x00   ) )
      {
        pWriteRes->Header.ErrorCode = SDAI_SERVICE_ERR_APPLICATION;
      }
      else
      {
        if (pWriteInd->Header.UseAs.Pn.Id == _SDAI_PN_SLOT_AND_SUBSLOT_TO_ID(PLATFORM_PERIPHERAL_ISOCHRONOUS_VECTOR, 1))
        {
          T_PN_UNIT_DATA*   pPnUnitData = (T_PN_UNIT_DATA*)pUnit->pProtocolData;
          U16               HighWord;
          U16               LowWord;
          U32               DataCycle;
          U32               DelayOutput;
          U32               DelayInput;

          memcpy (&pPnUnitData->IsochronousData, &pWriteInd->Data [0], pWriteInd->Header.Length);

          HighWord    = _HIGH_LOW_BYTES_TO_U16 (pPnUnitData->IsochronousData.TimeIOOutputHighWordHighByte, pPnUnitData->IsochronousData.TimeIOOutputHighWordLowByte);
          LowWord     = _HIGH_LOW_BYTES_TO_U16 (pPnUnitData->IsochronousData.TimeIOOutputLowWordHighByte, pPnUnitData->IsochronousData.TimeIOOutputLowWordLowByte);
          DelayOutput = _HIGH_LOW_WORDS_TO_U32 (HighWord, LowWord);

          HighWord    = _HIGH_LOW_BYTES_TO_U16 (pPnUnitData->IsochronousData.TimeIOInputHighWordHighByte, pPnUnitData->IsochronousData.TimeIOInputHighWordLowByte);
          LowWord     = _HIGH_LOW_BYTES_TO_U16 (pPnUnitData->IsochronousData.TimeIOInputLowWordHighByte, pPnUnitData->IsochronousData.TimeIOInputLowWordLowByte);
          DelayInput  = _HIGH_LOW_WORDS_TO_U32 (HighWord, LowWord);

          DataCycle   = (U32) _HIGH_LOW_BYTES_TO_U16 (pPnUnitData->IsochronousData.TimeDataCycleHighByte, pPnUnitData->IsochronousData.TimeDataCycleLowByte);

          DataCycle   = (DataCycle * PN_ISOCHRONOUS_T_DC_BASE * PN_ISOCHRONOUS_DC_BASE_NANOSECONDS);

          platform_configure_isochronous_unit (PLATFORM_PERIPHERAL_ISOCHRONOUS_VECTOR, (DataCycle - DelayInput), DelayOutput);
        }
      }
    }
    #endif
    else if (pWriteInd->Header.UseAs.Pn.Index == PN_INDEX_IDENT_AND_MAINTENANCE_1)
    {
       T_PN_IDENT_MAINTENANCE_1*   pIM1Data  = (T_PN_IDENT_MAINTENANCE_1*)&pWriteInd->Data [0];

       if ( (pIM1Data->Header.TypeHighByte != _U16_HIGH_BYTE (PN_BLOCK_TYPE_IDENT_AND_MAINTENANCE_1)  ) ||
            (pIM1Data->Header.TypeLowByte  != _U16_LOW_BYTE (PN_BLOCK_TYPE_IDENT_AND_MAINTENANCE_1)   ) ||
            (pIM1Data->Header.LengthHighByte != _U16_HIGH_BYTE (sizeof (T_PN_IDENT_MAINTENANCE_1) - 4)) ||
            (pIM1Data->Header.LengthLowByte != _U16_LOW_BYTE (sizeof (T_PN_IDENT_MAINTENANCE_1) - 4)  ) ||
            (pIM1Data->Header.VersionHighByte != 0x01) || (pIM1Data->Header.VersionLowByte != 0x00    ) )
       {
         pWriteRes->Header.ErrorCode = SDAI_SERVICE_ERR_APPLICATION;
       }
       else
       {
         if (pHeadUnit->Id == pWriteInd->Header.UseAs.Pn.Id)
         {
           memcpy (&pFlashConfigData->IdentMaintenanceData.Function [0], &pIM1Data->Function [0], sizeof (pFlashConfigData->IdentMaintenanceData.Function));
           memcpy (&pFlashConfigData->IdentMaintenanceData.Location [0], &pIM1Data->Location [0], sizeof (pFlashConfigData->IdentMaintenanceData.Location));

           pWriteRes->Header.ErrorCode = SDAI_SERVICE_SUCCESS;

           platform_send_event (EVENT_STORE_FLASH_DATA);
         }
       }
    }
    else if (pWriteInd->Header.UseAs.Pn.Index == PN_INDEX_IDENT_AND_MAINTENANCE_2)
    {
       T_PN_IDENT_MAINTENANCE_2*   pIM2Data = (T_PN_IDENT_MAINTENANCE_2*)&pWriteInd->Data [0];

       if ( (pIM2Data->Header.TypeHighByte != _U16_HIGH_BYTE (PN_BLOCK_TYPE_IDENT_AND_MAINTENANCE_2)  ) ||
            (pIM2Data->Header.TypeLowByte  != _U16_LOW_BYTE (PN_BLOCK_TYPE_IDENT_AND_MAINTENANCE_2)   ) ||
            (pIM2Data->Header.LengthHighByte != _U16_HIGH_BYTE (sizeof (T_PN_IDENT_MAINTENANCE_2) - 4)) ||
            (pIM2Data->Header.LengthLowByte != _U16_LOW_BYTE (sizeof (T_PN_IDENT_MAINTENANCE_2) - 4)  ) ||
            (pIM2Data->Header.VersionHighByte != 0x01) || (pIM2Data->Header.VersionLowByte != 0x00    ) )
       {
         pWriteRes->Header.ErrorCode = SDAI_SERVICE_ERR_APPLICATION;
       }
       else
       {
         if (pHeadUnit->Id == pWriteInd->Header.UseAs.Pn.Id)
         {
           memcpy (&pFlashConfigData->IdentMaintenanceData.Date [0], &pIM2Data->Date[0], sizeof (pFlashConfigData->IdentMaintenanceData.Date));

           pWriteRes->Header.ErrorCode = SDAI_SERVICE_SUCCESS;

           platform_send_event (EVENT_STORE_FLASH_DATA);
         }
       }
    }
    else if (pWriteInd->Header.UseAs.Pn.Index == PN_INDEX_IDENT_AND_MAINTENANCE_3)
    {
       T_PN_IDENT_MAINTENANCE_3*   pIM3Data = (T_PN_IDENT_MAINTENANCE_3*)&pWriteInd->Data [0];

       if ( (pIM3Data->Header.TypeHighByte != _U16_HIGH_BYTE (PN_BLOCK_TYPE_IDENT_AND_MAINTENANCE_3)  ) ||
            (pIM3Data->Header.TypeLowByte  != _U16_LOW_BYTE (PN_BLOCK_TYPE_IDENT_AND_MAINTENANCE_3)   ) ||
            (pIM3Data->Header.LengthHighByte != _U16_HIGH_BYTE (sizeof (T_PN_IDENT_MAINTENANCE_3) - 4)) ||
            (pIM3Data->Header.LengthLowByte != _U16_LOW_BYTE (sizeof (T_PN_IDENT_MAINTENANCE_3) - 4)  ) ||
            (pIM3Data->Header.VersionHighByte != 0x01) || (pIM3Data->Header.VersionLowByte != 0x00    ) )
       {
         pWriteRes->Header.ErrorCode = SDAI_SERVICE_ERR_APPLICATION;
       }
       else
       {
         if (pHeadUnit->Id == pWriteInd->Header.UseAs.Pn.Id)
         {
           memcpy (&pFlashConfigData->IdentMaintenanceData.Description [0], &pIM3Data->Description[0], sizeof (pFlashConfigData->IdentMaintenanceData.Description));

           pWriteRes->Header.ErrorCode = SDAI_SERVICE_SUCCESS;

           platform_send_event (EVENT_STORE_FLASH_DATA);
         }
       }
    }
    else
    {
      pWriteRes->Header.ErrorCode = SDAI_SERVICE_ERR_INVALID_PARAMETER;
    }
  }

  return (PROTO_FUNC_SUCCESS);
}

/*===========================================================================*/

/**
 * The function profinet_process_read_ind() processes the received read indication.
 * The function checks the address information and fills the pReadRes structure.
 * This sample application supports Index 1, where the configuration parameter
 * for the Unit are stored. Additionally the profinet indices I&M0 to I&M3 and
 * I&MFilterData are supported. This sample implementation rejects all other
 * indices with SDAI_SERVICE_ERR_INVALID_PARAMETER (a real application should
 * handle all needed indices differently).
 *
 * @return
 * - type : U8
 * - range: PROTO_FUNC_SUCCESS
 * @param[in] pReadInd
 * - type : struct T_DEMO_READ_IND*
 * - range: whole address range is valid
 * @param[in] pReadRes
 * - type : struct T_DEMO_READ_RES*
 * - range: whole address range is valid
 */
U8 profinet_process_read_ind (struct T_DEMO_READ_IND* pReadInd, struct T_DEMO_READ_RES* pReadRes)
{
  struct T_DEMO_UNIT*   pUnit;
  U32                   Length;


  _TRACE (("profinet_process_read_ind\r\n"));

  _DEBUG_LOG ( ("\r\nRead indication:\r\n  Unit ID:     0x" USIGN32_HEX_FORMAT_STRING "\r\n  Index:  0x%04X\r\n  Length:      %d\r\n",
                pReadInd->Header.UseAs.Pn.Id,
                pReadInd->Header.UseAs.Pn.Index,
                pReadInd->Header.Length) );

  pReadRes->Header.UseAs.Pn.Id     = pReadInd->Header.UseAs.Pn.Id;
  pReadRes->Header.UseAs.Pn.Index  = pReadInd->Header.UseAs.Pn.Index;
  pReadRes->Header.Length          = 0;

  pUnit = demo_find_unit (pReadInd->Header.UseAs.Pn.Id);

  if (pReadInd->Header.UseAs.Pn.Index == PN_INDEX_CONFIGURATION_DATA)
  {
    if (pUnit != NULL)
    {
      T_PN_UNIT_DATA*   pPnUnitData = (T_PN_UNIT_DATA*)pUnit->pProtocolData;

      Length = (pReadInd->Header.Length <= sizeof (pPnUnitData->ConfigData)) ? pReadInd->Header.Length : sizeof (pPnUnitData->ConfigData);

      memcpy(&pReadRes->Data [0], &pPnUnitData->ConfigData[0], Length);

      pReadRes->Header.Length    = (U16) Length;
      pReadRes->Header.ErrorCode = SDAI_SERVICE_SUCCESS;
    }
    else
    {
      pReadRes->Header.ErrorCode = SDAI_SERVICE_ERR_OBJECT_NOT_EXISTING;
    }
  }
  #ifdef PLATFORM_PERIPHERAL_ISOCHRONOUS_BASE
  else if (pReadInd->Header.UseAs.Pn.Index == PN_INDEX_SUBSLOT_ISOCHRONOUS_MODE_DATA)
  {
    if (pUnit != NULL)
    {
      if (pReadInd->Header.UseAs.Pn.Id == _SDAI_PN_SLOT_AND_SUBSLOT_TO_ID(PLATFORM_PERIPHERAL_ISOCHRONOUS_VECTOR, 1))
      {
        T_PN_UNIT_DATA*               pPnUnitData          = (T_PN_UNIT_DATA*)pUnit->pProtocolData;
        T_PN_ISOCHRONOUS_MODE_DATA*   pIsochronousModeData = (T_PN_ISOCHRONOUS_MODE_DATA*)&pReadRes->Data [0];

        pIsochronousModeData->Header.TypeHighByte    = _U16_HIGH_BYTE (PN_BLOCK_TYPE_ISOCHRONOUS_MODE_DATA);
        pIsochronousModeData->Header.TypeLowByte     = _U16_LOW_BYTE (PN_BLOCK_TYPE_ISOCHRONOUS_MODE_DATA);

        pIsochronousModeData->Header.LengthHighByte  = _U16_HIGH_BYTE (sizeof (T_PN_ISOCHRONOUS_MODE_DATA) - 4);
        pIsochronousModeData->Header.LengthLowByte   = _U16_LOW_BYTE (sizeof (T_PN_ISOCHRONOUS_MODE_DATA) - 4);

        pIsochronousModeData->Header.VersionHighByte = 0x01;
        pIsochronousModeData->Header.VersionLowByte  = 0x00;

        memcpy ((pIsochronousModeData + 1), &pPnUnitData->IsochronousData.Padding1, sizeof (T_PN_ISOCHRONOUS_MODE_DATA) - sizeof (T_PN_BLOCK_HEADER));

        pReadRes->Header.Length    = (U16) sizeof (T_PN_ISOCHRONOUS_MODE_DATA);
        pReadRes->Header.ErrorCode = SDAI_SERVICE_SUCCESS;
      }
      else
      {
        pReadRes->Header.ErrorCode = SDAI_SERVICE_ERR_INVALID_PARAMETER;
      }
    }
    else
    {
      pReadRes->Header.ErrorCode = SDAI_SERVICE_ERR_OBJECT_NOT_EXISTING;
    }
  }
  #endif
  else if (pReadInd->Header.UseAs.Pn.Index == PN_INDEX_IDENT_AND_MAINTENANCE_FILTER)
  {
    T_PN_IDENT_MAINTENANCE_0_FILTERDATAINFO*   pIMSubmoduleFilterData = (T_PN_IDENT_MAINTENANCE_0_FILTERDATAINFO*)&pReadRes->Data [0];
    T_PN_IDENT_MAINTENANCE_0_FILTERDATAINFO*   pIMDeviceFilterData = (pIMSubmoduleFilterData + 1);
    struct T_DEMO_UNIT*                        pHeadUnit     = &ApplManagement.UnitsManagement.UnitList[0u];

    /* Submodule filter data describe which submodules have discrete I&M data. In this sample only the submodule of the
       headmodule has I&M data. */
    pIMSubmoduleFilterData->Header.TypeHighByte                = _U16_HIGH_BYTE (PN_BLOCK_TYPE_IM0_FILTER_SUBMODULE);
    pIMSubmoduleFilterData->Header.TypeLowByte                 = _U16_LOW_BYTE (PN_BLOCK_TYPE_IM0_FILTER_SUBMODULE);

    pIMSubmoduleFilterData->Header.LengthHighByte              = _U16_HIGH_BYTE (sizeof (T_PN_IDENT_MAINTENANCE_0_FILTERDATAINFO) - 4);
    pIMSubmoduleFilterData->Header.LengthLowByte               = _U16_LOW_BYTE (sizeof (T_PN_IDENT_MAINTENANCE_0_FILTERDATAINFO) - 4);

    pIMSubmoduleFilterData->Header.VersionHighByte             = 0x01;
    pIMSubmoduleFilterData->Header.VersionLowByte              = 0x00;

    pIMSubmoduleFilterData->NumberApiHighByte                  = 0x00;
    pIMSubmoduleFilterData->NumberApiLowByte                   = 0x01;

    /* SDAI always uses the default API 0x00000000 */
    pIMSubmoduleFilterData->NumberApiHighWordHighByte          = 0x00;
    pIMSubmoduleFilterData->NumberApiHighWordLowByte           = 0x00;
    pIMSubmoduleFilterData->NumberApiLowWordHighByte           = 0x00;
    pIMSubmoduleFilterData->NumberApiLowWordLowByte            = 0x00;

    pIMSubmoduleFilterData->NumberModulsHighByte               = 0x00;
    pIMSubmoduleFilterData->NumberModulsLowByte                = 0x01;

    pIMSubmoduleFilterData->SlotNumberHighByte                 = _U16_HIGH_BYTE (_SDAI_PN_ID_TO_SLOT (U16, pHeadUnit->Id));
    pIMSubmoduleFilterData->SlotNumberLowByte                  = _U16_LOW_BYTE (_SDAI_PN_ID_TO_SLOT (U16, pHeadUnit->Id));

    pIMSubmoduleFilterData->SlotIdentnumberHighWordHighByte    = _U16_HIGH_BYTE (_U32_HIGH_WORD (pHeadUnit->CfgData.UseAs.Pn.IdentNumber));
    pIMSubmoduleFilterData->SlotIdentnumberHighWordLowByte     = _U16_LOW_BYTE (_U32_HIGH_WORD (pHeadUnit->CfgData.UseAs.Pn.IdentNumber));
    pIMSubmoduleFilterData->SlotIdentnumberLowWordHighByte     = _U16_HIGH_BYTE (_U32_LOW_WORD (pHeadUnit->CfgData.UseAs.Pn.IdentNumber));
    pIMSubmoduleFilterData->SlotIdentnumberLowWordLowByte      = _U16_LOW_BYTE (_U32_LOW_WORD (pHeadUnit->CfgData.UseAs.Pn.IdentNumber));

    pIMSubmoduleFilterData->NumberSubmodulesHighByte           = 0x00;
    pIMSubmoduleFilterData->NumberSubmodulesLowByte            = 0x01;

    pIMSubmoduleFilterData->SubSlotNumberHighByte              = _U16_HIGH_BYTE (_SDAI_PN_ID_TO_SUBSLOT (U16, pHeadUnit->Id));
    pIMSubmoduleFilterData->SubSlotNumberLowByte               = _U16_LOW_BYTE (_SDAI_PN_ID_TO_SUBSLOT (U16, pHeadUnit->Id));

    pIMSubmoduleFilterData->SubSlotIdentnumberHighWordHighByte = _U16_HIGH_BYTE (_U32_HIGH_WORD (pHeadUnit->CfgData.UseAs.Pn.SubmoduleIdentNumber));
    pIMSubmoduleFilterData->SubSlotIdentnumberHighWordLowByte  = _U16_LOW_BYTE (_U32_HIGH_WORD (pHeadUnit->CfgData.UseAs.Pn.SubmoduleIdentNumber));
    pIMSubmoduleFilterData->SubSlotIdentnumberLowWordHighByte  = _U16_HIGH_BYTE (_U32_LOW_WORD (pHeadUnit->CfgData.UseAs.Pn.SubmoduleIdentNumber));
    pIMSubmoduleFilterData->SubSlotIdentnumberLowWordLowByte   = _U16_LOW_BYTE (_U32_LOW_WORD (pHeadUnit->CfgData.UseAs.Pn.SubmoduleIdentNumber));

    /* Device filter data describe which submodules holds the device I&M data that supports writing of
       I&M1, I&M2, and I&M3 (mandatory). In this sample this is the same as the submodule filter data */
    memcpy (pIMDeviceFilterData, pIMSubmoduleFilterData, sizeof (T_PN_IDENT_MAINTENANCE_0_FILTERDATAINFO));

    pIMDeviceFilterData->Header.TypeHighByte = _U16_HIGH_BYTE (PN_BLOCK_TYPE_IM0_FILTER_DEVICE);
    pIMDeviceFilterData->Header.TypeLowByte  = _U16_LOW_BYTE (PN_BLOCK_TYPE_IM0_FILTER_DEVICE);

    pReadRes->Header.Length    = (2 * sizeof (T_PN_IDENT_MAINTENANCE_0_FILTERDATAINFO));
    pReadRes->Header.ErrorCode = SDAI_SERVICE_SUCCESS;
  }
  else if (pReadInd->Header.UseAs.Pn.Index == PN_INDEX_IDENT_AND_MAINTENANCE_0)
  {
    T_PN_IDENT_MAINTENANCE_0*   pIM0Data = (T_PN_IDENT_MAINTENANCE_0*)&pReadRes->Data [0];

    pIM0Data->Header.TypeHighByte        = _U16_HIGH_BYTE (PN_BLOCK_TYPE_IDENT_AND_MAINTENANCE_0);
    pIM0Data->Header.TypeLowByte         = _U16_LOW_BYTE (PN_BLOCK_TYPE_IDENT_AND_MAINTENANCE_0);

    pIM0Data->Header.LengthHighByte      = _U16_HIGH_BYTE (sizeof (T_PN_IDENT_MAINTENANCE_0) - 4);
    pIM0Data->Header.LengthLowByte       = _U16_LOW_BYTE (sizeof (T_PN_IDENT_MAINTENANCE_0) - 4);

    pIM0Data->Header.VersionHighByte     = 0x01;
    pIM0Data->Header.VersionLowByte      = 0x00;

    pIM0Data->VendorIdHighByte           = _U16_HIGH_BYTE (PN_VENDOR_ID_SOFTING);
    pIM0Data->VendorIdLowByte            = _U16_LOW_BYTE (PN_VENDOR_ID_SOFTING);

    pIM0Data->HwRevisionHighByte         = _U16_HIGH_BYTE (PN_HARDWARE_REVISION);
    pIM0Data->HwRevisionLowByte          = _U16_LOW_BYTE (PN_HARDWARE_REVISION);

    pIM0Data->SwRevisionHighWordHighByte = _U16_HIGH_BYTE (_U32_HIGH_WORD (PN_SOFTWARE_REVISION));
    pIM0Data->SwRevisionHighWordLowByte  = _U16_LOW_BYTE (_U32_HIGH_WORD (PN_SOFTWARE_REVISION));
    pIM0Data->SwRevisionLowWordHighByte  = _U16_HIGH_BYTE (_U32_LOW_WORD (PN_SOFTWARE_REVISION));
    pIM0Data->SwRevisionLowWordLowByte   = _U16_LOW_BYTE (_U32_LOW_WORD (PN_SOFTWARE_REVISION));

    pIM0Data->RevisionCounterHighByte    = 0x00;
    pIM0Data->RevisionCounterLowByte     = 0x01;

    pIM0Data->ProfileIdHighByte          = 0x00;
    pIM0Data->ProfileIdLowByte           = 0x00;

    pIM0Data->ProfileTypeHighByte        = 0x00;
    pIM0Data->ProfileTypeLowByte         = 0x00;

    pIM0Data->VersionHighByte            = 0x01;
    pIM0Data->VersionLowByte             = 0x01;

    pIM0Data->SupportedHighByte          = 0x00;
    pIM0Data->SupportedLowByte           = 0x0E; /* I&M1 to I&M3 are supported */

    memset (&pIM0Data->OrderId [0], ' ', sizeof(pIM0Data->OrderId));
    memcpy ((char*)&pIM0Data->OrderId [0], (const char*)&ApplManagement.SdaiInitData.Info.OrderId [0], _MIN(sizeof (pIM0Data->OrderId), strlen (&ApplManagement.SdaiInitData.Info.OrderId [0])));
    memset (&pIM0Data->SerialNumber [0], ' ', sizeof(pIM0Data->SerialNumber));

    pReadRes->Header.Length    = sizeof (T_PN_IDENT_MAINTENANCE_0);
    pReadRes->Header.ErrorCode = SDAI_SERVICE_SUCCESS;
  }
  else if (pReadInd->Header.UseAs.Pn.Index == PN_INDEX_IDENT_AND_MAINTENANCE_1)
  {
    T_PN_IDENT_MAINTENANCE_1*   pIM1Data = (T_PN_IDENT_MAINTENANCE_1*)&pReadRes->Data [0];

    pIM1Data->Header.TypeHighByte    = _U16_HIGH_BYTE (PN_BLOCK_TYPE_IDENT_AND_MAINTENANCE_1);
    pIM1Data->Header.TypeLowByte     = _U16_LOW_BYTE (PN_BLOCK_TYPE_IDENT_AND_MAINTENANCE_1);

    pIM1Data->Header.LengthHighByte  = _U16_HIGH_BYTE (sizeof (T_PN_IDENT_MAINTENANCE_1) - 4);
    pIM1Data->Header.LengthLowByte   = _U16_LOW_BYTE (sizeof (T_PN_IDENT_MAINTENANCE_1) - 4);

    pIM1Data->Header.VersionHighByte = 0x01;
    pIM1Data->Header.VersionLowByte  = 0x00;

    memcpy (&pIM1Data->Function [0], &pFlashConfigData->IdentMaintenanceData.Function [0], sizeof (pIM1Data->Function));
    memcpy (&pIM1Data->Location [0], &pFlashConfigData->IdentMaintenanceData.Location [0], sizeof (pIM1Data->Location));

    pReadRes->Header.Length    = sizeof (T_PN_IDENT_MAINTENANCE_1);
    pReadRes->Header.ErrorCode = SDAI_SERVICE_SUCCESS;
  }
  else if (pReadInd->Header.UseAs.Pn.Index == PN_INDEX_IDENT_AND_MAINTENANCE_2)
  {
    T_PN_IDENT_MAINTENANCE_2*   pIM2Data = (T_PN_IDENT_MAINTENANCE_2*)&pReadRes->Data [0];

    pIM2Data->Header.TypeHighByte    = _U16_HIGH_BYTE (PN_BLOCK_TYPE_IDENT_AND_MAINTENANCE_2);
    pIM2Data->Header.TypeLowByte     = _U16_LOW_BYTE (PN_BLOCK_TYPE_IDENT_AND_MAINTENANCE_2);

    pIM2Data->Header.LengthHighByte  = _U16_HIGH_BYTE (sizeof (T_PN_IDENT_MAINTENANCE_2) - 4);
    pIM2Data->Header.LengthLowByte   = _U16_LOW_BYTE (sizeof (T_PN_IDENT_MAINTENANCE_2) - 4);

    pIM2Data->Header.VersionHighByte = 0x01;
    pIM2Data->Header.VersionLowByte  = 0x00;

    memcpy (&pIM2Data->Date[0], &pFlashConfigData->IdentMaintenanceData.Date [0], sizeof (pIM2Data->Date));

    pReadRes->Header.Length    = sizeof (T_PN_IDENT_MAINTENANCE_2);
    pReadRes->Header.ErrorCode = SDAI_SERVICE_SUCCESS;
  }
  else if (pReadInd->Header.UseAs.Pn.Index == PN_INDEX_IDENT_AND_MAINTENANCE_3)
  {
    T_PN_IDENT_MAINTENANCE_3*   pIM3Data = (T_PN_IDENT_MAINTENANCE_3*)&pReadRes->Data [0];

    pIM3Data->Header.TypeHighByte    = _U16_HIGH_BYTE (PN_BLOCK_TYPE_IDENT_AND_MAINTENANCE_3);
    pIM3Data->Header.TypeLowByte     = _U16_LOW_BYTE (PN_BLOCK_TYPE_IDENT_AND_MAINTENANCE_3);

    pIM3Data->Header.LengthHighByte  = _U16_HIGH_BYTE (sizeof (T_PN_IDENT_MAINTENANCE_3) - 4);
    pIM3Data->Header.LengthLowByte   = _U16_LOW_BYTE (sizeof (T_PN_IDENT_MAINTENANCE_3) - 4);

    pIM3Data->Header.VersionHighByte = 0x01;
    pIM3Data->Header.VersionLowByte  = 0x00;

    memcpy (&pIM3Data->Description[0], &pFlashConfigData->IdentMaintenanceData.Description [0], sizeof (pIM3Data->Description));

    pReadRes->Header.Length    = sizeof (T_PN_IDENT_MAINTENANCE_3);
    pReadRes->Header.ErrorCode = SDAI_SERVICE_SUCCESS;
  }
  else
  {
    pReadRes->Header.Length    = 0u;
    pReadRes->Header.ErrorCode = SDAI_SERVICE_ERR_INVALID_PARAMETER;
  }

  return (PROTO_FUNC_SUCCESS);
}

/*===========================================================================*/

/**
 * The function profinet_process_control_ind() processes the received control indication.
 * For Profinet no response is generated for a control indication.
 *
 * @return
 * - type : U8
 * - range: PROTO_FUNC_SUCCESS
 * @param[in] pControlInd
 * - type : struct T_DEMO_CONTROL_IND*
 * - range: whole address range is valid
 * @param[out] pControlRes
 * - type : struct T_DEMO_CONTROL_RES*
 * - range: whole address range is valid
 */
U8 profinet_process_control_ind (struct T_DEMO_CONTROL_IND* pControlInd, struct T_DEMO_CONTROL_RES* pControlRes)
{
  _TRACE (("profinet_process_control_ind\r\n"));

  _DEBUG_LOG ( ("\r\nControl indication:\r\n  Unit ID:     0x" USIGN32_HEX_FORMAT_STRING "\r\n  ControlCode: 0x" USIGN32_HEX_FORMAT_STRING "\r\n  Length:      %d\r\n",
                pControlInd->Header.UseAs.Pn.Id,
                pControlInd->Header.ControlCode,
                pControlInd->Header.Length) );

  pControlRes->Header.ControlCode = pControlInd->Header.ControlCode;
  pControlRes->Header.Length      = 0;

  switch (pControlInd->Header.ControlCode)
  {
    case SDAI_PN_CONTROL_BLINK:
    {
      PnBlink = 1;
      break;
    }

    case SDAI_CONTROL_RESET_TO_DEFAULTS:
    {
      /* The factory default is defined to no devicename and no IP address */
      memset(pFlashConfigData->DeviceName, 0x00, SDAI_DEVNAME_MAX_LEN);

      memset(pFlashConfigData->IpAddress, 0x00, sizeof (pFlashConfigData->IpAddress));
      memset(pFlashConfigData->Netmask,   0x00, sizeof (pFlashConfigData->Netmask));
      memset(pFlashConfigData->Gateway,   0x00, sizeof (pFlashConfigData->Gateway));

      /* Reset all I&M data */
      memset (&pFlashConfigData->IdentMaintenanceData.Function [0], ' ', sizeof (pFlashConfigData->IdentMaintenanceData.Function));
      memset (&pFlashConfigData->IdentMaintenanceData.Location [0], ' ', sizeof (pFlashConfigData->IdentMaintenanceData.Location));
      memset (&pFlashConfigData->IdentMaintenanceData.Date [0], ' ', sizeof (pFlashConfigData->IdentMaintenanceData.Date));
      memset (&pFlashConfigData->IdentMaintenanceData.Description [0], ' ', sizeof (pFlashConfigData->IdentMaintenanceData.Description));

      platform_send_event (EVENT_STORE_FLASH_DATA);
      break;
    }

    case SDAI_PN_CONTROL_RESET_COMMUNICATION_DATA:
    {
      /* The factory default is defined to no devicename and no IP address */
      memset(pFlashConfigData->DeviceName, 0x00, SDAI_DEVNAME_MAX_LEN);

      memset(pFlashConfigData->IpAddress, 0x00, sizeof (pFlashConfigData->IpAddress));
      memset(pFlashConfigData->Netmask,   0x00, sizeof (pFlashConfigData->Netmask));
      memset(pFlashConfigData->Gateway,   0x00, sizeof (pFlashConfigData->Gateway));

      platform_send_event (EVENT_STORE_FLASH_DATA);
      break;
    }

    case SDAI_CONTROL_CODE_CFG_DATA_INFO:
    {
      struct SDAI_CONTROL_CFG_DATA_INFO*    pControlCFGInfo = (struct SDAI_CONTROL_CFG_DATA_INFO*) pControlInd->Data;

#ifndef PROFINET_PLUGIN_ENABLED
      U16                                   Slot;


      Slot = _SDAI_PN_ID_TO_SLOT(U16, pControlCFGInfo->Id);

      _DEBUG_LOG ( ("Requested Configuration for Slot %d Subslot %d\r\n", Slot, _SDAI_PN_ID_TO_SUBSLOT(U16, pControlCFGInfo->Id)) );
      #if !PLATFORM_SLOW_PRINTF
      _DEBUG_LOG ( ("Input size: %d Output size: %d\r\n", pControlCFGInfo->InputSize, pControlCFGInfo->OutputSize) );
      _DEBUG_LOG ( ("Module Ident    : 0x" USIGN32_HEX_FORMAT_STRING "\r\n", pControlCFGInfo->CfgData.UseAs.Pn.IdentNumber) );
      _DEBUG_LOG ( ("Submodule Ident : 0x" USIGN32_HEX_FORMAT_STRING "\r\n", pControlCFGInfo->CfgData.UseAs.Pn.SubmoduleIdentNumber) );
      #endif

      /*---------------------------------------------------------------------*/

      /* Slot: 0 to 4 are always present when IO_DATA_HANDLING_MIRROR is disabled
       * Slot: 5 to 31 are always taken from the expected controller configuration during connection establishment
       * Slot: 32 to 64 depend on the currently used platform and can not be replaced by the controller
       */
#if IO_DATA_HANDLING_MIRROR
      if (Slot > 0)
#else
      if (Slot > 4 && Slot < 32)
#endif
      {
        U8    Type = SDAI_UNIT_TYPE_GENERIC_INPUT;


        if ( (pControlCFGInfo->InputSize > 0) && (pControlCFGInfo->OutputSize > 0) )
        {
          Type = SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT;
        }
        else if (pControlCFGInfo->InputSize > 0)
        {
          Type = SDAI_UNIT_TYPE_GENERIC_INPUT;
        }
        else if (pControlCFGInfo->OutputSize > 0)
        {
          Type = SDAI_UNIT_TYPE_GENERIC_OUTPUT;
        }

        /* if the module was pluged, it is replaced automatically */
        (void) profinet_plug_unit
        (
          &ApplManagement.UnitsManagement,
          Type,
          pControlCFGInfo->InputSize,
          pControlCFGInfo->OutputSize,
          pControlCFGInfo->CfgData.UseAs.Pn.IdentNumber,
          pControlCFGInfo->CfgData.UseAs.Pn.SubmoduleIdentNumber,
          pControlCFGInfo->Id,
          0u,
          pControlCFGInfo->CfgData.UseAs.Pn.ProfileId
        );
      }
#endif

      pControlRes->Header.ErrorCode = SDAI_SERVICE_SUCCESS;

      break;
    }

    case SDAI_PN_CONTROL_PARAM_END:
    {
      _DEBUG_LOG ( ("Param End received\r\n") );

      /* we have to update all input data */
      demo_update_input_data ();
      break;
    }

    default:
    {
      break;
    }
  }

  return (PROTO_FUNC_SUCCESS);
}

/*===========================================================================*/

/**
 * The function profinet_print_additional_status_data() prints the additional Profinet
 * specific data that can be retrieved by the function sdai_get_status().
 *
 * @return
 * - type : U8
 * - range: PROTO_FUNC_SUCCESS
 * @param[in] pStatusData
 * - type : struct SDAI_ADDSTATUS_DATA*
 * - range: whole address range is valid
 */
U8 profinet_print_additional_status_data (struct SDAI_ADDSTATUS_DATA* pStatusData)
{
  _TRACE (("profinet_print_additional_status_data\r\n"));

  /* only print the data if something has changed */
  if (memcmp (&TmpAddStatusData.UseAs.Pn, &pStatusData->UseAs.Pn, sizeof(TmpAddStatusData.UseAs.Pn)))
  {
   _DEBUG_LOG ( ("PN status data changed\r\n") );

    memcpy (&TmpAddStatusData.UseAs.Pn, &pStatusData->UseAs.Pn, sizeof(TmpAddStatusData.UseAs.Pn));
  }

  /* only print the data if something has changed */
  if (memcmp (&TmpAddStatusData.InterfaceData, &pStatusData->InterfaceData, sizeof(TmpAddStatusData.InterfaceData)))
  {
    U8 Port;

    for (Port = 0; Port < pStatusData->InterfaceData.NumberExistingEthernetPorts; Port++)
    {
      if (pStatusData->InterfaceData.EthPortData[Port].LinkState == SDAI_ADDSTATUS_LINK_UP)
      {
        _DEBUG_LOG ( ("Port: %d up\r\n", Port) );
        _DEBUG_LOG ( ("   MAU type: ") );
        switch (pStatusData->InterfaceData.EthPortData[Port].ConnectionType)
        {
          case SDAI_ADDSTATUS_CONNECTION_100MBIT_FULLDUPLEX:
            _DEBUG_LOG ( ("100 Mbit Full Duplex\r\n") );
            break;
          case SDAI_ADDSTATUS_CONNECTION_100MBIT_HALFDUPLEX:
            _DEBUG_LOG ( ("100 Mbit Half Duplex\r\n") );
            break;
          case SDAI_ADDSTATUS_CONNECTION_10MBIT_FULLDUPLEX:
            _DEBUG_LOG ( ("10 Mbit Full Duplex\r\n") );
            break;
          case SDAI_ADDSTATUS_CONNECTION_10MBIT_HALFDUPLEX:
            _DEBUG_LOG ( ("10 Mbit Half Duplex\r\n") );
            break;
        }

        _DEBUG_LOG ( ("   Cable Length: %d\r\n", pStatusData->InterfaceData.EthPortData[Port].CableLength) );
      }
      else
      {
        _DEBUG_LOG ( ("Port: %d down\r\n", Port) );
      }
    }

    memcpy(&TmpAddStatusData.InterfaceData, &pStatusData->InterfaceData, sizeof(TmpAddStatusData.InterfaceData));
  }

  return (PROTO_FUNC_SUCCESS);
}

/*===========================================================================*/

/**
 * The function profinet_pull_unit() removes one unit from the configuration data.
 *
 * @return
 * - type : U8
 * @param[in] pUnitManagement
 * - type : struct T_DEMO_UNITS_MANAGEMENT*
 * - range: whole address range is valid
 * @param[in] UnitId
 * - type : U32
 * - range: whole range is valid
 */
U8 profinet_pull_unit (struct T_DEMO_UNITS_MANAGEMENT* pUnitManagement, U32 UnitId)
{
  struct T_DEMO_UNIT*   pUnit;

  U8                    Result = SDAI_ERR_NOSUCHUNIT;

  unsigned              Index;


  _TRACE (("profinet_pull_unit\r\n"));

  pUnit = &pUnitManagement->UnitList[0u];

  for (Index = 0u; Index < SDAI_MAX_UNITS; Index++)
  {
    if (pUnit->Id == UnitId)
    {
      break;
    }

    pUnit++;
  }

  /*-------------------------------------------------------------------------*/

  if (Index < SDAI_MAX_UNITS)
  {
    pUnit->Id          = SDAI_INVALID_ID;
    pUnit->InputState  = SDAI_DATA_STATUS_INVALID;
    pUnit->OutputState = SDAI_DATA_STATUS_INVALID;
    pUnit->InputSize   = 0u;
    pUnit->OutputSize  = 0u;

    Result = sdai_pull_unit (UnitId);

    if (Result != SDAI_SUCCESS)
    {
      demo_print_error (Result);
    }
  }

  return (Result);
}

/*===========================================================================*/

/**
 * The function profinet_plug_unit() adds one unit to the configuration data.
 *
 * @return
 * - type : U8
 * @param[in] pUnitManagement
 * - type : struct T_DEMO_UNITS_MANAGEMENT*
 * - range: whole address range is valid
 * @param[in] Type
 * - type : U8
 * - range:
 * @param[in] InputSize
 * - type : U8
 * - range: whole range is valid
 * @param[in] OutputSize
 * - type : U8
 * - range: whole range is valid
 * @param[in] ModuleIdentNumber
 * - type : U32
 * - range: whole range is valid
 * @param[in] SubmoduleIdentnumber
 * - type : U32
 * - range: whole range is valid
 * @param[in] UnitId
 * - type : U32
 * - range: whole range is valid
 * @param[in] Flags
 * - type : U8
 * - range: bit field of the following defines (see SDAI manual PnUnitFlags):
 *   SDAI_PN_UNIT_FLAG_ISSAFETY |
 *   SDAI_PN_UNIT_FLAG_SUPPORT_MODULE_SUBSTITUTION |
 *   SDAI_PN_UNIT_HEAD_FLAG_FULL_IM_SUPPORT |
 *   SDAI_PN_UNIT_FLAG_SUPPORT_DATA_SUBSTITUTION
 * @param[in] ProfileId
 * - type : U8
 * - range: one of the following numbers:
 *   SDAI_PN_DEFAULT_PROFILE |
 *   SDAI_PN_DRIVE_PROFILE |
 *   SDAI_PN_ENCODER_PROFILE |
 *   SDAI_PN_INTELLIGENT_PUMP_PROFILE
 */
U8 profinet_plug_unit
  (
    struct T_DEMO_UNITS_MANAGEMENT* pUnitManagement,
    U8                              Type,
    U8                              InputSize,
    U8                              OutputSize,
    U32                             ModuleIdentNumber,
    U32                             SubmoduleIdentnumber,
    U32                             UnitId,
    U8                              Flags,
    U8                              ProfileId
  )
{
  struct T_DEMO_UNIT*   pUnit;

  U8                    Result = SDAI_ERR_NOSUCHUNIT;

  unsigned              Index;


  _TRACE (("profinet_plug_unit\r\n"));

  pUnit = &pUnitManagement->UnitList[0u];

  for (Index = 0u; Index < SDAI_MAX_UNITS; Index++)
  {
    if ( (pUnit->Id == UnitId) || (pUnit->Id == 0uL) || (pUnit->Id == SDAI_INVALID_ID) )
    {
      break;
    }

    pUnit++;
  }

  /*-------------------------------------------------------------------------*/

  if (Index < SDAI_MAX_UNITS)
  {
    pUnit->Id                                    = UnitId;
    pUnit->Type                                  = Type;
    pUnit->InputState                            = SDAI_DATA_STATUS_INVALID;
    pUnit->OutputState                           = SDAI_DATA_STATUS_INVALID;
    pUnit->InputSize                             = InputSize;
    pUnit->OutputSize                            = OutputSize;
    pUnit->CfgData.PeripheralOffsetInput         = SDAI_PERIPHERAL_OFFSET_DPRAM;
    pUnit->CfgData.PeripheralOffsetOutput        = SDAI_PERIPHERAL_OFFSET_DPRAM;
    pUnit->CfgData.UseAs.Pn.IdentNumber          = ModuleIdentNumber;
    pUnit->CfgData.UseAs.Pn.SubmoduleIdentNumber = SubmoduleIdentnumber;
    pUnit->CfgData.UseAs.Pn.ProfileId            = ProfileId;
    if (Type == SDAI_UNIT_TYPE_GENERIC_OUTPUT) {pUnit->CfgData.UseAs.Pn.Flags = Flags | SDAI_PN_UNIT_FLAG_SUPPORT_DATA_SUBSTITUTION;}
    else                                       {pUnit->CfgData.UseAs.Pn.Flags = Flags;}
    pUnit->pProtocolData                         = (void*) &ProfinetUnitData [Index];

    memset (&ProfinetUnitData[Index], 0x00, sizeof (T_PN_UNIT_DATA));

    NumberUnits = Index + 1u;

    do
    {
      Result = sdai_plug_unit_ext (pUnit->Type, pUnit->InputSize, pUnit->OutputSize, &pUnit->Id, &pUnit->CfgData);

    } while (Result == SDAI_ERR_SERVICE_PENDING);

    if (Result != SDAI_SUCCESS)
    {
      demo_print_error (Result);
    }
  }

  return (Result);
}

/*===========================================================================*/

/**
 * The function profinet_cyclic_timer() is called cyclically by the main sample application.
 * For Profinet it is used to implement the Profinet signal functionality.
 *
 * @return
 * - type : U8
 * - range: PROTO_FUNC_SUCCESS
 */
U8 profinet_cyclic_timer ()
{
  _TRACE (("profinet_cyclic_timer\r\n"));

  if (ApplManagement.StackStatus & (SDAI_STATE_MASK_HARDWARE_ERROR | SDAI_STATE_MASK_LICENSE_ERROR))
  {
    STATUS_LED_SWITCH (PN_LED_MODULE, LED_OFF);
    STATUS_LED_SWITCH (PN_LED_MODULE_ERR, LED_ON);
  }
  else if (ApplManagement.StackStatus & SDAI_STATE_MASK_CONNECTED)
  {
    STATUS_LED_SWITCH (PN_LED_MODULE, LED_ON);
    STATUS_LED_SWITCH (PN_LED_MODULE_ERR, LED_OFF);
  }
  else
  {
    profinet_module_led_blink (PN_LED_MODULE);
    STATUS_LED_SWITCH (PN_LED_MODULE_ERR, LED_OFF);
  }

  /*-------------------------------------------------------------------------*/

  if (PnBlink == 1)
  {
    profinet_blink();
  }

  return (PROTO_FUNC_SUCCESS);
}

/*===========================================================================*/

/**
 * The function profinet_button_event() is called by the main sample application
 * every time there is a change on the buttons of the evaluation board. The first
 * two buttons can be used to send an alarm to the PLC and the third and fourth
 * button changes the speed of the running light.
 *
 * @return
 * - type : U8
 * - range: PROTO_FUNC_SUCCESS
 * @param[in] Button
 * - type : U8
 * - range: BUTTON_1 | BUTTON_2 | BUTTON_3 | BUTTON_4
 */
U8 profinet_button_event (U8 Button)
{
  _TRACE (("profinet_button_event\r\n"));

  if (Button & BUTTON_1)
  {
    profinet_send_alarm (1);
  }

  if (Button & BUTTON_2)
  {
    struct T_DEMO_UNIT*   pUnit = (ApplManagement.UnitsManagement.UnitList + 2);
    T_PN_UNIT_DATA*       pPnUnitData = (T_PN_UNIT_DATA*)pUnit->pProtocolData;

    if (pPnUnitData->Flags & PN_UNIT_FLAG_DIAGNOSIS_PENDING)
    {
      pPnUnitData->Flags &= ~PN_UNIT_FLAG_DIAGNOSIS_PENDING;

      profinet_send_diagnosis (2, DiagnosisAlarmIndex, SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_DISAPPEARS);

      DiagnosisAlarmIndex += 1u;
      DiagnosisAlarmIndex  = (DiagnosisAlarmIndex == _NUMBER_ARRAY_ELEMENTS (PredefinedDiagnosisAlarms)) ? 0u : DiagnosisAlarmIndex;
    }
    else
    {
      pPnUnitData->Flags |= PN_UNIT_FLAG_DIAGNOSIS_PENDING;

      profinet_send_diagnosis (2, DiagnosisAlarmIndex, SDAI_DIAGNOSIS_STATUS_DIAGNOSTICS_APPEARS);
    }
  }

  if (Button & BUTTON_3)
  {
    if (Axis0Reference < 70) {Axis0Reference += 5;}
  }

  if (Button & BUTTON_4)
  {
    if (Axis0Reference > 0) {Axis0Reference -= 5;}
  }

  return (PROTO_FUNC_SUCCESS);
}

/*===========================================================================*/

/**
 * The function profinet_get_input_data() is called by the main sample application
 * and returns the new input data for a unit. 
 *
 * @return
 * - type : U8
 * - range: PROTO_FUNC_SUCCESS
 * @param[in] pInputUnit
 * - type : struct T_DEMO_UNIT*
 * - range: whole address range is valid
 */
U8 profinet_get_input_data (struct T_DEMO_UNIT* pInputUnit)
{
  T_PN_UNIT_DATA* pPnUnitData = (T_PN_UNIT_DATA*)pInputUnit->pProtocolData;

  _TRACE (("profinet_get_input_data\r\n"));

  /* we use the first input unit to transfer motor speed data */
  if (pInputUnit->Id == _SDAI_PN_SLOT_AND_SUBSLOT_TO_ID(1, 1))
  {
    *((U8*) pInputUnit->pInputData) = Axis0Reference;
  }
  else
  {
    if (pInputUnit->InputSize != 0)
    {
      *((U8*) pInputUnit->pInputData) += 1;
    }
  }

  if (pPnUnitData->Flags & PN_UNIT_FLAG_DIAGNOSIS_PENDING)
  {
    pInputUnit->InputState = SDAI_DATA_STATUS_INVALID;
  }
  else
  {
    pInputUnit->InputState = SDAI_DATA_STATUS_VALID;
  }

  return (PROTO_FUNC_SUCCESS);
}

/*===========================================================================*/

/**
 * The function profinet_set_output_data() is called by the main sample application
 * and provides the new output data for a unit. 
 *
 * @return
 * - type : U8
 * - range: PROTO_FUNC_SUCCESS
 * @param[in] pOutputUnit
 * - type : struct T_DEMO_UNIT*
 * - range: whole address range is valid
 */
U8 profinet_set_output_data (struct T_DEMO_UNIT* pOutputUnit)
{
  _TRACE (("profinet_set_output_data\r\n"));

  if (pOutputUnit->OutputState == SDAI_DATA_STATUS_VALID)
  {
    /* we use the first input unit to transfer the data */
    if (pOutputUnit->Id == _SDAI_PN_SLOT_AND_SUBSLOT_TO_ID(2, 1))
    {
      int output = 
          pOutputUnit->pOutputData[0] + 
          (pOutputUnit->pOutputData[1] >> 8) + 
          (pOutputUnit->pOutputData[2] >> 16) + 
          (pOutputUnit->pOutputData[3] >> 24);
          AXIS0_SET_REFERENCE(output);
    }
  }

  return (PROTO_FUNC_SUCCESS);
}

/*===========================================================================*/

/**
 * The function profinet_get_init_data() fills the SDAI_INIT structure with the
 * values needed for Profinet. Some of the values must be the same as used in the
 * GSDML file to establish a connection to a controller.
 *
 * @return
 * - type : U8
 * - range: PROTO_FUNC_SUCCESS
 * @param[in] pFlashData
 * - type : struct T_FLASH_DATA*
 * - range: whole address range is valid
 * @param[in] pInitData
 * - type : struct SDAI_INIT*
 * - range: whole address range is valid
 */
U8 profinet_get_init_data (T_CONFIG_DATA_APPL* pFlashData, struct SDAI_INIT* pInitData)
{
  _TRACE (("profinet_get_init_data\r\n"));

  pInitData->BackEnd = SDAI_BACKEND_PN;

  memcpy (&IpAddress [0], &pFlashData->IpAddress[0], sizeof(IpAddress));

  memcpy (pInitData->Ident.UseAs.Pn.Address, &pFlashData->IpAddress[0], sizeof(pInitData->Ident.UseAs.Pn.Address));
  memcpy (pInitData->Ident.UseAs.Pn.Netmask, &pFlashData->Netmask[0], sizeof(pInitData->Ident.UseAs.Pn.Netmask));
  memcpy (pInitData->Ident.UseAs.Pn.Gateway, &pFlashData->Gateway[0], sizeof(pInitData->Ident.UseAs.Pn.Gateway));

  memcpy (pInitData->Ident.UseAs.Pn.MacAddressDevice, pFlashHwData->MacAddress, sizeof(pInitData->Ident.UseAs.Pn.MacAddressDevice));
  memcpy (pInitData->Ident.UseAs.Pn.MacAddressPort1, pFlashHwData->MacAddressPort1, sizeof(pInitData->Ident.UseAs.Pn.MacAddressPort1));
  memcpy (pInitData->Ident.UseAs.Pn.MacAddressPort2, pFlashHwData->MacAddressPort2, sizeof(pInitData->Ident.UseAs.Pn.MacAddressPort2));

  /* enable dynamic IO configuration */
  pInitData->Info.Flags                      = (SDAI_DYNAMIC_IO_CONFIGURATION | SDAI_DATA_CONSISTENCY_IO_IMAGE);

  pInitData->Info.VendorID                   = PN_VENDOR_ID_SOFTING;
  pInitData->Info.UseAs.Pn.SoftwareRevision  = PN_SOFTWARE_REVISION;
  pInitData->Info.UseAs.Pn.HardwareRevision  = PN_HARDWARE_REVISION;

  /* set the alarm ack callback */
  pInitData->Callback.AlarmAckCbk = profinet_alarm_ack_callback;

  memset (&TmpAddStatusData, 0, sizeof(TmpAddStatusData));

  AlarmIndex          = 0u;
  DiagnosisAlarmIndex = 0u;
  NumberUnits         = 0u;

  return (PROTO_FUNC_SUCCESS);
}

/*===========================================================================*/

/**
 * The function profinet_add_unit() returns one unit to the configuration data.
 *
 * @return
 * - type : VOID
 * @param[in] pUnitManagement
 * - type : struct T_DEMO_UNITS_MANAGEMENT*
 * - range: whole address range is valid
 * @param[in] Type
 * - type : U8
 * - range:
 * @param[in] InputSize
 * - type : U8
 * - range: whole range is valid
 * @param[in] OutputSize
 * - type : U8
 * - range: whole range is valid
 * @param[in] ModuleIdentNumber
 * - type : U32
 * - range: whole range is valid
 * @param[in] SubmoduleIdentnumber
 * - type : U32
 * - range: whole range is valid
 * @param[in] UnitId
 * - type : U32
 * - range: whole range is valid
 * @param[in] Flags
 * - type : U8
 * - range: bit field of the following defines (see SDAI manual PnUnitFlags):
 *   SDAI_PN_UNIT_FLAG_ISSAFETY |
 *   SDAI_PN_UNIT_FLAG_SUPPORT_MODULE_SUBSTITUTION |
 *   SDAI_PN_UNIT_HEAD_FLAG_FULL_IM_SUPPORT |
 *   SDAI_PN_UNIT_FLAG_SUPPORT_DATA_SUBSTITUTION
 * @param[in] ProfileId
 * - type : U8
 * - range: one of the following numbers:
 *   SDAI_PN_DEFAULT_PROFILE |
 *   SDAI_PN_DRIVE_PROFILE |
 *   SDAI_PN_ENCODER_PROFILE |
 *   SDAI_PN_INTELLIGENT_PUMP_PROFILE
 * @param[in] PeripheralOffsetInput
 * - type : U16
 * - range: whole range is valid
 * @param[in] PeripheralOffsetOutput
 * - type : U16
 * - range: whole range is valid
 */
void profinet_add_unit
  (
    struct T_DEMO_UNITS_MANAGEMENT* pUnitManagement,
    U8                              Type,
    U8                              InputSize,
    U8                              OutputSize,
    U32                             ModuleIdentNumber,
    U32                             SubmoduleIdentnumber,
    U32                             UnitId,
    U8                              Flags,
    U8                              ProfileId,
    U16                             PeripheralOffsetInput,
    U16                             PeripheralOffsetOutput
  )
{
  struct T_DEMO_UNIT* pUnit = &pUnitManagement->UnitList[NumberUnits];

  _TRACE (("profinet_add_unit\r\n"));

  pUnit->Id                                    = UnitId;
  pUnit->Type                                  = Type;
  pUnit->InputSize                             = InputSize;
  pUnit->OutputSize                            = OutputSize;
  pUnit->CfgData.PeripheralOffsetInput         = PeripheralOffsetInput;
  pUnit->CfgData.PeripheralOffsetOutput        = PeripheralOffsetOutput;
  pUnit->CfgData.UseAs.Pn.IdentNumber          = ModuleIdentNumber;
  pUnit->CfgData.UseAs.Pn.SubmoduleIdentNumber = SubmoduleIdentnumber;
  pUnit->CfgData.UseAs.Pn.ProfileId            = ProfileId;
  if (Type == SDAI_UNIT_TYPE_HEAD)                {pUnit->CfgData.UseAs.Pn.Flags = Flags | SDAI_PN_UNIT_HEAD_FLAG_FULL_IM_SUPPORT;}
  else if (Type == SDAI_UNIT_TYPE_GENERIC_OUTPUT) {pUnit->CfgData.UseAs.Pn.Flags = Flags | SDAI_PN_UNIT_FLAG_SUPPORT_DATA_SUBSTITUTION;}
  else                                            {pUnit->CfgData.UseAs.Pn.Flags = Flags;}
  pUnit->pProtocolData                         = (void*) &ProfinetUnitData [NumberUnits];

  memset (&ProfinetUnitData[NumberUnits], 0x00, sizeof (T_PN_UNIT_DATA));

  NumberUnits++;

  return;
}

/*===========================================================================*/

/**
 * The function profinet_get_unit_data() returns the unit (module) configuration.
 * The following I/O configuration will be used as default:
   \verbatim
   ----------------- ----------- ----------- ------------- -------------
    SDAI Unit Index   Unit Type   Data Size  Ident Number  Ident Number Submodule
   ----------------- ----------- ----------- ------------- -------------
          0           Head            0       0x00000200    0x00000000
          1           Input           4       0x00000103    0x00000000
          2           Output          4       0x00000203    0x00000000
   \endverbatim
 * Upto 8 additional unit are possible, depending on the dip switch support of the related
 * evaluation board.
 * The head module uses the DAP from the GSDML file with IRT capabilities. It has
 * activated the Unit-Flag SDAI_PN_UNIT_FLAG_SUPPORT_MODULE_SUBSTITUTION to behave like
 * the DAP without IRT capabilities (GSDML Ident Number 0x00000100) if this is requested
 * by the controller configuration. This replacement is possible, because the RT capable DAP
 * covers a subset of functionality of the IRT capable DAP. The opposite substitution
 * is not possible.
 *
 * @return
 * - type : U8
 * - range: PROTO_FUNC_SUCCESS
 * @param[in] pUnitManagement
 * - type : struct T_DEMO_UNITS_MANAGEMENT*
 * - range: whole address range is valid
 */
U8 profinet_get_unit_data (struct T_DEMO_UNITS_MANAGEMENT* pUnitManagement)
{
  _TRACE (("profinet_get_unit_data\r\n"));

  /* plug the head module it must be in Slot 0, Subslot 1*/
  profinet_add_unit (pUnitManagement, SDAI_UNIT_TYPE_HEAD, 0, 0, 0x00000200, 0x00000000,
                     _SDAI_PN_SLOT_AND_SUBSLOT_TO_ID(0, 1), SDAI_PN_UNIT_FLAG_SUPPORT_MODULE_SUBSTITUTION,
                     SDAI_PN_DEFAULT_PROFILE, SDAI_PERIPHERAL_OFFSET_DPRAM, SDAI_PERIPHERAL_OFFSET_DPRAM);

  /* plug 32 Bit input unit - used as reference input (to plc)*/
  profinet_add_unit (pUnitManagement, SDAI_UNIT_TYPE_GENERIC_INPUT, 4, 0, 0x00000103, 0x00000000,
                     _SDAI_PN_SLOT_AND_SUBSLOT_TO_ID(1, 1), 0u,
                     SDAI_PN_DEFAULT_PROFILE, SDAI_PERIPHERAL_OFFSET_DPRAM, SDAI_PERIPHERAL_OFFSET_DPRAM);

  /* plug 32 Bit output unit - used as reference output (to regulator)*/
  profinet_add_unit (pUnitManagement, SDAI_UNIT_TYPE_GENERIC_OUTPUT, 0, 4, 0x00000203, 0x00000000,
                     _SDAI_PN_SLOT_AND_SUBSLOT_TO_ID(2, 1), 0u,
                     SDAI_PN_DEFAULT_PROFILE, SDAI_PERIPHERAL_OFFSET_DPRAM, SDAI_PERIPHERAL_OFFSET_DPRAM);

  return (PROTO_FUNC_SUCCESS);
}

/*===========================================================================*/
/**
 * The function profinet_print_status_text() returns the status of the PROFINET device
 * state as a textual string.
 *
 * <TABLE>
 * <TR><TH>Text</TH><TH>Description</TH></TR>
 * <TR><TD>CONNECT</TD><TD>Device is connected to at least one controller</TD></TR>
 * <TR><TD>ONLINE</TD><TD>Device is online and ready to receive request from the network</TD></TR>
 * <TR><TD>INIT</TD><TD>Device is initialized. That means stack is started but not configured with a valid configuration yet</TD></TR>
 * <TR><TD>UNKNOWN</TD><TD>Stack is in an unknown state (e.g. sdai_init() not called)</TD></TR>
 * </TABLE>
 *
 * @return
 * - type : char* the status as string
 */
const char* profinet_print_status_text (void)
{
  if (ApplManagement.StackStatus & SDAI_STATE_MASK_CONNECTED)
  {
    return ("CONNECT");
  }
  else if (ApplManagement.StackStatus & SDAI_STATE_MASK_ONLINE)
  {
    return ("ONLINE ");
  }
  else if (ApplManagement.StackStatus & SDAI_STATE_MASK_INIT)
  {
    return ("INIT   ");
  }

  return ("UNKNOWN  ");
}

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
