/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

#ifdef SDAI_SOCKET_INTERFACE

/******************************************************************************
DEFINES
******************************************************************************/

/** \addtogroup socket_def */
//@{
#define SOCK_USER_FLAG_FREE                0x80000000uL                         /**< Socket is free */
#define SOCK_USER_FLAG_SERVER              0x00010000uL                         /**< Socket is a server socket */
#define SOCK_USER_FLAG_ECHO                0x00020000uL                         /**< Socket echos all received data */

/*---------------------------------------------------------------------------*/

#define SOCK_FIND_UDP_SOCKETS              0x01
#define SOCK_FIND_TCP_SOCKETS              0x02
#define SOCK_FIND_TCP_CLIENT_SOCKETS       0x03
#define SOCK_FIND_TCP_SERVER_SOCKETS       0x04
//@}

/******************************************************************************
TYPEDEFS
******************************************************************************/

/** \addtogroup socket_struct */
//@{
typedef struct _T_TCP_CONNECTION_STATUS
{
  T_SDAI_SOCK_TCP_CONNECTION_LOCAL_STATUS   Connection [MAX_NUMBER_CONNECTIONS_PER_SOCKET];

} T_TCP_CONNECTION_STATUS;

/*---------------------------------------------------------------------------*/

typedef struct _T_SOCKET_ELEMENT
{
  T_SDAI_SOCK_ADDR         LocalAddress;
  T_SDAI_SOCK_ADDR         RemoteAddress;
  U32                      Flags;

  unsigned short           SocketNumber;

  T_SDAI_SOCK_IO_CONTROL   SocketStatus;
  T_TCP_CONNECTION_STATUS  TcpConnectionStatus;

  U32                      ReceiveCounter;
  U32                      SentCounter;

} T_SOCKET_ELEMENT;
//@}

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

/** \addtogroup socket_func */
//@{
T_SOCKET_ELEMENT* socket_open                     (U32, T_SDAI_SOCK_ADDR*, T_SDAI_SOCK_ADDR*);
void              socket_close                    (T_SOCKET_ELEMENT*);
void              socket_close_connection         (U16, T_SDAI_SOCK_TCP_CONNECTION_LOCAL_STATUS*);
void              socket_send                     (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, U32, const U8*);
T_SOCKET_ELEMENT* socket_find_socket              (U8, U16, U16, U32);

void              handle_tcp_console_connect      (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*);
void              handle_tcp_console_command      (char*, int, T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*);
void              tcp_console_prompt              (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*);

#ifdef SDAI_WEBSERVER
void              web_handle_request              (char*, int, T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*);
#endif
//@}

#endif /* SDAI_SOCKET_INTERFACE */

/* EOF */
