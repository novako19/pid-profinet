/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

/******************************************************************************
MAINPAGE
******************************************************************************/

/**
 * @mainpage
 *
 * \copyright
 * 2015 SOFTING Industrial Automation GmbH @n @n
 * No part of these instructions may be reproduced (printed material, photocopies,
 * microfilm or other method) or processed, copied or distributed using electronic
 * systems in any form whatsoever without prior written permission of SOFTING Industrial Automation GmbH.
 * The producer reserves the right to make changes to the scope of supply as well
 * as changes to technical data, even without prior notice. A great deal of attention
 * was made to the quality and functional integrity in designing, manufacturing and
 * testing the system. However, no liability can be assumed for potential errors that
 * might exist or for their effects. Should you find errors, please inform your
 * distributor of the nature of the errors and the circumstances under which they occur.
 * We will be responsive to all reasonable ideas and will follow up on them,
 * taking measures to improve the product, if necessary.
 * We call your attention to the fact that the company name and trademark as
 * well as product names are, as a rule, protected by trademark, patent and product
 * brand laws. @n @n
 * All rights reserved.
 *
 * \b Content
 * - @subpage demo_appl_descr
 *   - \ref demo_appl_overview
 *   - \ref demo_appl_prot_spec
 *   - \ref demo_appl_socket_con
 *   - \ref demo_appl_defines
 *   - \ref demo_appl_structures
 *   - \ref demo_appl_functions
 * - @subpage platform_porting_descr
 *   - \ref platform_defines
 *   - \ref platform_structures
 *   - \ref platform_functions
 * - @subpage version_history
 */

/******************************************************************************
SUBPAGES
******************************************************************************/

/** @page demo_appl_descr Common Demo Application Description
 *
 * The basic functionality of the demo application is described in the following chapters:
 * - @subpage demo_appl_overview
 * - @subpage demo_appl_prot_spec
 * - @subpage demo_appl_socket_con
 *
 * A detailed description of all definitions, data structures and functions can be found in the following chapters:
 * - @subpage demo_appl_defines
 * - @subpage demo_appl_structures
 * - @subpage demo_appl_functions
 *
 * Source code:
 * - @subpage demo_source
 * - @subpage demo_header
 * - @subpage ethercat_source
 * - @subpage ethernetip_source
 * - @subpage modbus_source
 * - @subpage powerlink_source
 * - @subpage profinet_source
 * - @subpage profibus_source
 * - @subpage socket_source
 * - @subpage socket_header
 * - @subpage socket_con_source
 * - @subpage web_source
 * - @subpage firmware_source
 *
 */

/******************************************************************************/

/** @page demo_appl_overview Overview
 *
 * @section structure Application Structure
 * This document describes the functionality of the SDAI demo application. The demo
 * application shows you how to communicate with the Device Protocol Software via the Simple
 * Device Application Interface (SDAI). It is a sample application aimed at quickly getting you
 * started with implementing your own custom applications. The services and functions of the
 * Simple Device Application Interface, which are used by the demo application, are described
 * in detail by the SDAI documentation.
 *
 * The following figure shows the structure of the demo application:
 * @image html Application_Structure.jpg
 *
 * The demo application supports the protocols PROFINET IO, EtherNet/IP, EtherCAT, Modbus TCP, Ethernet Powerlink and PROFIBUS DP.
 * For better readability the source code is subdivided into different files. One file for the protocol independent
 * functionality (@subpage demo_source) and one file for each supported protocol (@subpage profinet_source,
 * @subpage ethernetip_source, @subpage ethercat_source, @subpage modbus_source, @subpage powerlink_source, @subpage profibus_source). The interface between the
 * protocol independent and the protocol specific modules is realized with function pointers. The function
 * pointers are set during initialization depending on the selected protocol.
 *
 * The demo application can be divided into the following functional sections:
 *
 * - Initializing the Device Protocol Software\n
 *   The function sdai_init() is used to set identification data (e.g. IP address, MAC address),
 *   device specific data (e.g. vendor ID, device ID) and to register the application specific
 *   callback functions.
 *
 * - Plugging of some I/O units\n
 *   The function sdai_plug_unit() is used to plug different input and output modules.
 *
 * - Starting the Protocol Software\n
 *   The function sdai_plug_unit() is used to signal that the plugging is completed. This action starts
 *   the Protocol Software and the network interface is Online.
 *
 * - Handling the events of the Protocol Software\n
 *   The Demo Application processes all events of the Protocol Software.
 *   These events (e.g. Read indication, Output data changed) are signaled by the Protocol
 *   Software by calling the respective callback function of the Demo Application.
 *
 * - Cyclic process data exchange\n
 *   The Demo Application cyclically updates the input data and reads the stack status.
 *   The cycle time can be configured with the define #CYCLE_TIME_IN_MS. New Output data are
 *   signaled by the Device Protocol Software via the respective callback function.
 *
 * - Acyclic communication\n
 *   A Master or configuration tool can access the Device by acyclic services. The Demo
 *   Application handles the read and write indications and sends a response back. The detailed handling
 *   is protocol specific and implemented in the module of the currently active protocol.
 *
 * @section running_light Running Light Sample
 *
 * The demo application implements a running light which is controlled by the remote PLC. The
 * following figure shows how the running light sample is realized:
 * @image html DemoApplicationRuntimeView.png
 * -# Via push buttons #BUTTON_3 and #BUTTON_4 on the evaluation board the speed of the running light can
 *    be increased or decreased.
 * -# The demo application reads the push buttons, updates the LED speed process variable and calls the
 *    function sdai_set_data() to transfer the LED speed to the protocol stack.
 * -# The protocol stack when transfers the LED speed via the input process data frame to the remote PLC.
 * -# The PLC program receives the LED speed and adapts its speed of changing the output data. The PLC
 *    transfers the LED output via the output process data frame to the evaluation board.
 * -# The demo application is informed that new output data are available. For that purpose the SDAI calls
 *    the function demo_output_data_callback(). Within this function the event #EVENT_OUTPUT_DATA_CHANGED is
 *    set to invoke the demo application task.
 * -# Within the function demo_update_output_data() the received output data are processed and the respective
 *    LEDs on the evaluation board are set.
 *
 * @section display LCD Display
 *
 * Some target platforms support a display to show information. What information is shown on the display depends on the
 * target hardware (see platform_visualize_data())
 *
 * @section tcp_ip TCP/IP Communication
 *
 * The files @subpage socket_source and @subpage socket_con_source are provided to demonstrate the usage of the Softing
 * socket interface. These components also provide access to the demo application via TCP sockets, e.g. to read the
 * current operating status, read/write IO data, change the module configuration. See \ref demo_appl_socket_con.
 *
 * The file @subpage web_source implements a simple web server example to show how web pages can be delivered with the SDAI socket interface
 *
 */

/******************************************************************************/

/** @page demo_appl_prot_spec Protocol specific Features
 *
 * @section pbdp PROFIBUS DP
 * The PROFIBUS DP Demo Application implements a simple failsafe handling. The failsafe mode can be configured
 * by the DP-master via the parameterization frame. The following failsafe modes are supported:
 * - set outputs to zero
 * - use last valid output values
 * - use a failsafe value
 *
 * In case of a master failure the configured failsafe action is performed for the module plugged to slot #SLOT_LED_VALUE_OUTPUT_UNIT.
 *
 * Via push buttons #BUTTON_1 and #BUTTON_2 on the evaluation board diagnosis messages can be generated. Push button
 * #BUTTON_1 triggers the status of the LED speed input module form VALID to INVALID and vice versa. Push button #BUTTON_2 can be used
 * to create channel-related diagnosis messages. Each time the button #BUTTON_2 is pushed the next diagnosis entry of the array ActualDiagMsg (see #T_DIAGNOSIS_DESCRIPTOR)
 * is send, see function profibus_button_event(). Thus creating, changing and removing of channel-related diagnosis entries
 * can be easily evaluated. In addition a device-related diagnosis is created which counts the number of cyclic connection restarts.
 * The delivered GSD file shows help texts for both channel- and device-related diagnosis messages.
 *
 * The used default I/O configuration is formed by 2 modules controlling the running light sample, the before-mentioned Led Speed Input and LED Value Output modules; and
 * 3 additional modules, which are 1 word input, 2 byte output and 1 word input/output respectively. In addition an empty module can be plugged to any slot if the respective slot is unused.
 * However, if an empty module is plugged to slot 0 or 1 the running light sample will not work. To support Dynamic I/O configuration, the arrays SlotDefinition and ModuleDescription
 * are defined. The first one describes the modules that can be plugged in each Slot. The second array, describe the different parameters of the modules that are supported
 * in the Sample application (i.e. Type, InputSize, OutputSize etc., see #T_MODULE_DESCRIPTOR). Whereas in slots 2 to 4 different modules are supported, the LED speed Input module
 * and LED value Output module are fixed in Slots 0 and 1. Trying to reconfigure slots 0 and/or 1 will cause a configuration error. In this case a Cfg-fault is signalled to the
 * DP-master and the slave will not enter Data-Exchange mode.
 *
 * The following figure shows the default PROFIBUS module structure of this sample application. As already mentioned the modules plugged on slots 2 to 4 can freely be plugged to another slot.
 * Only slots 0 and 1 are fixed. In addition the module structure shows all objects which are accessible via acyclic Read/Write services.
 * @image html PbModuleStructure.png
 *
 * @section pnio PROFINET IO
 * A Controller or Supervisor can access the Device by acyclic read and write commands.
 * For write indications Index 1 and I&M1 to I&M3 are supported and for read indications the
 * Index 1, I&M0 to I&M 3 and I&M filter data can be accessed. Requests to all
 * other indices are responded with the error "Invalid index" (SDAI_SERVICE_ERR_INVALID_PARAMETER).
 *
 * The following figure shows the default PROFINET IO module/submodule structure of this sample application. The modules plugged on slots 0 to 4 are fixed. The other slots can be plugged dynamically
 * depending on the the platform. In addition the module structure shows all objects which are accessible via acyclic Read/Write services.
 * @image html PnModuleStructure.png
 *
 * @section eip EtherNet/IP
 * A remote EtherNet/IP client can access the device objects with explicit messaging services.
 * These EtherNet/IP specific services are mapped by the SDAI to the Read, Write and Control
 * callback functions. The EtherNet/IP Demo Application implements a simple data object that
 * can be written to and read from. This data object can be accessed at Class ID = #EIPS_DEMO_OBJECT_VENDOR_SPEC_CLASS_ID,
 * Instance ID = #EIPS_DEMO_OBJECT_INSTANCE_ID and Attribute ID = #EIPS_DEMO_OBJECT_ATTRIBUTE_ID.
 * The length of this object is #EIPS_DEMO_OBJECT_LENGTH. The EtherNet/IP explicit messaging
 * service Reset (service code 0x05) can be used to clear the content of the data object.
 *
 * @section mbtcp Modbus TCP
 * The Modbus Demo Application implements a simple data memory that can be written to and read from.
 * The memory can be accessed on the address #MB_DEMO_OBJECT_START_ADDRESS with all Modbus services
 * mapped to the SDAI (see the SDAI documentation for a list of supported services).
 *
 * @section ecat EtherCAT
 * EtherCAT uses CoE (CANopen over EtherCAT) for the service data access. The EtherCAT Slave
 * Protocol Software already implements the standard objects which must be supported by every
 * EtherCAT slave device. In addition this demo application implements a object dictionary with
 * some application-specific objects. Access to a CANopen object dictionary is performed via SDO
 * Upload/Download and SDO Information services. These CoE services are mapped by the SDAI to
 * the Read and Write callback. The demo application shows how these CoE services are used
 * to access the object dictionary. The following objects are implemented by this demo application:
 *
 * <TABLE><TR>
 * <TH>Object</TH><TH>Index</TH><TH>Description</TH>
 * </TR><TR>
 * <TD>Output Synch Parameter</TD><TD>0x1C32</TD><TD>Holds the synchronization parameters for the output data.</TD>
 * </TR><TR>
 * <TD>Input Synch Parameter</TD><TD>0x1C33</TD><TD>Holds the synchronization parameters for the input data.</TD>
 * </TR><TR>
 * <TD>Test Read Write</TD><TD>0x2000</TD><TD>Simple data buffer for test purposes which can be read and written.</TD>
 * </TR><TR>
 * <TD>Fix Input Module</TD><TD>0x6000</TD><TD>Represents TxPDO 1. The mapping is fixed for this PDO. Used for the sample PLC program.</TD>
 * </TR><TR>
 * <TD>Variable Input Module</TD><TD>0x6001</TD><TD>Represents TxPDO 2. The mapping is variable for this PDO.</TD>
 * </TR><TR>
 * <TD>Fix Output Module</TD><TD>0x7000</TD><TD>Represents RxPDO 1. The mapping is fixed for this PDO. Used for the sample PLC program.</TD>
 * </TR><TR>
 * <TD>Variable Output Module</TD><TD>0x7001</TD><TD>Represents RxPDO 2. The mapping is variable for this PDO.</TD>
 * </TR></TABLE>
 */

/******************************************************************************/

/** @page demo_appl_socket_con Socket console
 *
 * To show the functionality of the SDAI socket interface the Demo Application implements a socket console which can be
 * accessed over TCP port 50000 (#TCP_SERVER_CONSOLE_PORT). After connecting the following string is printed
 *
 * \verbatim
   You are connected to the Softing command shell, Version 1.00 (Type "help" for command overview)

   [IP-Address]:
\endverbatim
 *
 * The command prompt [IP-Address] shows that the console is ready to receive commands. You can get a description of all supported command
 * by entering the "help" command. The following list shows all currently supported commands.
 *
 * <b>Socket interface related commands:</b>
 * <TABLE>
 * <TR><TH>Command</TH><TH>Description</TH></TR>
 * <TR><TD>socket_overview</TD><TD>Prints the status of all sockets (#_USAGE_SOCKET_OVERVIEW)</TD></TR>
 * <TR><TD>socket_status</TD><TD>Prints the detailed status of one socket (#_USAGE_SOCKET_STATUS)</TD></TR>
 * <TR><TD>open</TD><TD>Opens a new TCP/UPD socket (#_USAGE_OPEN)</TD></TR>
 * <TR><TD>close</TD><TD>Closes an existing TCP/UDP socket (#_USAGE_CLOSE)</TD></TR>
 * <TR><TD>connect</TD><TD>Connects to a remote TCP server socket (#_USAGE_CONNECT)</TD></TR>
 * <TR><TD>disconnect</TD><TD>Disconnects from a remote TCP server socket or a connection to a local TCP server socket (#_USAGE_DISCONNECT)</TD></TR>
 * <TR><TD>send</TD><TD>Sends data over an existing TCP/UDP socket (#_USAGE_SEND)</TD></TR>
 * <TR><TD>echo</TD><TD>Enables/disables echo on a local TCP/UDP socket (#_USAGE_ECHO)</TD></TR>
 * <TR><TD>filter</TD><TD>Sets an IP address filter for a local TCP server socket (#_USAGE_FILTER)</TD></TR>
 * <TR><TD>add_membership</TD><TD>Adds a local UDP socket to a multicast group (#_USAGE_ADD_MEM)</TD></TR>
 * <TR><TD>del_membership</TD><TD>Deletes a local UDP socket from a multicast group (#_USAGE_DEL_MEM)</TD></TR>
 * <TR><TD>socket_restart</TD><TD>Restarts the SDAI socket interfacec (#_USAGE_SOCK_RESTART)</TD></TR>
 * </TABLE>
 *
 * <b>Protocol stack related commands:</b>
 * <TABLE>
 * <TR><TH>Command</TH><TH>Description</TH></TR>
 * <TR><TD>stack_status</TD><TD>Prints the status of protocol stack (#_USAGE_STACK_STATUS)</TD></TR>
 * <TR><TD>stack_print_unitconfig</TD><TD>Prints the current unit configuration (#_USAGE_UNITCONFIG)</TD></TR>
 * <TR><TD>stack_input_data</TD><TD>Sets the input data of an unit (#_USAGE_INPUT_DATA)</TD></TR>
 * <TR><TD>stack_output_data</TD><TD>Prints the current output data of an unit (#_USAGE_OUTPUT_DATA)</TD></TR>
 * <TR><TD>stack_plug_unit</TD><TD>Add/replace a unit to the configuration (#_USAGE_PLUG)</TD></TR>
 * <TR><TD>stack_pull_unit</TD><TD>Remove a unit from the configuration (#_USAGE_PULL)</TD></TR>
 * <TR><TD>send_diagnosis</TD><TD>Sends a diagnosis for an unit (#_USAGE_DIAG)</TD></TR>
 * <TR><TD>stack_restart</TD><TD>Restarts the protocol stack (#_USAGE_RESTART_STACK)</TD></TR>
 * </TABLE>
 *
 * <b>Additional commands:</b>
 * <TABLE>
 * <TR><TH>Command</TH><TH>Description</TH></TR>
 * <TR><TD>firmware</TD><TD>Updates the firmware of the evaluation board (#_USAGE_FIRMWARE)</TD></TR>
 * </TABLE>
 */

/******************************************************************************/

/** @page version_history Version History
 * <TABLE><TR>
 * <TH>Date:</TH><TH>Version:</TH><TH>Description:</TH>
 * </TR><TR>
 * <TD>07.06.2010</TD><TD>1.00</TD>
 * <TD>
 * - First release with the communication protocols PROFINET, EtherNet/IP, Modbus TCP and EtherCAT used within a single, common demo application.
 * </TD></TR><TR>
 * <TD>29.12.2012</TD><TD>1.10</TD>
 * <TD>
 * - Integrate Powerlink support
 * </TD></TR><TR>
 * <TD>30.06.2013</TD><TD>1.20</TD>
 * <TD>
 * - Sample application restructured\n
 * - Implement variable PDO mapping/assignment for EtherCAT\n
 * - Add SDAI socket interface\n
 * - Add socket command console\n
 * - Documentation reworked
 * </TD></TR><TR>
 * <TD>16.09.2013</TD><TD>1.21</TD>
 * <TD>
 * - Remove EthernetIP specific network data from flash data\n
 * - Add SDAI socket interface for EthernetIP\n
 * </TD></TR><TR>
 * <TD>07.10.2013</TD><TD>1.22</TD>
 * <TD>
 * - Implement EthernetIP Modul Status LED\n
 * - Remove EthernetIP IO LED\n
 * </TD></TR><TR>
 * <TD>04.11.2013</TD><TD>1.30</TD>
 * <TD>
 * - Integrate PROFIBUS DP support\n
 * - Running light sample described\n
 * </TD></TR><TR>
 * <TD>20.11.2013</TD><TD>1.31</TD>
 * <TD>
 * - Dynamic IO Configuration supported for PROFIBUS DP\n
 * </TD></TR><TR>
 * <TD>11.12.2013</TD><TD>1.32</TD>
 * <TD>
 * - Object dictionary with acyclic Read/Write access for PROFIBUS DP implemented\n
 * </TD></TR><TR>
 * <TD>11.06.2014</TD><TD>1.33</TD>
 * <TD>
 * - minor bug fixes related to the socket communication\n
 * - handle acyclic Initiate/Abort for PROFIBUS DP\n
 * </TD></TR><TR>
 * <TD>22.09.2014</TD><TD>1.34</TD>
 * <TD>
 * - Dynamic IO Configuration supported for PROFINET IO\n
 * - Alternative IO data handling implemented (see #IO_DATA_HANDLING_MIRROR)\n
 * - I&M objects 1-3 supported for PROFINET IO\n
 * - Sample Application restructured. Webserver and SMTP client moved to platform specific part\n
 * - Documentation for socket.c and socket_con.c added\n
 * </TD></TR><TR>
 * <TD>04.05.2015</TD><TD>1.35</TD>
 * <TD>
 * - Use assertions to check SDAI parameters\n
 * - Remove some debug code to simplify application\n
 * - Implement direct peripheral access for some units (only on some platforms/protocols)\n
 * - Add implementation of web server example on SDAI socket interface\n
 * - Porting layer documentation improved
 * </TD></TR></TABLE>
 */

/******************************************************************************
MODULES
******************************************************************************/

/** @defgroup demo_appl Common Demo Application */

/**
 * @defgroup demo_appl_defines Defines
 * @ingroup demo_appl
 *
 * @defgroup pn_def PROFINET IO
 * @ingroup demo_appl_defines
 *
 * @defgroup pb_defines PROFIBUS
 * @ingroup demo_appl_defines
 *
 * @defgroup mb_defines ModbusTCP
 * @ingroup demo_appl_defines
 *
 * @defgroup ecat_def EtherCAT
 * @ingroup demo_appl_defines
 *
 * @defgroup epl_def Powerlink
 * @ingroup demo_appl_defines
 *
 * @defgroup eip_def Ethernet/IP
 * @ingroup demo_appl_defines
 *
 * @defgroup socket_def Socket
 * @ingroup demo_appl_defines
 *
 * @defgroup util_def Utility
 * @ingroup demo_appl_defines
 */

/**
 * @defgroup demo_appl_structures Data Structures
 * @ingroup demo_appl
 *
 * @defgroup pn_struct PROFINET IO
 * @ingroup demo_appl_structures
 *
 * @defgroup pb_struct PROFIBUS
 * @ingroup demo_appl_structures
 *
 * @defgroup ecat_struct EtherCAT
 * @ingroup demo_appl_structures
 *
 * @defgroup epl_struct Powerlink
 * @ingroup demo_appl_structures
 *
 * @defgroup socket_struct Socket
 * @ingroup demo_appl_structures
 */

/**
 * @defgroup demo_appl_functions Functions
 * @ingroup demo_appl
 *
 * @defgroup pn_func PROFINET IO
 * @ingroup demo_appl_functions
 *
 * @defgroup pb_func PROFIBUS
 * @ingroup demo_appl_functions
 *
 * @defgroup mb_func ModbusTCP
 * @ingroup demo_appl_functions
 *
 * @defgroup ecat_func EtherCAT
 * @ingroup demo_appl_functions
 *
 * @defgroup epl_func Powerlink
 * @ingroup demo_appl_functions
 *
 * @defgroup eip_func Ethernet/IP
 * @ingroup demo_appl_functions
 *
 * @defgroup socket_func Socket
 * @ingroup demo_appl_functions
 *
 * @defgroup util_func Utility
 * @ingroup demo_appl_functions
 */

/**
 * @defgroup demo_appl_files Source Files
 * @ingroup demo_appl
 */

/******************************************************************************/

/**
 * @defgroup demo_source demo.c
 * @ingroup demo_appl_files
 * \includelineno demo.c
 */

/**
 * @defgroup demo_header demo.h
 * @ingroup demo_appl_files
 * \includelineno demo.h
 */

/**
 * @defgroup ethercat_source ethercat.c
 * @ingroup demo_appl_files
 * \includelineno ethercat.c
 */

/**
 * @defgroup ethernetip_source ethernetip.c
 * @ingroup demo_appl_files
 * \includelineno ethernetip.c
 */

/**
 * @defgroup modbus_source modbus.c
 * @ingroup demo_appl_files
 * \includelineno modbus.c
 */

/**
 * @defgroup powerlink_source powerlink.c
 * @ingroup demo_appl_files
 * \includelineno powerlink.c
 */

/**
 * @defgroup profinet_source profinet.c
 * @ingroup demo_appl_files
 * \includelineno profinet.c
 */

/**
 * @defgroup profibus_source profibus.c
 * @ingroup demo_appl_files
 * \includelineno profibus.c
 */

/**
 * @defgroup socket_source socket.c
 * @ingroup demo_appl_files
 * \includelineno socket.c
 */

/**
 * @defgroup socket_header socket.h
 * @ingroup demo_appl_files
 * \includelineno socket.h
 */

/**
 * @defgroup socket_con_source socket_con.c
 * @ingroup demo_appl_files
 * \includelineno socket_con.c
 */

/**
 * @defgroup web_source web.c
 * @ingroup demo_appl_files
 * \includelineno web.c
 */

/**
 * @defgroup firmware_source firmware.c
 * @ingroup demo_appl_files
 * \includelineno firmware.c
 */