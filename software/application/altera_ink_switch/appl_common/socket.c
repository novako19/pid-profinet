/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

#ifdef SDAI_SOCKET_INTERFACE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include "sdai.h"

#include "demo_platform.h"
#include "demo.h"

#include "sdai_socket.h"

#include "socket.h"

/******************************************************************************
DEFINES
******************************************************************************/

/** \addtogroup socket_def */
//@{
#define TCP_SERVER_CONSOLE_PORT            50000 /**< port number used for the tcp console */
#define TCP_SERVER_WEB_PORT                80    /**< port number used for the tcp console */
//@}

/******************************************************************************
TYPEDEFS
******************************************************************************/


/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

/** \addtogroup socket_func */
//@{

/*--- SDAI callback functions -----------------------------------------------*/

void               socket_state_changed_callback   (U16 Socket);
void               socket_data_received_callback   (T_SDAI_SOCK_DATA_RECEIVED_IND*);

/*--- Local functions -------------------------------------------------------*/

static void        socket_print_error              (T_SOCKET_RESULT Error);
static void        socket_process_state_ind        (void);
static void        socket_process_receive_ind      (void);
//@}

/******************************************************************************
PUBLIC DATA
******************************************************************************/

T_SOCKET_ELEMENT                      Socket [MAX_NUMBER_SUPPORTED_SOCKETS];

/******************************************************************************
PRIVATE DATA
******************************************************************************/

static T_SDAI_SOCK_INIT               SocketInit;
static T_SDAI_SOCK_IO_CONTROL         SocketIOControl;
static U8                             SocketDataBuffer [SOCKET_FIFO_SIZE];
static T_SDAI_SOCK_DATA_RECEIVED_IND  SocketRcvInd;
static T_BITSET64                     SocketStateInd;

/******************************************************************************
FUNCTION IMPLEMENTATION
******************************************************************************/

/**
 * The function demo_bitset64_or_bitset64() executes a logical OR of the
 * bitfields Src1 and Scr2. The result of the OR operation is stored in Dest.
 *
 * @return
 * - type  : void
 * @param[out] pBitsetDest
 * - type : T_BITSET64*
 * - range: the whole address range is valid, except NULL
 * @param[in] pBitsetSrc1
 * - type : const T_BITSET64*
 * - range: the whole address range is valid, except NULL
 * @param[in] pBitsetSrc2
 * - type : const T_BITSET64*
 * - range: the whole address range is valid, except NULL
 * @pre The memory of the bitfields must be provided by the caller.
 */
void bitset64_or_bitset64 (T_BITSET64* pBitsetDest, const T_BITSET64* pBitsetSrc1, const T_BITSET64* pBitsetSrc2)
{
  int       ByteIndex;


  for (ByteIndex = 7; ByteIndex >= 0; ByteIndex--)
  {
    (*pBitsetDest) [ByteIndex] = (U8) (((*pBitsetSrc1) [ByteIndex]) | ((*pBitsetSrc2) [ByteIndex]));
  }

  return;
}

/*===========================================================================*/

/**
 * The function socket_get_free_socket() returns a pointer to a free socket
 * element if available, otherwise NULL.
 *
 * @return
 * - type  : T_SOCKET_ELEMENT*
 */
T_SOCKET_ELEMENT* socket_get_free_socket ()
{
  unsigned      SocketIndex;

  for (SocketIndex = 0; SocketIndex < MAX_NUMBER_SUPPORTED_SOCKETS; SocketIndex++)
  {
    if (Socket [SocketIndex].Flags & SOCK_USER_FLAG_FREE)
    {
      return (&Socket [SocketIndex]);
    }
  }

  return (NULL);
}


/*===========================================================================*/

/**
 * The function socket_find_socket() searches all socket element for the requested
 * socket. If the element is found it returns the address, otherwise NULL.
 *
 * @return
 * - type  : T_SOCKET_ELEMENT*
 * @param[in] SocketType specifies which socket shall be found
 * - type : U8
 * - range: SOCK_FIND_TCP_CLIENT_SOCKETS | SOCK_FIND_TCP_SERVER_SOCKETS | SOCK_FIND_UDP_SOCKETS | SOCK_FIND_ALL_SOCKETS
 * @param[in] LocalPort LocalPort of the socket which shall be found
 * - type : U16
 * - range: whole range is valid
 * @param[in] RemotePort RemotePort of the socket which shall be found
 * - type : U16
 * - range: whole range is valid
 * @param[in] RemoteIpAddress IP of the socket which shall be found
 * - type : U32
 * - range: whole range is valid
 */
T_SOCKET_ELEMENT* socket_find_socket (U8 SocketType, U16 LocalPort, U16 RemotePort, U32 RemoteIpAddress)
{
  unsigned      SocketIndex;


  for (SocketIndex = 0; SocketIndex < MAX_NUMBER_SUPPORTED_SOCKETS; SocketIndex++)
  {
    if (! (Socket [SocketIndex].Flags & SOCK_USER_FLAG_FREE))
    {
      U32 SocketFlags  = Socket [SocketIndex].Flags & (SOCK_FLAG_TYPE_TCP | SOCK_USER_FLAG_SERVER);

      switch (SocketType)
      {
        case SOCK_FIND_TCP_SOCKETS:
          if (! (SocketFlags & SOCK_FLAG_TYPE_TCP)) {continue;} break;

        case SOCK_FIND_TCP_CLIENT_SOCKETS:
          if (SocketFlags != SOCK_FLAG_TYPE_TCP) {continue;} break;

        case SOCK_FIND_TCP_SERVER_SOCKETS:
          if (SocketFlags != (SOCK_FLAG_TYPE_TCP | SOCK_USER_FLAG_SERVER)) {continue;} break;

        case SOCK_FIND_UDP_SOCKETS:
          if (SocketFlags != 0) {continue;} break;

        default:
          continue;
      }

      /*---------------------------------------------------------------------*/

      if (SocketType & SOCK_FIND_TCP_SERVER_SOCKETS)
      {
        T_SOCKET_RESULT   Result;


        SocketIOControl.Command = SOCK_IOC_GET_LOCAL_STATUS;
        Result = sdai_sock_ioctl (SocketIndex, &SocketIOControl);

        if (Result == SOCK_SUCCESS)
        {
          if (SocketIOControl.UseAs.GetLocalStatus.UseAs.Tcp.NumberConnections > 0uL)
          {
            T_SDAI_SOCK_TCP_CONNECTION_LOCAL_STATUS*    pConnection;

            unsigned                                    Connection;


            for (Connection = 0; Connection < SocketIOControl.UseAs.GetLocalStatus.UseAs.Tcp.NumberConnections; Connection++)
            {
              pConnection = &SocketIOControl.UseAs.GetLocalStatus.UseAs.Tcp.Connection [Connection];

              if ( (pConnection->RemoteAddr.IpAddress == RemoteIpAddress) && (pConnection->RemoteAddr.Port == RemotePort) )
              {
                return (&Socket [SocketIndex]);
              }
            }
          }
        }
      }

      /*---------------------------------------------------------------------*/

      if ( (RemoteIpAddress != SOCK_IP_ADDR_ANY) && (Socket [SocketIndex].RemoteAddress.IpAddress != RemoteIpAddress) )
      {
        continue;
      }

      if ( (LocalPort != SOCK_PORT_ANY) && (Socket [SocketIndex].LocalAddress.Port != LocalPort) )
      {
        continue;
      }

      if ( (RemotePort != SOCK_PORT_ANY) && (Socket [SocketIndex].RemoteAddress.Port != RemotePort) )
      {
        continue;
      }

      return (&Socket [SocketIndex]);
    }
  }

  return (NULL);
}

/*===========================================================================*/

/**
 * The function socket_state_changed_callback() is called by the SDAI if the state
 * of an socket has been changed.
 *
 * @return
 * - type : void
 * @param[in] SocketIndex socket ID for which the state has been changed
 * - type : U16
 * - range: 0..(MAX_NUMBER_SUPPORTED_SOCKETS - 1)
 */
void socket_state_changed_callback (U16 SocketIndex)
{
  _BITSET_SET_BIT (SocketStateInd, SocketIndex);

  platform_send_event (EVENT_SOCKET_STATE_CHANGED);

  return;
}

/*===========================================================================*/

/**
 * The function socket_data_received_callback() is called by the SDAI if new data have been received for
 * one or more sockets. The function copys the receive information to a local buffer and informs the main thread
 * for processing of the data.
 *
 * @return
 * - type : VOID
 * @param[in] pSocketRcvInfo pointer to a bitfield holding the information for which socket new data are available
 * - type : T_SDAI_SOCK_DATA_RECEIVED_IND*
 * - range: the whole address range is valid, except NULL
 */
void socket_data_received_callback (T_SDAI_SOCK_DATA_RECEIVED_IND* pSocketRcvInfo)
{
  bitset64_or_bitset64 (&SocketRcvInd.Socket, (const T_BITSET64*) &pSocketRcvInfo->Socket, (const T_BITSET64*) &SocketRcvInd.Socket);

  platform_send_event (EVENT_SOCKET_DATA_RECEIVED);

  return;
}

/*===========================================================================*/

/**
 * The function socket_open() creates a new socket. This function
 * waits until the socket creation is fully completed.
 *
 * @return
 * - type  : VOID
 * @param[in] Flags A bitfield used to set socket specific options. Every set bit activates the respective functionality.
 * - type : U32
 * - range: SOCK_FLAG_ENABLE_KEEPALIVE |
 *          SOCK_FLAG_ENABLE_BROADCAST
 * @param[in] pRemoteAddress If this pointer is set to NULL the socket is working in server mode. Otherwise the socket
 *                           is opened in client mode. In case of a client socket pRemoteAddr specifies the IP address
 *                           and port number of the remote server. For TCP client sockets the remote address is checked and if
 *                           not valid the error SOCK_ERR_INVALID_REMOTE_ADDR is returned, e.g. because the remote IP address
 *                           is not reachable. For UDP client sockets the passed remote address will be ignored (checked when
 *                           calling the function sdai_sock_send()).
 * - type : struct T_SDAI_SOCK_ADDR*
 * - range: the whole address range is valid, except NULL
 * @param[in] pLocalAddress Specifies the IP address and port number which shall be bind to the local socket. Setting
 *                          the local IP address to SOCK_IP_ADDR_ANY allows to bind the socket to all IP addresses
 *                          of the TCP/IP stack. If the local port is set to SOCK_PORT_ANY the TCP/IP stack automatically
 *                          selects a unique local port number from the dynamic client port range. The local address is
 *                          checked and if not valid the error SOCK_ERR_INVALID_LOCAL_ADDR is returned.
 * - type : struct T_SDAI_SOCK_ADDR*
 * - range: the whole address range is valid, except NULL
 */
T_SOCKET_ELEMENT* socket_open (U32 Flags, T_SDAI_SOCK_ADDR* pRemoteAddress, T_SDAI_SOCK_ADDR* pLocalAddress)
{
  T_SOCKET_RESULT   Result;
  T_SOCKET_ELEMENT* pSocket;

  pSocket = socket_get_free_socket ();

  if (pSocket != NULL)
  {
    Result = sdai_sock_create ((Flags & SOCK_FLAG_USER_MASK), pRemoteAddress, pLocalAddress, &pSocket->SocketNumber);

    if (Result != SOCK_SUCCESS)
    {
      socket_print_error (Result);
      pSocket = NULL;
    }
    else
    {
      /* wait until the socket creation is finished */
      do
      {
        SocketIOControl.Command = SOCK_IOC_GET_LOCAL_STATUS;
        Result = sdai_sock_ioctl (pSocket->SocketNumber, &SocketIOControl);

        if (Result != SOCK_SUCCESS)
        {
          socket_close (pSocket);
          socket_print_error (Result);
          return (NULL);
        }

      } while ( (SocketIOControl.UseAs.GetLocalStatus.Status == SOCK_LOCAL_STATUS_CLOSED) &&
                (SocketIOControl.UseAs.GetLocalStatus.StatusCode == SOCK_LOCAL_STATUS_CODE_NO_ERROR) );

      if (SocketIOControl.UseAs.GetLocalStatus.StatusCode == SOCK_LOCAL_STATUS_CODE_NO_ERROR)
      {
        pSocket->LocalAddress.IpAddress  = pLocalAddress->IpAddress;
        pSocket->LocalAddress.Port       = pLocalAddress->Port;
        pSocket->RemoteAddress.IpAddress = pRemoteAddress->IpAddress;
        pSocket->RemoteAddress.Port      = pRemoteAddress->Port;
        pSocket->Flags                   = Flags;

        pSocket->ReceiveCounter          = 0;
        pSocket->SentCounter             = 0;
      }
      else
      {
        socket_close (pSocket);
        return (NULL);
      }
    }
  }

  return (pSocket);
}

/*===========================================================================*/

/**
 * The function socket_close() closes a socket. This function
 * waits until the socket closure is fully completed.
 *
 * @return
 * - type  : VOID
 * @param[in] pSocket The socket specifies the local socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 */
void socket_close (T_SOCKET_ELEMENT* pSocket)
{
  T_SOCKET_RESULT   Result;


  /* close the socket */
  Result = sdai_sock_close (pSocket->SocketNumber);

  if (Result != SOCK_SUCCESS)
  {
    socket_print_error (Result);
  }
  else
  {
    /* wait until closing the socket is finished */
    do
    {
      SocketIOControl.Command = SOCK_IOC_GET_LOCAL_STATUS;
      Result = sdai_sock_ioctl (pSocket->SocketNumber, &SocketIOControl);

    } while (Result != SOCK_ERR_SOCKET_NOT_CREATED);

    pSocket->Flags = SOCK_USER_FLAG_FREE;
  }

  return;
}

/*===========================================================================*/

/**
 * The function socket_close_connection() closes a single TCP connection. This function
 * waits until the connection closure is fully completed.
 *
 * @return
 * - type  : VOID
 * @param[in] Socket The socket ID specifies the local socket
 * - type : U16
 * @param[in] pConnectionStatus Holds informations about a single TCP connection
 * - type : T_SDAI_SOCK_TCP_CONNECTION_LOCAL_STATUS*
 * - range: whole address range is valid
 */
void socket_close_connection (U16 Socket, T_SDAI_SOCK_TCP_CONNECTION_LOCAL_STATUS* pConnectionStatus)
{
  T_SOCKET_RESULT   Result;

  U32               ConnectionIndex;


  SocketIOControl.Command = SOCK_IOC_GET_LOCAL_STATUS;
  Result = sdai_sock_ioctl (Socket, &SocketIOControl);

  if (Result != SOCK_SUCCESS)
  {
    socket_print_error (Result);
    return;
  }

  for (ConnectionIndex = 0; ConnectionIndex < SocketIOControl.UseAs.GetLocalStatus.UseAs.Tcp.NumberConnections; ConnectionIndex++)
  {
    if ( (SocketIOControl.UseAs.GetLocalStatus.UseAs.Tcp.Connection [ConnectionIndex].RemoteAddr.IpAddress == pConnectionStatus->RemoteAddr.IpAddress) &&
         (SocketIOControl.UseAs.GetLocalStatus.UseAs.Tcp.Connection [ConnectionIndex].RemoteAddr.Port == pConnectionStatus->RemoteAddr.Port          )   )
    {
      break;
    }
  }

  SocketIOControl.Command = SOCK_IOC_CLOSE_TCP_CONNECTION;

  SocketIOControl.UseAs.CloseTcpConnection.RemoteAddr.IpAddress = pConnectionStatus->RemoteAddr.IpAddress;
  SocketIOControl.UseAs.CloseTcpConnection.RemoteAddr.Port      = pConnectionStatus->RemoteAddr.Port;

  Result = sdai_sock_ioctl (Socket, &SocketIOControl);

  if (Result != SOCK_SUCCESS)
  {
    socket_print_error (Result);
    return;
  }

  /* wait until closing the TCP connection is finished */
  do
  {
    SocketIOControl.Command = SOCK_IOC_GET_LOCAL_STATUS;
    Result = sdai_sock_ioctl (Socket, &SocketIOControl);

  } while ( (SocketIOControl.UseAs.GetLocalStatus.UseAs.Tcp.Connection [ConnectionIndex].Status == SOCK_TCP_CONNECTION_STATUS_CONNECTED) &&
            (Result != SOCK_ERR_SOCKET_NOT_CREATED) && (Result != SOCK_ERR_SOCKET_NOT_CONNECTED   ) );

  return;
}

/*===========================================================================*/

/**
 * The function socket_send() sends data on a socket. This function
 * waits until the send action is fully completed.
 *
 * @return
 * - type  : VOID
 * @param[in] pSocket The socket ID specifies the local socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Specifies the IP address and port number of the destination host.
 * - type : struct T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] DataLength Data length in bytes of the data pointed to by the pData parameter.
 * - type : U16
 * - range: whole range is valid
 * @param[in] pData Pointer to a buffer containing the data to be transmitted.
 * - type : const U8*
 * - range: whole address range is valid
 */
void socket_send (T_SOCKET_ELEMENT* pSocket, T_SDAI_SOCK_ADDR* pRemoteAddress, U32 DataLength, const U8* pData)
{
  T_SOCKET_RESULT   Result;
  U16               SendLength      = _MIN (DataLength, SOCKET_FIFO_SIZE);
  U32               RemainingLength = DataLength;

  while (SendLength != 0)
  {
    do
    {
      Result = sdai_sock_send (pSocket->SocketNumber, pRemoteAddress, SendLength, &pData [DataLength - RemainingLength]);
    } while (Result == SOCK_ERR_SOCKET_BUSY);

    if (Result == SOCK_SUCCESS)
    {
      do
      {
        SocketIOControl.Command = SOCK_IOC_GET_SEND_STATUS;
        Result = sdai_sock_ioctl (pSocket->SocketNumber, &SocketIOControl);

        if (Result != SOCK_SUCCESS)
        {
          socket_print_error (Result);
          return;
        }

      } while (SocketIOControl.UseAs.GetSendStatus.Status == SOCK_SEND_STATUS_BUSY);

      RemainingLength -= SendLength;
      SendLength       = _MIN (RemainingLength, SOCKET_FIFO_SIZE);
    }
    else
    {
      socket_print_error (Result);
      return;
    }
  }

  pSocket->SentCounter++;

  return;
}

/*===========================================================================*/

/**
 * The function socket_process_events() handles the SDAI socket events and branches to
 * the event handler function.
 *
 * @return
 * - type  : VOID
 * @param[in] Events
 * - type : U32
 * - range: #EVENT_SOCKET_STATE_CHANGED | #EVENT_SOCKET_DATA_RECEIVED
 */
void socket_process_events (U32 Events)
{
  if (Events & EVENT_SOCKET_STATE_CHANGED)
  {
    socket_process_state_ind ();
  }

  if (Events & EVENT_SOCKET_DATA_RECEIVED)
  {
    socket_process_receive_ind ();
  }

  return;
}

/*===========================================================================*/

/**
 * The function demo_socket_print_error() prints the error code returned by the SDAI socket interface.
 *
 * @return
 * - type : VOID
 * @param[in] Error Socket error code
 * - type : T_SOCKET_RESULT
 * - range: SOCK_SUCCESS                      |
 *          SOCK_ERR_INVALID_ARGUMENT         |
 *          SOCK_ERR_API_ALREADY_INITIALIZED  |
 *          SOCK_ERR_API_NOT_INITIALIZED      |
 *          SOCK_ERR_INVALID_SOCKET_ID        |
 *          SOCK_ERR_OUT_OF_SOCKETS           |
 *          SOCK_ERR_SOCKET_NOT_CREATED       |
 *          SOCK_ERR_SOCKET_OFFLINE           |
 *          SOCK_ERR_SOCKET_BUSY              |
 *          SOCK_ERR_SOCKET_NOT_CONNECTED     |
 *          SOCK_ERR_SOCKET_CONNECTED         |
 *          SOCK_ERR_ADDR_IN_USE              |
 *          SOCK_ERR_INVALID_LOCAL_ADDR       |
 *          SOCK_ERR_INVALID_REMOTE_ADDR      |
 *          SOCK_ERR_NO_DATA_RECEIVED         |
 *          SOCK_ERR_NOT_SUPPORTED            |
 *          SOCK_ERR_TEMPORARY_NOT_EXECUTABLE |
 *          SOCK_ERR_FATAL_ERROR
 */
static void socket_print_error (T_SOCKET_RESULT Error)
{
  switch (Error)
  {
    case SOCK_SUCCESS:                      { _ERROR_LOG (("Socket no Error!"));                         break; }
    case SOCK_ERR_INVALID_ARGUMENT:         { _ERROR_LOG (("Socket Error: INVALID_ARGUMENT\r\n"));         break; }
    case SOCK_ERR_API_ALREADY_INITIALIZED:  { _ERROR_LOG (("Socket Error: API_ALREADY_INITIALIZED\r\n"));  break; }
    case SOCK_ERR_API_NOT_INITIALIZED:      { _ERROR_LOG (("Socket Error: API_NOT_INITIALIZED\r\n"));      break; }
    case SOCK_ERR_INVALID_SOCKET_ID:        { _ERROR_LOG (("Socket Error: INVALID_SOCKET_ID\r\n"));        break; }
    case SOCK_ERR_OUT_OF_SOCKETS:           { _ERROR_LOG (("Socket Error: OUT_OF_SOCKETS\r\n"));           break; }
    case SOCK_ERR_SOCKET_NOT_CREATED:       { _ERROR_LOG (("Socket Error: SOCKET_NOT_CREATED\r\n"));       break; }
    case SOCK_ERR_SOCKET_OFFLINE:           { _ERROR_LOG (("Socket Error: SOCKET_OFFLINE\r\n"));           break; }
    case SOCK_ERR_SOCKET_BUSY:              { _ERROR_LOG (("Socket Error: SOCKET_BUSY\r\n"));              break; }
    case SOCK_ERR_SOCKET_NOT_CONNECTED:     { _ERROR_LOG (("Socket Error: SOCKET_NOT_CONNECTED\r\n"));     break; }
    case SOCK_ERR_SOCKET_CONNECTED:         { _ERROR_LOG (("Socket Error: SOCKET_CONNECTED\r\n"));         break; }
    case SOCK_ERR_ADDR_IN_USE:              { _ERROR_LOG (("Socket Error: ADDR_IN_USE\r\n"));              break; }
    case SOCK_ERR_INVALID_LOCAL_ADDR:       { _ERROR_LOG (("Socket Error: INVALID_LOCAL_ADDR\r\n"));       break; }
    case SOCK_ERR_INVALID_REMOTE_ADDR:      { _ERROR_LOG (("Socket Error: INVALID_REMOTE_ADDR\r\n"));      break; }
    case SOCK_ERR_NO_DATA_RECEIVED:         { _ERROR_LOG (("Socket Error: NO_DATA_RECEIVED\r\n"));         break; }
    case SOCK_ERR_NOT_SUPPORTED:            { _ERROR_LOG (("Socket Error: NOT_SUPPORTED\r\n"));            break; }
    case SOCK_ERR_TEMPORARY_NOT_EXECUTABLE: { _ERROR_LOG (("Socket Error: TEMPORARY_NOT_EXECUTABLE\r\n")); break; }
    case SOCK_ERR_FATAL_ERROR:              { _ERROR_LOG (("Socket Error: SOCK_ERR_FATAL_ERROR\r\n"));     break; }
    default:                                { _ERROR_LOG (("Socket Error: %d unknown\r\n", Error));        break; }
  }

  return;
}

/*===========================================================================*/

/**
 * The function socket_process_state_ind() is called when the status
 * of a socket has been changed. It reads the status of the changed sockets and stores
 * it to the application Socket variable.
 *
 * @return
 * - type  : VOID
 */
static void socket_process_state_ind (void)
{
  T_SOCKET_RESULT                             Result;
  T_SDAI_SOCK_TCP_CONNECTION_LOCAL_STATUS*    pConnection;
  T_SDAI_SOCK_IOC_GET_LOCAL_STATUS*           pStatus;
  unsigned                                    Connection;
  unsigned                                    SocketIndex;


  for (SocketIndex = 0; SocketIndex < MAX_NUMBER_SUPPORTED_SOCKETS; SocketIndex++)
  {
    if (_BITSET_IS_BIT_SET(SocketStateInd, SocketIndex))
    {
      _DEBUG_LOG (("Status for socket %d changed:\r\n", SocketIndex));

      platform_lock ();
      _BITSET_RESET_BIT(SocketStateInd, SocketIndex);
      platform_unlock ();

      Socket [SocketIndex].SocketStatus.Command = SOCK_IOC_GET_LOCAL_STATUS;
      Result = sdai_sock_ioctl (Socket [SocketIndex].SocketNumber, &Socket [SocketIndex].SocketStatus);

      pStatus = &Socket [SocketIndex].SocketStatus.UseAs.GetLocalStatus;

      if (Result == SOCK_SUCCESS)
      {
        switch (pStatus->Status)
        {
          case SOCK_LOCAL_STATUS_CLOSED : _DEBUG_LOG ((" Status: Closed\r\n"));      break;
          case SOCK_LOCAL_STATUS_OFFLINE: _DEBUG_LOG ((" Status: Offline\r\n"));     break;
          case SOCK_LOCAL_STATUS_ONLINE : _DEBUG_LOG ((" Status: Online\r\n"));      break;
          default: _DEBUG_LOG ((" Status: Unknown (0x%02lX)\r\n", pStatus->Status)); break;
        }

        switch (pStatus->StatusCode)
        {
          case SOCK_LOCAL_STATUS_CODE_NO_ERROR      : _DEBUG_LOG ((" StatusCode: No Error\r\n"));               break;
          case SOCK_LOCAL_STATUS_CODE_ADDR_IN_USE   : _DEBUG_LOG ((" StatusCode: Address already in use\r\n")); break;
          case SOCK_LOCAL_STATUS_CODE_INTERNAL_ERROR: _DEBUG_LOG ((" StatusCode: Internal Error\r\n"));         break;
          default: _DEBUG_LOG ((" StatusCode: Unknown (0x%02lX)\r\n", pStatus->Status));                        break;
        }

        if (pStatus->UseAs.Tcp.NumberConnections == 0)
        {
          memset (&Socket [SocketIndex].TcpConnectionStatus, 0, sizeof (Socket [SocketIndex].TcpConnectionStatus));
        }
        else
        {
          for (Connection = 0; Connection < pStatus->UseAs.Tcp.NumberConnections; Connection++)
          {
            _DEBUG_LOG (("  Status for TCP connection changed:\r\n"));

            pConnection = &pStatus->UseAs.Tcp.Connection [Connection];

            _DEBUG_LOG (("   Remote IP address: %03d.%03d.%03d.%03d\r\n", ((U8) ((U8*) &pConnection->RemoteAddr.IpAddress) [0]),
                                                                        ((U8) ((U8*) &pConnection->RemoteAddr.IpAddress) [1]),
                                                                        ((U8) ((U8*) &pConnection->RemoteAddr.IpAddress) [2]),
                                                                        ((U8) ((U8*) &pConnection->RemoteAddr.IpAddress) [3])));

            _DEBUG_LOG (("   Remote Port: %d\r\n", sdai_sock_htons (pConnection->RemoteAddr.Port)));

            switch (pConnection->Status)
            {
              case SOCK_TCP_CONNECTION_STATUS_UNCONNECTED: _DEBUG_LOG (("   Status: Unconnected\r\n"));  break;
              case SOCK_TCP_CONNECTION_STATUS_CONNECTING : _DEBUG_LOG (("   Status: Connecting\r\n"));   break;
              case SOCK_TCP_CONNECTION_STATUS_CONNECTED  : _DEBUG_LOG (("   Status: Connected\r\n"));    break;
              default: _DEBUG_LOG (("   Status: Unknown (0x%02lX)\r\n", pConnection->Status));           break;
            }

            switch (pConnection->StatusCode)
            {
              case SOCK_TCP_CONNECTION_STATUS_CODE_NO_ERROR        : _DEBUG_LOG (("   StatusCode: No Error\r\n"));                break;
              case SOCK_TCP_CONNECTION_STATUS_CODE_INTERNAL_ERROR  : _DEBUG_LOG (("   StatusCode: Address already in use\r\n"));  break;
              case SOCK_TCP_CONNECTION_STATUS_CODE_CLOSED_REMOTELY : _DEBUG_LOG (("   StatusCode: Closed remotely\r\n"));         break;
              case SOCK_TCP_CONNECTION_STATUS_CODE_ABORTED_REMOTELY: _DEBUG_LOG (("   StatusCode: Aborted remotely\r\n"));        break;
              case SOCK_TCP_CONNECTION_STATUS_CODE_TIMEOUT         : _DEBUG_LOG (("   StatusCode: Connection timeout\r\n"));      break;
              case SOCK_TCP_CONNECTION_STATUS_CODE_CONNECT_REJECTED: _DEBUG_LOG (("   StatusCode: Connection rejected\r\n"));     break;
              default: _DEBUG_LOG (("   StatusCode: Unknown (0x%02lX)\r\n", pConnection->Status));                                break;
            }

            if (pConnection->Status == SOCK_TCP_CONNECTION_STATUS_CONNECTED)
            {
              unsigned   Index;
              unsigned   Found = 0;

              /* Search if remote ipAddress+Port pair exists in the old structure */
              for (Index=0; Index<5; Index++)
              {
                if ( (pConnection->RemoteAddr.IpAddress == Socket [SocketIndex].TcpConnectionStatus.Connection [Index].RemoteAddr.IpAddress) &&
                     (pConnection->RemoteAddr.Port == Socket [SocketIndex].TcpConnectionStatus.Connection [Index].RemoteAddr.Port          ) )
                {
                  Found = 1; /* there is a previous connection with the same parameters that has been closed before and reopened */

                  if ( (Socket [SocketIndex].TcpConnectionStatus.Connection [Index].Status != SOCK_TCP_CONNECTION_STATUS_CONNECTED) &&
                       (Socket [SocketIndex].LocalAddress.Port == sdai_sock_htons (TCP_SERVER_CONSOLE_PORT)                        ) )
                  {
                    handle_tcp_console_connect (&Socket [SocketIndex], &pConnection->RemoteAddr);
                  }
                }
              }

              if (Found == 0)
              {
                if (Socket [SocketIndex].LocalAddress.Port == sdai_sock_htons (TCP_SERVER_CONSOLE_PORT))
                {
                  handle_tcp_console_connect (&Socket [SocketIndex], &pConnection->RemoteAddr);
                }
              }
            }

            memcpy (&Socket [SocketIndex].TcpConnectionStatus.Connection [Connection], pConnection, sizeof (Socket [SocketIndex].TcpConnectionStatus.Connection [Connection]));
          }
        }

        _DEBUG_LOG (("\r\n"));
      }
      else if (Result == SOCK_ERR_SOCKET_NOT_CREATED)
      {
        _DEBUG_LOG ((" Status: Closed\r\n"));
        _DEBUG_LOG ((" StatusCode: No Error\r\n"));
        _DEBUG_LOG (("\r\n"));

        Socket [SocketIndex].Flags                                        = SOCK_USER_FLAG_FREE;
        Socket [SocketIndex].SocketStatus.UseAs.GetLocalStatus.Status     = SOCK_LOCAL_STATUS_CLOSED;
        Socket [SocketIndex].SocketStatus.UseAs.GetLocalStatus.StatusCode = SOCK_LOCAL_STATUS_CODE_NO_ERROR;

        Socket [SocketIndex].SocketStatus.UseAs.GetLocalStatus.UseAs.Tcp.NumberConnections = 0;
      }
      else
      {
        socket_print_error (Result);
      }
    }
  }

  return;
}

/*===========================================================================*/

/**
 * The function socket_process_receive_ind() is called when new data on one or more sockets
 * have been received. Depending on the socket it either calls the console handler function
 * handle_tcp_console_command() or echos the data back if enabled for this socket.
 *
 * @return
 * - type  : VOID
 */
static void socket_process_receive_ind (void)
{
  T_BITSET64    SocketRcvInfo;
  unsigned      SocketIndex;


  platform_lock ();
  memcpy (&SocketRcvInfo, &SocketRcvInd.Socket, sizeof (SocketRcvInfo));
  memset (&SocketRcvInd.Socket, 0, sizeof (SocketRcvInd.Socket));
  platform_unlock ();

  for (SocketIndex = 0; SocketIndex < MAX_NUMBER_SUPPORTED_SOCKETS; SocketIndex++)
  {
    if (Socket [SocketIndex].SocketStatus.UseAs.GetLocalStatus.Status == SOCK_LOCAL_STATUS_ONLINE)
    {
      if (_BITSET_IS_BIT_SET (SocketRcvInfo, Socket [SocketIndex].SocketNumber))
      {
        T_SOCKET_RESULT     Result;
        T_SDAI_SOCK_ADDR    RemoteAddress;
        U16                 DataLength = sizeof (SocketDataBuffer);


        Result = sdai_sock_receive (Socket [SocketIndex].SocketNumber, &RemoteAddress, &DataLength, &SocketDataBuffer[0]);

        if (Result == SOCK_SUCCESS)
        {
          Socket [SocketIndex].ReceiveCounter++;

          if (Socket [SocketIndex].Flags & SOCK_FLAG_TYPE_TCP)
          {
            if (Socket [SocketIndex].LocalAddress.Port == sdai_sock_htons(TCP_SERVER_CONSOLE_PORT))
            {
              handle_tcp_console_command ((char*)&SocketDataBuffer[0], DataLength, &Socket [SocketIndex], &RemoteAddress);
              tcp_console_prompt (&Socket [SocketIndex], &RemoteAddress);
            }
            #ifdef SDAI_WEBSERVER
            else if (Socket [SocketIndex].LocalAddress.Port == sdai_sock_htons(TCP_SERVER_WEB_PORT))
            {
              web_handle_request ((char*)&SocketDataBuffer[0], DataLength, &Socket [SocketIndex], &RemoteAddress);
            }
            #endif
            else
            {
              if (Socket [SocketIndex].Flags & SOCK_USER_FLAG_ECHO)
              {
                socket_send (&Socket [SocketIndex], &RemoteAddress, DataLength, &SocketDataBuffer[0]);
              }
            }
          }
          else
          {
            /* UDP socket */
            if (Socket [SocketIndex].Flags & SOCK_USER_FLAG_ECHO)
            {
              socket_send (&Socket [SocketIndex], &RemoteAddress, DataLength, &SocketDataBuffer[0]);
            }
          }
        }
        else
        {
          socket_print_error (Result);
        }
      }
    }
  }

  return;
}

/*===========================================================================*/

/**
 * The function socket_term() closes all TCP connections, all previously created sockets and terminates
 * the socket interface afterwards.
 *
 * @return
 * - type  : VOID
 */
void socket_term (void)
{
  T_SOCKET_RESULT   Result;
  unsigned          SocketIndex;


  for (SocketIndex = 0; SocketIndex < MAX_NUMBER_SUPPORTED_SOCKETS; SocketIndex++)
  {
    if (!(Socket [SocketIndex].Flags & SOCK_USER_FLAG_FREE))
    {
      /* connection for a TCP server socket are closed by the socket interface */
      socket_close (&Socket [SocketIndex]);
    }
  }

  Result = sdai_sock_term ();

  if (Result != SOCK_SUCCESS)
  {
    socket_print_error (Result);
  }

  return;
}

/*===========================================================================*/

/**
 * The function socket_init() initialize the SDAI socket sample. It opens a socket at
 * port #TCP_SERVER_CONSOLE_PORT which can be accessed to use the socket console.
 *
 * @return
 * - type  : VOID
 */
void socket_init (void)
{
  unsigned                SocketIndex;
  unsigned                ConnectionIndex;
  T_SDAI_SOCK_ADDR        RemoteAddress;
  T_SDAI_SOCK_ADDR        LocalAddress;
  T_SOCKET_RESULT         Result;


  _TRACE(("socket_init\r\n"));

  memset (&SocketInit, 0, sizeof (SocketInit));
  memset (&SocketRcvInd, 0, sizeof (SocketRcvInd));

  for (SocketIndex = 0; SocketIndex < MAX_NUMBER_SUPPORTED_SOCKETS; SocketIndex++)
  {
    for (ConnectionIndex = 0; ConnectionIndex < MAX_NUMBER_CONNECTIONS_PER_SOCKET; ConnectionIndex++)
    {
      Socket [SocketIndex].TcpConnectionStatus.Connection [ConnectionIndex].Status = SOCK_TCP_CONNECTION_STATUS_UNCONNECTED;
    }

    Socket [SocketIndex].Flags = SOCK_USER_FLAG_FREE;
  }

  /* initialize socket interface */
  SocketInit.ConfigOptions.Timeout  = 10000;

  SocketInit.Callback.StateChangedCbk = socket_state_changed_callback;
  SocketInit.Callback.DataReceivedCbk = socket_data_received_callback;

  if ((Result = sdai_sock_init (&SocketInit)) != SOCK_SUCCESS)
  {
    socket_print_error (Result);
  }

  /* create TCP consol server socket */
  RemoteAddress.IpAddress = SOCK_IP_ADDR_ANY;
  RemoteAddress.Port      = SOCK_PORT_ANY;
  LocalAddress.IpAddress  = SOCK_IP_ADDR_ANY;
  LocalAddress.Port       = sdai_sock_htons (TCP_SERVER_CONSOLE_PORT);

  socket_open ((SOCK_FLAG_TYPE_TCP | SOCK_USER_FLAG_SERVER), &RemoteAddress, &LocalAddress);

  #ifdef SDAI_WEBSERVER
  /* create HTTP server socket */
  RemoteAddress.IpAddress = SOCK_IP_ADDR_ANY;
  RemoteAddress.Port      = SOCK_PORT_ANY;
  LocalAddress.IpAddress  = SOCK_IP_ADDR_ANY;
  LocalAddress.Port       = sdai_sock_htons (TCP_SERVER_WEB_PORT);

  socket_open ((SOCK_FLAG_TYPE_TCP | SOCK_USER_FLAG_SERVER), &RemoteAddress, &LocalAddress);
  #endif

  return;
}

#endif /* SDAI_SOCKET_INTERFACE */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
