/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include "sdai.h"

#include "demo_platform.h"
#include "demo.h"

#ifdef SDAI_SOCKET_INTERFACE
  #include "sdai_socket.h"
#endif

/******************************************************************************
DEFINES
******************************************************************************/

/******************************************************************************
TYPEDEFS
******************************************************************************/

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

/** \addtogroup demo_appl_functions */
//@{

/*--- SDAI callback functions -----------------------------------------------*/

void               demo_ident_data_callback          (struct SDAI_IDENT_DATA*);
void               demo_exception_callback           (struct SDAI_EXCEPTION_DATA*);
void               demo_output_data_callback         (struct SDAI_OUTPUT_CHANGED_IND*) _LOCATED_INTO_INSTRUCTION_FAST_MEMORY;
void               demo_write_req_callback           (struct SDAI_WRITE_IND*);
void               demo_read_req_callback            (struct SDAI_READ_IND*);
void               demo_control_req_callback         (struct SDAI_CONTROL_IND*);

/*--- Local functions -------------------------------------------------------*/

static void        demo_update_output_data           (void) _LOCATED_INTO_INSTRUCTION_FAST_MEMORY;
static void        demo_update_stack_status          (void);
static void        demo_process_exception            (void);
static void        demo_send_pending_res             (void);

static void        demo_plug_units                   (struct T_DEMO_UNITS_MANAGEMENT*);
static void        demo_print_unit                   (struct T_DEMO_UNIT*, BOOL);
//@}

/******************************************************************************
PUBLIC DATA
******************************************************************************/

struct T_DEMO_MANAGEMENT_DATA         ApplManagement;       /**< Management data and status information */

U8                                    Axis0Reference      = 40;   /**< Reference value of axis0 */

struct T_DEMO_EXCEPTION_DATA          ExceptionData;        /**< Local buffer exception data */

/******************************************************************************
PRIVATE DATA
******************************************************************************/

static struct T_DEMO_IDENT_DATA       IdentData;       /**< Local buffer for new ident data */
static struct T_DEMO_WRITE_IND        WriteInd;        /**< Local buffer write indication data */
static struct T_DEMO_WRITE_RES        WriteRes;        /**< Local buffer write response data */
static struct T_DEMO_CONTROL_IND      ControlInd;      /**< Local buffer control indication data */
static struct T_DEMO_CONTROL_RES      ControlRes;      /**< Local buffer control response data */
static struct T_DEMO_READ_IND         ReadInd;         /**< Local buffer read indication data */
static struct T_DEMO_READ_RES         ReadRes;         /**< Local buffer read response data */

static U8                             CurrentProtocol; /**< The currently selected protocol */

/******************************************************************************
FUNCTION IMPLEMENTATION
******************************************************************************/

/**
 * The function demo_ident_data_callback() is called by the SDAI when the network
 * parameter has been changed. The function copies the data to a local buffer and informs
 * the main thread for processing of the data.
 *
 * @return
 * - type : VOID
 * @param[in] pIdentData Pointer to the new network parameter
 * - type : struct SDAI_IDENT_DATA*
 * - range: whole address range is valid
 */
void demo_ident_data_callback (struct SDAI_IDENT_DATA *pIdentData)
{
  _TRACE (("demo_ident_data_callback\r\n"));

  _ASSERT (pIdentData != NULL);

  /* copy received data */
  if (IdentData.InUse == FALSE)
  {
    IdentData.InUse = TRUE;
    memcpy (&IdentData.IdentData, pIdentData, sizeof(IdentData.IdentData));

    /* let the demo application task process the event */
    platform_send_event (EVENT_IDENT_DATA_CHANGED);
  }
  else
  {
    _ERROR_LOG (("demo_ident_data_callback: buffer in use\r\n"));
  }

  return;
}

/*===========================================================================*/

/**
 * The function demo_exception_callback() is called by the SDAI when an exception has
 * been occurred. The function copies the data to a local buffer and informs the main thread
 * for processing of the data.
 *
 * @return
 * - type : VOID
 * @param[in] pExceptData Pointer to the exception data
 * - type : struct SDAI_EXCEPTION_DATA*
 * - range: whole address range is valid
 */
void demo_exception_callback (struct SDAI_EXCEPTION_DATA* pExceptData)
{
  _TRACE (("demo_exception_callback\r\n"));

  _ASSERT (pExceptData != NULL);

  if (ExceptionData.InUse == FALSE)
  {
    /* copy received data if local buffer is free */
    ExceptionData.InUse = TRUE;

    /* copy received data */
    memcpy (&ExceptionData.SdaiExceptionData,  pExceptData, sizeof (ExceptionData.SdaiExceptionData));

    /* let the demo application task process the event */
    platform_send_event (EVENT_EXCEPTION);
  }
  else
  {
    _WARN_LOG (("demo_exception_callback: buffer in use"));
  }

  return;
}

/*===========================================================================*/

/**
 * The function demo_output_data_callback() is called by the SDAI when the output data has
 * been changed. The behavior depends on the selected IO data handling. If #IO_DATA_HANDLING_MIRROR
 * is set the functions directly mirrors the data of the first output unit back to the first
 * input unit without informing the main thread. If #IO_DATA_HANDLING_RUNNINGLIGHT is set
 * the function copies the data of the changed units to a local buffer and informs the main thread
 * for processing of the data.
 *
 * @return
 * - type : VOID
 * @param[in] pUnitsChanged indicates for which unit the output data has been changed
 * - type : struct SDAI_OUTPUT_CHANGED_IND*
 * - range: whole address range is valid
 */
void _LOCATED_INTO_INSTRUCTION_FAST_MEMORY demo_output_data_callback (struct SDAI_OUTPUT_CHANGED_IND* pUnitsChanged)
{
  _TRACE (("demo_output_data_callback\r\n"));

  _ASSERT (pUnitsChanged != NULL);

  FUNCTION_ENTRY (PERFORMANCE_INDICATOR_OUTPUT_DATA_CALLBACK);

#if IO_DATA_HANDLING_MIRROR
  struct T_DEMO_UNIT*   pOutputUnit = (ApplManagement.UnitsManagement.pFirstOutputUnit);
  struct T_DEMO_UNIT*   pInputUnit  = (ApplManagement.UnitsManagement.pFirstInputUnit);

  /* echo data of first output unit to first input unit */
  sdai_get_data (pOutputUnit->Id, &pOutputUnit->OutputState, pOutputUnit->pOutputData);
  if (pOutputUnit->OutputState == SDAI_DATA_STATUS_VALID)
  {
    sdai_set_data (pInputUnit->Id, pOutputUnit->OutputState, pOutputUnit->pOutputData);

    if (ApplManagement.SdaiInitData.Info.Flags & SDAI_DATA_CONSISTENCY_IO_IMAGE)
    {
      (void) sdai_process_data_update_finished ();
    }
  }
#else
    /* let the demo application task process the event */
    platform_send_event (EVENT_OUTPUT_DATA_CHANGED);
#endif

  FUNCTION_EXIT (PERFORMANCE_INDICATOR_OUTPUT_DATA_CALLBACK);

  return;
}

/*===========================================================================*/

/**
 * The function demo_write_req_callback() is called by the SDAI on an incoming write
 * indication. The function copies the data to a local buffer and informs the main thread
 * for processing of the data.
 *
 * @return
 * - type : VOID
 * @param[in] pWriteInd Pointer to the received write indication structure
 * - type : struct SDAI_WRITE_IND*
 * - range: whole address range is valid
 */
void demo_write_req_callback (struct SDAI_WRITE_IND* pWriteInd)
{
  _TRACE (("demo_write_req_callback\r\n"));

  _ASSERT (pWriteInd != NULL);

  /* copy received data if local buffer is free */
  if (WriteInd.InUse == FALSE)
  {
    WriteInd.InUse = TRUE;

    /* copy the header */
    memcpy (&WriteInd.Header, pWriteInd, sizeof (WriteInd.Header));

    if ( (WriteInd.Header.Length > 0)                    &&
         (WriteInd.Header.Length <= sizeof (WriteInd.Data)) )
    {
      /* copy the data - they follow direct after the header */
      memcpy (WriteInd.Data, pWriteInd + 1, WriteInd.Header.Length);
    }

    /* let the demo application task process the event */
    platform_send_event (EVENT_WRITE_IND_RECEIVED);
  }
  else
  {
    _ERROR_LOG (("demo_write_req_callback: buffer in use"));
  }


  return;
}

/*===========================================================================*/

/**
 * The function demo_read_req_callback() is called by the SDAI on an incoming read
 * indication. The function copies the data to a local buffer and informs the main thread
 * for processing of the data.
 *
 * @return
 * - type : VOID
 * @param[in] pReadData Pointer to the received read indication structure
 * - type : struct SDAI_READ_IND*
 * - range: whole address range is valid
 */
void demo_read_req_callback (struct SDAI_READ_IND* pReadData)
{
  _TRACE (("demo_read_req_callback\r\n"));

  _ASSERT (pReadData != NULL);

  /* copy received data if local buffer is free */
  if (ReadInd.InUse == FALSE)
  {
    ReadInd.InUse = TRUE;

    /* copy received data */
    memcpy (&ReadInd.Header, pReadData, sizeof (ReadInd.Header));

    /* let the demo application task process the event */
    platform_send_event (EVENT_READ_IND_RECEIVED);
  }
  else
  {
    _ERROR_LOG (("demo_read_req_callback: buffer in use"));
  }

  return;
}

/*===========================================================================*/

/**
 * The function demo_control_req_callback() is called by the SDAI on an incoming
 * control indication. The function copies the data to a local buffer and informs the main thread
 * for processing of the data.
 *
 * @return
 * - type : VOID
 * @param[in] pControlData Pointer to the received control indication structure
 * - type : struct SDAI_CONTROL_IND*
 * - range: whole address range is valid
 */
void demo_control_req_callback (struct SDAI_CONTROL_IND* pControlData)
{
  _TRACE (("demo_control_req_callback\r\n"));

  _ASSERT (pControlData != NULL);

  /* copy received data if local buffer is free */
  if (ControlInd.InUse == FALSE)
  {
    ControlInd.InUse = TRUE;

    /* copy the header */
    memcpy (&ControlInd.Header, pControlData, sizeof (ControlInd.Header));

    if ( (ControlInd.Header.Length > 0)                         &&
         (ControlInd.Header.Length <= sizeof (ControlInd.Data)) )
    {
      /* copy the data */
      memcpy (ControlInd.Data, pControlData + 1, ControlInd.Header.Length);
    }

    /* let the demo application task process the event */
    platform_send_event (EVENT_CONTROL_IND_RECEIVED);
  }
  else
  {
    _ERROR_LOG (("demo_control_req_callback: buffer in use"));
  }

  return;
}

/*===========================================================================*/

/**
 * The function demo_update_input_data() updates the data for all input units.
 * If installed the function calls the protocol specific callback function
 * proto_get_input_data. If not installed or the return value is 0 the input data
 * are incremented and the status is set to valid.
 *
 * @return
 * - type : VOID
 */
void demo_update_input_data (void)
{
  struct T_DEMO_UNIT* pInputUnit;
  U32                 UnitIndex;
  U8                  Result;

  _TRACE (("demo_update_input_data\r\n"));

  FUNCTION_ENTRY (PERFORMANCE_INDICATOR_INPUT_DATA);

  if (ApplManagement.UnitsManagement.proto_get_input_data == NULL)
  {
    /* update all input units */
    for (UnitIndex = 0; UnitIndex < SDAI_MAX_UNITS; UnitIndex++)
    {
      pInputUnit = (ApplManagement.UnitsManagement.UnitList + UnitIndex);

      if ( (pInputUnit->Id != SDAI_INVALID_ID) && ((pInputUnit->InputSize != 0) || (pInputUnit->Type == SDAI_UNIT_TYPE_HEAD)) &&
           (pInputUnit->CfgData.PeripheralOffsetInput == SDAI_PERIPHERAL_OFFSET_DPRAM) )
      {
        if ((profinet_get_input_data(pInputUnit)) != PROTO_FUNC_SUCCESS)
        {
          pInputUnit->InputState = SDAI_DATA_STATUS_VALID;
        }

        if ((Result = sdai_set_data (pInputUnit->Id, pInputUnit->InputState, pInputUnit->pInputData)) != SDAI_SUCCESS)
        {
          demo_print_error (Result);
        }
      }
    }

    if (ApplManagement.SdaiInitData.Info.Flags & SDAI_DATA_CONSISTENCY_IO_IMAGE)
    {
      (void) sdai_process_data_update_finished ();
    }
  }

  FUNCTION_EXIT (PERFORMANCE_INDICATOR_INPUT_DATA);

  return;
}

/*===========================================================================*/

/**
 * The function demo_update_output_data() reads the output data for all units
 * with changed output data. If installed it calls the protocol specific callback function
 * proto_set_output_data for the changed units.
 *
 * @return
 * - type : VOID
 */
static void demo_update_output_data (void)
{
  struct T_DEMO_UNIT*   pOutputUnit;
  U32                   UnitIndex;
  U8                    Result;

  _TRACE (("demo_update_output_data\r\n"));

  FUNCTION_ENTRY (PERFORMANCE_INDICATOR_OUTPUT_DATA);

  for (UnitIndex = 0; UnitIndex < SDAI_MAX_UNITS; UnitIndex++)
  {
    /* update all output units, which is the worst case (ignore changed notification) */
    pOutputUnit = (ApplManagement.UnitsManagement.UnitList + UnitIndex);
    ApplManagement.UnitsManagement.UnitList [UnitIndex].DataChanged = 0;

    if ( (pOutputUnit->Id != SDAI_INVALID_ID) && (pOutputUnit->OutputSize > 0u) && (pOutputUnit->CfgData.PeripheralOffsetOutput == SDAI_PERIPHERAL_OFFSET_DPRAM))
    {
      if ((Result = sdai_get_data (pOutputUnit->Id, &pOutputUnit->OutputState, pOutputUnit->pOutputData)) != SDAI_SUCCESS)
      {
        demo_print_error (Result);
      }
      else
      {
        profinet_set_output_data(pOutputUnit);
        #if !PLATFORM_SLOW_PRINTF
        demo_print_unit (pOutputUnit, TRUE);
        #endif
      }
    }
  }

  FUNCTION_EXIT (PERFORMANCE_INDICATOR_OUTPUT_DATA);

  return;
}

/*===========================================================================*/

/**
 * The function demo_update_stack_status() reads the stack status and updates the LEDs.
 * If configured it also sends an email if the status changes from connected->unconnected
 * or unconnected->connected. If installed it calls the protocol specific callback function
 * proto_process_additional_status with the additional status data.
 *
 * @return
 * - type : VOID
 */
static void demo_update_stack_status (void)
{
  U16   NewStackStatus;
  U8    Result;

  _TRACE (("demo_update_stack_status\r\n"));

  if ((Result = sdai_get_state (&NewStackStatus, &ApplManagement.ProtocolStatusData)) != SDAI_SUCCESS)
  {
    demo_print_error (Result);
  }
  else
  {
    if (NewStackStatus != ApplManagement.StackStatus)
    {
      U8    first = 1;


      _DEBUG_LOG (("\r\nStack Status: "));

      if (NewStackStatus & SDAI_STATE_MASK_INIT)
      {
        if (! first) {_DEBUG_LOG ((" | "));}
        first = 0;
        _DEBUG_LOG (("INIT"));
      }
      if (NewStackStatus & SDAI_STATE_MASK_ONLINE)
      {
        if (! first) {_DEBUG_LOG ((" | "));}
        first = 0;
        _DEBUG_LOG (("ONLINE"));
      }
      if (NewStackStatus & SDAI_STATE_MASK_CONNECTED)
      {
        if (! first) {_DEBUG_LOG ((" | "));}
        first = 0;
        _DEBUG_LOG (("CONNECTED"));
      }
      if (NewStackStatus & SDAI_STATE_MASK_DOUBLE_ADDRESS_ERROR)
      {
        if (! first) {_DEBUG_LOG ((" | "));}
        first = 0;
        _DEBUG_LOG (("IP ADDRESS CONFLICT"));
      }
      if (NewStackStatus & SDAI_STATE_MASK_HARDWARE_ERROR)
      {
        if (! first) {_DEBUG_LOG ((" | "));}
        first = 0;
        _DEBUG_LOG (("HW ERROR"));
      }
      if (NewStackStatus & SDAI_STATE_MASK_LICENSE_ERROR)
      {
        if (! first) {_DEBUG_LOG ((" | "));}
        first = 0;
        _DEBUG_LOG (("LICENSE ERROR"));
      }
      if (NewStackStatus & SDAI_STATE_MASK_APPL_WATCHDOG_EXPIRED)
      {
        if (! first) {_DEBUG_LOG ((" | "));}
        first = 0;
        _DEBUG_LOG (("APPLICATION WATCHDOG EXPIRED"));
      }
      if (NewStackStatus & SDAI_STATE_MASK_PROTOCOL_ERROR)
      {
        if (! first) {_DEBUG_LOG ((" | "));}
        first = 0;
        _DEBUG_LOG (("PROTOCOL ERROR"));
      }
      if (NewStackStatus & SDAI_STATE_MASK_NON_VOLATILE_STORAGE_ERROR)
      {
        if (! first) {_DEBUG_LOG ((" | "));}
        first = 0;
        _DEBUG_LOG (("NV STORAGE ERROR"));
      }

      _DEBUG_LOG ((" (0x%02X)\r\n", NewStackStatus));

      ApplManagement.StackStatus = NewStackStatus;

      platform_visualize_data (&ApplManagement);
    }

    profinet_print_additional_status_data(&ApplManagement.ProtocolStatusData);

    if (ApplManagement.StackStatus & SDAI_STATE_MASK_INIT)
    {
      STATUS_LED_SWITCH (LED_DEMO_POWER, LED_ON);
    }
    else
    {
      STATUS_LED_SWITCH (LED_DEMO_POWER, LED_OFF);
    }

    if (ApplManagement.StackStatus & SDAI_STATE_MASK_CONNECTED)
    {
      STATUS_LED_SWITCH (LED_DEMO_CONNECTED, LED_ON);
    }
    else
    {
      STATUS_LED_SWITCH (LED_DEMO_CONNECTED, LED_OFF);
    }

    if ( (ApplManagement.StackStatus & SDAI_STATE_MASK_DOUBLE_ADDRESS_ERROR ) ||
         (ApplManagement.StackStatus & SDAI_STATE_MASK_HARDWARE_ERROR       ) ||
         (ApplManagement.StackStatus & SDAI_STATE_MASK_LICENSE_ERROR        ) ||
         (ApplManagement.StackStatus & SDAI_STATE_MASK_APPL_WATCHDOG_EXPIRED) )
    {
      STATUS_LED_SWITCH (LED_DEMO_ERROR, LED_ON);
    }
    else
    {
      STATUS_LED_SWITCH (LED_DEMO_ERROR, LED_OFF);
    }
  }

  return;
}

/*===========================================================================*/

/**
 * The function demo_process_exception() processes a received exception. The content
 * of the Exception data is stack specific. Please contact Softing AG with informations
 * about the use case and the exception data.
 *
 * @return
 * - type : VOID
 */
static void demo_process_exception (void)
{
  _TRACE (("demo_process_exception\r\n"));

  _DEBUG_LOG (("\r\n!!!EXCEPTION:\r\n"));
  _DEBUG_LOG (("  Code: 0x%05X\r\n", ExceptionData.SdaiExceptionData.Code));
  _DEBUG_LOG (("  ChannelId: 0x%05X\r\n", ExceptionData.SdaiExceptionData.ChannelId));
  _DEBUG_LOG (("  ModuleId: 0x%05X\r\n", ExceptionData.SdaiExceptionData.ModuleId));
  _DEBUG_LOG (("  FileId: 0x%05X\r\n", ExceptionData.SdaiExceptionData.FileId));
  _DEBUG_LOG (("  Line: 0x%05X\r\n", ExceptionData.SdaiExceptionData.Line));
  _DEBUG_LOG (("  ErrorId: 0x%05X\r\n", ExceptionData.SdaiExceptionData.ErrorId));
  _DEBUG_LOG (("  Parameter: 0x" USIGN32_HEX_FORMAT_STRING "\r\n", ExceptionData.SdaiExceptionData.Parameter));
  _DEBUG_LOG (("  String: %s\r\n", &ExceptionData.SdaiExceptionData.String [0]));
  _DEBUG_LOG (("\r\n"));

  /* do not clear the exception data flag. Only the first exception is relevant */
  /* ExceptionData.InUse = FALSE; */

  STATUS_LED_SWITCH(LED_ALL, LED_ON);

  /* add error handling here  */

  return;
}

/*===========================================================================*/

/**
 * The function demo_print_unit() prints all informations of one unit.
 *
 * @return
 * - type  : VOID
 * @param[in] pUnit
 * - type : struct DEMO_UNIT*
 * - range: whole address range
 * @param[in] PrintIOData
 * - type : BOOL
 * - range: TRUE | FALSE
 */
static void demo_print_unit (struct T_DEMO_UNIT* pUnit, BOOL PrintIOData)
{
  int Index;

  _TRACE (("demo_print_unit\r\n"));

  if (pUnit->Type == SDAI_UNIT_TYPE_GENERIC_INPUT)
  {
    _DEBUG_LOG (("Input Unit:\r\n  ID:     0x" USIGN32_HEX_FORMAT_STRING "\r\n  Size:   %d\r\n", pUnit->Id, pUnit->InputSize));

    if (PrintIOData)
    {
      _DEBUG_LOG (("  Status: 0x%02X\r\n  Data:   ", pUnit->InputState));
      for (Index = 0; Index < pUnit->InputSize; Index++)
      {
        _DEBUG_LOG (("0x%02X ", pUnit->pInputData [Index]));
      }
    }
  }
  else if (pUnit->Type == SDAI_UNIT_TYPE_GENERIC_OUTPUT)
  {
    _DEBUG_LOG (("Output Unit:\r\n  ID:     0x" USIGN32_HEX_FORMAT_STRING "\r\n  Size:   %d\r\n", pUnit->Id, pUnit->OutputSize));

    if (PrintIOData)
    {
      _DEBUG_LOG (("  Status: 0x%02X\r\n  Data:   ", pUnit->OutputState));
      for (Index = 0; Index < pUnit->OutputSize; Index++)
      {
        _DEBUG_LOG (("0x%02X ", pUnit->pOutputData [Index]));
      }
    }
  }
  else if (pUnit->Type == SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT)
  {
    _DEBUG_LOG (("Input/Output Unit:\r\n  ID:          0x" USIGN32_HEX_FORMAT_STRING "\r\n  InputSize:   %d\r\n  OutputSize:  %d\r\n", pUnit->Id, pUnit->InputSize, pUnit->OutputSize));

    if (PrintIOData)
    {
      _DEBUG_LOG (("  InputStatus: 0x%02X\r\n  Data:   ", pUnit->InputState));
      for (Index = 0; Index < pUnit->InputSize; Index++)
      {
        _DEBUG_LOG (("0x%02X ", pUnit->pInputData [Index]));
      }
      _DEBUG_LOG (("\r\n  OutputStatus: 0x%02X\r\n  Data:   ", pUnit->OutputState));
      for (Index = 0; Index < pUnit->OutputSize; Index++)
      {
        _DEBUG_LOG (("0x%02X ", pUnit->pOutputData [Index]));
      }
    }
  }

  _DEBUG_LOG (("\r\n"));

  return;
}

/*===========================================================================*/

/**
 * The function demo_send_pending_res() checks if there are pending responses for
 * SDAI services and sends them.
 *
 * @return
 * - type  : void
 */
static void demo_send_pending_res ()
{
  U8   Result;


  if (WriteRes.Pending == TRUE)
  {
    if((Result = sdai_write_response (&WriteRes.Header)) != SDAI_SUCCESS)
    {
      if (Result != SDAI_ERR_SERVICE_PENDING) {WriteRes.Pending = FALSE;}

      demo_print_error (Result);
    }
    else
    {
      WriteRes.Pending = FALSE;
    }
  }

  if (ReadRes.Pending == TRUE)
  {
    if((Result = sdai_read_response (&ReadRes.Header)) != SDAI_SUCCESS)
    {
      if (Result != SDAI_ERR_SERVICE_PENDING) {ReadRes.Pending = FALSE;}

      demo_print_error (Result);
    }
    else
    {
      ReadRes.Pending = FALSE;
    }
  }

  if (ControlRes.Pending == TRUE)
  {
    if((Result = sdai_control_response (&ControlRes.Header)) != SDAI_SUCCESS)
    {
      if (Result != SDAI_ERR_SERVICE_PENDING) {ControlRes.Pending = FALSE;}

      demo_print_error (Result);
    }
    else
    {
      ControlRes.Pending = FALSE;
    }
  }

  return;
}

/*===========================================================================*/

/**
 * The function demo_plug_units() adds the Units to the UnitList of the
 * T_DEMO_UNITS_MANAGEMENT structure to the configuration and calls the
 * sdai_plug_unit() function for every unit. After all units are plugged this
 * function also starts the protocol stack by calling sdai_plug_init() with unit type
 * SDAI_UNIT_TYPE_PLUGGING_COMPLETE.
 *
 * @return
 * - type : VOID
 * @param[in] pUnitManagement
 * - type : struct T_DEMO_UNITS_MANAGEMENT*
 * - range: whole address range is valid
 */
void demo_plug_units (struct T_DEMO_UNITS_MANAGEMENT* pUnitManagement)
{
  struct T_DEMO_UNIT* pUnit         = &pUnitManagement->UnitList[0];
  U32                 DummyId;
  U8*                 pDataPosition = &pUnitManagement->IOData[0];
  U8                  Result;


  _TRACE (("demo_plug_units\r\n"));

  while (pUnit <= &pUnitManagement->UnitList[SDAI_MAX_UNITS])
  {
    pUnit->InputState  = SDAI_DATA_STATUS_INVALID;
    pUnit->OutputState = SDAI_DATA_STATUS_INVALID;

    pUnit->pInputData  = NULL;
    pUnit->pOutputData = NULL;

    if (pDataPosition < &pUnitManagement->IOData [_NUMBER_ARRAY_ELEMENTS (pUnitManagement->IOData)])
    {

      /* assign input and output pointer to all units because it is possible to add units later */
      pUnit->pInputData  = pDataPosition;
      pDataPosition     += SDAI_MAX_UNIT_IO_DATA_SIZE;
      pUnit->pOutputData = pDataPosition;
      pDataPosition     += SDAI_MAX_UNIT_IO_DATA_SIZE;

      /* detect the first input and output unit needed for IO_DATA_HANDLING_MIRROR */
      if ((pUnitManagement->pFirstInputUnit == NULL           ) &&
          ((pUnit->Type == SDAI_UNIT_TYPE_GENERIC_INPUT      ) ||
           (pUnit->Type == SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT) ||
           (pUnit->Type == SDAI_MB_UNIT_TYPE_INPUT_REGISTER  ) ||
           (pUnit->Type == SDAI_MB_UNIT_TYPE_HOLDING_REGISTER)) &&
          (pUnit->InputSize != 0                              ))
      {
        pUnitManagement->pFirstInputUnit = pUnit;
      }

      if ((pUnitManagement->pFirstOutputUnit == NULL          ) &&
          ((pUnit->Type == SDAI_UNIT_TYPE_GENERIC_OUTPUT     ) ||
           (pUnit->Type == SDAI_UNIT_TYPE_GENERIC_INPUTOUTPUT) ||
           (pUnit->Type == SDAI_MB_UNIT_TYPE_HOLDING_REGISTER)) &&
          (pUnit->OutputSize != 0                             ))
      {
        pUnitManagement->pFirstOutputUnit = pUnit;
      }

      if (pUnit->Type != 0)
      {
        Result = sdai_plug_unit_ext (pUnit->Type, pUnit->InputSize, pUnit->OutputSize, &pUnit->Id, &pUnit->CfgData);

        demo_print_unit (pUnit, FALSE);

        if (Result != SDAI_SUCCESS)
        {
          demo_print_error (Result);
        }
      }
    }

    pUnit++;
  }

  _DEBUG_LOG (("Signal plugging units complete and start the stack\r\n"));

  Result = sdai_plug_unit (SDAI_UNIT_TYPE_PLUGGING_COMPLETE ,0, 0, &DummyId, NULL);

  if (Result != SDAI_SUCCESS)
  {
    demo_print_error (Result);
  }

  return;
}

/*===========================================================================*/

/**
 * The function demo_print_error() prints the error code returned by the SDAI.
 *
 * @return
 * - type : VOID
 * @param[in] Error SDAI error code
 * - type : U8
 * - range: SDAI_SUCCESS                         |
 *          SDAI_ERR_INTERNAL                    |
 *          SDAI_ERR_NOSUCHUNIT                  |
 *          SDAI_ERR_MAX_UNIT_REACHED            |
 *          SDAI_ERR_INVALID_SIZE                |
 *          SDAI_ERR_UNKNOWN_UNIT_TYPE           |
 *          SDAI_ERR_COM_NOT_SUPPORTED           |
 *          SDAI_ERR_RUNTIME_PLUG                |
 *          SDAI_ERR_INVALID_ARGUMENT            |
 *          SDAI_ERR_SERVICE_PENDING             |
 *          SDAI_ERR_INVALID_ADDR_DESCR          |
 *          SDAI_ERR_STACK_NOT_RDY               |
 *          SDAI_ERR_WATCHDOG_EXPIRED            |
 *          SDAI_ERR_PROTOCOL_NOT_SUPPORTED      |
 *          SDAI_ERR_PROTOCOL_LIBRARY_NOT_LOADED
 */
void demo_print_error (U8 Error)
{
  _TRACE (("demo_print_error\r\n"));

  switch (Error)
  {
    case SDAI_SUCCESS:                         { _ERROR_LOG (("No Error!\r\n"));                          break; }
    case SDAI_ERR_INTERNAL:                    { _ERROR_LOG (("Error: INTERNAL\r\n"));                    break; }
    case SDAI_ERR_NOSUCHUNIT:                  { _ERROR_LOG (("Error: NOSUCHUNIT\r\n"));                  break; }
    case SDAI_ERR_MAX_UNIT_REACHED:            { _ERROR_LOG (("Error: MAX_UNIT_REACHED\r\n"));            break; }
    case SDAI_ERR_INVALID_SIZE:                { _ERROR_LOG (("Error: INVALID_SIZE\r\n"));                break; }
    case SDAI_ERR_UNKNOWN_UNIT_TYPE:           { _ERROR_LOG (("Error: UNKNOWN_UNIT_TYPE\r\n"));           break; }
    case SDAI_ERR_COM_NOT_SUPPORTED:           { _ERROR_LOG (("Error: COM_NOT_SUPPORTED\r\n"));           break; }
    case SDAI_ERR_RUNTIME_PLUG:                { _ERROR_LOG (("Error: RUNTIME_PLUG\r\n"));                break; }
    case SDAI_ERR_INVALID_ARGUMENT:            { _ERROR_LOG (("Error: INVALID_ARGUMENT\r\n"));            break; }
    case SDAI_ERR_SERVICE_PENDING:             { _ERROR_LOG (("Error: SERVICE_PENDING\r\n"));             break; }
    case SDAI_ERR_INVALID_ADDR_DESCR:          { _ERROR_LOG (("Error: INVALID_ADDR_DESCR\r\n"));          break; }
    case SDAI_ERR_STACK_NOT_RDY:               { _ERROR_LOG (("Error: STACK_NOT_RDY\r\n"));               break; }
    case SDAI_ERR_WATCHDOG_EXPIRED:            { _ERROR_LOG (("Error: WATCHDOG_EXPIRED\r\n"));            break; }
    case SDAI_ERR_PROTOCOL_NOT_SUPPORTED:      { _ERROR_LOG (("Error: PROTOCOL_NOT_SUPPORTED\r\n"));      break; }
    case SDAI_ERR_PROTOCOL_LIBRARY_NOT_LOADED: { _ERROR_LOG (("Error: PROTOCOL_LIBRARY_NOT_LOADED\r\n")); break; }
    case SDAI_ERR_WRONG_STATE:                 { _ERROR_LOG (("Error: WRONG_STATE\r\n"));                 break; }
    case SDAI_ERR_MAX_DIAG_REACHED:            { _ERROR_LOG (("Error: MAX_DIAG_REACHED\r\n"));            break; }
    default:                                   { _ERROR_LOG (("Error: %d unknown\r\n", Error));           break; }
  }

  return;
}

/*===========================================================================*/

/**
 * The function demo_find_unit() returns a pointer to the unit for the passed ID
 * or NULL if the unit is not found. If SDAI_INVALID_ID is passed as ID the function
 * returns the first unused unit
 *
 * @return
 * - type  : struct DEMO_UNIT*
 * @param[in] Id
 * - type : U32
 * - range: protocol dependent
 */
struct T_DEMO_UNIT* demo_find_unit (U32 Id)
{
  struct T_DEMO_UNIT* pUnit;
  U8                  Index;


  _TRACE (("demo_find_unit\r\n"));

  for (Index = 0; Index < SDAI_MAX_UNITS; Index++)
  {
    pUnit = (ApplManagement.UnitsManagement.UnitList + Index);

    if (pUnit->Id == Id) {return (pUnit);}
  }

  return (NULL);
}

/*===========================================================================*/

/**
 * The function demo_get_ip_address() returns a pointer to memory where the IP-Address
 * is stored.
 *
 * @return
 * - type  : U8*
 */
U8* demo_get_ip_address (void)
{
  if (ApplManagement.ConfigDataAppl.BackEnd == SDAI_BACKEND_PN)
  {
    return (&IdentData.IdentData.UseAs.Pn.Address [0]);
  }
  else if (ApplManagement.ConfigDataAppl.BackEnd == SDAI_BACKEND_EPL)
  {
    IdentData.IdentData.UseAs.Pn.Address [0] = 192;
    IdentData.IdentData.UseAs.Pn.Address [1] = 168;
    IdentData.IdentData.UseAs.Pn.Address [2] = 100;
    IdentData.IdentData.UseAs.Pn.Address [3] = ApplManagement.ConfigDataAppl.EplData.EplNodeId;

    return (&IdentData.IdentData.UseAs.Pn.Address [0]);
  }

  return (&ApplManagement.ConfigDataAppl.IpAddress [0]);
}

/*===========================================================================*/

/**
 * The function demo_task_main_function() is the main function of the demo application.
 * The functionality of the demo application is described by the \ref demo_appl_overview.
 *
 * @return
 * - type  : VOID
 */
void demo_task_main_function (void)
{
  U32                     Events;
  U32                     EventsToWait = EVENT_ALL_EVENTS;
  U32                     Index;
  U8                      Result = 0;
  U8                      ButtonNew = 0;
  U8                      ButtonOld = 0;
  U8                      ProtoFuncReturn;


  _TRACE (("demo_task_main_function\r\n"));

  _DEBUG_LOG (("Starting SDAI Demo Application\r\n"));

  /*-----------------------------------------------------------*/
  /*--- initialization and start of the protocol software -----*/
  /*-----------------------------------------------------------*/

  /* init LEDs */
  STATUS_LED_INIT();
  DATA_LED_INIT();

  STATUS_LED_SWITCH(LED_ALL, LED_OFF);
  DATA_LED_SWITCH(0xFF, LED_OFF);

  platform_test_leds ();

  /* initialize unit management structure */
  memset (&ApplManagement, 0x00, sizeof(ApplManagement));

  /* read the flash configuration (no locking because stack is not running) */
  platform_read_flash_config (&ApplManagement.ConfigDataAppl, &ApplManagement.ConfigDataHw);

  /* init network, web server, sntp client and smtp client */
  #ifdef INCLUDE_WEBSERVER
  reinit_network ();
  #endif

Restart:

  STATUS_LED_SWITCH(LED_ALL, LED_OFF);
  DATA_LED_SWITCH(0xFF, LED_OFF);

  CurrentProtocol = ApplManagement.ConfigDataAppl.BackEnd;

  /* initialize stack status */
  ApplManagement.StackStatus = 0;

  /* initialize the unit management structure */
  memset(&ApplManagement.UnitsManagement, 0x00, sizeof(ApplManagement.UnitsManagement));

  ApplManagement.UnitsManagement.proto_set_output_data = NULL;
  ApplManagement.UnitsManagement.proto_get_input_data  = NULL;

  for (Index = 0; Index < _NUMBER_ARRAY_ELEMENTS (ApplManagement.UnitsManagement.UnitList); Index++)
  {
    ApplManagement.UnitsManagement.UnitList [Index].Id = SDAI_INVALID_ID;
  }

  /* initialize callback synchronization flags */
  WriteInd.InUse      = FALSE;
  WriteRes.Pending    = FALSE;
  ReadInd.InUse       = FALSE;
  ReadRes.Pending     = FALSE;
  ControlInd.InUse    = FALSE;
  ControlRes.Pending  = FALSE;
  ExceptionData.InUse = FALSE;
  IdentData.InUse     = FALSE;

  /* initialize protocol callback function with the selected protocol */
  switch (ApplManagement.ConfigDataAppl.BackEnd)
  {
    case SDAI_BACKEND_PN:
      ApplManagement.proto_get_init_data              = profinet_get_init_data;
      ApplManagement.proto_get_unit_data              = profinet_get_unit_data;
      ApplManagement.proto_cyclic_timer               = profinet_cyclic_timer;
      ApplManagement.proto_button_event               = profinet_button_event;
      ApplManagement.proto_get_input_data             = profinet_get_input_data;
      ApplManagement.proto_set_output_data            = profinet_set_output_data;
      ApplManagement.proto_process_write_ind          = profinet_process_write_ind;
      ApplManagement.proto_process_read_ind           = profinet_process_read_ind;
      ApplManagement.proto_process_control_ind        = profinet_process_control_ind;
      ApplManagement.proto_update_ident_data          = profinet_update_ident_data;
      ApplManagement.proto_process_alarm_ack          = NULL;
      ApplManagement.proto_process_additional_status  = profinet_print_additional_status_data;
      _DEBUG_LOG (("Selected Protocol: PROFINET IO\r\n"));
      break;

    default:
      _ERROR_LOG (("Invalid or not supported backend: %d\r\n\r\n", ApplManagement.ConfigDataAppl.BackEnd));
      break;
  }

  /*--- initialize SDAI and protocol software -------------------------------*/

  memset (&ApplManagement.SdaiInitData, 0, sizeof (ApplManagement.SdaiInitData));

  /* init all callback functions */
  ApplManagement.SdaiInitData.Callback.IdentDataCbk   = demo_ident_data_callback;
  ApplManagement.SdaiInitData.Callback.ExceptionCbk   = demo_exception_callback;
  ApplManagement.SdaiInitData.Callback.DataCbk        = demo_output_data_callback;
  ApplManagement.SdaiInitData.Callback.AlarmAckCbk    = NULL;                       // only used by profinet
  ApplManagement.SdaiInitData.Callback.WriteReqCbk    = demo_write_req_callback;
  ApplManagement.SdaiInitData.Callback.ReadReqCbk     = demo_read_req_callback;
  ApplManagement.SdaiInitData.Callback.ControlReqCbk  = demo_control_req_callback;

  /* set common init data */
  strncpy (ApplManagement.SdaiInitData.Ident.DevName, (char*) ApplManagement.ConfigDataAppl.DeviceName, sizeof (ApplManagement.SdaiInitData.Ident.DevName));

  /* get platform specific init data */
  platform_get_hw_data (&ApplManagement.SdaiInitData);

  /* get protocol specific init data, unused callback functions are set back to NULL */
  profinet_get_init_data(&ApplManagement.ConfigDataAppl, &ApplManagement.SdaiInitData);

  /* initialize protocol software */
  if ((Result = sdai_init (&ApplManagement.SdaiInitData)) != SDAI_SUCCESS)
  {
    demo_print_error (Result);

    _ERROR_LOG (("SDAI initialization failed!!!"));

    while (1) {;}
  }
  else
  {
    demo_update_stack_status ();
  }

  /* read and print firmware version */
  if ((Result = sdai_get_version (&ApplManagement.VersionData)) != SDAI_SUCCESS)
  {
    demo_print_error (Result);
  }
  else
  {
    /* The version string is not necessarily null terminated (the version string has a length of SDAI_VERSION_STRING_LENGTH bytes) */
    _DEBUG_LOG (("\r\nSdai Version:  %16s\r\n", ApplManagement.VersionData.SdaiVersionString));
    _DEBUG_LOG (("MAC Version:   %16s\r\n",     ApplManagement.VersionData.MacVersionString));
    _DEBUG_LOG (("Stack Version: %16s\r\n\r\n", ApplManagement.VersionData.StackVersionString));
  }

  /* get IO configuration for the selected protocol */
  profinet_get_unit_data(&ApplManagement.UnitsManagement);

  /* plug units and signal plugging is complete and start the stack */
  demo_plug_units(&ApplManagement.UnitsManagement);

  platform_visualize_data (&ApplManagement);

  #ifdef SDAI_SOCKET_INTERFACE
  socket_init ();
  #endif

  /*-------------------------------------------------------------------------*/
  /*--- cyclic input data update and processing of events from backend  -----*/
  /*--- stack or web server                                             -----*/
  /*-------------------------------------------------------------------------*/

  #if IO_DATA_HANDLING_MIRROR
  /* The data are only changed on response to changed output data so we need to set
     the state to SDAI_DATA_STATUS_VALID initially */
  demo_update_input_data ();
  #endif

  /* loop forever */
  while (1)
  {
    /* wait on events */
    Events = platform_wait_event (EventsToWait);

    /* process the occurred events */
    if (Events & EVENT_TERMINATE)
    {
      #ifdef SDAI_SOCKET_INTERFACE
      socket_term ();
      #endif
      sdai_deinit ();

      memset (&ApplManagement, 0x00, sizeof(ApplManagement));

      break;
    }

    if (Events & EVENT_REBOOT)
    {
      /* wait some time until all actions are finished */
      platform_busy_sleep (1000000);

      PLATFORM_RESTART();

      break;
    }

    if (Events & EVENT_CYCLIC_TIMER)
    {
      demo_update_stack_status ();
      #if !IO_DATA_HANDLING_MIRROR
      demo_update_input_data ();
      #endif
      demo_send_pending_res ();

      profinet_cyclic_timer();

      /* check for button event */
      ButtonNew = platform_get_buttons ();

      if (ButtonNew != ButtonOld)
      {
        profinet_button_event(ButtonNew & ~ButtonOld);

        ButtonOld = ButtonNew;

        platform_visualize_data (&ApplManagement);
      }
    }

    if (Events & EVENT_OUTPUT_DATA_CHANGED)
    {
      demo_update_output_data ();
    }

    if (Events & EVENT_IDENT_DATA_CHANGED)
    {
      profinet_update_ident_data(&IdentData);

      platform_visualize_data (&ApplManagement);

      IdentData.InUse = FALSE;
    }

    // if (Events & EVENT_ALARM_ACK_RECEIVED)
    // {
    //   profinet_process_alarm_ack(NULL);
    // }

    if (Events & EVENT_WRITE_IND_RECEIVED)
    {
      ProtoFuncReturn = profinet_process_write_ind(&WriteInd, &WriteRes);

      WriteInd.InUse = FALSE;

      if (ProtoFuncReturn == PROTO_FUNC_SUCCESS)
      {
        if ((Result = sdai_write_response (&WriteRes.Header)) != SDAI_SUCCESS)
        {
          if (Result == SDAI_ERR_SERVICE_PENDING) {WriteRes.Pending = TRUE;}

          demo_print_error (Result);
        }
      }
    }

    if (Events & EVENT_READ_IND_RECEIVED)
    {
      ProtoFuncReturn = profinet_process_read_ind(&ReadInd, &ReadRes);

      ReadInd.InUse = FALSE;

      if (ProtoFuncReturn == PROTO_FUNC_SUCCESS)
      {
        if ((Result = sdai_read_response (&ReadRes.Header)) != SDAI_SUCCESS)
        {
          if (Result == SDAI_ERR_SERVICE_PENDING) {ReadRes.Pending = TRUE;}

          demo_print_error (Result);
        }
      }
    }

    if (Events & EVENT_CONTROL_IND_RECEIVED)
    {
      profinet_process_control_ind(&ControlInd, &ControlRes);

      ControlInd.InUse = FALSE;

      do
      {
        Result = sdai_control_response (&ControlRes.Header);

      } while (Result == SDAI_ERR_SERVICE_PENDING);

      if (Result != SDAI_SUCCESS)
      {
        demo_print_error (Result);
      }
    }

    if (Events & EVENT_EXCEPTION)
    {
      EventsToWait = EVENT_EXCEPTION_EVENTS;

      demo_process_exception ();
    }

    if (Events & (EVENT_SOCKET_DATA_RECEIVED | EVENT_SOCKET_STATE_CHANGED))
    {
      #ifdef SDAI_SOCKET_INTERFACE
      socket_process_events (Events);
      #endif
    }

    /* first store new data to flash before restarting stack */
    if (Events & EVENT_STORE_FLASH_DATA)
    {
      /* we need locking because the stack is running and could also access the FLASH */
      sdai_lock_resources (SDAI_SHARED_RESOURCE_FLASH);
      platform_write_flash_config (&ApplManagement.ConfigDataAppl);
      sdai_unlock_resources (SDAI_SHARED_RESOURCE_FLASH);
    }

    if (Events & EVENT_RESTART_NETWORK)
    {
      #ifdef SDAI_SOCKET_INTERFACE
      socket_term ();
      socket_init ();
      #endif
      #ifdef INCLUDE_WEBSERVER
      reinit_network();
      #endif
    }

    if (Events & EVENT_RESTART_STACK)
    {
      #ifdef SDAI_SOCKET_INTERFACE
      socket_term ();
      #endif
      sdai_deinit ();

      /* the use of "goto" is controversial but here it is the best way */
      goto Restart;
    }
  }

  return;
}

/*===========================================================================*/

/**
 * The function demo_print_sdai_error_text() returns the SDAI error
 * state as a textual string. The following table shows the possible
 * strings and their meaning.
 *
 * <TABLE>
 * <TR><TH>Text</TH><TH>Description</TH></TR>
 * <TR><TD>ERR Hardware</TD><TD>Hardware error detected by the stack. For example Ethernet interface not working or IP core problem</TD></TR>
 * <TR><TD>ERR License</TD><TD>No valid licence could be detected. The device will stop working after the evaluation period.</TD></TR>
 * <TR><TD>ERR Watchdog</TD><TD>The application watchdog is expired (function sdai_check_and_trigger_watchdog() not called)</TD></TR>
 * <TR><TD>ERR NVStorage</TD><TD>The protocol stack cannot access non volatile storage.</TD></TR>
 * <TR><TD>ERR Protocol</TD><TD>Protocol specific error occured. Please see additional status data for more details</TD></TR>
 * </TABLE>
 *
 * @return
 * - type : char* The status as string
 */
const char* demo_print_sdai_error_text (void)
{
  if (ApplManagement.StackStatus & SDAI_STATE_ERROR_MASK)
  {
    if (ApplManagement.StackStatus & SDAI_STATE_MASK_HARDWARE_ERROR)
    {
      return ("ERR Hardware");
    }
    else if (ApplManagement.StackStatus & SDAI_STATE_MASK_LICENSE_ERROR)
    {
      return ("ERR License");
    }
    else if (ApplManagement.StackStatus & SDAI_STATE_MASK_APPL_WATCHDOG_EXPIRED)
    {
      return ("ERR Watchdog");
    }
    else if (ApplManagement.StackStatus & SDAI_STATE_MASK_NON_VOLATILE_STORAGE_ERROR)
    {
      return ("ERR NVStorage");
    }
    else if (ApplManagement.StackStatus & SDAI_STATE_MASK_PROTOCOL_ERROR)
    {
      return ("ERR Protocol");
    }
  }

  return ("");
}

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
