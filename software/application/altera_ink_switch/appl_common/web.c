/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

#if defined(SDAI_SOCKET_INTERFACE) && defined(SDAI_WEBSERVER)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <malloc.h>

#include "sdai.h"
#include "sdai_socket.h"

#include "demo_platform.h"
#include "demo.h"

#include "socket.h"

#include "webserver/output/css/stylesheet.h"

#include "webserver/output/html/header.h"
#include "webserver/output/html/footer.h"

#include "webserver/output/html/overview.h"
#include "webserver/output/html/iodata.h"
#include "webserver/output/html/licence.h"
#include "webserver/output/html/version.h"
#include "webserver/output/html/diagnosis.h"
#include "webserver/output/html/contact.h"
#include "webserver/output/html/network.h"
#include "webserver/output/html/firmware.h"
#include "webserver/output/html/not_found.h"
#include "webserver/output/html/no_access.h"
#include "webserver/output/html/inv_req.h"

#include "webserver/output/jscript/utility.h"
#include "webserver/output/jscript/overview.h"
#include "webserver/output/jscript/version.h"
#include "webserver/output/jscript/diagnosis.h"
#include "webserver/output/jscript/iodata.h"
#include "webserver/output/jscript/network.h"
#include "webserver/output/jscript/firmware.h"

#include "webserver/output/images/ajax_loader.h"
#include "webserver/output/images/favicon.h"
#include "webserver/output/images/softing_logo.h"
#include "webserver/output/images/body.h"
#include "webserver/output/images/footer_wrap.h"
#include "webserver/output/images/menu_act.h"
#include "webserver/output/images/submenu_act.h"
#include "webserver/output/images/news_feed_header.h"

/******************************************************************************
DEFINES
******************************************************************************/

/** \addtogroup socket_def */
//@{
#define WEB_PAGE_RETURN_SUCCESS        0  /**< Page handler executed successful */
#define WEB_PAGE_RETURN_ERROR_NOTEXIST 1  /**< Page handler returned page not existing */
#define WEB_PAGE_RETURN_ERROR_REQUEST  2  /**< Page handler returned request invalid */
#define WEB_PAGE_RETURN_ERROR_NOACCESS 3  /**< Page handler returned no access */

#define AJAX_PAGE_DIAGNOSIS   1  /**< Ajax request number for diagnosis page */
#define AJAX_PAGE_IODATA      2  /**< Ajax request number for IO data page */
#define AJAX_PAGE_FIRMWARE    3  /**< Ajax request number for firmware page */
#define AJAX_PAGE_OVERVIEW    4  /**< Ajax request number for overview page */
#define AJAX_PAGE_NETWORK     5  /**< Ajax request number for network page */
#define AJAX_PAGE_VERSION     6  /**< Ajax request number for version page */
//@}

/******************************************************************************
TYPEDEFS
******************************************************************************/

/** \addtogroup socket_struct */
//@{
/** \brief function pointer type of the web handlers */
typedef int webpage_handler (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata);

/** \brief This structure describes the informationen for one page entry */
typedef struct _T_WEB_PAGE_ENTRY
{
  char*            pFilename;   /**< Pointer to the filename of the page */
  webpage_handler* pHandler;    /**< Function pointer to the handler of the page */

} T_WEB_PAGE_ENTRY;

/** \brief This structure describes the information for a HTTP header line */
typedef struct _T_WEB_HTML_HEADER_LINE
{
  char*            pName;       /**< Pointer to the name of the header line */
  char*            pContent;    /**< Pointer to the content of the header line */

} T_WEB_HTML_HEADER_LINE;

/** \brief This structure describes the information for a HTTP request */
typedef struct _T_HTML_HEADER
{
  char*                   pRequest;         /**< Pointer to the request type in the header (e.g. GET) */
  char*                   pFilename;        /**< Pointer to the URL in the header */
  char*                   pFormdata;        /**< Pointer to the Formdata in the Header */
  char*                   pBody;            /**< Pointer to the start of the body */

  T_WEB_HTML_HEADER_LINE  HeaderLines [20]; /**< Array holding pointers to the individual header lines */
  U32                     NumberLines;      /**< Number of decoded header lines */

  U32                     RemainingLength;  /**< Length of the body */

} T_HTML_HEADER;
//@}

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

/** \addtogroup socket_func */
//@{
static int web_ajax_handler           (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);

static int web_css_stylesheet         (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);

static int web_html_overview          (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_html_iodata            (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_html_diagnosis         (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_html_version           (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_html_licence           (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_html_contact           (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_html_network           (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_html_firmware          (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);

static int web_jscript_utility        (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_jscript_overview       (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_jscript_diagnosis      (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_jscript_version        (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_jscript_iodata         (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_jscript_network        (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_jscript_firmware       (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);

static int web_image_ajaxloader       (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_image_favicon          (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_image_softing_logo     (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_image_body             (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_image_footer_wrap      (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_image_menu_act         (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_image_submenu_act      (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);
static int web_image_news_feed_header (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*);

static void web_http_start            (T_SOCKET_ELEMENT*, T_SDAI_SOCK_ADDR*, char*, int);
//@}

/******************************************************************************
PUBLIC DATA
******************************************************************************/

extern struct T_DEMO_EXCEPTION_DATA   ExceptionData; /**< Holds the exception data of the sample application */

/******************************************************************************
PRIVATE DATA
******************************************************************************/

static U8 WebTxDataBuffer [4 * SOCKET_FIFO_SIZE]; /**< Tx buffer for sending web responses */

static T_WEB_PAGE_ENTRY WebPages [] =
{
  {"/ajax_handler",                web_ajax_handler},
  {"/css/stylesheet.css",          web_css_stylesheet},
  {"/jscript/utility.js",          web_jscript_utility},
  {"/jscript/overview.js",         web_jscript_overview},
  {"/jscript/diagnosis.js",        web_jscript_diagnosis},
  {"/jscript/version.js",          web_jscript_version},
  {"/jscript/iodata.js",           web_jscript_iodata},
  {"/jscript/network.js" ,         web_jscript_network},
  {"/jscript/firmware.js",         web_jscript_firmware},
  {"/images/ajax_loader.gif",      web_image_ajaxloader},
  {"/images/favicon.ico",          web_image_favicon},
  {"/images/softing_logo.png",     web_image_softing_logo},
  {"/images/body.png",             web_image_body},
  {"/images/footer_wrap.png",      web_image_footer_wrap},
  {"/images/menu_act.png",         web_image_menu_act},
  {"/images/submenu_act.png",      web_image_submenu_act},
  {"/images/news_feed_header.png", web_image_news_feed_header},
  {"/",                            web_html_overview},
  {"/overview.html",               web_html_overview},
  {"/diagnosis.html",              web_html_diagnosis},
  {"/version.html",                web_html_version},
  {"/iodata.html",                 web_html_iodata},
  {"/licence.html",                web_html_licence},
  {"/contact.html",                web_html_contact},
  {"/firmware.html",               web_html_firmware},
  {"/network.html",                web_html_network}
}; /**< Array holding the URLs and handlers for the implemented web elements */

static T_HTML_HEADER         HtmlHeader;  /**< Variable holding the informations for the current HTTP request */

static T_CONFIG_DATA_APPL*   pFlashConfigData = &ApplManagement.ConfigDataAppl;
static T_CONFIG_DATA_HW*     pFlashHwData     = &ApplManagement.ConfigDataHw;

/******************************************************************************
FUNCTION IMPLEMENTATION
******************************************************************************/

/**
 * The function web_parse_header() parses and verifies the received
 * HTTP header. First it checks if the type is GET or POST. If a GET request
 * is received the Formdata are decoded following the URL. Finally the other
 * header lines are decoded and the references are stored.
 *
 * @return
 * - type : char*
 * @param[in] pData Pointer to the received http request
 * - type : char*
 * - range: whole address range is valid
 * @param[in] RemainingLength Maximum length of the
 * - type : int
 * - range: while range is valid
 */
char* web_parse_header (char* pData, int RemainingLength)
{
  char* pHelper;

  HtmlHeader.NumberLines = 0;

  HtmlHeader.pRequest = pData;

  HtmlHeader.pFilename = strstr (pData, " ");
  if (HtmlHeader.pFilename == NULL) {return (NULL);}
  *HtmlHeader.pFilename = '\0';
  HtmlHeader.pFilename++;

  /* only accept GET and POST requests */
  if (!strstr (HtmlHeader.pRequest, "GET") &&
      !strstr (HtmlHeader.pRequest, "POST"))
  {
    {return (NULL);}
  }

  HtmlHeader.pFormdata = strpbrk (HtmlHeader.pFilename, " ?");
  if (HtmlHeader.pFormdata == NULL) {return (NULL);}

  pHelper = strstr (HtmlHeader.pFormdata, "\r\n");
  if (pHelper == NULL) {return (NULL);}
  pHelper         += 2;
  RemainingLength -= (int)(pHelper - pData);
  pData            = pHelper;

  /* parse the rest of the html header lines */
  while (RemainingLength && (HtmlHeader.NumberLines < _NUMBER_ARRAY_ELEMENTS (HtmlHeader.HeaderLines)))
  {
    if ((*pData == '\r') && (*(pData + 1) == '\n'))
    {
      if (*HtmlHeader.pFormdata == '?')
      {
        pHelper = strstr (HtmlHeader.pFormdata, " ");
        *HtmlHeader.pFormdata = '\0';
        *pHelper = '\0';
        HtmlHeader.pFormdata++;
      }
      else
      {
        *HtmlHeader.pFormdata = '\0';

        if (strstr (HtmlHeader.pRequest, "POST"))
        {
          HtmlHeader.pFormdata = pData + 2;
        }
        else
        {
          HtmlHeader.pFormdata = NULL;
        }
      }

      return (pData + 2);
    }

    pHelper = strchr (pData, ':');
    if (pHelper == NULL) {return (NULL);}
    HtmlHeader.HeaderLines [HtmlHeader.NumberLines].pName = pData;
    *pHelper = '\0';
    pHelper += 2;

    HtmlHeader.HeaderLines [HtmlHeader.NumberLines].pContent = pHelper;
    pHelper = strstr (pHelper, "\r\n");
    if (pHelper == NULL) {return (NULL);}
    *pHelper = '\0';
    pHelper += 2;

    RemainingLength -= (int)(pHelper - pData);
    pData            = pHelper;

    HtmlHeader.NumberLines++;
  }

  return (NULL);
}

/*===========================================================================*/

/**
 * The function web_get_header_line() return a pointer to the header line
 * with the requested name.
 *
 * @return
 * - type : char*
 * @param[in] pName Pointer to the name string
 * - type : char*
 * - range: whole address range is valid
 */
char* web_get_header_line (char* pName)
{
  unsigned Index;

  for (Index = 0; Index < HtmlHeader.NumberLines; Index++)
  {
    if (strcmp (pName, HtmlHeader.HeaderLines [Index].pName) == 0)
    {
      return (HtmlHeader.HeaderLines [Index].pContent);
    }
  }

  return (NULL);
}

/*===========================================================================*/

/**
 * The function web_handle_request() is the main handler function for requests received
 * on TCP port 80. It is called by the socket interface and parses the received data for
 * a valid HTTP Header. If the request is valid the list of web handlers is searched for
 * the handler with the requested URL.
 *
 * @return
 * - type : void
 * @param[in] pData Pointer to the received data
 * - type : char*
 * - range: whole address range is valid
 * @param[in] RemainingLength Length of the received data
 * - type : int
 * - range: whole range is valid
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 */
void web_handle_request (char* pData, int RemainingLength, T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress)
{
  U32   Index;
  int   Result = WEB_PAGE_RETURN_ERROR_REQUEST;

  HtmlHeader.pBody = web_parse_header (pData, RemainingLength);

  HtmlHeader.RemainingLength = RemainingLength - (HtmlHeader.pBody - pData);

  if (HtmlHeader.pBody)
  {
    for (Index = 0; Index < _NUMBER_ARRAY_ELEMENTS(WebPages); Index++)
    {
      if (!strcmp (HtmlHeader.pFilename, WebPages [Index].pFilename))
      {
        Result = (WebPages [Index].pHandler)(pSocketEl, pRemoteAddress, HtmlHeader.pFormdata);
      }
    }
  }

  if (Result != WEB_PAGE_RETURN_SUCCESS)
  {
    if (Result == WEB_PAGE_RETURN_ERROR_NOTEXIST)
    {
      web_http_start (pSocketEl, pRemoteAddress, "text/html", sizeof (header_html) + sizeof (not_found_html) + sizeof (footer_html));
      socket_send (pSocketEl, pRemoteAddress, sizeof (header_html), (U8*)&header_html [0]);
      socket_send (pSocketEl, pRemoteAddress, sizeof (not_found_html), (U8*)&not_found_html [0]);
    }

    if (Result == WEB_PAGE_RETURN_ERROR_REQUEST)
    {
      web_http_start (pSocketEl, pRemoteAddress, "text/html", sizeof (header_html) + sizeof (inv_req_html) + sizeof (footer_html));
      socket_send (pSocketEl, pRemoteAddress, sizeof (header_html), (U8*)&header_html [0]);
      socket_send (pSocketEl, pRemoteAddress, sizeof (inv_req_html), (U8*)&inv_req_html [0]);
    }

    if (Result == WEB_PAGE_RETURN_ERROR_NOACCESS)
    {
      web_http_start (pSocketEl, pRemoteAddress, "text/html", sizeof (header_html) + sizeof (no_access_html) + sizeof (footer_html));
      socket_send (pSocketEl, pRemoteAddress, sizeof (header_html), (U8*)&header_html [0]);
      socket_send (pSocketEl, pRemoteAddress, sizeof (no_access_html), (U8*)&no_access_html [0]);
    }

    socket_send (pSocketEl, pRemoteAddress, sizeof (footer_html), (U8*)&footer_html [0]);
  }

  return;
}

/*===========================================================================*/

/**
 * The function web_http_start() creates and sends the HTTP header for the response
 *
 * @return
 * - type : void
 * @param[in] pSocketEl ...
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress ...
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] ContentType ...
 * - type : char*
 * - range: whole address range is valid
 * @param[in] ContentLength ...
 * - type : int
 * - range: whole address range is valid
 */
static void web_http_start (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* ContentType, int ContentLength)
{
  U32   DataLength;
  U8    SendBuffer [200];

  DataLength = snprintf ((char*) &SendBuffer [0], sizeof (SendBuffer),
               "HTTP/1.1 200 OK\n"
               "Server:SDAI Sample\n"
               "Cache-Control: max-age\n"
               "Expires: Thu, 31 Dec 2010 16:00:00 GMT\n"
               "Content-type: %s\n"
               "Content-length: %u\n"
               "Connection: close\n\n", ContentType, ContentLength);

  socket_send (pSocketEl, pRemoteAddress, DataLength, &SendBuffer [0]);

  return;
}

/*===========================================================================*/

/**
 * The function web_css_stylesheet() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_css_stylesheet (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "text/css", sizeof (stylesheet_css));
  socket_send (pSocketEl, pRemoteAddress, sizeof (stylesheet_css), (U8*)&stylesheet_css [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_html_overview() is the handler for one of the web objects (function name = object name).
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_html_overview (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "text/html", sizeof (header_html) + sizeof (overview_html) + sizeof (footer_html));
  socket_send (pSocketEl, pRemoteAddress, sizeof (header_html), (U8*)&header_html [0]);
  socket_send (pSocketEl, pRemoteAddress, sizeof (overview_html), (U8*)&overview_html [0]);
  socket_send (pSocketEl, pRemoteAddress, sizeof (footer_html), (U8*)&footer_html [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_html_iodata() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_html_iodata (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "text/html", sizeof (header_html) + sizeof (iodata_html) + sizeof (footer_html));
  socket_send (pSocketEl, pRemoteAddress, sizeof (header_html), (U8*)&header_html [0]);
  socket_send (pSocketEl, pRemoteAddress, sizeof (iodata_html), (U8*)&iodata_html [0]);
  socket_send (pSocketEl, pRemoteAddress, sizeof (footer_html), (U8*)&footer_html [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_html_diagnosis() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_html_diagnosis (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "text/html", sizeof (header_html) + sizeof (diagnosis_html) + sizeof (footer_html));
  socket_send (pSocketEl, pRemoteAddress, sizeof (header_html), (U8*)&header_html [0]);
  socket_send (pSocketEl, pRemoteAddress, sizeof (diagnosis_html), (U8*)&diagnosis_html [0]);
  socket_send (pSocketEl, pRemoteAddress, sizeof (footer_html), (U8*)&footer_html [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_html_version() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_html_version (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "text/html", sizeof (header_html) + sizeof (version_html) + sizeof (footer_html));
  socket_send (pSocketEl, pRemoteAddress, sizeof (header_html), (U8*)&header_html [0]);
  socket_send (pSocketEl, pRemoteAddress, sizeof (version_html), (U8*)&version_html [0]);
  socket_send (pSocketEl, pRemoteAddress, sizeof (footer_html), (U8*)&footer_html [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_html_licence() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_html_licence (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "text/html", sizeof (header_html) + sizeof (licence_html) + sizeof (footer_html));
  socket_send (pSocketEl, pRemoteAddress, sizeof (header_html), (U8*)&header_html [0]);
  socket_send (pSocketEl, pRemoteAddress, sizeof (licence_html), (U8*)&licence_html [0]);
  socket_send (pSocketEl, pRemoteAddress, sizeof (footer_html), (U8*)&footer_html [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_html_contact() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_html_contact (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "text/html", sizeof (header_html) + sizeof (contact_html) + sizeof (footer_html));
  socket_send (pSocketEl, pRemoteAddress, sizeof (header_html), (U8*)&header_html [0]);
  socket_send (pSocketEl, pRemoteAddress, sizeof (contact_html), (U8*)&contact_html [0]);
  socket_send (pSocketEl, pRemoteAddress, sizeof (footer_html), (U8*)&footer_html [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_html_network() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_html_network (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "text/html", sizeof (header_html) + sizeof (network_html) + sizeof (footer_html));
  socket_send (pSocketEl, pRemoteAddress, sizeof (header_html), (U8*)&header_html [0]);
  socket_send (pSocketEl, pRemoteAddress, sizeof (network_html), (U8*)&network_html [0]);
  socket_send (pSocketEl, pRemoteAddress, sizeof (footer_html), (U8*)&footer_html [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_html_firmware() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_html_firmware (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "text/html", sizeof (header_html) + sizeof (firmware_html) + sizeof (footer_html));
  socket_send (pSocketEl, pRemoteAddress, sizeof (header_html), (U8*)&header_html [0]);
  socket_send (pSocketEl, pRemoteAddress, sizeof (firmware_html), (U8*)&firmware_html [0]);
  socket_send (pSocketEl, pRemoteAddress, sizeof (footer_html), (U8*)&footer_html [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_jscript_utility() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_jscript_utility (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "text/js", sizeof (utility_js));
  socket_send (pSocketEl, pRemoteAddress, sizeof (utility_js), (U8*)&utility_js [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_jscript_overview() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_jscript_overview (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "text/js", sizeof (overview_js));
  socket_send (pSocketEl, pRemoteAddress, sizeof (overview_js), (U8*)&overview_js [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_jscript_diagnosis() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_jscript_diagnosis (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "text/js", sizeof (diagnosis_js));
  socket_send (pSocketEl, pRemoteAddress, sizeof (diagnosis_js), (U8*)&diagnosis_js [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_jscript_version() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_jscript_version (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "text/js", sizeof (version_js));
  socket_send (pSocketEl, pRemoteAddress, sizeof (version_js), (U8*)&version_js [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_jscript_iodata() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_jscript_iodata (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "text/js", sizeof (iodata_js));
  socket_send (pSocketEl, pRemoteAddress, sizeof (iodata_js), (U8*)&iodata_js [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_jscript_network() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_jscript_network (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "text/js", sizeof (network_js));
  socket_send (pSocketEl, pRemoteAddress, sizeof (network_js), (U8*)&network_js [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_jscript_firmware() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_jscript_firmware (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "text/js", sizeof (firmware_js));
  socket_send (pSocketEl, pRemoteAddress, sizeof (firmware_js), (U8*)&firmware_js [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_image_ajaxloader() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_image_ajaxloader (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "image/gif", sizeof (ajax_loader_gif));
  socket_send (pSocketEl, pRemoteAddress, sizeof (ajax_loader_gif), (U8*)&ajax_loader_gif [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_image_favicon() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_image_favicon (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "image/ico", sizeof (favicon_ico));
  socket_send (pSocketEl, pRemoteAddress, sizeof (favicon_ico), (U8*)&favicon_ico [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_image_softing_logo() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_image_softing_logo (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "image/png", sizeof (softing_logo_png));
  socket_send (pSocketEl, pRemoteAddress, sizeof (softing_logo_png), (U8*)&softing_logo_png [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_image_body() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_image_body (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "image/png", sizeof (body_png));
  socket_send (pSocketEl, pRemoteAddress, sizeof (body_png), (U8*)&body_png [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_image_footer_wrap() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_image_footer_wrap (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "image/png", sizeof (footer_wrap_png));
  socket_send (pSocketEl, pRemoteAddress, sizeof (footer_wrap_png), (U8*)&footer_wrap_png [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_image_menu_act() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_image_menu_act (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "image/png", sizeof (menu_act_png));
  socket_send (pSocketEl, pRemoteAddress, sizeof (menu_act_png), (U8*)&menu_act_png [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_image_submenu_act() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_image_submenu_act (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "image/png", sizeof (submenu_act_png));
  socket_send (pSocketEl, pRemoteAddress, sizeof (submenu_act_png), (U8*)&submenu_act_png [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_image_news_feed_header() is the handler for one of the web objects (function name = object name)
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_image_news_feed_header (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  web_http_start (pSocketEl, pRemoteAddress, "image/png", sizeof (news_feed_header_png));
  socket_send (pSocketEl, pRemoteAddress, sizeof (news_feed_header_png), (U8*)&news_feed_header_png [0]);

  return (WEB_PAGE_RETURN_SUCCESS);
}

/*===========================================================================*/

/**
 * The function web_split_formdata() decodes a formdata string and stores a
 * reference to each form in the Formdata array.
 *
 * @return
 * - type : int
 * @param[in] pFormdata Pointer to a form string
 * - type : char*
 * - range: whole address range is valid
 * @param[in] pFormlist Pointer to the Formlist array
 * - type : char*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : U32 Maximum number of forms in Formlist
 * - range: whole range is valid
 */
void web_split_formdata (char* pFormdata, char* pFormlist [], U32 MaxForms)
{
  U32 Index = 0;

  MaxForms--;

  pFormlist [0] = pFormdata;

  while ((*pFormdata != '\0') && (Index < MaxForms))
  {
    if (*pFormdata == '&')
    {
      *pFormdata++          = '\0';
      pFormlist [++Index]   = pFormdata;
      continue;
    }

    if (*pFormdata == '+') {*pFormdata = ' ';}

    pFormdata++;
  }

  *pFormlist [++Index] = 0;

  return;
}

/*===========================================================================*/

/**
 * The function web_find_form() searches for the form with the
 * requested name on a Formlist. The Formlist must be prepared with the
 * function web_split_formdata().
 *
 * @return
 * - type : int
 * @param[in] pFormlist Pointer to a Formlist array
 * - type : char**
 * - range: whole address range is valid
 * @param[in] pName Pointer to the requested form name
 * - type : char*
 * - range: whole address range is valid
 */
char* web_find_form (char* pFormlist [], char* pName)
{
  while (*pFormlist != 0)
  {
    char* pSrc = *pFormlist;
    char* pDst = pName;

    while (*pSrc == *pDst) {pSrc++, pDst++;}

    if (*pDst == 0 && *pSrc == '=') {return (pSrc + 1);}

    pFormlist++;
  }

  return (NULL);
}

/*===========================================================================*/

/**
 * The function web_ajax_handler() is the main handler for all ajax requests.
 * it generates the JASON response depending on the requesting webpage
 *
 * @return
 * - type : int
 * @param[in] pSocketEl Pointer to the receiving socket
 * - type : T_SOCKET_ELEMENT*
 * - range: whole address range is valid
 * @param[in] pRemoteAddress Pointer to the address information of the requestor
 * - type : T_SDAI_SOCK_ADDR*
 * - range: whole address range is valid
 * @param[in] pFormdata Pointer to the formdata of the request
 * - type : char*
 * - range: whole address range is valid
 */
static int web_ajax_handler (T_SOCKET_ELEMENT* pSocketEl, T_SDAI_SOCK_ADDR* pRemoteAddress, char* pFormdata)
{
  if (pFormdata != NULL)
  {
    U32   DataLength     = 0;
    U32   Page           = 0;
    char* pFormlist [10] = {0,};
    char* pForm;

    web_split_formdata (pFormdata, pFormlist, _NUMBER_ARRAY_ELEMENTS (pFormlist));

    pForm = web_find_form (pFormlist, "Page");
    if (pForm != NULL)
    {
      sscanf (pForm, "%ld", &Page);
    }

    switch (Page)
    {
      case AJAX_PAGE_DIAGNOSIS:
      {
        unsigned   Index;
        U8         Load100ms;
        U8         Load1s;
        U8         Load10s;

        platform_get_cpu_load (&Load100ms, &Load1s, &Load10s);

        DataLength = sprintf ((char*)&WebTxDataBuffer [0], "{\"Exception\":{\"Act\":\"%u\",\"Cod\":\"%u\",\"Cha\":\"%u\",\"Mod\":\"%u\",\"Fil\":\"%u\",\"Lin\":\"%u\",\"Err\":\"%u\",\"Par\":\"%lu\",\"Str\":\"%s\"},",
                              ExceptionData.InUse,
                              ExceptionData.SdaiExceptionData.Code, ExceptionData.SdaiExceptionData.ChannelId,
                              ExceptionData.SdaiExceptionData.ModuleId, ExceptionData.SdaiExceptionData.FileId,
                              ExceptionData.SdaiExceptionData.Line, ExceptionData.SdaiExceptionData.ErrorId,
                              ExceptionData.SdaiExceptionData.Parameter, ExceptionData.SdaiExceptionData.String);

        DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], "\"FBLoad\":{\"Load100ms\":\"%u\",\"Load1s\":\"%u\",\"Load10s\":\"%u\"},",
                              ApplManagement.ProtocolStatusData.PerformanceData.CpuLoad100ms,
                              ApplManagement.ProtocolStatusData.PerformanceData.CpuLoad1s,
                              ApplManagement.ProtocolStatusData.PerformanceData.CpuLoad10s);

        DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], "\"ApplLoad\":{\"Load100ms\":\"%u\",\"Load1s\":\"%u\",\"Load10s\":\"%u\"},",
                               Load100ms, Load1s, Load10s);

        DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], "\"Interface\":{\"Type\":\"1\",\"State\":\"%u\",\"Ports\":[",
                               ApplManagement.ProtocolStatusData.InterfaceData.InterfaceStatus);

        for (Index = 0u; Index < ApplManagement.ProtocolStatusData.InterfaceData.NumberExistingEthernetPorts; Index++)
        {
          struct SDAI_ADDSTATUS_ETH_PORT_DATA* pPortData = &ApplManagement.ProtocolStatusData.InterfaceData.EthPortData [Index];

          if (Index > 0u) { DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], ","); }

          DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], "{\"Lnk\":\"%u\",\"Con\":\"%u\",\"Neg\":\"%u\",\"Len\":\"%u\"}",
                                 pPortData->LinkState, pPortData->ConnectionType, pPortData->NegotiationState, pPortData->CableLength);
        }

        DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], "]}}");

        break;
      }

      case AJAX_PAGE_IODATA:
      {
        BOOL                FirstEntry = TRUE;
        struct T_DEMO_UNIT* pUnit      = &ApplManagement.UnitsManagement.UnitList [0];
        unsigned            Index;

        DataLength = sprintf ((char*)&WebTxDataBuffer [0], "{\"IO\":[");

        while (pUnit <= &ApplManagement.UnitsManagement.UnitList [SDAI_MAX_UNITS])
        {
          if (pUnit->Type != 0)
          {
            if (!FirstEntry) {DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], ",");}

            DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], "{\"Id\":\"0x%8.8lX\",\"Type\":\"%d\",\"StatusIn\":\"%X\",\"StatusOut\":\"%X\",\"DataIn\":[", pUnit->Id, pUnit->Type, pUnit->InputState, pUnit->OutputState);

            for (Index = 0u; Index < pUnit->InputSize; Index++)
            {
              if (Index > 0u) { DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], ",\"%d\"", pUnit->pInputData [Index]); }
              else            { DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], "\"%d\"", pUnit->pInputData [Index]);  }
            }

            DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], "],\"DataOut\":[");

            for (Index = 0u; Index < pUnit->OutputSize; Index++)
            {
              if (Index > 0u) { DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], ",\"%d\"", pUnit->pOutputData [Index]); }
              else            { DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], "\"%d\"", pUnit->pOutputData [Index]);  }
            }

            DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], "]}");

            FirstEntry = FALSE;
          }

          pUnit++;
        }

        DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], "]}");
        break;
      }

      case AJAX_PAGE_FIRMWARE:
      {
        char* pHeaderLine;
        char* pBoundary;
        U8    Result;

        DataLength = sprintf ((char*)&WebTxDataBuffer [0], "{\"Result\":\"Failed\"}");

        pHeaderLine = web_get_header_line ("Content-Type");

        if (pHeaderLine != NULL)
        {
          pBoundary = strstr (pHeaderLine, "boundary=");

          if (pBoundary != NULL)
          {
            U8* pFirmwareBuffer;
            int ContentLength;

            pBoundary += (sizeof("boundary=") - 1);

            pHeaderLine = web_get_header_line ("Content-Length");

            if (pHeaderLine != NULL)
            {
              ContentLength = atoi (pHeaderLine);

              pFirmwareBuffer = malloc (ContentLength);

              if (pFirmwareBuffer != NULL)
              {
                U32   ReceiveLength = HtmlHeader.RemainingLength;
                U16   Length        = _MIN(SOCKET_FIFO_SIZE, (ContentLength - ReceiveLength));

                memcpy (pFirmwareBuffer, HtmlHeader.pBody, ReceiveLength);

                while (ReceiveLength != ContentLength)
                {
                  Result = sdai_sock_receive (pSocketEl->SocketNumber, pRemoteAddress, &Length, (pFirmwareBuffer + ReceiveLength));

                  if (Result == SOCK_SUCCESS)
                  {
                    ReceiveLength += Length;
                    Length         = _MIN(SOCKET_FIFO_SIZE, (ContentLength - ReceiveLength));
                  }
                  else if (Result != SOCK_ERR_NO_DATA_RECEIVED)
                  {
                    break;
                  }
                }

                if (ReceiveLength == ContentLength)
                {
                  U8* pFirmwareStart = (U8*)strstr ((char*)pFirmwareBuffer, "\r\n\r\n");

                  if (pFirmwareStart != NULL)
                  {
                    U32 FirmwareSize;

                    pFirmwareStart += 4;
                    FirmwareSize    = ContentLength - (pFirmwareStart - pFirmwareBuffer) - strlen (pBoundary) - (sizeof ("\r\n\r\n--\r\n") - 1);

                    if ((U32)pFirmwareStart & 0x0000003)
                    {
                      /* pFirmwareStart must be word-aligned */
                      memmove ((U8*)((U32)pFirmwareStart & ~0x0000003), pFirmwareStart, FirmwareSize);
                      pFirmwareStart = (U8*)((U32)pFirmwareStart & ~0x0000003);
                    }

                    if (firmware_update (pFirmwareStart, FirmwareSize) == 0)
                    {
                      DataLength = sprintf ((char*)&WebTxDataBuffer [0], "{\"Result\":\"Success\"}");
                    }
                  }
                }

                free (pFirmwareBuffer);
              }
            }
          }
        }

        break;
      }

      case AJAX_PAGE_OVERVIEW:
      case AJAX_PAGE_NETWORK:
      {
        if (Page == AJAX_PAGE_NETWORK && strstr (HtmlHeader.pRequest, "POST"))
        {
          unsigned short Temp [4];

          pForm = web_find_form (pFormlist, "Name");
          if (pForm != NULL)
          {
            strncpy ((char*)pFlashConfigData->DeviceName, pForm, SDAI_DEVNAME_MAX_LEN);
          }

          pForm = web_find_form (pFormlist, "Ip");
          if (pForm != NULL)
          {
            sscanf (pForm, "%hu.%hu.%hu.%hu", &Temp [0], &Temp [1],
                                              &Temp [2], &Temp [3]);

            pFlashConfigData->IpAddress [0] = (U8)Temp [0];
            pFlashConfigData->IpAddress [1] = (U8)Temp [1];
            pFlashConfigData->IpAddress [2] = (U8)Temp [2];
            pFlashConfigData->IpAddress [3] = (U8)Temp [3];
          }

          pForm = web_find_form (pFormlist, "Nm");
          if (pForm != NULL)
          {
            sscanf (pForm, "%hu.%hu.%hu.%hu", &Temp [0], &Temp [1],
                                              &Temp [2], &Temp [3]);

            pFlashConfigData->Netmask [0] = (U8)Temp [0];
            pFlashConfigData->Netmask [1] = (U8)Temp [1];
            pFlashConfigData->Netmask [2] = (U8)Temp [2];
            pFlashConfigData->Netmask [3] = (U8)Temp [3];
          }

          pForm = web_find_form (pFormlist, "Gw");
          if (pForm != NULL)
          {
            sscanf (pForm, "%hu.%hu.%hu.%hu", &Temp [0], &Temp [1],
                                              &Temp [2], &Temp [3]);

            pFlashConfigData->Gateway [0] = (U8)Temp [0];
            pFlashConfigData->Gateway [1] = (U8)Temp [1];
            pFlashConfigData->Gateway [2] = (U8)Temp [2];
            pFlashConfigData->Gateway [3] = (U8)Temp [3];
          }

          platform_visualize_data (&ApplManagement);

          platform_send_event (EVENT_STORE_FLASH_DATA | EVENT_RESTART_STACK);
        }

        DataLength = sprintf ((char*)&WebTxDataBuffer [0], "{\"Proto\":\"%u\",\"Status\":\"%u\",",
                              pFlashConfigData->BackEnd, ApplManagement.StackStatus);

        DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], "\"Name\":\"%s\",",
                               &pFlashConfigData->DeviceName [0]);

        DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], "\"Md\":\"%02X:%02X:%02X:%02X:%02X:%02X\",",
                               pFlashHwData->MacAddress [0], pFlashHwData->MacAddress [1], pFlashHwData->MacAddress [2],
                               pFlashHwData->MacAddress [3], pFlashHwData->MacAddress [4], pFlashHwData->MacAddress [5]);

        DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], "\"Mp1\":\"%02X:%02X:%02X:%02X:%02X:%02X\",",
                               pFlashHwData->MacAddressPort1 [0], pFlashHwData->MacAddressPort1 [1], pFlashHwData->MacAddressPort1 [2],
                               pFlashHwData->MacAddressPort1 [3], pFlashHwData->MacAddressPort1 [4], pFlashHwData->MacAddressPort1 [5]);

        DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], "\"Mp2\":\"%02X:%02X:%02X:%02X:%02X:%02X\",",
                               pFlashHwData->MacAddressPort2 [0], pFlashHwData->MacAddressPort2 [1], pFlashHwData->MacAddressPort2 [2],
                               pFlashHwData->MacAddressPort2 [3], pFlashHwData->MacAddressPort2 [4], pFlashHwData->MacAddressPort2 [5]);

        DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], "\"Ip\":\"%d.%d.%d.%d\",",
                               pFlashConfigData->IpAddress [0], pFlashConfigData->IpAddress [1],
                               pFlashConfigData->IpAddress [2], pFlashConfigData->IpAddress [3]);

        DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], "\"Nm\":\"%d.%d.%d.%d\",",
                               pFlashConfigData->Netmask [0], pFlashConfigData->Netmask [1],
                               pFlashConfigData->Netmask [2], pFlashConfigData->Netmask [3]);

        DataLength += sprintf ((char*)&WebTxDataBuffer [DataLength], "\"Gw\":\"%d.%d.%d.%d\"}",
                               pFlashConfigData->Gateway [0], pFlashConfigData->Gateway [1],
                               pFlashConfigData->Gateway [2], pFlashConfigData->Gateway [3]);

        break;
      }

      case AJAX_PAGE_VERSION:
      {
        DataLength = sprintf ((char*)&WebTxDataBuffer [0], "{\"Appl\":\"%s\",\"Sdai\":\"%16s\",\"Stack\":\"%16s\",\"Mac\":\"%16s\"}",
                              APPLICATION_VERSION, ApplManagement.VersionData.SdaiVersionString, ApplManagement.VersionData.StackVersionString, ApplManagement.VersionData.MacVersionString);
        break;
      }

      default:
      {
        return (WEB_PAGE_RETURN_ERROR_REQUEST);
      }
    }

    web_http_start (pSocketEl, pRemoteAddress, "text/plain; charset=ISO-8859-1", DataLength);
    socket_send (pSocketEl, pRemoteAddress, DataLength, &WebTxDataBuffer [0]);
  }
  else
  {
    return (WEB_PAGE_RETURN_ERROR_REQUEST);
  }

  return (WEB_PAGE_RETURN_SUCCESS);
}

#endif /* SDAI_SOCKET_INTERFACE && SDAI_WEBSERVER */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
