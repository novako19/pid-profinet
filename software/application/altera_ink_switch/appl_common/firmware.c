/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

#ifdef SDAI_SOCKET_INTERFACE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include "sdai.h"

#include "demo_platform.h"
#include "demo.h"

/******************************************************************************
DEFINES
******************************************************************************/

/** \addtogroup util_def */
//@{
#define FIRMWARE_MAGIC_NUMBER            987654321u       /**< Magic value used in the firmware file header */
#define FIRMWARE_FILE_HEADER_SEGMENTS    2u               /**< Number of header segments in the firmware file (Magic number, Compatibility list) */

#define GET_FIRMWARE_SEGMENT(Buffer, Size, Position, Length, Segment)     \
    Length = ((Position + 4) <= Size) ? *(int*)(Buffer+Position) : -1;    \
    if (Length >= (Size - (Position + 4))) {                              \
      Segment = NULL; Position = Size;                                    \
    } else {                                                              \
      Segment = Buffer + (Position + 4); Position += ((Length + 7) & ~3); \
    }                                                                         /**< Macro to split the firmware file into segments */
//@}

/******************************************************************************
TYPEDEFS
******************************************************************************/

/**< \brief This structure defines the structure of a firmware segment */
typedef struct _T_FIRMWARE_SEGMENT
{
  U32   Length;
  U8*   pSegment;

} T_FIRMWARE_SEGMENT;

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

/** \addtogroup util_func */
//@{
static unsigned long   firmware_checksum            (unsigned char*, unsigned long);
static BOOL            firmware_update_check_file   (U8*, U32);
static BOOL            firmware_update_execute      ();
//@}

/******************************************************************************
PUBLIC DATA
******************************************************************************/

/******************************************************************************
PRIVATE DATA
******************************************************************************/

static T_CONFIG_DATA_HW*            pFlashHwData = &ApplManagement.ConfigDataHw;

static T_FIRMWARE_SEGMENT           FirmwareSegments [FIRMWARE_FILE_HEADER_SEGMENTS + FLASH_NUMBER_FW_SEGMENTS];

/******************************************************************************
FUNCTION IMPLEMENTATION
******************************************************************************/

/**
 * The function firmware_checksum() verifies the checksum of the firmware file.
 * If the checksum is correct the functions returns 0.
 *
 * @return
 * - type : unsigned long
 * @param[in] pBuffer
 * - type : unsigned char*
 * - range: whole address range is valid
 * @param[in] Size
 * - type : unsigned long
 * - range: whole range is valid
 */
static unsigned long firmware_checksum (unsigned char* pBuffer, unsigned long Size)
{
  long Sum = 0;

  _TRACE(("firmware_checksum\r\n"));

  while (Size >= sizeof(long))
  {
    Sum     += *(long*)pBuffer;
    pBuffer += sizeof(long);
    Size    -= sizeof(long);
  }

  return (-Sum);
}

/*===========================================================================*/

/**
 * The function firmware_update_check_file() checks if the firmware file is valid.
 * First the Magic Number and the checksum ist verified. Next the file is split up into
 * the firmware segments. Finally the compatibility list is checked against the bootcode
 * stored in the FLASH data. If the file is valid, TRUE ist returned, else FALSE. The references
 * to the firmware segments are stored in the global variable FirmwareSegments.
 *
 * @return
 * - type : BOOL
 * @param[in] pFirmwareBuffer
 * - type : U8*
 * - range: whole address range is valid
 * @param[in] FirmwareSize
 * - type : U32
 * - range: whole range is valid
 */
static BOOL firmware_update_check_file (U8* pFirmwareBuffer, U32 FirmwareSize)
{
  U32   Position = 4;
  U32   SegmentIndex;

  _TRACE(("firmware_update_check_file\r\n"));

  if ((FirmwareSize < 4) || (*((U32*)pFirmwareBuffer) != FIRMWARE_MAGIC_NUMBER))
  {
    return (FALSE);
  }

  if (firmware_checksum (pFirmwareBuffer, FirmwareSize) != 0)
  {
    return (FALSE);
  }

  for (SegmentIndex = 0; SegmentIndex < (FIRMWARE_FILE_HEADER_SEGMENTS + FLASH_NUMBER_FW_SEGMENTS); SegmentIndex++)
  {
    FirmwareSegments[SegmentIndex].pSegment = NULL;
    FirmwareSegments[SegmentIndex].Length   = 0;
  }

  for (SegmentIndex = 0; SegmentIndex < (FIRMWARE_FILE_HEADER_SEGMENTS + FLASH_NUMBER_FW_SEGMENTS); SegmentIndex++)
  {
    GET_FIRMWARE_SEGMENT (pFirmwareBuffer, FirmwareSize, Position, FirmwareSegments[SegmentIndex].Length, FirmwareSegments[SegmentIndex].pSegment);

    if (FirmwareSegments[SegmentIndex].pSegment)
    {
      FirmwareSegments[SegmentIndex].Length = (FirmwareSegments[SegmentIndex].Length + 3) & ~3;
    }
    else
    {
      Position = FirmwareSize - 4; /* correct the position, to pass further checks */
      FirmwareSegments[SegmentIndex].Length = 0;

      break;
    }
  }

  if ((Position + 4) != FirmwareSize)
  {
    return (FALSE);
  }

  for (Position = 0; Position < FirmwareSegments[1].Length; Position += 4)
  {
    if (pFlashHwData->Bootcode == *(unsigned long*)(FirmwareSegments[1].pSegment + Position)){break;}
  }

  if (Position >= FirmwareSegments[1].Length)
  {
    return (FALSE);
  }

  return (TRUE);
}

/*===========================================================================*/

/**
 * The function firmware_update_execute() stores the firmware segments in the
 * flash by calling the function platform_handle_firmware_segment(). First all
 * FLASH sectors are erased. Then the new data are written to the FLASH and
 * verified.
 *
 * @return
 * - type : BOOL
 * @pre Before this function the function firmware_update_check_file() must be
 *      called to fill the FirmwareSegments global variable.
 */

static BOOL firmware_update_execute ()
{
  U32   SegmentIndex;

  _TRACE(("firmware_update_execute\r\n"));

  /* delete all firmware segments first */
  for (SegmentIndex = 0; SegmentIndex < FLASH_NUMBER_FW_SEGMENTS; SegmentIndex++)
  {
    if (platform_handle_firmware_segment (FW_ACTION_ERASE, SegmentIndex, 0, NULL))
    {
      return (FALSE);
    }
  }

  /* write firmware, start with the software segments */
  for (SegmentIndex = 0; SegmentIndex < FLASH_NUMBER_FW_SEGMENTS; SegmentIndex++)
  {
    if ( (FirmwareSegments[FIRMWARE_FILE_HEADER_SEGMENTS + SegmentIndex].Length > 0      ) &&
         (FirmwareSegments[FIRMWARE_FILE_HEADER_SEGMENTS + SegmentIndex].pSegment != NULL) )
    {
      if (platform_handle_firmware_segment (FW_ACTION_WRITE, SegmentIndex, FirmwareSegments[FIRMWARE_FILE_HEADER_SEGMENTS + SegmentIndex].Length, FirmwareSegments[FIRMWARE_FILE_HEADER_SEGMENTS + SegmentIndex].pSegment))
      {
        return (FALSE);
      }
    }
  }

  /* finally verify all segments */
  for (SegmentIndex = 0; SegmentIndex < FLASH_NUMBER_FW_SEGMENTS; SegmentIndex++)
  {
    if ( (FirmwareSegments[FIRMWARE_FILE_HEADER_SEGMENTS + SegmentIndex].Length > 0      ) &&
         (FirmwareSegments[FIRMWARE_FILE_HEADER_SEGMENTS + SegmentIndex].pSegment != NULL) )
    {
      if (platform_handle_firmware_segment (FW_ACTION_VERIFY, SegmentIndex, FirmwareSegments[FIRMWARE_FILE_HEADER_SEGMENTS + SegmentIndex].Length, FirmwareSegments[FIRMWARE_FILE_HEADER_SEGMENTS + SegmentIndex].pSegment))
      {
        return (FALSE);
      }
    }
  }

  platform_handle_firmware_segment (FW_ACTION_FINISH, 0, 0, NULL);

  return (TRUE);
}

/*===========================================================================*/

/**
 * The function firmware_update() checks a firmware file and stores it to
 * flash if valid
 *
 * @return
 * - type  : U8
 * @param[in] pFirmwareBuffer
 * - type : U8*
 * - range: whole address range is valid
 * @param[in] FirmwareSize
 * - type : U32
 * - range: whole range is valid
 */
U8 firmware_update (U8* pFirmwareBuffer, U32 FirmwareSize)
{
  _TRACE(("firmware_update\r\n"));

  if (firmware_update_check_file (pFirmwareBuffer, FirmwareSize) == FALSE)
  {
    return (1);
  }

  if (firmware_update_execute () == FALSE)
  {
    return (1);
  }

  return (0);
}

#endif /* SDAI_SOCKET_INTERFACE */

/*****************************************************************************/
/* vim: set nu ts=2 et nowrap: */
