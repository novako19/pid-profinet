/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

#ifndef __EPCQ256_FLASH_MAP_H__
#define __EPCQ256_FLASH_MAP_H__

#ifdef DOXYGEN
/** @page platform_porting_flash_epcq256 EPCQ256
 *
 * <TABLE>
 * <TR><TH>Name</TH><TH>Address</TH><TH>Description</TH></TR>
 * <TR><TD>#FLASH_FPGA_IMAGE1_OFFSET</TD><TD>0x00000000</TD><TD>-</TD></TR>
 * <TR><TD>#FLASH_FW_IMAGE1_OFFSET</TD><TD>0x00D00000</TD><TD>-</TD></TR>
 * <TR><TD>#FLASH_FW_IMAGE2_OFFSET</TD><TD>0x00F00000</TD><TD>-</TD></TR>
 * <TR><TD>#FLASH_CONF_STACK1_OFFSET</TD><TD>0x01F00000</TD><TD>-</TD></TR>
 * <TR><TD>#FLASH_CONF_STACK2_OFFSET</TD><TD>0x01F10000</TD><TD>-</TD></TR>
 * <TR><TD>#FLASH_CONF_APPL1_OFFSET</TD><TD>0x01F20000</TD><TD>-</TD></TR>
 * <TR><TD>#FLASH_CONF_APPL2_OFFSET</TD><TD>0x01F30000</TD><TD>-</TD></TR>
 * <TR><TD>#FLASH_CONF_BOOT_OFFSET</TD><TD>0x01F40000</TD><TD>-</TD></TR>
 * </TABLE>
 *
 */
#endif

/** \addtogroup platform_flash_epcq256_def */
//@{
#define FLASH_FPGA_IMAGE1_OFFSET     0x00000000  /**< Holds the common FPGA image for protocol stack and the bootloader.
                                                      (bootloader located directly at the end of the FPGA factory image). */
#define FLASH_FW_IMAGE1_OFFSET       0x00D00000  /**< Holds the common firmware with the protocol stacks
                                                      The maximum size for this firmware is 2 Mbyte. */
#define FLASH_FW_IMAGE2_OFFSET       0x00F00000  /**< Holds the firmware with the application. The maximum size for
                                                      this firmware is 0,8 Mbyte. */
#define FLASH_CONF_STACK1_OFFSET     0x01F00000  /**< Holds the stack configuration data of the device. The maximum size for
                                                      this area is 64  Kbyte. Do not change this address without changing it also in Stack software */
#define FLASH_CONF_STACK2_OFFSET     0x01F10000  /**< Holds the stack configuration data of the device. The maximum size for
                                                      this area is 64  Kbyte. Do not change this address without changing it also in Stack software */
#define FLASH_CONF_APPL1_OFFSET      0x01F20000  /**< Holds the primary configuration data of the device. The maximum size for
                                                      this area is 64  Kbyte. */
#define FLASH_CONF_APPL2_OFFSET      0x01F30000  /**< Holds the backup configuration data of the device. The maximum size for
                                                      this area is 64  Kbyte. */
#define FLASH_CONF_BOOT_OFFSET       0x01F40000  /**< For future use. The maximum size for this area is 64  Kbyte */

#define FLASH_FPGA_IMAGE1_ADDR       (FLASH_BASE + FLASH_FPGA_IMAGE1_OFFSET )
#define FLASH_FW_IMAGE1_ADDR         (FLASH_BASE + FLASH_FW_IMAGE1_OFFSET   )
#define FLASH_FW_IMAGE2_ADDR         (FLASH_BASE + FLASH_FW_IMAGE2_OFFSET   )
#define FLASH_CONF_STACK1_ADDR       (FLASH_BASE + FLASH_CONF_STACK1_OFFSET )
#define FLASH_CONF_STACK2_ADDR       (FLASH_BASE + FLASH_CONF_STACK2_OFFSET )
#define FLASH_CONF_APPL1_ADDR        (FLASH_BASE + FLASH_CONF_APPL1_OFFSET  )
#define FLASH_CONF_APPL2_ADDR        (FLASH_BASE + FLASH_CONF_APPL2_OFFSET  )
#define FLASH_CONF_BOOT_ADDR         (FLASH_BASE + FLASH_CONF_BOOT_OFFSET   )

#define FLASH_FPGA_IMAGE1_SIZE       (FLASH_FW_IMAGE1_ADDR - FLASH_FPGA_IMAGE1_ADDR  )
#define FLASH_FW_IMAGE1_SIZE         (FLASH_FW_IMAGE2_ADDR - FLASH_FW_IMAGE1_ADDR    )
#define FLASH_FW_IMAGE2_SIZE         (FLASH_CONF_STACK1_ADDR - FLASH_FW_IMAGE2_ADDR  )
#define FLASH_CONF_STACK1_SIZE       (FLASH_CONF_STACK2_ADDR - FLASH_CONF_STACK1_ADDR)
#define FLASH_CONF_STACK2_SIZE       (FLASH_CONF_APPL1_ADDR - FLASH_CONF_STACK2_ADDR )
#define FLASH_CONF_APPL1_SIZE        (FLASH_CONF_APPL2_ADDR - FLASH_CONF_APPL1_ADDR  )
#define FLASH_CONF_APPL2_SIZE        (FLASH_CONF_BOOT_ADDR - FLASH_CONF_APPL2_ADDR   )
#define FLASH_CONF_BOOT_SIZE         (FLASH_CONF_BOOT_ADDR - FLASH_CONF_BOOT_ADDR    )

/*---------------------------------------------------------------------------*/

#define FLASH_REGISTER_ADDRESS         (EPCS_CONFIG_FLASH_BASE + EPCS_CONFIG_FLASH_REGISTER_OFFSET)
#define FLASH_DEVICE_EPCS              1

/*---------------------------------------------------------------------------*/

#define FLASH_MAGIC_NUMBER             0xA55A                /**< Magic number to mark data in flash as valid */

#define FLASH_APPL_CONFIG_VERSION      0x0101                /**< V1.1 Current version of application config stored in flash */
#define FLASH_BOOT_CONFIG_VERSION      0x0100                /**< V1.0 Current version of hardware config stored in flash */

/*---------------------------------------------------------------------------*/

#if defined RTE_PROTOCOL_ETHERNETIP
#define BACKEND   0x01
#elif defined RTE_PROTOCOL_PROFINET
#define BACKEND   0x02
#elif defined RTE_PROTOCOL_PROFIBUS
#define BACKEND   0x03
#elif defined RTE_PROTOCOL_ETHERCAT
#define BACKEND   0x04
#elif defined RTE_PROTOCOL_MODBUS
#define BACKEND   0x05
#elif defined RTE_PROTOCOL_POWERLINK
#define BACKEND   0x06
#else
#define BACKEND   0x00
#endif
//@}

#endif /* __EPCQ256_FLASH_MAP_H__ */
