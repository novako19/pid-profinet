TARGET=ink_pn
CPU = nios
BOARD  = ink
BOARDVENDOR = altera
FPGAVENDOR = altera
IPCORE = switch
PROTOCOL := PROFINET
SOPCINFO_FILE = $(SOFTWARE_BASE_PATH)/../hardware/fpga/profinet/$(BOARDVENDOR)_$(BOARD)_$(IPCORE)/qsys_profinet_system.sopcinfo
SOF_FILE = $(SOFTWARE_BASE_PATH)/../hardware/fpga/profinet/$(BOARDVENDOR)_$(BOARD)_$(IPCORE)/$(BOARDVENDOR)_$(TARGET).sof
JDI_FILE = $(SOFTWARE_BASE_PATH)/../hardware/fpga/profinet/$(BOARDVENDOR)_$(BOARD)_$(IPCORE)/$(BOARDVENDOR)_$(TARGET).jdi
HW_FLAGS += -DHARDWARE_INK
include $(SOFTWARE_BASE_PATH)/application/$(BOARDVENDOR)_$(BOARD)_$(IPCORE)/epcs64.flashmap
