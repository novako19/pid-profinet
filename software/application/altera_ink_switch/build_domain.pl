#******************************************************************************

# SOFTING Industrial Automation GmbH
# Richard-Reitzner-Allee 6
# D-85540 Haar
# Phone: ++49-89-4 56 56-0
# Fax: ++49-89-4 56 56-3 99
# http://www.softing.com

# Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

# Version: 1.35.00

#******************************************************************************/

#! /usr/bin/perl

sub error($)
{
  print "$_[0]\n";
  exit 1;
}

sub srec2hex($)
{
  my $hex;
  open (INPUT, "<$_[0]") or error "failed to open $_[0]";
  $hex = "";
  while (<INPUT>)
  {
    if    ( /^S1......((..)+)[0-9A-F][0-9A-F][\r]?$/ )
    {
      $hex .= $1;
    }
    elsif ( /^S2........((..)+)[0-9A-F][0-9A-F][\r]?$/ )
    {
      $hex .= $1;
    }
    elsif ( /^S3..........((..)+)[0-9A-F][0-9A-F][\r]?$/ )
    {
      $hex .= $1;
    }
    elsif ( !/^S/ )
    {
      error "syntax error in $_[0] line $.";
    }
  } 
  close INPUT;
  $hex = unpack(H8,pack(V,length($hex)/2)) . $hex;
  while (length($hex)/2 % 4)
  {
    $hex .= "FF";
  }
  return $hex;
}


#
# main
#

if ($#ARGV < 0)
{
  print "USAGE: $0 download_domain.bin version.srec compat.srec fpga.hexout image0.srec image1.srec\n";
  exit 1;
}

$MAGIC_NUMBER     = 987654321;
$DOWNLOAD_DOMAIN  = unpack(H8,pack(V,$MAGIC_NUMBER));

for ($i=1; $i<=$#ARGV; $i++)
{
  $DOWNLOAD_DOMAIN .= srec2hex($ARGV[$i]);
}

$sum = 0;
for ($i=0; $i<length($DOWNLOAD_DOMAIN); $i+=8)
{
  $sum += unpack(V,pack(H8,substr($DOWNLOAD_DOMAIN,$i,8)));
}

$DOWNLOAD_DOMAIN .= unpack(H8,pack(V,-$sum));
open (OUTPUT, ">$ARGV[0]") or error "failed to create $_ARGV[0]";
print OUTPUT pack("H*",$DOWNLOAD_DOMAIN);
close (OUTPUT);

###EOF###
