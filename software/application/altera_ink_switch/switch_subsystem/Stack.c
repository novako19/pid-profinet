/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.00.00

******************************************************************************/

#include <pkgconf/system.h>

#ifdef CYGPKG_HAL_MICROBLAZE
  #include <pkgconf/hal_microblaze.h>
  #include <pkgconf/hal_microblaze_platform.h>
  #include <cyg/hal/platform.h>
  #include <cyg/hal/var_cache.h>
#elif defined CYGPKG_HAL_NIOS2
  #include <cyg/hal/system.h>
#endif

#include <cyg/hal/hal_arch.h>
#include <cyg/hal/hal_io.h>

#include <cyg/kernel/kapi.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sdai.h"

/******************************************************************************
DEFINES
******************************************************************************/

#ifndef CONCAT
  #define CONCATx(a,b) a##b
  #define CONCAT(a,b) CONCATx(a,b)
#endif

#ifdef __SYSTEM_CAPABILITIES
  #define CAPABILITIES_EXT_TO_AVM_ENABLED      CONCAT(CYG_HAL_SUBSYSTEM_NAME, _SUBSYSTEM_CAPABILITIES_EXT_TO_AVM_ENABLED)
#endif

#ifdef __SOFTING_EXTBUS_TO_AVM
  #define EXTBUS_TO_AVM_BASE                   CONCAT(CYG_HAL_SUBSYSTEM_NAME, _EXTBUS_TO_AVM_BASE)
#endif

#ifdef __SOFTING_DEBUG_SLAVE
  #define DEBUG_SLAVE_BASE                     CONCAT(CYG_HAL_SUBSYSTEM_NAME, _DEBUG_SLAVE_BASE)
  #define DEBUG_SLAVE_IRQ                      CONCAT(CYG_HAL_SUBSYSTEM_NAME, _DEBUG_SLAVE_IRQ)
#endif

#ifdef CYGPKG_HAL_MICROBLAZE
  #define PIO_GIER_OFFSET                      0x11C
  #define PIO_IER_OFFSET                       0x128
  #define PIO_ISR_OFFSET                       0x120
  #define PIO_DATA_REGISTER_OFFSET             0x00
#endif

/*---------------------------------------------------------------------------*/

#define TASK_STACK_SIZE         4096

#define MAPPING_TASK_PRIORITY   19

/******************************************************************************
TYPEDEFS
******************************************************************************/

/******************************************************************************
FUNCTION DECLARATION
******************************************************************************/

void        mapping_thread                     (cyg_addrword_t);

#ifdef CYGPKG_HAL_MICROBLAZE
cyg_uint32  reset_isr                          (cyg_vector_t, cyg_addrword_t);
#endif

#ifdef DEBUG_SLAVE_BASE
cyg_uint32  debug_slave_isr                    (cyg_vector_t, cyg_addrword_t);
void        debug_slave_dsr                    (cyg_vector_t, cyg_ucount32, cyg_addrword_t);
#endif

extern void sdai_protocol_stack_task_main_loop (void);

/******************************************************************************
PUBLIC DATA
******************************************************************************/

/******************************************************************************
PRIVATE DATA
******************************************************************************/

static cyg_handle_t  MappingHandle;
static cyg_thread    MappingThread;
static unsigned char MappingStack [TASK_STACK_SIZE];

#ifdef CYGPKG_HAL_MICROBLAZE
static cyg_interrupt ResetInterrupt;
static cyg_handle_t  ResetInterruptHandle;
#endif

#ifdef DEBUG_SLAVE_BASE
static cyg_interrupt DebugInterrupt;
static cyg_handle_t  DebugInterruptHandle;
#endif

/******************************************************************************
FUNCTION IMPLEMENTATION
******************************************************************************/

int main(int argc, char *argv[])
{
  _AVOID_UNUSED_WARNING(argc);
  _AVOID_UNUSED_WARNING(argv);

#ifdef CAPABILITIES_EXT_TO_AVM_ENABLED
  if (hal_get_hardware_capabilities_0 () & CAPABILITIES_EXT_TO_AVM_ENABLED)
#endif
  {
    #ifdef EXTBUS_TO_AVM_BASE
    /* activate external memory interface if necessary */
    HAL_WRITE_UINT32 ((volatile U32*) EXTBUS_TO_AVM_BASE, 0x01);
    #endif
  }

#ifdef CYGPKG_HAL_MICROBLAZE
  HAL_WRITE_UINT32 ((volatile U32*) (0x30050000 /* PIO_RESET_CPU_FIELDBUS_BASE */ + PIO_GIER_OFFSET), 0x80000000);
  HAL_WRITE_UINT32 ((volatile U32*) (0x30050000 /* PIO_RESET_CPU_FIELDBUS_BASE */ + PIO_IER_OFFSET), 0x00000001);

  cyg_interrupt_create (5, /* PIO_RESET_CPU_FIELDBUS_IRQ */
                        (cyg_priority_t)0,
                        (cyg_addrword_t)NULL,
                        reset_isr,
                        NULL,
                        &ResetInterruptHandle,
                        &ResetInterrupt);

  cyg_interrupt_attach (ResetInterruptHandle);
  cyg_interrupt_acknowledge (5);
  cyg_interrupt_unmask (5);
#endif

#ifdef DEBUG_SLAVE_BASE
  HAL_WRITE_UINT32 ((volatile unsigned long*) (DEBUG_SLAVE_BASE + 4), 0x00000001);

  /* create debug interrupt */
  cyg_interrupt_create (DEBUG_SLAVE_IRQ,
                        (cyg_priority_t)0,
                        (cyg_addrword_t)NULL,
                        debug_slave_isr,
                        debug_slave_dsr,
                        &DebugInterruptHandle,
                        &DebugInterrupt);

  cyg_interrupt_attach(DebugInterruptHandle);
  cyg_interrupt_acknowledge(DEBUG_SLAVE_IRQ);
  cyg_interrupt_unmask(DEBUG_SLAVE_IRQ);
#endif

  /* create protocol mapping task */
  cyg_thread_create (MAPPING_TASK_PRIORITY,
                     mapping_thread,
                     0,
                     "Mapping",
                     &MappingStack[0],
                     TASK_STACK_SIZE,
                     &MappingHandle,
                     &MappingThread);

  cyg_thread_resume (MappingHandle);

  return (0);
}

/*===========================================================================*/

#ifdef CYGPKG_HAL_MICROBLAZE
cyg_uint32 reset_isr (cyg_vector_t vector, cyg_addrword_t data)
{
  _AVOID_UNUSED_WARNING (data);

  cyg_interrupt_mask (vector);
  cyg_interrupt_acknowledge (vector);

  //HAL_PLATFORM_RESET();

  return (CYG_ISR_HANDLED);
}
#endif

/*===========================================================================*/

#ifdef DEBUG_SLAVE_BASE
cyg_uint32 debug_slave_isr (cyg_vector_t vector, cyg_addrword_t data)
{
  _AVOID_UNUSED_WARNING(data);

  cyg_interrupt_mask(vector);
  cyg_interrupt_acknowledge(vector);

  return (CYG_ISR_HANDLED | CYG_ISR_CALL_DSR);
}
#endif

/*===========================================================================*/

#ifdef DEBUG_SLAVE_BASE
void debug_slave_dsr (cyg_vector_t vector, cyg_ucount32 count, cyg_addrword_t data)
{
  _AVOID_UNUSED_WARNING(count);
  _AVOID_UNUSED_WARNING(data);

  HAL_WRITE_UINT32 ((volatile unsigned long*) (DEBUG_SLAVE_BASE + 4), 0x00000001);

  cyg_interrupt_unmask(vector);

  return;
}
#endif

/*===========================================================================*/

void mapping_thread (cyg_addrword_t Data)
{
  _AVOID_UNUSED_WARNING(Data);

  do
  {
    sdai_protocol_stack_task_main_loop ();

  } while (1);

  return;
}


