/******************************************************************************

SOFTING Industrial Automation GmbH
Richard-Reitzner-Allee 6
D-85540 Haar
Phone: ++49-89-4 56 56-0
Fax: ++49-89-4 56 56-3 99
http://www.softing.com

Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

Version: 1.35.00

******************************************************************************/

#ifndef __QSPI256_FLASH_MAP_H__
#define __QSPI256_FLASH_MAP_H__

#ifdef DOXYGEN
/** @page platform_porting_flash_qspi256 QSPI256
 *
 * <TABLE>
 * <TR><TH>Name</TH><TH>Address</TH><TH>Description</TH></TR>
 * <TR><TD>#FLASH_FPGA_IMAGE1_OFFSET</TD><TD>0x00000000</TD><TD>-</TD></TR>
 * <TR><TD>#FLASH_FW_IMAGE1_OFFSET</TD><TD>0x01000000</TD><TD>-</TD></TR>
 * <TR><TD>#FLASH_FW_IMAGE2_OFFSET</TD><TD>0x01800000</TD><TD>-</TD></TR>
 * <TR><TD>#FLASH_CONF_STACK1_OFFSET</TD><TD>0x01F00000</TD><TD>-</TD></TR>
 * <TR><TD>#FLASH_CONF_STACK2_OFFSET</TD><TD>0x01F50000</TD><TD>-</TD></TR>
 * <TR><TD>#FLASH_CONF_APPL1_OFFSET</TD><TD>0x01FD0000</TD><TD>-</TD></TR>
 * <TR><TD>#FLASH_CONF_APPL2_OFFSET</TD><TD>0x01FE0000</TD><TD>-</TD></TR>
 * <TR><TD>#FLASH_CONF_BOOT_OFFSET</TD><TD>0x01FF0000</TD><TD>-</TD></TR>
 * </TABLE>
 *
 */
#endif

/** \addtogroup platform_flash_qspi256_def */
//@{
#define FLASH_FPGA_IMAGE1_OFFSET     0x00000000  /**< Holds the common FPGA image for protocol stack. */
#define FLASH_FW_IMAGE1_OFFSET       0x01000000  /**< Holds the common firmware with the protocol stacks. */
#define FLASH_FW_IMAGE2_OFFSET       0x01800000  /**< Holds the firmware with the application. */
#define FLASH_CONF_STACK1_OFFSET     0x01F00000  /**< Holds the stack configuration data of the device. */
#define FLASH_CONF_STACK2_OFFSET     0x01F50000  /**< Holds the stack configuration data of the device. */
#define FLASH_CONF_APPL1_OFFSET      0x01FD0000  /**< Holds the primary configuration data of the device. */
#define FLASH_CONF_APPL2_OFFSET      0x01FE0000  /**< Holds the backup configuration data of the device. */
#define FLASH_CONF_BOOT_OFFSET       0x01FF0000  /**< Holds the hardware specific data of the device. */

#define FLASH_FPGA_IMAGE1_ADDR       FLASH_FPGA_IMAGE1_OFFSET
#define FLASH_FW_IMAGE1_ADDR         FLASH_FW_IMAGE1_OFFSET
#define FLASH_FW_IMAGE2_ADDR         FLASH_FW_IMAGE2_OFFSET
#define FLASH_CONF_STACK1_ADDR       FLASH_CONF_STACK1_OFFSET
#define FLASH_CONF_STACK2_ADDR       FLASH_CONF_STACK2_OFFSET
#define FLASH_CONF_APPL1_ADDR        FLASH_CONF_APPL1_OFFSET
#define FLASH_CONF_APPL2_ADDR        FLASH_CONF_APPL2_OFFSET
#define FLASH_CONF_BOOT_ADDR         FLASH_CONF_BOOT_OFFSET

#define FLASH_FPGA_IMAGE1_SIZE       (FLASH_FW_IMAGE1_ADDR - FLASH_FPGA_IMAGE1_ADDR)
#define FLASH_FW_IMAGE1_SIZE         (FLASH_CONF_STACK1_ADDR - FLASH_FW_IMAGE1_ADDR)
#define FLASH_FW_IMAGE2_SIZE         (FLASH_FPGA_IMAGE1_ADDR - FLASH_FW_IMAGE2_ADDR)
#define FLASH_CONF_STACK1_SIZE       0x50000
#define FLASH_CONF_STACK2_SIZE       0x50000
#define FLASH_CONF_APPL1_SIZE        0x10000
#define FLASH_CONF_APPL2_SIZE        0x10000
#define FLASH_CONF_BOOT_SIZE         0x10000

/*---------------------------------------------------------------------------*/

#define FLASH_DEVICE_QSPI              1

/*---------------------------------------------------------------------------*/

#define FLASH_MAGIC_NUMBER             0xA55A                /**< Magic number to mark data in flash as valid */

#define FLASH_APPL_CONFIG_VERSION      0x0101                /**< V1.1 Current version of application config stored in flash */
#define FLASH_BOOT_CONFIG_VERSION      0x0100                /**< V1.0 Current version of hardware config stored in flash */

/*---------------------------------------------------------------------------*/

#if defined RTE_PROTOCOL_ETHERNETIP
#define BACKEND   0x01
#elif defined RTE_PROTOCOL_PROFINET
#define BACKEND   0x02
#elif defined RTE_PROTOCOL_PROFIBUS
#define BACKEND   0x03
#elif defined RTE_PROTOCOL_ETHERCAT
#define BACKEND   0x04
#elif defined RTE_PROTOCOL_MODBUS
#define BACKEND   0x05
#elif defined RTE_PROTOCOL_POWERLINK
#define BACKEND   0x06
#else
#define BACKEND   0x00
#endif
//@}

#endif /* __QSPI256_FLASH_MAP_H__ */
