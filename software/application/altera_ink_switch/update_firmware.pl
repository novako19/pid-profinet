#******************************************************************************

# SOFTING Industrial Automation GmbH
# Richard-Reitzner-Allee 6
# D-85540 Haar
# Phone: ++49-89-4 56 56-0
# Fax: ++49-89-4 56 56-3 99
# http://www.softing.com

# Copyright (C) SOFTING Industrial Automation GmbH 2005-2015. All Rights Reserved

# Version: 1.35.00

#******************************************************************************/

#! /usr/bin/perl

use IO::Socket::INET;

#
# main
#

if ($#ARGV != 1)
{
  print "USAGE: update_firmware.pl file ip-address\n";
  exit 1;
}

open (INPUT, "<$ARGV[0]") or die "failed to open $ARGV[0]";

$socket = new IO::Socket::INET (PeerHost => $ARGV[1], PeerPort => '50000', Proto => 'tcp',) or die "ERROR in Socket Creation : $!\n";

print "Connected to $ARGV[1]\n";

sleep (1);

#ignore first response of device
$socket->recv($response, 1000);

while (<INPUT>)
{
  $file .= $_;
}

$size = length($file);
$data = "firmware $size $file";

print "Sending Firmware with size $size byte to $ARGV[1]\n";

$socket->send($data);

print "finished\n";

$socket->recv($response, 1000);
print "[$ARGV[1]]: $response\n";

$socket->recv($response, 1000);
print "[$ARGV[1]]: $response\n";

$socket->close();
close (INPUT);

###EOF###
